var Prototype = {
    Version: '1.7',
    Browser: (function() {
        var ua = navigator.userAgent;
        var isOpera = Object.prototype.toString.call(window.opera) == '[object Opera]';
        return {
            IE: !!window.attachEvent && !isOpera,
            Opera: isOpera,
            WebKit: ua.indexOf('AppleWebKit/') > -1,
            Gecko: ua.indexOf('Gecko') > -1 && ua.indexOf('KHTML') === -1,
            MobileSafari: /Apple.*Mobile/.test(ua)
        }
    })(),
    BrowserFeatures: {
        XPath: !!document.evaluate,
        SelectorsAPI: !!document.querySelector,
        ElementExtensions: (function() {
            var constructor = window.Element || window.HTMLElement;
            return !!(constructor && constructor.prototype);
        })(),
        SpecificElementExtensions: (function() {
            if (typeof window.HTMLDivElement !== 'undefined')
                return true;
            var div = document.createElement('div'),
                form = document.createElement('form'),
                isSupported = false;
            if (div['__proto__'] && (div['__proto__'] !== form['__proto__'])) {
                isSupported = true;
            }
            div = form = null;
            return isSupported;
        })()
    },
    ScriptFragment: '<script[^>]*>([\\S\\s]*?)<\/script>',
    JSONFilter: /^\/\*-secure-([\s\S]*)\*\/\s*$/,
    emptyFunction: function() {},
    K: function(x) {
        return x
    }
};
if (Prototype.Browser.MobileSafari)
    Prototype.BrowserFeatures.SpecificElementExtensions = false;
var Abstract = {};
var Try = {
    these: function() {
        var returnValue;
        for (var i = 0, length = arguments.length; i < length; i++) {
            var lambda = arguments[i];
            try {
                returnValue = lambda();
                break;
            } catch (e) {}
        }
        return returnValue;
    }
};
var Class = (function() {
    var IS_DONTENUM_BUGGY = (function() {
        for (var p in {
                toString: 1
            }) {
            if (p === 'toString') return false;
        }
        return true;
    })();

    function subclass() {};

    function create() {
        var parent = null,
            properties = $A(arguments);
        if (Object.isFunction(properties[0]))
            parent = properties.shift();

        function klass() {
            this.initialize.apply(this, arguments);
        }
        Object.extend(klass, Class.Methods);
        klass.superclass = parent;
        klass.subclasses = [];
        if (parent) {
            subclass.prototype = parent.prototype;
            klass.prototype = new subclass;
            parent.subclasses.push(klass);
        }
        for (var i = 0, length = properties.length; i < length; i++)
            klass.addMethods(properties[i]);
        if (!klass.prototype.initialize)
            klass.prototype.initialize = Prototype.emptyFunction;
        klass.prototype.constructor = klass;
        return klass;
    }

    function addMethods(source) {
        var ancestor = this.superclass && this.superclass.prototype,
            properties = Object.keys(source);
        if (IS_DONTENUM_BUGGY) {
            if (source.toString != Object.prototype.toString)
                properties.push("toString");
            if (source.valueOf != Object.prototype.valueOf)
                properties.push("valueOf");
        }
        for (var i = 0, length = properties.length; i < length; i++) {
            var property = properties[i],
                value = source[property];
            if (ancestor && Object.isFunction(value) && value.argumentNames()[0] == "$super") {
                var method = value;
                value = (function(m) {
                    return function() {
                        return ancestor[m].apply(this, arguments);
                    };
                })(property).wrap(method);
                value.valueOf = method.valueOf.bind(method);
                value.toString = method.toString.bind(method);
            }
            this.prototype[property] = value;
        }
        return this;
    }
    return {
        create: create,
        Methods: {
            addMethods: addMethods
        }
    };
})();
(function() {
    var _toString = Object.prototype.toString,
        NULL_TYPE = 'Null',
        UNDEFINED_TYPE = 'Undefined',
        BOOLEAN_TYPE = 'Boolean',
        NUMBER_TYPE = 'Number',
        STRING_TYPE = 'String',
        OBJECT_TYPE = 'Object',
        FUNCTION_CLASS = '[object Function]',
        BOOLEAN_CLASS = '[object Boolean]',
        NUMBER_CLASS = '[object Number]',
        STRING_CLASS = '[object String]',
        ARRAY_CLASS = '[object Array]',
        DATE_CLASS = '[object Date]',
        NATIVE_JSON_STRINGIFY_SUPPORT = window.JSON && typeof JSON.stringify === 'function' && JSON.stringify(0) === '0' && typeof JSON.stringify(Prototype.K) === 'undefined';

    function Type(o) {
        switch (o) {
            case null:
                return NULL_TYPE;
            case (void 0):
                return UNDEFINED_TYPE;
        }
        var type = typeof o;
        switch (type) {
            case 'boolean':
                return BOOLEAN_TYPE;
            case 'number':
                return NUMBER_TYPE;
            case 'string':
                return STRING_TYPE;
        }
        return OBJECT_TYPE;
    }

    function extend(destination, source) {
        for (var property in source)
            destination[property] = source[property];
        return destination;
    }

    function inspect(object) {
        try {
            if (isUndefined(object)) return 'undefined';
            if (object === null) return 'null';
            return object.inspect ? object.inspect() : String(object);
        } catch (e) {
            if (e instanceof RangeError) return '...';
            throw e;
        }
    }

    function toJSON(value) {
        return Str('', {
            '': value
        }, []);
    }

    function Str(key, holder, stack) {
        var value = holder[key],
            type = typeof value;
        if (Type(value) === OBJECT_TYPE && typeof value.toJSON === 'function') {
            value = value.toJSON(key);
        }
        var _class = _toString.call(value);
        switch (_class) {
            case NUMBER_CLASS:
            case BOOLEAN_CLASS:
            case STRING_CLASS:
                value = value.valueOf();
        }
        switch (value) {
            case null:
                return 'null';
            case true:
                return 'true';
            case false:
                return 'false';
        }
        type = typeof value;
        switch (type) {
            case 'string':
                return value.inspect(true);
            case 'number':
                return isFinite(value) ? String(value) : 'null';
            case 'object':
                for (var i = 0, length = stack.length; i < length; i++) {
                    if (stack[i] === value) {
                        throw new TypeError();
                    }
                }
                stack.push(value);
                var partial = [];
                if (_class === ARRAY_CLASS) {
                    for (var i = 0, length = value.length; i < length; i++) {
                        var str = Str(i, value, stack);
                        partial.push(typeof str === 'undefined' ? 'null' : str);
                    }
                    partial = '[' + partial.join(',') + ']';
                } else {
                    var keys = Object.keys(value);
                    for (var i = 0, length = keys.length; i < length; i++) {
                        var key = keys[i],
                            str = Str(key, value, stack);
                        if (typeof str !== "undefined") {
                            partial.push(key.inspect(true) + ':' + str);
                        }
                    }
                    partial = '{' + partial.join(',') + '}';
                }
                stack.pop();
                return partial;
        }
    }

    function stringify(object) {
        return JSON.stringify(object);
    }

    function toQueryString(object) {
        return $H(object).toQueryString();
    }

    function toHTML(object) {
        return object && object.toHTML ? object.toHTML() : String.interpret(object);
    }

    function keys(object) {
        if (Type(object) !== OBJECT_TYPE) {
            throw new TypeError();
        }
        var results = [];
        for (var property in object) {
            if (object.hasOwnProperty(property)) {
                results.push(property);
            }
        }
        return results;
    }

    function values(object) {
        var results = [];
        for (var property in object)
            results.push(object[property]);
        return results;
    }

    function clone(object) {
        return extend({}, object);
    }

    function isElement(object) {
        return !!(object && object.nodeType == 1);
    }

    function isArray(object) {
        return _toString.call(object) === ARRAY_CLASS;
    }
    var hasNativeIsArray = (typeof Array.isArray == 'function') && Array.isArray([]) && !Array.isArray({});
    if (hasNativeIsArray) {
        isArray = Array.isArray;
    }

    function isHash(object) {
        return object instanceof Hash;
    }

    function isFunction(object) {
        return _toString.call(object) === FUNCTION_CLASS;
    }

    function isString(object) {
        return _toString.call(object) === STRING_CLASS;
    }

    function isNumber(object) {
        return _toString.call(object) === NUMBER_CLASS;
    }

    function isDate(object) {
        return _toString.call(object) === DATE_CLASS;
    }

    function isUndefined(object) {
        return typeof object === "undefined";
    }
    extend(Object, {
        extend: extend,
        inspect: inspect,
        toJSON: NATIVE_JSON_STRINGIFY_SUPPORT ? stringify : toJSON,
        toQueryString: toQueryString,
        toHTML: toHTML,
        keys: Object.keys || keys,
        values: values,
        clone: clone,
        isElement: isElement,
        isArray: isArray,
        isHash: isHash,
        isFunction: isFunction,
        isString: isString,
        isNumber: isNumber,
        isDate: isDate,
        isUndefined: isUndefined
    });
})();
Object.extend(Function.prototype, (function() {
    var slice = Array.prototype.slice;

    function update(array, args) {
        var arrayLength = array.length,
            length = args.length;
        while (length--) array[arrayLength + length] = args[length];
        return array;
    }

    function merge(array, args) {
        array = slice.call(array, 0);
        return update(array, args);
    }

    function argumentNames() {
        var names = this.toString().match(/^[\s\(]*function[^(]*\(([^)]*)\)/)[1].replace(/\/\/.*?[\r\n]|\/\*(?:.|[\r\n])*?\*\//g, '').replace(/\s+/g, '').split(',');
        return names.length == 1 && !names[0] ? [] : names;
    }

    function bind(context) {
        if (arguments.length < 2 && Object.isUndefined(arguments[0])) return this;
        var __method = this,
            args = slice.call(arguments, 1);
        return function() {
            var a = merge(args, arguments);
            return __method.apply(context, a);
        }
    }

    function bindAsEventListener(context) {
        var __method = this,
            args = slice.call(arguments, 1);
        return function(event) {
            var a = update([event || window.event], args);
            return __method.apply(context, a);
        }
    }

    function curry() {
        if (!arguments.length) return this;
        var __method = this,
            args = slice.call(arguments, 0);
        return function() {
            var a = merge(args, arguments);
            return __method.apply(this, a);
        }
    }

    function delay(timeout) {
        var __method = this,
            args = slice.call(arguments, 1);
        timeout = timeout * 1000;
        return window.setTimeout(function() {
            return __method.apply(__method, args);
        }, timeout);
    }

    function defer() {
        var args = update([0.01], arguments);
        return this.delay.apply(this, args);
    }

    function wrap(wrapper) {
        var __method = this;
        return function() {
            var a = update([__method.bind(this)], arguments);
            return wrapper.apply(this, a);
        }
    }

    function methodize() {
        if (this._methodized) return this._methodized;
        var __method = this;
        return this._methodized = function() {
            var a = update([this], arguments);
            return __method.apply(null, a);
        };
    }
    return {
        argumentNames: argumentNames,
        bind: bind,
        bindAsEventListener: bindAsEventListener,
        curry: curry,
        delay: delay,
        defer: defer,
        wrap: wrap,
        methodize: methodize
    }
})());
(function(proto) {
    function toISOString() {
        return this.getUTCFullYear() + '-' +
            (this.getUTCMonth() + 1).toPaddedString(2) + '-' +
            this.getUTCDate().toPaddedString(2) + 'T' +
            this.getUTCHours().toPaddedString(2) + ':' +
            this.getUTCMinutes().toPaddedString(2) + ':' +
            this.getUTCSeconds().toPaddedString(2) + 'Z';
    }

    function toJSON() {
        return this.toISOString();
    }
    if (!proto.toISOString) proto.toISOString = toISOString;
    if (!proto.toJSON) proto.toJSON = toJSON;
})(Date.prototype);
RegExp.prototype.match = RegExp.prototype.test;
RegExp.escape = function(str) {
    return String(str).replace(/([.*+?^=!:${}()|[\]\/\\])/g, '\\$1');
};
var PeriodicalExecuter = Class.create({
    initialize: function(callback, frequency) {
        this.callback = callback;
        this.frequency = frequency;
        this.currentlyExecuting = false;
        this.registerCallback();
    },
    registerCallback: function() {
        this.timer = setInterval(this.onTimerEvent.bind(this), this.frequency * 1000);
    },
    execute: function() {
        this.callback(this);
    },
    stop: function() {
        if (!this.timer) return;
        clearInterval(this.timer);
        this.timer = null;
    },
    onTimerEvent: function() {
        if (!this.currentlyExecuting) {
            try {
                this.currentlyExecuting = true;
                this.execute();
                this.currentlyExecuting = false;
            } catch (e) {
                this.currentlyExecuting = false;
                throw e;
            }
        }
    }
});
Object.extend(String, {
    interpret: function(value) {
        return value == null ? '' : String(value);
    },
    specialChar: {
        '\b': '\\b',
        '\t': '\\t',
        '\n': '\\n',
        '\f': '\\f',
        '\r': '\\r',
        '\\': '\\\\'
    }
});
Object.extend(String.prototype, (function() {
    var NATIVE_JSON_PARSE_SUPPORT = window.JSON && typeof JSON.parse === 'function' && JSON.parse('{"test": true}').test;

    function prepareReplacement(replacement) {
        if (Object.isFunction(replacement)) return replacement;
        var template = new Template(replacement);
        return function(match) {
            return template.evaluate(match)
        };
    }

    function gsub(pattern, replacement) {
        var result = '',
            source = this,
            match;
        replacement = prepareReplacement(replacement);
        if (Object.isString(pattern))
            pattern = RegExp.escape(pattern);
        if (!(pattern.length || pattern.source)) {
            replacement = replacement('');
            return replacement + source.split('').join(replacement) + replacement;
        }
        while (source.length > 0) {
            if (match = source.match(pattern)) {
                result += source.slice(0, match.index);
                result += String.interpret(replacement(match));
                source = source.slice(match.index + match[0].length);
            } else {
                result += source, source = '';
            }
        }
        return result;
    }

    function sub(pattern, replacement, count) {
        replacement = prepareReplacement(replacement);
        count = Object.isUndefined(count) ? 1 : count;
        return this.gsub(pattern, function(match) {
            if (--count < 0) return match[0];
            return replacement(match);
        });
    }

    function scan(pattern, iterator) {
        this.gsub(pattern, iterator);
        return String(this);
    }

    function truncate(length, truncation) {
        length = length || 30;
        truncation = Object.isUndefined(truncation) ? '...' : truncation;
        return this.length > length ? this.slice(0, length - truncation.length) + truncation : String(this);
    }

    function strip() {
        return this.replace(/^\s+/, '').replace(/\s+$/, '');
    }

    function stripTags() {
        return this.replace(/<\w+(\s+("[^"]*"|'[^']*'|[^>])+)?>|<\/\w+>/gi, '');
    }

    function stripScripts() {
        return this.replace(new RegExp(Prototype.ScriptFragment, 'img'), '');
    }

    function extractScripts() {
        var matchAll = new RegExp(Prototype.ScriptFragment, 'img'),
            matchOne = new RegExp(Prototype.ScriptFragment, 'im');
        return (this.match(matchAll) || []).map(function(scriptTag) {
            return (scriptTag.match(matchOne) || ['', ''])[1];
        });
    }

    function evalScripts() {
        return this.extractScripts().map(function(script) {
            return eval(script)
        });
    }

    function escapeHTML() {
        return this.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    }

    function unescapeHTML() {
        return this.stripTags().replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&amp;/g, '&');
    }

    function toQueryParams(separator) {
        var match = this.strip().match(/([^?#]*)(#.*)?$/);
        if (!match) return {};
        return match[1].split(separator || '&').inject({}, function(hash, pair) {
            if ((pair = pair.split('='))[0]) {
                var key = decodeURIComponent(pair.shift()),
                    value = pair.length > 1 ? pair.join('=') : pair[0];
                if (value != undefined) value = decodeURIComponent(value);
                if (key in hash) {
                    if (!Object.isArray(hash[key])) hash[key] = [hash[key]];
                    hash[key].push(value);
                } else hash[key] = value;
            }
            return hash;
        });
    }

    function toArray() {
        return this.split('');
    }

    function succ() {
        return this.slice(0, this.length - 1) +
            String.fromCharCode(this.charCodeAt(this.length - 1) + 1);
    }

    function times(count) {
        return count < 1 ? '' : new Array(count + 1).join(this);
    }

    function camelize() {
        return this.replace(/-+(.)?/g, function(match, chr) {
            return chr ? chr.toUpperCase() : '';
        });
    }

    function capitalize() {
        return this.charAt(0).toUpperCase() + this.substring(1).toLowerCase();
    }

    function underscore() {
        return this.replace(/::/g, '/').replace(/([A-Z]+)([A-Z][a-z])/g, '$1_$2').replace(/([a-z\d])([A-Z])/g, '$1_$2').replace(/-/g, '_').toLowerCase();
    }

    function dasherize() {
        return this.replace(/_/g, '-');
    }

    function inspect(useDoubleQuotes) {
        var escapedString = this.replace(/[\x00-\x1f\\]/g, function(character) {
            if (character in String.specialChar) {
                return String.specialChar[character];
            }
            return '\\u00' + character.charCodeAt().toPaddedString(2, 16);
        });
        if (useDoubleQuotes) return '"' + escapedString.replace(/"/g, '\\"') + '"';
        return "'" + escapedString.replace(/'/g, '\\\'') + "'";
    }

    function unfilterJSON(filter) {
        return this.replace(filter || Prototype.JSONFilter, '$1');
    }

    function isJSON() {
        var str = this;
        if (str.blank()) return false;
        str = str.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@');
        str = str.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']');
        str = str.replace(/(?:^|:|,)(?:\s*\[)+/g, '');
        return (/^[\],:{}\s]*$/).test(str);
    }

    function evalJSON(sanitize) {
        var json = this.unfilterJSON(),
            cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
        if (cx.test(json)) {
            json = json.replace(cx, function(a) {
                return '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
            });
        }
        try {
            if (!sanitize || json.isJSON()) return eval('(' + json + ')');
        } catch (e) {}
        throw new SyntaxError('Badly formed JSON string: ' + this.inspect());
    }

    function parseJSON() {
        var json = this.unfilterJSON();
        return JSON.parse(json);
    }

    function include(pattern) {
        return this.indexOf(pattern) > -1;
    }

    function startsWith(pattern) {
        return this.lastIndexOf(pattern, 0) === 0;
    }

    function endsWith(pattern) {
        var d = this.length - pattern.length;
        return d >= 0 && this.indexOf(pattern, d) === d;
    }

    function empty() {
        return this == '';
    }

    function blank() {
        return /^\s*$/.test(this);
    }

    function interpolate(object, pattern) {
        return new Template(this, pattern).evaluate(object);
    }
    return {
        gsub: gsub,
        sub: sub,
        scan: scan,
        truncate: truncate,
        strip: String.prototype.trim || strip,
        stripTags: stripTags,
        stripScripts: stripScripts,
        extractScripts: extractScripts,
        evalScripts: evalScripts,
        escapeHTML: escapeHTML,
        unescapeHTML: unescapeHTML,
        toQueryParams: toQueryParams,
        parseQuery: toQueryParams,
        toArray: toArray,
        succ: succ,
        times: times,
        camelize: camelize,
        capitalize: capitalize,
        underscore: underscore,
        dasherize: dasherize,
        inspect: inspect,
        unfilterJSON: unfilterJSON,
        isJSON: isJSON,
        evalJSON: NATIVE_JSON_PARSE_SUPPORT ? parseJSON : evalJSON,
        include: include,
        startsWith: startsWith,
        endsWith: endsWith,
        empty: empty,
        blank: blank,
        interpolate: interpolate
    };
})());
var Template = Class.create({
    initialize: function(template, pattern) {
        this.template = template.toString();
        this.pattern = pattern || Template.Pattern;
    },
    evaluate: function(object) {
        if (object && Object.isFunction(object.toTemplateReplacements))
            object = object.toTemplateReplacements();
        return this.template.gsub(this.pattern, function(match) {
            if (object == null) return (match[1] + '');
            var before = match[1] || '';
            if (before == '\\') return match[2];
            var ctx = object,
                expr = match[3],
                pattern = /^([^.[]+|\[((?:.*?[^\\])?)\])(\.|\[|$)/;
            match = pattern.exec(expr);
            if (match == null) return before;
            while (match != null) {
                var comp = match[1].startsWith('[') ? match[2].replace(/\\\\]/g, ']') : match[1];
                ctx = ctx[comp];
                if (null == ctx || '' == match[3]) break;
                expr = expr.substring('[' == match[3] ? match[1].length : match[0].length);
                match = pattern.exec(expr);
            }
            return before + String.interpret(ctx);
        });
    }
});
Template.Pattern = /(^|.|\r|\n)(#\{(.*?)\})/;
var $break = {};
var Enumerable = (function() {
    function each(iterator, context) {
        var index = 0;
        try {
            this._each(function(value) {
                iterator.call(context, value, index++);
            });
        } catch (e) {
            if (e != $break) throw e;
        }
        return this;
    }

    function eachSlice(number, iterator, context) {
        var index = -number,
            slices = [],
            array = this.toArray();
        if (number < 1) return array;
        while ((index += number) < array.length)
            slices.push(array.slice(index, index + number));
        return slices.collect(iterator, context);
    }

    function all(iterator, context) {
        iterator = iterator || Prototype.K;
        var result = true;
        this.each(function(value, index) {
            result = result && !!iterator.call(context, value, index);
            if (!result) throw $break;
        });
        return result;
    }

    function any(iterator, context) {
        iterator = iterator || Prototype.K;
        var result = false;
        this.each(function(value, index) {
            if (result = !!iterator.call(context, value, index))
                throw $break;
        });
        return result;
    }

    function collect(iterator, context) {
        iterator = iterator || Prototype.K;
        var results = [];
        this.each(function(value, index) {
            results.push(iterator.call(context, value, index));
        });
        return results;
    }

    function detect(iterator, context) {
        var result;
        this.each(function(value, index) {
            if (iterator.call(context, value, index)) {
                result = value;
                throw $break;
            }
        });
        return result;
    }

    function findAll(iterator, context) {
        var results = [];
        this.each(function(value, index) {
            if (iterator.call(context, value, index))
                results.push(value);
        });
        return results;
    }

    function grep(filter, iterator, context) {
        iterator = iterator || Prototype.K;
        var results = [];
        if (Object.isString(filter))
            filter = new RegExp(RegExp.escape(filter));
        this.each(function(value, index) {
            if (filter.match(value))
                results.push(iterator.call(context, value, index));
        });
        return results;
    }

    function include(object) {
        if (Object.isFunction(this.indexOf))
            if (this.indexOf(object) != -1) return true;
        var found = false;
        this.each(function(value) {
            if (value == object) {
                found = true;
                throw $break;
            }
        });
        return found;
    }

    function inGroupsOf(number, fillWith) {
        fillWith = Object.isUndefined(fillWith) ? null : fillWith;
        return this.eachSlice(number, function(slice) {
            while (slice.length < number) slice.push(fillWith);
            return slice;
        });
    }

    function inject(memo, iterator, context) {
        this.each(function(value, index) {
            memo = iterator.call(context, memo, value, index);
        });
        return memo;
    }

    function invoke(method) {
        var args = $A(arguments).slice(1);
        return this.map(function(value) {
            return value[method].apply(value, args);
        });
    }

    function max(iterator, context) {
        iterator = iterator || Prototype.K;
        var result;
        this.each(function(value, index) {
            value = iterator.call(context, value, index);
            if (result == null || value >= result)
                result = value;
        });
        return result;
    }

    function min(iterator, context) {
        iterator = iterator || Prototype.K;
        var result;
        this.each(function(value, index) {
            value = iterator.call(context, value, index);
            if (result == null || value < result)
                result = value;
        });
        return result;
    }

    function partition(iterator, context) {
        iterator = iterator || Prototype.K;
        var trues = [],
            falses = [];
        this.each(function(value, index) {
            (iterator.call(context, value, index) ? trues : falses).push(value);
        });
        return [trues, falses];
    }

    function pluck(property) {
        var results = [];
        this.each(function(value) {
            results.push(value[property]);
        });
        return results;
    }

    function reject(iterator, context) {
        var results = [];
        this.each(function(value, index) {
            if (!iterator.call(context, value, index))
                results.push(value);
        });
        return results;
    }

    function sortBy(iterator, context) {
        return this.map(function(value, index) {
            return {
                value: value,
                criteria: iterator.call(context, value, index)
            };
        }).sort(function(left, right) {
            var a = left.criteria,
                b = right.criteria;
            return a < b ? -1 : a > b ? 1 : 0;
        }).pluck('value');
    }

    function toArray() {
        return this.map();
    }

    function zip() {
        var iterator = Prototype.K,
            args = $A(arguments);
        if (Object.isFunction(args.last()))
            iterator = args.pop();
        var collections = [this].concat(args).map($A);
        return this.map(function(value, index) {
            return iterator(collections.pluck(index));
        });
    }

    function size() {
        return this.toArray().length;
    }

    function inspect() {
        return '#<Enumerable:' + this.toArray().inspect() + '>';
    }
    return {
        each: each,
        eachSlice: eachSlice,
        all: all,
        every: all,
        any: any,
        some: any,
        collect: collect,
        map: collect,
        detect: detect,
        findAll: findAll,
        select: findAll,
        filter: findAll,
        grep: grep,
        include: include,
        member: include,
        inGroupsOf: inGroupsOf,
        inject: inject,
        invoke: invoke,
        max: max,
        min: min,
        partition: partition,
        pluck: pluck,
        reject: reject,
        sortBy: sortBy,
        toArray: toArray,
        entries: toArray,
        zip: zip,
        size: size,
        inspect: inspect,
        find: detect
    };
})();

function $A(iterable) {
    if (!iterable) return [];
    if ('toArray' in Object(iterable)) return iterable.toArray();
    var length = iterable.length || 0,
        results = new Array(length);
    while (length--) results[length] = iterable[length];
    return results;
}

function $w(string) {
    if (!Object.isString(string)) return [];
    string = string.strip();
    return string ? string.split(/\s+/) : [];
}
Array.from = $A;
(function() {
    var arrayProto = Array.prototype,
        slice = arrayProto.slice,
        _each = arrayProto.forEach;

    function each(iterator, context) {
        for (var i = 0, length = this.length >>> 0; i < length; i++) {
            if (i in this) iterator.call(context, this[i], i, this);
        }
    }
    if (!_each) _each = each;

    function clear() {
        this.length = 0;
        return this;
    }

    function first() {
        return this[0];
    }

    function last() {
        return this[this.length - 1];
    }

    function compact() {
        return this.select(function(value) {
            return value != null;
        });
    }

    function flatten() {
        return this.inject([], function(array, value) {
            if (Object.isArray(value))
                return array.concat(value.flatten());
            array.push(value);
            return array;
        });
    }

    function without() {
        var values = slice.call(arguments, 0);
        return this.select(function(value) {
            return !values.include(value);
        });
    }

    function reverse(inline) {
        return (inline === false ? this.toArray() : this)._reverse();
    }

    function uniq(sorted) {
        return this.inject([], function(array, value, index) {
            if (0 == index || (sorted ? array.last() != value : !array.include(value)))
                array.push(value);
            return array;
        });
    }

    function intersect(array) {
        return this.uniq().findAll(function(item) {
            return array.detect(function(value) {
                return item === value
            });
        });
    }

    function clone() {
        return slice.call(this, 0);
    }

    function size() {
        return this.length;
    }

    function inspect() {
        return '[' + this.map(Object.inspect).join(', ') + ']';
    }

    function indexOf(item, i) {
        i || (i = 0);
        var length = this.length;
        if (i < 0) i = length + i;
        for (; i < length; i++)
            if (this[i] === item) return i;
        return -1;
    }

    function lastIndexOf(item, i) {
        i = isNaN(i) ? this.length : (i < 0 ? this.length + i : i) + 1;
        var n = this.slice(0, i).reverse().indexOf(item);
        return (n < 0) ? n : i - n - 1;
    }

    function concat() {
        var array = slice.call(this, 0),
            item;
        for (var i = 0, length = arguments.length; i < length; i++) {
            item = arguments[i];
            if (Object.isArray(item) && !('callee' in item)) {
                for (var j = 0, arrayLength = item.length; j < arrayLength; j++)
                    array.push(item[j]);
            } else {
                array.push(item);
            }
        }
        return array;
    }
    Object.extend(arrayProto, Enumerable);
    if (!arrayProto._reverse)
        arrayProto._reverse = arrayProto.reverse;
    Object.extend(arrayProto, {
        _each: _each,
        clear: clear,
        first: first,
        last: last,
        compact: compact,
        flatten: flatten,
        without: without,
        reverse: reverse,
        uniq: uniq,
        intersect: intersect,
        clone: clone,
        toArray: clone,
        size: size,
        inspect: inspect
    });
    var CONCAT_ARGUMENTS_BUGGY = (function() {
        return [].concat(arguments)[0][0] !== 1;
    })(1, 2)
    if (CONCAT_ARGUMENTS_BUGGY) arrayProto.concat = concat;
    if (!arrayProto.indexOf) arrayProto.indexOf = indexOf;
    if (!arrayProto.lastIndexOf) arrayProto.lastIndexOf = lastIndexOf;
})();

function $H(object) {
    return new Hash(object);
};
var Hash = Class.create(Enumerable, (function() {
    function initialize(object) {
        this._object = Object.isHash(object) ? object.toObject() : Object.clone(object);
    }

    function _each(iterator) {
        for (var key in this._object) {
            var value = this._object[key],
                pair = [key, value];
            pair.key = key;
            pair.value = value;
            iterator(pair);
        }
    }

    function set(key, value) {
        return this._object[key] = value;
    }

    function get(key) {
        if (this._object[key] !== Object.prototype[key])
            return this._object[key];
    }

    function unset(key) {
        var value = this._object[key];
        delete this._object[key];
        return value;
    }

    function toObject() {
        return Object.clone(this._object);
    }

    function keys() {
        return this.pluck('key');
    }

    function values() {
        return this.pluck('value');
    }

    function index(value) {
        var match = this.detect(function(pair) {
            return pair.value === value;
        });
        return match && match.key;
    }

    function merge(object) {
        return this.clone().update(object);
    }

    function update(object) {
        return new Hash(object).inject(this, function(result, pair) {
            result.set(pair.key, pair.value);
            return result;
        });
    }

    function toQueryPair(key, value) {
        if (Object.isUndefined(value)) return key;
        return key + '=' + encodeURIComponent(String.interpret(value));
    }

    function toQueryString() {
        return this.inject([], function(results, pair) {
            var key = encodeURIComponent(pair.key),
                values = pair.value;
            if (values && typeof values == 'object') {
                if (Object.isArray(values)) {
                    var queryValues = [];
                    for (var i = 0, len = values.length, value; i < len; i++) {
                        value = values[i];
                        queryValues.push(toQueryPair(key, value));
                    }
                    return results.concat(queryValues);
                }
            } else results.push(toQueryPair(key, values));
            return results;
        }).join('&');
    }

    function inspect() {
        return '#<Hash:{' + this.map(function(pair) {
            return pair.map(Object.inspect).join(': ');
        }).join(', ') + '}>';
    }

    function clone() {
        return new Hash(this);
    }
    return {
        initialize: initialize,
        _each: _each,
        set: set,
        get: get,
        unset: unset,
        toObject: toObject,
        toTemplateReplacements: toObject,
        keys: keys,
        values: values,
        index: index,
        merge: merge,
        update: update,
        toQueryString: toQueryString,
        inspect: inspect,
        toJSON: toObject,
        clone: clone
    };
})());
Hash.from = $H;
Object.extend(Number.prototype, (function() {
    function toColorPart() {
        return this.toPaddedString(2, 16);
    }

    function succ() {
        return this + 1;
    }

    function times(iterator, context) {
        $R(0, this, true).each(iterator, context);
        return this;
    }

    function toPaddedString(length, radix) {
        var string = this.toString(radix || 10);
        return '0'.times(length - string.length) + string;
    }

    function abs() {
        return Math.abs(this);
    }

    function round() {
        return Math.round(this);
    }

    function ceil() {
        return Math.ceil(this);
    }

    function floor() {
        return Math.floor(this);
    }
    return {
        toColorPart: toColorPart,
        succ: succ,
        times: times,
        toPaddedString: toPaddedString,
        abs: abs,
        round: round,
        ceil: ceil,
        floor: floor
    };
})());

function $R(start, end, exclusive) {
    return new ObjectRange(start, end, exclusive);
}
var ObjectRange = Class.create(Enumerable, (function() {
    function initialize(start, end, exclusive) {
        this.start = start;
        this.end = end;
        this.exclusive = exclusive;
    }

    function _each(iterator) {
        var value = this.start;
        while (this.include(value)) {
            iterator(value);
            value = value.succ();
        }
    }

    function include(value) {
        if (value < this.start)
            return false;
        if (this.exclusive)
            return value < this.end;
        return value <= this.end;
    }
    return {
        initialize: initialize,
        _each: _each,
        include: include
    };
})());
var Ajax = {
    getTransport: function() {
        return Try.these(function() {
            return new XMLHttpRequest()
        }, function() {
            return new ActiveXObject('Msxml2.XMLHTTP')
        }, function() {
            return new ActiveXObject('Microsoft.XMLHTTP')
        }) || false;
    },
    activeRequestCount: 0
};
Ajax.Responders = {
    responders: [],
    _each: function(iterator) {
        this.responders._each(iterator);
    },
    register: function(responder) {
        if (!this.include(responder))
            this.responders.push(responder);
    },
    unregister: function(responder) {
        this.responders = this.responders.without(responder);
    },
    dispatch: function(callback, request, transport, json) {
        this.each(function(responder) {
            if (Object.isFunction(responder[callback])) {
                try {
                    responder[callback].apply(responder, [request, transport, json]);
                } catch (e) {}
            }
        });
    }
};
Object.extend(Ajax.Responders, Enumerable);
Ajax.Responders.register({
    onCreate: function() {
        Ajax.activeRequestCount++
    },
    onComplete: function() {
        Ajax.activeRequestCount--
    }
});
Ajax.Base = Class.create({
    initialize: function(options) {
        this.options = {
            method: 'post',
            asynchronous: true,
            contentType: 'application/x-www-form-urlencoded',
            encoding: 'UTF-8',
            parameters: '',
            evalJSON: true,
            evalJS: true
        };
        Object.extend(this.options, options || {});
        this.options.method = this.options.method.toLowerCase();
        if (Object.isHash(this.options.parameters))
            this.options.parameters = this.options.parameters.toObject();
    }
});
Ajax.Request = Class.create(Ajax.Base, {
    _complete: false,
    initialize: function($super, url, options) {
        $super(options);
        this.transport = Ajax.getTransport();
        this.request(url);
    },
    request: function(url) {
        this.url = url;
        this.method = this.options.method;
        var params = Object.isString(this.options.parameters) ? this.options.parameters : Object.toQueryString(this.options.parameters);
        if (!['get', 'post'].include(this.method)) {
            params += (params ? '&' : '') + "_method=" + this.method;
            this.method = 'post';
        }
        if (params && this.method === 'get') {
            this.url += (this.url.include('?') ? '&' : '?') + params;
        }
        this.parameters = params.toQueryParams();
        try {
            var response = new Ajax.Response(this);
            if (this.options.onCreate) this.options.onCreate(response);
            Ajax.Responders.dispatch('onCreate', this, response);
            this.transport.open(this.method.toUpperCase(), this.url, this.options.asynchronous);
            if (this.options.asynchronous) this.respondToReadyState.bind(this).defer(1);
            this.transport.onreadystatechange = this.onStateChange.bind(this);
            this.setRequestHeaders();
            this.body = this.method == 'post' ? (this.options.postBody || params) : null;
            this.transport.send(this.body);
            if (!this.options.asynchronous && this.transport.overrideMimeType)
                this.onStateChange();
        } catch (e) {
            this.dispatchException(e);
        }
    },
    onStateChange: function() {
        var readyState = this.transport.readyState;
        if (readyState > 1 && !((readyState == 4) && this._complete))
            this.respondToReadyState(this.transport.readyState);
    },
    setRequestHeaders: function() {
        var headers = {
            'X-Requested-With': 'XMLHttpRequest',
            'X-Prototype-Version': Prototype.Version,
            'Accept': 'text/javascript, text/html, application/xml, text/xml, */*'
        };
        if (this.method == 'post') {
            headers['Content-type'] = this.options.contentType +
                (this.options.encoding ? '; charset=' + this.options.encoding : '');
            if (this.transport.overrideMimeType && (navigator.userAgent.match(/Gecko\/(\d{4})/) || [0, 2005])[1] < 2005)
                headers['Connection'] = 'close';
        }
        if (typeof this.options.requestHeaders == 'object') {
            var extras = this.options.requestHeaders;
            if (Object.isFunction(extras.push))
                for (var i = 0, length = extras.length; i < length; i += 2)
                    headers[extras[i]] = extras[i + 1];
            else
                $H(extras).each(function(pair) {
                    headers[pair.key] = pair.value
                });
        }
        for (var name in headers)
            this.transport.setRequestHeader(name, headers[name]);
    },
    success: function() {
        var status = this.getStatus();
        return !status || (status >= 200 && status < 300) || status == 304;
    },
    getStatus: function() {
        try {
            if (this.transport.status === 1223) return 204;
            return this.transport.status || 0;
        } catch (e) {
            return 0
        }
    },
    respondToReadyState: function(readyState) {
        var state = Ajax.Request.Events[readyState],
            response = new Ajax.Response(this);
        if (state == 'Complete') {
            try {
                this._complete = true;
                (this.options['on' + response.status] || this.options['on' + (this.success() ? 'Success' : 'Failure')] || Prototype.emptyFunction)(response, response.headerJSON);
            } catch (e) {
                this.dispatchException(e);
            }
            var contentType = response.getHeader('Content-type');
            if (this.options.evalJS == 'force' || (this.options.evalJS && this.isSameOrigin() && contentType && contentType.match(/^\s*(text|application)\/(x-)?(java|ecma)script(;.*)?\s*$/i)))
                this.evalResponse();
        }
        try {
            (this.options['on' + state] || Prototype.emptyFunction)(response, response.headerJSON);
            Ajax.Responders.dispatch('on' + state, this, response, response.headerJSON);
        } catch (e) {
            this.dispatchException(e);
        }
        if (state == 'Complete') {
            this.transport.onreadystatechange = Prototype.emptyFunction;
        }
    },
    isSameOrigin: function() {
        var m = this.url.match(/^\s*https?:\/\/[^\/]*/);
        return !m || (m[0] == '#{protocol}//#{domain}#{port}'.interpolate({
            protocol: location.protocol,
            domain: document.domain,
            port: location.port ? ':' + location.port : ''
        }));
    },
    getHeader: function(name) {
        try {
            return this.transport.getResponseHeader(name) || null;
        } catch (e) {
            return null;
        }
    },
    evalResponse: function() {
        try {
            return eval((this.transport.responseText || '').unfilterJSON());
        } catch (e) {
            this.dispatchException(e);
        }
    },
    dispatchException: function(exception) {
        (this.options.onException || Prototype.emptyFunction)(this, exception);
        Ajax.Responders.dispatch('onException', this, exception);
    }
});
Ajax.Request.Events = ['Uninitialized', 'Loading', 'Loaded', 'Interactive', 'Complete'];
Ajax.Response = Class.create({
    initialize: function(request) {
        this.request = request;
        var transport = this.transport = request.transport,
            readyState = this.readyState = transport.readyState;
        if ((readyState > 2 && !Prototype.Browser.IE) || readyState == 4) {
            this.status = this.getStatus();
            this.statusText = this.getStatusText();
            this.responseText = String.interpret(transport.responseText);
            this.headerJSON = this._getHeaderJSON();
        }
        if (readyState == 4) {
            var xml = transport.responseXML;
            this.responseXML = Object.isUndefined(xml) ? null : xml;
            this.responseJSON = this._getResponseJSON();
        }
    },
    status: 0,
    statusText: '',
    getStatus: Ajax.Request.prototype.getStatus,
    getStatusText: function() {
        try {
            return this.transport.statusText || '';
        } catch (e) {
            return ''
        }
    },
    getHeader: Ajax.Request.prototype.getHeader,
    getAllHeaders: function() {
        try {
            return this.getAllResponseHeaders();
        } catch (e) {
            return null
        }
    },
    getResponseHeader: function(name) {
        return this.transport.getResponseHeader(name);
    },
    getAllResponseHeaders: function() {
        return this.transport.getAllResponseHeaders();
    },
    _getHeaderJSON: function() {
        var json = this.getHeader('X-JSON');
        if (!json) return null;
        json = decodeURIComponent(escape(json));
        try {
            return json.evalJSON(this.request.options.sanitizeJSON || !this.request.isSameOrigin());
        } catch (e) {
            this.request.dispatchException(e);
        }
    },
    _getResponseJSON: function() {
        var options = this.request.options;
        if (!options.evalJSON || (options.evalJSON != 'force' && !(this.getHeader('Content-type') || '').include('application/json')) || this.responseText.blank())
            return null;
        try {
            return this.responseText.evalJSON(options.sanitizeJSON || !this.request.isSameOrigin());
        } catch (e) {
            this.request.dispatchException(e);
        }
    }
});
Ajax.Updater = Class.create(Ajax.Request, {
    initialize: function($super, container, url, options) {
        this.container = {
            success: (container.success || container),
            failure: (container.failure || (container.success ? null : container))
        };
        options = Object.clone(options);
        var onComplete = options.onComplete;
        options.onComplete = (function(response, json) {
            this.updateContent(response.responseText);
            if (Object.isFunction(onComplete)) onComplete(response, json);
        }).bind(this);
        $super(url, options);
    },
    updateContent: function(responseText) {
        var receiver = this.container[this.success() ? 'success' : 'failure'],
            options = this.options;
        if (!options.evalScripts) responseText = responseText.stripScripts();
        if (receiver = $(receiver)) {
            if (options.insertion) {
                if (Object.isString(options.insertion)) {
                    var insertion = {};
                    insertion[options.insertion] = responseText;
                    receiver.insert(insertion);
                } else options.insertion(receiver, responseText);
            } else receiver.update(responseText);
        }
    }
});
Ajax.PeriodicalUpdater = Class.create(Ajax.Base, {
    initialize: function($super, container, url, options) {
        $super(options);
        this.onComplete = this.options.onComplete;
        this.frequency = (this.options.frequency || 2);
        this.decay = (this.options.decay || 1);
        this.updater = {};
        this.container = container;
        this.url = url;
        this.start();
    },
    start: function() {
        this.options.onComplete = this.updateComplete.bind(this);
        this.onTimerEvent();
    },
    stop: function() {
        this.updater.options.onComplete = undefined;
        clearTimeout(this.timer);
        (this.onComplete || Prototype.emptyFunction).apply(this, arguments);
    },
    updateComplete: function(response) {
        if (this.options.decay) {
            this.decay = (response.responseText == this.lastText ? this.decay * this.options.decay : 1);
            this.lastText = response.responseText;
        }
        this.timer = this.onTimerEvent.bind(this).delay(this.decay * this.frequency);
    },
    onTimerEvent: function() {
        this.updater = new Ajax.Updater(this.container, this.url, this.options);
    }
});

function $(element) {
    if (arguments.length > 1) {
        for (var i = 0, elements = [], length = arguments.length; i < length; i++)
            elements.push($(arguments[i]));
        return elements;
    }
    if (Object.isString(element))
        element = document.getElementById(element);
    return Element.extend(element);
}
if (Prototype.BrowserFeatures.XPath) {
    document._getElementsByXPath = function(expression, parentElement) {
        var results = [];
        var query = document.evaluate(expression, $(parentElement) || document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
        for (var i = 0, length = query.snapshotLength; i < length; i++)
            results.push(Element.extend(query.snapshotItem(i)));
        return results;
    };
}
if (!Node) var Node = {};
if (!Node.ELEMENT_NODE) {
    Object.extend(Node, {
        ELEMENT_NODE: 1,
        ATTRIBUTE_NODE: 2,
        TEXT_NODE: 3,
        CDATA_SECTION_NODE: 4,
        ENTITY_REFERENCE_NODE: 5,
        ENTITY_NODE: 6,
        PROCESSING_INSTRUCTION_NODE: 7,
        COMMENT_NODE: 8,
        DOCUMENT_NODE: 9,
        DOCUMENT_TYPE_NODE: 10,
        DOCUMENT_FRAGMENT_NODE: 11,
        NOTATION_NODE: 12
    });
}
(function(global) {
    function shouldUseCache(tagName, attributes) {
        if (tagName === 'select') return false;
        if ('type' in attributes) return false;
        return true;
    }
    var HAS_EXTENDED_CREATE_ELEMENT_SYNTAX = (function() {
        try {
            var el = document.createElement('<input name="x">');
            return el.tagName.toLowerCase() === 'input' && el.name === 'x';
        } catch (err) {
            return false;
        }
    })();
    var element = global.Element;
    global.Element = function(tagName, attributes) {
        attributes = attributes || {};
        tagName = tagName.toLowerCase();
        var cache = Element.cache;
        if (HAS_EXTENDED_CREATE_ELEMENT_SYNTAX && attributes.name) {
            tagName = '<' + tagName + ' name="' + attributes.name + '">';
            delete attributes.name;
            return Element.writeAttribute(document.createElement(tagName), attributes);
        }
        if (!cache[tagName]) cache[tagName] = Element.extend(document.createElement(tagName));
        var node = shouldUseCache(tagName, attributes) ? cache[tagName].cloneNode(false) : document.createElement(tagName);
        return Element.writeAttribute(node, attributes);
    };
    Object.extend(global.Element, element || {});
    if (element) global.Element.prototype = element.prototype;
})(this);
Element.idCounter = 1;
Element.cache = {};
Element._purgeElement = function(element) {
    var uid = element._prototypeUID;
    if (uid) {
        Element.stopObserving(element);
        element._prototypeUID = void 0;
        delete Element.Storage[uid];
    }
}
Element.Methods = {
    visible: function(element) {
        return $(element).style.display != 'none';
    },
    toggle: function(element) {
        element = $(element);
        Element[Element.visible(element) ? 'hide' : 'show'](element);
        return element;
    },
    hide: function(element) {
        element = $(element);
        element.style.display = 'none';
        return element;
    },
    show: function(element) {
        element = $(element);
        element.style.display = '';
        return element;
    },
    remove: function(element) {
        element = $(element);
        element.parentNode.removeChild(element);
        return element;
    },
    update: (function() {
        var SELECT_ELEMENT_INNERHTML_BUGGY = (function() {
            var el = document.createElement("select"),
                isBuggy = true;
            el.innerHTML = "<option value=\"test\">test</option>";
            if (el.options && el.options[0]) {
                isBuggy = el.options[0].nodeName.toUpperCase() !== "OPTION";
            }
            el = null;
            return isBuggy;
        })();
        var TABLE_ELEMENT_INNERHTML_BUGGY = (function() {
            try {
                var el = document.createElement("table");
                if (el && el.tBodies) {
                    el.innerHTML = "<tbody><tr><td>test</td></tr></tbody>";
                    var isBuggy = typeof el.tBodies[0] == "undefined";
                    el = null;
                    return isBuggy;
                }
            } catch (e) {
                return true;
            }
        })();
        var LINK_ELEMENT_INNERHTML_BUGGY = (function() {
            try {
                var el = document.createElement('div');
                el.innerHTML = "<link>";
                var isBuggy = (el.childNodes.length === 0);
                el = null;
                return isBuggy;
            } catch (e) {
                return true;
            }
        })();
        var ANY_INNERHTML_BUGGY = SELECT_ELEMENT_INNERHTML_BUGGY || TABLE_ELEMENT_INNERHTML_BUGGY || LINK_ELEMENT_INNERHTML_BUGGY;
        var SCRIPT_ELEMENT_REJECTS_TEXTNODE_APPENDING = (function() {
            var s = document.createElement("script"),
                isBuggy = false;
            try {
                s.appendChild(document.createTextNode(""));
                isBuggy = !s.firstChild || s.firstChild && s.firstChild.nodeType !== 3;
            } catch (e) {
                isBuggy = true;
            }
            s = null;
            return isBuggy;
        })();

        function update(element, content) {
            element = $(element);
            var purgeElement = Element._purgeElement;
            var descendants = element.getElementsByTagName('*'),
                i = descendants.length;
            while (i--) purgeElement(descendants[i]);
            if (content && content.toElement)
                content = content.toElement();
            if (Object.isElement(content))
                return element.update().insert(content);
            content = Object.toHTML(content);
            var tagName = element.tagName.toUpperCase();
            if (tagName === 'SCRIPT' && SCRIPT_ELEMENT_REJECTS_TEXTNODE_APPENDING) {
                element.text = content;
                return element;
            }
            if (ANY_INNERHTML_BUGGY) {
                if (tagName in Element._insertionTranslations.tags) {
                    while (element.firstChild) {
                        element.removeChild(element.firstChild);
                    }
                    Element._getContentFromAnonymousElement(tagName, content.stripScripts()).each(function(node) {
                        element.appendChild(node)
                    });
                } else if (LINK_ELEMENT_INNERHTML_BUGGY && Object.isString(content) && content.indexOf('<link') > -1) {
                    while (element.firstChild) {
                        element.removeChild(element.firstChild);
                    }
                    var nodes = Element._getContentFromAnonymousElement(tagName, content.stripScripts(), true);
                    nodes.each(function(node) {
                        element.appendChild(node)
                    });
                } else {
                    element.innerHTML = content.stripScripts();
                }
            } else {
                element.innerHTML = content.stripScripts();
            }
            content.evalScripts.bind(content).defer();
            return element;
        }
        return update;
    })(),
    replace: function(element, content) {
        element = $(element);
        if (content && content.toElement) content = content.toElement();
        else if (!Object.isElement(content)) {
            content = Object.toHTML(content);
            var range = element.ownerDocument.createRange();
            range.selectNode(element);
            content.evalScripts.bind(content).defer();
            content = range.createContextualFragment(content.stripScripts());
        }
        element.parentNode.replaceChild(content, element);
        return element;
    },
    insert: function(element, insertions) {
        element = $(element);
        if (Object.isString(insertions) || Object.isNumber(insertions) || Object.isElement(insertions) || (insertions && (insertions.toElement || insertions.toHTML)))
            insertions = {
                bottom: insertions
            };
        var content, insert, tagName, childNodes;
        for (var position in insertions) {
            content = insertions[position];
            position = position.toLowerCase();
            insert = Element._insertionTranslations[position];
            if (content && content.toElement) content = content.toElement();
            if (Object.isElement(content)) {
                insert(element, content);
                continue;
            }
            content = Object.toHTML(content);
            tagName = ((position == 'before' || position == 'after') ? element.parentNode : element).tagName.toUpperCase();
            childNodes = Element._getContentFromAnonymousElement(tagName, content.stripScripts());
            if (position == 'top' || position == 'after') childNodes.reverse();
            childNodes.each(insert.curry(element));
            content.evalScripts.bind(content).defer();
        }
        return element;
    },
    wrap: function(element, wrapper, attributes) {
        element = $(element);
        if (Object.isElement(wrapper))
            $(wrapper).writeAttribute(attributes || {});
        else if (Object.isString(wrapper)) wrapper = new Element(wrapper, attributes);
        else wrapper = new Element('div', wrapper);
        if (element.parentNode)
            element.parentNode.replaceChild(wrapper, element);
        wrapper.appendChild(element);
        return wrapper;
    },
    inspect: function(element) {
        element = $(element);
        var result = '<' + element.tagName.toLowerCase();
        $H({
            'id': 'id',
            'className': 'class'
        }).each(function(pair) {
            var property = pair.first(),
                attribute = pair.last(),
                value = (element[property] || '').toString();
            if (value) result += ' ' + attribute + '=' + value.inspect(true);
        });
        return result + '>';
    },
    recursivelyCollect: function(element, property, maximumLength) {
        element = $(element);
        maximumLength = maximumLength || -1;
        var elements = [];
        while (element = element[property]) {
            if (element.nodeType == 1)
                elements.push(Element.extend(element));
            if (elements.length == maximumLength)
                break;
        }
        return elements;
    },
    ancestors: function(element) {
        return Element.recursivelyCollect(element, 'parentNode');
    },
    descendants: function(element) {
        return Element.select(element, "*");
    },
    firstDescendant: function(element) {
        element = $(element).firstChild;
        while (element && element.nodeType != 1) element = element.nextSibling;
        return $(element);
    },
    immediateDescendants: function(element) {
        var results = [],
            child = $(element).firstChild;
        while (child) {
            if (child.nodeType === 1) {
                results.push(Element.extend(child));
            }
            child = child.nextSibling;
        }
        return results;
    },
    previousSiblings: function(element, maximumLength) {
        return Element.recursivelyCollect(element, 'previousSibling');
    },
    nextSiblings: function(element) {
        return Element.recursivelyCollect(element, 'nextSibling');
    },
    siblings: function(element) {
        element = $(element);
        return Element.previousSiblings(element).reverse().concat(Element.nextSiblings(element));
    },
    match: function(element, selector) {
        element = $(element);
        if (Object.isString(selector))
            return Prototype.Selector.match(element, selector);
        return selector.match(element);
    },
    up: function(element, expression, index) {
        element = $(element);
        if (arguments.length == 1) return $(element.parentNode);
        var ancestors = Element.ancestors(element);
        return Object.isNumber(expression) ? ancestors[expression] : Prototype.Selector.find(ancestors, expression, index);
    },
    down: function(element, expression, index) {
        element = $(element);
        if (arguments.length == 1) return Element.firstDescendant(element);
        return Object.isNumber(expression) ? Element.descendants(element)[expression] : Element.select(element, expression)[index || 0];
    },
    previous: function(element, expression, index) {
        element = $(element);
        if (Object.isNumber(expression)) index = expression, expression = false;
        if (!Object.isNumber(index)) index = 0;
        if (expression) {
            return Prototype.Selector.find(element.previousSiblings(), expression, index);
        } else {
            return element.recursivelyCollect("previousSibling", index + 1)[index];
        }
    },
    next: function(element, expression, index) {
        element = $(element);
        if (Object.isNumber(expression)) index = expression, expression = false;
        if (!Object.isNumber(index)) index = 0;
        if (expression) {
            return Prototype.Selector.find(element.nextSiblings(), expression, index);
        } else {
            var maximumLength = Object.isNumber(index) ? index + 1 : 1;
            return element.recursivelyCollect("nextSibling", index + 1)[index];
        }
    },
    select: function(element) {
        element = $(element);
        var expressions = Array.prototype.slice.call(arguments, 1).join(', ');
        return Prototype.Selector.select(expressions, element);
    },
    adjacent: function(element) {
        element = $(element);
        var expressions = Array.prototype.slice.call(arguments, 1).join(', ');
        return Prototype.Selector.select(expressions, element.parentNode).without(element);
    },
    identify: function(element) {
        element = $(element);
        var id = Element.readAttribute(element, 'id');
        if (id) return id;
        do {
            id = 'anonymous_element_' + Element.idCounter++
        } while ($(id));
        Element.writeAttribute(element, 'id', id);
        return id;
    },
    readAttribute: function(element, name) {
        element = $(element);
        if (Prototype.Browser.IE) {
            var t = Element._attributeTranslations.read;
            if (t.values[name]) return t.values[name](element, name);
            if (t.names[name]) name = t.names[name];
            if (name.include(':')) {
                return (!element.attributes || !element.attributes[name]) ? null : element.attributes[name].value;
            }
        }
        return element.getAttribute(name);
    },
    writeAttribute: function(element, name, value) {
        element = $(element);
        var attributes = {},
            t = Element._attributeTranslations.write;
        if (typeof name == 'object') attributes = name;
        else attributes[name] = Object.isUndefined(value) ? true : value;
        for (var attr in attributes) {
            name = t.names[attr] || attr;
            value = attributes[attr];
            if (t.values[attr]) name = t.values[attr](element, value);
            if (value === false || value === null)
                element.removeAttribute(name);
            else if (value === true)
                element.setAttribute(name, name);
            else element.setAttribute(name, value);
        }
        return element;
    },
    getHeight: function(element) {
        return Element.getDimensions(element).height;
    },
    getWidth: function(element) {
        return Element.getDimensions(element).width;
    },
    classNames: function(element) {
        return new Element.ClassNames(element);
    },
    hasClassName: function(element, className) {
        if (!(element = $(element))) return;
        var elementClassName = element.className;
        return (elementClassName.length > 0 && (elementClassName == className || new RegExp("(^|\\s)" + className + "(\\s|$)").test(elementClassName)));
    },
    addClassName: function(element, className) {
        if (!(element = $(element))) return;
        if (!Element.hasClassName(element, className))
            element.className += (element.className ? ' ' : '') + className;
        return element;
    },
    removeClassName: function(element, className) {
        if (!(element = $(element))) return;
        element.className = element.className.replace(new RegExp("(^|\\s+)" + className + "(\\s+|$)"), ' ').strip();
        return element;
    },
    toggleClassName: function(element, className) {
        if (!(element = $(element))) return;
        return Element[Element.hasClassName(element, className) ? 'removeClassName' : 'addClassName'](element, className);
    },
    cleanWhitespace: function(element) {
        element = $(element);
        var node = element.firstChild;
        while (node) {
            var nextNode = node.nextSibling;
            if (node.nodeType == 3 && !/\S/.test(node.nodeValue))
                element.removeChild(node);
            node = nextNode;
        }
        return element;
    },
    empty: function(element) {
        return $(element).innerHTML.blank();
    },
    descendantOf: function(element, ancestor) {
        element = $(element), ancestor = $(ancestor);
        if (element.compareDocumentPosition)
            return (element.compareDocumentPosition(ancestor) & 8) === 8;
        if (ancestor.contains)
            return ancestor.contains(element) && ancestor !== element;
        while (element = element.parentNode)
            if (element == ancestor) return true;
        return false;
    },
    scrollTo: function(element) {
        element = $(element);
        var pos = Element.cumulativeOffset(element);
        window.scrollTo(pos[0], pos[1]);
        return element;
    },
    getStyle: function(element, style) {
        element = $(element);
        style = style == 'float' ? 'cssFloat' : style.camelize();
        var value = element.style[style];
        if (!value || value == 'auto') {
            var css = document.defaultView.getComputedStyle(element, null);
            value = css ? css[style] : null;
        }
        if (style == 'opacity') return value ? parseFloat(value) : 1.0;
        return value == 'auto' ? null : value;
    },
    getOpacity: function(element) {
        return $(element).getStyle('opacity');
    },
    setStyle: function(element, styles) {
        element = $(element);
        var elementStyle = element.style,
            match;
        if (Object.isString(styles)) {
            element.style.cssText += ';' + styles;
            return styles.include('opacity') ? element.setOpacity(styles.match(/opacity:\s*(\d?\.?\d*)/)[1]) : element;
        }
        for (var property in styles)
            if (property == 'opacity') element.setOpacity(styles[property]);
            else
                elementStyle[(property == 'float' || property == 'cssFloat') ? (Object.isUndefined(elementStyle.styleFloat) ? 'cssFloat' : 'styleFloat') : property] = styles[property];
        return element;
    },
    setOpacity: function(element, value) {
        element = $(element);
        element.style.opacity = (value == 1 || value === '') ? '' : (value < 0.00001) ? 0 : value;
        return element;
    },
    makePositioned: function(element) {
        element = $(element);
        var pos = Element.getStyle(element, 'position');
        if (pos == 'static' || !pos) {
            element._madePositioned = true;
            element.style.position = 'relative';
            if (Prototype.Browser.Opera) {
                element.style.top = 0;
                element.style.left = 0;
            }
        }
        return element;
    },
    undoPositioned: function(element) {
        element = $(element);
        if (element._madePositioned) {
            element._madePositioned = undefined;
            element.style.position = element.style.top = element.style.left = element.style.bottom = element.style.right = '';
        }
        return element;
    },
    makeClipping: function(element) {
        element = $(element);
        if (element._overflow) return element;
        element._overflow = Element.getStyle(element, 'overflow') || 'auto';
        if (element._overflow !== 'hidden')
            element.style.overflow = 'hidden';
        return element;
    },
    undoClipping: function(element) {
        element = $(element);
        if (!element._overflow) return element;
        element.style.overflow = element._overflow == 'auto' ? '' : element._overflow;
        element._overflow = null;
        return element;
    },
    clonePosition: function(element, source) {
        var options = Object.extend({
            setLeft: true,
            setTop: true,
            setWidth: true,
            setHeight: true,
            offsetTop: 0,
            offsetLeft: 0
        }, arguments[2] || {});
        source = $(source);
        var p = Element.viewportOffset(source),
            delta = [0, 0],
            parent = null;
        element = $(element);
        if (Element.getStyle(element, 'position') == 'absolute') {
            parent = Element.getOffsetParent(element);
            delta = Element.viewportOffset(parent);
        }
        if (parent == document.body) {
            delta[0] -= document.body.offsetLeft;
            delta[1] -= document.body.offsetTop;
        }
        if (options.setLeft) element.style.left = (p[0] - delta[0] + options.offsetLeft) + 'px';
        if (options.setTop) element.style.top = (p[1] - delta[1] + options.offsetTop) + 'px';
        if (options.setWidth) element.style.width = source.offsetWidth + 'px';
        if (options.setHeight) element.style.height = source.offsetHeight + 'px';
        return element;
    }
};
Object.extend(Element.Methods, {
    getElementsBySelector: Element.Methods.select,
    childElements: Element.Methods.immediateDescendants
});
Element._attributeTranslations = {
    write: {
        names: {
            className: 'class',
            htmlFor: 'for'
        },
        values: {}
    }
};
if (Prototype.Browser.Opera) {
    Element.Methods.getStyle = Element.Methods.getStyle.wrap(function(proceed, element, style) {
        switch (style) {
            case 'height':
            case 'width':
                if (!Element.visible(element)) return null;
                var dim = parseInt(proceed(element, style), 10);
                if (dim !== element['offset' + style.capitalize()])
                    return dim + 'px';
                var properties;
                if (style === 'height') {
                    properties = ['border-top-width', 'padding-top', 'padding-bottom', 'border-bottom-width'];
                } else {
                    properties = ['border-left-width', 'padding-left', 'padding-right', 'border-right-width'];
                }
                return properties.inject(dim, function(memo, property) {
                    var val = proceed(element, property);
                    return val === null ? memo : memo - parseInt(val, 10);
                }) + 'px';
            default:
                return proceed(element, style);
        }
    });
    Element.Methods.readAttribute = Element.Methods.readAttribute.wrap(function(proceed, element, attribute) {
        if (attribute === 'title') return element.title;
        return proceed(element, attribute);
    });
} else if (Prototype.Browser.IE) {
    Element.Methods.getStyle = function(element, style) {
        element = $(element);
        style = (style == 'float' || style == 'cssFloat') ? 'styleFloat' : style.camelize();
        var value = element.style[style];
        if (!value && element.currentStyle) value = element.currentStyle[style];
        if (style == 'opacity') {
            if (value = (element.getStyle('filter') || '').match(/alpha\(opacity=(.*)\)/))
                if (value[1]) return parseFloat(value[1]) / 100;
            return 1.0;
        }
        if (value == 'auto') {
            if ((style == 'width' || style == 'height') && (element.getStyle('display') != 'none'))
                return element['offset' + style.capitalize()] + 'px';
            return null;
        }
        return value;
    };
    Element.Methods.setOpacity = function(element, value) {
        function stripAlpha(filter) {
            return filter.replace(/alpha\([^\)]*\)/gi, '');
        }
        element = $(element);
        var currentStyle = element.currentStyle;
        if ((currentStyle && !currentStyle.hasLayout) || (!currentStyle && element.style.zoom == 'normal'))
            element.style.zoom = 1;
        var filter = element.getStyle('filter'),
            style = element.style;
        if (value == 1 || value === '') {
            (filter = stripAlpha(filter)) ? style.filter = filter: style.removeAttribute('filter');
            return element;
        } else if (value < 0.00001) value = 0;
        style.filter = stripAlpha(filter) +
            'alpha(opacity=' + (value * 100) + ')';
        return element;
    };
    Element._attributeTranslations = (function() {
        var classProp = 'className',
            forProp = 'for',
            el = document.createElement('div');
        el.setAttribute(classProp, 'x');
        if (el.className !== 'x') {
            el.setAttribute('class', 'x');
            if (el.className === 'x') {
                classProp = 'class';
            }
        }
        el = null;
        el = document.createElement('label');
        el.setAttribute(forProp, 'x');
        if (el.htmlFor !== 'x') {
            el.setAttribute('htmlFor', 'x');
            if (el.htmlFor === 'x') {
                forProp = 'htmlFor';
            }
        }
        el = null;
        return {
            read: {
                names: {
                    'class': classProp,
                    'className': classProp,
                    'for': forProp,
                    'htmlFor': forProp
                },
                values: {
                    _getAttr: function(element, attribute) {
                        return element.getAttribute(attribute);
                    },
                    _getAttr2: function(element, attribute) {
                        return element.getAttribute(attribute, 2);
                    },
                    _getAttrNode: function(element, attribute) {
                        var node = element.getAttributeNode(attribute);
                        return node ? node.value : "";
                    },
                    _getEv: (function() {
                        var el = document.createElement('div'),
                            f;
                        el.onclick = Prototype.emptyFunction;
                        var value = el.getAttribute('onclick');
                        if (String(value).indexOf('{') > -1) {
                            f = function(element, attribute) {
                                attribute = element.getAttribute(attribute);
                                if (!attribute) return null;
                                attribute = attribute.toString();
                                attribute = attribute.split('{')[1];
                                attribute = attribute.split('}')[0];
                                return attribute.strip();
                            };
                        } else if (value === '') {
                            f = function(element, attribute) {
                                attribute = element.getAttribute(attribute);
                                if (!attribute) return null;
                                return attribute.strip();
                            };
                        }
                        el = null;
                        return f;
                    })(),
                    _flag: function(element, attribute) {
                        return $(element).hasAttribute(attribute) ? attribute : null;
                    },
                    style: function(element) {
                        return element.style.cssText.toLowerCase();
                    },
                    title: function(element) {
                        return element.title;
                    }
                }
            }
        }
    })();
    Element._attributeTranslations.write = {
        names: Object.extend({
            cellpadding: 'cellPadding',
            cellspacing: 'cellSpacing'
        }, Element._attributeTranslations.read.names),
        values: {
            checked: function(element, value) {
                element.checked = !!value;
            },
            style: function(element, value) {
                element.style.cssText = value ? value : '';
            }
        }
    };
    Element._attributeTranslations.has = {};
    $w('colSpan rowSpan vAlign dateTime accessKey tabIndex ' +
        'encType maxLength readOnly longDesc frameBorder').each(function(attr) {
        Element._attributeTranslations.write.names[attr.toLowerCase()] = attr;
        Element._attributeTranslations.has[attr.toLowerCase()] = attr;
    });
    (function(v) {
        Object.extend(v, {
            href: v._getAttr2,
            src: v._getAttr2,
            type: v._getAttr,
            action: v._getAttrNode,
            disabled: v._flag,
            checked: v._flag,
            readonly: v._flag,
            multiple: v._flag,
            onload: v._getEv,
            onunload: v._getEv,
            onclick: v._getEv,
            ondblclick: v._getEv,
            onmousedown: v._getEv,
            onmouseup: v._getEv,
            onmouseover: v._getEv,
            onmousemove: v._getEv,
            onmouseout: v._getEv,
            onfocus: v._getEv,
            onblur: v._getEv,
            onkeypress: v._getEv,
            onkeydown: v._getEv,
            onkeyup: v._getEv,
            onsubmit: v._getEv,
            onreset: v._getEv,
            onselect: v._getEv,
            onchange: v._getEv
        });
    })(Element._attributeTranslations.read.values);
    if (Prototype.BrowserFeatures.ElementExtensions) {
        (function() {
            function _descendants(element) {
                var nodes = element.getElementsByTagName('*'),
                    results = [];
                for (var i = 0, node; node = nodes[i]; i++)
                    if (node.tagName !== "!")
                        results.push(node);
                return results;
            }
            Element.Methods.down = function(element, expression, index) {
                element = $(element);
                if (arguments.length == 1) return element.firstDescendant();
                return Object.isNumber(expression) ? _descendants(element)[expression] : Element.select(element, expression)[index || 0];
            }
        })();
    }
} else if (Prototype.Browser.Gecko && /rv:1\.8\.0/.test(navigator.userAgent)) {
    Element.Methods.setOpacity = function(element, value) {
        element = $(element);
        element.style.opacity = (value == 1) ? 0.999999 : (value === '') ? '' : (value < 0.00001) ? 0 : value;
        return element;
    };
} else if (Prototype.Browser.WebKit) {
    Element.Methods.setOpacity = function(element, value) {
        element = $(element);
        element.style.opacity = (value == 1 || value === '') ? '' : (value < 0.00001) ? 0 : value;
        if (value == 1)
            if (element.tagName.toUpperCase() == 'IMG' && element.width) {
                element.width++;
                element.width--;
            } else try {
                var n = document.createTextNode(' ');
                element.appendChild(n);
                element.removeChild(n);
            } catch (e) {}
        return element;
    };
}
if ('outerHTML' in document.documentElement) {
    Element.Methods.replace = function(element, content) {
        element = $(element);
        if (content && content.toElement) content = content.toElement();
        if (Object.isElement(content)) {
            element.parentNode.replaceChild(content, element);
            return element;
        }
        content = Object.toHTML(content);
        var parent = element.parentNode,
            tagName = parent.tagName.toUpperCase();
        if (Element._insertionTranslations.tags[tagName]) {
            var nextSibling = element.next(),
                fragments = Element._getContentFromAnonymousElement(tagName, content.stripScripts());
            parent.removeChild(element);
            if (nextSibling)
                fragments.each(function(node) {
                    parent.insertBefore(node, nextSibling)
                });
            else
                fragments.each(function(node) {
                    parent.appendChild(node)
                });
        } else element.outerHTML = content.stripScripts();
        content.evalScripts.bind(content).defer();
        return element;
    };
}
Element._returnOffset = function(l, t) {
    var result = [l, t];
    result.left = l;
    result.top = t;
    return result;
};
Element._getContentFromAnonymousElement = function(tagName, html, force) {
    var div = new Element('div'),
        t = Element._insertionTranslations.tags[tagName];
    var workaround = false;
    if (t) workaround = true;
    else if (force) {
        workaround = true;
        t = ['', '', 0];
    }
    if (workaround) {
        div.innerHTML = '&nbsp;' + t[0] + html + t[1];
        div.removeChild(div.firstChild);
        for (var i = t[2]; i--;) {
            div = div.firstChild;
        }
    } else {
        div.innerHTML = html;
    }
    return $A(div.childNodes);
};
Element._insertionTranslations = {
    before: function(element, node) {
        element.parentNode.insertBefore(node, element);
    },
    top: function(element, node) {
        element.insertBefore(node, element.firstChild);
    },
    bottom: function(element, node) {
        element.appendChild(node);
    },
    after: function(element, node) {
        element.parentNode.insertBefore(node, element.nextSibling);
    },
    tags: {
        TABLE: ['<table>', '</table>', 1],
        TBODY: ['<table><tbody>', '</tbody></table>', 2],
        TR: ['<table><tbody><tr>', '</tr></tbody></table>', 3],
        TD: ['<table><tbody><tr><td>', '</td></tr></tbody></table>', 4],
        SELECT: ['<select>', '</select>', 1]
    }
};
(function() {
    var tags = Element._insertionTranslations.tags;
    Object.extend(tags, {
        THEAD: tags.TBODY,
        TFOOT: tags.TBODY,
        TH: tags.TD
    });
})();
Element.Methods.Simulated = {
    hasAttribute: function(element, attribute) {
        attribute = Element._attributeTranslations.has[attribute] || attribute;
        var node = $(element).getAttributeNode(attribute);
        return !!(node && node.specified);
    }
};
Element.Methods.ByTag = {};
Object.extend(Element, Element.Methods);
(function(div) {
    if (!Prototype.BrowserFeatures.ElementExtensions && div['__proto__']) {
        window.HTMLElement = {};
        window.HTMLElement.prototype = div['__proto__'];
        Prototype.BrowserFeatures.ElementExtensions = true;
    }
    div = null;
})(document.createElement('div'));
Element.extend = (function() {
    function checkDeficiency(tagName) {
        if (typeof window.Element != 'undefined') {
            var proto = window.Element.prototype;
            if (proto) {
                var id = '_' + (Math.random() + '').slice(2),
                    el = document.createElement(tagName);
                proto[id] = 'x';
                var isBuggy = (el[id] !== 'x');
                delete proto[id];
                el = null;
                return isBuggy;
            }
        }
        return false;
    }

    function extendElementWith(element, methods) {
        for (var property in methods) {
            var value = methods[property];
            if (Object.isFunction(value) && !(property in element))
                element[property] = value.methodize();
        }
    }
    var HTMLOBJECTELEMENT_PROTOTYPE_BUGGY = checkDeficiency('object');
    if (Prototype.BrowserFeatures.SpecificElementExtensions) {
        if (HTMLOBJECTELEMENT_PROTOTYPE_BUGGY) {
            return function(element) {
                if (element && typeof element._extendedByPrototype == 'undefined') {
                    var t = element.tagName;
                    if (t && (/^(?:object|applet|embed)$/i.test(t))) {
                        extendElementWith(element, Element.Methods);
                        extendElementWith(element, Element.Methods.Simulated);
                        extendElementWith(element, Element.Methods.ByTag[t.toUpperCase()]);
                    }
                }
                return element;
            }
        }
        return Prototype.K;
    }
    var Methods = {},
        ByTag = Element.Methods.ByTag;
    var extend = Object.extend(function(element) {
        if (!element || typeof element._extendedByPrototype != 'undefined' || element.nodeType != 1 || element == window) return element;
        var methods = Object.clone(Methods),
            tagName = element.tagName.toUpperCase();
        if (ByTag[tagName]) Object.extend(methods, ByTag[tagName]);
        extendElementWith(element, methods);
        element._extendedByPrototype = Prototype.emptyFunction;
        return element;
    }, {
        refresh: function() {
            if (!Prototype.BrowserFeatures.ElementExtensions) {
                Object.extend(Methods, Element.Methods);
                Object.extend(Methods, Element.Methods.Simulated);
            }
        }
    });
    extend.refresh();
    return extend;
})();
if (document.documentElement.hasAttribute) {
    Element.hasAttribute = function(element, attribute) {
        return element.hasAttribute(attribute);
    };
} else {
    Element.hasAttribute = Element.Methods.Simulated.hasAttribute;
}
Element.addMethods = function(methods) {
    var F = Prototype.BrowserFeatures,
        T = Element.Methods.ByTag;
    if (!methods) {
        Object.extend(Form, Form.Methods);
        Object.extend(Form.Element, Form.Element.Methods);
        Object.extend(Element.Methods.ByTag, {
            "FORM": Object.clone(Form.Methods),
            "INPUT": Object.clone(Form.Element.Methods),
            "SELECT": Object.clone(Form.Element.Methods),
            "TEXTAREA": Object.clone(Form.Element.Methods),
            "BUTTON": Object.clone(Form.Element.Methods)
        });
    }
    if (arguments.length == 2) {
        var tagName = methods;
        methods = arguments[1];
    }
    if (!tagName) Object.extend(Element.Methods, methods || {});
    else {
        if (Object.isArray(tagName)) tagName.each(extend);
        else extend(tagName);
    }

    function extend(tagName) {
        tagName = tagName.toUpperCase();
        if (!Element.Methods.ByTag[tagName])
            Element.Methods.ByTag[tagName] = {};
        Object.extend(Element.Methods.ByTag[tagName], methods);
    }

    function copy(methods, destination, onlyIfAbsent) {
        onlyIfAbsent = onlyIfAbsent || false;
        for (var property in methods) {
            var value = methods[property];
            if (!Object.isFunction(value)) continue;
            if (!onlyIfAbsent || !(property in destination))
                destination[property] = value.methodize();
        }
    }

    function findDOMClass(tagName) {
        var klass;
        var trans = {
            "OPTGROUP": "OptGroup",
            "TEXTAREA": "TextArea",
            "P": "Paragraph",
            "FIELDSET": "FieldSet",
            "UL": "UList",
            "OL": "OList",
            "DL": "DList",
            "DIR": "Directory",
            "H1": "Heading",
            "H2": "Heading",
            "H3": "Heading",
            "H4": "Heading",
            "H5": "Heading",
            "H6": "Heading",
            "Q": "Quote",
            "INS": "Mod",
            "DEL": "Mod",
            "A": "Anchor",
            "IMG": "Image",
            "CAPTION": "TableCaption",
            "COL": "TableCol",
            "COLGROUP": "TableCol",
            "THEAD": "TableSection",
            "TFOOT": "TableSection",
            "TBODY": "TableSection",
            "TR": "TableRow",
            "TH": "TableCell",
            "TD": "TableCell",
            "FRAMESET": "FrameSet",
            "IFRAME": "IFrame"
        };
        if (trans[tagName]) klass = 'HTML' + trans[tagName] + 'Element';
        if (window[klass]) return window[klass];
        klass = 'HTML' + tagName + 'Element';
        if (window[klass]) return window[klass];
        klass = 'HTML' + tagName.capitalize() + 'Element';
        if (window[klass]) return window[klass];
        var element = document.createElement(tagName),
            proto = element['__proto__'] || element.constructor.prototype;
        element = null;
        return proto;
    }
    var elementPrototype = window.HTMLElement ? HTMLElement.prototype : Element.prototype;
    if (F.ElementExtensions) {
        copy(Element.Methods, elementPrototype);
        copy(Element.Methods.Simulated, elementPrototype, true);
    }
    if (F.SpecificElementExtensions) {
        for (var tag in Element.Methods.ByTag) {
            var klass = findDOMClass(tag);
            if (Object.isUndefined(klass)) continue;
            copy(T[tag], klass.prototype);
        }
    }
    Object.extend(Element, Element.Methods);
    delete Element.ByTag;
    if (Element.extend.refresh) Element.extend.refresh();
    Element.cache = {};
};
document.viewport = {
    getDimensions: function() {
        return {
            width: this.getWidth(),
            height: this.getHeight()
        };
    },
    getScrollOffsets: function() {
        return Element._returnOffset(window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft, window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop);
    }
};
(function(viewport) {
    var B = Prototype.Browser,
        doc = document,
        element, property = {};

    function getRootElement() {
        if (B.WebKit && !doc.evaluate)
            return document;
        if (B.Opera && window.parseFloat(window.opera.version()) < 9.5)
            return document.body;
        return document.documentElement;
    }

    function define(D) {
        if (!element) element = getRootElement();
        property[D] = 'client' + D;
        viewport['get' + D] = function() {
            return element[property[D]]
        };
        return viewport['get' + D]();
    }
    viewport.getWidth = define.curry('Width');
    viewport.getHeight = define.curry('Height');
})(document.viewport);
Element.Storage = {
    UID: 1
};
Element.addMethods({
    getStorage: function(element) {
        if (!(element = $(element))) return;
        var uid;
        if (element === window) {
            uid = 0;
        } else {
            if (typeof element._prototypeUID === "undefined")
                element._prototypeUID = Element.Storage.UID++;
            uid = element._prototypeUID;
        }
        if (!Element.Storage[uid])
            Element.Storage[uid] = $H();
        return Element.Storage[uid];
    },
    store: function(element, key, value) {
        if (!(element = $(element))) return;
        if (arguments.length === 2) {
            Element.getStorage(element).update(key);
        } else {
            Element.getStorage(element).set(key, value);
        }
        return element;
    },
    retrieve: function(element, key, defaultValue) {
        if (!(element = $(element))) return;
        var hash = Element.getStorage(element),
            value = hash.get(key);
        if (Object.isUndefined(value)) {
            hash.set(key, defaultValue);
            value = defaultValue;
        }
        return value;
    },
    clone: function(element, deep) {
        if (!(element = $(element))) return;
        var clone = element.cloneNode(deep);
        clone._prototypeUID = void 0;
        if (deep) {
            var descendants = Element.select(clone, '*'),
                i = descendants.length;
            while (i--) {
                descendants[i]._prototypeUID = void 0;
            }
        }
        return Element.extend(clone);
    },
    purge: function(element) {
        if (!(element = $(element))) return;
        var purgeElement = Element._purgeElement;
        purgeElement(element);
        var descendants = element.getElementsByTagName('*'),
            i = descendants.length;
        while (i--) purgeElement(descendants[i]);
        return null;
    }
});
(function() {
    function toDecimal(pctString) {
        var match = pctString.match(/^(\d+)%?$/i);
        if (!match) return null;
        return (Number(match[1]) / 100);
    }

    function getPixelValue(value, property, context) {
        var element = null;
        if (Object.isElement(value)) {
            element = value;
            value = element.getStyle(property);
        }
        if (value === null) {
            return null;
        }
        if ((/^(?:-)?\d+(\.\d+)?(px)?$/i).test(value)) {
            return window.parseFloat(value);
        }
        var isPercentage = value.include('%'),
            isViewport = (context === document.viewport);
        if (/\d/.test(value) && element && element.runtimeStyle && !(isPercentage && isViewport)) {
            var style = element.style.left,
                rStyle = element.runtimeStyle.left;
            element.runtimeStyle.left = element.currentStyle.left;
            element.style.left = value || 0;
            value = element.style.pixelLeft;
            element.style.left = style;
            element.runtimeStyle.left = rStyle;
            return value;
        }
        if (element && isPercentage) {
            context = context || element.parentNode;
            var decimal = toDecimal(value);
            var whole = null;
            var position = element.getStyle('position');
            var isHorizontal = property.include('left') || property.include('right') || property.include('width');
            var isVertical = property.include('top') || property.include('bottom') || property.include('height');
            if (context === document.viewport) {
                if (isHorizontal) {
                    whole = document.viewport.getWidth();
                } else if (isVertical) {
                    whole = document.viewport.getHeight();
                }
            } else {
                if (isHorizontal) {
                    whole = $(context).measure('width');
                } else if (isVertical) {
                    whole = $(context).measure('height');
                }
            }
            return (whole === null) ? 0 : whole * decimal;
        }
        return 0;
    }

    function toCSSPixels(number) {
        if (Object.isString(number) && number.endsWith('px')) {
            return number;
        }
        return number + 'px';
    }

    function isDisplayed(element) {
        var originalElement = element;
        while (element && element.parentNode) {
            var display = element.getStyle('display');
            if (display === 'none') {
                return false;
            }
            element = $(element.parentNode);
        }
        return true;
    }
    var hasLayout = Prototype.K;
    if ('currentStyle' in document.documentElement) {
        hasLayout = function(element) {
            if (!element.currentStyle.hasLayout) {
                element.style.zoom = 1;
            }
            return element;
        };
    }

    function cssNameFor(key) {
        if (key.include('border')) key = key + '-width';
        return key.camelize();
    }
    Element.Layout = Class.create(Hash, {
        initialize: function($super, element, preCompute) {
            $super();
            this.element = $(element);
            Element.Layout.PROPERTIES.each(function(property) {
                this._set(property, null);
            }, this);
            if (preCompute) {
                this._preComputing = true;
                this._begin();
                Element.Layout.PROPERTIES.each(this._compute, this);
                this._end();
                this._preComputing = false;
            }
        },
        _set: function(property, value) {
            return Hash.prototype.set.call(this, property, value);
        },
        set: function(property, value) {
            throw "Properties of Element.Layout are read-only.";
        },
        get: function($super, property) {
            var value = $super(property);
            return value === null ? this._compute(property) : value;
        },
        _begin: function() {
            if (this._prepared) return;
            var element = this.element;
            if (isDisplayed(element)) {
                this._prepared = true;
                return;
            }
            var originalStyles = {
                position: element.style.position || '',
                width: element.style.width || '',
                visibility: element.style.visibility || '',
                display: element.style.display || ''
            };
            element.store('prototype_original_styles', originalStyles);
            var position = element.getStyle('position'),
                width = element.getStyle('width');
            if (width === "0px" || width === null) {
                element.style.display = 'block';
                width = element.getStyle('width');
            }
            var context = (position === 'fixed') ? document.viewport : element.parentNode;
            element.setStyle({
                position: 'absolute',
                visibility: 'hidden',
                display: 'block'
            });
            var positionedWidth = element.getStyle('width');
            var newWidth;
            if (width && (positionedWidth === width)) {
                newWidth = getPixelValue(element, 'width', context);
            } else if (position === 'absolute' || position === 'fixed') {
                newWidth = getPixelValue(element, 'width', context);
            } else {
                var parent = element.parentNode,
                    pLayout = $(parent).getLayout();
                newWidth = pLayout.get('width') -
                    this.get('margin-left') -
                    this.get('border-left') -
                    this.get('padding-left') -
                    this.get('padding-right') -
                    this.get('border-right') -
                    this.get('margin-right');
            }
            element.setStyle({
                width: newWidth + 'px'
            });
            this._prepared = true;
        },
        _end: function() {
            var element = this.element;
            var originalStyles = element.retrieve('prototype_original_styles');
            element.store('prototype_original_styles', null);
            element.setStyle(originalStyles);
            this._prepared = false;
        },
        _compute: function(property) {
            var COMPUTATIONS = Element.Layout.COMPUTATIONS;
            if (!(property in COMPUTATIONS)) {
                throw "Property not found.";
            }
            return this._set(property, COMPUTATIONS[property].call(this, this.element));
        },
        toObject: function() {
            var args = $A(arguments);
            var keys = (args.length === 0) ? Element.Layout.PROPERTIES : args.join(' ').split(' ');
            var obj = {};
            keys.each(function(key) {
                if (!Element.Layout.PROPERTIES.include(key)) return;
                var value = this.get(key);
                if (value != null) obj[key] = value;
            }, this);
            return obj;
        },
        toHash: function() {
            var obj = this.toObject.apply(this, arguments);
            return new Hash(obj);
        },
        toCSS: function() {
            var args = $A(arguments);
            var keys = (args.length === 0) ? Element.Layout.PROPERTIES : args.join(' ').split(' ');
            var css = {};
            keys.each(function(key) {
                if (!Element.Layout.PROPERTIES.include(key)) return;
                if (Element.Layout.COMPOSITE_PROPERTIES.include(key)) return;
                var value = this.get(key);
                if (value != null) css[cssNameFor(key)] = value + 'px';
            }, this);
            return css;
        },
        inspect: function() {
            return "#<Element.Layout>";
        }
    });
    Object.extend(Element.Layout, {
        PROPERTIES: $w('height width top left right bottom border-left border-right border-top border-bottom padding-left padding-right padding-top padding-bottom margin-top margin-bottom margin-left margin-right padding-box-width padding-box-height border-box-width border-box-height margin-box-width margin-box-height'),
        COMPOSITE_PROPERTIES: $w('padding-box-width padding-box-height margin-box-width margin-box-height border-box-width border-box-height'),
        COMPUTATIONS: {
            'height': function(element) {
                if (!this._preComputing) this._begin();
                var bHeight = this.get('border-box-height');
                if (bHeight <= 0) {
                    if (!this._preComputing) this._end();
                    return 0;
                }
                var bTop = this.get('border-top'),
                    bBottom = this.get('border-bottom');
                var pTop = this.get('padding-top'),
                    pBottom = this.get('padding-bottom');
                if (!this._preComputing) this._end();
                return bHeight - bTop - bBottom - pTop - pBottom;
            },
            'width': function(element) {
                if (!this._preComputing) this._begin();
                var bWidth = this.get('border-box-width');
                if (bWidth <= 0) {
                    if (!this._preComputing) this._end();
                    return 0;
                }
                var bLeft = this.get('border-left'),
                    bRight = this.get('border-right');
                var pLeft = this.get('padding-left'),
                    pRight = this.get('padding-right');
                if (!this._preComputing) this._end();
                return bWidth - bLeft - bRight - pLeft - pRight;
            },
            'padding-box-height': function(element) {
                var height = this.get('height'),
                    pTop = this.get('padding-top'),
                    pBottom = this.get('padding-bottom');
                return height + pTop + pBottom;
            },
            'padding-box-width': function(element) {
                var width = this.get('width'),
                    pLeft = this.get('padding-left'),
                    pRight = this.get('padding-right');
                return width + pLeft + pRight;
            },
            'border-box-height': function(element) {
                if (!this._preComputing) this._begin();
                var height = element.offsetHeight;
                if (!this._preComputing) this._end();
                return height;
            },
            'border-box-width': function(element) {
                if (!this._preComputing) this._begin();
                var width = element.offsetWidth;
                if (!this._preComputing) this._end();
                return width;
            },
            'margin-box-height': function(element) {
                var bHeight = this.get('border-box-height'),
                    mTop = this.get('margin-top'),
                    mBottom = this.get('margin-bottom');
                if (bHeight <= 0) return 0;
                return bHeight + mTop + mBottom;
            },
            'margin-box-width': function(element) {
                var bWidth = this.get('border-box-width'),
                    mLeft = this.get('margin-left'),
                    mRight = this.get('margin-right');
                if (bWidth <= 0) return 0;
                return bWidth + mLeft + mRight;
            },
            'top': function(element) {
                var offset = element.positionedOffset();
                return offset.top;
            },
            'bottom': function(element) {
                var offset = element.positionedOffset(),
                    parent = element.getOffsetParent(),
                    pHeight = parent.measure('height');
                var mHeight = this.get('border-box-height');
                return pHeight - mHeight - offset.top;
            },
            'left': function(element) {
                var offset = element.positionedOffset();
                return offset.left;
            },
            'right': function(element) {
                var offset = element.positionedOffset(),
                    parent = element.getOffsetParent(),
                    pWidth = parent.measure('width');
                var mWidth = this.get('border-box-width');
                return pWidth - mWidth - offset.left;
            },
            'padding-top': function(element) {
                return getPixelValue(element, 'paddingTop');
            },
            'padding-bottom': function(element) {
                return getPixelValue(element, 'paddingBottom');
            },
            'padding-left': function(element) {
                return getPixelValue(element, 'paddingLeft');
            },
            'padding-right': function(element) {
                return getPixelValue(element, 'paddingRight');
            },
            'border-top': function(element) {
                return getPixelValue(element, 'borderTopWidth');
            },
            'border-bottom': function(element) {
                return getPixelValue(element, 'borderBottomWidth');
            },
            'border-left': function(element) {
                return getPixelValue(element, 'borderLeftWidth');
            },
            'border-right': function(element) {
                return getPixelValue(element, 'borderRightWidth');
            },
            'margin-top': function(element) {
                return getPixelValue(element, 'marginTop');
            },
            'margin-bottom': function(element) {
                return getPixelValue(element, 'marginBottom');
            },
            'margin-left': function(element) {
                return getPixelValue(element, 'marginLeft');
            },
            'margin-right': function(element) {
                return getPixelValue(element, 'marginRight');
            }
        }
    });
    if ('getBoundingClientRect' in document.documentElement) {
        Object.extend(Element.Layout.COMPUTATIONS, {
            'right': function(element) {
                var parent = hasLayout(element.getOffsetParent());
                var rect = element.getBoundingClientRect(),
                    pRect = parent.getBoundingClientRect();
                return (pRect.right - rect.right).round();
            },
            'bottom': function(element) {
                var parent = hasLayout(element.getOffsetParent());
                var rect = element.getBoundingClientRect(),
                    pRect = parent.getBoundingClientRect();
                return (pRect.bottom - rect.bottom).round();
            }
        });
    }
    Element.Offset = Class.create({
        initialize: function(left, top) {
            this.left = left.round();
            this.top = top.round();
            this[0] = this.left;
            this[1] = this.top;
        },
        relativeTo: function(offset) {
            return new Element.Offset(this.left - offset.left, this.top - offset.top);
        },
        inspect: function() {
            return "#<Element.Offset left: #{left} top: #{top}>".interpolate(this);
        },
        toString: function() {
            return "[#{left}, #{top}]".interpolate(this);
        },
        toArray: function() {
            return [this.left, this.top];
        }
    });

    function getLayout(element, preCompute) {
        return new Element.Layout(element, preCompute);
    }

    function measure(element, property) {
        return $(element).getLayout().get(property);
    }

    function getDimensions(element) {
        element = $(element);
        var display = Element.getStyle(element, 'display');
        if (display && display !== 'none') {
            return {
                width: element.offsetWidth,
                height: element.offsetHeight
            };
        }
        var style = element.style;
        var originalStyles = {
            visibility: style.visibility,
            position: style.position,
            display: style.display
        };
        var newStyles = {
            visibility: 'hidden',
            display: 'block'
        };
        if (originalStyles.position !== 'fixed')
            newStyles.position = 'absolute';
        Element.setStyle(element, newStyles);
        var dimensions = {
            width: element.offsetWidth,
            height: element.offsetHeight
        };
        Element.setStyle(element, originalStyles);
        return dimensions;
    }

    function getOffsetParent(element) {
        element = $(element);
        if (isDocument(element) || isDetached(element) || isBody(element) || isHtml(element))
            return $(document.body);
        var isInline = (Element.getStyle(element, 'display') === 'inline');
        if (!isInline && element.offsetParent) return $(element.offsetParent);
        while ((element = element.parentNode) && element !== document.body) {
            if (Element.getStyle(element, 'position') !== 'static') {
                return isHtml(element) ? $(document.body) : $(element);
            }
        }
        return $(document.body);
    }

    function cumulativeOffset(element) {
        element = $(element);
        var valueT = 0,
            valueL = 0;
        if (element.parentNode) {
            do {
                valueT += element.offsetTop || 0;
                valueL += element.offsetLeft || 0;
                element = element.offsetParent;
            } while (element);
        }
        return new Element.Offset(valueL, valueT);
    }

    function positionedOffset(element) {
        element = $(element);
        var layout = element.getLayout();
        var valueT = 0,
            valueL = 0;
        do {
            valueT += element.offsetTop || 0;
            valueL += element.offsetLeft || 0;
            element = element.offsetParent;
            if (element) {
                if (isBody(element)) break;
                var p = Element.getStyle(element, 'position');
                if (p !== 'static') break;
            }
        } while (element);
        valueL -= layout.get('margin-top');
        valueT -= layout.get('margin-left');
        return new Element.Offset(valueL, valueT);
    }

    function cumulativeScrollOffset(element) {
        var valueT = 0,
            valueL = 0;
        do {
            valueT += element.scrollTop || 0;
            valueL += element.scrollLeft || 0;
            element = element.parentNode;
        } while (element);
        return new Element.Offset(valueL, valueT);
    }

    function viewportOffset(forElement) {
        element = $(element);
        var valueT = 0,
            valueL = 0,
            docBody = document.body;
        var element = forElement;
        do {
            valueT += element.offsetTop || 0;
            valueL += element.offsetLeft || 0;
            if (element.offsetParent == docBody && Element.getStyle(element, 'position') == 'absolute') break;
        } while (element = element.offsetParent);
        element = forElement;
        do {
            if (element != docBody) {
                valueT -= element.scrollTop || 0;
                valueL -= element.scrollLeft || 0;
            }
        } while (element = element.parentNode);
        return new Element.Offset(valueL, valueT);
    }

    function absolutize(element) {
        element = $(element);
        if (Element.getStyle(element, 'position') === 'absolute') {
            return element;
        }
        var offsetParent = getOffsetParent(element);
        var eOffset = element.viewportOffset(),
            pOffset = offsetParent.viewportOffset();
        var offset = eOffset.relativeTo(pOffset);
        var layout = element.getLayout();
        element.store('prototype_absolutize_original_styles', {
            left: element.getStyle('left'),
            top: element.getStyle('top'),
            width: element.getStyle('width'),
            height: element.getStyle('height')
        });
        element.setStyle({
            position: 'absolute',
            top: offset.top + 'px',
            left: offset.left + 'px',
            width: layout.get('width') + 'px',
            height: layout.get('height') + 'px'
        });
        return element;
    }

    function relativize(element) {
        element = $(element);
        if (Element.getStyle(element, 'position') === 'relative') {
            return element;
        }
        var originalStyles = element.retrieve('prototype_absolutize_original_styles');
        if (originalStyles) element.setStyle(originalStyles);
        return element;
    }
    if (Prototype.Browser.IE) {
        getOffsetParent = getOffsetParent.wrap(function(proceed, element) {
            element = $(element);
            if (isDocument(element) || isDetached(element) || isBody(element) || isHtml(element))
                return $(document.body);
            var position = element.getStyle('position');
            if (position !== 'static') return proceed(element);
            element.setStyle({
                position: 'relative'
            });
            var value = proceed(element);
            element.setStyle({
                position: position
            });
            return value;
        });
        positionedOffset = positionedOffset.wrap(function(proceed, element) {
            element = $(element);
            if (!element.parentNode) return new Element.Offset(0, 0);
            var position = element.getStyle('position');
            if (position !== 'static') return proceed(element);
            var offsetParent = element.getOffsetParent();
            if (offsetParent && offsetParent.getStyle('position') === 'fixed')
                hasLayout(offsetParent);
            element.setStyle({
                position: 'relative'
            });
            var value = proceed(element);
            element.setStyle({
                position: position
            });
            return value;
        });
    } else if (Prototype.Browser.Webkit) {
        cumulativeOffset = function(element) {
            element = $(element);
            var valueT = 0,
                valueL = 0;
            do {
                valueT += element.offsetTop || 0;
                valueL += element.offsetLeft || 0;
                if (element.offsetParent == document.body)
                    if (Element.getStyle(element, 'position') == 'absolute') break;
                element = element.offsetParent;
            } while (element);
            return new Element.Offset(valueL, valueT);
        };
    }
    Element.addMethods({
        getLayout: getLayout,
        measure: measure,
        getDimensions: getDimensions,
        getOffsetParent: getOffsetParent,
        cumulativeOffset: cumulativeOffset,
        positionedOffset: positionedOffset,
        cumulativeScrollOffset: cumulativeScrollOffset,
        viewportOffset: viewportOffset,
        absolutize: absolutize,
        relativize: relativize
    });

    function isBody(element) {
        return element.nodeName.toUpperCase() === 'BODY';
    }

    function isHtml(element) {
        return element.nodeName.toUpperCase() === 'HTML';
    }

    function isDocument(element) {
        return element.nodeType === Node.DOCUMENT_NODE;
    }

    function isDetached(element) {
        return element !== document.body && !Element.descendantOf(element, document.body);
    }
    if ('getBoundingClientRect' in document.documentElement) {
        Element.addMethods({
            viewportOffset: function(element) {
                element = $(element);
                if (isDetached(element)) return new Element.Offset(0, 0);
                var rect = element.getBoundingClientRect(),
                    docEl = document.documentElement;
                return new Element.Offset(rect.left - docEl.clientLeft, rect.top - docEl.clientTop);
            }
        });
    }
})();
window.$$ = function() {
    var expression = $A(arguments).join(', ');
    return Prototype.Selector.select(expression, document);
};
Prototype.Selector = (function() {
    function select() {
        throw new Error('Method "Prototype.Selector.select" must be defined.');
    }

    function match() {
        throw new Error('Method "Prototype.Selector.match" must be defined.');
    }

    function find(elements, expression, index) {
        index = index || 0;
        var match = Prototype.Selector.match,
            length = elements.length,
            matchIndex = 0,
            i;
        for (i = 0; i < length; i++) {
            if (match(elements[i], expression) && index == matchIndex++) {
                return Element.extend(elements[i]);
            }
        }
    }

    function extendElements(elements) {
        for (var i = 0, length = elements.length; i < length; i++) {
            Element.extend(elements[i]);
        }
        return elements;
    }
    var K = Prototype.K;
    return {
        select: select,
        match: match,
        find: find,
        extendElements: (Element.extend === K) ? K : extendElements,
        extendElement: Element.extend
    };
})();
Prototype._original_property = window.Sizzle;
/*!
 * Sizzle CSS Selector Engine - v1.0
 * Copyright 2009, The Dojo Foundation
 * Released under the MIT, BSD, and GPL Licenses.
 * More information: http://sizzlejs.com/
 */
(function() {
    var chunker = /((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^[\]]*\]|['"][^'"]*['"]|[^[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g,
        done = 0,
        toString = Object.prototype.toString,
        hasDuplicate = false,
        baseHasDuplicate = true;
    [0, 0].sort(function() {
        baseHasDuplicate = false;
        return 0;
    });
    var Sizzle = function(selector, context, results, seed) {
        results = results || [];
        var origContext = context = context || document;
        if (context.nodeType !== 1 && context.nodeType !== 9) {
            return [];
        }
        if (!selector || typeof selector !== "string") {
            return results;
        }
        var parts = [],
            m, set, checkSet, check, mode, extra, prune = true,
            contextXML = isXML(context),
            soFar = selector;
        while ((chunker.exec(""), m = chunker.exec(soFar)) !== null) {
            soFar = m[3];
            parts.push(m[1]);
            if (m[2]) {
                extra = m[3];
                break;
            }
        }
        if (parts.length > 1 && origPOS.exec(selector)) {
            if (parts.length === 2 && Expr.relative[parts[0]]) {
                set = posProcess(parts[0] + parts[1], context);
            } else {
                set = Expr.relative[parts[0]] ? [context] : Sizzle(parts.shift(), context);
                while (parts.length) {
                    selector = parts.shift();
                    if (Expr.relative[selector])
                        selector += parts.shift();
                    set = posProcess(selector, set);
                }
            }
        } else {
            if (!seed && parts.length > 1 && context.nodeType === 9 && !contextXML && Expr.match.ID.test(parts[0]) && !Expr.match.ID.test(parts[parts.length - 1])) {
                var ret = Sizzle.find(parts.shift(), context, contextXML);
                context = ret.expr ? Sizzle.filter(ret.expr, ret.set)[0] : ret.set[0];
            }
            if (context) {
                var ret = seed ? {
                    expr: parts.pop(),
                    set: makeArray(seed)
                } : Sizzle.find(parts.pop(), parts.length === 1 && (parts[0] === "~" || parts[0] === "+") && context.parentNode ? context.parentNode : context, contextXML);
                set = ret.expr ? Sizzle.filter(ret.expr, ret.set) : ret.set;
                if (parts.length > 0) {
                    checkSet = makeArray(set);
                } else {
                    prune = false;
                }
                while (parts.length) {
                    var cur = parts.pop(),
                        pop = cur;
                    if (!Expr.relative[cur]) {
                        cur = "";
                    } else {
                        pop = parts.pop();
                    }
                    if (pop == null) {
                        pop = context;
                    }
                    Expr.relative[cur](checkSet, pop, contextXML);
                }
            } else {
                checkSet = parts = [];
            }
        }
        if (!checkSet) {
            checkSet = set;
        }
        if (!checkSet) {
            throw "Syntax error, unrecognized expression: " + (cur || selector);
        }
        if (toString.call(checkSet) === "[object Array]") {
            if (!prune) {
                results.push.apply(results, checkSet);
            } else if (context && context.nodeType === 1) {
                for (var i = 0; checkSet[i] != null; i++) {
                    if (checkSet[i] && (checkSet[i] === true || checkSet[i].nodeType === 1 && contains(context, checkSet[i]))) {
                        results.push(set[i]);
                    }
                }
            } else {
                for (var i = 0; checkSet[i] != null; i++) {
                    if (checkSet[i] && checkSet[i].nodeType === 1) {
                        results.push(set[i]);
                    }
                }
            }
        } else {
            makeArray(checkSet, results);
        }
        if (extra) {
            Sizzle(extra, origContext, results, seed);
            Sizzle.uniqueSort(results);
        }
        return results;
    };
    Sizzle.uniqueSort = function(results) {
        if (sortOrder) {
            hasDuplicate = baseHasDuplicate;
            results.sort(sortOrder);
            if (hasDuplicate) {
                for (var i = 1; i < results.length; i++) {
                    if (results[i] === results[i - 1]) {
                        results.splice(i--, 1);
                    }
                }
            }
        }
        return results;
    };
    Sizzle.matches = function(expr, set) {
        return Sizzle(expr, null, null, set);
    };
    Sizzle.find = function(expr, context, isXML) {
        var set, match;
        if (!expr) {
            return [];
        }
        for (var i = 0, l = Expr.order.length; i < l; i++) {
            var type = Expr.order[i],
                match;
            if ((match = Expr.leftMatch[type].exec(expr))) {
                var left = match[1];
                match.splice(1, 1);
                if (left.substr(left.length - 1) !== "\\") {
                    match[1] = (match[1] || "").replace(/\\/g, "");
                    set = Expr.find[type](match, context, isXML);
                    if (set != null) {
                        expr = expr.replace(Expr.match[type], "");
                        break;
                    }
                }
            }
        }
        if (!set) {
            set = context.getElementsByTagName("*");
        }
        return {
            set: set,
            expr: expr
        };
    };
    Sizzle.filter = function(expr, set, inplace, not) {
        var old = expr,
            result = [],
            curLoop = set,
            match, anyFound, isXMLFilter = set && set[0] && isXML(set[0]);
        while (expr && set.length) {
            for (var type in Expr.filter) {
                if ((match = Expr.match[type].exec(expr)) != null) {
                    var filter = Expr.filter[type],
                        found, item;
                    anyFound = false;
                    if (curLoop == result) {
                        result = [];
                    }
                    if (Expr.preFilter[type]) {
                        match = Expr.preFilter[type](match, curLoop, inplace, result, not, isXMLFilter);
                        if (!match) {
                            anyFound = found = true;
                        } else if (match === true) {
                            continue;
                        }
                    }
                    if (match) {
                        for (var i = 0;
                            (item = curLoop[i]) != null; i++) {
                            if (item) {
                                found = filter(item, match, i, curLoop);
                                var pass = not ^ !!found;
                                if (inplace && found != null) {
                                    if (pass) {
                                        anyFound = true;
                                    } else {
                                        curLoop[i] = false;
                                    }
                                } else if (pass) {
                                    result.push(item);
                                    anyFound = true;
                                }
                            }
                        }
                    }
                    if (found !== undefined) {
                        if (!inplace) {
                            curLoop = result;
                        }
                        expr = expr.replace(Expr.match[type], "");
                        if (!anyFound) {
                            return [];
                        }
                        break;
                    }
                }
            }
            if (expr == old) {
                if (anyFound == null) {
                    throw "Syntax error, unrecognized expression: " + expr;
                } else {
                    break;
                }
            }
            old = expr;
        }
        return curLoop;
    };
    var Expr = Sizzle.selectors = {
        order: ["ID", "NAME", "TAG"],
        match: {
            ID: /#((?:[\w\u00c0-\uFFFF-]|\\.)+)/,
            CLASS: /\.((?:[\w\u00c0-\uFFFF-]|\\.)+)/,
            NAME: /\[name=['"]*((?:[\w\u00c0-\uFFFF-]|\\.)+)['"]*\]/,
            ATTR: /\[\s*((?:[\w\u00c0-\uFFFF-]|\\.)+)\s*(?:(\S?=)\s*(['"]*)(.*?)\3|)\s*\]/,
            TAG: /^((?:[\w\u00c0-\uFFFF\*-]|\\.)+)/,
            CHILD: /:(only|nth|last|first)-child(?:\((even|odd|[\dn+-]*)\))?/,
            POS: /:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^-]|$)/,
            PSEUDO: /:((?:[\w\u00c0-\uFFFF-]|\\.)+)(?:\((['"]*)((?:\([^\)]+\)|[^\2\(\)]*)+)\2\))?/
        },
        leftMatch: {},
        attrMap: {
            "class": "className",
            "for": "htmlFor"
        },
        attrHandle: {
            href: function(elem) {
                return elem.getAttribute("href");
            }
        },
        relative: {
            "+": function(checkSet, part, isXML) {
                var isPartStr = typeof part === "string",
                    isTag = isPartStr && !/\W/.test(part),
                    isPartStrNotTag = isPartStr && !isTag;
                if (isTag && !isXML) {
                    part = part.toUpperCase();
                }
                for (var i = 0, l = checkSet.length, elem; i < l; i++) {
                    if ((elem = checkSet[i])) {
                        while ((elem = elem.previousSibling) && elem.nodeType !== 1) {}
                        checkSet[i] = isPartStrNotTag || elem && elem.nodeName === part ? elem || false : elem === part;
                    }
                }
                if (isPartStrNotTag) {
                    Sizzle.filter(part, checkSet, true);
                }
            },
            ">": function(checkSet, part, isXML) {
                var isPartStr = typeof part === "string";
                if (isPartStr && !/\W/.test(part)) {
                    part = isXML ? part : part.toUpperCase();
                    for (var i = 0, l = checkSet.length; i < l; i++) {
                        var elem = checkSet[i];
                        if (elem) {
                            var parent = elem.parentNode;
                            checkSet[i] = parent.nodeName === part ? parent : false;
                        }
                    }
                } else {
                    for (var i = 0, l = checkSet.length; i < l; i++) {
                        var elem = checkSet[i];
                        if (elem) {
                            checkSet[i] = isPartStr ? elem.parentNode : elem.parentNode === part;
                        }
                    }
                    if (isPartStr) {
                        Sizzle.filter(part, checkSet, true);
                    }
                }
            },
            "": function(checkSet, part, isXML) {
                var doneName = done++,
                    checkFn = dirCheck;
                if (!/\W/.test(part)) {
                    var nodeCheck = part = isXML ? part : part.toUpperCase();
                    checkFn = dirNodeCheck;
                }
                checkFn("parentNode", part, doneName, checkSet, nodeCheck, isXML);
            },
            "~": function(checkSet, part, isXML) {
                var doneName = done++,
                    checkFn = dirCheck;
                if (typeof part === "string" && !/\W/.test(part)) {
                    var nodeCheck = part = isXML ? part : part.toUpperCase();
                    checkFn = dirNodeCheck;
                }
                checkFn("previousSibling", part, doneName, checkSet, nodeCheck, isXML);
            }
        },
        find: {
            ID: function(match, context, isXML) {
                if (typeof context.getElementById !== "undefined" && !isXML) {
                    var m = context.getElementById(match[1]);
                    return m ? [m] : [];
                }
            },
            NAME: function(match, context, isXML) {
                if (typeof context.getElementsByName !== "undefined") {
                    var ret = [],
                        results = context.getElementsByName(match[1]);
                    for (var i = 0, l = results.length; i < l; i++) {
                        if (results[i].getAttribute("name") === match[1]) {
                            ret.push(results[i]);
                        }
                    }
                    return ret.length === 0 ? null : ret;
                }
            },
            TAG: function(match, context) {
                return context.getElementsByTagName(match[1]);
            }
        },
        preFilter: {
            CLASS: function(match, curLoop, inplace, result, not, isXML) {
                match = " " + match[1].replace(/\\/g, "") + " ";
                if (isXML) {
                    return match;
                }
                for (var i = 0, elem;
                    (elem = curLoop[i]) != null; i++) {
                    if (elem) {
                        if (not ^ (elem.className && (" " + elem.className + " ").indexOf(match) >= 0)) {
                            if (!inplace)
                                result.push(elem);
                        } else if (inplace) {
                            curLoop[i] = false;
                        }
                    }
                }
                return false;
            },
            ID: function(match) {
                return match[1].replace(/\\/g, "");
            },
            TAG: function(match, curLoop) {
                for (var i = 0; curLoop[i] === false; i++) {}
                return curLoop[i] && isXML(curLoop[i]) ? match[1] : match[1].toUpperCase();
            },
            CHILD: function(match) {
                if (match[1] == "nth") {
                    var test = /(-?)(\d*)n((?:\+|-)?\d*)/.exec(match[2] == "even" && "2n" || match[2] == "odd" && "2n+1" || !/\D/.test(match[2]) && "0n+" + match[2] || match[2]);
                    match[2] = (test[1] + (test[2] || 1)) - 0;
                    match[3] = test[3] - 0;
                }
                match[0] = done++;
                return match;
            },
            ATTR: function(match, curLoop, inplace, result, not, isXML) {
                var name = match[1].replace(/\\/g, "");
                if (!isXML && Expr.attrMap[name]) {
                    match[1] = Expr.attrMap[name];
                }
                if (match[2] === "~=") {
                    match[4] = " " + match[4] + " ";
                }
                return match;
            },
            PSEUDO: function(match, curLoop, inplace, result, not) {
                if (match[1] === "not") {
                    if ((chunker.exec(match[3]) || "").length > 1 || /^\w/.test(match[3])) {
                        match[3] = Sizzle(match[3], null, null, curLoop);
                    } else {
                        var ret = Sizzle.filter(match[3], curLoop, inplace, true ^ not);
                        if (!inplace) {
                            result.push.apply(result, ret);
                        }
                        return false;
                    }
                } else if (Expr.match.POS.test(match[0]) || Expr.match.CHILD.test(match[0])) {
                    return true;
                }
                return match;
            },
            POS: function(match) {
                match.unshift(true);
                return match;
            }
        },
        filters: {
            enabled: function(elem) {
                return elem.disabled === false && elem.type !== "hidden";
            },
            disabled: function(elem) {
                return elem.disabled === true;
            },
            checked: function(elem) {
                return elem.checked === true;
            },
            selected: function(elem) {
                elem.parentNode.selectedIndex;
                return elem.selected === true;
            },
            parent: function(elem) {
                return !!elem.firstChild;
            },
            empty: function(elem) {
                return !elem.firstChild;
            },
            has: function(elem, i, match) {
                return !!Sizzle(match[3], elem).length;
            },
            header: function(elem) {
                return /h\d/i.test(elem.nodeName);
            },
            text: function(elem) {
                return "text" === elem.type;
            },
            radio: function(elem) {
                return "radio" === elem.type;
            },
            checkbox: function(elem) {
                return "checkbox" === elem.type;
            },
            file: function(elem) {
                return "file" === elem.type;
            },
            password: function(elem) {
                return "password" === elem.type;
            },
            submit: function(elem) {
                return "submit" === elem.type;
            },
            image: function(elem) {
                return "image" === elem.type;
            },
            reset: function(elem) {
                return "reset" === elem.type;
            },
            button: function(elem) {
                return "button" === elem.type || elem.nodeName.toUpperCase() === "BUTTON";
            },
            input: function(elem) {
                return /input|select|textarea|button/i.test(elem.nodeName);
            }
        },
        setFilters: {
            first: function(elem, i) {
                return i === 0;
            },
            last: function(elem, i, match, array) {
                return i === array.length - 1;
            },
            even: function(elem, i) {
                return i % 2 === 0;
            },
            odd: function(elem, i) {
                return i % 2 === 1;
            },
            lt: function(elem, i, match) {
                return i < match[3] - 0;
            },
            gt: function(elem, i, match) {
                return i > match[3] - 0;
            },
            nth: function(elem, i, match) {
                return match[3] - 0 == i;
            },
            eq: function(elem, i, match) {
                return match[3] - 0 == i;
            }
        },
        filter: {
            PSEUDO: function(elem, match, i, array) {
                var name = match[1],
                    filter = Expr.filters[name];
                if (filter) {
                    return filter(elem, i, match, array);
                } else if (name === "contains") {
                    return (elem.textContent || elem.innerText || "").indexOf(match[3]) >= 0;
                } else if (name === "not") {
                    var not = match[3];
                    for (var i = 0, l = not.length; i < l; i++) {
                        if (not[i] === elem) {
                            return false;
                        }
                    }
                    return true;
                }
            },
            CHILD: function(elem, match) {
                var type = match[1],
                    node = elem;
                switch (type) {
                    case 'only':
                    case 'first':
                        while ((node = node.previousSibling)) {
                            if (node.nodeType === 1) return false;
                        }
                        if (type == 'first') return true;
                        node = elem;
                    case 'last':
                        while ((node = node.nextSibling)) {
                            if (node.nodeType === 1) return false;
                        }
                        return true;
                    case 'nth':
                        var first = match[2],
                            last = match[3];
                        if (first == 1 && last == 0) {
                            return true;
                        }
                        var doneName = match[0],
                            parent = elem.parentNode;
                        if (parent && (parent.sizcache !== doneName || !elem.nodeIndex)) {
                            var count = 0;
                            for (node = parent.firstChild; node; node = node.nextSibling) {
                                if (node.nodeType === 1) {
                                    node.nodeIndex = ++count;
                                }
                            }
                            parent.sizcache = doneName;
                        }
                        var diff = elem.nodeIndex - last;
                        if (first == 0) {
                            return diff == 0;
                        } else {
                            return (diff % first == 0 && diff / first >= 0);
                        }
                }
            },
            ID: function(elem, match) {
                return elem.nodeType === 1 && elem.getAttribute("id") === match;
            },
            TAG: function(elem, match) {
                return (match === "*" && elem.nodeType === 1) || elem.nodeName === match;
            },
            CLASS: function(elem, match) {
                return (" " + (elem.className || elem.getAttribute("class")) + " ").indexOf(match) > -1;
            },
            ATTR: function(elem, match) {
                var name = match[1],
                    result = Expr.attrHandle[name] ? Expr.attrHandle[name](elem) : elem[name] != null ? elem[name] : elem.getAttribute(name),
                    value = result + "",
                    type = match[2],
                    check = match[4];
                return result == null ? type === "!=" : type === "=" ? value === check : type === "*=" ? value.indexOf(check) >= 0 : type === "~=" ? (" " + value + " ").indexOf(check) >= 0 : !check ? value && result !== false : type === "!=" ? value != check : type === "^=" ? value.indexOf(check) === 0 : type === "$=" ? value.substr(value.length - check.length) === check : type === "|=" ? value === check || value.substr(0, check.length + 1) === check + "-" : false;
            },
            POS: function(elem, match, i, array) {
                var name = match[2],
                    filter = Expr.setFilters[name];
                if (filter) {
                    return filter(elem, i, match, array);
                }
            }
        }
    };
    var origPOS = Expr.match.POS;
    for (var type in Expr.match) {
        Expr.match[type] = new RegExp(Expr.match[type].source + /(?![^\[]*\])(?![^\(]*\))/.source);
        Expr.leftMatch[type] = new RegExp(/(^(?:.|\r|\n)*?)/.source + Expr.match[type].source);
    }
    var makeArray = function(array, results) {
        array = Array.prototype.slice.call(array, 0);
        if (results) {
            results.push.apply(results, array);
            return results;
        }
        return array;
    };
    try {
        Array.prototype.slice.call(document.documentElement.childNodes, 0);
    } catch (e) {
        makeArray = function(array, results) {
            var ret = results || [];
            if (toString.call(array) === "[object Array]") {
                Array.prototype.push.apply(ret, array);
            } else {
                if (typeof array.length === "number") {
                    for (var i = 0, l = array.length; i < l; i++) {
                        ret.push(array[i]);
                    }
                } else {
                    for (var i = 0; array[i]; i++) {
                        ret.push(array[i]);
                    }
                }
            }
            return ret;
        };
    }
    var sortOrder;
    if (document.documentElement.compareDocumentPosition) {
        sortOrder = function(a, b) {
            if (!a.compareDocumentPosition || !b.compareDocumentPosition) {
                if (a == b) {
                    hasDuplicate = true;
                }
                return 0;
            }
            var ret = a.compareDocumentPosition(b) & 4 ? -1 : a === b ? 0 : 1;
            if (ret === 0) {
                hasDuplicate = true;
            }
            return ret;
        };
    } else if ("sourceIndex" in document.documentElement) {
        sortOrder = function(a, b) {
            if (!a.sourceIndex || !b.sourceIndex) {
                if (a == b) {
                    hasDuplicate = true;
                }
                return 0;
            }
            var ret = a.sourceIndex - b.sourceIndex;
            if (ret === 0) {
                hasDuplicate = true;
            }
            return ret;
        };
    } else if (document.createRange) {
        sortOrder = function(a, b) {
            if (!a.ownerDocument || !b.ownerDocument) {
                if (a == b) {
                    hasDuplicate = true;
                }
                return 0;
            }
            var aRange = a.ownerDocument.createRange(),
                bRange = b.ownerDocument.createRange();
            aRange.setStart(a, 0);
            aRange.setEnd(a, 0);
            bRange.setStart(b, 0);
            bRange.setEnd(b, 0);
            var ret = aRange.compareBoundaryPoints(Range.START_TO_END, bRange);
            if (ret === 0) {
                hasDuplicate = true;
            }
            return ret;
        };
    }
    (function() {
        var form = document.createElement("div"),
            id = "script" + (new Date).getTime();
        form.innerHTML = "<a name='" + id + "'/>";
        var root = document.documentElement;
        root.insertBefore(form, root.firstChild);
        if (!!document.getElementById(id)) {
            Expr.find.ID = function(match, context, isXML) {
                if (typeof context.getElementById !== "undefined" && !isXML) {
                    var m = context.getElementById(match[1]);
                    return m ? m.id === match[1] || typeof m.getAttributeNode !== "undefined" && m.getAttributeNode("id").nodeValue === match[1] ? [m] : undefined : [];
                }
            };
            Expr.filter.ID = function(elem, match) {
                var node = typeof elem.getAttributeNode !== "undefined" && elem.getAttributeNode("id");
                return elem.nodeType === 1 && node && node.nodeValue === match;
            };
        }
        root.removeChild(form);
        root = form = null;
    })();
    (function() {
        var div = document.createElement("div");
        div.appendChild(document.createComment(""));
        if (div.getElementsByTagName("*").length > 0) {
            Expr.find.TAG = function(match, context) {
                var results = context.getElementsByTagName(match[1]);
                if (match[1] === "*") {
                    var tmp = [];
                    for (var i = 0; results[i]; i++) {
                        if (results[i].nodeType === 1) {
                            tmp.push(results[i]);
                        }
                    }
                    results = tmp;
                }
                return results;
            };
        }
        div.innerHTML = "<a href='#'></a>";
        if (div.firstChild && typeof div.firstChild.getAttribute !== "undefined" && div.firstChild.getAttribute("href") !== "#") {
            Expr.attrHandle.href = function(elem) {
                return elem.getAttribute("href", 2);
            };
        }
        div = null;
    })();
    if (document.querySelectorAll)(function() {
        var oldSizzle = Sizzle,
            div = document.createElement("div");
        div.innerHTML = "<p class='TEST'></p>";
        if (div.querySelectorAll && div.querySelectorAll(".TEST").length === 0) {
            return;
        }
        Sizzle = function(query, context, extra, seed) {
            context = context || document;
            if (!seed && context.nodeType === 9 && !isXML(context)) {
                try {
                    return makeArray(context.querySelectorAll(query), extra);
                } catch (e) {}
            }
            return oldSizzle(query, context, extra, seed);
        };
        for (var prop in oldSizzle) {
            Sizzle[prop] = oldSizzle[prop];
        }
        div = null;
    })();
    if (document.getElementsByClassName && document.documentElement.getElementsByClassName)(function() {
        var div = document.createElement("div");
        div.innerHTML = "<div class='test e'></div><div class='test'></div>";
        if (div.getElementsByClassName("e").length === 0)
            return;
        div.lastChild.className = "e";
        if (div.getElementsByClassName("e").length === 1)
            return;
        Expr.order.splice(1, 0, "CLASS");
        Expr.find.CLASS = function(match, context, isXML) {
            if (typeof context.getElementsByClassName !== "undefined" && !isXML) {
                return context.getElementsByClassName(match[1]);
            }
        };
        div = null;
    })();

    function dirNodeCheck(dir, cur, doneName, checkSet, nodeCheck, isXML) {
        var sibDir = dir == "previousSibling" && !isXML;
        for (var i = 0, l = checkSet.length; i < l; i++) {
            var elem = checkSet[i];
            if (elem) {
                if (sibDir && elem.nodeType === 1) {
                    elem.sizcache = doneName;
                    elem.sizset = i;
                }
                elem = elem[dir];
                var match = false;
                while (elem) {
                    if (elem.sizcache === doneName) {
                        match = checkSet[elem.sizset];
                        break;
                    }
                    if (elem.nodeType === 1 && !isXML) {
                        elem.sizcache = doneName;
                        elem.sizset = i;
                    }
                    if (elem.nodeName === cur) {
                        match = elem;
                        break;
                    }
                    elem = elem[dir];
                }
                checkSet[i] = match;
            }
        }
    }

    function dirCheck(dir, cur, doneName, checkSet, nodeCheck, isXML) {
        var sibDir = dir == "previousSibling" && !isXML;
        for (var i = 0, l = checkSet.length; i < l; i++) {
            var elem = checkSet[i];
            if (elem) {
                if (sibDir && elem.nodeType === 1) {
                    elem.sizcache = doneName;
                    elem.sizset = i;
                }
                elem = elem[dir];
                var match = false;
                while (elem) {
                    if (elem.sizcache === doneName) {
                        match = checkSet[elem.sizset];
                        break;
                    }
                    if (elem.nodeType === 1) {
                        if (!isXML) {
                            elem.sizcache = doneName;
                            elem.sizset = i;
                        }
                        if (typeof cur !== "string") {
                            if (elem === cur) {
                                match = true;
                                break;
                            }
                        } else if (Sizzle.filter(cur, [elem]).length > 0) {
                            match = elem;
                            break;
                        }
                    }
                    elem = elem[dir];
                }
                checkSet[i] = match;
            }
        }
    }
    var contains = document.compareDocumentPosition ? function(a, b) {
        return a.compareDocumentPosition(b) & 16;
    } : function(a, b) {
        return a !== b && (a.contains ? a.contains(b) : true);
    };
    var isXML = function(elem) {
        return elem.nodeType === 9 && elem.documentElement.nodeName !== "HTML" || !!elem.ownerDocument && elem.ownerDocument.documentElement.nodeName !== "HTML";
    };
    var posProcess = function(selector, context) {
        var tmpSet = [],
            later = "",
            match, root = context.nodeType ? [context] : context;
        while ((match = Expr.match.PSEUDO.exec(selector))) {
            later += match[0];
            selector = selector.replace(Expr.match.PSEUDO, "");
        }
        selector = Expr.relative[selector] ? selector + "*" : selector;
        for (var i = 0, l = root.length; i < l; i++) {
            Sizzle(selector, root[i], tmpSet);
        }
        return Sizzle.filter(later, tmpSet);
    };
    window.Sizzle = Sizzle;
})();;
(function(engine) {
    var extendElements = Prototype.Selector.extendElements;

    function select(selector, scope) {
        return extendElements(engine(selector, scope || document));
    }

    function match(element, selector) {
        return engine.matches(selector, [element]).length == 1;
    }
    Prototype.Selector.engine = engine;
    Prototype.Selector.select = select;
    Prototype.Selector.match = match;
})(Sizzle);
window.Sizzle = Prototype._original_property;
delete Prototype._original_property;
var Form = {
    reset: function(form) {
        form = $(form);
        form.reset();
        return form;
    },
    serializeElements: function(elements, options) {
        if (typeof options != 'object') options = {
            hash: !!options
        };
        else if (Object.isUndefined(options.hash)) options.hash = true;
        var key, value, submitted = false,
            submit = options.submit,
            accumulator, initial;
        if (options.hash) {
            initial = {};
            accumulator = function(result, key, value) {
                if (key in result) {
                    if (!Object.isArray(result[key])) result[key] = [result[key]];
                    result[key].push(value);
                } else result[key] = value;
                return result;
            };
        } else {
            initial = '';
            accumulator = function(result, key, value) {
                return result + (result ? '&' : '') + encodeURIComponent(key) + '=' + encodeURIComponent(value);
            }
        }
        return elements.inject(initial, function(result, element) {
            if (!element.disabled && element.name) {
                key = element.name;
                value = $(element).getValue();
                if (value != null && element.type != 'file' && (element.type != 'submit' || (!submitted && submit !== false && (!submit || key == submit) && (submitted = true)))) {
                    result = accumulator(result, key, value);
                }
            }
            return result;
        });
    }
};
Form.Methods = {
    serialize: function(form, options) {
        return Form.serializeElements(Form.getElements(form), options);
    },
    getElements: function(form) {
        var elements = $(form).getElementsByTagName('*'),
            element, arr = [],
            serializers = Form.Element.Serializers;
        for (var i = 0; element = elements[i]; i++) {
            arr.push(element);
        }
        return arr.inject([], function(elements, child) {
            if (serializers[child.tagName.toLowerCase()])
                elements.push(Element.extend(child));
            return elements;
        })
    },
    getInputs: function(form, typeName, name) {
        form = $(form);
        var inputs = form.getElementsByTagName('input');
        if (!typeName && !name) return $A(inputs).map(Element.extend);
        for (var i = 0, matchingInputs = [], length = inputs.length; i < length; i++) {
            var input = inputs[i];
            if ((typeName && input.type != typeName) || (name && input.name != name))
                continue;
            matchingInputs.push(Element.extend(input));
        }
        return matchingInputs;
    },
    disable: function(form) {
        form = $(form);
        Form.getElements(form).invoke('disable');
        return form;
    },
    enable: function(form) {
        form = $(form);
        Form.getElements(form).invoke('enable');
        return form;
    },
    findFirstElement: function(form) {
        var elements = $(form).getElements().findAll(function(element) {
            return 'hidden' != element.type && !element.disabled;
        });
        var firstByIndex = elements.findAll(function(element) {
            return element.hasAttribute('tabIndex') && element.tabIndex >= 0;
        }).sortBy(function(element) {
            return element.tabIndex
        }).first();
        return firstByIndex ? firstByIndex : elements.find(function(element) {
            return /^(?:input|select|textarea)$/i.test(element.tagName);
        });
    },
    focusFirstElement: function(form) {
        form = $(form);
        var element = form.findFirstElement();
        if (element) element.activate();
        return form;
    },
    request: function(form, options) {
        form = $(form), options = Object.clone(options || {});
        var params = options.parameters,
            action = form.readAttribute('action') || '';
        if (action.blank()) action = window.location.href;
        options.parameters = form.serialize(true);
        if (params) {
            if (Object.isString(params)) params = params.toQueryParams();
            Object.extend(options.parameters, params);
        }
        if (form.hasAttribute('method') && !options.method)
            options.method = form.method;
        return new Ajax.Request(action, options);
    }
};
Form.Element = {
    focus: function(element) {
        $(element).focus();
        return element;
    },
    select: function(element) {
        $(element).select();
        return element;
    }
};
Form.Element.Methods = {
    serialize: function(element) {
        element = $(element);
        if (!element.disabled && element.name) {
            var value = element.getValue();
            if (value != undefined) {
                var pair = {};
                pair[element.name] = value;
                return Object.toQueryString(pair);
            }
        }
        return '';
    },
    getValue: function(element) {
        element = $(element);
        var method = element.tagName.toLowerCase();
        return Form.Element.Serializers[method](element);
    },
    setValue: function(element, value) {
        element = $(element);
        var method = element.tagName.toLowerCase();
        Form.Element.Serializers[method](element, value);
        return element;
    },
    clear: function(element) {
        $(element).value = '';
        return element;
    },
    present: function(element) {
        return $(element).value != '';
    },
    activate: function(element) {
        element = $(element);
        try {
            element.focus();
            if (element.select && (element.tagName.toLowerCase() != 'input' || !(/^(?:button|reset|submit)$/i.test(element.type))))
                element.select();
        } catch (e) {}
        return element;
    },
    disable: function(element) {
        element = $(element);
        element.disabled = true;
        return element;
    },
    enable: function(element) {
        element = $(element);
        element.disabled = false;
        return element;
    }
};
var Field = Form.Element;
var $F = Form.Element.Methods.getValue;
Form.Element.Serializers = (function() {
    function input(element, value) {
        switch (element.type.toLowerCase()) {
            case 'checkbox':
            case 'radio':
                return inputSelector(element, value);
            default:
                return valueSelector(element, value);
        }
    }

    function inputSelector(element, value) {
        if (Object.isUndefined(value))
            return element.checked ? element.value : null;
        else element.checked = !!value;
    }

    function valueSelector(element, value) {
        if (Object.isUndefined(value)) return element.value;
        else element.value = value;
    }

    function select(element, value) {
        if (Object.isUndefined(value))
            return (element.type === 'select-one' ? selectOne : selectMany)(element);
        var opt, currentValue, single = !Object.isArray(value);
        for (var i = 0, length = element.length; i < length; i++) {
            opt = element.options[i];
            currentValue = this.optionValue(opt);
            if (single) {
                if (currentValue == value) {
                    opt.selected = true;
                    return;
                }
            } else opt.selected = value.include(currentValue);
        }
    }

    function selectOne(element) {
        var index = element.selectedIndex;
        return index >= 0 ? optionValue(element.options[index]) : null;
    }

    function selectMany(element) {
        var values, length = element.length;
        if (!length) return null;
        for (var i = 0, values = []; i < length; i++) {
            var opt = element.options[i];
            if (opt.selected) values.push(optionValue(opt));
        }
        return values;
    }

    function optionValue(opt) {
        return Element.hasAttribute(opt, 'value') ? opt.value : opt.text;
    }
    return {
        input: input,
        inputSelector: inputSelector,
        textarea: valueSelector,
        select: select,
        selectOne: selectOne,
        selectMany: selectMany,
        optionValue: optionValue,
        button: valueSelector
    };
})();
Abstract.TimedObserver = Class.create(PeriodicalExecuter, {
    initialize: function($super, element, frequency, callback) {
        $super(callback, frequency);
        this.element = $(element);
        this.lastValue = this.getValue();
    },
    execute: function() {
        var value = this.getValue();
        if (Object.isString(this.lastValue) && Object.isString(value) ? this.lastValue != value : String(this.lastValue) != String(value)) {
            this.callback(this.element, value);
            this.lastValue = value;
        }
    }
});
Form.Element.Observer = Class.create(Abstract.TimedObserver, {
    getValue: function() {
        return Form.Element.getValue(this.element);
    }
});
Form.Observer = Class.create(Abstract.TimedObserver, {
    getValue: function() {
        return Form.serialize(this.element);
    }
});
Abstract.EventObserver = Class.create({
    initialize: function(element, callback) {
        this.element = $(element);
        this.callback = callback;
        this.lastValue = this.getValue();
        if (this.element.tagName.toLowerCase() == 'form')
            this.registerFormCallbacks();
        else
            this.registerCallback(this.element);
    },
    onElementEvent: function() {
        var value = this.getValue();
        if (this.lastValue != value) {
            this.callback(this.element, value);
            this.lastValue = value;
        }
    },
    registerFormCallbacks: function() {
        Form.getElements(this.element).each(this.registerCallback, this);
    },
    registerCallback: function(element) {
        if (element.type) {
            switch (element.type.toLowerCase()) {
                case 'checkbox':
                case 'radio':
                    Event.observe(element, 'click', this.onElementEvent.bind(this));
                    break;
                default:
                    Event.observe(element, 'change', this.onElementEvent.bind(this));
                    break;
            }
        }
    }
});
Form.Element.EventObserver = Class.create(Abstract.EventObserver, {
    getValue: function() {
        return Form.Element.getValue(this.element);
    }
});
Form.EventObserver = Class.create(Abstract.EventObserver, {
    getValue: function() {
        return Form.serialize(this.element);
    }
});
(function() {
    var Event = {
        KEY_BACKSPACE: 8,
        KEY_TAB: 9,
        KEY_RETURN: 13,
        KEY_ESC: 27,
        KEY_LEFT: 37,
        KEY_UP: 38,
        KEY_RIGHT: 39,
        KEY_DOWN: 40,
        KEY_DELETE: 46,
        KEY_HOME: 36,
        KEY_END: 35,
        KEY_PAGEUP: 33,
        KEY_PAGEDOWN: 34,
        KEY_INSERT: 45,
        cache: {}
    };
    var docEl = document.documentElement;
    var MOUSEENTER_MOUSELEAVE_EVENTS_SUPPORTED = 'onmouseenter' in docEl && 'onmouseleave' in docEl;
    var isIELegacyEvent = function(event) {
        return false;
    };
    if (window.attachEvent) {
        if (window.addEventListener) {
            isIELegacyEvent = function(event) {
                return !(event instanceof window.Event);
            };
        } else {
            isIELegacyEvent = function(event) {
                return true;
            };
        }
    }
    var _isButton;

    function _isButtonForDOMEvents(event, code) {
        return event.which ? (event.which === code + 1) : (event.button === code);
    }
    var legacyButtonMap = {
        0: 1,
        1: 4,
        2: 2
    };

    function _isButtonForLegacyEvents(event, code) {
        return event.button === legacyButtonMap[code];
    }

    function _isButtonForWebKit(event, code) {
        switch (code) {
            case 0:
                return event.which == 1 && !event.metaKey;
            case 1:
                return event.which == 2 || (event.which == 1 && event.metaKey);
            case 2:
                return event.which == 3;
            default:
                return false;
        }
    }
    if (window.attachEvent) {
        if (!window.addEventListener) {
            _isButton = _isButtonForLegacyEvents;
        } else {
            _isButton = function(event, code) {
                return isIELegacyEvent(event) ? _isButtonForLegacyEvents(event, code) : _isButtonForDOMEvents(event, code);
            }
        }
    } else if (Prototype.Browser.WebKit) {
        _isButton = _isButtonForWebKit;
    } else {
        _isButton = _isButtonForDOMEvents;
    }

    function isLeftClick(event) {
        return _isButton(event, 0)
    }

    function isMiddleClick(event) {
        return _isButton(event, 1)
    }

    function isRightClick(event) {
        return _isButton(event, 2)
    }

    function element(event) {
        event = Event.extend(event);
        var node = event.target,
            type = event.type,
            currentTarget = event.currentTarget;
        if (currentTarget && currentTarget.tagName) {
            if (type === 'load' || type === 'error' || (type === 'click' && currentTarget.tagName.toLowerCase() === 'input' && currentTarget.type === 'radio'))
                node = currentTarget;
        }
        if (node.nodeType == Node.TEXT_NODE)
            node = node.parentNode;
        return Element.extend(node);
    }

    function findElement(event, expression) {
        var element = Event.element(event);
        if (!expression) return element;
        while (element) {
            if (Object.isElement(element) && Prototype.Selector.match(element, expression)) {
                return Element.extend(element);
            }
            element = element.parentNode;
        }
    }

    function pointer(event) {
        return {
            x: pointerX(event),
            y: pointerY(event)
        };
    }

    function pointerX(event) {
        var docElement = document.documentElement,
            body = document.body || {
                scrollLeft: 0
            };
        return event.pageX || (event.clientX +
            (docElement.scrollLeft || body.scrollLeft) -
            (docElement.clientLeft || 0));
    }

    function pointerY(event) {
        var docElement = document.documentElement,
            body = document.body || {
                scrollTop: 0
            };
        return event.pageY || (event.clientY +
            (docElement.scrollTop || body.scrollTop) -
            (docElement.clientTop || 0));
    }

    function stop(event) {
        Event.extend(event);
        event.preventDefault();
        event.stopPropagation();
        event.stopped = true;
    }
    Event.Methods = {
        isLeftClick: isLeftClick,
        isMiddleClick: isMiddleClick,
        isRightClick: isRightClick,
        element: element,
        findElement: findElement,
        pointer: pointer,
        pointerX: pointerX,
        pointerY: pointerY,
        stop: stop
    };
    var methods = Object.keys(Event.Methods).inject({}, function(m, name) {
        m[name] = Event.Methods[name].methodize();
        return m;
    });
    if (window.attachEvent) {
        function _relatedTarget(event) {
            var element;
            switch (event.type) {
                case 'mouseover':
                case 'mouseenter':
                    element = event.fromElement;
                    break;
                case 'mouseout':
                case 'mouseleave':
                    element = event.toElement;
                    break;
                default:
                    return null;
            }
            return Element.extend(element);
        }
        var additionalMethods = {
            stopPropagation: function() {
                this.cancelBubble = true
            },
            preventDefault: function() {
                this.returnValue = false
            },
            inspect: function() {
                return '[object Event]'
            }
        };
        Event.extend = function(event, element) {
            if (!event) return false;
            if (!isIELegacyEvent(event)) return event;
            if (event._extendedByPrototype) return event;
            event._extendedByPrototype = Prototype.emptyFunction;
            var pointer = Event.pointer(event);
            Object.extend(event, {
                target: event.srcElement || element,
                relatedTarget: _relatedTarget(event),
                pageX: pointer.x,
                pageY: pointer.y
            });
            Object.extend(event, methods);
            Object.extend(event, additionalMethods);
            return event;
        };
    } else {
        Event.extend = Prototype.K;
    }
    if (window.addEventListener) {
        Event.prototype = window.Event.prototype || document.createEvent('HTMLEvents').__proto__;
        Object.extend(Event.prototype, methods);
    }

    function _createResponder(element, eventName, handler) {
        var registry = Element.retrieve(element, 'prototype_event_registry');
        if (Object.isUndefined(registry)) {
            CACHE.push(element);
            registry = Element.retrieve(element, 'prototype_event_registry', $H());
        }
        var respondersForEvent = registry.get(eventName);
        if (Object.isUndefined(respondersForEvent)) {
            respondersForEvent = [];
            registry.set(eventName, respondersForEvent);
        }
        if (respondersForEvent.pluck('handler').include(handler)) return false;
        var responder;
        if (eventName.include(":")) {
            responder = function(event) {
                if (Object.isUndefined(event.eventName))
                    return false;
                if (event.eventName !== eventName)
                    return false;
                Event.extend(event, element);
                handler.call(element, event);
            };
        } else {
            if (!MOUSEENTER_MOUSELEAVE_EVENTS_SUPPORTED && (eventName === "mouseenter" || eventName === "mouseleave")) {
                if (eventName === "mouseenter" || eventName === "mouseleave") {
                    responder = function(event) {
                        Event.extend(event, element);
                        var parent = event.relatedTarget;
                        while (parent && parent !== element) {
                            try {
                                parent = parent.parentNode;
                            } catch (e) {
                                parent = element;
                            }
                        }
                        if (parent === element) return;
                        handler.call(element, event);
                    };
                }
            } else {
                responder = function(event) {
                    Event.extend(event, element);
                    handler.call(element, event);
                };
            }
        }
        responder.handler = handler;
        respondersForEvent.push(responder);
        return responder;
    }

    function _destroyCache() {
        for (var i = 0, length = CACHE.length; i < length; i++) {
            Event.stopObserving(CACHE[i]);
            CACHE[i] = null;
        }
    }
    var CACHE = [];
    if (Prototype.Browser.IE)
        window.attachEvent('onunload', _destroyCache);
    if (Prototype.Browser.WebKit)
        window.addEventListener('unload', Prototype.emptyFunction, false);
    var _getDOMEventName = Prototype.K,
        translations = {
            mouseenter: "mouseover",
            mouseleave: "mouseout"
        };
    if (!MOUSEENTER_MOUSELEAVE_EVENTS_SUPPORTED) {
        _getDOMEventName = function(eventName) {
            return (translations[eventName] || eventName);
        };
    }

    function observe(element, eventName, handler) {
        element = $(element);
        var responder = _createResponder(element, eventName, handler);
        if (!responder) return element;
        if (eventName.include(':')) {
            if (element.addEventListener)
                element.addEventListener("dataavailable", responder, false);
            else {
                element.attachEvent("ondataavailable", responder);
                element.attachEvent("onlosecapture", responder);
            }
        } else {
            var actualEventName = _getDOMEventName(eventName);
            if (element.addEventListener)
                element.addEventListener(actualEventName, responder, false);
            else
                element.attachEvent("on" + actualEventName, responder);
        }
        return element;
    }

    function stopObserving(element, eventName, handler) {
        element = $(element);
        var registry = Element.retrieve(element, 'prototype_event_registry');
        if (!registry) return element;
        if (!eventName) {
            registry.each(function(pair) {
                var eventName = pair.key;
                stopObserving(element, eventName);
            });
            return element;
        }
        var responders = registry.get(eventName);
        if (!responders) return element;
        if (!handler) {
            responders.each(function(r) {
                stopObserving(element, eventName, r.handler);
            });
            return element;
        }
        var i = responders.length,
            responder;
        while (i--) {
            if (responders[i].handler === handler) {
                responder = responders[i];
                break;
            }
        }
        if (!responder) return element;
        if (eventName.include(':')) {
            if (element.removeEventListener)
                element.removeEventListener("dataavailable", responder, false);
            else {
                element.detachEvent("ondataavailable", responder);
                element.detachEvent("onlosecapture", responder);
            }
        } else {
            var actualEventName = _getDOMEventName(eventName);
            if (element.removeEventListener)
                element.removeEventListener(actualEventName, responder, false);
            else
                element.detachEvent('on' + actualEventName, responder);
        }
        registry.set(eventName, responders.without(responder));
        return element;
    }

    function fire(element, eventName, memo, bubble) {
        element = $(element);
        if (Object.isUndefined(bubble))
            bubble = true;
        if (element == document && document.createEvent && !element.dispatchEvent)
            element = document.documentElement;
        var event;
        if (document.createEvent) {
            event = document.createEvent('HTMLEvents');
            event.initEvent('dataavailable', bubble, true);
        } else {
            event = document.createEventObject();
            event.eventType = bubble ? 'ondataavailable' : 'onlosecapture';
        }
        event.eventName = eventName;
        event.memo = memo || {};
        if (document.createEvent)
            element.dispatchEvent(event);
        else
            element.fireEvent(event.eventType, event);
        return Event.extend(event);
    }
    Event.Handler = Class.create({
        initialize: function(element, eventName, selector, callback) {
            this.element = $(element);
            this.eventName = eventName;
            this.selector = selector;
            this.callback = callback;
            this.handler = this.handleEvent.bind(this);
        },
        start: function() {
            Event.observe(this.element, this.eventName, this.handler);
            return this;
        },
        stop: function() {
            Event.stopObserving(this.element, this.eventName, this.handler);
            return this;
        },
        handleEvent: function(event) {
            var element = Event.findElement(event, this.selector);
            if (element) this.callback.call(this.element, event, element);
        }
    });

    function on(element, eventName, selector, callback) {
        element = $(element);
        if (Object.isFunction(selector) && Object.isUndefined(callback)) {
            callback = selector, selector = null;
        }
        return new Event.Handler(element, eventName, selector, callback).start();
    }
    Object.extend(Event, Event.Methods);
    Object.extend(Event, {
        fire: fire,
        observe: observe,
        stopObserving: stopObserving,
        on: on
    });
    Element.addMethods({
        fire: fire,
        observe: observe,
        stopObserving: stopObserving,
        on: on
    });
    Object.extend(document, {
        fire: fire.methodize(),
        observe: observe.methodize(),
        stopObserving: stopObserving.methodize(),
        on: on.methodize(),
        loaded: false
    });
    if (window.Event) Object.extend(window.Event, Event);
    else window.Event = Event;
})();
(function() {
    var timer;

    function fireContentLoadedEvent() {
        if (document.loaded) return;
        if (timer) window.clearTimeout(timer);
        document.loaded = true;
        document.fire('dom:loaded');
    }

    function checkReadyState() {
        if (document.readyState === 'complete') {
            document.stopObserving('readystatechange', checkReadyState);
            fireContentLoadedEvent();
        }
    }

    function pollDoScroll() {
        try {
            document.documentElement.doScroll('left');
        } catch (e) {
            timer = pollDoScroll.defer();
            return;
        }
        fireContentLoadedEvent();
    }
    if (document.addEventListener) {
        document.addEventListener('DOMContentLoaded', fireContentLoadedEvent, false);
    } else {
        document.observe('readystatechange', checkReadyState);
        if (window == top)
            timer = pollDoScroll.defer();
    }
    Event.observe(window, 'load', fireContentLoadedEvent);
})();
Element.addMethods();
Hash.toQueryString = Object.toQueryString;
var Toggle = {
    display: Element.toggle
};
Element.Methods.childOf = Element.Methods.descendantOf;
var Insertion = {
    Before: function(element, content) {
        return Element.insert(element, {
            before: content
        });
    },
    Top: function(element, content) {
        return Element.insert(element, {
            top: content
        });
    },
    Bottom: function(element, content) {
        return Element.insert(element, {
            bottom: content
        });
    },
    After: function(element, content) {
        return Element.insert(element, {
            after: content
        });
    }
};
var $continue = new Error('"throw $continue" is deprecated, use "return" instead');
var Position = {
    includeScrollOffsets: false,
    prepare: function() {
        this.deltaX = window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft || 0;
        this.deltaY = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    },
    within: function(element, x, y) {
        if (this.includeScrollOffsets)
            return this.withinIncludingScrolloffsets(element, x, y);
        this.xcomp = x;
        this.ycomp = y;
        this.offset = Element.cumulativeOffset(element);
        return (y >= this.offset[1] && y < this.offset[1] + element.offsetHeight && x >= this.offset[0] && x < this.offset[0] + element.offsetWidth);
    },
    withinIncludingScrolloffsets: function(element, x, y) {
        var offsetcache = Element.cumulativeScrollOffset(element);
        this.xcomp = x + offsetcache[0] - this.deltaX;
        this.ycomp = y + offsetcache[1] - this.deltaY;
        this.offset = Element.cumulativeOffset(element);
        return (this.ycomp >= this.offset[1] && this.ycomp < this.offset[1] + element.offsetHeight && this.xcomp >= this.offset[0] && this.xcomp < this.offset[0] + element.offsetWidth);
    },
    overlap: function(mode, element) {
        if (!mode) return 0;
        if (mode == 'vertical')
            return ((this.offset[1] + element.offsetHeight) - this.ycomp) / element.offsetHeight;
        if (mode == 'horizontal')
            return ((this.offset[0] + element.offsetWidth) - this.xcomp) / element.offsetWidth;
    },
    cumulativeOffset: Element.Methods.cumulativeOffset,
    positionedOffset: Element.Methods.positionedOffset,
    absolutize: function(element) {
        Position.prepare();
        return Element.absolutize(element);
    },
    relativize: function(element) {
        Position.prepare();
        return Element.relativize(element);
    },
    realOffset: Element.Methods.cumulativeScrollOffset,
    offsetParent: Element.Methods.getOffsetParent,
    page: Element.Methods.viewportOffset,
    clone: function(source, target, options) {
        options = options || {};
        return Element.clonePosition(target, source, options);
    }
};
if (!document.getElementsByClassName) document.getElementsByClassName = function(instanceMethods) {
    function iter(name) {
        return name.blank() ? null : "[contains(concat(' ', @class, ' '), ' " + name + " ')]";
    }
    instanceMethods.getElementsByClassName = Prototype.BrowserFeatures.XPath ? function(element, className) {
        className = className.toString().strip();
        var cond = /\s/.test(className) ? $w(className).map(iter).join('') : iter(className);
        return cond ? document._getElementsByXPath('.//*' + cond, element) : [];
    } : function(element, className) {
        className = className.toString().strip();
        var elements = [],
            classNames = (/\s/.test(className) ? $w(className) : null);
        if (!classNames && !className) return elements;
        var nodes = $(element).getElementsByTagName('*');
        className = ' ' + className + ' ';
        for (var i = 0, child, cn; child = nodes[i]; i++) {
            if (child.className && (cn = ' ' + child.className + ' ') && (cn.include(className) || (classNames && classNames.all(function(name) {
                    return !name.toString().blank() && cn.include(' ' + name + ' ');
                }))))
                elements.push(Element.extend(child));
        }
        return elements;
    };
    return function(className, parentElement) {
        return $(parentElement || document.body).getElementsByClassName(className);
    };
}(Element.Methods);
Element.ClassNames = Class.create();
Element.ClassNames.prototype = {
    initialize: function(element) {
        this.element = $(element);
    },
    _each: function(iterator) {
        this.element.className.split(/\s+/).select(function(name) {
            return name.length > 0;
        })._each(iterator);
    },
    set: function(className) {
        this.element.className = className;
    },
    add: function(classNameToAdd) {
        if (this.include(classNameToAdd)) return;
        this.set($A(this).concat(classNameToAdd).join(' '));
    },
    remove: function(classNameToRemove) {
        if (!this.include(classNameToRemove)) return;
        this.set($A(this).without(classNameToRemove).join(' '));
    },
    toString: function() {
        return $A(this).join(' ');
    }
};
Object.extend(Element.ClassNames.prototype, Enumerable);
(function() {
    window.Selector = Class.create({
        initialize: function(expression) {
            this.expression = expression.strip();
        },
        findElements: function(rootElement) {
            return Prototype.Selector.select(this.expression, rootElement);
        },
        match: function(element) {
            return Prototype.Selector.match(element, this.expression);
        },
        toString: function() {
            return this.expression;
        },
        inspect: function() {
            return "#<Selector: " + this.expression + ">";
        }
    });
    Object.extend(Selector, {
        matchElements: function(elements, expression) {
            var match = Prototype.Selector.match,
                results = [];
            for (var i = 0, length = elements.length; i < length; i++) {
                var element = elements[i];
                if (match(element, expression)) {
                    results.push(Element.extend(element));
                }
            }
            return results;
        },
        findElement: function(elements, expression, index) {
            index = index || 0;
            var matchIndex = 0,
                element;
            for (var i = 0, length = elements.length; i < length; i++) {
                element = elements[i];
                if (Prototype.Selector.match(element, expression) && index === matchIndex++) {
                    return Element.extend(element);
                }
            }
        },
        findChildElements: function(element, expressions) {
            var selector = expressions.toArray().join(', ');
            return Prototype.Selector.select(selector, element || document);
        }
    });
})();

function validateCreditCard(s) {
    var v = "0123456789";
    var w = "";
    for (i = 0; i < s.length; i++) {
        x = s.charAt(i);
        if (v.indexOf(x, 0) != -1)
            w += x;
    }
    j = w.length / 2;
    k = Math.floor(j);
    m = Math.ceil(j) - k;
    c = 0;
    for (i = 0; i < k; i++) {
        a = w.charAt(i * 2 + m) * 2;
        c += a > 9 ? Math.floor(a / 10 + a % 10) : a;
    }
    for (i = 0; i < k + m; i++) c += w.charAt(i * 2 + 1 - m) * 1;
    return (c % 10 == 0);
}
var Validator = Class.create();
Validator.prototype = {
    initialize: function(className, error, test, options) {
        if (typeof test == 'function') {
            this.options = $H(options);
            this._test = test;
        } else {
            this.options = $H(test);
            this._test = function() {
                return true
            };
        }
        this.error = error || 'Validation failed.';
        this.className = className;
    },
    test: function(v, elm) {
        return (this._test(v, elm) && this.options.all(function(p) {
            return Validator.methods[p.key] ? Validator.methods[p.key](v, elm, p.value) : true;
        }));
    }
}
Validator.methods = {
    pattern: function(v, elm, opt) {
        return Validation.get('IsEmpty').test(v) || opt.test(v)
    },
    minLength: function(v, elm, opt) {
        return v.length >= opt
    },
    maxLength: function(v, elm, opt) {
        return v.length <= opt
    },
    min: function(v, elm, opt) {
        return v >= parseFloat(opt)
    },
    max: function(v, elm, opt) {
        return v <= parseFloat(opt)
    },
    notOneOf: function(v, elm, opt) {
        return $A(opt).all(function(value) {
            return v != value;
        })
    },
    oneOf: function(v, elm, opt) {
        return $A(opt).any(function(value) {
            return v == value;
        })
    },
    is: function(v, elm, opt) {
        return v == opt
    },
    isNot: function(v, elm, opt) {
        return v != opt
    },
    equalToField: function(v, elm, opt) {
        return v == $F(opt)
    },
    notEqualToField: function(v, elm, opt) {
        return v != $F(opt)
    },
    include: function(v, elm, opt) {
        return $A(opt).all(function(value) {
            return Validation.get(value).test(v, elm);
        })
    }
}
var Validation = Class.create();
Validation.defaultOptions = {
    onSubmit: true,
    stopOnFirst: false,
    immediate: false,
    focusOnError: true,
    useTitles: false,
    addClassNameToContainer: false,
    containerClassName: '.input-box',
    onFormValidate: function(result, form) {},
    onElementValidate: function(result, elm) {}
};
Validation.prototype = {
    initialize: function(form, options) {
        this.form = $(form);
        if (!this.form) {
            return;
        }
        this.options = Object.extend({
            onSubmit: Validation.defaultOptions.onSubmit,
            stopOnFirst: Validation.defaultOptions.stopOnFirst,
            immediate: Validation.defaultOptions.immediate,
            focusOnError: Validation.defaultOptions.focusOnError,
            useTitles: Validation.defaultOptions.useTitles,
            onFormValidate: Validation.defaultOptions.onFormValidate,
            onElementValidate: Validation.defaultOptions.onElementValidate
        }, options || {});
        if (this.options.onSubmit) Event.observe(this.form, 'submit', this.onSubmit.bind(this), false);
        if (this.options.immediate) {
            Form.getElements(this.form).each(function(input) {
                if (input.tagName.toLowerCase() == 'select') {
                    Event.observe(input, 'blur', this.onChange.bindAsEventListener(this));
                }
                if (input.type.toLowerCase() == 'radio' || input.type.toLowerCase() == 'checkbox') {
                    Event.observe(input, 'click', this.onChange.bindAsEventListener(this));
                } else {
                    Event.observe(input, 'change', this.onChange.bindAsEventListener(this));
                }
            }, this);
        }
    },
    onChange: function(ev) {
        Validation.isOnChange = true;
        Validation.validate(Event.element(ev), {
            useTitle: this.options.useTitles,
            onElementValidate: this.options.onElementValidate
        });
        Validation.isOnChange = false;
    },
    onSubmit: function(ev) {
        if (!this.validate()) Event.stop(ev);
    },
    validate: function() {
        var result = false;
        var useTitles = this.options.useTitles;
        var callback = this.options.onElementValidate;
        try {
            if (this.options.stopOnFirst) {
                result = Form.getElements(this.form).all(function(elm) {
                    if (elm.hasClassName('local-validation') && !this.isElementInForm(elm, this.form)) {
                        return true;
                    }
                    return Validation.validate(elm, {
                        useTitle: useTitles,
                        onElementValidate: callback
                    });
                }, this);
            } else {
                result = Form.getElements(this.form).collect(function(elm) {
                    if (elm.hasClassName('local-validation') && !this.isElementInForm(elm, this.form)) {
                        return true;
                    }
                    return Validation.validate(elm, {
                        useTitle: useTitles,
                        onElementValidate: callback
                    });
                }, this).all();
            }
        } catch (e) {}
        if (!result && this.options.focusOnError) {
            try {
                Form.getElements(this.form).findAll(function(elm) {
                    return $(elm).hasClassName('validation-failed')
                }).first().focus()
            } catch (e) {}
        }
        this.options.onFormValidate(result, this.form);
        return result;
    },
    reset: function() {
        Form.getElements(this.form).each(Validation.reset);
    },
    isElementInForm: function(elm, form) {
        var domForm = elm.up('form');
        if (domForm == form) {
            return true;
        }
        return false;
    }
}
Object.extend(Validation, {
    validate: function(elm, options) {
        options = Object.extend({
            useTitle: false,
            onElementValidate: function(result, elm) {}
        }, options || {});
        elm = $(elm);
        var cn = $w(elm.className);
        return result = cn.all(function(value) {
            var test = Validation.test(value, elm, options.useTitle);
            options.onElementValidate(test, elm);
            return test;
        });
    },
    insertAdvice: function(elm, advice) {
        var container = $(elm).up('.field-row');
        if (container) {
            Element.insert(container, {
                after: advice
            });
        } else if (elm.up('td.value')) {
            elm.up('td.value').insert({
                bottom: advice
            });
        } else if (elm.advaiceContainer && $(elm.advaiceContainer)) {
            $(elm.advaiceContainer).update(advice);
        } else {
            switch (elm.type.toLowerCase()) {
                case 'checkbox':
                case 'radio':
                    var p = elm.parentNode;
                    if (p) {
                        Element.insert(p, {
                            'bottom': advice
                        });
                    } else {
                        Element.insert(elm, {
                            'after': advice
                        });
                    }
                    break;
                default:
                    Element.insert(elm, {
                        'after': advice
                    });
            }
        }
    },
    showAdvice: function(elm, advice, adviceName) {
        if (!elm.advices) {
            elm.advices = new Hash();
        } else {
            elm.advices.each(function(pair) {
                if (!advice || pair.value.id != advice.id) {
                    this.hideAdvice(elm, pair.value);
                }
            }.bind(this));
        }
        elm.advices.set(adviceName, advice);
        if (typeof Effect == 'undefined') {
            advice.style.display = 'block';
        } else {
            if (!advice._adviceAbsolutize) {
                new Effect.Appear(advice, {
                    duration: 1
                });
            } else {
                Position.absolutize(advice);
                advice.show();
                advice.setStyle({
                    'top': advice._adviceTop,
                    'left': advice._adviceLeft,
                    'width': advice._adviceWidth,
                    'z-index': 1000
                });
                advice.addClassName('advice-absolute');
            }
        }
    },
    hideAdvice: function(elm, advice) {
        if (advice != null) {
            new Effect.Fade(advice, {
                duration: 1,
                afterFinishInternal: function() {
                    advice.hide();
                }
            });
        }
    },
    updateCallback: function(elm, status) {
        if (typeof elm.callbackFunction != 'undefined') {
            eval(elm.callbackFunction + '(\'' + elm.id + '\',\'' + status + '\')');
        }
    },
    ajaxError: function(elm, errorMsg) {
        var name = 'validate-ajax';
        var advice = Validation.getAdvice(name, elm);
        if (advice == null) {
            advice = this.createAdvice(name, elm, false, errorMsg);
        }
        this.showAdvice(elm, advice, 'validate-ajax');
        this.updateCallback(elm, 'failed');
        elm.addClassName('validation-failed');
        elm.addClassName('validate-ajax');
        if (Validation.defaultOptions.addClassNameToContainer && Validation.defaultOptions.containerClassName != '') {
            var container = elm.up(Validation.defaultOptions.containerClassName);
            if (container && this.allowContainerClassName(elm)) {
                container.removeClassName('validation-passed');
                container.addClassName('validation-error');
            }
        }
    },
    allowContainerClassName: function(elm) {
        if (elm.type == 'radio' || elm.type == 'checkbox') {
            return elm.hasClassName('change-container-classname');
        }
        return true;
    },
    test: function(name, elm, useTitle) {
        var v = Validation.get(name);
        var prop = '__advice' + name.camelize();
        try {
            if (Validation.isVisible(elm) && !v.test($F(elm), elm)) {
                var advice = Validation.getAdvice(name, elm);
                if (advice == null) {
                    advice = this.createAdvice(name, elm, useTitle);
                }
                this.showAdvice(elm, advice, name);
                this.updateCallback(elm, 'failed');
                elm[prop] = 1;
                if (!elm.advaiceContainer) {
                    elm.removeClassName('validation-passed');
                    elm.addClassName('validation-failed');
                }
                if (Validation.defaultOptions.addClassNameToContainer && Validation.defaultOptions.containerClassName != '') {
                    var container = elm.up(Validation.defaultOptions.containerClassName);
                    if (container && this.allowContainerClassName(elm)) {
                        container.removeClassName('validation-passed');
                        container.addClassName('validation-error');
                    }
                }
                return false;
            } else {
                var advice = Validation.getAdvice(name, elm);
                this.hideAdvice(elm, advice);
                this.updateCallback(elm, 'passed');
                elm[prop] = '';
                elm.removeClassName('validation-failed');
                elm.addClassName('validation-passed');
                if (Validation.defaultOptions.addClassNameToContainer && Validation.defaultOptions.containerClassName != '') {
                    var container = elm.up(Validation.defaultOptions.containerClassName);
                    if (container && !container.down('.validation-failed') && this.allowContainerClassName(elm)) {
                        if (!Validation.get('IsEmpty').test(elm.value) || !this.isVisible(elm)) {
                            container.addClassName('validation-passed');
                        } else {
                            container.removeClassName('validation-passed');
                        }
                        container.removeClassName('validation-error');
                    }
                }
                return true;
            }
        } catch (e) {
            throw (e)
        }
    },
    isVisible: function(elm) {
        while (elm.tagName != 'BODY') {
            if (!$(elm).visible()) return false;
            elm = elm.parentNode;
        }
        return true;
    },
    getAdvice: function(name, elm) {
        return $('advice-' + name + '-' + Validation.getElmID(elm)) || $('advice-' + Validation.getElmID(elm));
    },
    createAdvice: function(name, elm, useTitle, customError) {
        var v = Validation.get(name);
        var errorMsg = useTitle ? ((elm && elm.title) ? elm.title : v.error) : v.error;
        if (customError) {
            errorMsg = customError;
        }
        try {
            if (Translator) {
                errorMsg = Translator.translate(errorMsg);
            }
        } catch (e) {}
        advice = '<div class="validation-advice" id="advice-' + name + '-' + Validation.getElmID(elm) + '" style="display:none">' + errorMsg + '</div>'
        Validation.insertAdvice(elm, advice);
        advice = Validation.getAdvice(name, elm);
        if ($(elm).hasClassName('absolute-advice')) {
            var dimensions = $(elm).getDimensions();
            var originalPosition = Position.cumulativeOffset(elm);
            advice._adviceTop = (originalPosition[1] + dimensions.height) + 'px';
            advice._adviceLeft = (originalPosition[0]) + 'px';
            advice._adviceWidth = (dimensions.width) + 'px';
            advice._adviceAbsolutize = true;
        }
        return advice;
    },
    getElmID: function(elm) {
        return elm.id ? elm.id : elm.name;
    },
    reset: function(elm) {
        elm = $(elm);
        var cn = $w(elm.className);
        cn.each(function(value) {
            var prop = '__advice' + value.camelize();
            if (elm[prop]) {
                var advice = Validation.getAdvice(value, elm);
                if (advice) {
                    advice.hide();
                }
                elm[prop] = '';
            }
            elm.removeClassName('validation-failed');
            elm.removeClassName('validation-passed');
            if (Validation.defaultOptions.addClassNameToContainer && Validation.defaultOptions.containerClassName != '') {
                var container = elm.up(Validation.defaultOptions.containerClassName);
                if (container) {
                    container.removeClassName('validation-passed');
                    container.removeClassName('validation-error');
                }
            }
        });
    },
    add: function(className, error, test, options) {
        var nv = {};
        nv[className] = new Validator(className, error, test, options);
        Object.extend(Validation.methods, nv);
    },
    addAllThese: function(validators) {
        var nv = {};
        $A(validators).each(function(value) {
            nv[value[0]] = new Validator(value[0], value[1], value[2], (value.length > 3 ? value[3] : {}));
        });
        Object.extend(Validation.methods, nv);
    },
    get: function(name) {
        return Validation.methods[name] ? Validation.methods[name] : Validation.methods['_LikeNoIDIEverSaw_'];
    },
    methods: {
        '_LikeNoIDIEverSaw_': new Validator('_LikeNoIDIEverSaw_', '', {})
    }
});
Validation.add('IsEmpty', '', function(v) {
    return (v == '' || (v == null) || (v.length == 0) || /^\s+$/.test(v));
});
Validation.addAllThese([
    ['validate-no-html-tags', 'HTML tags are not allowed', function(v) {
        return !/<(\/)?\w+/.test(v);
    }],
    ['validate-select', 'Please select an option.', function(v) {
        return ((v != "none") && (v != null) && (v.length != 0));
    }],
    ['required-entry', 'This is a required field.', function(v) {
        return !Validation.get('IsEmpty').test(v);
    }],
    ['validate-number', 'Please enter a valid number in this field.', function(v) {
        return Validation.get('IsEmpty').test(v) || (!isNaN(parseNumber(v)) && /^\s*-?\d*(\.\d*)?\s*$/.test(v));
    }],
    ['validate-number-range', 'The value is not within the specified range.', function(v, elm) {
        if (Validation.get('IsEmpty').test(v)) {
            return true;
        }
        var numValue = parseNumber(v);
        if (isNaN(numValue)) {
            return false;
        }
        var reRange = /^number-range-(-?[\d.,]+)?-(-?[\d.,]+)?$/,
            result = true;
        $w(elm.className).each(function(name) {
            var m = reRange.exec(name);
            if (m) {
                result = result && (m[1] == null || m[1] == '' || numValue >= parseNumber(m[1])) && (m[2] == null || m[2] == '' || numValue <= parseNumber(m[2]));
            }
        });
        return result;
    }],
    ['validate-digits', 'Please use numbers only in this field. Please avoid spaces or other characters such as dots or commas.', function(v) {
        return Validation.get('IsEmpty').test(v) || !/[^\d]/.test(v);
    }],
    ['validate-digits-range', 'The value is not within the specified range.', function(v, elm) {
        if (Validation.get('IsEmpty').test(v)) {
            return true;
        }
        var numValue = parseNumber(v);
        if (isNaN(numValue)) {
            return false;
        }
        var reRange = /^digits-range-(-?\d+)?-(-?\d+)?$/,
            result = true;
        $w(elm.className).each(function(name) {
            var m = reRange.exec(name);
            if (m) {
                result = result && (m[1] == null || m[1] == '' || numValue >= parseNumber(m[1])) && (m[2] == null || m[2] == '' || numValue <= parseNumber(m[2]));
            }
        });
        return result;
    }],
    ['validate-alpha', 'Please use letters only (a-z or A-Z) in this field.', function(v) {
        return Validation.get('IsEmpty').test(v) || /^[a-zA-Z]+$/.test(v)
    }],
    ['validate-code', 'Please use only letters (a-z), numbers (0-9) or underscore(_) in this field, first character should be a letter.', function(v) {
        return Validation.get('IsEmpty').test(v) || /^[a-z]+[a-z0-9_]+$/.test(v)
    }],
    ['validate-alphanum', 'Please use only letters (a-z or A-Z) or numbers (0-9) only in this field. No spaces or other characters are allowed.', function(v) {
        return Validation.get('IsEmpty').test(v) || /^[a-zA-Z0-9]+$/.test(v)
    }],
    ['validate-alphanum-with-spaces', 'Please use only letters (a-z or A-Z), numbers (0-9) or spaces only in this field.', function(v) {
        return Validation.get('IsEmpty').test(v) || /^[a-zA-Z0-9 ]+$/.test(v)
    }],
    ['validate-street', 'Please use only letters (a-z or A-Z) or numbers (0-9) or spaces and # only in this field.', function(v) {
        return Validation.get('IsEmpty').test(v) || /^[ \w]{3,}([A-Za-z]\.)?([ \w]*\#\d+)?(\r\n| )[ \w]{3,}/.test(v)
    }],
    ['validate-phoneStrict', 'Please enter a valid phone number. For example (123) 456-7890 or 123-456-7890.', function(v) {
        return Validation.get('IsEmpty').test(v) || /^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/.test(v);
    }],
    ['validate-phoneLax', 'Please enter a valid phone number. For example (123) 456-7890 or 123-456-7890.', function(v) {
        return Validation.get('IsEmpty').test(v) || /^((\d[-. ]?)?((\(\d{3}\))|\d{3}))?[-. ]?\d{3}[-. ]?\d{4}$/.test(v);
    }],
    ['validate-fax', 'Please enter a valid fax number. For example (123) 456-7890 or 123-456-7890.', function(v) {
        return Validation.get('IsEmpty').test(v) || /^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/.test(v);
    }],
    ['validate-date', 'Please enter a valid date.', function(v) {
        var test = new Date(v);
        return Validation.get('IsEmpty').test(v) || !isNaN(test);
    }],
    ['validate-date-range', 'The From Date value should be less than or equal to the To Date value.', function(v, elm) {
        var m = /\bdate-range-(\w+)-(\w+)\b/.exec(elm.className);
        if (!m || m[2] == 'to' || Validation.get('IsEmpty').test(v)) {
            return true;
        }
        var currentYear = new Date().getFullYear() + '';
        var normalizedTime = function(v) {
            v = v.split(/[.\/]/);
            if (v[2] && v[2].length < 4) {
                v[2] = currentYear.substr(0, v[2].length) + v[2];
            }
            return new Date(v.join('/')).getTime();
        };
        var dependentElements = Element.select(elm.form, '.validate-date-range.date-range-' + m[1] + '-to');
        return !dependentElements.length || Validation.get('IsEmpty').test(dependentElements[0].value) || normalizedTime(v) <= normalizedTime(dependentElements[0].value);
    }],
    ['validate-email', 'Please enter a valid email address. For example johndoe@domain.com.', function(v) {
        return Validation.get('IsEmpty').test(v) || /^([a-z0-9,!\#\$%&'\*\+\/=\?\^_`\{\|\}~-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z0-9,!\#\$%&'\*\+\/=\?\^_`\{\|\}~-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*@([a-z0-9-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z0-9-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*\.(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]){2,})$/i.test(v)
    }],
    ['validate-emailSender', 'Please use only visible characters and spaces.', function(v) {
        return Validation.get('IsEmpty').test(v) || /^[\S ]+$/.test(v)
    }],
    ['validate-password', 'Please enter 6 or more characters. Leading or trailing spaces will be ignored.', function(v) {
        var pass = v.strip();
        return !(pass.length > 0 && pass.length < 6);
    }],
    ['validate-admin-password', 'Please enter 7 or more characters. Password should contain both numeric and alphabetic characters.', function(v) {
        var pass = v.strip();
        if (0 == pass.length) {
            return true;
        }
        if (!(/[a-z]/i.test(v)) || !(/[0-9]/.test(v))) {
            return false;
        }
        return !(pass.length < 7);
    }],
    ['validate-cpassword', 'Please make sure your passwords match.', function(v) {
        var conf = $('confirmation') ? $('confirmation') : $$('.validate-cpassword')[0];
        var pass = false;
        if ($('password')) {
            pass = $('password');
        }
        var passwordElements = $$('.validate-password');
        for (var i = 0; i < passwordElements.size(); i++) {
            var passwordElement = passwordElements[i];
            if (passwordElement.up('form').id == conf.up('form').id) {
                pass = passwordElement;
            }
        }
        if ($$('.validate-admin-password').size()) {
            pass = $$('.validate-admin-password')[0];
        }
        return (pass.value == conf.value);
    }],
    ['validate-both-passwords', 'Please make sure your passwords match.', function(v, input) {
        var dependentInput = $(input.form[input.name == 'password' ? 'confirmation' : 'password']),
            isEqualValues = input.value == dependentInput.value;
        if (isEqualValues && dependentInput.hasClassName('validation-failed')) {
            Validation.test(this.className, dependentInput);
        }
        return dependentInput.value == '' || isEqualValues;
    }],
    ['validate-url', 'Please enter a valid URL. Protocol is required (http://, https:// or ftp://)', function(v) {
        v = (v || '').replace(/^\s+/, '').replace(/\s+$/, '');
        return Validation.get('IsEmpty').test(v) || /^(http|https|ftp):\/\/(([A-Z0-9]([A-Z0-9_-]*[A-Z0-9]|))(\.[A-Z0-9]([A-Z0-9_-]*[A-Z0-9]|))*)(:(\d+))?(\/[A-Z0-9~](([A-Z0-9_~-]|\.)*[A-Z0-9~]|))*\/?(.*)?$/i.test(v)
    }],
    ['validate-clean-url', 'Please enter a valid URL. For example http://www.example.com or www.example.com', function(v) {
        return Validation.get('IsEmpty').test(v) || /^(http|https|ftp):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+.(com|org|net|dk|at|us|tv|info|uk|co.uk|biz|se)$)(:(\d+))?\/?/i.test(v) || /^(www)((\.[A-Z0-9][A-Z0-9_-]*)+.(com|org|net|dk|at|us|tv|info|uk|co.uk|biz|se)$)(:(\d+))?\/?/i.test(v)
    }],
    ['validate-identifier', 'Please enter a valid URL Key. For example "example-page", "example-page.html" or "anotherlevel/example-page".', function(v) {
        return Validation.get('IsEmpty').test(v) || /^[a-z0-9][a-z0-9_\/-]+(\.[a-z0-9_-]+)?$/.test(v)
    }],
    ['validate-xml-identifier', 'Please enter a valid XML-identifier. For example something_1, block5, id-4.', function(v) {
        return Validation.get('IsEmpty').test(v) || /^[A-Z][A-Z0-9_\/-]*$/i.test(v)
    }],
    ['validate-ssn', 'Please enter a valid social security number. For example 123-45-6789.', function(v) {
        return Validation.get('IsEmpty').test(v) || /^\d{3}-?\d{2}-?\d{4}$/.test(v);
    }],
    ['validate-zip', 'Please enter a valid zip code. For example 90602 or 90602-1234.', function(v) {
        return Validation.get('IsEmpty').test(v) || /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(v);
    }],
    ['validate-zip-international', 'Please enter a valid zip code.', function(v) {
        return true;
    }],
    ['validate-date-au', 'Please use this date format: dd/mm/yyyy. For example 17/03/2006 for the 17th of March, 2006.', function(v) {
        if (Validation.get('IsEmpty').test(v)) return true;
        var regex = /^(\d{2})\/(\d{2})\/(\d{4})$/;
        if (!regex.test(v)) return false;
        var d = new Date(v.replace(regex, '$2/$1/$3'));
        return (parseInt(RegExp.$2, 10) == (1 + d.getMonth())) && (parseInt(RegExp.$1, 10) == d.getDate()) && (parseInt(RegExp.$3, 10) == d.getFullYear());
    }],
    ['validate-currency-dollar', 'Please enter a valid $ amount. For example $100.00.', function(v) {
        return Validation.get('IsEmpty').test(v) || /^\$?\-?([1-9]{1}[0-9]{0,2}(\,[0-9]{3})*(\.[0-9]{0,2})?|[1-9]{1}\d*(\.[0-9]{0,2})?|0(\.[0-9]{0,2})?|(\.[0-9]{1,2})?)$/.test(v)
    }],
    ['validate-one-required', 'Please select one of the above options.', function(v, elm) {
        var p = elm.parentNode;
        var options = p.getElementsByTagName('INPUT');
        return $A(options).any(function(elm) {
            return $F(elm);
        });
    }],
    ['validate-one-required-by-name', 'Please select one of the options.', function(v, elm) {
        var inputs = $$('input[name="' + elm.name.replace(/([\\"])/g, '\\$1') + '"]');
        var error = 1;
        for (var i = 0; i < inputs.length; i++) {
            if ((inputs[i].type == 'checkbox' || inputs[i].type == 'radio') && inputs[i].checked == true) {
                error = 0;
            }
            if (Validation.isOnChange && (inputs[i].type == 'checkbox' || inputs[i].type == 'radio')) {
                Validation.reset(inputs[i]);
            }
        }
        if (error == 0) {
            return true;
        } else {
            return false;
        }
    }],
    ['validate-not-negative-number', 'Please enter a number 0 or greater in this field.', function(v) {
        if (Validation.get('IsEmpty').test(v)) {
            return true;
        }
        v = parseNumber(v);
        return !isNaN(v) && v >= 0;
    }],
    ['validate-zero-or-greater', 'Please enter a number 0 or greater in this field.', function(v) {
        return Validation.get('validate-not-negative-number').test(v);
    }],
    ['validate-greater-than-zero', 'Please enter a number greater than 0 in this field.', function(v) {
        if (Validation.get('IsEmpty').test(v)) {
            return true;
        }
        v = parseNumber(v);
        return !isNaN(v) && v > 0;
    }],
    ['validate-state', 'Please select State/Province.', function(v) {
        return (v != 0 || v == '');
    }],
    ['validate-new-password', 'Please enter 6 or more characters. Leading or trailing spaces will be ignored.', function(v) {
        if (!Validation.get('validate-password').test(v)) return false;
        if (Validation.get('IsEmpty').test(v) && v != '') return false;
        return true;
    }],
    ['validate-cc-number', 'Please enter a valid credit card number.', function(v, elm) {
        var ccTypeContainer = $(elm.id.substr(0, elm.id.indexOf('_cc_number')) + '_cc_type');
        if (ccTypeContainer && typeof Validation.creditCartTypes.get(ccTypeContainer.value) != 'undefined' && Validation.creditCartTypes.get(ccTypeContainer.value)[2] == false) {
            if (!Validation.get('IsEmpty').test(v) && Validation.get('validate-digits').test(v)) {
                return true;
            } else {
                return false;
            }
        }
        return validateCreditCard(v);
    }],
    ['validate-cc-type', 'Credit card number does not match credit card type.', function(v, elm) {
        elm.value = removeDelimiters(elm.value);
        v = removeDelimiters(v);
        var ccTypeContainer = $(elm.id.substr(0, elm.id.indexOf('_cc_number')) + '_cc_type');
        if (!ccTypeContainer) {
            return true;
        }
        var ccType = ccTypeContainer.value;
        if (typeof Validation.creditCartTypes.get(ccType) == 'undefined') {
            return false;
        }
        if (Validation.creditCartTypes.get(ccType)[0] == false) {
            return true;
        }
        var ccMatchedType = '';
        Validation.creditCartTypes.each(function(pair) {
            if (pair.value[0] && v.match(pair.value[0])) {
                ccMatchedType = pair.key;
                throw $break;
            }
        });
        if (ccMatchedType != ccType) {
            return false;
        }
        if (ccTypeContainer.hasClassName('validation-failed') && Validation.isOnChange) {
            Validation.validate(ccTypeContainer);
        }
        return true;
    }],
    ['validate-cc-type-select', 'Card type does not match credit card number.', function(v, elm) {
        var ccNumberContainer = $(elm.id.substr(0, elm.id.indexOf('_cc_type')) + '_cc_number');
        var ccTypeContainer = $(elm.id.substr(0, elm.id.indexOf('_cc_type')) + '_cc_cid');
        if (Validation.isOnChange && Validation.get('IsEmpty').test(ccNumberContainer.value)) {
            return true;
        }
        if (Validation.get('validate-cc-type').test(ccNumberContainer.value, ccNumberContainer)) {
            Validation.validate(ccNumberContainer);
            Validation.validate(ccTypeContainer);
        }
        return Validation.get('validate-cc-type').test(ccNumberContainer.value, ccNumberContainer);
    }],
    ['validate-cc-exp', 'Incorrect credit card expiration date.', function(v, elm) {
        var ccExpMonth = v;
        var ccExpYear = $(elm.id.substr(0, elm.id.indexOf('_expiration')) + '_expiration_yr').value;
        var currentTime = new Date();
        var currentMonth = currentTime.getMonth() + 1;
        var currentYear = currentTime.getFullYear();
        if (ccExpMonth < currentMonth && ccExpYear == currentYear) {
            return false;
        }
        return true;
    }],
    ['validate-cc-cvn', 'Please enter a valid credit card verification number.', function(v, elm) {
        var ccTypeContainer = $(elm.id.substr(0, elm.id.indexOf('_cc_cid')) + '_cc_type');
        if (!ccTypeContainer) {
            return true;
        }
        var ccType = ccTypeContainer.value;
        if (typeof Validation.creditCartTypes.get(ccType) == 'undefined') {
            return false;
        }
        var re = Validation.creditCartTypes.get(ccType)[1];
        if (v.match(re)) {
            return true;
        }
        return false;
    }],
    ['validate-ajax', '', function(v, elm) {
        return true;
    }],
    ['validate-data', 'Please use only letters (a-z or A-Z), numbers (0-9) or underscore(_) in this field, first character should be a letter.', function(v) {
        if (v != '' && v) {
            return /^[A-Za-z]+[A-Za-z0-9_]+$/.test(v);
        }
        return true;
    }],
    ['validate-css-length', 'Please input a valid CSS-length. For example 100px or 77pt or 20em or .5ex or 50%.', function(v) {
        if (v != '' && v) {
            return /^[0-9\.]+(px|pt|em|ex|%)?$/.test(v) && (!(/\..*\./.test(v))) && !(/\.$/.test(v));
        }
        return true;
    }],
    ['validate-length', 'Text length does not satisfy specified text range.', function(v, elm) {
        var reMax = new RegExp(/^maximum-length-[0-9]+$/);
        var reMin = new RegExp(/^minimum-length-[0-9]+$/);
        var result = true;
        $w(elm.className).each(function(name, index) {
            if (name.match(reMax) && result) {
                var length = name.split('-')[2];
                result = (v.length <= length);
            }
            if (name.match(reMin) && result && !Validation.get('IsEmpty').test(v)) {
                var length = name.split('-')[2];
                result = (v.length >= length);
            }
        });
        return result;
    }],
    ['validate-percents', 'Please enter a number lower than 100.', {
        max: 100
    }],
    ['required-file', 'Please select a file', function(v, elm) {
        var result = !Validation.get('IsEmpty').test(v);
        if (result === false) {
            ovId = elm.id + '_value';
            if ($(ovId)) {
                result = !Validation.get('IsEmpty').test($(ovId).value);
            }
        }
        return result;
    }],
    ['validate-cc-ukss', 'Please enter issue number or start date for switch/solo card type.', function(v, elm) {
        var endposition;
        if (elm.id.match(/(.)+_cc_issue$/)) {
            endposition = elm.id.indexOf('_cc_issue');
        } else if (elm.id.match(/(.)+_start_month$/)) {
            endposition = elm.id.indexOf('_start_month');
        } else {
            endposition = elm.id.indexOf('_start_year');
        }
        var prefix = elm.id.substr(0, endposition);
        var ccTypeContainer = $(prefix + '_cc_type');
        if (!ccTypeContainer) {
            return true;
        }
        var ccType = ccTypeContainer.value;
        if (['SS', 'SM', 'SO'].indexOf(ccType) == -1) {
            return true;
        }
        $(prefix + '_cc_issue').advaiceContainer = $(prefix + '_start_month').advaiceContainer = $(prefix + '_start_year').advaiceContainer = $(prefix + '_cc_type_ss_div').down('ul li.adv-container');
        var ccIssue = $(prefix + '_cc_issue').value;
        var ccSMonth = $(prefix + '_start_month').value;
        var ccSYear = $(prefix + '_start_year').value;
        var ccStartDatePresent = (ccSMonth && ccSYear) ? true : false;
        if (!ccStartDatePresent && !ccIssue) {
            return false;
        }
        return true;
    }]
]);

function removeDelimiters(v) {
    v = v.replace(/\s/g, '');
    v = v.replace(/\-/g, '');
    return v;
}

function parseNumber(v) {
    if (typeof v != 'string') {
        return parseFloat(v);
    }
    var isDot = v.indexOf('.');
    var isComa = v.indexOf(',');
    if (isDot != -1 && isComa != -1) {
        if (isComa > isDot) {
            v = v.replace('.', '').replace(',', '.');
        } else {
            v = v.replace(',', '');
        }
    } else if (isComa != -1) {
        v = v.replace(',', '.');
    }
    return parseFloat(v);
}
Validation.creditCartTypes = $H({
    'SO': [new RegExp('^(6334[5-9]([0-9]{11}|[0-9]{13,14}))|(6767([0-9]{12}|[0-9]{14,15}))$'), new RegExp('^([0-9]{3}|[0-9]{4})?$'), true],
    'SM': [new RegExp('(^(5[0678])[0-9]{11,18}$)|(^(6[^05])[0-9]{11,18}$)|(^(601)[^1][0-9]{9,16}$)|(^(6011)[0-9]{9,11}$)|(^(6011)[0-9]{13,16}$)|(^(65)[0-9]{11,13}$)|(^(65)[0-9]{15,18}$)|(^(49030)[2-9]([0-9]{10}$|[0-9]{12,13}$))|(^(49033)[5-9]([0-9]{10}$|[0-9]{12,13}$))|(^(49110)[1-2]([0-9]{10}$|[0-9]{12,13}$))|(^(49117)[4-9]([0-9]{10}$|[0-9]{12,13}$))|(^(49118)[0-2]([0-9]{10}$|[0-9]{12,13}$))|(^(4936)([0-9]{12}$|[0-9]{14,15}$))'), new RegExp('^([0-9]{3}|[0-9]{4})?$'), true],
    'VI': [new RegExp('^4[0-9]{12}([0-9]{3})?$'), new RegExp('^[0-9]{3}$'), true],
    'MC': [new RegExp('^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$'), new RegExp('^[0-9]{3}$'), true],
    'AE': [new RegExp('^3[47][0-9]{13}$'), new RegExp('^[0-9]{4}$'), true],
    'DI': [new RegExp('^6011[0-9]{12}$'), new RegExp('^[0-9]{3}$'), true],
    'JCB': [new RegExp('^(3[0-9]{15}|(2131|1800)[0-9]{11})$'), new RegExp('^[0-9]{3,4}$'), true],
    'OT': [false, new RegExp('^([0-9]{3}|[0-9]{4})?$'), false]
});
var Builder = {
    NODEMAP: {
        AREA: 'map',
        CAPTION: 'table',
        COL: 'table',
        COLGROUP: 'table',
        LEGEND: 'fieldset',
        OPTGROUP: 'select',
        OPTION: 'select',
        PARAM: 'object',
        TBODY: 'table',
        TD: 'table',
        TFOOT: 'table',
        TH: 'table',
        THEAD: 'table',
        TR: 'table'
    },
    node: function(elementName) {
        elementName = elementName.toUpperCase();
        var parentTag = this.NODEMAP[elementName] || 'div';
        var parentElement = document.createElement(parentTag);
        try {
            parentElement.innerHTML = "<" + elementName + "></" + elementName + ">";
        } catch (e) {}
        var element = parentElement.firstChild || null;
        if (element && (element.tagName.toUpperCase() != elementName))
            element = element.getElementsByTagName(elementName)[0];
        if (!element) element = document.createElement(elementName);
        if (!element) return;
        if (arguments[1])
            if (this._isStringOrNumber(arguments[1]) || (arguments[1] instanceof Array) || arguments[1].tagName) {
                this._children(element, arguments[1]);
            } else {
                var attrs = this._attributes(arguments[1]);
                if (attrs.length) {
                    try {
                        parentElement.innerHTML = "<" + elementName + " " +
                            attrs + "></" + elementName + ">";
                    } catch (e) {}
                    element = parentElement.firstChild || null;
                    if (!element) {
                        element = document.createElement(elementName);
                        for (attr in arguments[1])
                            element[attr == 'class' ? 'className' : attr] = arguments[1][attr];
                    }
                    if (element.tagName.toUpperCase() != elementName)
                        element = parentElement.getElementsByTagName(elementName)[0];
                }
            }
        if (arguments[2])
            this._children(element, arguments[2]);
        return $(element);
    },
    _text: function(text) {
        return document.createTextNode(text);
    },
    ATTR_MAP: {
        'className': 'class',
        'htmlFor': 'for'
    },
    _attributes: function(attributes) {
        var attrs = [];
        for (attribute in attributes)
            attrs.push((attribute in this.ATTR_MAP ? this.ATTR_MAP[attribute] : attribute) +
                '="' + attributes[attribute].toString().escapeHTML().gsub(/"/, '&quot;') + '"');
        return attrs.join(" ");
    },
    _children: function(element, children) {
        if (children.tagName) {
            element.appendChild(children);
            return;
        }
        if (typeof children == 'object') {
            children.flatten().each(function(e) {
                if (typeof e == 'object')
                    element.appendChild(e);
                else
                if (Builder._isStringOrNumber(e))
                    element.appendChild(Builder._text(e));
            });
        } else
        if (Builder._isStringOrNumber(children))
            element.appendChild(Builder._text(children));
    },
    _isStringOrNumber: function(param) {
        return (typeof param == 'string' || typeof param == 'number');
    },
    build: function(html) {
        var element = this.node('div');
        $(element).update(html.strip());
        return element.down();
    },
    dump: function(scope) {
        if (typeof scope != 'object' && typeof scope != 'function') scope = window;
        var tags = ("A ABBR ACRONYM ADDRESS APPLET AREA B BASE BASEFONT BDO BIG BLOCKQUOTE BODY " +
            "BR BUTTON CAPTION CENTER CITE CODE COL COLGROUP DD DEL DFN DIR DIV DL DT EM FIELDSET " +
            "FONT FORM FRAME FRAMESET H1 H2 H3 H4 H5 H6 HEAD HR HTML I IFRAME IMG INPUT INS ISINDEX " +
            "KBD LABEL LEGEND LI LINK MAP MENU META NOFRAMES NOSCRIPT OBJECT OL OPTGROUP OPTION P " +
            "PARAM PRE Q S SAMP SCRIPT SELECT SMALL SPAN STRIKE STRONG STYLE SUB SUP TABLE TBODY TD " +
            "TEXTAREA TFOOT TH THEAD TITLE TR TT U UL VAR").split(/\s+/);
        tags.each(function(tag) {
            scope[tag] = function() {
                return Builder.node.apply(Builder, [tag].concat($A(arguments)));
            };
        });
    }
};
String.prototype.parseColor = function() {
    var color = '#';
    if (this.slice(0, 4) == 'rgb(') {
        var cols = this.slice(4, this.length - 1).split(',');
        var i = 0;
        do {
            color += parseInt(cols[i]).toColorPart()
        } while (++i < 3);
    } else {
        if (this.slice(0, 1) == '#') {
            if (this.length == 4)
                for (var i = 1; i < 4; i++) color += (this.charAt(i) + this.charAt(i)).toLowerCase();
            if (this.length == 7) color = this.toLowerCase();
        }
    }
    return (color.length == 7 ? color : (arguments[0] || this));
};
Element.collectTextNodes = function(element) {
    return $A($(element).childNodes).collect(function(node) {
        return (node.nodeType == 3 ? node.nodeValue : (node.hasChildNodes() ? Element.collectTextNodes(node) : ''));
    }).flatten().join('');
};
Element.collectTextNodesIgnoreClass = function(element, className) {
    return $A($(element).childNodes).collect(function(node) {
        return (node.nodeType == 3 ? node.nodeValue : ((node.hasChildNodes() && !Element.hasClassName(node, className)) ? Element.collectTextNodesIgnoreClass(node, className) : ''));
    }).flatten().join('');
};
Element.setContentZoom = function(element, percent) {
    element = $(element);
    element.setStyle({
        fontSize: (percent / 100) + 'em'
    });
    if (Prototype.Browser.WebKit) window.scrollBy(0, 0);
    return element;
};
Element.getInlineOpacity = function(element) {
    return $(element).style.opacity || '';
};
Element.forceRerendering = function(element) {
    try {
        element = $(element);
        var n = document.createTextNode(' ');
        element.appendChild(n);
        element.removeChild(n);
    } catch (e) {}
};
var Effect = {
    _elementDoesNotExistError: {
        name: 'ElementDoesNotExistError',
        message: 'The specified DOM element does not exist, but is required for this effect to operate'
    },
    Transitions: {
        linear: Prototype.K,
        sinoidal: function(pos) {
            return (-Math.cos(pos * Math.PI) / 2) + .5;
        },
        reverse: function(pos) {
            return 1 - pos;
        },
        flicker: function(pos) {
            var pos = ((-Math.cos(pos * Math.PI) / 4) + .75) + Math.random() / 4;
            return pos > 1 ? 1 : pos;
        },
        wobble: function(pos) {
            return (-Math.cos(pos * Math.PI * (9 * pos)) / 2) + .5;
        },
        pulse: function(pos, pulses) {
            return (-Math.cos((pos * ((pulses || 5) - .5) * 2) * Math.PI) / 2) + .5;
        },
        spring: function(pos) {
            return 1 - (Math.cos(pos * 4.5 * Math.PI) * Math.exp(-pos * 6));
        },
        none: function(pos) {
            return 0;
        },
        full: function(pos) {
            return 1;
        }
    },
    DefaultOptions: {
        duration: 1.0,
        fps: 100,
        sync: false,
        from: 0.0,
        to: 1.0,
        delay: 0.0,
        queue: 'parallel'
    },
    tagifyText: function(element) {
        var tagifyStyle = 'position:relative';
        if (Prototype.Browser.IE) tagifyStyle += ';zoom:1';
        element = $(element);
        $A(element.childNodes).each(function(child) {
            if (child.nodeType == 3) {
                child.nodeValue.toArray().each(function(character) {
                    element.insertBefore(new Element('span', {
                        style: tagifyStyle
                    }).update(character == ' ' ? String.fromCharCode(160) : character), child);
                });
                Element.remove(child);
            }
        });
    },
    multiple: function(element, effect) {
        var elements;
        if (((typeof element == 'object') || Object.isFunction(element)) && (element.length))
            elements = element;
        else
            elements = $(element).childNodes;
        var options = Object.extend({
            speed: 0.1,
            delay: 0.0
        }, arguments[2] || {});
        var masterDelay = options.delay;
        $A(elements).each(function(element, index) {
            new effect(element, Object.extend(options, {
                delay: index * options.speed + masterDelay
            }));
        });
    },
    PAIRS: {
        'slide': ['SlideDown', 'SlideUp'],
        'blind': ['BlindDown', 'BlindUp'],
        'appear': ['Appear', 'Fade']
    },
    toggle: function(element, effect) {
        element = $(element);
        effect = (effect || 'appear').toLowerCase();
        var options = Object.extend({
            queue: {
                position: 'end',
                scope: (element.id || 'global'),
                limit: 1
            }
        }, arguments[2] || {});
        Effect[element.visible() ? Effect.PAIRS[effect][1] : Effect.PAIRS[effect][0]](element, options);
    }
};
Effect.DefaultOptions.transition = Effect.Transitions.sinoidal;
Effect.ScopedQueue = Class.create(Enumerable, {
    initialize: function() {
        this.effects = [];
        this.interval = null;
    },
    _each: function(iterator) {
        this.effects._each(iterator);
    },
    add: function(effect) {
        var timestamp = new Date().getTime();
        var position = Object.isString(effect.options.queue) ? effect.options.queue : effect.options.queue.position;
        switch (position) {
            case 'front':
                this.effects.findAll(function(e) {
                    return e.state == 'idle'
                }).each(function(e) {
                    e.startOn += effect.finishOn;
                    e.finishOn += effect.finishOn;
                });
                break;
            case 'with-last':
                timestamp = this.effects.pluck('startOn').max() || timestamp;
                break;
            case 'end':
                timestamp = this.effects.pluck('finishOn').max() || timestamp;
                break;
        }
        effect.startOn += timestamp;
        effect.finishOn += timestamp;
        if (!effect.options.queue.limit || (this.effects.length < effect.options.queue.limit))
            this.effects.push(effect);
        if (!this.interval)
            this.interval = setInterval(this.loop.bind(this), 15);
    },
    remove: function(effect) {
        this.effects = this.effects.reject(function(e) {
            return e == effect
        });
        if (this.effects.length == 0) {
            clearInterval(this.interval);
            this.interval = null;
        }
    },
    loop: function() {
        var timePos = new Date().getTime();
        for (var i = 0, len = this.effects.length; i < len; i++)
            this.effects[i] && this.effects[i].loop(timePos);
    }
});
Effect.Queues = {
    instances: $H(),
    get: function(queueName) {
        if (!Object.isString(queueName)) return queueName;
        return this.instances.get(queueName) || this.instances.set(queueName, new Effect.ScopedQueue());
    }
};
Effect.Queue = Effect.Queues.get('global');
Effect.Base = Class.create({
    position: null,
    start: function(options) {
        function codeForEvent(options, eventName) {
            return ((options[eventName + 'Internal'] ? 'this.options.' + eventName + 'Internal(this);' : '') +
                (options[eventName] ? 'this.options.' + eventName + '(this);' : ''));
        }
        if (options && options.transition === false) options.transition = Effect.Transitions.linear;
        this.options = Object.extend(Object.extend({}, Effect.DefaultOptions), options || {});
        this.currentFrame = 0;
        this.state = 'idle';
        this.startOn = this.options.delay * 1000;
        this.finishOn = this.startOn + (this.options.duration * 1000);
        this.fromToDelta = this.options.to - this.options.from;
        this.totalTime = this.finishOn - this.startOn;
        this.totalFrames = this.options.fps * this.options.duration;
        this.render = (function() {
            function dispatch(effect, eventName) {
                if (effect.options[eventName + 'Internal'])
                    effect.options[eventName + 'Internal'](effect);
                if (effect.options[eventName])
                    effect.options[eventName](effect);
            }
            return function(pos) {
                if (this.state === "idle") {
                    this.state = "running";
                    dispatch(this, 'beforeSetup');
                    if (this.setup) this.setup();
                    dispatch(this, 'afterSetup');
                }
                if (this.state === "running") {
                    pos = (this.options.transition(pos) * this.fromToDelta) + this.options.from;
                    this.position = pos;
                    dispatch(this, 'beforeUpdate');
                    if (this.update) this.update(pos);
                    dispatch(this, 'afterUpdate');
                }
            };
        })();
        this.event('beforeStart');
        if (!this.options.sync)
            Effect.Queues.get(Object.isString(this.options.queue) ? 'global' : this.options.queue.scope).add(this);
    },
    loop: function(timePos) {
        if (timePos >= this.startOn) {
            if (timePos >= this.finishOn) {
                this.render(1.0);
                this.cancel();
                this.event('beforeFinish');
                if (this.finish) this.finish();
                this.event('afterFinish');
                return;
            }
            var pos = (timePos - this.startOn) / this.totalTime,
                frame = (pos * this.totalFrames).round();
            if (frame > this.currentFrame) {
                this.render(pos);
                this.currentFrame = frame;
            }
        }
    },
    cancel: function() {
        if (!this.options.sync)
            Effect.Queues.get(Object.isString(this.options.queue) ? 'global' : this.options.queue.scope).remove(this);
        this.state = 'finished';
    },
    event: function(eventName) {
        if (this.options[eventName + 'Internal']) this.options[eventName + 'Internal'](this);
        if (this.options[eventName]) this.options[eventName](this);
    },
    inspect: function() {
        var data = $H();
        for (property in this)
            if (!Object.isFunction(this[property])) data.set(property, this[property]);
        return '#<Effect:' + data.inspect() + ',options:' + $H(this.options).inspect() + '>';
    }
});
Effect.Parallel = Class.create(Effect.Base, {
    initialize: function(effects) {
        this.effects = effects || [];
        this.start(arguments[1]);
    },
    update: function(position) {
        this.effects.invoke('render', position);
    },
    finish: function(position) {
        this.effects.each(function(effect) {
            effect.render(1.0);
            effect.cancel();
            effect.event('beforeFinish');
            if (effect.finish) effect.finish(position);
            effect.event('afterFinish');
        });
    }
});
Effect.Tween = Class.create(Effect.Base, {
    initialize: function(object, from, to) {
        object = Object.isString(object) ? $(object) : object;
        var args = $A(arguments),
            method = args.last(),
            options = args.length == 5 ? args[3] : null;
        this.method = Object.isFunction(method) ? method.bind(object) : Object.isFunction(object[method]) ? object[method].bind(object) : function(value) {
            object[method] = value
        };
        this.start(Object.extend({
            from: from,
            to: to
        }, options || {}));
    },
    update: function(position) {
        this.method(position);
    }
});
Effect.Event = Class.create(Effect.Base, {
    initialize: function() {
        this.start(Object.extend({
            duration: 0
        }, arguments[0] || {}));
    },
    update: Prototype.emptyFunction
});
Effect.Opacity = Class.create(Effect.Base, {
    initialize: function(element) {
        this.element = $(element);
        if (!this.element) throw (Effect._elementDoesNotExistError);
        if (Prototype.Browser.IE && (!this.element.currentStyle.hasLayout))
            this.element.setStyle({
                zoom: 1
            });
        var options = Object.extend({
            from: this.element.getOpacity() || 0.0,
            to: 1.0
        }, arguments[1] || {});
        this.start(options);
    },
    update: function(position) {
        this.element.setOpacity(position);
    }
});
Effect.Move = Class.create(Effect.Base, {
    initialize: function(element) {
        this.element = $(element);
        if (!this.element) throw (Effect._elementDoesNotExistError);
        var options = Object.extend({
            x: 0,
            y: 0,
            mode: 'relative'
        }, arguments[1] || {});
        this.start(options);
    },
    setup: function() {
        this.element.makePositioned();
        this.originalLeft = parseFloat(this.element.getStyle('left') || '0');
        this.originalTop = parseFloat(this.element.getStyle('top') || '0');
        if (this.options.mode == 'absolute') {
            this.options.x = this.options.x - this.originalLeft;
            this.options.y = this.options.y - this.originalTop;
        }
    },
    update: function(position) {
        this.element.setStyle({
            left: (this.options.x * position + this.originalLeft).round() + 'px',
            top: (this.options.y * position + this.originalTop).round() + 'px'
        });
    }
});
Effect.MoveBy = function(element, toTop, toLeft) {
    return new Effect.Move(element, Object.extend({
        x: toLeft,
        y: toTop
    }, arguments[3] || {}));
};
Effect.Scale = Class.create(Effect.Base, {
    initialize: function(element, percent) {
        this.element = $(element);
        if (!this.element) throw (Effect._elementDoesNotExistError);
        var options = Object.extend({
            scaleX: true,
            scaleY: true,
            scaleContent: true,
            scaleFromCenter: false,
            scaleMode: 'box',
            scaleFrom: 100.0,
            scaleTo: percent
        }, arguments[2] || {});
        this.start(options);
    },
    setup: function() {
        this.restoreAfterFinish = this.options.restoreAfterFinish || false;
        this.elementPositioning = this.element.getStyle('position');
        this.originalStyle = {};
        ['top', 'left', 'width', 'height', 'fontSize'].each(function(k) {
            this.originalStyle[k] = this.element.style[k];
        }.bind(this));
        this.originalTop = this.element.offsetTop;
        this.originalLeft = this.element.offsetLeft;
        var fontSize = this.element.getStyle('font-size') || '100%';
        ['em', 'px', '%', 'pt'].each(function(fontSizeType) {
            if (fontSize.indexOf(fontSizeType) > 0) {
                this.fontSize = parseFloat(fontSize);
                this.fontSizeType = fontSizeType;
            }
        }.bind(this));
        this.factor = (this.options.scaleTo - this.options.scaleFrom) / 100;
        this.dims = null;
        if (this.options.scaleMode == 'box')
            this.dims = [this.element.offsetHeight, this.element.offsetWidth];
        if (/^content/.test(this.options.scaleMode))
            this.dims = [this.element.scrollHeight, this.element.scrollWidth];
        if (!this.dims)
            this.dims = [this.options.scaleMode.originalHeight, this.options.scaleMode.originalWidth];
    },
    update: function(position) {
        var currentScale = (this.options.scaleFrom / 100.0) + (this.factor * position);
        if (this.options.scaleContent && this.fontSize)
            this.element.setStyle({
                fontSize: this.fontSize * currentScale + this.fontSizeType
            });
        this.setDimensions(this.dims[0] * currentScale, this.dims[1] * currentScale);
    },
    finish: function(position) {
        if (this.restoreAfterFinish) this.element.setStyle(this.originalStyle);
    },
    setDimensions: function(height, width) {
        var d = {};
        if (this.options.scaleX) d.width = width.round() + 'px';
        if (this.options.scaleY) d.height = height.round() + 'px';
        if (this.options.scaleFromCenter) {
            var topd = (height - this.dims[0]) / 2;
            var leftd = (width - this.dims[1]) / 2;
            if (this.elementPositioning == 'absolute') {
                if (this.options.scaleY) d.top = this.originalTop - topd + 'px';
                if (this.options.scaleX) d.left = this.originalLeft - leftd + 'px';
            } else {
                if (this.options.scaleY) d.top = -topd + 'px';
                if (this.options.scaleX) d.left = -leftd + 'px';
            }
        }
        this.element.setStyle(d);
    }
});
Effect.Highlight = Class.create(Effect.Base, {
    initialize: function(element) {
        this.element = $(element);
        if (!this.element) throw (Effect._elementDoesNotExistError);
        var options = Object.extend({
            startcolor: '#ffff99'
        }, arguments[1] || {});
        this.start(options);
    },
    setup: function() {
        if (this.element.getStyle('display') == 'none') {
            this.cancel();
            return;
        }
        this.oldStyle = {};
        if (!this.options.keepBackgroundImage) {
            this.oldStyle.backgroundImage = this.element.getStyle('background-image');
            this.element.setStyle({
                backgroundImage: 'none'
            });
        }
        if (!this.options.endcolor)
            this.options.endcolor = this.element.getStyle('background-color').parseColor('#ffffff');
        if (!this.options.restorecolor)
            this.options.restorecolor = this.element.getStyle('background-color');
        this._base = $R(0, 2).map(function(i) {
            return parseInt(this.options.startcolor.slice(i * 2 + 1, i * 2 + 3), 16)
        }.bind(this));
        this._delta = $R(0, 2).map(function(i) {
            return parseInt(this.options.endcolor.slice(i * 2 + 1, i * 2 + 3), 16) - this._base[i]
        }.bind(this));
    },
    update: function(position) {
        this.element.setStyle({
            backgroundColor: $R(0, 2).inject('#', function(m, v, i) {
                return m + ((this._base[i] + (this._delta[i] * position)).round().toColorPart());
            }.bind(this))
        });
    },
    finish: function() {
        this.element.setStyle(Object.extend(this.oldStyle, {
            backgroundColor: this.options.restorecolor
        }));
    }
});
Effect.ScrollTo = function(element) {
    var options = arguments[1] || {},
        scrollOffsets = document.viewport.getScrollOffsets(),
        elementOffsets = $(element).cumulativeOffset();
    if (options.offset) elementOffsets[1] += options.offset;
    return new Effect.Tween(null, scrollOffsets.top, elementOffsets[1], options, function(p) {
        scrollTo(scrollOffsets.left, p.round());
    });
};
Effect.Fade = function(element) {
    element = $(element);
    var oldOpacity = element.getInlineOpacity();
    var options = Object.extend({
        from: element.getOpacity() || 1.0,
        to: 0.0,
        afterFinishInternal: function(effect) {
            if (effect.options.to != 0) return;
            effect.element.hide().setStyle({
                opacity: oldOpacity
            });
        }
    }, arguments[1] || {});
    return new Effect.Opacity(element, options);
};
Effect.Appear = function(element) {
    element = $(element);
    var options = Object.extend({
        from: (element.getStyle('display') == 'none' ? 0.0 : element.getOpacity() || 0.0),
        to: 1.0,
        afterFinishInternal: function(effect) {
            effect.element.forceRerendering();
        },
        beforeSetup: function(effect) {
            effect.element.setOpacity(effect.options.from).show();
        }
    }, arguments[1] || {});
    return new Effect.Opacity(element, options);
};
Effect.Puff = function(element) {
    element = $(element);
    var oldStyle = {
        opacity: element.getInlineOpacity(),
        position: element.getStyle('position'),
        top: element.style.top,
        left: element.style.left,
        width: element.style.width,
        height: element.style.height
    };
    return new Effect.Parallel([new Effect.Scale(element, 200, {
        sync: true,
        scaleFromCenter: true,
        scaleContent: true,
        restoreAfterFinish: true
    }), new Effect.Opacity(element, {
        sync: true,
        to: 0.0
    })], Object.extend({
        duration: 1.0,
        beforeSetupInternal: function(effect) {
            Position.absolutize(effect.effects[0].element);
        },
        afterFinishInternal: function(effect) {
            effect.effects[0].element.hide().setStyle(oldStyle);
        }
    }, arguments[1] || {}));
};
Effect.BlindUp = function(element) {
    element = $(element);
    element.makeClipping();
    return new Effect.Scale(element, 0, Object.extend({
        scaleContent: false,
        scaleX: false,
        restoreAfterFinish: true,
        afterFinishInternal: function(effect) {
            effect.element.hide().undoClipping();
        }
    }, arguments[1] || {}));
};
Effect.BlindDown = function(element) {
    element = $(element);
    var elementDimensions = element.getDimensions();
    return new Effect.Scale(element, 100, Object.extend({
        scaleContent: false,
        scaleX: false,
        scaleFrom: 0,
        scaleMode: {
            originalHeight: elementDimensions.height,
            originalWidth: elementDimensions.width
        },
        restoreAfterFinish: true,
        afterSetup: function(effect) {
            effect.element.makeClipping().setStyle({
                height: '0px'
            }).show();
        },
        afterFinishInternal: function(effect) {
            effect.element.undoClipping();
        }
    }, arguments[1] || {}));
};
Effect.SwitchOff = function(element) {
    element = $(element);
    var oldOpacity = element.getInlineOpacity();
    return new Effect.Appear(element, Object.extend({
        duration: 0.4,
        from: 0,
        transition: Effect.Transitions.flicker,
        afterFinishInternal: function(effect) {
            new Effect.Scale(effect.element, 1, {
                duration: 0.3,
                scaleFromCenter: true,
                scaleX: false,
                scaleContent: false,
                restoreAfterFinish: true,
                beforeSetup: function(effect) {
                    effect.element.makePositioned().makeClipping();
                },
                afterFinishInternal: function(effect) {
                    effect.element.hide().undoClipping().undoPositioned().setStyle({
                        opacity: oldOpacity
                    });
                }
            });
        }
    }, arguments[1] || {}));
};
Effect.DropOut = function(element) {
    element = $(element);
    var oldStyle = {
        top: element.getStyle('top'),
        left: element.getStyle('left'),
        opacity: element.getInlineOpacity()
    };
    return new Effect.Parallel([new Effect.Move(element, {
        x: 0,
        y: 100,
        sync: true
    }), new Effect.Opacity(element, {
        sync: true,
        to: 0.0
    })], Object.extend({
        duration: 0.5,
        beforeSetup: function(effect) {
            effect.effects[0].element.makePositioned();
        },
        afterFinishInternal: function(effect) {
            effect.effects[0].element.hide().undoPositioned().setStyle(oldStyle);
        }
    }, arguments[1] || {}));
};
Effect.Shake = function(element) {
    element = $(element);
    var options = Object.extend({
        distance: 20,
        duration: 0.5
    }, arguments[1] || {});
    var distance = parseFloat(options.distance);
    var split = parseFloat(options.duration) / 10.0;
    var oldStyle = {
        top: element.getStyle('top'),
        left: element.getStyle('left')
    };
    return new Effect.Move(element, {
        x: distance,
        y: 0,
        duration: split,
        afterFinishInternal: function(effect) {
            new Effect.Move(effect.element, {
                x: -distance * 2,
                y: 0,
                duration: split * 2,
                afterFinishInternal: function(effect) {
                    new Effect.Move(effect.element, {
                        x: distance * 2,
                        y: 0,
                        duration: split * 2,
                        afterFinishInternal: function(effect) {
                            new Effect.Move(effect.element, {
                                x: -distance * 2,
                                y: 0,
                                duration: split * 2,
                                afterFinishInternal: function(effect) {
                                    new Effect.Move(effect.element, {
                                        x: distance * 2,
                                        y: 0,
                                        duration: split * 2,
                                        afterFinishInternal: function(effect) {
                                            new Effect.Move(effect.element, {
                                                x: -distance,
                                                y: 0,
                                                duration: split,
                                                afterFinishInternal: function(effect) {
                                                    effect.element.undoPositioned().setStyle(oldStyle);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};
Effect.SlideDown = function(element) {
    element = $(element).cleanWhitespace();
    var oldInnerBottom = element.down().getStyle('bottom');
    var elementDimensions = element.getDimensions();
    return new Effect.Scale(element, 100, Object.extend({
        scaleContent: false,
        scaleX: false,
        scaleFrom: window.opera ? 0 : 1,
        scaleMode: {
            originalHeight: elementDimensions.height,
            originalWidth: elementDimensions.width
        },
        restoreAfterFinish: true,
        afterSetup: function(effect) {
            effect.element.makePositioned();
            effect.element.down().makePositioned();
            if (window.opera) effect.element.setStyle({
                top: ''
            });
            effect.element.makeClipping().setStyle({
                height: '0px'
            }).show();
        },
        afterUpdateInternal: function(effect) {
            effect.element.down().setStyle({
                bottom: (effect.dims[0] - effect.element.clientHeight) + 'px'
            });
        },
        afterFinishInternal: function(effect) {
            effect.element.undoClipping().undoPositioned();
            effect.element.down().undoPositioned().setStyle({
                bottom: oldInnerBottom
            });
        }
    }, arguments[1] || {}));
};
Effect.SlideUp = function(element) {
    element = $(element).cleanWhitespace();
    var oldInnerBottom = element.down().getStyle('bottom');
    var elementDimensions = element.getDimensions();
    return new Effect.Scale(element, window.opera ? 0 : 1, Object.extend({
        scaleContent: false,
        scaleX: false,
        scaleMode: 'box',
        scaleFrom: 100,
        scaleMode: {
            originalHeight: elementDimensions.height,
            originalWidth: elementDimensions.width
        },
        restoreAfterFinish: true,
        afterSetup: function(effect) {
            effect.element.makePositioned();
            effect.element.down().makePositioned();
            if (window.opera) effect.element.setStyle({
                top: ''
            });
            effect.element.makeClipping().show();
        },
        afterUpdateInternal: function(effect) {
            effect.element.down().setStyle({
                bottom: (effect.dims[0] - effect.element.clientHeight) + 'px'
            });
        },
        afterFinishInternal: function(effect) {
            effect.element.hide().undoClipping().undoPositioned();
            effect.element.down().undoPositioned().setStyle({
                bottom: oldInnerBottom
            });
        }
    }, arguments[1] || {}));
};
Effect.Squish = function(element) {
    return new Effect.Scale(element, window.opera ? 1 : 0, {
        restoreAfterFinish: true,
        beforeSetup: function(effect) {
            effect.element.makeClipping();
        },
        afterFinishInternal: function(effect) {
            effect.element.hide().undoClipping();
        }
    });
};
Effect.Grow = function(element) {
    element = $(element);
    var options = Object.extend({
        direction: 'center',
        moveTransition: Effect.Transitions.sinoidal,
        scaleTransition: Effect.Transitions.sinoidal,
        opacityTransition: Effect.Transitions.full
    }, arguments[1] || {});
    var oldStyle = {
        top: element.style.top,
        left: element.style.left,
        height: element.style.height,
        width: element.style.width,
        opacity: element.getInlineOpacity()
    };
    var dims = element.getDimensions();
    var initialMoveX, initialMoveY;
    var moveX, moveY;
    switch (options.direction) {
        case 'top-left':
            initialMoveX = initialMoveY = moveX = moveY = 0;
            break;
        case 'top-right':
            initialMoveX = dims.width;
            initialMoveY = moveY = 0;
            moveX = -dims.width;
            break;
        case 'bottom-left':
            initialMoveX = moveX = 0;
            initialMoveY = dims.height;
            moveY = -dims.height;
            break;
        case 'bottom-right':
            initialMoveX = dims.width;
            initialMoveY = dims.height;
            moveX = -dims.width;
            moveY = -dims.height;
            break;
        case 'center':
            initialMoveX = dims.width / 2;
            initialMoveY = dims.height / 2;
            moveX = -dims.width / 2;
            moveY = -dims.height / 2;
            break;
    }
    return new Effect.Move(element, {
        x: initialMoveX,
        y: initialMoveY,
        duration: 0.01,
        beforeSetup: function(effect) {
            effect.element.hide().makeClipping().makePositioned();
        },
        afterFinishInternal: function(effect) {
            new Effect.Parallel([new Effect.Opacity(effect.element, {
                sync: true,
                to: 1.0,
                from: 0.0,
                transition: options.opacityTransition
            }), new Effect.Move(effect.element, {
                x: moveX,
                y: moveY,
                sync: true,
                transition: options.moveTransition
            }), new Effect.Scale(effect.element, 100, {
                scaleMode: {
                    originalHeight: dims.height,
                    originalWidth: dims.width
                },
                sync: true,
                scaleFrom: window.opera ? 1 : 0,
                transition: options.scaleTransition,
                restoreAfterFinish: true
            })], Object.extend({
                beforeSetup: function(effect) {
                    effect.effects[0].element.setStyle({
                        height: '0px'
                    }).show();
                },
                afterFinishInternal: function(effect) {
                    effect.effects[0].element.undoClipping().undoPositioned().setStyle(oldStyle);
                }
            }, options));
        }
    });
};
Effect.Shrink = function(element) {
    element = $(element);
    var options = Object.extend({
        direction: 'center',
        moveTransition: Effect.Transitions.sinoidal,
        scaleTransition: Effect.Transitions.sinoidal,
        opacityTransition: Effect.Transitions.none
    }, arguments[1] || {});
    var oldStyle = {
        top: element.style.top,
        left: element.style.left,
        height: element.style.height,
        width: element.style.width,
        opacity: element.getInlineOpacity()
    };
    var dims = element.getDimensions();
    var moveX, moveY;
    switch (options.direction) {
        case 'top-left':
            moveX = moveY = 0;
            break;
        case 'top-right':
            moveX = dims.width;
            moveY = 0;
            break;
        case 'bottom-left':
            moveX = 0;
            moveY = dims.height;
            break;
        case 'bottom-right':
            moveX = dims.width;
            moveY = dims.height;
            break;
        case 'center':
            moveX = dims.width / 2;
            moveY = dims.height / 2;
            break;
    }
    return new Effect.Parallel([new Effect.Opacity(element, {
        sync: true,
        to: 0.0,
        from: 1.0,
        transition: options.opacityTransition
    }), new Effect.Scale(element, window.opera ? 1 : 0, {
        sync: true,
        transition: options.scaleTransition,
        restoreAfterFinish: true
    }), new Effect.Move(element, {
        x: moveX,
        y: moveY,
        sync: true,
        transition: options.moveTransition
    })], Object.extend({
        beforeStartInternal: function(effect) {
            effect.effects[0].element.makePositioned().makeClipping();
        },
        afterFinishInternal: function(effect) {
            effect.effects[0].element.hide().undoClipping().undoPositioned().setStyle(oldStyle);
        }
    }, options));
};
Effect.Pulsate = function(element) {
    element = $(element);
    var options = arguments[1] || {},
        oldOpacity = element.getInlineOpacity(),
        transition = options.transition || Effect.Transitions.linear,
        reverser = function(pos) {
            return 1 - transition((-Math.cos((pos * (options.pulses || 5) * 2) * Math.PI) / 2) + .5);
        };
    return new Effect.Opacity(element, Object.extend(Object.extend({
        duration: 2.0,
        from: 0,
        afterFinishInternal: function(effect) {
            effect.element.setStyle({
                opacity: oldOpacity
            });
        }
    }, options), {
        transition: reverser
    }));
};
Effect.Fold = function(element) {
    element = $(element);
    var oldStyle = {
        top: element.style.top,
        left: element.style.left,
        width: element.style.width,
        height: element.style.height
    };
    element.makeClipping();
    return new Effect.Scale(element, 5, Object.extend({
        scaleContent: false,
        scaleX: false,
        afterFinishInternal: function(effect) {
            new Effect.Scale(element, 1, {
                scaleContent: false,
                scaleY: false,
                afterFinishInternal: function(effect) {
                    effect.element.hide().undoClipping().setStyle(oldStyle);
                }
            });
        }
    }, arguments[1] || {}));
};
Effect.Morph = Class.create(Effect.Base, {
    initialize: function(element) {
        this.element = $(element);
        if (!this.element) throw (Effect._elementDoesNotExistError);
        var options = Object.extend({
            style: {}
        }, arguments[1] || {});
        if (!Object.isString(options.style)) this.style = $H(options.style);
        else {
            if (options.style.include(':'))
                this.style = options.style.parseStyle();
            else {
                this.element.addClassName(options.style);
                this.style = $H(this.element.getStyles());
                this.element.removeClassName(options.style);
                var css = this.element.getStyles();
                this.style = this.style.reject(function(style) {
                    return style.value == css[style.key];
                });
                options.afterFinishInternal = function(effect) {
                    effect.element.addClassName(effect.options.style);
                    effect.transforms.each(function(transform) {
                        effect.element.style[transform.style] = '';
                    });
                };
            }
        }
        this.start(options);
    },
    setup: function() {
        function parseColor(color) {
            if (!color || ['rgba(0, 0, 0, 0)', 'transparent'].include(color)) color = '#ffffff';
            color = color.parseColor();
            return $R(0, 2).map(function(i) {
                return parseInt(color.slice(i * 2 + 1, i * 2 + 3), 16);
            });
        }
        this.transforms = this.style.map(function(pair) {
            var property = pair[0],
                value = pair[1],
                unit = null;
            if (value.parseColor('#zzzzzz') != '#zzzzzz') {
                value = value.parseColor();
                unit = 'color';
            } else if (property == 'opacity') {
                value = parseFloat(value);
                if (Prototype.Browser.IE && (!this.element.currentStyle.hasLayout))
                    this.element.setStyle({
                        zoom: 1
                    });
            } else if (Element.CSS_LENGTH.test(value)) {
                var components = value.match(/^([\+\-]?[0-9\.]+)(.*)$/);
                value = parseFloat(components[1]);
                unit = (components.length == 3) ? components[2] : null;
            }
            var originalValue = this.element.getStyle(property);
            return {
                style: property.camelize(),
                originalValue: unit == 'color' ? parseColor(originalValue) : parseFloat(originalValue || 0),
                targetValue: unit == 'color' ? parseColor(value) : value,
                unit: unit
            };
        }.bind(this)).reject(function(transform) {
            return ((transform.originalValue == transform.targetValue) || (transform.unit != 'color' && (isNaN(transform.originalValue) || isNaN(transform.targetValue))));
        });
    },
    update: function(position) {
        var style = {},
            transform, i = this.transforms.length;
        while (i--)
            style[(transform = this.transforms[i]).style] = transform.unit == 'color' ? '#' +
            (Math.round(transform.originalValue[0] +
                (transform.targetValue[0] - transform.originalValue[0]) * position)).toColorPart() +
            (Math.round(transform.originalValue[1] +
                (transform.targetValue[1] - transform.originalValue[1]) * position)).toColorPart() +
            (Math.round(transform.originalValue[2] +
                (transform.targetValue[2] - transform.originalValue[2]) * position)).toColorPart() : (transform.originalValue +
                (transform.targetValue - transform.originalValue) * position).toFixed(3) +
            (transform.unit === null ? '' : transform.unit);
        this.element.setStyle(style, true);
    }
});
Effect.Transform = Class.create({
    initialize: function(tracks) {
        this.tracks = [];
        this.options = arguments[1] || {};
        this.addTracks(tracks);
    },
    addTracks: function(tracks) {
        tracks.each(function(track) {
            track = $H(track);
            var data = track.values().first();
            this.tracks.push($H({
                ids: track.keys().first(),
                effect: Effect.Morph,
                options: {
                    style: data
                }
            }));
        }.bind(this));
        return this;
    },
    play: function() {
        return new Effect.Parallel(this.tracks.map(function(track) {
            var ids = track.get('ids'),
                effect = track.get('effect'),
                options = track.get('options');
            var elements = [$(ids) || $$(ids)].flatten();
            return elements.map(function(e) {
                return new effect(e, Object.extend({
                    sync: true
                }, options))
            });
        }).flatten(), this.options);
    }
});
Element.CSS_PROPERTIES = $w('backgroundColor backgroundPosition borderBottomColor borderBottomStyle ' +
    'borderBottomWidth borderLeftColor borderLeftStyle borderLeftWidth ' +
    'borderRightColor borderRightStyle borderRightWidth borderSpacing ' +
    'borderTopColor borderTopStyle borderTopWidth bottom clip color ' +
    'fontSize fontWeight height left letterSpacing lineHeight ' +
    'marginBottom marginLeft marginRight marginTop markerOffset maxHeight ' +
    'maxWidth minHeight minWidth opacity outlineColor outlineOffset ' +
    'outlineWidth paddingBottom paddingLeft paddingRight paddingTop ' +
    'right textIndent top width wordSpacing zIndex');
Element.CSS_LENGTH = /^(([\+\-]?[0-9\.]+)(em|ex|px|in|cm|mm|pt|pc|\%))|0$/;
String.__parseStyleElement = document.createElement('div');
String.prototype.parseStyle = function() {
    var style, styleRules = $H();
    if (Prototype.Browser.WebKit)
        style = new Element('div', {
            style: this
        }).style;
    else {
        String.__parseStyleElement.innerHTML = '<div style="' + this + '"></div>';
        style = String.__parseStyleElement.childNodes[0].style;
    }
    Element.CSS_PROPERTIES.each(function(property) {
        if (style[property]) styleRules.set(property, style[property]);
    });
    if (Prototype.Browser.IE && this.include('opacity'))
        styleRules.set('opacity', this.match(/opacity:\s*((?:0|1)?(?:\.\d*)?)/)[1]);
    return styleRules;
};
if (document.defaultView && document.defaultView.getComputedStyle) {
    Element.getStyles = function(element) {
        var css = document.defaultView.getComputedStyle($(element), null);
        return Element.CSS_PROPERTIES.inject({}, function(styles, property) {
            styles[property] = css[property];
            return styles;
        });
    };
} else {
    Element.getStyles = function(element) {
        element = $(element);
        var css = element.currentStyle,
            styles;
        styles = Element.CSS_PROPERTIES.inject({}, function(results, property) {
            results[property] = css[property];
            return results;
        });
        if (!styles.opacity) styles.opacity = element.getOpacity();
        return styles;
    };
}
Effect.Methods = {
    morph: function(element, style) {
        element = $(element);
        new Effect.Morph(element, Object.extend({
            style: style
        }, arguments[2] || {}));
        return element;
    },
    visualEffect: function(element, effect, options) {
        element = $(element);
        var s = effect.dasherize().camelize(),
            klass = s.charAt(0).toUpperCase() + s.substring(1);
        new Effect[klass](element, options);
        return element;
    },
    highlight: function(element, options) {
        element = $(element);
        new Effect.Highlight(element, options);
        return element;
    }
};
$w('fade appear grow shrink fold blindUp blindDown slideUp slideDown ' +
    'pulsate shake puff squish switchOff dropOut').each(function(effect) {
    Effect.Methods[effect] = function(element, options) {
        element = $(element);
        Effect[effect.charAt(0).toUpperCase() + effect.substring(1)](element, options);
        return element;
    };
});
$w('getInlineOpacity forceRerendering setContentZoom collectTextNodes collectTextNodesIgnoreClass getStyles').each(function(f) {
    Effect.Methods[f] = Element[f];
});
Element.addMethods(Effect.Methods);
if (Object.isUndefined(Effect))
    throw ("dragdrop.js requires including script.aculo.us' effects.js library");
var Droppables = {
    drops: [],
    remove: function(element) {
        this.drops = this.drops.reject(function(d) {
            return d.element == $(element)
        });
    },
    add: function(element) {
        element = $(element);
        var options = Object.extend({
            greedy: true,
            hoverclass: null,
            tree: false
        }, arguments[1] || {});
        if (options.containment) {
            options._containers = [];
            var containment = options.containment;
            if (Object.isArray(containment)) {
                containment.each(function(c) {
                    options._containers.push($(c))
                });
            } else {
                options._containers.push($(containment));
            }
        }
        if (options.accept) options.accept = [options.accept].flatten();
        Element.makePositioned(element);
        options.element = element;
        this.drops.push(options);
    },
    findDeepestChild: function(drops) {
        deepest = drops[0];
        for (i = 1; i < drops.length; ++i)
            if (Element.isParent(drops[i].element, deepest.element))
                deepest = drops[i];
        return deepest;
    },
    isContained: function(element, drop) {
        var containmentNode;
        if (drop.tree) {
            containmentNode = element.treeNode;
        } else {
            containmentNode = element.parentNode;
        }
        return drop._containers.detect(function(c) {
            return containmentNode == c
        });
    },
    isAffected: function(point, element, drop) {
        return ((drop.element != element) && ((!drop._containers) || this.isContained(element, drop)) && ((!drop.accept) || (Element.classNames(element).detect(function(v) {
            return drop.accept.include(v)
        }))) && Position.within(drop.element, point[0], point[1]));
    },
    deactivate: function(drop) {
        if (drop.hoverclass)
            Element.removeClassName(drop.element, drop.hoverclass);
        this.last_active = null;
    },
    activate: function(drop) {
        if (drop.hoverclass)
            Element.addClassName(drop.element, drop.hoverclass);
        this.last_active = drop;
    },
    show: function(point, element) {
        if (!this.drops.length) return;
        var drop, affected = [];
        this.drops.each(function(drop) {
            if (Droppables.isAffected(point, element, drop))
                affected.push(drop);
        });
        if (affected.length > 0)
            drop = Droppables.findDeepestChild(affected);
        if (this.last_active && this.last_active != drop) this.deactivate(this.last_active);
        if (drop) {
            Position.within(drop.element, point[0], point[1]);
            if (drop.onHover)
                drop.onHover(element, drop.element, Position.overlap(drop.overlap, drop.element));
            if (drop != this.last_active) Droppables.activate(drop);
        }
    },
    fire: function(event, element) {
        if (!this.last_active) return;
        Position.prepare();
        if (this.isAffected([Event.pointerX(event), Event.pointerY(event)], element, this.last_active))
            if (this.last_active.onDrop) {
                this.last_active.onDrop(element, this.last_active.element, event);
                return true;
            }
    },
    reset: function() {
        if (this.last_active)
            this.deactivate(this.last_active);
    }
};
var Draggables = {
    drags: [],
    observers: [],
    register: function(draggable) {
        if (this.drags.length == 0) {
            this.eventMouseUp = this.endDrag.bindAsEventListener(this);
            this.eventMouseMove = this.updateDrag.bindAsEventListener(this);
            this.eventKeypress = this.keyPress.bindAsEventListener(this);
            Event.observe(document, "mouseup", this.eventMouseUp);
            Event.observe(document, "mousemove", this.eventMouseMove);
            Event.observe(document, "keypress", this.eventKeypress);
        }
        this.drags.push(draggable);
    },
    unregister: function(draggable) {
        this.drags = this.drags.reject(function(d) {
            return d == draggable
        });
        if (this.drags.length == 0) {
            Event.stopObserving(document, "mouseup", this.eventMouseUp);
            Event.stopObserving(document, "mousemove", this.eventMouseMove);
            Event.stopObserving(document, "keypress", this.eventKeypress);
        }
    },
    activate: function(draggable) {
        if (draggable.options.delay) {
            this._timeout = setTimeout(function() {
                Draggables._timeout = null;
                window.focus();
                Draggables.activeDraggable = draggable;
            }.bind(this), draggable.options.delay);
        } else {
            window.focus();
            this.activeDraggable = draggable;
        }
    },
    deactivate: function() {
        this.activeDraggable = null;
    },
    updateDrag: function(event) {
        if (!this.activeDraggable) return;
        var pointer = [Event.pointerX(event), Event.pointerY(event)];
        if (this._lastPointer && (this._lastPointer.inspect() == pointer.inspect())) return;
        this._lastPointer = pointer;
        this.activeDraggable.updateDrag(event, pointer);
    },
    endDrag: function(event) {
        if (this._timeout) {
            clearTimeout(this._timeout);
            this._timeout = null;
        }
        if (!this.activeDraggable) return;
        this._lastPointer = null;
        this.activeDraggable.endDrag(event);
        this.activeDraggable = null;
    },
    keyPress: function(event) {
        if (this.activeDraggable)
            this.activeDraggable.keyPress(event);
    },
    addObserver: function(observer) {
        this.observers.push(observer);
        this._cacheObserverCallbacks();
    },
    removeObserver: function(element) {
        this.observers = this.observers.reject(function(o) {
            return o.element == element
        });
        this._cacheObserverCallbacks();
    },
    notify: function(eventName, draggable, event) {
        if (this[eventName + 'Count'] > 0)
            this.observers.each(function(o) {
                if (o[eventName]) o[eventName](eventName, draggable, event);
            });
        if (draggable.options[eventName]) draggable.options[eventName](draggable, event);
    },
    _cacheObserverCallbacks: function() {
        ['onStart', 'onEnd', 'onDrag'].each(function(eventName) {
            Draggables[eventName + 'Count'] = Draggables.observers.select(function(o) {
                return o[eventName];
            }).length;
        });
    }
};
var Draggable = Class.create({
    initialize: function(element) {
        var defaults = {
            handle: false,
            reverteffect: function(element, top_offset, left_offset) {
                var dur = Math.sqrt(Math.abs(top_offset ^ 2) + Math.abs(left_offset ^ 2)) * 0.02;
                new Effect.Move(element, {
                    x: -left_offset,
                    y: -top_offset,
                    duration: dur,
                    queue: {
                        scope: '_draggable',
                        position: 'end'
                    }
                });
            },
            endeffect: function(element) {
                var toOpacity = Object.isNumber(element._opacity) ? element._opacity : 1.0;
                new Effect.Opacity(element, {
                    duration: 0.2,
                    from: 0.7,
                    to: toOpacity,
                    queue: {
                        scope: '_draggable',
                        position: 'end'
                    },
                    afterFinish: function() {
                        Draggable._dragging[element] = false
                    }
                });
            },
            zindex: 1000,
            revert: false,
            quiet: false,
            scroll: false,
            scrollSensitivity: 20,
            scrollSpeed: 15,
            snap: false,
            delay: 0
        };
        if (!arguments[1] || Object.isUndefined(arguments[1].endeffect))
            Object.extend(defaults, {
                starteffect: function(element) {
                    element._opacity = Element.getOpacity(element);
                    Draggable._dragging[element] = true;
                    new Effect.Opacity(element, {
                        duration: 0.2,
                        from: element._opacity,
                        to: 0.7
                    });
                }
            });
        var options = Object.extend(defaults, arguments[1] || {});
        this.element = $(element);
        if (options.handle && Object.isString(options.handle))
            this.handle = this.element.down('.' + options.handle, 0);
        if (!this.handle) this.handle = $(options.handle);
        if (!this.handle) this.handle = this.element;
        if (options.scroll && !options.scroll.scrollTo && !options.scroll.outerHTML) {
            options.scroll = $(options.scroll);
            this._isScrollChild = Element.childOf(this.element, options.scroll);
        }
        Element.makePositioned(this.element);
        this.options = options;
        this.dragging = false;
        this.eventMouseDown = this.initDrag.bindAsEventListener(this);
        Event.observe(this.handle, "mousedown", this.eventMouseDown);
        Draggables.register(this);
    },
    destroy: function() {
        Event.stopObserving(this.handle, "mousedown", this.eventMouseDown);
        Draggables.unregister(this);
    },
    currentDelta: function() {
        return ([parseInt(Element.getStyle(this.element, 'left') || '0'), parseInt(Element.getStyle(this.element, 'top') || '0')]);
    },
    initDrag: function(event) {
        if (!Object.isUndefined(Draggable._dragging[this.element]) && Draggable._dragging[this.element]) return;
        if (Event.isLeftClick(event)) {
            var src = Event.element(event);
            if ((tag_name = src.tagName.toUpperCase()) && (tag_name == 'INPUT' || tag_name == 'SELECT' || tag_name == 'OPTION' || tag_name == 'BUTTON' || tag_name == 'TEXTAREA')) return;
            var pointer = [Event.pointerX(event), Event.pointerY(event)];
            var pos = this.element.cumulativeOffset();
            this.offset = [0, 1].map(function(i) {
                return (pointer[i] - pos[i])
            });
            Draggables.activate(this);
            Event.stop(event);
        }
    },
    startDrag: function(event) {
        this.dragging = true;
        if (!this.delta)
            this.delta = this.currentDelta();
        if (this.options.zindex) {
            this.originalZ = parseInt(Element.getStyle(this.element, 'z-index') || 0);
            this.element.style.zIndex = this.options.zindex;
        }
        if (this.options.ghosting) {
            this._clone = this.element.cloneNode(true);
            this._originallyAbsolute = (this.element.getStyle('position') == 'absolute');
            if (!this._originallyAbsolute)
                Position.absolutize(this.element);
            this.element.parentNode.insertBefore(this._clone, this.element);
        }
        if (this.options.scroll) {
            if (this.options.scroll == window) {
                var where = this._getWindowScroll(this.options.scroll);
                this.originalScrollLeft = where.left;
                this.originalScrollTop = where.top;
            } else {
                this.originalScrollLeft = this.options.scroll.scrollLeft;
                this.originalScrollTop = this.options.scroll.scrollTop;
            }
        }
        Draggables.notify('onStart', this, event);
        if (this.options.starteffect) this.options.starteffect(this.element);
    },
    updateDrag: function(event, pointer) {
        if (!this.dragging) this.startDrag(event);
        if (!this.options.quiet) {
            Position.prepare();
            Droppables.show(pointer, this.element);
        }
        Draggables.notify('onDrag', this, event);
        this.draw(pointer);
        if (this.options.change) this.options.change(this);
        if (this.options.scroll) {
            this.stopScrolling();
            var p;
            if (this.options.scroll == window) {
                with(this._getWindowScroll(this.options.scroll)) {
                    p = [left, top, left + width, top + height];
                }
            } else {
                p = Position.page(this.options.scroll).toArray();
                p[0] += this.options.scroll.scrollLeft + Position.deltaX;
                p[1] += this.options.scroll.scrollTop + Position.deltaY;
                p.push(p[0] + this.options.scroll.offsetWidth);
                p.push(p[1] + this.options.scroll.offsetHeight);
            }
            var speed = [0, 0];
            if (pointer[0] < (p[0] + this.options.scrollSensitivity)) speed[0] = pointer[0] - (p[0] + this.options.scrollSensitivity);
            if (pointer[1] < (p[1] + this.options.scrollSensitivity)) speed[1] = pointer[1] - (p[1] + this.options.scrollSensitivity);
            if (pointer[0] > (p[2] - this.options.scrollSensitivity)) speed[0] = pointer[0] - (p[2] - this.options.scrollSensitivity);
            if (pointer[1] > (p[3] - this.options.scrollSensitivity)) speed[1] = pointer[1] - (p[3] - this.options.scrollSensitivity);
            this.startScrolling(speed);
        }
        if (Prototype.Browser.WebKit) window.scrollBy(0, 0);
        Event.stop(event);
    },
    finishDrag: function(event, success) {
        this.dragging = false;
        if (this.options.quiet) {
            Position.prepare();
            var pointer = [Event.pointerX(event), Event.pointerY(event)];
            Droppables.show(pointer, this.element);
        }
        if (this.options.ghosting) {
            if (!this._originallyAbsolute)
                Position.relativize(this.element);
            delete this._originallyAbsolute;
            Element.remove(this._clone);
            this._clone = null;
        }
        var dropped = false;
        if (success) {
            dropped = Droppables.fire(event, this.element);
            if (!dropped) dropped = false;
        }
        if (dropped && this.options.onDropped) this.options.onDropped(this.element);
        Draggables.notify('onEnd', this, event);
        var revert = this.options.revert;
        if (revert && Object.isFunction(revert)) revert = revert(this.element);
        var d = this.currentDelta();
        if (revert && this.options.reverteffect) {
            if (dropped == 0 || revert != 'failure')
                this.options.reverteffect(this.element, d[1] - this.delta[1], d[0] - this.delta[0]);
        } else {
            this.delta = d;
        }
        if (this.options.zindex)
            this.element.style.zIndex = this.originalZ;
        if (this.options.endeffect)
            this.options.endeffect(this.element);
        Draggables.deactivate(this);
        Droppables.reset();
    },
    keyPress: function(event) {
        if (event.keyCode != Event.KEY_ESC) return;
        this.finishDrag(event, false);
        Event.stop(event);
    },
    endDrag: function(event) {
        if (!this.dragging) return;
        this.stopScrolling();
        this.finishDrag(event, true);
        Event.stop(event);
    },
    draw: function(point) {
        var pos = this.element.cumulativeOffset();
        if (this.options.ghosting) {
            var r = Position.realOffset(this.element);
            pos[0] += r[0] - Position.deltaX;
            pos[1] += r[1] - Position.deltaY;
        }
        var d = this.currentDelta();
        pos[0] -= d[0];
        pos[1] -= d[1];
        if (this.options.scroll && (this.options.scroll != window && this._isScrollChild)) {
            pos[0] -= this.options.scroll.scrollLeft - this.originalScrollLeft;
            pos[1] -= this.options.scroll.scrollTop - this.originalScrollTop;
        }
        var p = [0, 1].map(function(i) {
            return (point[i] - pos[i] - this.offset[i])
        }.bind(this));
        if (this.options.snap) {
            if (Object.isFunction(this.options.snap)) {
                p = this.options.snap(p[0], p[1], this);
            } else {
                if (Object.isArray(this.options.snap)) {
                    p = p.map(function(v, i) {
                        return (v / this.options.snap[i]).round() * this.options.snap[i]
                    }.bind(this));
                } else {
                    p = p.map(function(v) {
                        return (v / this.options.snap).round() * this.options.snap
                    }.bind(this));
                }
            }
        }
        var style = this.element.style;
        if ((!this.options.constraint) || (this.options.constraint == 'horizontal'))
            style.left = p[0] + "px";
        if ((!this.options.constraint) || (this.options.constraint == 'vertical'))
            style.top = p[1] + "px";
        if (style.visibility == "hidden") style.visibility = "";
    },
    stopScrolling: function() {
        if (this.scrollInterval) {
            clearInterval(this.scrollInterval);
            this.scrollInterval = null;
            Draggables._lastScrollPointer = null;
        }
    },
    startScrolling: function(speed) {
        if (!(speed[0] || speed[1])) return;
        this.scrollSpeed = [speed[0] * this.options.scrollSpeed, speed[1] * this.options.scrollSpeed];
        this.lastScrolled = new Date();
        this.scrollInterval = setInterval(this.scroll.bind(this), 10);
    },
    scroll: function() {
        var current = new Date();
        var delta = current - this.lastScrolled;
        this.lastScrolled = current;
        if (this.options.scroll == window) {
            with(this._getWindowScroll(this.options.scroll)) {
                if (this.scrollSpeed[0] || this.scrollSpeed[1]) {
                    var d = delta / 1000;
                    this.options.scroll.scrollTo(left + d * this.scrollSpeed[0], top + d * this.scrollSpeed[1]);
                }
            }
        } else {
            this.options.scroll.scrollLeft += this.scrollSpeed[0] * delta / 1000;
            this.options.scroll.scrollTop += this.scrollSpeed[1] * delta / 1000;
        }
        Position.prepare();
        Droppables.show(Draggables._lastPointer, this.element);
        Draggables.notify('onDrag', this);
        if (this._isScrollChild) {
            Draggables._lastScrollPointer = Draggables._lastScrollPointer || $A(Draggables._lastPointer);
            Draggables._lastScrollPointer[0] += this.scrollSpeed[0] * delta / 1000;
            Draggables._lastScrollPointer[1] += this.scrollSpeed[1] * delta / 1000;
            if (Draggables._lastScrollPointer[0] < 0)
                Draggables._lastScrollPointer[0] = 0;
            if (Draggables._lastScrollPointer[1] < 0)
                Draggables._lastScrollPointer[1] = 0;
            this.draw(Draggables._lastScrollPointer);
        }
        if (this.options.change) this.options.change(this);
    },
    _getWindowScroll: function(w) {
        var T, L, W, H;
        with(w.document) {
            if (w.document.documentElement && documentElement.scrollTop) {
                T = documentElement.scrollTop;
                L = documentElement.scrollLeft;
            } else if (w.document.body) {
                T = body.scrollTop;
                L = body.scrollLeft;
            }
            if (w.innerWidth) {
                W = w.innerWidth;
                H = w.innerHeight;
            } else if (w.document.documentElement && documentElement.clientWidth) {
                W = documentElement.clientWidth;
                H = documentElement.clientHeight;
            } else {
                W = body.offsetWidth;
                H = body.offsetHeight;
            }
        }
        return {
            top: T,
            left: L,
            width: W,
            height: H
        };
    }
});
Draggable._dragging = {};
var SortableObserver = Class.create({
    initialize: function(element, observer) {
        this.element = $(element);
        this.observer = observer;
        this.lastValue = Sortable.serialize(this.element);
    },
    onStart: function() {
        this.lastValue = Sortable.serialize(this.element);
    },
    onEnd: function() {
        Sortable.unmark();
        if (this.lastValue != Sortable.serialize(this.element))
            this.observer(this.element)
    }
});
var Sortable = {
    SERIALIZE_RULE: /^[^_\-](?:[A-Za-z0-9\-\_]*)[_](.*)$/,
    sortables: {},
    _findRootElement: function(element) {
        while (element.tagName.toUpperCase() != "BODY") {
            if (element.id && Sortable.sortables[element.id]) return element;
            element = element.parentNode;
        }
    },
    options: function(element) {
        element = Sortable._findRootElement($(element));
        if (!element) return;
        return Sortable.sortables[element.id];
    },
    destroy: function(element) {
        element = $(element);
        var s = Sortable.sortables[element.id];
        if (s) {
            Draggables.removeObserver(s.element);
            s.droppables.each(function(d) {
                Droppables.remove(d)
            });
            s.draggables.invoke('destroy');
            delete Sortable.sortables[s.element.id];
        }
    },
    create: function(element) {
        element = $(element);
        var options = Object.extend({
            element: element,
            tag: 'li',
            dropOnEmpty: false,
            tree: false,
            treeTag: 'ul',
            overlap: 'vertical',
            constraint: 'vertical',
            containment: element,
            handle: false,
            only: false,
            delay: 0,
            hoverclass: null,
            ghosting: false,
            quiet: false,
            scroll: false,
            scrollSensitivity: 20,
            scrollSpeed: 15,
            format: this.SERIALIZE_RULE,
            elements: false,
            handles: false,
            onChange: Prototype.emptyFunction,
            onUpdate: Prototype.emptyFunction
        }, arguments[1] || {});
        this.destroy(element);
        var options_for_draggable = {
            revert: true,
            quiet: options.quiet,
            scroll: options.scroll,
            scrollSpeed: options.scrollSpeed,
            scrollSensitivity: options.scrollSensitivity,
            delay: options.delay,
            ghosting: options.ghosting,
            constraint: options.constraint,
            handle: options.handle
        };
        if (options.starteffect)
            options_for_draggable.starteffect = options.starteffect;
        if (options.reverteffect)
            options_for_draggable.reverteffect = options.reverteffect;
        else
        if (options.ghosting) options_for_draggable.reverteffect = function(element) {
            element.style.top = 0;
            element.style.left = 0;
        };
        if (options.endeffect)
            options_for_draggable.endeffect = options.endeffect;
        if (options.zindex)
            options_for_draggable.zindex = options.zindex;
        var options_for_droppable = {
            overlap: options.overlap,
            containment: options.containment,
            tree: options.tree,
            hoverclass: options.hoverclass,
            onHover: Sortable.onHover
        };
        var options_for_tree = {
            onHover: Sortable.onEmptyHover,
            overlap: options.overlap,
            containment: options.containment,
            hoverclass: options.hoverclass
        };
        Element.cleanWhitespace(element);
        options.draggables = [];
        options.droppables = [];
        if (options.dropOnEmpty || options.tree) {
            Droppables.add(element, options_for_tree);
            options.droppables.push(element);
        }
        (options.elements || this.findElements(element, options) || []).each(function(e, i) {
            var handle = options.handles ? $(options.handles[i]) : (options.handle ? $(e).select('.' + options.handle)[0] : e);
            options.draggables.push(new Draggable(e, Object.extend(options_for_draggable, {
                handle: handle
            })));
            Droppables.add(e, options_for_droppable);
            if (options.tree) e.treeNode = element;
            options.droppables.push(e);
        });
        if (options.tree) {
            (Sortable.findTreeElements(element, options) || []).each(function(e) {
                Droppables.add(e, options_for_tree);
                e.treeNode = element;
                options.droppables.push(e);
            });
        }
        this.sortables[element.identify()] = options;
        Draggables.addObserver(new SortableObserver(element, options.onUpdate));
    },
    findElements: function(element, options) {
        return Element.findChildren(element, options.only, options.tree ? true : false, options.tag);
    },
    findTreeElements: function(element, options) {
        return Element.findChildren(element, options.only, options.tree ? true : false, options.treeTag);
    },
    onHover: function(element, dropon, overlap) {
        if (Element.isParent(dropon, element)) return;
        if (overlap > .33 && overlap < .66 && Sortable.options(dropon).tree) {
            return;
        } else if (overlap > 0.5) {
            Sortable.mark(dropon, 'before');
            if (dropon.previousSibling != element) {
                var oldParentNode = element.parentNode;
                element.style.visibility = "hidden";
                dropon.parentNode.insertBefore(element, dropon);
                if (dropon.parentNode != oldParentNode)
                    Sortable.options(oldParentNode).onChange(element);
                Sortable.options(dropon.parentNode).onChange(element);
            }
        } else {
            Sortable.mark(dropon, 'after');
            var nextElement = dropon.nextSibling || null;
            if (nextElement != element) {
                var oldParentNode = element.parentNode;
                element.style.visibility = "hidden";
                dropon.parentNode.insertBefore(element, nextElement);
                if (dropon.parentNode != oldParentNode)
                    Sortable.options(oldParentNode).onChange(element);
                Sortable.options(dropon.parentNode).onChange(element);
            }
        }
    },
    onEmptyHover: function(element, dropon, overlap) {
        var oldParentNode = element.parentNode;
        var droponOptions = Sortable.options(dropon);
        if (!Element.isParent(dropon, element)) {
            var index;
            var children = Sortable.findElements(dropon, {
                tag: droponOptions.tag,
                only: droponOptions.only
            });
            var child = null;
            if (children) {
                var offset = Element.offsetSize(dropon, droponOptions.overlap) * (1.0 - overlap);
                for (index = 0; index < children.length; index += 1) {
                    if (offset - Element.offsetSize(children[index], droponOptions.overlap) >= 0) {
                        offset -= Element.offsetSize(children[index], droponOptions.overlap);
                    } else if (offset - (Element.offsetSize(children[index], droponOptions.overlap) / 2) >= 0) {
                        child = index + 1 < children.length ? children[index + 1] : null;
                        break;
                    } else {
                        child = children[index];
                        break;
                    }
                }
            }
            dropon.insertBefore(element, child);
            Sortable.options(oldParentNode).onChange(element);
            droponOptions.onChange(element);
        }
    },
    unmark: function() {
        if (Sortable._marker) Sortable._marker.hide();
    },
    mark: function(dropon, position) {
        var sortable = Sortable.options(dropon.parentNode);
        if (sortable && !sortable.ghosting) return;
        if (!Sortable._marker) {
            Sortable._marker = ($('dropmarker') || Element.extend(document.createElement('DIV'))).hide().addClassName('dropmarker').setStyle({
                position: 'absolute'
            });
            document.getElementsByTagName("body").item(0).appendChild(Sortable._marker);
        }
        var offsets = dropon.cumulativeOffset();
        Sortable._marker.setStyle({
            left: offsets[0] + 'px',
            top: offsets[1] + 'px'
        });
        if (position == 'after')
            if (sortable.overlap == 'horizontal')
                Sortable._marker.setStyle({
                    left: (offsets[0] + dropon.clientWidth) + 'px'
                });
            else
                Sortable._marker.setStyle({
                    top: (offsets[1] + dropon.clientHeight) + 'px'
                });
        Sortable._marker.show();
    },
    _tree: function(element, options, parent) {
        var children = Sortable.findElements(element, options) || [];
        for (var i = 0; i < children.length; ++i) {
            var match = children[i].id.match(options.format);
            if (!match) continue;
            var child = {
                id: encodeURIComponent(match ? match[1] : null),
                element: element,
                parent: parent,
                children: [],
                position: parent.children.length,
                container: $(children[i]).down(options.treeTag)
            };
            if (child.container)
                this._tree(child.container, options, child);
            parent.children.push(child);
        }
        return parent;
    },
    tree: function(element) {
        element = $(element);
        var sortableOptions = this.options(element);
        var options = Object.extend({
            tag: sortableOptions.tag,
            treeTag: sortableOptions.treeTag,
            only: sortableOptions.only,
            name: element.id,
            format: sortableOptions.format
        }, arguments[1] || {});
        var root = {
            id: null,
            parent: null,
            children: [],
            container: element,
            position: 0
        };
        return Sortable._tree(element, options, root);
    },
    _constructIndex: function(node) {
        var index = '';
        do {
            if (node.id) index = '[' + node.position + ']' + index;
        } while ((node = node.parent) != null);
        return index;
    },
    sequence: function(element) {
        element = $(element);
        var options = Object.extend(this.options(element), arguments[1] || {});
        return $(this.findElements(element, options) || []).map(function(item) {
            return item.id.match(options.format) ? item.id.match(options.format)[1] : '';
        });
    },
    setSequence: function(element, new_sequence) {
        element = $(element);
        var options = Object.extend(this.options(element), arguments[2] || {});
        var nodeMap = {};
        this.findElements(element, options).each(function(n) {
            if (n.id.match(options.format))
                nodeMap[n.id.match(options.format)[1]] = [n, n.parentNode];
            n.parentNode.removeChild(n);
        });
        new_sequence.each(function(ident) {
            var n = nodeMap[ident];
            if (n) {
                n[1].appendChild(n[0]);
                delete nodeMap[ident];
            }
        });
    },
    serialize: function(element) {
        element = $(element);
        var options = Object.extend(Sortable.options(element), arguments[1] || {});
        var name = encodeURIComponent((arguments[1] && arguments[1].name) ? arguments[1].name : element.id);
        if (options.tree) {
            return Sortable.tree(element, arguments[1]).children.map(function(item) {
                return [name + Sortable._constructIndex(item) + "[id]=" +
                    encodeURIComponent(item.id)
                ].concat(item.children.map(arguments.callee));
            }).flatten().join('&');
        } else {
            return Sortable.sequence(element, arguments[1]).map(function(item) {
                return name + "[]=" + encodeURIComponent(item);
            }).join('&');
        }
    }
};
Element.isParent = function(child, element) {
    if (!child.parentNode || child == element) return false;
    if (child.parentNode == element) return true;
    return Element.isParent(child.parentNode, element);
};
Element.findChildren = function(element, only, recursive, tagName) {
    if (!element.hasChildNodes()) return null;
    tagName = tagName.toUpperCase();
    if (only) only = [only].flatten();
    var elements = [];
    $A(element.childNodes).each(function(e) {
        if (e.tagName && e.tagName.toUpperCase() == tagName && (!only || (Element.classNames(e).detect(function(v) {
                return only.include(v)
            }))))
            elements.push(e);
        if (recursive) {
            var grandchildren = Element.findChildren(e, only, recursive, tagName);
            if (grandchildren) elements.push(grandchildren);
        }
    });
    return (elements.length > 0 ? elements.flatten() : []);
};
Element.offsetSize = function(element, type) {
    return element['offset' + ((type == 'vertical' || type == 'height') ? 'Height' : 'Width')];
};
if (typeof Effect == 'undefined')
    throw ("controls.js requires including script.aculo.us' effects.js library");
var Autocompleter = {};
Autocompleter.Base = Class.create({
    baseInitialize: function(element, update, options) {
        element = $(element);
        this.element = element;
        this.update = $(update);
        this.hasFocus = false;
        this.changed = false;
        this.active = false;
        this.index = 0;
        this.entryCount = 0;
        this.oldElementValue = this.element.value;
        if (this.setOptions)
            this.setOptions(options);
        else
            this.options = options || {};
        this.options.paramName = this.options.paramName || this.element.name;
        this.options.tokens = this.options.tokens || [];
        this.options.frequency = this.options.frequency || 0.4;
        this.options.minChars = this.options.minChars || 1;
        this.options.onShow = this.options.onShow || function(element, update) {
            if (!update.style.position || update.style.position == 'absolute') {
                update.style.position = 'absolute';
                Position.clone(element, update, {
                    setHeight: false,
                    offsetTop: element.offsetHeight
                });
            }
            Effect.Appear(update, {
                duration: 0.15
            });
        };
        this.options.onHide = this.options.onHide || function(element, update) {
            new Effect.Fade(update, {
                duration: 0.15
            })
        };
        if (typeof(this.options.tokens) == 'string')
            this.options.tokens = new Array(this.options.tokens);
        if (!this.options.tokens.include('\n'))
            this.options.tokens.push('\n');
        this.observer = null;
        this.element.setAttribute('autocomplete', 'off');
        Element.hide(this.update);
        Event.observe(this.element, 'blur', this.onBlur.bindAsEventListener(this));
        Event.observe(this.element, 'keydown', this.onKeyPress.bindAsEventListener(this));
    },
    show: function() {
        if (Element.getStyle(this.update, 'display') == 'none') this.options.onShow(this.element, this.update);
        if (!this.iefix && (Prototype.Browser.IE) && (Element.getStyle(this.update, 'position') == 'absolute')) {
            new Insertion.After(this.update, '<iframe id="' + this.update.id + '_iefix" ' +
                'style="display:none;position:absolute;filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);" ' +
                'src="javascript:false;" frameborder="0" scrolling="no"></iframe>');
            this.iefix = $(this.update.id + '_iefix');
        }
        if (this.iefix) setTimeout(this.fixIEOverlapping.bind(this), 50);
    },
    fixIEOverlapping: function() {
        Position.clone(this.update, this.iefix, {
            setTop: (!this.update.style.height)
        });
        this.iefix.style.zIndex = 1;
        this.update.style.zIndex = 2;
        Element.show(this.iefix);
    },
    hide: function() {
        this.stopIndicator();
        if (Element.getStyle(this.update, 'display') != 'none') this.options.onHide(this.element, this.update);
        if (this.iefix) Element.hide(this.iefix);
    },
    startIndicator: function() {
        if (this.options.indicator) Element.show(this.options.indicator);
    },
    stopIndicator: function() {
        if (this.options.indicator) Element.hide(this.options.indicator);
    },
    onKeyPress: function(event) {
        if (this.active)
            switch (event.keyCode) {
                case Event.KEY_TAB:
                case Event.KEY_RETURN:
                    this.selectEntry();
                    Event.stop(event);
                case Event.KEY_ESC:
                    this.hide();
                    this.active = false;
                    Event.stop(event);
                    return;
                case Event.KEY_LEFT:
                case Event.KEY_RIGHT:
                    return;
                case Event.KEY_UP:
                    this.markPrevious();
                    this.render();
                    Event.stop(event);
                    return;
                case Event.KEY_DOWN:
                    this.markNext();
                    this.render();
                    Event.stop(event);
                    return;
            } else
        if (event.keyCode == Event.KEY_TAB || event.keyCode == Event.KEY_RETURN || (Prototype.Browser.WebKit > 0 && event.keyCode == 0)) return;
        this.changed = true;
        this.hasFocus = true;
        if (this.observer) clearTimeout(this.observer);
        this.observer = setTimeout(this.onObserverEvent.bind(this), this.options.frequency * 1000);
    },
    activate: function() {
        this.changed = false;
        this.hasFocus = true;
        this.getUpdatedChoices();
    },
    onHover: function(event) {
        var element = Event.findElement(event, 'LI');
        if (this.index != element.autocompleteIndex) {
            this.index = element.autocompleteIndex;
            this.render();
        }
        Event.stop(event);
    },
    onClick: function(event) {
        var element = Event.findElement(event, 'LI');
        this.index = element.autocompleteIndex;
        this.selectEntry();
        this.hide();
    },
    onBlur: function(event) {
        setTimeout(this.hide.bind(this), 250);
        this.hasFocus = false;
        this.active = false;
    },
    render: function() {
        if (this.entryCount > 0) {
            for (var i = 0; i < this.entryCount; i++)
                this.index == i ? Element.addClassName(this.getEntry(i), "selected") : Element.removeClassName(this.getEntry(i), "selected");
            if (this.hasFocus) {
                this.show();
                this.active = true;
            }
        } else {
            this.active = false;
            this.hide();
        }
    },
    markPrevious: function() {
        if (this.index > 0) this.index--;
        else this.index = this.entryCount - 1;
    },
    markNext: function() {
        if (this.index < this.entryCount - 1) this.index++;
        else this.index = 0;
        this.getEntry(this.index).scrollIntoView(false);
    },
    getEntry: function(index) {
        return this.update.firstChild.childNodes[index];
    },
    getCurrentEntry: function() {
        return this.getEntry(this.index);
    },
    selectEntry: function() {
        this.active = false;
        this.updateElement(this.getCurrentEntry());
    },
    updateElement: function(selectedElement) {
        if (this.options.updateElement) {
            this.options.updateElement(selectedElement);
            return;
        }
        var value = '';
        if (this.options.select) {
            var nodes = $(selectedElement).select('.' + this.options.select) || [];
            if (nodes.length > 0) value = Element.collectTextNodes(nodes[0], this.options.select);
        } else
            value = Element.collectTextNodesIgnoreClass(selectedElement, 'informal');
        var bounds = this.getTokenBounds();
        if (bounds[0] != -1) {
            var newValue = this.element.value.substr(0, bounds[0]);
            var whitespace = this.element.value.substr(bounds[0]).match(/^\s+/);
            if (whitespace)
                newValue += whitespace[0];
            this.element.value = newValue + value + this.element.value.substr(bounds[1]);
        } else {
            this.element.value = value;
        }
        this.oldElementValue = this.element.value;
        this.element.focus();
        if (this.options.afterUpdateElement)
            this.options.afterUpdateElement(this.element, selectedElement);
    },
    updateChoices: function(choices) {
        if (!this.changed && this.hasFocus) {
            this.update.innerHTML = choices;
            Element.cleanWhitespace(this.update);
            Element.cleanWhitespace(this.update.down());
            if (this.update.firstChild && this.update.down().childNodes) {
                this.entryCount = this.update.down().childNodes.length;
                for (var i = 0; i < this.entryCount; i++) {
                    var entry = this.getEntry(i);
                    entry.autocompleteIndex = i;
                    this.addObservers(entry);
                }
            } else {
                this.entryCount = 0;
            }
            this.stopIndicator();
            this.index = 0;
            if (this.entryCount == 1 && this.options.autoSelect) {
                this.selectEntry();
                this.hide();
            } else {
                this.render();
            }
        }
    },
    addObservers: function(element) {
        Event.observe(element, "mouseover", this.onHover.bindAsEventListener(this));
        Event.observe(element, "click", this.onClick.bindAsEventListener(this));
    },
    onObserverEvent: function() {
        this.changed = false;
        this.tokenBounds = null;
        if (this.getToken().length >= this.options.minChars) {
            this.getUpdatedChoices();
        } else {
            this.active = false;
            this.hide();
        }
        this.oldElementValue = this.element.value;
    },
    getToken: function() {
        var bounds = this.getTokenBounds();
        return this.element.value.substring(bounds[0], bounds[1]).strip();
    },
    getTokenBounds: function() {
        if (null != this.tokenBounds) return this.tokenBounds;
        var value = this.element.value;
        if (value.strip().empty()) return [-1, 0];
        var diff = arguments.callee.getFirstDifferencePos(value, this.oldElementValue);
        var offset = (diff == this.oldElementValue.length ? 1 : 0);
        var prevTokenPos = -1,
            nextTokenPos = value.length;
        var tp;
        for (var index = 0, l = this.options.tokens.length; index < l; ++index) {
            tp = value.lastIndexOf(this.options.tokens[index], diff + offset - 1);
            if (tp > prevTokenPos) prevTokenPos = tp;
            tp = value.indexOf(this.options.tokens[index], diff + offset);
            if (-1 != tp && tp < nextTokenPos) nextTokenPos = tp;
        }
        return (this.tokenBounds = [prevTokenPos + 1, nextTokenPos]);
    }
});
Autocompleter.Base.prototype.getTokenBounds.getFirstDifferencePos = function(newS, oldS) {
    var boundary = Math.min(newS.length, oldS.length);
    for (var index = 0; index < boundary; ++index)
        if (newS[index] != oldS[index])
            return index;
    return boundary;
};
Ajax.Autocompleter = Class.create(Autocompleter.Base, {
    initialize: function(element, update, url, options) {
        this.baseInitialize(element, update, options);
        this.options.asynchronous = true;
        this.options.onComplete = this.onComplete.bind(this);
        this.options.defaultParams = this.options.parameters || null;
        this.url = url;
    },
    getUpdatedChoices: function() {
        this.startIndicator();
        var entry = encodeURIComponent(this.options.paramName) + '=' +
            encodeURIComponent(this.getToken());
        this.options.parameters = this.options.callback ? this.options.callback(this.element, entry) : entry;
        if (this.options.defaultParams)
            this.options.parameters += '&' + this.options.defaultParams;
        new Ajax.Request(this.url, this.options);
    },
    onComplete: function(request) {
        this.updateChoices(request.responseText);
    }
});
Autocompleter.Local = Class.create(Autocompleter.Base, {
    initialize: function(element, update, array, options) {
        this.baseInitialize(element, update, options);
        this.options.array = array;
    },
    getUpdatedChoices: function() {
        this.updateChoices(this.options.selector(this));
    },
    setOptions: function(options) {
        this.options = Object.extend({
            choices: 10,
            partialSearch: true,
            partialChars: 2,
            ignoreCase: true,
            fullSearch: false,
            selector: function(instance) {
                var ret = [];
                var partial = [];
                var entry = instance.getToken();
                var count = 0;
                for (var i = 0; i < instance.options.array.length && ret.length < instance.options.choices; i++) {
                    var elem = instance.options.array[i];
                    var foundPos = instance.options.ignoreCase ? elem.toLowerCase().indexOf(entry.toLowerCase()) : elem.indexOf(entry);
                    while (foundPos != -1) {
                        if (foundPos == 0 && elem.length != entry.length) {
                            ret.push("<li><strong>" + elem.substr(0, entry.length) + "</strong>" +
                                elem.substr(entry.length) + "</li>");
                            break;
                        } else if (entry.length >= instance.options.partialChars && instance.options.partialSearch && foundPos != -1) {
                            if (instance.options.fullSearch || /\s/.test(elem.substr(foundPos - 1, 1))) {
                                partial.push("<li>" + elem.substr(0, foundPos) + "<strong>" +
                                    elem.substr(foundPos, entry.length) + "</strong>" + elem.substr(foundPos + entry.length) + "</li>");
                                break;
                            }
                        }
                        foundPos = instance.options.ignoreCase ? elem.toLowerCase().indexOf(entry.toLowerCase(), foundPos + 1) : elem.indexOf(entry, foundPos + 1);
                    }
                }
                if (partial.length)
                    ret = ret.concat(partial.slice(0, instance.options.choices - ret.length));
                return "<ul>" + ret.join('') + "</ul>";
            }
        }, options || {});
    }
});
Field.scrollFreeActivate = function(field) {
    setTimeout(function() {
        Field.activate(field);
    }, 1);
};
Ajax.InPlaceEditor = Class.create({
    initialize: function(element, url, options) {
        this.url = url;
        this.element = element = $(element);
        this.prepareOptions();
        this._controls = {};
        arguments.callee.dealWithDeprecatedOptions(options);
        Object.extend(this.options, options || {});
        if (!this.options.formId && this.element.id) {
            this.options.formId = this.element.id + '-inplaceeditor';
            if ($(this.options.formId))
                this.options.formId = '';
        }
        if (this.options.externalControl)
            this.options.externalControl = $(this.options.externalControl);
        if (!this.options.externalControl)
            this.options.externalControlOnly = false;
        this._originalBackground = this.element.getStyle('background-color') || 'transparent';
        this.element.title = this.options.clickToEditText;
        this._boundCancelHandler = this.handleFormCancellation.bind(this);
        this._boundComplete = (this.options.onComplete || Prototype.emptyFunction).bind(this);
        this._boundFailureHandler = this.handleAJAXFailure.bind(this);
        this._boundSubmitHandler = this.handleFormSubmission.bind(this);
        this._boundWrapperHandler = this.wrapUp.bind(this);
        this.registerListeners();
    },
    checkForEscapeOrReturn: function(e) {
        if (!this._editing || e.ctrlKey || e.altKey || e.shiftKey) return;
        if (Event.KEY_ESC == e.keyCode)
            this.handleFormCancellation(e);
        else if (Event.KEY_RETURN == e.keyCode)
            this.handleFormSubmission(e);
    },
    createControl: function(mode, handler, extraClasses) {
        var control = this.options[mode + 'Control'];
        var text = this.options[mode + 'Text'];
        if ('button' == control) {
            var btn = document.createElement('input');
            btn.type = 'submit';
            btn.value = text;
            btn.className = 'editor_' + mode + '_button';
            if ('cancel' == mode)
                btn.onclick = this._boundCancelHandler;
            this._form.appendChild(btn);
            this._controls[mode] = btn;
        } else if ('link' == control) {
            var link = document.createElement('a');
            link.href = '#';
            link.appendChild(document.createTextNode(text));
            link.onclick = 'cancel' == mode ? this._boundCancelHandler : this._boundSubmitHandler;
            link.className = 'editor_' + mode + '_link';
            if (extraClasses)
                link.className += ' ' + extraClasses;
            this._form.appendChild(link);
            this._controls[mode] = link;
        }
    },
    createEditField: function() {
        var text = (this.options.loadTextURL ? this.options.loadingText : this.getText());
        var fld;
        if (1 >= this.options.rows && !/\r|\n/.test(this.getText())) {
            fld = document.createElement('input');
            fld.type = 'text';
            var size = this.options.size || this.options.cols || 0;
            if (0 < size) fld.size = size;
        } else {
            fld = document.createElement('textarea');
            fld.rows = (1 >= this.options.rows ? this.options.autoRows : this.options.rows);
            fld.cols = this.options.cols || 40;
        }
        fld.name = this.options.paramName;
        fld.value = text;
        fld.className = 'editor_field';
        if (this.options.submitOnBlur)
            fld.onblur = this._boundSubmitHandler;
        this._controls.editor = fld;
        if (this.options.loadTextURL)
            this.loadExternalText();
        this._form.appendChild(this._controls.editor);
    },
    createForm: function() {
        var ipe = this;

        function addText(mode, condition) {
            var text = ipe.options['text' + mode + 'Controls'];
            if (!text || condition === false) return;
            ipe._form.appendChild(document.createTextNode(text));
        };
        this._form = $(document.createElement('form'));
        this._form.id = this.options.formId;
        this._form.addClassName(this.options.formClassName);
        this._form.onsubmit = this._boundSubmitHandler;
        this.createEditField();
        if ('textarea' == this._controls.editor.tagName.toLowerCase())
            this._form.appendChild(document.createElement('br'));
        if (this.options.onFormCustomization)
            this.options.onFormCustomization(this, this._form);
        addText('Before', this.options.okControl || this.options.cancelControl);
        this.createControl('ok', this._boundSubmitHandler);
        addText('Between', this.options.okControl && this.options.cancelControl);
        this.createControl('cancel', this._boundCancelHandler, 'editor_cancel');
        addText('After', this.options.okControl || this.options.cancelControl);
    },
    destroy: function() {
        if (this._oldInnerHTML)
            this.element.innerHTML = this._oldInnerHTML;
        this.leaveEditMode();
        this.unregisterListeners();
    },
    enterEditMode: function(e) {
        if (this._saving || this._editing) return;
        this._editing = true;
        this.triggerCallback('onEnterEditMode');
        if (this.options.externalControl)
            this.options.externalControl.hide();
        this.element.hide();
        this.createForm();
        this.element.parentNode.insertBefore(this._form, this.element);
        if (!this.options.loadTextURL)
            this.postProcessEditField();
        if (e) Event.stop(e);
    },
    enterHover: function(e) {
        if (this.options.hoverClassName)
            this.element.addClassName(this.options.hoverClassName);
        if (this._saving) return;
        this.triggerCallback('onEnterHover');
    },
    getText: function() {
        return this.element.innerHTML.unescapeHTML();
    },
    handleAJAXFailure: function(transport) {
        this.triggerCallback('onFailure', transport);
        if (this._oldInnerHTML) {
            this.element.innerHTML = this._oldInnerHTML;
            this._oldInnerHTML = null;
        }
    },
    handleFormCancellation: function(e) {
        this.wrapUp();
        if (e) Event.stop(e);
    },
    handleFormSubmission: function(e) {
        var form = this._form;
        var value = $F(this._controls.editor);
        this.prepareSubmission();
        var params = this.options.callback(form, value) || '';
        if (Object.isString(params))
            params = params.toQueryParams();
        params.editorId = this.element.id;
        if (this.options.htmlResponse) {
            var options = Object.extend({
                evalScripts: true
            }, this.options.ajaxOptions);
            Object.extend(options, {
                parameters: params,
                onComplete: this._boundWrapperHandler,
                onFailure: this._boundFailureHandler
            });
            new Ajax.Updater({
                success: this.element
            }, this.url, options);
        } else {
            var options = Object.extend({
                method: 'get'
            }, this.options.ajaxOptions);
            Object.extend(options, {
                parameters: params,
                onComplete: this._boundWrapperHandler,
                onFailure: this._boundFailureHandler
            });
            new Ajax.Request(this.url, options);
        }
        if (e) Event.stop(e);
    },
    leaveEditMode: function() {
        this.element.removeClassName(this.options.savingClassName);
        this.removeForm();
        this.leaveHover();
        this.element.style.backgroundColor = this._originalBackground;
        this.element.show();
        if (this.options.externalControl)
            this.options.externalControl.show();
        this._saving = false;
        this._editing = false;
        this._oldInnerHTML = null;
        this.triggerCallback('onLeaveEditMode');
    },
    leaveHover: function(e) {
        if (this.options.hoverClassName)
            this.element.removeClassName(this.options.hoverClassName);
        if (this._saving) return;
        this.triggerCallback('onLeaveHover');
    },
    loadExternalText: function() {
        this._form.addClassName(this.options.loadingClassName);
        this._controls.editor.disabled = true;
        var options = Object.extend({
            method: 'get'
        }, this.options.ajaxOptions);
        Object.extend(options, {
            parameters: 'editorId=' + encodeURIComponent(this.element.id),
            onComplete: Prototype.emptyFunction,
            onSuccess: function(transport) {
                this._form.removeClassName(this.options.loadingClassName);
                var text = transport.responseText;
                if (this.options.stripLoadedTextTags)
                    text = text.stripTags();
                this._controls.editor.value = text;
                this._controls.editor.disabled = false;
                this.postProcessEditField();
            }.bind(this),
            onFailure: this._boundFailureHandler
        });
        new Ajax.Request(this.options.loadTextURL, options);
    },
    postProcessEditField: function() {
        var fpc = this.options.fieldPostCreation;
        if (fpc)
            $(this._controls.editor)['focus' == fpc ? 'focus' : 'activate']();
    },
    prepareOptions: function() {
        this.options = Object.clone(Ajax.InPlaceEditor.DefaultOptions);
        Object.extend(this.options, Ajax.InPlaceEditor.DefaultCallbacks);
        [this._extraDefaultOptions].flatten().compact().each(function(defs) {
            Object.extend(this.options, defs);
        }.bind(this));
    },
    prepareSubmission: function() {
        this._saving = true;
        this.removeForm();
        this.leaveHover();
        this.showSaving();
    },
    registerListeners: function() {
        this._listeners = {};
        var listener;
        $H(Ajax.InPlaceEditor.Listeners).each(function(pair) {
            listener = this[pair.value].bind(this);
            this._listeners[pair.key] = listener;
            if (!this.options.externalControlOnly)
                this.element.observe(pair.key, listener);
            if (this.options.externalControl)
                this.options.externalControl.observe(pair.key, listener);
        }.bind(this));
    },
    removeForm: function() {
        if (!this._form) return;
        this._form.remove();
        this._form = null;
        this._controls = {};
    },
    showSaving: function() {
        this._oldInnerHTML = this.element.innerHTML;
        this.element.innerHTML = this.options.savingText;
        this.element.addClassName(this.options.savingClassName);
        this.element.style.backgroundColor = this._originalBackground;
        this.element.show();
    },
    triggerCallback: function(cbName, arg) {
        if ('function' == typeof this.options[cbName]) {
            this.options[cbName](this, arg);
        }
    },
    unregisterListeners: function() {
        $H(this._listeners).each(function(pair) {
            if (!this.options.externalControlOnly)
                this.element.stopObserving(pair.key, pair.value);
            if (this.options.externalControl)
                this.options.externalControl.stopObserving(pair.key, pair.value);
        }.bind(this));
    },
    wrapUp: function(transport) {
        this.leaveEditMode();
        this._boundComplete(transport, this.element);
    }
});
Object.extend(Ajax.InPlaceEditor.prototype, {
    dispose: Ajax.InPlaceEditor.prototype.destroy
});
Ajax.InPlaceCollectionEditor = Class.create(Ajax.InPlaceEditor, {
    initialize: function($super, element, url, options) {
        this._extraDefaultOptions = Ajax.InPlaceCollectionEditor.DefaultOptions;
        $super(element, url, options);
    },
    createEditField: function() {
        var list = document.createElement('select');
        list.name = this.options.paramName;
        list.size = 1;
        this._controls.editor = list;
        this._collection = this.options.collection || [];
        if (this.options.loadCollectionURL)
            this.loadCollection();
        else
            this.checkForExternalText();
        this._form.appendChild(this._controls.editor);
    },
    loadCollection: function() {
        this._form.addClassName(this.options.loadingClassName);
        this.showLoadingText(this.options.loadingCollectionText);
        var options = Object.extend({
            method: 'get'
        }, this.options.ajaxOptions);
        Object.extend(options, {
            parameters: 'editorId=' + encodeURIComponent(this.element.id),
            onComplete: Prototype.emptyFunction,
            onSuccess: function(transport) {
                var js = transport.responseText.strip();
                if (!/^\[.*\]$/.test(js))
                    throw ('Server returned an invalid collection representation.');
                this._collection = eval(js);
                this.checkForExternalText();
            }.bind(this),
            onFailure: this.onFailure
        });
        new Ajax.Request(this.options.loadCollectionURL, options);
    },
    showLoadingText: function(text) {
        this._controls.editor.disabled = true;
        var tempOption = this._controls.editor.firstChild;
        if (!tempOption) {
            tempOption = document.createElement('option');
            tempOption.value = '';
            this._controls.editor.appendChild(tempOption);
            tempOption.selected = true;
        }
        tempOption.update((text || '').stripScripts().stripTags());
    },
    checkForExternalText: function() {
        this._text = this.getText();
        if (this.options.loadTextURL)
            this.loadExternalText();
        else
            this.buildOptionList();
    },
    loadExternalText: function() {
        this.showLoadingText(this.options.loadingText);
        var options = Object.extend({
            method: 'get'
        }, this.options.ajaxOptions);
        Object.extend(options, {
            parameters: 'editorId=' + encodeURIComponent(this.element.id),
            onComplete: Prototype.emptyFunction,
            onSuccess: function(transport) {
                this._text = transport.responseText.strip();
                this.buildOptionList();
            }.bind(this),
            onFailure: this.onFailure
        });
        new Ajax.Request(this.options.loadTextURL, options);
    },
    buildOptionList: function() {
        this._form.removeClassName(this.options.loadingClassName);
        this._collection = this._collection.map(function(entry) {
            return 2 === entry.length ? entry : [entry, entry].flatten();
        });
        var marker = ('value' in this.options) ? this.options.value : this._text;
        var textFound = this._collection.any(function(entry) {
            return entry[0] == marker;
        }.bind(this));
        this._controls.editor.update('');
        var option;
        this._collection.each(function(entry, index) {
            option = document.createElement('option');
            option.value = entry[0];
            option.selected = textFound ? entry[0] == marker : 0 == index;
            option.appendChild(document.createTextNode(entry[1]));
            this._controls.editor.appendChild(option);
        }.bind(this));
        this._controls.editor.disabled = false;
        Field.scrollFreeActivate(this._controls.editor);
    }
});
Ajax.InPlaceEditor.prototype.initialize.dealWithDeprecatedOptions = function(options) {
    if (!options) return;

    function fallback(name, expr) {
        if (name in options || expr === undefined) return;
        options[name] = expr;
    };
    fallback('cancelControl', (options.cancelLink ? 'link' : (options.cancelButton ? 'button' : options.cancelLink == options.cancelButton == false ? false : undefined)));
    fallback('okControl', (options.okLink ? 'link' : (options.okButton ? 'button' : options.okLink == options.okButton == false ? false : undefined)));
    fallback('highlightColor', options.highlightcolor);
    fallback('highlightEndColor', options.highlightendcolor);
};
Object.extend(Ajax.InPlaceEditor, {
    DefaultOptions: {
        ajaxOptions: {},
        autoRows: 3,
        cancelControl: 'link',
        cancelText: 'cancel',
        clickToEditText: 'Click to edit',
        externalControl: null,
        externalControlOnly: false,
        fieldPostCreation: 'activate',
        formClassName: 'inplaceeditor-form',
        formId: null,
        highlightColor: '#ffff99',
        highlightEndColor: '#ffffff',
        hoverClassName: '',
        htmlResponse: true,
        loadingClassName: 'inplaceeditor-loading',
        loadingText: 'Loading...',
        okControl: 'button',
        okText: 'ok',
        paramName: 'value',
        rows: 1,
        savingClassName: 'inplaceeditor-saving',
        savingText: 'Saving...',
        size: 0,
        stripLoadedTextTags: false,
        submitOnBlur: false,
        textAfterControls: '',
        textBeforeControls: '',
        textBetweenControls: ''
    },
    DefaultCallbacks: {
        callback: function(form) {
            return Form.serialize(form);
        },
        onComplete: function(transport, element) {
            new Effect.Highlight(element, {
                startcolor: this.options.highlightColor,
                keepBackgroundImage: true
            });
        },
        onEnterEditMode: null,
        onEnterHover: function(ipe) {
            ipe.element.style.backgroundColor = ipe.options.highlightColor;
            if (ipe._effect)
                ipe._effect.cancel();
        },
        onFailure: function(transport, ipe) {
            alert('Error communication with the server: ' + transport.responseText.stripTags());
        },
        onFormCustomization: null,
        onLeaveEditMode: null,
        onLeaveHover: function(ipe) {
            ipe._effect = new Effect.Highlight(ipe.element, {
                startcolor: ipe.options.highlightColor,
                endcolor: ipe.options.highlightEndColor,
                restorecolor: ipe._originalBackground,
                keepBackgroundImage: true
            });
        }
    },
    Listeners: {
        click: 'enterEditMode',
        keydown: 'checkForEscapeOrReturn',
        mouseover: 'enterHover',
        mouseout: 'leaveHover'
    }
});
Ajax.InPlaceCollectionEditor.DefaultOptions = {
    loadingCollectionText: 'Loading options...'
};
Form.Element.DelayedObserver = Class.create({
    initialize: function(element, delay, callback) {
        this.delay = delay || 0.5;
        this.element = $(element);
        this.callback = callback;
        this.timer = null;
        this.lastValue = $F(this.element);
        Event.observe(this.element, 'keyup', this.delayedListener.bindAsEventListener(this));
    },
    delayedListener: function(event) {
        if (this.lastValue == $F(this.element)) return;
        if (this.timer) clearTimeout(this.timer);
        this.timer = setTimeout(this.onTimerEvent.bind(this), this.delay * 1000);
        this.lastValue = $F(this.element);
    },
    onTimerEvent: function() {
        this.timer = null;
        this.callback(this.element, $F(this.element));
    }
});
if (!Control) var Control = {};
Control.Slider = Class.create({
    initialize: function(handle, track, options) {
        var slider = this;
        if (Object.isArray(handle)) {
            this.handles = handle.collect(function(e) {
                return $(e)
            });
        } else {
            this.handles = [$(handle)];
        }
        this.track = $(track);
        this.options = options || {};
        this.axis = this.options.axis || 'horizontal';
        this.increment = this.options.increment || 1;
        this.step = parseInt(this.options.step || '1');
        this.range = this.options.range || $R(0, 1);
        this.value = 0;
        this.values = this.handles.map(function() {
            return 0
        });
        this.spans = this.options.spans ? this.options.spans.map(function(s) {
            return $(s)
        }) : false;
        this.options.startSpan = $(this.options.startSpan || null);
        this.options.endSpan = $(this.options.endSpan || null);
        this.restricted = this.options.restricted || false;
        this.maximum = this.options.maximum || this.range.end;
        this.minimum = this.options.minimum || this.range.start;
        this.alignX = parseInt(this.options.alignX || '0');
        this.alignY = parseInt(this.options.alignY || '0');
        this.trackLength = this.maximumOffset() - this.minimumOffset();
        this.handleLength = this.isVertical() ? (this.handles[0].offsetHeight != 0 ? this.handles[0].offsetHeight : this.handles[0].style.height.replace(/px$/, "")) : (this.handles[0].offsetWidth != 0 ? this.handles[0].offsetWidth : this.handles[0].style.width.replace(/px$/, ""));
        this.active = false;
        this.dragging = false;
        this.disabled = false;
        if (this.options.disabled) this.setDisabled();
        this.allowedValues = this.options.values ? this.options.values.sortBy(Prototype.K) : false;
        if (this.allowedValues) {
            this.minimum = this.allowedValues.min();
            this.maximum = this.allowedValues.max();
        }
        this.eventMouseDown = this.startDrag.bindAsEventListener(this);
        this.eventMouseUp = this.endDrag.bindAsEventListener(this);
        this.eventMouseMove = this.update.bindAsEventListener(this);
        this.handles.each(function(h, i) {
            i = slider.handles.length - 1 - i;
            slider.setValue(parseFloat((Object.isArray(slider.options.sliderValue) ? slider.options.sliderValue[i] : slider.options.sliderValue) || slider.range.start), i);
            h.makePositioned().observe("mousedown", slider.eventMouseDown);
        });
        this.track.observe("mousedown", this.eventMouseDown);
        document.observe("mouseup", this.eventMouseUp);
        $(this.track.parentNode.parentNode).observe("mousemove", this.eventMouseMove);
        this.initialized = true;
    },
    dispose: function() {
        var slider = this;
        Event.stopObserving(this.track, "mousedown", this.eventMouseDown);
        Event.stopObserving(document, "mouseup", this.eventMouseUp);
        Event.stopObserving(this.track.parentNode.parentNode, "mousemove", this.eventMouseMove);
        this.handles.each(function(h) {
            Event.stopObserving(h, "mousedown", slider.eventMouseDown);
        });
    },
    setDisabled: function() {
        this.disabled = true;
        this.track.parentNode.className = this.track.parentNode.className + ' disabled';
    },
    setEnabled: function() {
        this.disabled = false;
    },
    getNearestValue: function(value) {
        if (this.allowedValues) {
            if (value >= this.allowedValues.max()) return (this.allowedValues.max());
            if (value <= this.allowedValues.min()) return (this.allowedValues.min());
            var offset = Math.abs(this.allowedValues[0] - value);
            var newValue = this.allowedValues[0];
            this.allowedValues.each(function(v) {
                var currentOffset = Math.abs(v - value);
                if (currentOffset <= offset) {
                    newValue = v;
                    offset = currentOffset;
                }
            });
            return newValue;
        }
        if (value > this.range.end) return this.range.end;
        if (value < this.range.start) return this.range.start;
        return value;
    },
    setValue: function(sliderValue, handleIdx) {
        if (!this.active) {
            this.activeHandleIdx = handleIdx || 0;
            this.activeHandle = this.handles[this.activeHandleIdx];
            this.updateStyles();
        }
        handleIdx = handleIdx || this.activeHandleIdx || 0;
        if (this.initialized && this.restricted) {
            if ((handleIdx > 0) && (sliderValue < this.values[handleIdx - 1]))
                sliderValue = this.values[handleIdx - 1];
            if ((handleIdx < (this.handles.length - 1)) && (sliderValue > this.values[handleIdx + 1]))
                sliderValue = this.values[handleIdx + 1];
        }
        sliderValue = this.getNearestValue(sliderValue);
        this.values[handleIdx] = sliderValue;
        this.value = this.values[0];
        this.handles[handleIdx].style[this.isVertical() ? 'top' : 'left'] = this.translateToPx(sliderValue);
        this.drawSpans();
        if (!this.dragging || !this.event) this.updateFinished();
    },
    setValueBy: function(delta, handleIdx) {
        this.setValue(this.values[handleIdx || this.activeHandleIdx || 0] + delta, handleIdx || this.activeHandleIdx || 0);
    },
    translateToPx: function(value) {
        return Math.round(((this.trackLength - this.handleLength) / (this.range.end - this.range.start)) * (value - this.range.start)) + "px";
    },
    translateToValue: function(offset) {
        return ((offset / (this.trackLength - this.handleLength) * (this.range.end - this.range.start)) + this.range.start);
    },
    getRange: function(range) {
        var v = this.values.sortBy(Prototype.K);
        range = range || 0;
        return $R(v[range], v[range + 1]);
    },
    minimumOffset: function() {
        return (this.isVertical() ? this.alignY : this.alignX);
    },
    maximumOffset: function() {
        return (this.isVertical() ? (this.track.offsetHeight != 0 ? this.track.offsetHeight : this.track.style.height.replace(/px$/, "")) - this.alignY : (this.track.offsetWidth != 0 ? this.track.offsetWidth : this.track.style.width.replace(/px$/, "")) - this.alignX);
    },
    isVertical: function() {
        return (this.axis == 'vertical');
    },
    drawSpans: function() {
        var slider = this;
        if (this.spans)
            $R(0, this.spans.length - 1).each(function(r) {
                slider.setSpan(slider.spans[r], slider.getRange(r))
            });
        if (this.options.startSpan)
            this.setSpan(this.options.startSpan, $R(0, this.values.length > 1 ? this.getRange(0).min() : this.value));
        if (this.options.endSpan)
            this.setSpan(this.options.endSpan, $R(this.values.length > 1 ? this.getRange(this.spans.length - 1).max() : this.value, this.maximum));
    },
    setSpan: function(span, range) {
        if (this.isVertical()) {
            span.style.top = this.translateToPx(range.start);
            span.style.height = this.translateToPx(range.end - range.start + this.range.start);
        } else {
            span.style.left = this.translateToPx(range.start);
            span.style.width = this.translateToPx(range.end - range.start + this.range.start);
        }
    },
    updateStyles: function() {
        this.handles.each(function(h) {
            Element.removeClassName(h, 'selected')
        });
        Element.addClassName(this.activeHandle, 'selected');
    },
    startDrag: function(event) {
        if (Event.isLeftClick(event)) {
            if (!this.disabled) {
                this.active = true;
                var handle = Event.element(event);
                var pointer = [Event.pointerX(event), Event.pointerY(event)];
                var track = handle;
                if (track == this.track) {
                    var offsets = Position.cumulativeOffset(this.track);
                    this.event = event;
                    this.setValue(this.translateToValue((this.isVertical() ? pointer[1] - offsets[1] : pointer[0] - offsets[0]) - (this.handleLength / 2)));
                    var offsets = Position.cumulativeOffset(this.activeHandle);
                    this.offsetX = (pointer[0] - offsets[0]);
                    this.offsetY = (pointer[1] - offsets[1]);
                } else {
                    while ((this.handles.indexOf(handle) == -1) && handle.parentNode)
                        handle = handle.parentNode;
                    if (this.handles.indexOf(handle) != -1) {
                        this.activeHandle = handle;
                        this.activeHandleIdx = this.handles.indexOf(this.activeHandle);
                        this.updateStyles();
                        var offsets = Position.cumulativeOffset(this.activeHandle);
                        this.offsetX = (pointer[0] - offsets[0]);
                        this.offsetY = (pointer[1] - offsets[1]);
                    }
                }
            }
            Event.stop(event);
        }
    },
    update: function(event) {
        if (this.active) {
            if (!this.dragging) this.dragging = true;
            this.draw(event);
            if (Prototype.Browser.WebKit) window.scrollBy(0, 0);
            Event.stop(event);
        }
    },
    draw: function(event) {
        var pointer = [Event.pointerX(event), Event.pointerY(event)];
        var offsets = Position.cumulativeOffset(this.track);
        pointer[0] -= this.offsetX + offsets[0];
        pointer[1] -= this.offsetY + offsets[1];
        this.event = event;
        this.setValue(this.translateToValue(this.isVertical() ? pointer[1] : pointer[0]));
        if (this.initialized && this.options.onSlide)
            this.options.onSlide(this.values.length > 1 ? this.values : this.value, this);
    },
    endDrag: function(event) {
        if (this.active && this.dragging) {
            this.finishDrag(event, true);
            Event.stop(event);
        }
        this.active = false;
        this.dragging = false;
    },
    finishDrag: function(event, success) {
        this.active = false;
        this.dragging = false;
        this.updateFinished();
    },
    updateFinished: function() {
        if (this.initialized && this.options.onChange)
            this.options.onChange(this.values.length > 1 ? this.values : this.value, this);
        this.event = null;
    }
});

function popWin(url, win, para) {
    var win = window.open(url, win, para);
    win.focus();
}

function setLocation(url) {
    window.location.href = url;
}

function setPLocation(url, setFocus) {
    if (setFocus) {
        window.opener.focus();
    }
    window.opener.location.href = url;
}

function setLanguageCode(code, fromCode) {
    var href = window.location.href;
    var after = '',
        dash;
    if (dash = href.match(/\#(.*)$/)) {
        href = href.replace(/\#(.*)$/, '');
        after = dash[0];
    }
    if (href.match(/[?]/)) {
        var re = /([?&]store=)[a-z0-9_]*/;
        if (href.match(re)) {
            href = href.replace(re, '$1' + code);
        } else {
            href += '&store=' + code;
        }
        var re = /([?&]from_store=)[a-z0-9_]*/;
        if (href.match(re)) {
            href = href.replace(re, '');
        }
    } else {
        href += '?store=' + code;
    }
    if (typeof(fromCode) != 'undefined') {
        href += '&from_store=' + fromCode;
    }
    href += after;
    setLocation(href);
}

function decorateGeneric(elements, decorateParams) {
    var allSupportedParams = ['odd', 'even', 'first', 'last'];
    var _decorateParams = {};
    var total = elements.length;
    if (total) {
        if (typeof(decorateParams) == 'undefined') {
            decorateParams = allSupportedParams;
        }
        if (!decorateParams.length) {
            return;
        }
        for (var k in allSupportedParams) {
            _decorateParams[allSupportedParams[k]] = false;
        }
        for (var k in decorateParams) {
            _decorateParams[decorateParams[k]] = true;
        }
        if (_decorateParams.first) {
            Element.addClassName(elements[0], 'first');
        }
        if (_decorateParams.last) {
            Element.addClassName(elements[total - 1], 'last');
        }
        for (var i = 0; i < total; i++) {
            if ((i + 1) % 2 == 0) {
                if (_decorateParams.even) {
                    Element.addClassName(elements[i], 'even');
                }
            } else {
                if (_decorateParams.odd) {
                    Element.addClassName(elements[i], 'odd');
                }
            }
        }
    }
}

function decorateTable(table, options) {
    var table = $(table);
    if (table) {
        var _options = {
            'tbody': false,
            'tbody tr': ['odd', 'even', 'first', 'last'],
            'thead tr': ['first', 'last'],
            'tfoot tr': ['first', 'last'],
            'tr td': ['last']
        };
        if (typeof(options) != 'undefined') {
            for (var k in options) {
                _options[k] = options[k];
            }
        }
        if (_options['tbody']) {
            decorateGeneric(table.select('tbody'), _options['tbody']);
        }
        if (_options['tbody tr']) {
            decorateGeneric(table.select('tbody tr'), _options['tbody tr']);
        }
        if (_options['thead tr']) {
            decorateGeneric(table.select('thead tr'), _options['thead tr']);
        }
        if (_options['tfoot tr']) {
            decorateGeneric(table.select('tfoot tr'), _options['tfoot tr']);
        }
        if (_options['tr td']) {
            var allRows = table.select('tr');
            if (allRows.length) {
                for (var i = 0; i < allRows.length; i++) {
                    decorateGeneric(allRows[i].getElementsByTagName('TD'), _options['tr td']);
                }
            }
        }
    }
}

function decorateList(list, nonRecursive) {
    if ($(list)) {
        if (typeof(nonRecursive) == 'undefined') {
            var items = $(list).select('li')
        } else {
            var items = $(list).childElements();
        }
        decorateGeneric(items, ['odd', 'even', 'last']);
    }
}

function decorateDataList(list) {
    list = $(list);
    if (list) {
        decorateGeneric(list.select('dt'), ['odd', 'even', 'last']);
        decorateGeneric(list.select('dd'), ['odd', 'even', 'last']);
    }
}

function parseSidUrl(baseUrl, urlExt) {
    var sidPos = baseUrl.indexOf('/?SID=');
    var sid = '';
    urlExt = (urlExt != undefined) ? urlExt : '';
    if (sidPos > -1) {
        sid = '?' + baseUrl.substring(sidPos + 2);
        baseUrl = baseUrl.substring(0, sidPos + 1);
    }
    return baseUrl + urlExt + sid;
}

function formatCurrency(price, format, showPlus) {
    var precision = isNaN(format.precision = Math.abs(format.precision)) ? 2 : format.precision;
    var requiredPrecision = isNaN(format.requiredPrecision = Math.abs(format.requiredPrecision)) ? 2 : format.requiredPrecision;
    precision = requiredPrecision;
    var integerRequired = isNaN(format.integerRequired = Math.abs(format.integerRequired)) ? 1 : format.integerRequired;
    var decimalSymbol = format.decimalSymbol == undefined ? "," : format.decimalSymbol;
    var groupSymbol = format.groupSymbol == undefined ? "." : format.groupSymbol;
    var groupLength = format.groupLength == undefined ? 3 : format.groupLength;
    var s = '';
    if (showPlus == undefined || showPlus == true) {
        s = price < 0 ? "-" : (showPlus ? "+" : "");
    } else if (showPlus == false) {
        s = '';
    }
    var i = parseInt(price = Math.abs(+price || 0).toFixed(precision)) + "";
    var pad = (i.length < integerRequired) ? (integerRequired - i.length) : 0;
    while (pad) {
        i = '0' + i;
        pad--;
    }
    j = (j = i.length) > groupLength ? j % groupLength : 0;
    re = new RegExp("(\\d{" + groupLength + "})(?=\\d)", "g");
    var r = (j ? i.substr(0, j) + groupSymbol : "") + i.substr(j).replace(re, "$1" + groupSymbol) + (precision ? decimalSymbol + Math.abs(price - i).toFixed(precision).replace(/-/, 0).slice(2) : "")
    var pattern = '';
    if (format.pattern.indexOf('{sign}') == -1) {
        pattern = s + format.pattern;
    } else {
        pattern = format.pattern.replace('{sign}', s);
    }
    return pattern.replace('%s', r).replace(/^\s\s*/, '').replace(/\s\s*$/, '');
};

function expandDetails(el, childClass) {
    if (Element.hasClassName(el, 'show-details')) {
        $$(childClass).each(function(item) {
            item.hide()
        });
        Element.removeClassName(el, 'show-details');
    } else {
        $$(childClass).each(function(item) {
            item.show()
        });
        Element.addClassName(el, 'show-details');
    }
}
var isIE = navigator.appVersion.match(/MSIE/) == "MSIE";
if (!window.Varien)
    var Varien = new Object();
Varien.showLoading = function() {
    var loader = $('loading-process');
    loader && loader.show();
}
Varien.hideLoading = function() {
    var loader = $('loading-process');
    loader && loader.hide();
}
Varien.GlobalHandlers = {
    onCreate: function() {
        Varien.showLoading();
    },
    onComplete: function() {
        if (Ajax.activeRequestCount == 0) {
            Varien.hideLoading();
        }
    }
};
Ajax.Responders.register(Varien.GlobalHandlers);
Varien.searchForm = Class.create();
Varien.searchForm.prototype = {
    initialize: function(form, field, emptyText) {
        this.form = $(form);
        this.field = $(field);
        this.emptyText = emptyText;
        Event.observe(this.form, 'submit', this.submit.bind(this));
        Event.observe(this.field, 'focus', this.focus.bind(this));
        Event.observe(this.field, 'blur', this.blur.bind(this));
        this.blur();
    },
    submit: function(event) {
        if (this.field.value == this.emptyText || this.field.value == '') {
            Event.stop(event);
            return false;
        }
        return true;
    },
    focus: function(event) {
        if (this.field.value == this.emptyText) {
            this.field.value = '';
        }
    },
    blur: function(event) {
        if (this.field.value == '') {
            this.field.value = this.emptyText;
        }
    },
    initAutocomplete: function(url, destinationElement) {
        new Ajax.Autocompleter(this.field, destinationElement, url, {
            paramName: this.field.name,
            method: 'get',
            minChars: 2,
            updateElement: this._selectAutocompleteItem.bind(this),
            onShow: function(element, update) {
                if (!update.style.position || update.style.position == 'absolute') {
                    update.style.position = 'absolute';
                    Position.clone(element, update, {
                        setHeight: false,
                        offsetTop: element.offsetHeight
                    });
                }
                Effect.Appear(update, {
                    duration: 0
                });
            }
        });
    },
    _selectAutocompleteItem: function(element) {
        if (element.title) {
            this.field.value = element.title;
        }
        this.form.submit();
    }
}
Varien.Tabs = Class.create();
Varien.Tabs.prototype = {
    initialize: function(selector) {
        var self = this;
        $$(selector + ' a').each(this.initTab.bind(this));
    },
    initTab: function(el) {
        el.href = 'javascript:void(0)';
        if ($(el.parentNode).hasClassName('active')) {
            this.showContent(el);
        }
        el.observe('click', this.showContent.bind(this, el));
    },
    showContent: function(a) {
        var li = $(a.parentNode),
            ul = $(li.parentNode);
        ul.getElementsBySelector('li', 'ol').each(function(el) {
            var contents = $(el.id + '_contents');
            if (el == li) {
                el.addClassName('active');
                contents.show();
            } else {
                el.removeClassName('active');
                contents.hide();
            }
        });
    }
}
Varien.DateElement = Class.create();
Varien.DateElement.prototype = {
    initialize: function(type, content, required, format) {
        if (type == 'id') {
            this.day = $(content + 'day');
            this.month = $(content + 'month');
            this.year = $(content + 'year');
            this.full = $(content + 'full');
            this.advice = $(content + 'date-advice');
        } else if (type == 'container') {
            this.day = content.day;
            this.month = content.month;
            this.year = content.year;
            this.full = content.full;
            this.advice = content.advice;
        } else {
            return;
        }
        this.required = required;
        this.format = format;
        this.day.addClassName('validate-custom');
        this.day.validate = this.validate.bind(this);
        this.month.addClassName('validate-custom');
        this.month.validate = this.validate.bind(this);
        this.year.addClassName('validate-custom');
        this.year.validate = this.validate.bind(this);
        this.setDateRange(false, false);
        this.year.setAttribute('autocomplete', 'off');
        this.advice.hide();
    },
    validate: function() {
        var error = false,
            day = parseInt(this.day.value, 10) || 0,
            month = parseInt(this.month.value, 10) || 0,
            year = parseInt(this.year.value, 10) || 0;
        if (this.day.value.strip().empty() && this.month.value.strip().empty() && this.year.value.strip().empty()) {
            if (this.required) {
                error = 'This date is a required value.';
            } else {
                this.full.value = '';
            }
        } else if (!day || !month || !year) {
            error = 'Please enter a valid full date.';
        } else {
            var date = new Date,
                countDaysInMonth = 0,
                errorType = null;
            date.setYear(year);
            date.setMonth(month - 1);
            date.setDate(32);
            countDaysInMonth = 32 - date.getDate();
            if (!countDaysInMonth || countDaysInMonth > 31) countDaysInMonth = 31;
            if (day < 1 || day > countDaysInMonth) {
                errorType = 'day';
                error = 'Please enter a valid day (1-%d).';
            } else if (month < 1 || month > 12) {
                errorType = 'month';
                error = 'Please enter a valid month (1-12).';
            } else {
                if (day % 10 == day) this.day.value = '0' + day;
                if (month % 10 == month) this.month.value = '0' + month;
                this.full.value = this.format.replace(/%[mb]/i, this.month.value).replace(/%[de]/i, this.day.value).replace(/%y/i, this.year.value);
                var testFull = this.month.value + '/' + this.day.value + '/' + this.year.value;
                var test = new Date(testFull);
                if (isNaN(test)) {
                    error = 'Please enter a valid date.';
                } else {
                    this.setFullDate(test);
                }
            }
            var valueError = false;
            if (!error && !this.validateData()) {
                errorType = this.validateDataErrorType;
                valueError = this.validateDataErrorText;
                error = valueError;
            }
        }
        if (error !== false) {
            try {
                error = Translator.translate(error);
            } catch (e) {}
            if (!valueError) {
                this.advice.innerHTML = error.replace('%d', countDaysInMonth);
            } else {
                this.advice.innerHTML = this.errorTextModifier(error);
            }
            this.advice.show();
            return false;
        }
        this.day.removeClassName('validation-failed');
        this.month.removeClassName('validation-failed');
        this.year.removeClassName('validation-failed');
        this.advice.hide();
        return true;
    },
    validateData: function() {
        var year = this.fullDate.getFullYear();
        var date = new Date;
        this.curyear = date.getFullYear();
        return (year >= 1900 && year <= this.curyear);
    },
    validateDataErrorType: 'year',
    validateDataErrorText: 'Please enter a valid year (1900-%d).',
    errorTextModifier: function(text) {
        return text.replace('%d', this.curyear);
    },
    setDateRange: function(minDate, maxDate) {
        this.minDate = minDate;
        this.maxDate = maxDate;
    },
    setFullDate: function(date) {
        this.fullDate = date;
    }
};
Varien.DOB = Class.create();
Varien.DOB.prototype = {
    initialize: function(selector, required, format) {
        var el = $$(selector)[0];
        var container = {};
        container.day = Element.select(el, '.dob-day input')[0];
        container.month = Element.select(el, '.dob-month input')[0];
        container.year = Element.select(el, '.dob-year input')[0];
        container.full = Element.select(el, '.dob-full input')[0];
        container.advice = Element.select(el, '.validation-advice')[0];
        new Varien.DateElement('container', container, required, format);
    }
};
Varien.dateRangeDate = Class.create();
Varien.dateRangeDate.prototype = Object.extend(new Varien.DateElement(), {
    validateData: function() {
        var validate = true;
        if (this.minDate || this.maxValue) {
            if (this.minDate) {
                this.minDate = new Date(this.minDate);
                this.minDate.setHours(0);
                if (isNaN(this.minDate)) {
                    this.minDate = new Date('1/1/1900');
                }
                validate = validate && (this.fullDate >= this.minDate)
            }
            if (this.maxDate) {
                this.maxDate = new Date(this.maxDate)
                this.minDate.setHours(0);
                if (isNaN(this.maxDate)) {
                    this.maxDate = new Date();
                }
                validate = validate && (this.fullDate <= this.maxDate)
            }
            if (this.maxDate && this.minDate) {
                this.validateDataErrorText = 'Please enter a valid date between %s and %s';
            } else if (this.maxDate) {
                this.validateDataErrorText = 'Please enter a valid date less than or equal to %s';
            } else if (this.minDate) {
                this.validateDataErrorText = 'Please enter a valid date equal to or greater than %s';
            } else {
                this.validateDataErrorText = '';
            }
        }
        return validate;
    },
    validateDataErrorText: 'Date should be between %s and %s',
    errorTextModifier: function(text) {
        if (this.minDate) {
            text = text.sub('%s', this.dateFormat(this.minDate));
        }
        if (this.maxDate) {
            text = text.sub('%s', this.dateFormat(this.maxDate));
        }
        return text;
    },
    dateFormat: function(date) {
        return (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
    }
});
Varien.FileElement = Class.create();
Varien.FileElement.prototype = {
    initialize: function(id) {
        this.fileElement = $(id);
        this.hiddenElement = $(id + '_value');
        this.fileElement.observe('change', this.selectFile.bind(this));
    },
    selectFile: function(event) {
        this.hiddenElement.value = this.fileElement.getValue();
    }
};
Validation.addAllThese([
    ['validate-custom', ' ', function(v, elm) {
        return elm.validate();
    }]
]);

function truncateOptions() {
    $$('.truncated').each(function(element) {
        Event.observe(element, 'mouseover', function() {
            if (element.down('div.truncated_full_value')) {
                element.down('div.truncated_full_value').addClassName('show')
            }
        });
        Event.observe(element, 'mouseout', function() {
            if (element.down('div.truncated_full_value')) {
                element.down('div.truncated_full_value').removeClassName('show')
            }
        });
    });
}
Event.observe(window, 'load', function() {
    truncateOptions();
});
Element.addMethods({
    getInnerText: function(element) {
        element = $(element);
        if (element.innerText && !Prototype.Browser.Opera) {
            return element.innerText
        }
        return element.innerHTML.stripScripts().unescapeHTML().replace(/[\n\r\s]+/g, ' ').strip();
    }
});

function fireEvent(element, event) {
    if (document.createEvent) {
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent(event, true, true);
        return element.dispatchEvent(evt);
    } else {
        var evt = document.createEventObject();
        return element.fireEvent('on' + event, evt)
    }
}

function modulo(dividend, divisor) {
    var epsilon = divisor / 10000;
    var remainder = dividend % divisor;
    if (Math.abs(remainder - divisor) < epsilon || Math.abs(remainder) < epsilon) {
        remainder = 0;
    }
    return remainder;
}
if ((typeof Range != "undefined") && !Range.prototype.createContextualFragment) {
    Range.prototype.createContextualFragment = function(html) {
        var frag = document.createDocumentFragment(),
            div = document.createElement("div");
        frag.appendChild(div);
        div.outerHTML = html;
        return frag;
    };
}
VarienForm = Class.create();
VarienForm.prototype = {
    initialize: function(formId, firstFieldFocus) {
        this.form = $(formId);
        if (!this.form) {
            return;
        }
        this.cache = $A();
        this.currLoader = false;
        this.currDataIndex = false;
        this.validator = new Validation(this.form);
        this.elementFocus = this.elementOnFocus.bindAsEventListener(this);
        this.elementBlur = this.elementOnBlur.bindAsEventListener(this);
        this.childLoader = this.onChangeChildLoad.bindAsEventListener(this);
        this.highlightClass = 'highlight';
        this.extraChildParams = '';
        this.firstFieldFocus = firstFieldFocus || false;
        this.bindElements();
        if (this.firstFieldFocus) {
            try {
                Form.Element.focus(Form.findFirstElement(this.form))
            } catch (e) {}
        }
    },
    submit: function(url) {
        if (this.validator && this.validator.validate()) {
            this.form.submit();
        }
        return false;
    },
    bindElements: function() {
        var elements = Form.getElements(this.form);
        for (var row in elements) {
            if (elements[row].id) {
                Event.observe(elements[row], 'focus', this.elementFocus);
                Event.observe(elements[row], 'blur', this.elementBlur);
            }
        }
    },
    elementOnFocus: function(event) {
        var element = Event.findElement(event, 'fieldset');
        if (element) {
            Element.addClassName(element, this.highlightClass);
        }
    },
    elementOnBlur: function(event) {
        var element = Event.findElement(event, 'fieldset');
        if (element) {
            Element.removeClassName(element, this.highlightClass);
        }
    },
    setElementsRelation: function(parent, child, dataUrl, first) {
        if (parent = $(parent)) {
            if (!this.cache[parent.id]) {
                this.cache[parent.id] = $A();
                this.cache[parent.id]['child'] = child;
                this.cache[parent.id]['dataUrl'] = dataUrl;
                this.cache[parent.id]['data'] = $A();
                this.cache[parent.id]['first'] = first || false;
            }
            Event.observe(parent, 'change', this.childLoader);
        }
    },
    onChangeChildLoad: function(event) {
        element = Event.element(event);
        this.elementChildLoad(element);
    },
    elementChildLoad: function(element, callback) {
        this.callback = callback || false;
        if (element.value) {
            this.currLoader = element.id;
            this.currDataIndex = element.value;
            if (this.cache[element.id]['data'][element.value]) {
                this.setDataToChild(this.cache[element.id]['data'][element.value]);
            } else {
                new Ajax.Request(this.cache[this.currLoader]['dataUrl'], {
                    method: 'post',
                    parameters: {
                        "parent": element.value
                    },
                    onComplete: this.reloadChildren.bind(this)
                });
            }
        }
    },
    reloadChildren: function(transport) {
        var data = eval('(' + transport.responseText + ')');
        this.cache[this.currLoader]['data'][this.currDataIndex] = data;
        this.setDataToChild(data);
    },
    setDataToChild: function(data) {
        if (data.length) {
            var child = $(this.cache[this.currLoader]['child']);
            if (child) {
                var html = '<select name="' + child.name + '" id="' + child.id + '" class="' + child.className + '" title="' + child.title + '" ' + this.extraChildParams + '>';
                if (this.cache[this.currLoader]['first']) {
                    html += '<option value="">' + this.cache[this.currLoader]['first'] + '</option>';
                }
                for (var i in data) {
                    if (data[i].value) {
                        html += '<option value="' + data[i].value + '"';
                        if (child.value && (child.value == data[i].value || child.value == data[i].label)) {
                            html += ' selected';
                        }
                        html += '>' + data[i].label + '</option>';
                    }
                }
                html += '</select>';
                Element.insert(child, {
                    before: html
                });
                Element.remove(child);
            }
        } else {
            var child = $(this.cache[this.currLoader]['child']);
            if (child) {
                var html = '<input type="text" name="' + child.name + '" id="' + child.id + '" class="' + child.className + '" title="' + child.title + '" ' + this.extraChildParams + '>';
                Element.insert(child, {
                    before: html
                });
                Element.remove(child);
            }
        }
        this.bindElements();
        if (this.callback) {
            this.callback();
        }
    }
}
RegionUpdater = Class.create();
RegionUpdater.prototype = {
    initialize: function(countryEl, regionTextEl, regionSelectEl, regions, disableAction, zipEl) {
        this.countryEl = $(countryEl);
        this.regionTextEl = $(regionTextEl);
        this.regionSelectEl = $(regionSelectEl);
        this.zipEl = $(zipEl);
        this.config = regions['config'];
        delete regions.config;
        this.regions = regions;
        this.disableAction = (typeof disableAction == 'undefined') ? 'hide' : disableAction;
        this.zipOptions = (typeof zipOptions == 'undefined') ? false : zipOptions;
        if (this.regionSelectEl.options.length <= 1) {
            this.update();
        }
        Event.observe(this.countryEl, 'change', this.update.bind(this));
    },
    _checkRegionRequired: function() {
        var label, wildCard;
        var elements = [this.regionTextEl, this.regionSelectEl];
        var that = this;
        if (typeof this.config == 'undefined') {
            return;
        }
        var regionRequired = this.config.regions_required.indexOf(this.countryEl.value) >= 0;
        elements.each(function(currentElement) {
            Validation.reset(currentElement);
            label = $$('label[for="' + currentElement.id + '"]')[0];
            if (label) {
                wildCard = label.down('em') || label.down('span.required');
                if (!that.config.show_all_regions) {
                    if (regionRequired) {
                        label.up().show();
                    } else {
                        label.up().hide();
                    }
                }
            }
            if (label && wildCard) {
                if (!regionRequired) {
                    wildCard.hide();
                    if (label.hasClassName('required')) {
                        label.removeClassName('required');
                    }
                } else if (regionRequired) {
                    wildCard.show();
                    if (!label.hasClassName('required')) {
                        label.addClassName('required')
                    }
                }
            }
            if (!regionRequired) {
                if (currentElement.hasClassName('required-entry')) {
                    currentElement.removeClassName('required-entry');
                }
                if ('select' == currentElement.tagName.toLowerCase() && currentElement.hasClassName('validate-select')) {
                    currentElement.removeClassName('validate-select');
                }
            } else {
                if (!currentElement.hasClassName('required-entry')) {
                    currentElement.addClassName('required-entry');
                }
                if ('select' == currentElement.tagName.toLowerCase() && !currentElement.hasClassName('validate-select')) {
                    currentElement.addClassName('validate-select');
                }
            }
        });
    },
    update: function() {
        if (this.regions[this.countryEl.value]) {
            var i, option, region, def;
            def = this.regionSelectEl.getAttribute('defaultValue');
            if (this.regionTextEl) {
                if (!def) {
                    def = this.regionTextEl.value.toLowerCase();
                }
                this.regionTextEl.value = '';
            }
            if (this.regionSelectEl && this.regionSelectEl.value && !def) {
                def = this.regionSelectEl.value;
            }
            this.regionSelectEl.options.length = 1;
            for (regionId in this.regions[this.countryEl.value]) {
                region = this.regions[this.countryEl.value][regionId];
                option = document.createElement('OPTION');
                option.value = regionId;
                option.text = region.name.stripTags();
                option.title = region.name;
                if (this.regionSelectEl.options.add) {
                    this.regionSelectEl.options.add(option);
                } else {
                    this.regionSelectEl.appendChild(option);
                }
                if (regionId == def || (region.name && region.name.toLowerCase() == def) || (region.name && region.code.toLowerCase() == def)) {
                    this.regionSelectEl.value = regionId;
                }
            }
            if (this.disableAction == 'hide') {
                if (this.regionTextEl) {
                    this.regionTextEl.style.display = 'none';
                }
                this.regionSelectEl.style.display = '';
            } else if (this.disableAction == 'disable') {
                if (this.regionTextEl) {
                    this.regionTextEl.disabled = true;
                }
                this.regionSelectEl.disabled = false;
            }
            this.setMarkDisplay(this.regionSelectEl, true);
        } else {
            if (this.disableAction == 'hide') {
                if (this.regionTextEl) {
                    this.regionTextEl.style.display = '';
                }
                this.regionSelectEl.style.display = 'none';
                Validation.reset(this.regionSelectEl);
            } else if (this.disableAction == 'disable') {
                if (this.regionTextEl) {
                    this.regionTextEl.disabled = false;
                }
                this.regionSelectEl.disabled = true;
            } else if (this.disableAction == 'nullify') {
                this.regionSelectEl.options.length = 1;
                this.regionSelectEl.value = '';
                this.regionSelectEl.selectedIndex = 0;
                this.lastCountryId = '';
            }
            this.setMarkDisplay(this.regionSelectEl, false);
        }
        this._checkRegionRequired();
        var zipUpdater = new ZipUpdater(this.countryEl.value, this.zipEl);
        zipUpdater.update();
    },
    setMarkDisplay: function(elem, display) {
        elem = $(elem);
        var labelElement = elem.up(0).down('label > span.required') || elem.up(1).down('label > span.required') || elem.up(0).down('label.required > em') || elem.up(1).down('label.required > em');
        if (labelElement) {
            inputElement = labelElement.up().next('input');
            if (display) {
                labelElement.show();
                if (inputElement) {
                    inputElement.addClassName('required-entry');
                }
            } else {
                labelElement.hide();
                if (inputElement) {
                    inputElement.removeClassName('required-entry');
                }
            }
        }
    }
}
ZipUpdater = Class.create();
ZipUpdater.prototype = {
    initialize: function(country, zipElement) {
        this.country = country;
        this.zipElement = $(zipElement);
    },
    update: function() {
        if (typeof optionalZipCountries == 'undefined') {
            return false;
        }
        if (this.zipElement != undefined) {
            this._setPostcodeOptional();
        } else {
            Event.observe(window, "load", this._setPostcodeOptional.bind(this));
        }
    },
    _setPostcodeOptional: function() {
        this.zipElement = $(this.zipElement);
        if (this.zipElement == undefined) {
            return false;
        }
        var label = $$('label[for="' + this.zipElement.id + '"]')[0];
        if (label != undefined) {
            var wildCard = label.down('em') || label.down('span.required');
        }
        if (optionalZipCountries.indexOf(this.country) != -1) {
            while (this.zipElement.hasClassName('required-entry')) {
                this.zipElement.removeClassName('required-entry');
            }
            if (wildCard != undefined) {
                wildCard.hide();
            }
        } else {
            this.zipElement.addClassName('required-entry');
            if (wildCard != undefined) {
                wildCard.show();
            }
        }
    }
}
var mainNav = function() {
    var main = {
        obj_nav: $(arguments[0]) || $("nav"),
        settings: {
            show_delay: 0,
            hide_delay: 0,
            _ie6: /MSIE 6.+Win/.test(navigator.userAgent),
            _ie7: /MSIE 7.+Win/.test(navigator.userAgent)
        },
        init: function(obj, level) {
            obj.lists = obj.childElements();
            obj.lists.each(function(el, ind) {
                main.handlNavElement(el);
                if ((main.settings._ie6 || main.settings._ie7) && level) {
                    main.ieFixZIndex(el, ind, obj.lists.size());
                }
            });
            if (main.settings._ie6 && !level) {
                document.execCommand("BackgroundImageCache", false, true);
            }
        },
        handlNavElement: function(list) {
            if (list !== undefined) {
                list.onmouseover = function() {
                    main.fireNavEvent(this, true);
                };
                list.onmouseout = function() {
                    main.fireNavEvent(this, false);
                };
                if (list.down("ul")) {
                    main.init(list.down("ul"), true);
                }
            }
        },
        ieFixZIndex: function(el, i, l) {
            if (el.tagName.toString().toLowerCase().indexOf("iframe") == -1) {
                el.style.zIndex = l - i;
            } else {
                el.onmouseover = "null";
                el.onmouseout = "null";
            }
        },
        fireNavEvent: function(elm, ev) {
            if (ev) {
                elm.addClassName("over");
                elm.down("a").addClassName("over");
                if (elm.childElements()[1]) {
                    main.show(elm.childElements()[1]);
                }
            } else {
                elm.removeClassName("over");
                elm.down("a").removeClassName("over");
                if (elm.childElements()[1]) {
                    main.hide(elm.childElements()[1]);
                }
            }
        },
        show: function(sub_elm) {
            if (sub_elm.hide_time_id) {
                clearTimeout(sub_elm.hide_time_id);
            }
            sub_elm.show_time_id = setTimeout(function() {
                if (!sub_elm.hasClassName("shown-sub")) {
                    sub_elm.addClassName("shown-sub");
                }
            }, main.settings.show_delay);
        },
        hide: function(sub_elm) {
            if (sub_elm.show_time_id) {
                clearTimeout(sub_elm.show_time_id);
            }
            sub_elm.hide_time_id = setTimeout(function() {
                if (sub_elm.hasClassName("shown-sub")) {
                    sub_elm.removeClassName("shown-sub");
                }
            }, main.settings.hide_delay);
        }
    };
    if (arguments[1]) {
        main.settings = Object.extend(main.settings, arguments[1]);
    }
    if (main.obj_nav) {
        main.init(main.obj_nav, false);
    }
};
document.observe("dom:loaded", function() {
    mainNav("nav", {
        "show_delay": "100",
        "hide_delay": "100"
    });
});
var Translate = Class.create();
Translate.prototype = {
    initialize: function(data) {
        this.data = $H(data);
    },
    translate: function() {
        var args = arguments;
        var text = arguments[0];
        if (this.data.get(text)) {
            return this.data.get(text);
        }
        return text;
    },
    add: function() {
        if (arguments.length > 1) {
            this.data.set(arguments[0], arguments[1]);
        } else if (typeof arguments[0] == 'object') {
            $H(arguments[0]).each(function(pair) {
                this.data.set(pair.key, pair.value);
            }.bind(this));
        }
    }
}
if (!window.Mage) var Mage = {};
Mage.Cookies = {};
Mage.Cookies.expires = null;
Mage.Cookies.path = '/';
Mage.Cookies.domain = null;
Mage.Cookies.secure = false;
Mage.Cookies.set = function(name, value) {
    var argv = arguments;
    var argc = arguments.length;
    var expires = (argc > 2) ? argv[2] : Mage.Cookies.expires;
    var path = (argc > 3) ? argv[3] : Mage.Cookies.path;
    var domain = (argc > 4) ? argv[4] : Mage.Cookies.domain;
    var secure = (argc > 5) ? argv[5] : Mage.Cookies.secure;
    document.cookie = name + "=" + escape(value) +
        ((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
        ((path == null) ? "" : ("; path=" + path)) +
        ((domain == null) ? "" : ("; domain=" + domain)) +
        ((secure == true) ? "; secure" : "");
};
Mage.Cookies.get = function(name) {
    var arg = name + "=";
    var alen = arg.length;
    var clen = document.cookie.length;
    var i = 0;
    var j = 0;
    while (i < clen) {
        j = i + alen;
        if (document.cookie.substring(i, j) == arg)
            return Mage.Cookies.getCookieVal(j);
        i = document.cookie.indexOf(" ", i) + 1;
        if (i == 0)
            break;
    }
    return null;
};
Mage.Cookies.clear = function(name) {
    if (Mage.Cookies.get(name)) {
        document.cookie = name + "=" +
            "; expires=Thu, 01-Jan-70 00:00:01 GMT";
    }
};
Mage.Cookies.getCookieVal = function(offset) {
    var endstr = document.cookie.indexOf(";", offset);
    if (endstr == -1) {
        endstr = document.cookie.length;
    }
    return unescape(document.cookie.substring(offset, endstr));
};
/*! jQuery v1.9.1 | (c) 2005, 2012 jQuery Foundation, Inc. | jquery.org/license
//@ sourceMappingURL=jquery.min.map
*/
(function(e, t) {
    var n, r, i = typeof t,
        o = e.document,
        a = e.location,
        s = e.jQuery,
        u = e.$,
        l = {},
        c = [],
        p = "1.9.1",
        f = c.concat,
        d = c.push,
        h = c.slice,
        g = c.indexOf,
        m = l.toString,
        y = l.hasOwnProperty,
        v = p.trim,
        b = function(e, t) {
            return new b.fn.init(e, t, r)
        },
        x = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        w = /\S+/g,
        T = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        N = /^(?:(<[\w\W]+>)[^>]*|#([\w-]*))$/,
        C = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
        k = /^[\],:{}\s]*$/,
        E = /(?:^|:|,)(?:\s*\[)+/g,
        S = /\\(?:["\\\/bfnrt]|u[\da-fA-F]{4})/g,
        A = /"[^"\\\r\n]*"|true|false|null|-?(?:\d+\.|)\d+(?:[eE][+-]?\d+|)/g,
        j = /^-ms-/,
        D = /-([\da-z])/gi,
        L = function(e, t) {
            return t.toUpperCase()
        },
        H = function(e) {
            (o.addEventListener || "load" === e.type || "complete" === o.readyState) && (q(), b.ready())
        },
        q = function() {
            o.addEventListener ? (o.removeEventListener("DOMContentLoaded", H, !1), e.removeEventListener("load", H, !1)) : (o.detachEvent("onreadystatechange", H), e.detachEvent("onload", H))
        };
    b.fn = b.prototype = {
        jquery: p,
        constructor: b,
        init: function(e, n, r) {
            var i, a;
            if (!e) return this;
            if ("string" == typeof e) {
                if (i = "<" === e.charAt(0) && ">" === e.charAt(e.length - 1) && e.length >= 3 ? [null, e, null] : N.exec(e), !i || !i[1] && n) return !n || n.jquery ? (n || r).find(e) : this.constructor(n).find(e);
                if (i[1]) {
                    if (n = n instanceof b ? n[0] : n, b.merge(this, b.parseHTML(i[1], n && n.nodeType ? n.ownerDocument || n : o, !0)), C.test(i[1]) && b.isPlainObject(n))
                        for (i in n) b.isFunction(this[i]) ? this[i](n[i]) : this.attr(i, n[i]);
                    return this
                }
                if (a = o.getElementById(i[2]), a && a.parentNode) {
                    if (a.id !== i[2]) return r.find(e);
                    this.length = 1, this[0] = a
                }
                return this.context = o, this.selector = e, this
            }
            return e.nodeType ? (this.context = this[0] = e, this.length = 1, this) : b.isFunction(e) ? r.ready(e) : (e.selector !== t && (this.selector = e.selector, this.context = e.context), b.makeArray(e, this))
        },
        selector: "",
        length: 0,
        size: function() {
            return this.length
        },
        toArray: function() {
            return h.call(this)
        },
        get: function(e) {
            return null == e ? this.toArray() : 0 > e ? this[this.length + e] : this[e]
        },
        pushStack: function(e) {
            var t = b.merge(this.constructor(), e);
            return t.prevObject = this, t.context = this.context, t
        },
        each: function(e, t) {
            return b.each(this, e, t)
        },
        ready: function(e) {
            return b.ready.promise().done(e), this
        },
        slice: function() {
            return this.pushStack(h.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(e) {
            var t = this.length,
                n = +e + (0 > e ? t : 0);
            return this.pushStack(n >= 0 && t > n ? [this[n]] : [])
        },
        map: function(e) {
            return this.pushStack(b.map(this, function(t, n) {
                return e.call(t, n, t)
            }))
        },
        end: function() {
            return this.prevObject || this.constructor(null)
        },
        push: d,
        sort: [].sort,
        splice: [].splice
    }, b.fn.init.prototype = b.fn, b.extend = b.fn.extend = function() {
        var e, n, r, i, o, a, s = arguments[0] || {},
            u = 1,
            l = arguments.length,
            c = !1;
        for ("boolean" == typeof s && (c = s, s = arguments[1] || {}, u = 2), "object" == typeof s || b.isFunction(s) || (s = {}), l === u && (s = this, --u); l > u; u++)
            if (null != (o = arguments[u]))
                for (i in o) e = s[i], r = o[i], s !== r && (c && r && (b.isPlainObject(r) || (n = b.isArray(r))) ? (n ? (n = !1, a = e && b.isArray(e) ? e : []) : a = e && b.isPlainObject(e) ? e : {}, s[i] = b.extend(c, a, r)) : r !== t && (s[i] = r));
        return s
    }, b.extend({
        noConflict: function(t) {
            return e.$ === b && (e.$ = u), t && e.jQuery === b && (e.jQuery = s), b
        },
        isReady: !1,
        readyWait: 1,
        holdReady: function(e) {
            e ? b.readyWait++ : b.ready(!0)
        },
        ready: function(e) {
            if (e === !0 ? !--b.readyWait : !b.isReady) {
                if (!o.body) return setTimeout(b.ready);
                b.isReady = !0, e !== !0 && --b.readyWait > 0 || (n.resolveWith(o, [b]), b.fn.trigger && b(o).trigger("ready").off("ready"))
            }
        },
        isFunction: function(e) {
            return "function" === b.type(e)
        },
        isArray: Array.isArray || function(e) {
            return "array" === b.type(e)
        },
        isWindow: function(e) {
            return null != e && e == e.window
        },
        isNumeric: function(e) {
            return !isNaN(parseFloat(e)) && isFinite(e)
        },
        type: function(e) {
            return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? l[m.call(e)] || "object" : typeof e
        },
        isPlainObject: function(e) {
            if (!e || "object" !== b.type(e) || e.nodeType || b.isWindow(e)) return !1;
            try {
                if (e.constructor && !y.call(e, "constructor") && !y.call(e.constructor.prototype, "isPrototypeOf")) return !1
            } catch (n) {
                return !1
            }
            var r;
            for (r in e);
            return r === t || y.call(e, r)
        },
        isEmptyObject: function(e) {
            var t;
            for (t in e) return !1;
            return !0
        },
        error: function(e) {
            throw Error(e)
        },
        parseHTML: function(e, t, n) {
            if (!e || "string" != typeof e) return null;
            "boolean" == typeof t && (n = t, t = !1), t = t || o;
            var r = C.exec(e),
                i = !n && [];
            return r ? [t.createElement(r[1])] : (r = b.buildFragment([e], t, i), i && b(i).remove(), b.merge([], r.childNodes))
        },
        parseJSON: function(n) {
            return e.JSON && e.JSON.parse ? e.JSON.parse(n) : null === n ? n : "string" == typeof n && (n = b.trim(n), n && k.test(n.replace(S, "@").replace(A, "]").replace(E, ""))) ? Function("return " + n)() : (b.error("Invalid JSON: " + n), t)
        },
        parseXML: function(n) {
            var r, i;
            if (!n || "string" != typeof n) return null;
            try {
                e.DOMParser ? (i = new DOMParser, r = i.parseFromString(n, "text/xml")) : (r = new ActiveXObject("Microsoft.XMLDOM"), r.async = "false", r.loadXML(n))
            } catch (o) {
                r = t
            }
            return r && r.documentElement && !r.getElementsByTagName("parsererror").length || b.error("Invalid XML: " + n), r
        },
        noop: function() {},
        globalEval: function(t) {
            t && b.trim(t) && (e.execScript || function(t) {
                e.eval.call(e, t)
            })(t)
        },
        camelCase: function(e) {
            return e.replace(j, "ms-").replace(D, L)
        },
        nodeName: function(e, t) {
            return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
        },
        each: function(e, t, n) {
            var r, i = 0,
                o = e.length,
                a = M(e);
            if (n) {
                if (a) {
                    for (; o > i; i++)
                        if (r = t.apply(e[i], n), r === !1) break
                } else
                    for (i in e)
                        if (r = t.apply(e[i], n), r === !1) break
            } else if (a) {
                for (; o > i; i++)
                    if (r = t.call(e[i], i, e[i]), r === !1) break
            } else
                for (i in e)
                    if (r = t.call(e[i], i, e[i]), r === !1) break; return e
        },
        trim: v && !v.call("\ufeff\u00a0") ? function(e) {
            return null == e ? "" : v.call(e)
        } : function(e) {
            return null == e ? "" : (e + "").replace(T, "")
        },
        makeArray: function(e, t) {
            var n = t || [];
            return null != e && (M(Object(e)) ? b.merge(n, "string" == typeof e ? [e] : e) : d.call(n, e)), n
        },
        inArray: function(e, t, n) {
            var r;
            if (t) {
                if (g) return g.call(t, e, n);
                for (r = t.length, n = n ? 0 > n ? Math.max(0, r + n) : n : 0; r > n; n++)
                    if (n in t && t[n] === e) return n
            }
            return -1
        },
        merge: function(e, n) {
            var r = n.length,
                i = e.length,
                o = 0;
            if ("number" == typeof r)
                for (; r > o; o++) e[i++] = n[o];
            else
                while (n[o] !== t) e[i++] = n[o++];
            return e.length = i, e
        },
        grep: function(e, t, n) {
            var r, i = [],
                o = 0,
                a = e.length;
            for (n = !!n; a > o; o++) r = !!t(e[o], o), n !== r && i.push(e[o]);
            return i
        },
        map: function(e, t, n) {
            var r, i = 0,
                o = e.length,
                a = M(e),
                s = [];
            if (a)
                for (; o > i; i++) r = t(e[i], i, n), null != r && (s[s.length] = r);
            else
                for (i in e) r = t(e[i], i, n), null != r && (s[s.length] = r);
            return f.apply([], s)
        },
        guid: 1,
        proxy: function(e, n) {
            var r, i, o;
            return "string" == typeof n && (o = e[n], n = e, e = o), b.isFunction(e) ? (r = h.call(arguments, 2), i = function() {
                return e.apply(n || this, r.concat(h.call(arguments)))
            }, i.guid = e.guid = e.guid || b.guid++, i) : t
        },
        access: function(e, n, r, i, o, a, s) {
            var u = 0,
                l = e.length,
                c = null == r;
            if ("object" === b.type(r)) {
                o = !0;
                for (u in r) b.access(e, n, u, r[u], !0, a, s)
            } else if (i !== t && (o = !0, b.isFunction(i) || (s = !0), c && (s ? (n.call(e, i), n = null) : (c = n, n = function(e, t, n) {
                    return c.call(b(e), n)
                })), n))
                for (; l > u; u++) n(e[u], r, s ? i : i.call(e[u], u, n(e[u], r)));
            return o ? e : c ? n.call(e) : l ? n(e[0], r) : a
        },
        now: function() {
            return (new Date).getTime()
        }
    }), b.ready.promise = function(t) {
        if (!n)
            if (n = b.Deferred(), "complete" === o.readyState) setTimeout(b.ready);
            else if (o.addEventListener) o.addEventListener("DOMContentLoaded", H, !1), e.addEventListener("load", H, !1);
        else {
            o.attachEvent("onreadystatechange", H), e.attachEvent("onload", H);
            var r = !1;
            try {
                r = null == e.frameElement && o.documentElement
            } catch (i) {}
            r && r.doScroll && function a() {
                if (!b.isReady) {
                    try {
                        r.doScroll("left")
                    } catch (e) {
                        return setTimeout(a, 50)
                    }
                    q(), b.ready()
                }
            }()
        }
        return n.promise(t)
    }, b.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(e, t) {
        l["[object " + t + "]"] = t.toLowerCase()
    });

    function M(e) {
        var t = e.length,
            n = b.type(e);
        return b.isWindow(e) ? !1 : 1 === e.nodeType && t ? !0 : "array" === n || "function" !== n && (0 === t || "number" == typeof t && t > 0 && t - 1 in e)
    }
    r = b(o);
    var _ = {};

    function F(e) {
        var t = _[e] = {};
        return b.each(e.match(w) || [], function(e, n) {
            t[n] = !0
        }), t
    }
    b.Callbacks = function(e) {
        e = "string" == typeof e ? _[e] || F(e) : b.extend({}, e);
        var n, r, i, o, a, s, u = [],
            l = !e.once && [],
            c = function(t) {
                for (r = e.memory && t, i = !0, a = s || 0, s = 0, o = u.length, n = !0; u && o > a; a++)
                    if (u[a].apply(t[0], t[1]) === !1 && e.stopOnFalse) {
                        r = !1;
                        break
                    }
                n = !1, u && (l ? l.length && c(l.shift()) : r ? u = [] : p.disable())
            },
            p = {
                add: function() {
                    if (u) {
                        var t = u.length;
                        (function i(t) {
                            b.each(t, function(t, n) {
                                var r = b.type(n);
                                "function" === r ? e.unique && p.has(n) || u.push(n) : n && n.length && "string" !== r && i(n)
                            })
                        })(arguments), n ? o = u.length : r && (s = t, c(r))
                    }
                    return this
                },
                remove: function() {
                    return u && b.each(arguments, function(e, t) {
                        var r;
                        while ((r = b.inArray(t, u, r)) > -1) u.splice(r, 1), n && (o >= r && o--, a >= r && a--)
                    }), this
                },
                has: function(e) {
                    return e ? b.inArray(e, u) > -1 : !(!u || !u.length)
                },
                empty: function() {
                    return u = [], this
                },
                disable: function() {
                    return u = l = r = t, this
                },
                disabled: function() {
                    return !u
                },
                lock: function() {
                    return l = t, r || p.disable(), this
                },
                locked: function() {
                    return !l
                },
                fireWith: function(e, t) {
                    return t = t || [], t = [e, t.slice ? t.slice() : t], !u || i && !l || (n ? l.push(t) : c(t)), this
                },
                fire: function() {
                    return p.fireWith(this, arguments), this
                },
                fired: function() {
                    return !!i
                }
            };
        return p
    }, b.extend({
        Deferred: function(e) {
            var t = [
                    ["resolve", "done", b.Callbacks("once memory"), "resolved"],
                    ["reject", "fail", b.Callbacks("once memory"), "rejected"],
                    ["notify", "progress", b.Callbacks("memory")]
                ],
                n = "pending",
                r = {
                    state: function() {
                        return n
                    },
                    always: function() {
                        return i.done(arguments).fail(arguments), this
                    },
                    then: function() {
                        var e = arguments;
                        return b.Deferred(function(n) {
                            b.each(t, function(t, o) {
                                var a = o[0],
                                    s = b.isFunction(e[t]) && e[t];
                                i[o[1]](function() {
                                    var e = s && s.apply(this, arguments);
                                    e && b.isFunction(e.promise) ? e.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[a + "With"](this === r ? n.promise() : this, s ? [e] : arguments)
                                })
                            }), e = null
                        }).promise()
                    },
                    promise: function(e) {
                        return null != e ? b.extend(e, r) : r
                    }
                },
                i = {};
            return r.pipe = r.then, b.each(t, function(e, o) {
                var a = o[2],
                    s = o[3];
                r[o[1]] = a.add, s && a.add(function() {
                    n = s
                }, t[1 ^ e][2].disable, t[2][2].lock), i[o[0]] = function() {
                    return i[o[0] + "With"](this === i ? r : this, arguments), this
                }, i[o[0] + "With"] = a.fireWith
            }), r.promise(i), e && e.call(i, i), i
        },
        when: function(e) {
            var t = 0,
                n = h.call(arguments),
                r = n.length,
                i = 1 !== r || e && b.isFunction(e.promise) ? r : 0,
                o = 1 === i ? e : b.Deferred(),
                a = function(e, t, n) {
                    return function(r) {
                        t[e] = this, n[e] = arguments.length > 1 ? h.call(arguments) : r, n === s ? o.notifyWith(t, n) : --i || o.resolveWith(t, n)
                    }
                },
                s, u, l;
            if (r > 1)
                for (s = Array(r), u = Array(r), l = Array(r); r > t; t++) n[t] && b.isFunction(n[t].promise) ? n[t].promise().done(a(t, l, n)).fail(o.reject).progress(a(t, u, s)) : --i;
            return i || o.resolveWith(l, n), o.promise()
        }
    }), b.support = function() {
        var t, n, r, a, s, u, l, c, p, f, d = o.createElement("div");
        if (d.setAttribute("className", "t"), d.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", n = d.getElementsByTagName("*"), r = d.getElementsByTagName("a")[0], !n || !r || !n.length) return {};
        s = o.createElement("select"), l = s.appendChild(o.createElement("option")), a = d.getElementsByTagName("input")[0], r.style.cssText = "top:1px;float:left;opacity:.5", t = {
            getSetAttribute: "t" !== d.className,
            leadingWhitespace: 3 === d.firstChild.nodeType,
            tbody: !d.getElementsByTagName("tbody").length,
            htmlSerialize: !!d.getElementsByTagName("link").length,
            style: /top/.test(r.getAttribute("style")),
            hrefNormalized: "/a" === r.getAttribute("href"),
            opacity: /^0.5/.test(r.style.opacity),
            cssFloat: !!r.style.cssFloat,
            checkOn: !!a.value,
            optSelected: l.selected,
            enctype: !!o.createElement("form").enctype,
            html5Clone: "<:nav></:nav>" !== o.createElement("nav").cloneNode(!0).outerHTML,
            boxModel: "CSS1Compat" === o.compatMode,
            deleteExpando: !0,
            noCloneEvent: !0,
            inlineBlockNeedsLayout: !1,
            shrinkWrapBlocks: !1,
            reliableMarginRight: !0,
            boxSizingReliable: !0,
            pixelPosition: !1
        }, a.checked = !0, t.noCloneChecked = a.cloneNode(!0).checked, s.disabled = !0, t.optDisabled = !l.disabled;
        try {
            delete d.test
        } catch (h) {
            t.deleteExpando = !1
        }
        a = o.createElement("input"), a.setAttribute("value", ""), t.input = "" === a.getAttribute("value"), a.value = "t", a.setAttribute("type", "radio"), t.radioValue = "t" === a.value, a.setAttribute("checked", "t"), a.setAttribute("name", "t"), u = o.createDocumentFragment(), u.appendChild(a), t.appendChecked = a.checked, t.checkClone = u.cloneNode(!0).cloneNode(!0).lastChild.checked, d.attachEvent && (d.attachEvent("onclick", function() {
            t.noCloneEvent = !1
        }), d.cloneNode(!0).click());
        for (f in {
                submit: !0,
                change: !0,
                focusin: !0
            }) d.setAttribute(c = "on" + f, "t"), t[f + "Bubbles"] = c in e || d.attributes[c].expando === !1;
        return d.style.backgroundClip = "content-box", d.cloneNode(!0).style.backgroundClip = "", t.clearCloneStyle = "content-box" === d.style.backgroundClip, b(function() {
            var n, r, a, s = "padding:0;margin:0;border:0;display:block;box-sizing:content-box;-moz-box-sizing:content-box;-webkit-box-sizing:content-box;",
                u = o.getElementsByTagName("body")[0];
            u && (n = o.createElement("div"), n.style.cssText = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px", u.appendChild(n).appendChild(d), d.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", a = d.getElementsByTagName("td"), a[0].style.cssText = "padding:0;margin:0;border:0;display:none", p = 0 === a[0].offsetHeight, a[0].style.display = "", a[1].style.display = "none", t.reliableHiddenOffsets = p && 0 === a[0].offsetHeight, d.innerHTML = "", d.style.cssText = "box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding:1px;border:1px;display:block;width:4px;margin-top:1%;position:absolute;top:1%;", t.boxSizing = 4 === d.offsetWidth, t.doesNotIncludeMarginInBodyOffset = 1 !== u.offsetTop, e.getComputedStyle && (t.pixelPosition = "1%" !== (e.getComputedStyle(d, null) || {}).top, t.boxSizingReliable = "4px" === (e.getComputedStyle(d, null) || {
                width: "4px"
            }).width, r = d.appendChild(o.createElement("div")), r.style.cssText = d.style.cssText = s, r.style.marginRight = r.style.width = "0", d.style.width = "1px", t.reliableMarginRight = !parseFloat((e.getComputedStyle(r, null) || {}).marginRight)), typeof d.style.zoom !== i && (d.innerHTML = "", d.style.cssText = s + "width:1px;padding:1px;display:inline;zoom:1", t.inlineBlockNeedsLayout = 3 === d.offsetWidth, d.style.display = "block", d.innerHTML = "<div></div>", d.firstChild.style.width = "5px", t.shrinkWrapBlocks = 3 !== d.offsetWidth, t.inlineBlockNeedsLayout && (u.style.zoom = 1)), u.removeChild(n), n = d = a = r = null)
        }), n = s = u = l = r = a = null, t
    }();
    var O = /(?:\{[\s\S]*\}|\[[\s\S]*\])$/,
        B = /([A-Z])/g;

    function P(e, n, r, i) {
        if (b.acceptData(e)) {
            var o, a, s = b.expando,
                u = "string" == typeof n,
                l = e.nodeType,
                p = l ? b.cache : e,
                f = l ? e[s] : e[s] && s;
            if (f && p[f] && (i || p[f].data) || !u || r !== t) return f || (l ? e[s] = f = c.pop() || b.guid++ : f = s), p[f] || (p[f] = {}, l || (p[f].toJSON = b.noop)), ("object" == typeof n || "function" == typeof n) && (i ? p[f] = b.extend(p[f], n) : p[f].data = b.extend(p[f].data, n)), o = p[f], i || (o.data || (o.data = {}), o = o.data), r !== t && (o[b.camelCase(n)] = r), u ? (a = o[n], null == a && (a = o[b.camelCase(n)])) : a = o, a
        }
    }

    function R(e, t, n) {
        if (b.acceptData(e)) {
            var r, i, o, a = e.nodeType,
                s = a ? b.cache : e,
                u = a ? e[b.expando] : b.expando;
            if (s[u]) {
                if (t && (o = n ? s[u] : s[u].data)) {
                    b.isArray(t) ? t = t.concat(b.map(t, b.camelCase)) : t in o ? t = [t] : (t = b.camelCase(t), t = t in o ? [t] : t.split(" "));
                    for (r = 0, i = t.length; i > r; r++) delete o[t[r]];
                    if (!(n ? $ : b.isEmptyObject)(o)) return
                }(n || (delete s[u].data, $(s[u]))) && (a ? b.cleanData([e], !0) : b.support.deleteExpando || s != s.window ? delete s[u] : s[u] = null)
            }
        }
    }
    b.extend({
        cache: {},
        expando: "jQuery" + (p + Math.random()).replace(/\D/g, ""),
        noData: {
            embed: !0,
            object: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
            applet: !0
        },
        hasData: function(e) {
            return e = e.nodeType ? b.cache[e[b.expando]] : e[b.expando], !!e && !$(e)
        },
        data: function(e, t, n) {
            return P(e, t, n)
        },
        removeData: function(e, t) {
            return R(e, t)
        },
        _data: function(e, t, n) {
            return P(e, t, n, !0)
        },
        _removeData: function(e, t) {
            return R(e, t, !0)
        },
        acceptData: function(e) {
            if (e.nodeType && 1 !== e.nodeType && 9 !== e.nodeType) return !1;
            var t = e.nodeName && b.noData[e.nodeName.toLowerCase()];
            return !t || t !== !0 && e.getAttribute("classid") === t
        }
    }), b.fn.extend({
        data: function(e, n) {
            var r, i, o = this[0],
                a = 0,
                s = null;
            if (e === t) {
                if (this.length && (s = b.data(o), 1 === o.nodeType && !b._data(o, "parsedAttrs"))) {
                    for (r = o.attributes; r.length > a; a++) i = r[a].name, i.indexOf("data-") || (i = b.camelCase(i.slice(5)), W(o, i, s[i]));
                    b._data(o, "parsedAttrs", !0)
                }
                return s
            }
            return "object" == typeof e ? this.each(function() {
                b.data(this, e)
            }) : b.access(this, function(n) {
                return n === t ? o ? W(o, e, b.data(o, e)) : null : (this.each(function() {
                    b.data(this, e, n)
                }), t)
            }, null, n, arguments.length > 1, null, !0)
        },
        removeData: function(e) {
            return this.each(function() {
                b.removeData(this, e)
            })
        }
    });

    function W(e, n, r) {
        if (r === t && 1 === e.nodeType) {
            var i = "data-" + n.replace(B, "-$1").toLowerCase();
            if (r = e.getAttribute(i), "string" == typeof r) {
                try {
                    r = "true" === r ? !0 : "false" === r ? !1 : "null" === r ? null : +r + "" === r ? +r : O.test(r) ? b.parseJSON(r) : r
                } catch (o) {}
                b.data(e, n, r)
            } else r = t
        }
        return r
    }

    function $(e) {
        var t;
        for (t in e)
            if (("data" !== t || !b.isEmptyObject(e[t])) && "toJSON" !== t) return !1;
        return !0
    }
    b.extend({
        queue: function(e, n, r) {
            var i;
            return e ? (n = (n || "fx") + "queue", i = b._data(e, n), r && (!i || b.isArray(r) ? i = b._data(e, n, b.makeArray(r)) : i.push(r)), i || []) : t
        },
        dequeue: function(e, t) {
            t = t || "fx";
            var n = b.queue(e, t),
                r = n.length,
                i = n.shift(),
                o = b._queueHooks(e, t),
                a = function() {
                    b.dequeue(e, t)
                };
            "inprogress" === i && (i = n.shift(), r--), o.cur = i, i && ("fx" === t && n.unshift("inprogress"), delete o.stop, i.call(e, a, o)), !r && o && o.empty.fire()
        },
        _queueHooks: function(e, t) {
            var n = t + "queueHooks";
            return b._data(e, n) || b._data(e, n, {
                empty: b.Callbacks("once memory").add(function() {
                    b._removeData(e, t + "queue"), b._removeData(e, n)
                })
            })
        }
    }), b.fn.extend({
        queue: function(e, n) {
            var r = 2;
            return "string" != typeof e && (n = e, e = "fx", r--), r > arguments.length ? b.queue(this[0], e) : n === t ? this : this.each(function() {
                var t = b.queue(this, e, n);
                b._queueHooks(this, e), "fx" === e && "inprogress" !== t[0] && b.dequeue(this, e)
            })
        },
        dequeue: function(e) {
            return this.each(function() {
                b.dequeue(this, e)
            })
        },
        delay: function(e, t) {
            return e = b.fx ? b.fx.speeds[e] || e : e, t = t || "fx", this.queue(t, function(t, n) {
                var r = setTimeout(t, e);
                n.stop = function() {
                    clearTimeout(r)
                }
            })
        },
        clearQueue: function(e) {
            return this.queue(e || "fx", [])
        },
        promise: function(e, n) {
            var r, i = 1,
                o = b.Deferred(),
                a = this,
                s = this.length,
                u = function() {
                    --i || o.resolveWith(a, [a])
                };
            "string" != typeof e && (n = e, e = t), e = e || "fx";
            while (s--) r = b._data(a[s], e + "queueHooks"), r && r.empty && (i++, r.empty.add(u));
            return u(), o.promise(n)
        }
    });
    var I, z, X = /[\t\r\n]/g,
        U = /\r/g,
        V = /^(?:input|select|textarea|button|object)$/i,
        Y = /^(?:a|area)$/i,
        J = /^(?:checked|selected|autofocus|autoplay|async|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped)$/i,
        G = /^(?:checked|selected)$/i,
        Q = b.support.getSetAttribute,
        K = b.support.input;
    b.fn.extend({
        attr: function(e, t) {
            return b.access(this, b.attr, e, t, arguments.length > 1)
        },
        removeAttr: function(e) {
            return this.each(function() {
                b.removeAttr(this, e)
            })
        },
        prop: function(e, t) {
            return b.access(this, b.prop, e, t, arguments.length > 1)
        },
        removeProp: function(e) {
            return e = b.propFix[e] || e, this.each(function() {
                try {
                    this[e] = t, delete this[e]
                } catch (n) {}
            })
        },
        addClass: function(e) {
            var t, n, r, i, o, a = 0,
                s = this.length,
                u = "string" == typeof e && e;
            if (b.isFunction(e)) return this.each(function(t) {
                b(this).addClass(e.call(this, t, this.className))
            });
            if (u)
                for (t = (e || "").match(w) || []; s > a; a++)
                    if (n = this[a], r = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(X, " ") : " ")) {
                        o = 0;
                        while (i = t[o++]) 0 > r.indexOf(" " + i + " ") && (r += i + " ");
                        n.className = b.trim(r)
                    }
            return this
        },
        removeClass: function(e) {
            var t, n, r, i, o, a = 0,
                s = this.length,
                u = 0 === arguments.length || "string" == typeof e && e;
            if (b.isFunction(e)) return this.each(function(t) {
                b(this).removeClass(e.call(this, t, this.className))
            });
            if (u)
                for (t = (e || "").match(w) || []; s > a; a++)
                    if (n = this[a], r = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(X, " ") : "")) {
                        o = 0;
                        while (i = t[o++])
                            while (r.indexOf(" " + i + " ") >= 0) r = r.replace(" " + i + " ", " ");
                        n.className = e ? b.trim(r) : ""
                    }
            return this
        },
        toggleClass: function(e, t) {
            var n = typeof e,
                r = "boolean" == typeof t;
            return b.isFunction(e) ? this.each(function(n) {
                b(this).toggleClass(e.call(this, n, this.className, t), t)
            }) : this.each(function() {
                if ("string" === n) {
                    var o, a = 0,
                        s = b(this),
                        u = t,
                        l = e.match(w) || [];
                    while (o = l[a++]) u = r ? u : !s.hasClass(o), s[u ? "addClass" : "removeClass"](o)
                } else(n === i || "boolean" === n) && (this.className && b._data(this, "__className__", this.className), this.className = this.className || e === !1 ? "" : b._data(this, "__className__") || "")
            })
        },
        hasClass: function(e) {
            var t = " " + e + " ",
                n = 0,
                r = this.length;
            for (; r > n; n++)
                if (1 === this[n].nodeType && (" " + this[n].className + " ").replace(X, " ").indexOf(t) >= 0) return !0;
            return !1
        },
        val: function(e) {
            var n, r, i, o = this[0]; {
                if (arguments.length) return i = b.isFunction(e), this.each(function(n) {
                    var o, a = b(this);
                    1 === this.nodeType && (o = i ? e.call(this, n, a.val()) : e, null == o ? o = "" : "number" == typeof o ? o += "" : b.isArray(o) && (o = b.map(o, function(e) {
                        return null == e ? "" : e + ""
                    })), r = b.valHooks[this.type] || b.valHooks[this.nodeName.toLowerCase()], r && "set" in r && r.set(this, o, "value") !== t || (this.value = o))
                });
                if (o) return r = b.valHooks[o.type] || b.valHooks[o.nodeName.toLowerCase()], r && "get" in r && (n = r.get(o, "value")) !== t ? n : (n = o.value, "string" == typeof n ? n.replace(U, "") : null == n ? "" : n)
            }
        }
    }), b.extend({
        valHooks: {
            option: {
                get: function(e) {
                    var t = e.attributes.value;
                    return !t || t.specified ? e.value : e.text
                }
            },
            select: {
                get: function(e) {
                    var t, n, r = e.options,
                        i = e.selectedIndex,
                        o = "select-one" === e.type || 0 > i,
                        a = o ? null : [],
                        s = o ? i + 1 : r.length,
                        u = 0 > i ? s : o ? i : 0;
                    for (; s > u; u++)
                        if (n = r[u], !(!n.selected && u !== i || (b.support.optDisabled ? n.disabled : null !== n.getAttribute("disabled")) || n.parentNode.disabled && b.nodeName(n.parentNode, "optgroup"))) {
                            if (t = b(n).val(), o) return t;
                            a.push(t)
                        }
                    return a
                },
                set: function(e, t) {
                    var n = b.makeArray(t);
                    return b(e).find("option").each(function() {
                        this.selected = b.inArray(b(this).val(), n) >= 0
                    }), n.length || (e.selectedIndex = -1), n
                }
            }
        },
        attr: function(e, n, r) {
            var o, a, s, u = e.nodeType;
            if (e && 3 !== u && 8 !== u && 2 !== u) return typeof e.getAttribute === i ? b.prop(e, n, r) : (a = 1 !== u || !b.isXMLDoc(e), a && (n = n.toLowerCase(), o = b.attrHooks[n] || (J.test(n) ? z : I)), r === t ? o && a && "get" in o && null !== (s = o.get(e, n)) ? s : (typeof e.getAttribute !== i && (s = e.getAttribute(n)), null == s ? t : s) : null !== r ? o && a && "set" in o && (s = o.set(e, r, n)) !== t ? s : (e.setAttribute(n, r + ""), r) : (b.removeAttr(e, n), t))
        },
        removeAttr: function(e, t) {
            var n, r, i = 0,
                o = t && t.match(w);
            if (o && 1 === e.nodeType)
                while (n = o[i++]) r = b.propFix[n] || n, J.test(n) ? !Q && G.test(n) ? e[b.camelCase("default-" + n)] = e[r] = !1 : e[r] = !1 : b.attr(e, n, ""), e.removeAttribute(Q ? n : r)
        },
        attrHooks: {
            type: {
                set: function(e, t) {
                    if (!b.support.radioValue && "radio" === t && b.nodeName(e, "input")) {
                        var n = e.value;
                        return e.setAttribute("type", t), n && (e.value = n), t
                    }
                }
            }
        },
        propFix: {
            tabindex: "tabIndex",
            readonly: "readOnly",
            "for": "htmlFor",
            "class": "className",
            maxlength: "maxLength",
            cellspacing: "cellSpacing",
            cellpadding: "cellPadding",
            rowspan: "rowSpan",
            colspan: "colSpan",
            usemap: "useMap",
            frameborder: "frameBorder",
            contenteditable: "contentEditable"
        },
        prop: function(e, n, r) {
            var i, o, a, s = e.nodeType;
            if (e && 3 !== s && 8 !== s && 2 !== s) return a = 1 !== s || !b.isXMLDoc(e), a && (n = b.propFix[n] || n, o = b.propHooks[n]), r !== t ? o && "set" in o && (i = o.set(e, r, n)) !== t ? i : e[n] = r : o && "get" in o && null !== (i = o.get(e, n)) ? i : e[n]
        },
        propHooks: {
            tabIndex: {
                get: function(e) {
                    var n = e.getAttributeNode("tabindex");
                    return n && n.specified ? parseInt(n.value, 10) : V.test(e.nodeName) || Y.test(e.nodeName) && e.href ? 0 : t
                }
            }
        }
    }), z = {
        get: function(e, n) {
            var r = b.prop(e, n),
                i = "boolean" == typeof r && e.getAttribute(n),
                o = "boolean" == typeof r ? K && Q ? null != i : G.test(n) ? e[b.camelCase("default-" + n)] : !!i : e.getAttributeNode(n);
            return o && o.value !== !1 ? n.toLowerCase() : t
        },
        set: function(e, t, n) {
            return t === !1 ? b.removeAttr(e, n) : K && Q || !G.test(n) ? e.setAttribute(!Q && b.propFix[n] || n, n) : e[b.camelCase("default-" + n)] = e[n] = !0, n
        }
    }, K && Q || (b.attrHooks.value = {
        get: function(e, n) {
            var r = e.getAttributeNode(n);
            return b.nodeName(e, "input") ? e.defaultValue : r && r.specified ? r.value : t
        },
        set: function(e, n, r) {
            return b.nodeName(e, "input") ? (e.defaultValue = n, t) : I && I.set(e, n, r)
        }
    }), Q || (I = b.valHooks.button = {
        get: function(e, n) {
            var r = e.getAttributeNode(n);
            return r && ("id" === n || "name" === n || "coords" === n ? "" !== r.value : r.specified) ? r.value : t
        },
        set: function(e, n, r) {
            var i = e.getAttributeNode(r);
            return i || e.setAttributeNode(i = e.ownerDocument.createAttribute(r)), i.value = n += "", "value" === r || n === e.getAttribute(r) ? n : t
        }
    }, b.attrHooks.contenteditable = {
        get: I.get,
        set: function(e, t, n) {
            I.set(e, "" === t ? !1 : t, n)
        }
    }, b.each(["width", "height"], function(e, n) {
        b.attrHooks[n] = b.extend(b.attrHooks[n], {
            set: function(e, r) {
                return "" === r ? (e.setAttribute(n, "auto"), r) : t
            }
        })
    })), b.support.hrefNormalized || (b.each(["href", "src", "width", "height"], function(e, n) {
        b.attrHooks[n] = b.extend(b.attrHooks[n], {
            get: function(e) {
                var r = e.getAttribute(n, 2);
                return null == r ? t : r
            }
        })
    }), b.each(["href", "src"], function(e, t) {
        b.propHooks[t] = {
            get: function(e) {
                return e.getAttribute(t, 4)
            }
        }
    })), b.support.style || (b.attrHooks.style = {
        get: function(e) {
            return e.style.cssText || t
        },
        set: function(e, t) {
            return e.style.cssText = t + ""
        }
    }), b.support.optSelected || (b.propHooks.selected = b.extend(b.propHooks.selected, {
        get: function(e) {
            var t = e.parentNode;
            return t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex), null
        }
    })), b.support.enctype || (b.propFix.enctype = "encoding"), b.support.checkOn || b.each(["radio", "checkbox"], function() {
        b.valHooks[this] = {
            get: function(e) {
                return null === e.getAttribute("value") ? "on" : e.value
            }
        }
    }), b.each(["radio", "checkbox"], function() {
        b.valHooks[this] = b.extend(b.valHooks[this], {
            set: function(e, n) {
                return b.isArray(n) ? e.checked = b.inArray(b(e).val(), n) >= 0 : t
            }
        })
    });
    var Z = /^(?:input|select|textarea)$/i,
        et = /^key/,
        tt = /^(?:mouse|contextmenu)|click/,
        nt = /^(?:focusinfocus|focusoutblur)$/,
        rt = /^([^.]*)(?:\.(.+)|)$/;

    function it() {
        return !0
    }

    function ot() {
        return !1
    }
    b.event = {
            global: {},
            add: function(e, n, r, o, a) {
                var s, u, l, c, p, f, d, h, g, m, y, v = b._data(e);
                if (v) {
                    r.handler && (c = r, r = c.handler, a = c.selector), r.guid || (r.guid = b.guid++), (u = v.events) || (u = v.events = {}), (f = v.handle) || (f = v.handle = function(e) {
                        return typeof b === i || e && b.event.triggered === e.type ? t : b.event.dispatch.apply(f.elem, arguments)
                    }, f.elem = e), n = (n || "").match(w) || [""], l = n.length;
                    while (l--) s = rt.exec(n[l]) || [], g = y = s[1], m = (s[2] || "").split(".").sort(), p = b.event.special[g] || {}, g = (a ? p.delegateType : p.bindType) || g, p = b.event.special[g] || {}, d = b.extend({
                        type: g,
                        origType: y,
                        data: o,
                        handler: r,
                        guid: r.guid,
                        selector: a,
                        needsContext: a && b.expr.match.needsContext.test(a),
                        namespace: m.join(".")
                    }, c), (h = u[g]) || (h = u[g] = [], h.delegateCount = 0, p.setup && p.setup.call(e, o, m, f) !== !1 || (e.addEventListener ? e.addEventListener(g, f, !1) : e.attachEvent && e.attachEvent("on" + g, f))), p.add && (p.add.call(e, d), d.handler.guid || (d.handler.guid = r.guid)), a ? h.splice(h.delegateCount++, 0, d) : h.push(d), b.event.global[g] = !0;
                    e = null
                }
            },
            remove: function(e, t, n, r, i) {
                var o, a, s, u, l, c, p, f, d, h, g, m = b.hasData(e) && b._data(e);
                if (m && (c = m.events)) {
                    t = (t || "").match(w) || [""], l = t.length;
                    while (l--)
                        if (s = rt.exec(t[l]) || [], d = g = s[1], h = (s[2] || "").split(".").sort(), d) {
                            p = b.event.special[d] || {}, d = (r ? p.delegateType : p.bindType) || d, f = c[d] || [], s = s[2] && RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), u = o = f.length;
                            while (o--) a = f[o], !i && g !== a.origType || n && n.guid !== a.guid || s && !s.test(a.namespace) || r && r !== a.selector && ("**" !== r || !a.selector) || (f.splice(o, 1), a.selector && f.delegateCount--, p.remove && p.remove.call(e, a));
                            u && !f.length && (p.teardown && p.teardown.call(e, h, m.handle) !== !1 || b.removeEvent(e, d, m.handle), delete c[d])
                        } else
                            for (d in c) b.event.remove(e, d + t[l], n, r, !0);
                    b.isEmptyObject(c) && (delete m.handle, b._removeData(e, "events"))
                }
            },
            trigger: function(n, r, i, a) {
                var s, u, l, c, p, f, d, h = [i || o],
                    g = y.call(n, "type") ? n.type : n,
                    m = y.call(n, "namespace") ? n.namespace.split(".") : [];
                if (l = f = i = i || o, 3 !== i.nodeType && 8 !== i.nodeType && !nt.test(g + b.event.triggered) && (g.indexOf(".") >= 0 && (m = g.split("."), g = m.shift(), m.sort()), u = 0 > g.indexOf(":") && "on" + g, n = n[b.expando] ? n : new b.Event(g, "object" == typeof n && n), n.isTrigger = !0, n.namespace = m.join("."), n.namespace_re = n.namespace ? RegExp("(^|\\.)" + m.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, n.result = t, n.target || (n.target = i), r = null == r ? [n] : b.makeArray(r, [n]), p = b.event.special[g] || {}, a || !p.trigger || p.trigger.apply(i, r) !== !1)) {
                    if (!a && !p.noBubble && !b.isWindow(i)) {
                        for (c = p.delegateType || g, nt.test(c + g) || (l = l.parentNode); l; l = l.parentNode) h.push(l), f = l;
                        f === (i.ownerDocument || o) && h.push(f.defaultView || f.parentWindow || e)
                    }
                    d = 0;
                    while ((l = h[d++]) && !n.isPropagationStopped()) n.type = d > 1 ? c : p.bindType || g, s = (b._data(l, "events") || {})[n.type] && b._data(l, "handle"), s && s.apply(l, r), s = u && l[u], s && b.acceptData(l) && s.apply && s.apply(l, r) === !1 && n.preventDefault();
                    if (n.type = g, !(a || n.isDefaultPrevented() || p._default && p._default.apply(i.ownerDocument, r) !== !1 || "click" === g && b.nodeName(i, "a") || !b.acceptData(i) || !u || !i[g] || b.isWindow(i))) {
                        f = i[u], f && (i[u] = null), b.event.triggered = g;
                        try {
                            i[g]()
                        } catch (v) {}
                        b.event.triggered = t, f && (i[u] = f)
                    }
                    return n.result
                }
            },
            dispatch: function(e) {
                e = b.event.fix(e);
                var n, r, i, o, a, s = [],
                    u = h.call(arguments),
                    l = (b._data(this, "events") || {})[e.type] || [],
                    c = b.event.special[e.type] || {};
                if (u[0] = e, e.delegateTarget = this, !c.preDispatch || c.preDispatch.call(this, e) !== !1) {
                    s = b.event.handlers.call(this, e, l), n = 0;
                    while ((o = s[n++]) && !e.isPropagationStopped()) {
                        e.currentTarget = o.elem, a = 0;
                        while ((i = o.handlers[a++]) && !e.isImmediatePropagationStopped())(!e.namespace_re || e.namespace_re.test(i.namespace)) && (e.handleObj = i, e.data = i.data, r = ((b.event.special[i.origType] || {}).handle || i.handler).apply(o.elem, u), r !== t && (e.result = r) === !1 && (e.preventDefault(), e.stopPropagation()))
                    }
                    return c.postDispatch && c.postDispatch.call(this, e), e.result
                }
            },
            handlers: function(e, n) {
                var r, i, o, a, s = [],
                    u = n.delegateCount,
                    l = e.target;
                if (u && l.nodeType && (!e.button || "click" !== e.type))
                    for (; l != this; l = l.parentNode || this)
                        if (1 === l.nodeType && (l.disabled !== !0 || "click" !== e.type)) {
                            for (o = [], a = 0; u > a; a++) i = n[a], r = i.selector + " ", o[r] === t && (o[r] = i.needsContext ? b(r, this).index(l) >= 0 : b.find(r, this, null, [l]).length), o[r] && o.push(i);
                            o.length && s.push({
                                elem: l,
                                handlers: o
                            })
                        }
                return n.length > u && s.push({
                    elem: this,
                    handlers: n.slice(u)
                }), s
            },
            fix: function(e) {
                if (e[b.expando]) return e;
                var t, n, r, i = e.type,
                    a = e,
                    s = this.fixHooks[i];
                s || (this.fixHooks[i] = s = tt.test(i) ? this.mouseHooks : et.test(i) ? this.keyHooks : {}), r = s.props ? this.props.concat(s.props) : this.props, e = new b.Event(a), t = r.length;
                while (t--) n = r[t], e[n] = a[n];
                return e.target || (e.target = a.srcElement || o), 3 === e.target.nodeType && (e.target = e.target.parentNode), e.metaKey = !!e.metaKey, s.filter ? s.filter(e, a) : e
            },
            props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
            fixHooks: {},
            keyHooks: {
                props: "char charCode key keyCode".split(" "),
                filter: function(e, t) {
                    return null == e.which && (e.which = null != t.charCode ? t.charCode : t.keyCode), e
                }
            },
            mouseHooks: {
                props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
                filter: function(e, n) {
                    var r, i, a, s = n.button,
                        u = n.fromElement;
                    return null == e.pageX && null != n.clientX && (i = e.target.ownerDocument || o, a = i.documentElement, r = i.body, e.pageX = n.clientX + (a && a.scrollLeft || r && r.scrollLeft || 0) - (a && a.clientLeft || r && r.clientLeft || 0), e.pageY = n.clientY + (a && a.scrollTop || r && r.scrollTop || 0) - (a && a.clientTop || r && r.clientTop || 0)), !e.relatedTarget && u && (e.relatedTarget = u === e.target ? n.toElement : u), e.which || s === t || (e.which = 1 & s ? 1 : 2 & s ? 3 : 4 & s ? 2 : 0), e
                }
            },
            special: {
                load: {
                    noBubble: !0
                },
                click: {
                    trigger: function() {
                        return b.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(), !1) : t
                    }
                },
                focus: {
                    trigger: function() {
                        if (this !== o.activeElement && this.focus) try {
                            return this.focus(), !1
                        } catch (e) {}
                    },
                    delegateType: "focusin"
                },
                blur: {
                    trigger: function() {
                        return this === o.activeElement && this.blur ? (this.blur(), !1) : t
                    },
                    delegateType: "focusout"
                },
                beforeunload: {
                    postDispatch: function(e) {
                        e.result !== t && (e.originalEvent.returnValue = e.result)
                    }
                }
            },
            simulate: function(e, t, n, r) {
                var i = b.extend(new b.Event, n, {
                    type: e,
                    isSimulated: !0,
                    originalEvent: {}
                });
                r ? b.event.trigger(i, null, t) : b.event.dispatch.call(t, i), i.isDefaultPrevented() && n.preventDefault()
            }
        }, b.removeEvent = o.removeEventListener ? function(e, t, n) {
            e.removeEventListener && e.removeEventListener(t, n, !1)
        } : function(e, t, n) {
            var r = "on" + t;
            e.detachEvent && (typeof e[r] === i && (e[r] = null), e.detachEvent(r, n))
        }, b.Event = function(e, n) {
            return this instanceof b.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || e.returnValue === !1 || e.getPreventDefault && e.getPreventDefault() ? it : ot) : this.type = e, n && b.extend(this, n), this.timeStamp = e && e.timeStamp || b.now(), this[b.expando] = !0, t) : new b.Event(e, n)
        }, b.Event.prototype = {
            isDefaultPrevented: ot,
            isPropagationStopped: ot,
            isImmediatePropagationStopped: ot,
            preventDefault: function() {
                var e = this.originalEvent;
                this.isDefaultPrevented = it, e && (e.preventDefault ? e.preventDefault() : e.returnValue = !1)
            },
            stopPropagation: function() {
                var e = this.originalEvent;
                this.isPropagationStopped = it, e && (e.stopPropagation && e.stopPropagation(), e.cancelBubble = !0)
            },
            stopImmediatePropagation: function() {
                this.isImmediatePropagationStopped = it, this.stopPropagation()
            }
        }, b.each({
            mouseenter: "mouseover",
            mouseleave: "mouseout"
        }, function(e, t) {
            b.event.special[e] = {
                delegateType: t,
                bindType: t,
                handle: function(e) {
                    var n, r = this,
                        i = e.relatedTarget,
                        o = e.handleObj;
                    return (!i || i !== r && !b.contains(r, i)) && (e.type = o.origType, n = o.handler.apply(this, arguments), e.type = t), n
                }
            }
        }), b.support.submitBubbles || (b.event.special.submit = {
            setup: function() {
                return b.nodeName(this, "form") ? !1 : (b.event.add(this, "click._submit keypress._submit", function(e) {
                    var n = e.target,
                        r = b.nodeName(n, "input") || b.nodeName(n, "button") ? n.form : t;
                    r && !b._data(r, "submitBubbles") && (b.event.add(r, "submit._submit", function(e) {
                        e._submit_bubble = !0
                    }), b._data(r, "submitBubbles", !0))
                }), t)
            },
            postDispatch: function(e) {
                e._submit_bubble && (delete e._submit_bubble, this.parentNode && !e.isTrigger && b.event.simulate("submit", this.parentNode, e, !0))
            },
            teardown: function() {
                return b.nodeName(this, "form") ? !1 : (b.event.remove(this, "._submit"), t)
            }
        }), b.support.changeBubbles || (b.event.special.change = {
            setup: function() {
                return Z.test(this.nodeName) ? (("checkbox" === this.type || "radio" === this.type) && (b.event.add(this, "propertychange._change", function(e) {
                    "checked" === e.originalEvent.propertyName && (this._just_changed = !0)
                }), b.event.add(this, "click._change", function(e) {
                    this._just_changed && !e.isTrigger && (this._just_changed = !1), b.event.simulate("change", this, e, !0)
                })), !1) : (b.event.add(this, "beforeactivate._change", function(e) {
                    var t = e.target;
                    Z.test(t.nodeName) && !b._data(t, "changeBubbles") && (b.event.add(t, "change._change", function(e) {
                        !this.parentNode || e.isSimulated || e.isTrigger || b.event.simulate("change", this.parentNode, e, !0)
                    }), b._data(t, "changeBubbles", !0))
                }), t)
            },
            handle: function(e) {
                var n = e.target;
                return this !== n || e.isSimulated || e.isTrigger || "radio" !== n.type && "checkbox" !== n.type ? e.handleObj.handler.apply(this, arguments) : t
            },
            teardown: function() {
                return b.event.remove(this, "._change"), !Z.test(this.nodeName)
            }
        }), b.support.focusinBubbles || b.each({
            focus: "focusin",
            blur: "focusout"
        }, function(e, t) {
            var n = 0,
                r = function(e) {
                    b.event.simulate(t, e.target, b.event.fix(e), !0)
                };
            b.event.special[t] = {
                setup: function() {
                    0 === n++ && o.addEventListener(e, r, !0)
                },
                teardown: function() {
                    0 === --n && o.removeEventListener(e, r, !0)
                }
            }
        }), b.fn.extend({
            on: function(e, n, r, i, o) {
                var a, s;
                if ("object" == typeof e) {
                    "string" != typeof n && (r = r || n, n = t);
                    for (a in e) this.on(a, n, r, e[a], o);
                    return this
                }
                if (null == r && null == i ? (i = n, r = n = t) : null == i && ("string" == typeof n ? (i = r, r = t) : (i = r, r = n, n = t)), i === !1) i = ot;
                else if (!i) return this;
                return 1 === o && (s = i, i = function(e) {
                    return b().off(e), s.apply(this, arguments)
                }, i.guid = s.guid || (s.guid = b.guid++)), this.each(function() {
                    b.event.add(this, e, i, r, n)
                })
            },
            one: function(e, t, n, r) {
                return this.on(e, t, n, r, 1)
            },
            off: function(e, n, r) {
                var i, o;
                if (e && e.preventDefault && e.handleObj) return i = e.handleObj, b(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
                if ("object" == typeof e) {
                    for (o in e) this.off(o, n, e[o]);
                    return this
                }
                return (n === !1 || "function" == typeof n) && (r = n, n = t), r === !1 && (r = ot), this.each(function() {
                    b.event.remove(this, e, r, n)
                })
            },
            bind: function(e, t, n) {
                return this.on(e, null, t, n)
            },
            unbind: function(e, t) {
                return this.off(e, null, t)
            },
            delegate: function(e, t, n, r) {
                return this.on(t, e, n, r)
            },
            undelegate: function(e, t, n) {
                return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
            },
            trigger: function(e, t) {
                return this.each(function() {
                    b.event.trigger(e, t, this)
                })
            },
            triggerHandler: function(e, n) {
                var r = this[0];
                return r ? b.event.trigger(e, n, r, !0) : t
            }
        }),
        function(e, t) {
            var n, r, i, o, a, s, u, l, c, p, f, d, h, g, m, y, v, x = "sizzle" + -new Date,
                w = e.document,
                T = {},
                N = 0,
                C = 0,
                k = it(),
                E = it(),
                S = it(),
                A = typeof t,
                j = 1 << 31,
                D = [],
                L = D.pop,
                H = D.push,
                q = D.slice,
                M = D.indexOf || function(e) {
                    var t = 0,
                        n = this.length;
                    for (; n > t; t++)
                        if (this[t] === e) return t;
                    return -1
                },
                _ = "[\\x20\\t\\r\\n\\f]",
                F = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
                O = F.replace("w", "w#"),
                B = "([*^$|!~]?=)",
                P = "\\[" + _ + "*(" + F + ")" + _ + "*(?:" + B + _ + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + O + ")|)|)" + _ + "*\\]",
                R = ":(" + F + ")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|" + P.replace(3, 8) + ")*)|.*)\\)|)",
                W = RegExp("^" + _ + "+|((?:^|[^\\\\])(?:\\\\.)*)" + _ + "+$", "g"),
                $ = RegExp("^" + _ + "*," + _ + "*"),
                I = RegExp("^" + _ + "*([\\x20\\t\\r\\n\\f>+~])" + _ + "*"),
                z = RegExp(R),
                X = RegExp("^" + O + "$"),
                U = {
                    ID: RegExp("^#(" + F + ")"),
                    CLASS: RegExp("^\\.(" + F + ")"),
                    NAME: RegExp("^\\[name=['\"]?(" + F + ")['\"]?\\]"),
                    TAG: RegExp("^(" + F.replace("w", "w*") + ")"),
                    ATTR: RegExp("^" + P),
                    PSEUDO: RegExp("^" + R),
                    CHILD: RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + _ + "*(even|odd|(([+-]|)(\\d*)n|)" + _ + "*(?:([+-]|)" + _ + "*(\\d+)|))" + _ + "*\\)|)", "i"),
                    needsContext: RegExp("^" + _ + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + _ + "*((?:-\\d)?\\d*)" + _ + "*\\)|)(?=[^-]|$)", "i")
                },
                V = /[\x20\t\r\n\f]*[+~]/,
                Y = /^[^{]+\{\s*\[native code/,
                J = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
                G = /^(?:input|select|textarea|button)$/i,
                Q = /^h\d$/i,
                K = /'|\\/g,
                Z = /\=[\x20\t\r\n\f]*([^'"\]]*)[\x20\t\r\n\f]*\]/g,
                et = /\\([\da-fA-F]{1,6}[\x20\t\r\n\f]?|.)/g,
                tt = function(e, t) {
                    var n = "0x" + t - 65536;
                    return n !== n ? t : 0 > n ? String.fromCharCode(n + 65536) : String.fromCharCode(55296 | n >> 10, 56320 | 1023 & n)
                };
            try {
                q.call(w.documentElement.childNodes, 0)[0].nodeType
            } catch (nt) {
                q = function(e) {
                    var t, n = [];
                    while (t = this[e++]) n.push(t);
                    return n
                }
            }

            function rt(e) {
                return Y.test(e + "")
            }

            function it() {
                var e, t = [];
                return e = function(n, r) {
                    return t.push(n += " ") > i.cacheLength && delete e[t.shift()], e[n] = r
                }
            }

            function ot(e) {
                return e[x] = !0, e
            }

            function at(e) {
                var t = p.createElement("div");
                try {
                    return e(t)
                } catch (n) {
                    return !1
                } finally {
                    t = null
                }
            }

            function st(e, t, n, r) {
                var i, o, a, s, u, l, f, g, m, v;
                if ((t ? t.ownerDocument || t : w) !== p && c(t), t = t || p, n = n || [], !e || "string" != typeof e) return n;
                if (1 !== (s = t.nodeType) && 9 !== s) return [];
                if (!d && !r) {
                    if (i = J.exec(e))
                        if (a = i[1]) {
                            if (9 === s) {
                                if (o = t.getElementById(a), !o || !o.parentNode) return n;
                                if (o.id === a) return n.push(o), n
                            } else if (t.ownerDocument && (o = t.ownerDocument.getElementById(a)) && y(t, o) && o.id === a) return n.push(o), n
                        } else {
                            if (i[2]) return H.apply(n, q.call(t.getElementsByTagName(e), 0)), n;
                            if ((a = i[3]) && T.getByClassName && t.getElementsByClassName) return H.apply(n, q.call(t.getElementsByClassName(a), 0)), n
                        }
                    if (T.qsa && !h.test(e)) {
                        if (f = !0, g = x, m = t, v = 9 === s && e, 1 === s && "object" !== t.nodeName.toLowerCase()) {
                            l = ft(e), (f = t.getAttribute("id")) ? g = f.replace(K, "\\$&") : t.setAttribute("id", g), g = "[id='" + g + "'] ", u = l.length;
                            while (u--) l[u] = g + dt(l[u]);
                            m = V.test(e) && t.parentNode || t, v = l.join(",")
                        }
                        if (v) try {
                            return H.apply(n, q.call(m.querySelectorAll(v), 0)), n
                        } catch (b) {} finally {
                            f || t.removeAttribute("id")
                        }
                    }
                }
                return wt(e.replace(W, "$1"), t, n, r)
            }
            a = st.isXML = function(e) {
                var t = e && (e.ownerDocument || e).documentElement;
                return t ? "HTML" !== t.nodeName : !1
            }, c = st.setDocument = function(e) {
                var n = e ? e.ownerDocument || e : w;
                return n !== p && 9 === n.nodeType && n.documentElement ? (p = n, f = n.documentElement, d = a(n), T.tagNameNoComments = at(function(e) {
                    return e.appendChild(n.createComment("")), !e.getElementsByTagName("*").length
                }), T.attributes = at(function(e) {
                    e.innerHTML = "<select></select>";
                    var t = typeof e.lastChild.getAttribute("multiple");
                    return "boolean" !== t && "string" !== t
                }), T.getByClassName = at(function(e) {
                    return e.innerHTML = "<div class='hidden e'></div><div class='hidden'></div>", e.getElementsByClassName && e.getElementsByClassName("e").length ? (e.lastChild.className = "e", 2 === e.getElementsByClassName("e").length) : !1
                }), T.getByName = at(function(e) {
                    e.id = x + 0, e.innerHTML = "<a name='" + x + "'></a><div name='" + x + "'></div>", f.insertBefore(e, f.firstChild);
                    var t = n.getElementsByName && n.getElementsByName(x).length === 2 + n.getElementsByName(x + 0).length;
                    return T.getIdNotName = !n.getElementById(x), f.removeChild(e), t
                }), i.attrHandle = at(function(e) {
                    return e.innerHTML = "<a href='#'></a>", e.firstChild && typeof e.firstChild.getAttribute !== A && "#" === e.firstChild.getAttribute("href")
                }) ? {} : {
                    href: function(e) {
                        return e.getAttribute("href", 2)
                    },
                    type: function(e) {
                        return e.getAttribute("type")
                    }
                }, T.getIdNotName ? (i.find.ID = function(e, t) {
                    if (typeof t.getElementById !== A && !d) {
                        var n = t.getElementById(e);
                        return n && n.parentNode ? [n] : []
                    }
                }, i.filter.ID = function(e) {
                    var t = e.replace(et, tt);
                    return function(e) {
                        return e.getAttribute("id") === t
                    }
                }) : (i.find.ID = function(e, n) {
                    if (typeof n.getElementById !== A && !d) {
                        var r = n.getElementById(e);
                        return r ? r.id === e || typeof r.getAttributeNode !== A && r.getAttributeNode("id").value === e ? [r] : t : []
                    }
                }, i.filter.ID = function(e) {
                    var t = e.replace(et, tt);
                    return function(e) {
                        var n = typeof e.getAttributeNode !== A && e.getAttributeNode("id");
                        return n && n.value === t
                    }
                }), i.find.TAG = T.tagNameNoComments ? function(e, n) {
                    return typeof n.getElementsByTagName !== A ? n.getElementsByTagName(e) : t
                } : function(e, t) {
                    var n, r = [],
                        i = 0,
                        o = t.getElementsByTagName(e);
                    if ("*" === e) {
                        while (n = o[i++]) 1 === n.nodeType && r.push(n);
                        return r
                    }
                    return o
                }, i.find.NAME = T.getByName && function(e, n) {
                    return typeof n.getElementsByName !== A ? n.getElementsByName(name) : t
                }, i.find.CLASS = T.getByClassName && function(e, n) {
                    return typeof n.getElementsByClassName === A || d ? t : n.getElementsByClassName(e)
                }, g = [], h = [":focus"], (T.qsa = rt(n.querySelectorAll)) && (at(function(e) {
                    e.innerHTML = "<select><option selected=''></option></select>", e.querySelectorAll("[selected]").length || h.push("\\[" + _ + "*(?:checked|disabled|ismap|multiple|readonly|selected|value)"), e.querySelectorAll(":checked").length || h.push(":checked")
                }), at(function(e) {
                    e.innerHTML = "<input type='hidden' i=''/>", e.querySelectorAll("[i^='']").length && h.push("[*^$]=" + _ + "*(?:\"\"|'')"), e.querySelectorAll(":enabled").length || h.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), h.push(",.*:")
                })), (T.matchesSelector = rt(m = f.matchesSelector || f.mozMatchesSelector || f.webkitMatchesSelector || f.oMatchesSelector || f.msMatchesSelector)) && at(function(e) {
                    T.disconnectedMatch = m.call(e, "div"), m.call(e, "[s!='']:x"), g.push("!=", R)
                }), h = RegExp(h.join("|")), g = RegExp(g.join("|")), y = rt(f.contains) || f.compareDocumentPosition ? function(e, t) {
                    var n = 9 === e.nodeType ? e.documentElement : e,
                        r = t && t.parentNode;
                    return e === r || !(!r || 1 !== r.nodeType || !(n.contains ? n.contains(r) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(r)))
                } : function(e, t) {
                    if (t)
                        while (t = t.parentNode)
                            if (t === e) return !0;
                    return !1
                }, v = f.compareDocumentPosition ? function(e, t) {
                    var r;
                    return e === t ? (u = !0, 0) : (r = t.compareDocumentPosition && e.compareDocumentPosition && e.compareDocumentPosition(t)) ? 1 & r || e.parentNode && 11 === e.parentNode.nodeType ? e === n || y(w, e) ? -1 : t === n || y(w, t) ? 1 : 0 : 4 & r ? -1 : 1 : e.compareDocumentPosition ? -1 : 1
                } : function(e, t) {
                    var r, i = 0,
                        o = e.parentNode,
                        a = t.parentNode,
                        s = [e],
                        l = [t];
                    if (e === t) return u = !0, 0;
                    if (!o || !a) return e === n ? -1 : t === n ? 1 : o ? -1 : a ? 1 : 0;
                    if (o === a) return ut(e, t);
                    r = e;
                    while (r = r.parentNode) s.unshift(r);
                    r = t;
                    while (r = r.parentNode) l.unshift(r);
                    while (s[i] === l[i]) i++;
                    return i ? ut(s[i], l[i]) : s[i] === w ? -1 : l[i] === w ? 1 : 0
                }, u = !1, [0, 0].sort(v), T.detectDuplicates = u, p) : p
            }, st.matches = function(e, t) {
                return st(e, null, null, t)
            }, st.matchesSelector = function(e, t) {
                if ((e.ownerDocument || e) !== p && c(e), t = t.replace(Z, "='$1']"), !(!T.matchesSelector || d || g && g.test(t) || h.test(t))) try {
                    var n = m.call(e, t);
                    if (n || T.disconnectedMatch || e.document && 11 !== e.document.nodeType) return n
                } catch (r) {}
                return st(t, p, null, [e]).length > 0
            }, st.contains = function(e, t) {
                return (e.ownerDocument || e) !== p && c(e), y(e, t)
            }, st.attr = function(e, t) {
                var n;
                return (e.ownerDocument || e) !== p && c(e), d || (t = t.toLowerCase()), (n = i.attrHandle[t]) ? n(e) : d || T.attributes ? e.getAttribute(t) : ((n = e.getAttributeNode(t)) || e.getAttribute(t)) && e[t] === !0 ? t : n && n.specified ? n.value : null
            }, st.error = function(e) {
                throw Error("Syntax error, unrecognized expression: " + e)
            }, st.uniqueSort = function(e) {
                var t, n = [],
                    r = 1,
                    i = 0;
                if (u = !T.detectDuplicates, e.sort(v), u) {
                    for (; t = e[r]; r++) t === e[r - 1] && (i = n.push(r));
                    while (i--) e.splice(n[i], 1)
                }
                return e
            };

            function ut(e, t) {
                var n = t && e,
                    r = n && (~t.sourceIndex || j) - (~e.sourceIndex || j);
                if (r) return r;
                if (n)
                    while (n = n.nextSibling)
                        if (n === t) return -1;
                return e ? 1 : -1
            }

            function lt(e) {
                return function(t) {
                    var n = t.nodeName.toLowerCase();
                    return "input" === n && t.type === e
                }
            }

            function ct(e) {
                return function(t) {
                    var n = t.nodeName.toLowerCase();
                    return ("input" === n || "button" === n) && t.type === e
                }
            }

            function pt(e) {
                return ot(function(t) {
                    return t = +t, ot(function(n, r) {
                        var i, o = e([], n.length, t),
                            a = o.length;
                        while (a--) n[i = o[a]] && (n[i] = !(r[i] = n[i]))
                    })
                })
            }
            o = st.getText = function(e) {
                var t, n = "",
                    r = 0,
                    i = e.nodeType;
                if (i) {
                    if (1 === i || 9 === i || 11 === i) {
                        if ("string" == typeof e.textContent) return e.textContent;
                        for (e = e.firstChild; e; e = e.nextSibling) n += o(e)
                    } else if (3 === i || 4 === i) return e.nodeValue
                } else
                    for (; t = e[r]; r++) n += o(t);
                return n
            }, i = st.selectors = {
                cacheLength: 50,
                createPseudo: ot,
                match: U,
                find: {},
                relative: {
                    ">": {
                        dir: "parentNode",
                        first: !0
                    },
                    " ": {
                        dir: "parentNode"
                    },
                    "+": {
                        dir: "previousSibling",
                        first: !0
                    },
                    "~": {
                        dir: "previousSibling"
                    }
                },
                preFilter: {
                    ATTR: function(e) {
                        return e[1] = e[1].replace(et, tt), e[3] = (e[4] || e[5] || "").replace(et, tt), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                    },
                    CHILD: function(e) {
                        return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || st.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && st.error(e[0]), e
                    },
                    PSEUDO: function(e) {
                        var t, n = !e[5] && e[2];
                        return U.CHILD.test(e[0]) ? null : (e[4] ? e[2] = e[4] : n && z.test(n) && (t = ft(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
                    }
                },
                filter: {
                    TAG: function(e) {
                        return "*" === e ? function() {
                            return !0
                        } : (e = e.replace(et, tt).toLowerCase(), function(t) {
                            return t.nodeName && t.nodeName.toLowerCase() === e
                        })
                    },
                    CLASS: function(e) {
                        var t = k[e + " "];
                        return t || (t = RegExp("(^|" + _ + ")" + e + "(" + _ + "|$)")) && k(e, function(e) {
                            return t.test(e.className || typeof e.getAttribute !== A && e.getAttribute("class") || "")
                        })
                    },
                    ATTR: function(e, t, n) {
                        return function(r) {
                            var i = st.attr(r, e);
                            return null == i ? "!=" === t : t ? (i += "", "=" === t ? i === n : "!=" === t ? i !== n : "^=" === t ? n && 0 === i.indexOf(n) : "*=" === t ? n && i.indexOf(n) > -1 : "$=" === t ? n && i.slice(-n.length) === n : "~=" === t ? (" " + i + " ").indexOf(n) > -1 : "|=" === t ? i === n || i.slice(0, n.length + 1) === n + "-" : !1) : !0
                        }
                    },
                    CHILD: function(e, t, n, r, i) {
                        var o = "nth" !== e.slice(0, 3),
                            a = "last" !== e.slice(-4),
                            s = "of-type" === t;
                        return 1 === r && 0 === i ? function(e) {
                            return !!e.parentNode
                        } : function(t, n, u) {
                            var l, c, p, f, d, h, g = o !== a ? "nextSibling" : "previousSibling",
                                m = t.parentNode,
                                y = s && t.nodeName.toLowerCase(),
                                v = !u && !s;
                            if (m) {
                                if (o) {
                                    while (g) {
                                        p = t;
                                        while (p = p[g])
                                            if (s ? p.nodeName.toLowerCase() === y : 1 === p.nodeType) return !1;
                                        h = g = "only" === e && !h && "nextSibling"
                                    }
                                    return !0
                                }
                                if (h = [a ? m.firstChild : m.lastChild], a && v) {
                                    c = m[x] || (m[x] = {}), l = c[e] || [], d = l[0] === N && l[1], f = l[0] === N && l[2], p = d && m.childNodes[d];
                                    while (p = ++d && p && p[g] || (f = d = 0) || h.pop())
                                        if (1 === p.nodeType && ++f && p === t) {
                                            c[e] = [N, d, f];
                                            break
                                        }
                                } else if (v && (l = (t[x] || (t[x] = {}))[e]) && l[0] === N) f = l[1];
                                else
                                    while (p = ++d && p && p[g] || (f = d = 0) || h.pop())
                                        if ((s ? p.nodeName.toLowerCase() === y : 1 === p.nodeType) && ++f && (v && ((p[x] || (p[x] = {}))[e] = [N, f]), p === t)) break; return f -= i, f === r || 0 === f % r && f / r >= 0
                            }
                        }
                    },
                    PSEUDO: function(e, t) {
                        var n, r = i.pseudos[e] || i.setFilters[e.toLowerCase()] || st.error("unsupported pseudo: " + e);
                        return r[x] ? r(t) : r.length > 1 ? (n = [e, e, "", t], i.setFilters.hasOwnProperty(e.toLowerCase()) ? ot(function(e, n) {
                            var i, o = r(e, t),
                                a = o.length;
                            while (a--) i = M.call(e, o[a]), e[i] = !(n[i] = o[a])
                        }) : function(e) {
                            return r(e, 0, n)
                        }) : r
                    }
                },
                pseudos: {
                    not: ot(function(e) {
                        var t = [],
                            n = [],
                            r = s(e.replace(W, "$1"));
                        return r[x] ? ot(function(e, t, n, i) {
                            var o, a = r(e, null, i, []),
                                s = e.length;
                            while (s--)(o = a[s]) && (e[s] = !(t[s] = o))
                        }) : function(e, i, o) {
                            return t[0] = e, r(t, null, o, n), !n.pop()
                        }
                    }),
                    has: ot(function(e) {
                        return function(t) {
                            return st(e, t).length > 0
                        }
                    }),
                    contains: ot(function(e) {
                        return function(t) {
                            return (t.textContent || t.innerText || o(t)).indexOf(e) > -1
                        }
                    }),
                    lang: ot(function(e) {
                        return X.test(e || "") || st.error("unsupported lang: " + e), e = e.replace(et, tt).toLowerCase(),
                            function(t) {
                                var n;
                                do
                                    if (n = d ? t.getAttribute("xml:lang") || t.getAttribute("lang") : t.lang) return n = n.toLowerCase(), n === e || 0 === n.indexOf(e + "-");
                                while ((t = t.parentNode) && 1 === t.nodeType);
                                return !1
                            }
                    }),
                    target: function(t) {
                        var n = e.location && e.location.hash;
                        return n && n.slice(1) === t.id
                    },
                    root: function(e) {
                        return e === f
                    },
                    focus: function(e) {
                        return e === p.activeElement && (!p.hasFocus || p.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                    },
                    enabled: function(e) {
                        return e.disabled === !1
                    },
                    disabled: function(e) {
                        return e.disabled === !0
                    },
                    checked: function(e) {
                        var t = e.nodeName.toLowerCase();
                        return "input" === t && !!e.checked || "option" === t && !!e.selected
                    },
                    selected: function(e) {
                        return e.parentNode && e.parentNode.selectedIndex, e.selected === !0
                    },
                    empty: function(e) {
                        for (e = e.firstChild; e; e = e.nextSibling)
                            if (e.nodeName > "@" || 3 === e.nodeType || 4 === e.nodeType) return !1;
                        return !0
                    },
                    parent: function(e) {
                        return !i.pseudos.empty(e)
                    },
                    header: function(e) {
                        return Q.test(e.nodeName)
                    },
                    input: function(e) {
                        return G.test(e.nodeName)
                    },
                    button: function(e) {
                        var t = e.nodeName.toLowerCase();
                        return "input" === t && "button" === e.type || "button" === t
                    },
                    text: function(e) {
                        var t;
                        return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || t.toLowerCase() === e.type)
                    },
                    first: pt(function() {
                        return [0]
                    }),
                    last: pt(function(e, t) {
                        return [t - 1]
                    }),
                    eq: pt(function(e, t, n) {
                        return [0 > n ? n + t : n]
                    }),
                    even: pt(function(e, t) {
                        var n = 0;
                        for (; t > n; n += 2) e.push(n);
                        return e
                    }),
                    odd: pt(function(e, t) {
                        var n = 1;
                        for (; t > n; n += 2) e.push(n);
                        return e
                    }),
                    lt: pt(function(e, t, n) {
                        var r = 0 > n ? n + t : n;
                        for (; --r >= 0;) e.push(r);
                        return e
                    }),
                    gt: pt(function(e, t, n) {
                        var r = 0 > n ? n + t : n;
                        for (; t > ++r;) e.push(r);
                        return e
                    })
                }
            };
            for (n in {
                    radio: !0,
                    checkbox: !0,
                    file: !0,
                    password: !0,
                    image: !0
                }) i.pseudos[n] = lt(n);
            for (n in {
                    submit: !0,
                    reset: !0
                }) i.pseudos[n] = ct(n);

            function ft(e, t) {
                var n, r, o, a, s, u, l, c = E[e + " "];
                if (c) return t ? 0 : c.slice(0);
                s = e, u = [], l = i.preFilter;
                while (s) {
                    (!n || (r = $.exec(s))) && (r && (s = s.slice(r[0].length) || s), u.push(o = [])), n = !1, (r = I.exec(s)) && (n = r.shift(), o.push({
                        value: n,
                        type: r[0].replace(W, " ")
                    }), s = s.slice(n.length));
                    for (a in i.filter) !(r = U[a].exec(s)) || l[a] && !(r = l[a](r)) || (n = r.shift(), o.push({
                        value: n,
                        type: a,
                        matches: r
                    }), s = s.slice(n.length));
                    if (!n) break
                }
                return t ? s.length : s ? st.error(e) : E(e, u).slice(0)
            }

            function dt(e) {
                var t = 0,
                    n = e.length,
                    r = "";
                for (; n > t; t++) r += e[t].value;
                return r
            }

            function ht(e, t, n) {
                var i = t.dir,
                    o = n && "parentNode" === i,
                    a = C++;
                return t.first ? function(t, n, r) {
                    while (t = t[i])
                        if (1 === t.nodeType || o) return e(t, n, r)
                } : function(t, n, s) {
                    var u, l, c, p = N + " " + a;
                    if (s) {
                        while (t = t[i])
                            if ((1 === t.nodeType || o) && e(t, n, s)) return !0
                    } else
                        while (t = t[i])
                            if (1 === t.nodeType || o)
                                if (c = t[x] || (t[x] = {}), (l = c[i]) && l[0] === p) {
                                    if ((u = l[1]) === !0 || u === r) return u === !0
                                } else if (l = c[i] = [p], l[1] = e(t, n, s) || r, l[1] === !0) return !0
                }
            }

            function gt(e) {
                return e.length > 1 ? function(t, n, r) {
                    var i = e.length;
                    while (i--)
                        if (!e[i](t, n, r)) return !1;
                    return !0
                } : e[0]
            }

            function mt(e, t, n, r, i) {
                var o, a = [],
                    s = 0,
                    u = e.length,
                    l = null != t;
                for (; u > s; s++)(o = e[s]) && (!n || n(o, r, i)) && (a.push(o), l && t.push(s));
                return a
            }

            function yt(e, t, n, r, i, o) {
                return r && !r[x] && (r = yt(r)), i && !i[x] && (i = yt(i, o)), ot(function(o, a, s, u) {
                    var l, c, p, f = [],
                        d = [],
                        h = a.length,
                        g = o || xt(t || "*", s.nodeType ? [s] : s, []),
                        m = !e || !o && t ? g : mt(g, f, e, s, u),
                        y = n ? i || (o ? e : h || r) ? [] : a : m;
                    if (n && n(m, y, s, u), r) {
                        l = mt(y, d), r(l, [], s, u), c = l.length;
                        while (c--)(p = l[c]) && (y[d[c]] = !(m[d[c]] = p))
                    }
                    if (o) {
                        if (i || e) {
                            if (i) {
                                l = [], c = y.length;
                                while (c--)(p = y[c]) && l.push(m[c] = p);
                                i(null, y = [], l, u)
                            }
                            c = y.length;
                            while (c--)(p = y[c]) && (l = i ? M.call(o, p) : f[c]) > -1 && (o[l] = !(a[l] = p))
                        }
                    } else y = mt(y === a ? y.splice(h, y.length) : y), i ? i(null, a, y, u) : H.apply(a, y)
                })
            }

            function vt(e) {
                var t, n, r, o = e.length,
                    a = i.relative[e[0].type],
                    s = a || i.relative[" "],
                    u = a ? 1 : 0,
                    c = ht(function(e) {
                        return e === t
                    }, s, !0),
                    p = ht(function(e) {
                        return M.call(t, e) > -1
                    }, s, !0),
                    f = [function(e, n, r) {
                        return !a && (r || n !== l) || ((t = n).nodeType ? c(e, n, r) : p(e, n, r))
                    }];
                for (; o > u; u++)
                    if (n = i.relative[e[u].type]) f = [ht(gt(f), n)];
                    else {
                        if (n = i.filter[e[u].type].apply(null, e[u].matches), n[x]) {
                            for (r = ++u; o > r; r++)
                                if (i.relative[e[r].type]) break;
                            return yt(u > 1 && gt(f), u > 1 && dt(e.slice(0, u - 1)).replace(W, "$1"), n, r > u && vt(e.slice(u, r)), o > r && vt(e = e.slice(r)), o > r && dt(e))
                        }
                        f.push(n)
                    }
                return gt(f)
            }

            function bt(e, t) {
                var n = 0,
                    o = t.length > 0,
                    a = e.length > 0,
                    s = function(s, u, c, f, d) {
                        var h, g, m, y = [],
                            v = 0,
                            b = "0",
                            x = s && [],
                            w = null != d,
                            T = l,
                            C = s || a && i.find.TAG("*", d && u.parentNode || u),
                            k = N += null == T ? 1 : Math.random() || .1;
                        for (w && (l = u !== p && u, r = n); null != (h = C[b]); b++) {
                            if (a && h) {
                                g = 0;
                                while (m = e[g++])
                                    if (m(h, u, c)) {
                                        f.push(h);
                                        break
                                    }
                                w && (N = k, r = ++n)
                            }
                            o && ((h = !m && h) && v--, s && x.push(h))
                        }
                        if (v += b, o && b !== v) {
                            g = 0;
                            while (m = t[g++]) m(x, y, u, c);
                            if (s) {
                                if (v > 0)
                                    while (b--) x[b] || y[b] || (y[b] = L.call(f));
                                y = mt(y)
                            }
                            H.apply(f, y), w && !s && y.length > 0 && v + t.length > 1 && st.uniqueSort(f)
                        }
                        return w && (N = k, l = T), x
                    };
                return o ? ot(s) : s
            }
            s = st.compile = function(e, t) {
                var n, r = [],
                    i = [],
                    o = S[e + " "];
                if (!o) {
                    t || (t = ft(e)), n = t.length;
                    while (n--) o = vt(t[n]), o[x] ? r.push(o) : i.push(o);
                    o = S(e, bt(i, r))
                }
                return o
            };

            function xt(e, t, n) {
                var r = 0,
                    i = t.length;
                for (; i > r; r++) st(e, t[r], n);
                return n
            }

            function wt(e, t, n, r) {
                var o, a, u, l, c, p = ft(e);
                if (!r && 1 === p.length) {
                    if (a = p[0] = p[0].slice(0), a.length > 2 && "ID" === (u = a[0]).type && 9 === t.nodeType && !d && i.relative[a[1].type]) {
                        if (t = i.find.ID(u.matches[0].replace(et, tt), t)[0], !t) return n;
                        e = e.slice(a.shift().value.length)
                    }
                    o = U.needsContext.test(e) ? 0 : a.length;
                    while (o--) {
                        if (u = a[o], i.relative[l = u.type]) break;
                        if ((c = i.find[l]) && (r = c(u.matches[0].replace(et, tt), V.test(a[0].type) && t.parentNode || t))) {
                            if (a.splice(o, 1), e = r.length && dt(a), !e) return H.apply(n, q.call(r, 0)), n;
                            break
                        }
                    }
                }
                return s(e, p)(r, t, d, n, V.test(e)), n
            }
            i.pseudos.nth = i.pseudos.eq;

            function Tt() {}
            i.filters = Tt.prototype = i.pseudos, i.setFilters = new Tt, c(), st.attr = b.attr, b.find = st, b.expr = st.selectors, b.expr[":"] = b.expr.pseudos, b.unique = st.uniqueSort, b.text = st.getText, b.isXMLDoc = st.isXML, b.contains = st.contains
        }(e);
    var at = /Until$/,
        st = /^(?:parents|prev(?:Until|All))/,
        ut = /^.[^:#\[\.,]*$/,
        lt = b.expr.match.needsContext,
        ct = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
    b.fn.extend({
        find: function(e) {
            var t, n, r, i = this.length;
            if ("string" != typeof e) return r = this, this.pushStack(b(e).filter(function() {
                for (t = 0; i > t; t++)
                    if (b.contains(r[t], this)) return !0
            }));
            for (n = [], t = 0; i > t; t++) b.find(e, this[t], n);
            return n = this.pushStack(i > 1 ? b.unique(n) : n), n.selector = (this.selector ? this.selector + " " : "") + e, n
        },
        has: function(e) {
            var t, n = b(e, this),
                r = n.length;
            return this.filter(function() {
                for (t = 0; r > t; t++)
                    if (b.contains(this, n[t])) return !0
            })
        },
        not: function(e) {
            return this.pushStack(ft(this, e, !1))
        },
        filter: function(e) {
            return this.pushStack(ft(this, e, !0))
        },
        is: function(e) {
            return !!e && ("string" == typeof e ? lt.test(e) ? b(e, this.context).index(this[0]) >= 0 : b.filter(e, this).length > 0 : this.filter(e).length > 0)
        },
        closest: function(e, t) {
            var n, r = 0,
                i = this.length,
                o = [],
                a = lt.test(e) || "string" != typeof e ? b(e, t || this.context) : 0;
            for (; i > r; r++) {
                n = this[r];
                while (n && n.ownerDocument && n !== t && 11 !== n.nodeType) {
                    if (a ? a.index(n) > -1 : b.find.matchesSelector(n, e)) {
                        o.push(n);
                        break
                    }
                    n = n.parentNode
                }
            }
            return this.pushStack(o.length > 1 ? b.unique(o) : o)
        },
        index: function(e) {
            return e ? "string" == typeof e ? b.inArray(this[0], b(e)) : b.inArray(e.jquery ? e[0] : e, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(e, t) {
            var n = "string" == typeof e ? b(e, t) : b.makeArray(e && e.nodeType ? [e] : e),
                r = b.merge(this.get(), n);
            return this.pushStack(b.unique(r))
        },
        addBack: function(e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
        }
    }), b.fn.andSelf = b.fn.addBack;

    function pt(e, t) {
        do e = e[t]; while (e && 1 !== e.nodeType);
        return e
    }
    b.each({
        parent: function(e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null
        },
        parents: function(e) {
            return b.dir(e, "parentNode")
        },
        parentsUntil: function(e, t, n) {
            return b.dir(e, "parentNode", n)
        },
        next: function(e) {
            return pt(e, "nextSibling")
        },
        prev: function(e) {
            return pt(e, "previousSibling")
        },
        nextAll: function(e) {
            return b.dir(e, "nextSibling")
        },
        prevAll: function(e) {
            return b.dir(e, "previousSibling")
        },
        nextUntil: function(e, t, n) {
            return b.dir(e, "nextSibling", n)
        },
        prevUntil: function(e, t, n) {
            return b.dir(e, "previousSibling", n)
        },
        siblings: function(e) {
            return b.sibling((e.parentNode || {}).firstChild, e)
        },
        children: function(e) {
            return b.sibling(e.firstChild)
        },
        contents: function(e) {
            return b.nodeName(e, "iframe") ? e.contentDocument || e.contentWindow.document : b.merge([], e.childNodes)
        }
    }, function(e, t) {
        b.fn[e] = function(n, r) {
            var i = b.map(this, t, n);
            return at.test(e) || (r = n), r && "string" == typeof r && (i = b.filter(r, i)), i = this.length > 1 && !ct[e] ? b.unique(i) : i, this.length > 1 && st.test(e) && (i = i.reverse()), this.pushStack(i)
        }
    }), b.extend({
        filter: function(e, t, n) {
            return n && (e = ":not(" + e + ")"), 1 === t.length ? b.find.matchesSelector(t[0], e) ? [t[0]] : [] : b.find.matches(e, t)
        },
        dir: function(e, n, r) {
            var i = [],
                o = e[n];
            while (o && 9 !== o.nodeType && (r === t || 1 !== o.nodeType || !b(o).is(r))) 1 === o.nodeType && i.push(o), o = o[n];
            return i
        },
        sibling: function(e, t) {
            var n = [];
            for (; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
            return n
        }
    });

    function ft(e, t, n) {
        if (t = t || 0, b.isFunction(t)) return b.grep(e, function(e, r) {
            var i = !!t.call(e, r, e);
            return i === n
        });
        if (t.nodeType) return b.grep(e, function(e) {
            return e === t === n
        });
        if ("string" == typeof t) {
            var r = b.grep(e, function(e) {
                return 1 === e.nodeType
            });
            if (ut.test(t)) return b.filter(t, r, !n);
            t = b.filter(t, r)
        }
        return b.grep(e, function(e) {
            return b.inArray(e, t) >= 0 === n
        })
    }

    function dt(e) {
        var t = ht.split("|"),
            n = e.createDocumentFragment();
        if (n.createElement)
            while (t.length) n.createElement(t.pop());
        return n
    }
    var ht = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
        gt = / jQuery\d+="(?:null|\d+)"/g,
        mt = RegExp("<(?:" + ht + ")[\\s/>]", "i"),
        yt = /^\s+/,
        vt = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
        bt = /<([\w:]+)/,
        xt = /<tbody/i,
        wt = /<|&#?\w+;/,
        Tt = /<(?:script|style|link)/i,
        Nt = /^(?:checkbox|radio)$/i,
        Ct = /checked\s*(?:[^=]|=\s*.checked.)/i,
        kt = /^$|\/(?:java|ecma)script/i,
        Et = /^true\/(.*)/,
        St = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
        At = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            legend: [1, "<fieldset>", "</fieldset>"],
            area: [1, "<map>", "</map>"],
            param: [1, "<object>", "</object>"],
            thead: [1, "<table>", "</table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: b.support.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
        },
        jt = dt(o),
        Dt = jt.appendChild(o.createElement("div"));
    At.optgroup = At.option, At.tbody = At.tfoot = At.colgroup = At.caption = At.thead, At.th = At.td, b.fn.extend({
        text: function(e) {
            return b.access(this, function(e) {
                return e === t ? b.text(this) : this.empty().append((this[0] && this[0].ownerDocument || o).createTextNode(e))
            }, null, e, arguments.length)
        },
        wrapAll: function(e) {
            if (b.isFunction(e)) return this.each(function(t) {
                b(this).wrapAll(e.call(this, t))
            });
            if (this[0]) {
                var t = b(e, this[0].ownerDocument).eq(0).clone(!0);
                this[0].parentNode && t.insertBefore(this[0]), t.map(function() {
                    var e = this;
                    while (e.firstChild && 1 === e.firstChild.nodeType) e = e.firstChild;
                    return e
                }).append(this)
            }
            return this
        },
        wrapInner: function(e) {
            return b.isFunction(e) ? this.each(function(t) {
                b(this).wrapInner(e.call(this, t))
            }) : this.each(function() {
                var t = b(this),
                    n = t.contents();
                n.length ? n.wrapAll(e) : t.append(e)
            })
        },
        wrap: function(e) {
            var t = b.isFunction(e);
            return this.each(function(n) {
                b(this).wrapAll(t ? e.call(this, n) : e)
            })
        },
        unwrap: function() {
            return this.parent().each(function() {
                b.nodeName(this, "body") || b(this).replaceWith(this.childNodes)
            }).end()
        },
        append: function() {
            return this.domManip(arguments, !0, function(e) {
                (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) && this.appendChild(e)
            })
        },
        prepend: function() {
            return this.domManip(arguments, !0, function(e) {
                (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) && this.insertBefore(e, this.firstChild)
            })
        },
        before: function() {
            return this.domManip(arguments, !1, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this)
            })
        },
        after: function() {
            return this.domManip(arguments, !1, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
            })
        },
        remove: function(e, t) {
            var n, r = 0;
            for (; null != (n = this[r]); r++)(!e || b.filter(e, [n]).length > 0) && (t || 1 !== n.nodeType || b.cleanData(Ot(n)), n.parentNode && (t && b.contains(n.ownerDocument, n) && Mt(Ot(n, "script")), n.parentNode.removeChild(n)));
            return this
        },
        empty: function() {
            var e, t = 0;
            for (; null != (e = this[t]); t++) {
                1 === e.nodeType && b.cleanData(Ot(e, !1));
                while (e.firstChild) e.removeChild(e.firstChild);
                e.options && b.nodeName(e, "select") && (e.options.length = 0)
            }
            return this
        },
        clone: function(e, t) {
            return e = null == e ? !1 : e, t = null == t ? e : t, this.map(function() {
                return b.clone(this, e, t)
            })
        },
        html: function(e) {
            return b.access(this, function(e) {
                var n = this[0] || {},
                    r = 0,
                    i = this.length;
                if (e === t) return 1 === n.nodeType ? n.innerHTML.replace(gt, "") : t;
                if (!("string" != typeof e || Tt.test(e) || !b.support.htmlSerialize && mt.test(e) || !b.support.leadingWhitespace && yt.test(e) || At[(bt.exec(e) || ["", ""])[1].toLowerCase()])) {
                    e = e.replace(vt, "<$1></$2>");
                    try {
                        for (; i > r; r++) n = this[r] || {}, 1 === n.nodeType && (b.cleanData(Ot(n, !1)), n.innerHTML = e);
                        n = 0
                    } catch (o) {}
                }
                n && this.empty().append(e)
            }, null, e, arguments.length)
        },
        replaceWith: function(e) {
            var t = b.isFunction(e);
            return t || "string" == typeof e || (e = b(e).not(this).detach()), this.domManip([e], !0, function(e) {
                var t = this.nextSibling,
                    n = this.parentNode;
                n && (b(this).remove(), n.insertBefore(e, t))
            })
        },
        detach: function(e) {
            return this.remove(e, !0)
        },
        domManip: function(e, n, r) {
            e = f.apply([], e);
            var i, o, a, s, u, l, c = 0,
                p = this.length,
                d = this,
                h = p - 1,
                g = e[0],
                m = b.isFunction(g);
            if (m || !(1 >= p || "string" != typeof g || b.support.checkClone) && Ct.test(g)) return this.each(function(i) {
                var o = d.eq(i);
                m && (e[0] = g.call(this, i, n ? o.html() : t)), o.domManip(e, n, r)
            });
            if (p && (l = b.buildFragment(e, this[0].ownerDocument, !1, this), i = l.firstChild, 1 === l.childNodes.length && (l = i), i)) {
                for (n = n && b.nodeName(i, "tr"), s = b.map(Ot(l, "script"), Ht), a = s.length; p > c; c++) o = l, c !== h && (o = b.clone(o, !0, !0), a && b.merge(s, Ot(o, "script"))), r.call(n && b.nodeName(this[c], "table") ? Lt(this[c], "tbody") : this[c], o, c);
                if (a)
                    for (u = s[s.length - 1].ownerDocument, b.map(s, qt), c = 0; a > c; c++) o = s[c], kt.test(o.type || "") && !b._data(o, "globalEval") && b.contains(u, o) && (o.src ? b.ajax({
                        url: o.src,
                        type: "GET",
                        dataType: "script",
                        async: !1,
                        global: !1,
                        "throws": !0
                    }) : b.globalEval((o.text || o.textContent || o.innerHTML || "").replace(St, "")));
                l = i = null
            }
            return this
        }
    });

    function Lt(e, t) {
        return e.getElementsByTagName(t)[0] || e.appendChild(e.ownerDocument.createElement(t))
    }

    function Ht(e) {
        var t = e.getAttributeNode("type");
        return e.type = (t && t.specified) + "/" + e.type, e
    }

    function qt(e) {
        var t = Et.exec(e.type);
        return t ? e.type = t[1] : e.removeAttribute("type"), e
    }

    function Mt(e, t) {
        var n, r = 0;
        for (; null != (n = e[r]); r++) b._data(n, "globalEval", !t || b._data(t[r], "globalEval"))
    }

    function _t(e, t) {
        if (1 === t.nodeType && b.hasData(e)) {
            var n, r, i, o = b._data(e),
                a = b._data(t, o),
                s = o.events;
            if (s) {
                delete a.handle, a.events = {};
                for (n in s)
                    for (r = 0, i = s[n].length; i > r; r++) b.event.add(t, n, s[n][r])
            }
            a.data && (a.data = b.extend({}, a.data))
        }
    }

    function Ft(e, t) {
        var n, r, i;
        if (1 === t.nodeType) {
            if (n = t.nodeName.toLowerCase(), !b.support.noCloneEvent && t[b.expando]) {
                i = b._data(t);
                for (r in i.events) b.removeEvent(t, r, i.handle);
                t.removeAttribute(b.expando)
            }
            "script" === n && t.text !== e.text ? (Ht(t).text = e.text, qt(t)) : "object" === n ? (t.parentNode && (t.outerHTML = e.outerHTML), b.support.html5Clone && e.innerHTML && !b.trim(t.innerHTML) && (t.innerHTML = e.innerHTML)) : "input" === n && Nt.test(e.type) ? (t.defaultChecked = t.checked = e.checked, t.value !== e.value && (t.value = e.value)) : "option" === n ? t.defaultSelected = t.selected = e.defaultSelected : ("input" === n || "textarea" === n) && (t.defaultValue = e.defaultValue)
        }
    }
    b.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(e, t) {
        b.fn[e] = function(e) {
            var n, r = 0,
                i = [],
                o = b(e),
                a = o.length - 1;
            for (; a >= r; r++) n = r === a ? this : this.clone(!0), b(o[r])[t](n), d.apply(i, n.get());
            return this.pushStack(i)
        }
    });

    function Ot(e, n) {
        var r, o, a = 0,
            s = typeof e.getElementsByTagName !== i ? e.getElementsByTagName(n || "*") : typeof e.querySelectorAll !== i ? e.querySelectorAll(n || "*") : t;
        if (!s)
            for (s = [], r = e.childNodes || e; null != (o = r[a]); a++) !n || b.nodeName(o, n) ? s.push(o) : b.merge(s, Ot(o, n));
        return n === t || n && b.nodeName(e, n) ? b.merge([e], s) : s
    }

    function Bt(e) {
        Nt.test(e.type) && (e.defaultChecked = e.checked)
    }
    b.extend({
        clone: function(e, t, n) {
            var r, i, o, a, s, u = b.contains(e.ownerDocument, e);
            if (b.support.html5Clone || b.isXMLDoc(e) || !mt.test("<" + e.nodeName + ">") ? o = e.cloneNode(!0) : (Dt.innerHTML = e.outerHTML, Dt.removeChild(o = Dt.firstChild)), !(b.support.noCloneEvent && b.support.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || b.isXMLDoc(e)))
                for (r = Ot(o), s = Ot(e), a = 0; null != (i = s[a]); ++a) r[a] && Ft(i, r[a]);
            if (t)
                if (n)
                    for (s = s || Ot(e), r = r || Ot(o), a = 0; null != (i = s[a]); a++) _t(i, r[a]);
                else _t(e, o);
            return r = Ot(o, "script"), r.length > 0 && Mt(r, !u && Ot(e, "script")), r = s = i = null, o
        },
        buildFragment: function(e, t, n, r) {
            var i, o, a, s, u, l, c, p = e.length,
                f = dt(t),
                d = [],
                h = 0;
            for (; p > h; h++)
                if (o = e[h], o || 0 === o)
                    if ("object" === b.type(o)) b.merge(d, o.nodeType ? [o] : o);
                    else if (wt.test(o)) {
                s = s || f.appendChild(t.createElement("div")), u = (bt.exec(o) || ["", ""])[1].toLowerCase(), c = At[u] || At._default, s.innerHTML = c[1] + o.replace(vt, "<$1></$2>") + c[2], i = c[0];
                while (i--) s = s.lastChild;
                if (!b.support.leadingWhitespace && yt.test(o) && d.push(t.createTextNode(yt.exec(o)[0])), !b.support.tbody) {
                    o = "table" !== u || xt.test(o) ? "<table>" !== c[1] || xt.test(o) ? 0 : s : s.firstChild, i = o && o.childNodes.length;
                    while (i--) b.nodeName(l = o.childNodes[i], "tbody") && !l.childNodes.length && o.removeChild(l)
                }
                b.merge(d, s.childNodes), s.textContent = "";
                while (s.firstChild) s.removeChild(s.firstChild);
                s = f.lastChild
            } else d.push(t.createTextNode(o));
            s && f.removeChild(s), b.support.appendChecked || b.grep(Ot(d, "input"), Bt), h = 0;
            while (o = d[h++])
                if ((!r || -1 === b.inArray(o, r)) && (a = b.contains(o.ownerDocument, o), s = Ot(f.appendChild(o), "script"), a && Mt(s), n)) {
                    i = 0;
                    while (o = s[i++]) kt.test(o.type || "") && n.push(o)
                }
            return s = null, f
        },
        cleanData: function(e, t) {
            var n, r, o, a, s = 0,
                u = b.expando,
                l = b.cache,
                p = b.support.deleteExpando,
                f = b.event.special;
            for (; null != (n = e[s]); s++)
                if ((t || b.acceptData(n)) && (o = n[u], a = o && l[o])) {
                    if (a.events)
                        for (r in a.events) f[r] ? b.event.remove(n, r) : b.removeEvent(n, r, a.handle);
                    l[o] && (delete l[o], p ? delete n[u] : typeof n.removeAttribute !== i ? n.removeAttribute(u) : n[u] = null, c.push(o))
                }
        }
    });
    var Pt, Rt, Wt, $t = /alpha\([^)]*\)/i,
        It = /opacity\s*=\s*([^)]*)/,
        zt = /^(top|right|bottom|left)$/,
        Xt = /^(none|table(?!-c[ea]).+)/,
        Ut = /^margin/,
        Vt = RegExp("^(" + x + ")(.*)$", "i"),
        Yt = RegExp("^(" + x + ")(?!px)[a-z%]+$", "i"),
        Jt = RegExp("^([+-])=(" + x + ")", "i"),
        Gt = {
            BODY: "block"
        },
        Qt = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        Kt = {
            letterSpacing: 0,
            fontWeight: 400
        },
        Zt = ["Top", "Right", "Bottom", "Left"],
        en = ["Webkit", "O", "Moz", "ms"];

    function tn(e, t) {
        if (t in e) return t;
        var n = t.charAt(0).toUpperCase() + t.slice(1),
            r = t,
            i = en.length;
        while (i--)
            if (t = en[i] + n, t in e) return t;
        return r
    }

    function nn(e, t) {
        return e = t || e, "none" === b.css(e, "display") || !b.contains(e.ownerDocument, e)
    }

    function rn(e, t) {
        var n, r, i, o = [],
            a = 0,
            s = e.length;
        for (; s > a; a++) r = e[a], r.style && (o[a] = b._data(r, "olddisplay"), n = r.style.display, t ? (o[a] || "none" !== n || (r.style.display = ""), "" === r.style.display && nn(r) && (o[a] = b._data(r, "olddisplay", un(r.nodeName)))) : o[a] || (i = nn(r), (n && "none" !== n || !i) && b._data(r, "olddisplay", i ? n : b.css(r, "display"))));
        for (a = 0; s > a; a++) r = e[a], r.style && (t && "none" !== r.style.display && "" !== r.style.display || (r.style.display = t ? o[a] || "" : "none"));
        return e
    }
    b.fn.extend({
        css: function(e, n) {
            return b.access(this, function(e, n, r) {
                var i, o, a = {},
                    s = 0;
                if (b.isArray(n)) {
                    for (o = Rt(e), i = n.length; i > s; s++) a[n[s]] = b.css(e, n[s], !1, o);
                    return a
                }
                return r !== t ? b.style(e, n, r) : b.css(e, n)
            }, e, n, arguments.length > 1)
        },
        show: function() {
            return rn(this, !0)
        },
        hide: function() {
            return rn(this)
        },
        toggle: function(e) {
            var t = "boolean" == typeof e;
            return this.each(function() {
                (t ? e : nn(this)) ? b(this).show(): b(this).hide()
            })
        }
    }), b.extend({
        cssHooks: {
            opacity: {
                get: function(e, t) {
                    if (t) {
                        var n = Wt(e, "opacity");
                        return "" === n ? "1" : n
                    }
                }
            }
        },
        cssNumber: {
            columnCount: !0,
            fillOpacity: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            "float": b.support.cssFloat ? "cssFloat" : "styleFloat"
        },
        style: function(e, n, r, i) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var o, a, s, u = b.camelCase(n),
                    l = e.style;
                if (n = b.cssProps[u] || (b.cssProps[u] = tn(l, u)), s = b.cssHooks[n] || b.cssHooks[u], r === t) return s && "get" in s && (o = s.get(e, !1, i)) !== t ? o : l[n];
                if (a = typeof r, "string" === a && (o = Jt.exec(r)) && (r = (o[1] + 1) * o[2] + parseFloat(b.css(e, n)), a = "number"), !(null == r || "number" === a && isNaN(r) || ("number" !== a || b.cssNumber[u] || (r += "px"), b.support.clearCloneStyle || "" !== r || 0 !== n.indexOf("background") || (l[n] = "inherit"), s && "set" in s && (r = s.set(e, r, i)) === t))) try {
                    l[n] = r
                } catch (c) {}
            }
        },
        css: function(e, n, r, i) {
            var o, a, s, u = b.camelCase(n);
            return n = b.cssProps[u] || (b.cssProps[u] = tn(e.style, u)), s = b.cssHooks[n] || b.cssHooks[u], s && "get" in s && (a = s.get(e, !0, r)), a === t && (a = Wt(e, n, i)), "normal" === a && n in Kt && (a = Kt[n]), "" === r || r ? (o = parseFloat(a), r === !0 || b.isNumeric(o) ? o || 0 : a) : a
        },
        swap: function(e, t, n, r) {
            var i, o, a = {};
            for (o in t) a[o] = e.style[o], e.style[o] = t[o];
            i = n.apply(e, r || []);
            for (o in t) e.style[o] = a[o];
            return i
        }
    }), e.getComputedStyle ? (Rt = function(t) {
        return e.getComputedStyle(t, null)
    }, Wt = function(e, n, r) {
        var i, o, a, s = r || Rt(e),
            u = s ? s.getPropertyValue(n) || s[n] : t,
            l = e.style;
        return s && ("" !== u || b.contains(e.ownerDocument, e) || (u = b.style(e, n)), Yt.test(u) && Ut.test(n) && (i = l.width, o = l.minWidth, a = l.maxWidth, l.minWidth = l.maxWidth = l.width = u, u = s.width, l.width = i, l.minWidth = o, l.maxWidth = a)), u
    }) : o.documentElement.currentStyle && (Rt = function(e) {
        return e.currentStyle
    }, Wt = function(e, n, r) {
        var i, o, a, s = r || Rt(e),
            u = s ? s[n] : t,
            l = e.style;
        return null == u && l && l[n] && (u = l[n]), Yt.test(u) && !zt.test(n) && (i = l.left, o = e.runtimeStyle, a = o && o.left, a && (o.left = e.currentStyle.left), l.left = "fontSize" === n ? "1em" : u, u = l.pixelLeft + "px", l.left = i, a && (o.left = a)), "" === u ? "auto" : u
    });

    function on(e, t, n) {
        var r = Vt.exec(t);
        return r ? Math.max(0, r[1] - (n || 0)) + (r[2] || "px") : t
    }

    function an(e, t, n, r, i) {
        var o = n === (r ? "border" : "content") ? 4 : "width" === t ? 1 : 0,
            a = 0;
        for (; 4 > o; o += 2) "margin" === n && (a += b.css(e, n + Zt[o], !0, i)), r ? ("content" === n && (a -= b.css(e, "padding" + Zt[o], !0, i)), "margin" !== n && (a -= b.css(e, "border" + Zt[o] + "Width", !0, i))) : (a += b.css(e, "padding" + Zt[o], !0, i), "padding" !== n && (a += b.css(e, "border" + Zt[o] + "Width", !0, i)));
        return a
    }

    function sn(e, t, n) {
        var r = !0,
            i = "width" === t ? e.offsetWidth : e.offsetHeight,
            o = Rt(e),
            a = b.support.boxSizing && "border-box" === b.css(e, "boxSizing", !1, o);
        if (0 >= i || null == i) {
            if (i = Wt(e, t, o), (0 > i || null == i) && (i = e.style[t]), Yt.test(i)) return i;
            r = a && (b.support.boxSizingReliable || i === e.style[t]), i = parseFloat(i) || 0
        }
        return i + an(e, t, n || (a ? "border" : "content"), r, o) + "px"
    }

    function un(e) {
        var t = o,
            n = Gt[e];
        return n || (n = ln(e, t), "none" !== n && n || (Pt = (Pt || b("<iframe frameborder='0' width='0' height='0'/>").css("cssText", "display:block !important")).appendTo(t.documentElement), t = (Pt[0].contentWindow || Pt[0].contentDocument).document, t.write("<!doctype html><html><body>"), t.close(), n = ln(e, t), Pt.detach()), Gt[e] = n), n
    }

    function ln(e, t) {
        var n = b(t.createElement(e)).appendTo(t.body),
            r = b.css(n[0], "display");
        return n.remove(), r
    }
    b.each(["height", "width"], function(e, n) {
        b.cssHooks[n] = {
            get: function(e, r, i) {
                return r ? 0 === e.offsetWidth && Xt.test(b.css(e, "display")) ? b.swap(e, Qt, function() {
                    return sn(e, n, i)
                }) : sn(e, n, i) : t
            },
            set: function(e, t, r) {
                var i = r && Rt(e);
                return on(e, t, r ? an(e, n, r, b.support.boxSizing && "border-box" === b.css(e, "boxSizing", !1, i), i) : 0)
            }
        }
    }), b.support.opacity || (b.cssHooks.opacity = {
        get: function(e, t) {
            return It.test((t && e.currentStyle ? e.currentStyle.filter : e.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : t ? "1" : ""
        },
        set: function(e, t) {
            var n = e.style,
                r = e.currentStyle,
                i = b.isNumeric(t) ? "alpha(opacity=" + 100 * t + ")" : "",
                o = r && r.filter || n.filter || "";
            n.zoom = 1, (t >= 1 || "" === t) && "" === b.trim(o.replace($t, "")) && n.removeAttribute && (n.removeAttribute("filter"), "" === t || r && !r.filter) || (n.filter = $t.test(o) ? o.replace($t, i) : o + " " + i)
        }
    }), b(function() {
        b.support.reliableMarginRight || (b.cssHooks.marginRight = {
            get: function(e, n) {
                return n ? b.swap(e, {
                    display: "inline-block"
                }, Wt, [e, "marginRight"]) : t
            }
        }), !b.support.pixelPosition && b.fn.position && b.each(["top", "left"], function(e, n) {
            b.cssHooks[n] = {
                get: function(e, r) {
                    return r ? (r = Wt(e, n), Yt.test(r) ? b(e).position()[n] + "px" : r) : t
                }
            }
        })
    }), b.expr && b.expr.filters && (b.expr.filters.hidden = function(e) {
        return 0 >= e.offsetWidth && 0 >= e.offsetHeight || !b.support.reliableHiddenOffsets && "none" === (e.style && e.style.display || b.css(e, "display"))
    }, b.expr.filters.visible = function(e) {
        return !b.expr.filters.hidden(e)
    }), b.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(e, t) {
        b.cssHooks[e + t] = {
            expand: function(n) {
                var r = 0,
                    i = {},
                    o = "string" == typeof n ? n.split(" ") : [n];
                for (; 4 > r; r++) i[e + Zt[r] + t] = o[r] || o[r - 2] || o[0];
                return i
            }
        }, Ut.test(e) || (b.cssHooks[e + t].set = on)
    });
    var cn = /%20/g,
        pn = /\[\]$/,
        fn = /\r?\n/g,
        dn = /^(?:submit|button|image|reset|file)$/i,
        hn = /^(?:input|select|textarea|keygen)/i;
    b.fn.extend({
        serialize: function() {
            return b.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var e = b.prop(this, "elements");
                return e ? b.makeArray(e) : this
            }).filter(function() {
                var e = this.type;
                return this.name && !b(this).is(":disabled") && hn.test(this.nodeName) && !dn.test(e) && (this.checked || !Nt.test(e))
            }).map(function(e, t) {
                var n = b(this).val();
                return null == n ? null : b.isArray(n) ? b.map(n, function(e) {
                    return {
                        name: t.name,
                        value: e.replace(fn, "\r\n")
                    }
                }) : {
                    name: t.name,
                    value: n.replace(fn, "\r\n")
                }
            }).get()
        }
    }), b.param = function(e, n) {
        var r, i = [],
            o = function(e, t) {
                t = b.isFunction(t) ? t() : null == t ? "" : t, i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t)
            };
        if (n === t && (n = b.ajaxSettings && b.ajaxSettings.traditional), b.isArray(e) || e.jquery && !b.isPlainObject(e)) b.each(e, function() {
            o(this.name, this.value)
        });
        else
            for (r in e) gn(r, e[r], n, o);
        return i.join("&").replace(cn, "+")
    };

    function gn(e, t, n, r) {
        var i;
        if (b.isArray(t)) b.each(t, function(t, i) {
            n || pn.test(e) ? r(e, i) : gn(e + "[" + ("object" == typeof i ? t : "") + "]", i, n, r)
        });
        else if (n || "object" !== b.type(t)) r(e, t);
        else
            for (i in t) gn(e + "[" + i + "]", t[i], n, r)
    }
    b.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(e, t) {
        b.fn[t] = function(e, n) {
            return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
        }
    }), b.fn.hover = function(e, t) {
        return this.mouseenter(e).mouseleave(t || e)
    };
    var mn, yn, vn = b.now(),
        bn = /\?/,
        xn = /#.*$/,
        wn = /([?&])_=[^&]*/,
        Tn = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
        Nn = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
        Cn = /^(?:GET|HEAD)$/,
        kn = /^\/\//,
        En = /^([\w.+-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/,
        Sn = b.fn.load,
        An = {},
        jn = {},
        Dn = "*/".concat("*");
    try {
        yn = a.href
    } catch (Ln) {
        yn = o.createElement("a"), yn.href = "", yn = yn.href
    }
    mn = En.exec(yn.toLowerCase()) || [];

    function Hn(e) {
        return function(t, n) {
            "string" != typeof t && (n = t, t = "*");
            var r, i = 0,
                o = t.toLowerCase().match(w) || [];
            if (b.isFunction(n))
                while (r = o[i++]) "+" === r[0] ? (r = r.slice(1) || "*", (e[r] = e[r] || []).unshift(n)) : (e[r] = e[r] || []).push(n)
        }
    }

    function qn(e, n, r, i) {
        var o = {},
            a = e === jn;

        function s(u) {
            var l;
            return o[u] = !0, b.each(e[u] || [], function(e, u) {
                var c = u(n, r, i);
                return "string" != typeof c || a || o[c] ? a ? !(l = c) : t : (n.dataTypes.unshift(c), s(c), !1)
            }), l
        }
        return s(n.dataTypes[0]) || !o["*"] && s("*")
    }

    function Mn(e, n) {
        var r, i, o = b.ajaxSettings.flatOptions || {};
        for (i in n) n[i] !== t && ((o[i] ? e : r || (r = {}))[i] = n[i]);
        return r && b.extend(!0, e, r), e
    }
    b.fn.load = function(e, n, r) {
        if ("string" != typeof e && Sn) return Sn.apply(this, arguments);
        var i, o, a, s = this,
            u = e.indexOf(" ");
        return u >= 0 && (i = e.slice(u, e.length), e = e.slice(0, u)), b.isFunction(n) ? (r = n, n = t) : n && "object" == typeof n && (a = "POST"), s.length > 0 && b.ajax({
            url: e,
            type: a,
            dataType: "html",
            data: n
        }).done(function(e) {
            o = arguments, s.html(i ? b("<div>").append(b.parseHTML(e)).find(i) : e)
        }).complete(r && function(e, t) {
            s.each(r, o || [e.responseText, t, e])
        }), this
    }, b.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) {
        b.fn[t] = function(e) {
            return this.on(t, e)
        }
    }), b.each(["get", "post"], function(e, n) {
        b[n] = function(e, r, i, o) {
            return b.isFunction(r) && (o = o || i, i = r, r = t), b.ajax({
                url: e,
                type: n,
                dataType: o,
                data: r,
                success: i
            })
        }
    }), b.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: yn,
            type: "GET",
            isLocal: Nn.test(mn[1]),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Dn,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText"
            },
            converters: {
                "* text": e.String,
                "text html": !0,
                "text json": b.parseJSON,
                "text xml": b.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(e, t) {
            return t ? Mn(Mn(e, b.ajaxSettings), t) : Mn(b.ajaxSettings, e)
        },
        ajaxPrefilter: Hn(An),
        ajaxTransport: Hn(jn),
        ajax: function(e, n) {
            "object" == typeof e && (n = e, e = t), n = n || {};
            var r, i, o, a, s, u, l, c, p = b.ajaxSetup({}, n),
                f = p.context || p,
                d = p.context && (f.nodeType || f.jquery) ? b(f) : b.event,
                h = b.Deferred(),
                g = b.Callbacks("once memory"),
                m = p.statusCode || {},
                y = {},
                v = {},
                x = 0,
                T = "canceled",
                N = {
                    readyState: 0,
                    getResponseHeader: function(e) {
                        var t;
                        if (2 === x) {
                            if (!c) {
                                c = {};
                                while (t = Tn.exec(a)) c[t[1].toLowerCase()] = t[2]
                            }
                            t = c[e.toLowerCase()]
                        }
                        return null == t ? null : t
                    },
                    getAllResponseHeaders: function() {
                        return 2 === x ? a : null
                    },
                    setRequestHeader: function(e, t) {
                        var n = e.toLowerCase();
                        return x || (e = v[n] = v[n] || e, y[e] = t), this
                    },
                    overrideMimeType: function(e) {
                        return x || (p.mimeType = e), this
                    },
                    statusCode: function(e) {
                        var t;
                        if (e)
                            if (2 > x)
                                for (t in e) m[t] = [m[t], e[t]];
                            else N.always(e[N.status]);
                        return this
                    },
                    abort: function(e) {
                        var t = e || T;
                        return l && l.abort(t), k(0, t), this
                    }
                };
            if (h.promise(N).complete = g.add, N.success = N.done, N.error = N.fail, p.url = ((e || p.url || yn) + "").replace(xn, "").replace(kn, mn[1] + "//"), p.type = n.method || n.type || p.method || p.type, p.dataTypes = b.trim(p.dataType || "*").toLowerCase().match(w) || [""], null == p.crossDomain && (r = En.exec(p.url.toLowerCase()), p.crossDomain = !(!r || r[1] === mn[1] && r[2] === mn[2] && (r[3] || ("http:" === r[1] ? 80 : 443)) == (mn[3] || ("http:" === mn[1] ? 80 : 443)))), p.data && p.processData && "string" != typeof p.data && (p.data = b.param(p.data, p.traditional)), qn(An, p, n, N), 2 === x) return N;
            u = p.global, u && 0 === b.active++ && b.event.trigger("ajaxStart"), p.type = p.type.toUpperCase(), p.hasContent = !Cn.test(p.type), o = p.url, p.hasContent || (p.data && (o = p.url += (bn.test(o) ? "&" : "?") + p.data, delete p.data), p.cache === !1 && (p.url = wn.test(o) ? o.replace(wn, "$1_=" + vn++) : o + (bn.test(o) ? "&" : "?") + "_=" + vn++)), p.ifModified && (b.lastModified[o] && N.setRequestHeader("If-Modified-Since", b.lastModified[o]), b.etag[o] && N.setRequestHeader("If-None-Match", b.etag[o])), (p.data && p.hasContent && p.contentType !== !1 || n.contentType) && N.setRequestHeader("Content-Type", p.contentType), N.setRequestHeader("Accept", p.dataTypes[0] && p.accepts[p.dataTypes[0]] ? p.accepts[p.dataTypes[0]] + ("*" !== p.dataTypes[0] ? ", " + Dn + "; q=0.01" : "") : p.accepts["*"]);
            for (i in p.headers) N.setRequestHeader(i, p.headers[i]);
            if (p.beforeSend && (p.beforeSend.call(f, N, p) === !1 || 2 === x)) return N.abort();
            T = "abort";
            for (i in {
                    success: 1,
                    error: 1,
                    complete: 1
                }) N[i](p[i]);
            if (l = qn(jn, p, n, N)) {
                N.readyState = 1, u && d.trigger("ajaxSend", [N, p]), p.async && p.timeout > 0 && (s = setTimeout(function() {
                    N.abort("timeout")
                }, p.timeout));
                try {
                    x = 1, l.send(y, k)
                } catch (C) {
                    if (!(2 > x)) throw C;
                    k(-1, C)
                }
            } else k(-1, "No Transport");

            function k(e, n, r, i) {
                var c, y, v, w, T, C = n;
                2 !== x && (x = 2, s && clearTimeout(s), l = t, a = i || "", N.readyState = e > 0 ? 4 : 0, r && (w = _n(p, N, r)), e >= 200 && 300 > e || 304 === e ? (p.ifModified && (T = N.getResponseHeader("Last-Modified"), T && (b.lastModified[o] = T), T = N.getResponseHeader("etag"), T && (b.etag[o] = T)), 204 === e ? (c = !0, C = "nocontent") : 304 === e ? (c = !0, C = "notmodified") : (c = Fn(p, w), C = c.state, y = c.data, v = c.error, c = !v)) : (v = C, (e || !C) && (C = "error", 0 > e && (e = 0))), N.status = e, N.statusText = (n || C) + "", c ? h.resolveWith(f, [y, C, N]) : h.rejectWith(f, [N, C, v]), N.statusCode(m), m = t, u && d.trigger(c ? "ajaxSuccess" : "ajaxError", [N, p, c ? y : v]), g.fireWith(f, [N, C]), u && (d.trigger("ajaxComplete", [N, p]), --b.active || b.event.trigger("ajaxStop")))
            }
            return N
        },
        getScript: function(e, n) {
            return b.get(e, t, n, "script")
        },
        getJSON: function(e, t, n) {
            return b.get(e, t, n, "json")
        }
    });

    function _n(e, n, r) {
        var i, o, a, s, u = e.contents,
            l = e.dataTypes,
            c = e.responseFields;
        for (s in c) s in r && (n[c[s]] = r[s]);
        while ("*" === l[0]) l.shift(), o === t && (o = e.mimeType || n.getResponseHeader("Content-Type"));
        if (o)
            for (s in u)
                if (u[s] && u[s].test(o)) {
                    l.unshift(s);
                    break
                }
        if (l[0] in r) a = l[0];
        else {
            for (s in r) {
                if (!l[0] || e.converters[s + " " + l[0]]) {
                    a = s;
                    break
                }
                i || (i = s)
            }
            a = a || i
        }
        return a ? (a !== l[0] && l.unshift(a), r[a]) : t
    }

    function Fn(e, t) {
        var n, r, i, o, a = {},
            s = 0,
            u = e.dataTypes.slice(),
            l = u[0];
        if (e.dataFilter && (t = e.dataFilter(t, e.dataType)), u[1])
            for (i in e.converters) a[i.toLowerCase()] = e.converters[i];
        for (; r = u[++s];)
            if ("*" !== r) {
                if ("*" !== l && l !== r) {
                    if (i = a[l + " " + r] || a["* " + r], !i)
                        for (n in a)
                            if (o = n.split(" "), o[1] === r && (i = a[l + " " + o[0]] || a["* " + o[0]])) {
                                i === !0 ? i = a[n] : a[n] !== !0 && (r = o[0], u.splice(s--, 0, r));
                                break
                            }
                    if (i !== !0)
                        if (i && e["throws"]) t = i(t);
                        else try {
                            t = i(t)
                        } catch (c) {
                            return {
                                state: "parsererror",
                                error: i ? c : "No conversion from " + l + " to " + r
                            }
                        }
                }
                l = r
            }
        return {
            state: "success",
            data: t
        }
    }
    b.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /(?:java|ecma)script/
        },
        converters: {
            "text script": function(e) {
                return b.globalEval(e), e
            }
        }
    }), b.ajaxPrefilter("script", function(e) {
        e.cache === t && (e.cache = !1), e.crossDomain && (e.type = "GET", e.global = !1)
    }), b.ajaxTransport("script", function(e) {
        if (e.crossDomain) {
            var n, r = o.head || b("head")[0] || o.documentElement;
            return {
                send: function(t, i) {
                    n = o.createElement("script"), n.async = !0, e.scriptCharset && (n.charset = e.scriptCharset), n.src = e.url, n.onload = n.onreadystatechange = function(e, t) {
                        (t || !n.readyState || /loaded|complete/.test(n.readyState)) && (n.onload = n.onreadystatechange = null, n.parentNode && n.parentNode.removeChild(n), n = null, t || i(200, "success"))
                    }, r.insertBefore(n, r.firstChild)
                },
                abort: function() {
                    n && n.onload(t, !0)
                }
            }
        }
    });
    var On = [],
        Bn = /(=)\?(?=&|$)|\?\?/;
    b.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var e = On.pop() || b.expando + "_" + vn++;
            return this[e] = !0, e
        }
    }), b.ajaxPrefilter("json jsonp", function(n, r, i) {
        var o, a, s, u = n.jsonp !== !1 && (Bn.test(n.url) ? "url" : "string" == typeof n.data && !(n.contentType || "").indexOf("application/x-www-form-urlencoded") && Bn.test(n.data) && "data");
        return u || "jsonp" === n.dataTypes[0] ? (o = n.jsonpCallback = b.isFunction(n.jsonpCallback) ? n.jsonpCallback() : n.jsonpCallback, u ? n[u] = n[u].replace(Bn, "$1" + o) : n.jsonp !== !1 && (n.url += (bn.test(n.url) ? "&" : "?") + n.jsonp + "=" + o), n.converters["script json"] = function() {
            return s || b.error(o + " was not called"), s[0]
        }, n.dataTypes[0] = "json", a = e[o], e[o] = function() {
            s = arguments
        }, i.always(function() {
            e[o] = a, n[o] && (n.jsonpCallback = r.jsonpCallback, On.push(o)), s && b.isFunction(a) && a(s[0]), s = a = t
        }), "script") : t
    });
    var Pn, Rn, Wn = 0,
        $n = e.ActiveXObject && function() {
            var e;
            for (e in Pn) Pn[e](t, !0)
        };

    function In() {
        try {
            return new e.XMLHttpRequest
        } catch (t) {}
    }

    function zn() {
        try {
            return new e.ActiveXObject("Microsoft.XMLHTTP")
        } catch (t) {}
    }
    b.ajaxSettings.xhr = e.ActiveXObject ? function() {
        return !this.isLocal && In() || zn()
    } : In, Rn = b.ajaxSettings.xhr(), b.support.cors = !!Rn && "withCredentials" in Rn, Rn = b.support.ajax = !!Rn, Rn && b.ajaxTransport(function(n) {
        if (!n.crossDomain || b.support.cors) {
            var r;
            return {
                send: function(i, o) {
                    var a, s, u = n.xhr();
                    if (n.username ? u.open(n.type, n.url, n.async, n.username, n.password) : u.open(n.type, n.url, n.async), n.xhrFields)
                        for (s in n.xhrFields) u[s] = n.xhrFields[s];
                    n.mimeType && u.overrideMimeType && u.overrideMimeType(n.mimeType), n.crossDomain || i["X-Requested-With"] || (i["X-Requested-With"] = "XMLHttpRequest");
                    try {
                        for (s in i) u.setRequestHeader(s, i[s])
                    } catch (l) {}
                    u.send(n.hasContent && n.data || null), r = function(e, i) {
                        var s, l, c, p;
                        try {
                            if (r && (i || 4 === u.readyState))
                                if (r = t, a && (u.onreadystatechange = b.noop, $n && delete Pn[a]), i) 4 !== u.readyState && u.abort();
                                else {
                                    p = {}, s = u.status, l = u.getAllResponseHeaders(), "string" == typeof u.responseText && (p.text = u.responseText);
                                    try {
                                        c = u.statusText
                                    } catch (f) {
                                        c = ""
                                    }
                                    s || !n.isLocal || n.crossDomain ? 1223 === s && (s = 204) : s = p.text ? 200 : 404
                                }
                        } catch (d) {
                            i || o(-1, d)
                        }
                        p && o(s, c, p, l)
                    }, n.async ? 4 === u.readyState ? setTimeout(r) : (a = ++Wn, $n && (Pn || (Pn = {}, b(e).unload($n)), Pn[a] = r), u.onreadystatechange = r) : r()
                },
                abort: function() {
                    r && r(t, !0)
                }
            }
        }
    });
    var Xn, Un, Vn = /^(?:toggle|show|hide)$/,
        Yn = RegExp("^(?:([+-])=|)(" + x + ")([a-z%]*)$", "i"),
        Jn = /queueHooks$/,
        Gn = [nr],
        Qn = {
            "*": [function(e, t) {
                var n, r, i = this.createTween(e, t),
                    o = Yn.exec(t),
                    a = i.cur(),
                    s = +a || 0,
                    u = 1,
                    l = 20;
                if (o) {
                    if (n = +o[2], r = o[3] || (b.cssNumber[e] ? "" : "px"), "px" !== r && s) {
                        s = b.css(i.elem, e, !0) || n || 1;
                        do u = u || ".5", s /= u, b.style(i.elem, e, s + r); while (u !== (u = i.cur() / a) && 1 !== u && --l)
                    }
                    i.unit = r, i.start = s, i.end = o[1] ? s + (o[1] + 1) * n : n
                }
                return i
            }]
        };

    function Kn() {
        return setTimeout(function() {
            Xn = t
        }), Xn = b.now()
    }

    function Zn(e, t) {
        b.each(t, function(t, n) {
            var r = (Qn[t] || []).concat(Qn["*"]),
                i = 0,
                o = r.length;
            for (; o > i; i++)
                if (r[i].call(e, t, n)) return
        })
    }

    function er(e, t, n) {
        var r, i, o = 0,
            a = Gn.length,
            s = b.Deferred().always(function() {
                delete u.elem
            }),
            u = function() {
                if (i) return !1;
                var t = Xn || Kn(),
                    n = Math.max(0, l.startTime + l.duration - t),
                    r = n / l.duration || 0,
                    o = 1 - r,
                    a = 0,
                    u = l.tweens.length;
                for (; u > a; a++) l.tweens[a].run(o);
                return s.notifyWith(e, [l, o, n]), 1 > o && u ? n : (s.resolveWith(e, [l]), !1)
            },
            l = s.promise({
                elem: e,
                props: b.extend({}, t),
                opts: b.extend(!0, {
                    specialEasing: {}
                }, n),
                originalProperties: t,
                originalOptions: n,
                startTime: Xn || Kn(),
                duration: n.duration,
                tweens: [],
                createTween: function(t, n) {
                    var r = b.Tween(e, l.opts, t, n, l.opts.specialEasing[t] || l.opts.easing);
                    return l.tweens.push(r), r
                },
                stop: function(t) {
                    var n = 0,
                        r = t ? l.tweens.length : 0;
                    if (i) return this;
                    for (i = !0; r > n; n++) l.tweens[n].run(1);
                    return t ? s.resolveWith(e, [l, t]) : s.rejectWith(e, [l, t]), this
                }
            }),
            c = l.props;
        for (tr(c, l.opts.specialEasing); a > o; o++)
            if (r = Gn[o].call(l, e, c, l.opts)) return r;
        return Zn(l, c), b.isFunction(l.opts.start) && l.opts.start.call(e, l), b.fx.timer(b.extend(u, {
            elem: e,
            anim: l,
            queue: l.opts.queue
        })), l.progress(l.opts.progress).done(l.opts.done, l.opts.complete).fail(l.opts.fail).always(l.opts.always)
    }

    function tr(e, t) {
        var n, r, i, o, a;
        for (i in e)
            if (r = b.camelCase(i), o = t[r], n = e[i], b.isArray(n) && (o = n[1], n = e[i] = n[0]), i !== r && (e[r] = n, delete e[i]), a = b.cssHooks[r], a && "expand" in a) {
                n = a.expand(n), delete e[r];
                for (i in n) i in e || (e[i] = n[i], t[i] = o)
            } else t[r] = o
    }
    b.Animation = b.extend(er, {
        tweener: function(e, t) {
            b.isFunction(e) ? (t = e, e = ["*"]) : e = e.split(" ");
            var n, r = 0,
                i = e.length;
            for (; i > r; r++) n = e[r], Qn[n] = Qn[n] || [], Qn[n].unshift(t)
        },
        prefilter: function(e, t) {
            t ? Gn.unshift(e) : Gn.push(e)
        }
    });

    function nr(e, t, n) {
        var r, i, o, a, s, u, l, c, p, f = this,
            d = e.style,
            h = {},
            g = [],
            m = e.nodeType && nn(e);
        n.queue || (c = b._queueHooks(e, "fx"), null == c.unqueued && (c.unqueued = 0, p = c.empty.fire, c.empty.fire = function() {
            c.unqueued || p()
        }), c.unqueued++, f.always(function() {
            f.always(function() {
                c.unqueued--, b.queue(e, "fx").length || c.empty.fire()
            })
        })), 1 === e.nodeType && ("height" in t || "width" in t) && (n.overflow = [d.overflow, d.overflowX, d.overflowY], "inline" === b.css(e, "display") && "none" === b.css(e, "float") && (b.support.inlineBlockNeedsLayout && "inline" !== un(e.nodeName) ? d.zoom = 1 : d.display = "inline-block")), n.overflow && (d.overflow = "hidden", b.support.shrinkWrapBlocks || f.always(function() {
            d.overflow = n.overflow[0], d.overflowX = n.overflow[1], d.overflowY = n.overflow[2]
        }));
        for (i in t)
            if (a = t[i], Vn.exec(a)) {
                if (delete t[i], u = u || "toggle" === a, a === (m ? "hide" : "show")) continue;
                g.push(i)
            }
        if (o = g.length) {
            s = b._data(e, "fxshow") || b._data(e, "fxshow", {}), "hidden" in s && (m = s.hidden), u && (s.hidden = !m), m ? b(e).show() : f.done(function() {
                b(e).hide()
            }), f.done(function() {
                var t;
                b._removeData(e, "fxshow");
                for (t in h) b.style(e, t, h[t])
            });
            for (i = 0; o > i; i++) r = g[i], l = f.createTween(r, m ? s[r] : 0), h[r] = s[r] || b.style(e, r), r in s || (s[r] = l.start, m && (l.end = l.start, l.start = "width" === r || "height" === r ? 1 : 0))
        }
    }

    function rr(e, t, n, r, i) {
        return new rr.prototype.init(e, t, n, r, i)
    }
    b.Tween = rr, rr.prototype = {
        constructor: rr,
        init: function(e, t, n, r, i, o) {
            this.elem = e, this.prop = n, this.easing = i || "swing", this.options = t, this.start = this.now = this.cur(), this.end = r, this.unit = o || (b.cssNumber[n] ? "" : "px")
        },
        cur: function() {
            var e = rr.propHooks[this.prop];
            return e && e.get ? e.get(this) : rr.propHooks._default.get(this)
        },
        run: function(e) {
            var t, n = rr.propHooks[this.prop];
            return this.pos = t = this.options.duration ? b.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : rr.propHooks._default.set(this), this
        }
    }, rr.prototype.init.prototype = rr.prototype, rr.propHooks = {
        _default: {
            get: function(e) {
                var t;
                return null == e.elem[e.prop] || e.elem.style && null != e.elem.style[e.prop] ? (t = b.css(e.elem, e.prop, ""), t && "auto" !== t ? t : 0) : e.elem[e.prop]
            },
            set: function(e) {
                b.fx.step[e.prop] ? b.fx.step[e.prop](e) : e.elem.style && (null != e.elem.style[b.cssProps[e.prop]] || b.cssHooks[e.prop]) ? b.style(e.elem, e.prop, e.now + e.unit) : e.elem[e.prop] = e.now
            }
        }
    }, rr.propHooks.scrollTop = rr.propHooks.scrollLeft = {
        set: function(e) {
            e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
        }
    }, b.each(["toggle", "show", "hide"], function(e, t) {
        var n = b.fn[t];
        b.fn[t] = function(e, r, i) {
            return null == e || "boolean" == typeof e ? n.apply(this, arguments) : this.animate(ir(t, !0), e, r, i)
        }
    }), b.fn.extend({
        fadeTo: function(e, t, n, r) {
            return this.filter(nn).css("opacity", 0).show().end().animate({
                opacity: t
            }, e, n, r)
        },
        animate: function(e, t, n, r) {
            var i = b.isEmptyObject(e),
                o = b.speed(t, n, r),
                a = function() {
                    var t = er(this, b.extend({}, e), o);
                    a.finish = function() {
                        t.stop(!0)
                    }, (i || b._data(this, "finish")) && t.stop(!0)
                };
            return a.finish = a, i || o.queue === !1 ? this.each(a) : this.queue(o.queue, a)
        },
        stop: function(e, n, r) {
            var i = function(e) {
                var t = e.stop;
                delete e.stop, t(r)
            };
            return "string" != typeof e && (r = n, n = e, e = t), n && e !== !1 && this.queue(e || "fx", []), this.each(function() {
                var t = !0,
                    n = null != e && e + "queueHooks",
                    o = b.timers,
                    a = b._data(this);
                if (n) a[n] && a[n].stop && i(a[n]);
                else
                    for (n in a) a[n] && a[n].stop && Jn.test(n) && i(a[n]);
                for (n = o.length; n--;) o[n].elem !== this || null != e && o[n].queue !== e || (o[n].anim.stop(r), t = !1, o.splice(n, 1));
                (t || !r) && b.dequeue(this, e)
            })
        },
        finish: function(e) {
            return e !== !1 && (e = e || "fx"), this.each(function() {
                var t, n = b._data(this),
                    r = n[e + "queue"],
                    i = n[e + "queueHooks"],
                    o = b.timers,
                    a = r ? r.length : 0;
                for (n.finish = !0, b.queue(this, e, []), i && i.cur && i.cur.finish && i.cur.finish.call(this), t = o.length; t--;) o[t].elem === this && o[t].queue === e && (o[t].anim.stop(!0), o.splice(t, 1));
                for (t = 0; a > t; t++) r[t] && r[t].finish && r[t].finish.call(this);
                delete n.finish
            })
        }
    });

    function ir(e, t) {
        var n, r = {
                height: e
            },
            i = 0;
        for (t = t ? 1 : 0; 4 > i; i += 2 - t) n = Zt[i], r["margin" + n] = r["padding" + n] = e;
        return t && (r.opacity = r.width = e), r
    }
    b.each({
        slideDown: ir("show"),
        slideUp: ir("hide"),
        slideToggle: ir("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(e, t) {
        b.fn[e] = function(e, n, r) {
            return this.animate(t, e, n, r)
        }
    }), b.speed = function(e, t, n) {
        var r = e && "object" == typeof e ? b.extend({}, e) : {
            complete: n || !n && t || b.isFunction(e) && e,
            duration: e,
            easing: n && t || t && !b.isFunction(t) && t
        };
        return r.duration = b.fx.off ? 0 : "number" == typeof r.duration ? r.duration : r.duration in b.fx.speeds ? b.fx.speeds[r.duration] : b.fx.speeds._default, (null == r.queue || r.queue === !0) && (r.queue = "fx"), r.old = r.complete, r.complete = function() {
            b.isFunction(r.old) && r.old.call(this), r.queue && b.dequeue(this, r.queue)
        }, r
    }, b.easing = {
        linear: function(e) {
            return e
        },
        swing: function(e) {
            return .5 - Math.cos(e * Math.PI) / 2
        }
    }, b.timers = [], b.fx = rr.prototype.init, b.fx.tick = function() {
        var e, n = b.timers,
            r = 0;
        for (Xn = b.now(); n.length > r; r++) e = n[r], e() || n[r] !== e || n.splice(r--, 1);
        n.length || b.fx.stop(), Xn = t
    }, b.fx.timer = function(e) {
        e() && b.timers.push(e) && b.fx.start()
    }, b.fx.interval = 13, b.fx.start = function() {
        Un || (Un = setInterval(b.fx.tick, b.fx.interval))
    }, b.fx.stop = function() {
        clearInterval(Un), Un = null
    }, b.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    }, b.fx.step = {}, b.expr && b.expr.filters && (b.expr.filters.animated = function(e) {
        return b.grep(b.timers, function(t) {
            return e === t.elem
        }).length
    }), b.fn.offset = function(e) {
        if (arguments.length) return e === t ? this : this.each(function(t) {
            b.offset.setOffset(this, e, t)
        });
        var n, r, o = {
                top: 0,
                left: 0
            },
            a = this[0],
            s = a && a.ownerDocument;
        if (s) return n = s.documentElement, b.contains(n, a) ? (typeof a.getBoundingClientRect !== i && (o = a.getBoundingClientRect()), r = or(s), {
            top: o.top + (r.pageYOffset || n.scrollTop) - (n.clientTop || 0),
            left: o.left + (r.pageXOffset || n.scrollLeft) - (n.clientLeft || 0)
        }) : o
    }, b.offset = {
        setOffset: function(e, t, n) {
            var r = b.css(e, "position");
            "static" === r && (e.style.position = "relative");
            var i = b(e),
                o = i.offset(),
                a = b.css(e, "top"),
                s = b.css(e, "left"),
                u = ("absolute" === r || "fixed" === r) && b.inArray("auto", [a, s]) > -1,
                l = {},
                c = {},
                p, f;
            u ? (c = i.position(), p = c.top, f = c.left) : (p = parseFloat(a) || 0, f = parseFloat(s) || 0), b.isFunction(t) && (t = t.call(e, n, o)), null != t.top && (l.top = t.top - o.top + p), null != t.left && (l.left = t.left - o.left + f), "using" in t ? t.using.call(e, l) : i.css(l)
        }
    }, b.fn.extend({
        position: function() {
            if (this[0]) {
                var e, t, n = {
                        top: 0,
                        left: 0
                    },
                    r = this[0];
                return "fixed" === b.css(r, "position") ? t = r.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), b.nodeName(e[0], "html") || (n = e.offset()), n.top += b.css(e[0], "borderTopWidth", !0), n.left += b.css(e[0], "borderLeftWidth", !0)), {
                    top: t.top - n.top - b.css(r, "marginTop", !0),
                    left: t.left - n.left - b.css(r, "marginLeft", !0)
                }
            }
        },
        offsetParent: function() {
            return this.map(function() {
                var e = this.offsetParent || o.documentElement;
                while (e && !b.nodeName(e, "html") && "static" === b.css(e, "position")) e = e.offsetParent;
                return e || o.documentElement
            })
        }
    }), b.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(e, n) {
        var r = /Y/.test(n);
        b.fn[e] = function(i) {
            return b.access(this, function(e, i, o) {
                var a = or(e);
                return o === t ? a ? n in a ? a[n] : a.document.documentElement[i] : e[i] : (a ? a.scrollTo(r ? b(a).scrollLeft() : o, r ? o : b(a).scrollTop()) : e[i] = o, t)
            }, e, i, arguments.length, null)
        }
    });

    function or(e) {
        return b.isWindow(e) ? e : 9 === e.nodeType ? e.defaultView || e.parentWindow : !1
    }
    b.each({
        Height: "height",
        Width: "width"
    }, function(e, n) {
        b.each({
            padding: "inner" + e,
            content: n,
            "": "outer" + e
        }, function(r, i) {
            b.fn[i] = function(i, o) {
                var a = arguments.length && (r || "boolean" != typeof i),
                    s = r || (i === !0 || o === !0 ? "margin" : "border");
                return b.access(this, function(n, r, i) {
                    var o;
                    return b.isWindow(n) ? n.document.documentElement["client" + e] : 9 === n.nodeType ? (o = n.documentElement, Math.max(n.body["scroll" + e], o["scroll" + e], n.body["offset" + e], o["offset" + e], o["client" + e])) : i === t ? b.css(n, r, s) : b.style(n, r, i, s)
                }, n, a ? i : t, a, null)
            }
        })
    }), e.jQuery = e.$ = b, "function" == typeof define && define.amd && define.amd.jQuery && define("jquery", [], function() {
        return b
    })
})(window);
jQuery.noConflict();
Varien.Tabs = Class.create();
Varien.Tabs.prototype = {
    initialize: function(selector) {
        var self = this;
        $$(selector + ' a').each(this.initTab.bind(this));
    },
    initTab: function(el) {
        if ($(el.parentNode).hasClassName('active')) {
            this.showContent(null, el);
        }
        el.observe('click', this.showContent.bindAsEventListener(this, el));
    },
    showContent: function(e, a) {
        var li = $(a.parentNode),
            ul = $(li.parentNode);
        this.switchTab(ul, li);
        if (e) {
            e.stop();
        }
    },
    switchTab: function(ul, li) {
        ul.select('li', 'ol').each(function(el) {
            var contents = $(el.id + '_contents');
            if (el == li) {
                el.addClassName('active');
                contents.show();
            } else {
                el.removeClassName('active');
                contents.hide();
            }
        });
    }
}
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
var isIE = navigator.userAgent.indexOf("MSIE") != -1 || navigator.userAgent.match(/Trident.*rv[ :]*11\./);
var AW_AjaxCartProConfig = {
    actionsObservers: {},
    targetsToUpdate: {},
    data: {},
    addUpdater: function(updater) {
        this.targetsToUpdate[updater.name] = updater;
    },
    addObserver: function(observer) {
        this.actionsObservers[observer.name] = observer;
    },
    addSystemData: function(data) {
        if (typeof(data.custom) != 'undefined') {
            this.addUpdaterData(data.custom);
        }
        Object.extend(this.data, data);
    },
    addUpdaterData: function(customData) {
        for (var item in customData) {
            if (this.targetsToUpdate[item]) {
                this.targetsToUpdate[item].selectors = customData[item].selectors || null;
                this.targetsToUpdate[item].parentSelector = customData[item].parentSelector || null;
            }
        }
    }
};
var AW_AjaxCartPro = {
    config: AW_AjaxCartProConfig,
    init: function(config) {
        this.connector = AW_AjaxCartProConnector;
        this.ui = AW_AjaxCartProUI;
        this.config.addSystemData(config);
        this.startObservers();
    },
    registerUpdater: function(updater) {
        this.config.addUpdater(updater);
    },
    registerObserver: function(observer) {
        this.config.addObserver(observer);
    },
    startObservers: function(name) {
        var me = this;
        if (Object.isString(name)) {
            var observer = this.config.actionsObservers[name];
            if (observer) {
                observer.run();
            }
        } else {
            if (typeof(this.config.actionsObservers) == 'object') {
                Object.keys(this.config.actionsObservers).each(function(k) {
                    me.config.actionsObservers[k].run();
                });
            }
        }
    },
    stopObservers: function(name) {
        var me = this;
        if (Object.isString(name)) {
            var observer = this.config.actionsObservers[name];
            if (observer) {
                observer.stop();
            }
        } else {
            if (typeof(this.config.actionsObservers) == 'object') {
                Object.keys(this.config.actionsObservers).each(function(k) {
                    me.config.actionsObservers[k].stop();
                });
            }
        }
    },
    callUpdaters: function(blocks) {
        var me = this;
        var blocks = blocks || {};
        var isSuccessUpdate = true;
        this.ui.beforeUpdate(me.msg);
        Object.keys(blocks).each(function(k) {
            if (!isSuccessUpdate || blocks[k] === null) {
                return;
            }
            var result = true;
            try {
                result = me.config.targetsToUpdate[k].update(blocks[k]);
            } catch (e) {
                result = false;
            }
            isSuccessUpdate = isSuccessUpdate && result;
        });
        this.ui.afterUpdate(me.msg);
        delete me.msg;
        return isSuccessUpdate;
    },
    fire: function(url, parameters, observer) {
        var me = this;
        this.ui.observer = observer;
        this.ui.beforeFire();
        var parameters = parameters || {};
        parameters['block[]'] = [];
        Object.keys(me.config.targetsToUpdate).each(function(k) {
            if (me.config.targetsToUpdate[k].updateOnActionRequest) {
                parameters['block[]'].push(k);
            }
        });
        parameters[new Date().getTime()] = new Date().getTime();
        this.connector.sendRequest(url, parameters, function(response) {
            var isResponseHasBlock = Object.values(response.block).without(null).length > 0;
            if (isResponseHasBlock) {
                var isSuccessUpdate = me.callUpdaters(response.block);
                if (!isSuccessUpdate) {
                    document.location.reload();
                    return;
                }
                me.stopObservers();
                me.startObservers();
                return;
            }
            if (response.redirect_to) {
                me.msg = response.msg;
                me.fire(response.redirect_to, parameters, observer);
                return;
            }
            me.update(function(json) {
                document.location.reload();
            }, {
                actionData: Object.toJSON(response.action_data)
            });
        }, function(json) {
            observer.fireOriginal(url, parameters);
        });
    },
    update: function(failureFn, additionalParams) {
        var me = this;
        failureFn = failureFn || function() {};
        var url = document.location.pathname + document.location.search;
        if (document.domain.match(/resultsdemo|resultsstage|\.local|search\.staging|search|health\.healthpost/gi)) {
            url = document.location.protocol + '//' + document.domain + '/configurable' + document.location.search;
        }
        var parameters = additionalParams;
        parameters['block[]'] = [];
        Object.keys(me.config.targetsToUpdate).each(function(k) {
            if (me.config.targetsToUpdate[k].updateOnUpdateRequest) {
                parameters['block[]'].push(k);
            }
        });
        parameters[new Date().getTime()] = new Date().getTime();
        this.connector.sendRequest(url, parameters, function(json) {
            var isSuccessUpdate = me.callUpdaters(json.block);
            if (!isSuccessUpdate) {
                failureFn(json);
                return;
            }
            me.stopObservers();
            me.startObservers();
            me.ui.afterFire(parameters);
        }, failureFn);
    }
};
var AW_AjaxCartProConnector = {
    defaultParameters: {
        awacp: 1,
        no_cache: 1
    },
    sendRequest: function(url, parameters, success, failure) {
        var me = this;
        var parameters = parameters || {};
        var failure = failure || function() {};
        var success = success || function() {};
        url = url.replace('https://', 'http://');
        if (window.location.protocol == 'https:') {
            url = url.replace('http://', 'https://');
        }
        Object.extend(parameters, this.defaultParameters);
        var options = {
            parameters: parameters,
            method: 'get',
            onSuccess: this.onSuccessFn(success, failure),
            onFailure: this.onFailureFn(success, failure)
        };
        url = url.replace(/http[^:]*:/, document.location.protocol);
        if ($$('.cms-index-noroute')[0] != undefined) {
            new Ajax.Request(url, {
                onSuccess: function(response) {
                    new Ajax.Request(url, options);
                },
                onFailure: function() {
                    new Ajax.Request('/', options);
                }
            });
        } else {
            new Ajax.Request(url, options);
        }
    },
    onSuccessFn: function(success, failure) {
        return function(transport) {
            try {
                eval("var json = " + transport.responseText + " || {}");
            } catch (e) {
                failure({});
                return;
            }
            if (!json.success) {
                failure(json);
                return;
            }
            success(json);
        };
    },
    onFailureFn: function(success, failure) {
        return function(transport) {
            failure(json);
        };
    }
};
var AW_AjaxCartProUI = {
    observer: null,
    blocks: {},
    config: AW_AjaxCartProConfig.data,
    hideCls: 'ajaxcartpro-box-hide',
    showCls: 'ajaxcartpro-box-show',
    overlayCssSelector: '#acp-overlay',
    beforeFire: function() {
        return this._call('beforeFire', arguments);
    },
    afterFire: function() {
        return this._call('afterFire', arguments);
    },
    beforeUpdate: function(msg) {
        return this._call('beforeUpdate', arguments);
    },
    afterUpdate: function() {
        return this._call('afterUpdate', arguments);
    },
    registerBlock: function(block) {
        var block = block || {};
        if (!block.name) {
            return;
        }
        this.blocks[block.name] = block;
    },
    showBlock: function(el) {
        el = this._initEl(el);
        if (!el) {
            return false;
        }
        this._resizeBlock(el);
        var me = this;
        el.onWindowResizeHandler = function(e) {
            me._resizeBlock(el);
        };
        Event.observe(window, 'resize', el.onWindowResizeHandler);
        this._show(el);
        var overlay = $$(this.overlayCssSelector)[0];
        overlay.observe('click', this._clickOnOverlay.bind(this));
        this._show(overlay);
        $$('div.page')[0].addClassName('fixed');
        return true;
    },
    hideBlock: function(el) {
        el = this._initEl(el);
        if (!el) {
            return false;
        }
        el.setStyle({
            height: 'auto',
            width: 'auto'
        });
        var me = this;
        var activeBlocks = 0;
        Object.keys(me.blocks).each(function(blockName) {
            var block = me.blocks[blockName];
            if (block.enabled === true) {
                activeBlocks++;
            }
        });
        if (activeBlocks === 0) {
            var overlay = $$(this.overlayCssSelector)[0];
            overlay.stopObserving('click', this._clickOnOverlay.bind(this));
            this._hide(overlay);
        }
        this._hide(el);
        if (typeof(el.onWindowResizeHandler) === 'function') {
            Event.stopObserving(window, 'resize', el.onWindowResizeHandler);
            delete el.onWindowResizeHandler;
        }
        $$('div.page')[0].removeClassName('fixed');
        return true;
    },
    _clickOnOverlay: function(e) {
        $$('.aw-acp-continue').each(function(el) {
            if (!el.click) {
                if (document.createEvent) {
                    var evt = document.createEvent('MouseEvents');
                    evt.initEvent('click', true, false);
                    el.dispatchEvent(evt);
                } else if (document.createEventObject) {
                    el.fireEvent('onclick');
                } else if (typeof node.onclick == 'function') {
                    el.onclick();
                }
            } else {
                el.click();
            }
        });
    },
    _call: function(fnName, args) {
        var me = this;
        Object.keys(me.blocks).each(function(blockName) {
            if (me.observer.uiBlocks.indexOf(blockName) === -1) {
                return;
            }
            var block = me.blocks[blockName];
            if (typeof(block[fnName]) == 'function') {
                block[fnName](args);
            }
        });
        var isSuccessCall = true;
        Object.keys(me.blocks).each(function(blockName) {
            if (!isSuccessCall) {
                return;
            }
            var block = me.blocks[blockName];
            if (block.enabled === true) {
                isSuccessCall = isSuccessCall && me.showBlock(block.cssSelector);
            } else {
                isSuccessCall = isSuccessCall && me.hideBlock(block.cssSelector);
            }
        });
        return isSuccessCall;
    },
    _show: function(el) {
        el.removeClassName(this.hideCls);
        el.addClassName(this.showCls);
    },
    _hide: function(el) {
        el.removeClassName(this.showCls);
        el.addClassName(this.hideCls);
    },
    _initEl: function(el) {
        if (Object.isString(el)) {
            el = $$(el);
            if (el.length > 0) {
                el = el[0];
            } else {
                return false;
            }
        }
        el = $(el);
        if (!el) {
            return false;
        }
        return el;
    },
    _collectPos: function(el, horPos, verPos) {
        var x, y;
        var elWidth = el.getWidth();
        var docWidth = document.viewport.getWidth();
        switch (horPos) {
            case 'center':
                x = docWidth / 2 - elWidth / 2;
                break;
            case 'left':
                x = 50;
                break;
            case 'right':
                x = docWidth - elWidth;
                break;
            default:
        }
        var elHeight = el.getHeight();
        var docHeight = document.viewport.getHeight();
        switch (verPos) {
            case 'top':
                y = 0;
                break;
            case 'center':
                y = docHeight / 2 - elHeight / 2;
                break;
            case 'bottom':
                y = docHeight - elHeight;
                break;
            default:
        }
        return [x, y];
    },
    _resizeBlock: function(el) {
        var viewport = document.viewport.getDimensions();
        var width = viewport.width;
        var _height = viewport.height;
        var head = $$('.acp-msg-block');
        if (head.length != 0) {
            var head_height = head.clientHeight;
        }
        el.getHeight();
        if (_height < el.getHeight()) {
            el.setStyle({
                height: _height - 20 + 'px',
                width: 'auto'
            });
        }
        var xy = this._collectPos(el, 'center', this.config.dialogsVAlign);
        if (xy[0] < 50) {
            xy[0] = 50;
            el.setStyle({
                width: (document.viewport.getWidth() - 100) + 'px'
            });
        }
        if (xy[1] < 50) {
            xy[1] = 50;
            el.setStyle({
                height: _height - 20 + 'px'
            });
        }
        el.setStyle({
            'left': xy[0] + 'px',
            'top': xy[1] + 'px'
        });
    }
};
var AW_AjaxCartProObserver = Class.create();
AW_AjaxCartProObserver.prototype = {
    name: null,
    uiBlocks: [],
    initialize: function(name) {
        this.name = name;
    },
    run: function() {
        return null;
    },
    stop: function() {
        return null;
    },
    fireOriginal: function(url, parameters) {
        document.location.href = url;
        return null;
    },
    fireCustom: function(url, parameters) {
        var parameters = parameters || {};
        AW_AjaxCartPro.fire(url, parameters, this);
    }
};
var AW_AjaxCartProUpdater = Class.create();
AW_AjaxCartProUpdater.prototype = {
    config: AW_AjaxCartProConfig.data,
    selectors: null,
    parentSelector: null,
    name: null,
    initialize: function(name, selectors, parentSelector) {
        this.name = name;
        this.selectors = selectors || null;
        this.parentSelector = parentSelector || null;
    },
    beforeUpdate: function(html) {
        return null;
    },
    afterUpdate: function(html) {
        return null;
    },
    update: function(html) {
        this.beforeUpdate(html);
        var me = this;
        var selectors = this.selectors;
        if (selectors === null) {
            selectors = this._getRootSelectors(html);
        }
        var storage = new Element('div');
        storage.innerHTML = html;
        if (storage.childElements().length != selectors.length && storage.childElements().length > 0) {
            return false;
        }
        if (!this._checkSelectorsOnUnique(selectors)) {
            return false;
        }
        selectors.each(function(cssSelector) {
            var part = storage.select(cssSelector)[0];
            var target = null;
            me._getSelectorsToTarget(cssSelector).each(function(selector) {
                if (target !== null) {
                    return;
                }
                if ($$(selector).length > 0) {
                    target = $$(selector)[0];
                }
            });
            if (!target) {
                return;
            }
            if (!part) {
                target.parentNode.removeChild(target);
                return;
            }
            target.parentNode.replaceChild(part, target);
        });
        delete storage;
        this._evalScripts(html);
        this.afterUpdate(html, selectors);
        return true;
    },
    _getRootSelectors: function(html) {
        var div = new Element('div');
        div.innerHTML = html;
        var selectors = [];
        div.childElements().each(function(el) {
            selectors.push(this._getCssSelectorsByElement(el));
        }, this);
        delete div;
        return selectors;
    },
    _checkSelectorsOnUnique: function(selectors) {
        var isUnique = true;
        selectors.each(function(cssSelector) {
            var possibleSelectors = this._getSelectorsToTarget(cssSelector);
            var selectorToCheck = null;
            possibleSelectors.each(function(selector) {
                if (selectorToCheck !== null) {
                    return;
                }
                if ($$(selector) > 0) {
                    selectorToCheck = $$(selector)[0];
                }
            });
            if ($$(selectorToCheck).length > 1) {
                isUnique = false;
            }
        }, this);
        return isUnique;
    },
    _getSelectorsToTarget: function(selector) {
        var selectors = [];
        if (this.parentSelector !== null) {
            this.parentSelector.each(function(parent) {
                var selectorToTarget = parent + ' ' + selector;
                selectors.push(selectorToTarget);
            });
        } else {
            selectors.push(selector);
        }
        return selectors;
    },
    _getCssSelectorsByElement: function(element) {
        element = $(element);
        var cssSelector = element.tagName.toLowerCase();
        $H({
            'id': 'id',
            'className': 'class'
        }).each(function(pair) {
            var property = pair.first(),
                attribute = pair.last(),
                value = (element[property] || '').toString();
            if (value) {
                if (attribute === 'id') {
                    cssSelector += '#' + value;
                } else {
                    value = value.split(' ').join('.');
                    cssSelector += '.' + value;
                }
            }
        });
        return cssSelector;
    },
    _evalScripts: function(html) {
        var scripts = html.extractScripts();
        scripts.each(function(script) {
            try {
                script = script.replace('//<![CDATA[', '').replace('//]]>', '');
                script = script.replace('/*<![CDATA[*/', '').replace('/*]]>*/', '');
                eval(script.replace(/var /gi, ""));
            } catch (e) {}
        });
    }
};
var AW_AjaxCartProUIBlocks = [{
    cssSelector: '#ajaxcartpro-progress',
    name: 'progress',
    enabled: false,
    beforeFire: function(args) {
        $$('.messages').each(function(el) {
            el.hide();
        });
        if (!AW_AjaxCartPro.config.data.useProgress) {
            return;
        }
        this.enabled = true;
    },
    afterFire: function(args) {},
    beforeUpdate: function(args) {},
    afterUpdate: function(args) {
        this.enabled = false;
    }
}, {
    name: 'add_confirmation',
    cssSelector: '#ajaxcartpro-add-confirm',
    enabled: false,
    beforeFire: function(args) {
        this.enabled = false;
    },
    afterFire: function(args) {
        eval("var actionDataAsJson = " + args[0].actionData);
        actionDataAsJson = actionDataAsJson || {};
        var isConfirmationEnabled = AW_AjaxCartPro.config.data.addProductConfirmationEnabled;
        if ("confirmation_enabled" in actionDataAsJson) {
            isConfirmationEnabled = actionDataAsJson.confirmation_enabled;
        }
        if (!isConfirmationEnabled) {
            return;
        }
        this.enabled = true;
        this._cntBtnInit(actionDataAsJson);
    },
    beforeUpdate: function(args) {},
    afterUpdate: function(args) {},
    _cntBtnInit: function(actionDataAsJson) {
        var cntBtn = $$(this.cssSelector)[0].select('.aw-acp-continue')[0];
        if (!cntBtn) {
            cntBtn = new Element('div');
            cntBtn.addClassName('aw-acp-continue');
            cntBtn.setStyle({
                'display': 'none'
            });
            $$(this.cssSelector)[0].appendChild(cntBtn);
        }
        cntBtn.stopObserving('click', this._cntBtnOnClick.bind(this));
        cntBtn.observe('click', this._cntBtnOnClick.bind(this));
        var counter = AW_AjaxCartPro.config.data.addProductCounterBeginFrom;
        if ("counter_begin_from" in actionDataAsJson) {
            counter = actionDataAsJson.counter_begin_from;
        }
        if (counter > 0) {
            this._initCounterForBtn(cntBtn, counter);
        }
    },
    _cntBtnOnClick: function(event) {
        this.enabled = false;
        AW_AjaxCartProUI.hideBlock(this.cssSelector);
        event.stop();
    },
    _initCounterForBtn: function(cntBtn, counter) {
        var originalTxt = cntBtn.innerHTML;
        cntBtn.innerHTML = originalTxt + ' (' + counter + ')';
        var intId = setInterval(function() {
            counter--;
            if (counter === 0) {
                if (!cntBtn.click) {
                    if (document.createEvent) {
                        var evt = document.createEvent('MouseEvents');
                        evt.initEvent('click', true, false);
                        cntBtn.dispatchEvent(evt);
                    } else if (document.createEventObject) {
                        cntBtn.fireEvent('onclick');
                    } else if (typeof node.onclick == 'function') {
                        cntBtn.onclick();
                    }
                } else {
                    cntBtn.click();
                }
            }
            cntBtn.innerHTML = originalTxt + ' (' + counter + ')';
        }, 1000);
        var clearIntervalFn = function(e) {
            clearInterval(intId);
            cntBtn.stopObserving('click', this.bind(this));
        };
        cntBtn.observe('click', clearIntervalFn.bind(clearIntervalFn));
    }
}, {
    name: 'remove_confirmation',
    cssSelector: '#ajaxcartpro-remove-confirm',
    enabled: false,
    beforeFire: function(args) {
        this.enabled = false;
    },
    afterFire: function(args) {
        eval("var actionDataAsJson = " + args[0].actionData);
        actionDataAsJson = actionDataAsJson || {};
        var isConfirmationEnabled = AW_AjaxCartPro.config.data.removeProductConfirmationEnabled;
        if ("confirmation_enabled" in actionDataAsJson) {
            isConfirmationEnabled = actionDataAsJson.confirmation_enabled;
        }
        if (!isConfirmationEnabled) {
            return;
        }
        this.enabled = true;
        this._cntBtnInit(actionDataAsJson);
    },
    beforeUpdate: function(args) {},
    afterUpdate: function(args) {},
    _cntBtnInit: function(actionDataAsJson) {
        var cntBtn = $$(this.cssSelector)[0].select('.aw-acp-continue')[0];
        if (!cntBtn) {
            cntBtn = new Element('div');
            cntBtn.addClassName('aw-acp-continue');
            cntBtn.setStyle({
                'display': 'none'
            });
            $$(this.cssSelector)[0].appendChild(cntBtn);
        }
        cntBtn.stopObserving('click', this._cntBtnOnClick.bind(this));
        cntBtn.observe('click', this._cntBtnOnClick.bind(this));
        var counter = AW_AjaxCartPro.config.data.removeProductCounterBeginFrom;
        if ("counter_begin_from" in actionDataAsJson) {
            counter = actionDataAsJson.counter_begin_from;
        }
        if (counter > 0) {
            this._initCounterForBtn(cntBtn, counter);
        }
    },
    _cntBtnOnClick: function(event) {
        this.enabled = false;
        AW_AjaxCartProUI.hideBlock(this.cssSelector);
        event.stop();
    },
    _initCounterForBtn: function(cntBtn, counter) {
        var originalTxt = cntBtn.innerHTML;
        cntBtn.innerHTML = originalTxt + ' (' + counter + ')';
        var intId = setInterval(function() {
            counter--;
            if (counter === 0) {
                if (!cntBtn.click) {
                    if (document.createEvent) {
                        var evt = document.createEvent('MouseEvents');
                        evt.initEvent('click', true, false);
                        cntBtn.dispatchEvent(evt);
                    } else if (document.createEventObject) {
                        cntBtn.fireEvent('onclick');
                    } else if (typeof node.onclick == 'function') {
                        cntBtn.onclick();
                    }
                } else {
                    cntBtn.click();
                }
            }
            cntBtn.innerHTML = originalTxt + ' (' + counter + ')';
        }, 1000);
        var clearIntervalFn = function(e) {
            clearInterval(intId);
            cntBtn.stopObserving('click', this.bind(this));
        };
        cntBtn.observe('click', clearIntervalFn.bind(clearIntervalFn));
    }
}, {
    name: 'options',
    rootCssSelector: '#acp-configurable-block',
    cssSelector: '#acp-product-options',
    enabled: false,
    beforeFire: function(args) {},
    afterFire: function(args) {},
    beforeUpdate: function(args) {},
    afterUpdate: function(args) {
        var el = $$(this.cssSelector);
        if (el.length === 1) {
            this.enabled = true;
            var msg = args[0];
            this._addMsgBlock(msg);
            this._appearGroupedBlock();
            this._appearGiftBlock();
            this._appearAWGiftBlock();
            this._cancelBtnInit();
            this._addToCartBtnInit();
            this._addToCartEnterInit();
        }
    },
    _addMsgBlock: function(msg) {
        if (!msg || !msg.length || msg.length < 1) {
            return;
        }
        var msgBlock = $$('.acp-msg-block')[0];
        msgBlock.addClassName('messages');
        AW_AjaxCartProUI._show(msgBlock);
        msgBlock.appendChild(new Element('li'));
        msgBlock = msgBlock.select('li')[0];
        msgBlock.innerHTML = '';
        var typeCount = {
            'error': 0,
            'warning': 0,
            'notice': 0
        };
        msg.each(function(message) {
            switch (message.type) {
                case 'error':
                    typeCount.error++;
                    break;
                case 'warning':
                    typeCount.warning++;
                    break;
                case 'notice':
                    typeCount.notice++;
                    break;
                default:
            }
        });
        var type = 'notice';
        if (typeCount.warning > 0) {
            type = 'warning';
        }
        if (typeCount.error > 0) {
            type = 'error';
        }
        switch (type) {
            case 'error':
                msgBlock.addClassName('error-msg');
                msgBlock.removeClassName('notice-msg');
                msgBlock.removeClassName('warning-msg');
                break;
            case 'warning':
                msgBlock.addClassName('warning-msg');
                msgBlock.removeClassName('error-msg');
                msgBlock.removeClassName('notice-msg');
                break;
            case 'notice':
                msgBlock.addClassName('notice-msg');
                msgBlock.removeClassName('error-msg');
                msgBlock.removeClassName('warning-msg');
                break;
            default:
                AW_AjaxCartProUI._hide(msgBlock);
        }
        msg.each(function(message) {
            msgBlock.innerHTML += message.text + '<br />';
        });
    },
    _appearGroupedBlock: function() {
        var possibleSelectors = ['table.grouped-items-table', 'table#super-product-table'];
        var groupedBlock = null;
        possibleSelectors.each(function(selector) {
            if (groupedBlock !== null) {
                return;
            }
            groupedBlock = $('acp-product-type-data').select(selector);
            if (groupedBlock.length === 0) {
                groupedBlock = null;
                return;
            }
            groupedBlock = groupedBlock[0];
        });
        AW_AjaxCartProUI._show($('acp-product-type-data'));
        $('acp-product-type-data').childElements().each(function(el) {
            if (el === groupedBlock || el.tagName.toLocaleLowerCase() === 'script') {
                return;
            }
        });
        return null;
    },
    _appearGiftBlock: function() {
        var giftBlock = $('acp-product-type-data').select('.giftcard-send-form');
        if (giftBlock.length === 0) {
            return;
        }
        giftBlock = giftBlock[0];
        AW_AjaxCartProUI._show($('acp-product-type-data'));
        AW_AjaxCartProUI._show(giftBlock.up());
        giftBlock.up().childElements().each(function(el) {
            if (el === giftBlock || el.tagName.toLocaleLowerCase() === 'script') {
                return;
            }
            AW_AjaxCartProUI._hide(el);
        });
        return null;
    },
    _appearAWGiftBlock: function() {
        var giftBlock = $('acp-product-type-data').select('.aw-gc-form');
        if (giftBlock.length === 0) {
            return;
        }
        giftBlock = giftBlock[0];
        AW_AjaxCartProUI._show($('acp-product-type-data'));
        AW_AjaxCartProUI._show(giftBlock);
        return null;
    },
    _cancelBtnInit: function() {
        var cancelBtn = $$(this.cssSelector)[0].select('.aw-acp-continue')[0];
        cancelBtn.stopObserving('click', this._cancelBtnOnClick.bind(this));
        cancelBtn.observe('click', this._cancelBtnOnClick.bind(this));
    },
    _cancelBtnOnClick: function(event) {
        this._hideBlock();
        event.stop();
    },
    _addToCartBtnInit: function() {
        var addToCartButton = $$(this.cssSelector)[0].select('.aw-acp-checkout')[0];
        addToCartButton.stopObserving('click', this._addToCartBtnOnClick.bind(this));
        addToCartButton.observe('click', this._addToCartBtnOnClick.bind(this));
    },
    _addToCartEnterInit: function() {
        var that = this;
        $$(this.cssSelector).each(function(f) {
            $(f).select("input").each(function(e) {
                e.stopObserving('keypress', that._addToCartEnterOnClick.bind(that));
                e.observe('keypress', that._addToCartEnterOnClick.bind(that));
            });
        });
    },
    _addToCartBtnOnClick: function(event) {
        if (productAddToCartFormAcp.validator && productAddToCartFormAcp.validator.validate()) {
            productAddToCartFormAcp.form.submit();
            this._hideBlock();
        }
        event.stop();
    },
    _addToCartEnterOnClick: function(event) {
        if (event.keyCode == Event.KEY_RETURN || event.which == Event.KEY_RETURN) {
            if (productAddToCartFormAcp.validator && productAddToCartFormAcp.validator.validate()) {
                productAddToCartFormAcp.form.submit();
                this._hideBlock();
            }
            event.stop();
        }
    },
    _hideBlock: function() {
        this.enabled = false;
        AW_AjaxCartProUI.hideBlock(this.cssSelector);
        $$(this.rootCssSelector)[0].down().remove();
    }
}];
AW_AjaxCartProUIBlocks.each(function(block) {
    AW_AjaxCartProUI.registerBlock(block);
});
var AW_AjaxCartProUpdaterObject = new AW_AjaxCartProUpdater('cart', ['.cart']);
Object.extend(AW_AjaxCartProUpdaterObject, {
    updateOnUpdateRequest: true,
    updateOnActionRequest: false,
    beforeUpdate: function(html) {
        return null;
    },
    update: function(html) {
        this.beforeUpdate(html);
        var selector = this._getTargetSelector();
        if (selector === null) {
            return false;
        }
        $$(selector)[0].innerHTML = html;
        this._evalScripts(html);
        this.afterUpdate(html, [selector]);
        return true;
    },
    afterUpdate: function(html, selectors) {
        asignQuantityCounter();
        var me = this;
        selectors.each(function(selector) {
            me._effect(selector);
        });
        return null;
    },
    _getTargetSelector: function() {
        var targetSelector = null;
        this.selectors.each(function(selector) {
            if (targetSelector !== null) {
                return;
            }
            if ($$(selector).length > 0) {
                targetSelector = selector;
            }
        });
        return targetSelector;
    },
    _effect: function(obj) {
        var el = $$(obj)[0];
        switch (this.config.cartAnimation) {
            case 'opacity':
                el.hide();
                new Effect.Appear(el);
                break;
            case 'grow':
                el.hide();
                new Effect.BlindDown(el);
                break;
            case 'blink':
                new Effect.Pulsate(el);
                break;
            default:
        }
    }
});
AW_AjaxCartPro.registerUpdater(AW_AjaxCartProUpdaterObject);
delete AW_AjaxCartProUpdaterObject;
var AW_AjaxCartProUpdaterObject = new AW_AjaxCartProUpdater('sidebar');
Object.extend(AW_AjaxCartProUpdaterObject, {
    updateOnUpdateRequest: true,
    updateOnActionRequest: false,
    beforeUpdate: function(html) {
        return null;
    },
    afterUpdate: function(html, selectors) {
        var me = this;
        if (typeof(truncateOptions) === 'function') {
            truncateOptions();
        }
        selectors.each(function(selector) {
            me._effect(selector);
        });
        return null;
    },
    _effect: function(obj) {
        var el = $$(obj)[0];
        if (typeof(el) == 'undefined') {
            return null;
        }
        switch (this.config.cartAnimation) {
            case 'opacity':
                el.hide();
                new Effect.Appear(el);
                break;
            case 'grow':
                el.hide();
                new Effect.BlindDown(el);
                break;
            case 'blink':
                new Effect.Pulsate(el);
                break;
            default:
        }
    }
});
AW_AjaxCartPro.registerUpdater(AW_AjaxCartProUpdaterObject);
delete AW_AjaxCartProUpdaterObject;
var AW_AjaxCartProUpdaterObject = new AW_AjaxCartProUpdater('topLinks', null, ['.header .quick-access>', '.toplinks-bar']);
Object.extend(AW_AjaxCartProUpdaterObject, {
    updateOnUpdateRequest: true,
    updateOnActionRequest: false,
    beforeUpdate: function(html) {
        return null;
    },
    afterUpdate: function(html, selectors) {
        return null;
    }
});
AW_AjaxCartPro.registerUpdater(AW_AjaxCartProUpdaterObject);
delete AW_AjaxCartProUpdaterObject;
var AW_AjaxCartProUpdaterObject = new AW_AjaxCartProUpdater('options', ['#acp-configurable-block']);
Object.extend(AW_AjaxCartProUpdaterObject, {
    updateOnUpdateRequest: false,
    updateOnActionRequest: true,
    beforeUpdate: function(html) {
        var el = $$(this.selectors[0])[0];
        if (el && el.down()) {
            el.down().remove();
        }
        return null;
    },
    update: function(html) {
        this.beforeUpdate(html);
        var el = new Element('div');
        el.innerHTML = html;
        $$(this.selectors[0])[0].appendChild(el.down());
        this._evalScripts(html);
        this.afterUpdate(html, this.selectors);
        return true;
    },
    afterUpdate: function(html, selectors) {
        return null;
    }
});
AW_AjaxCartPro.registerUpdater(AW_AjaxCartProUpdaterObject);
delete AW_AjaxCartProUpdaterObject;
var AW_AjaxCartProUpdaterObject = new AW_AjaxCartProUpdater('wishlist', ['.my-account']);
Object.extend(AW_AjaxCartProUpdaterObject, {
    updateOnUpdateRequest: true,
    updateOnActionRequest: false,
    beforeUpdate: function(html) {
        return null;
    },
    update: function(html) {
        this.beforeUpdate(html);
        var selector = this.selectors[0];
        $$(selector)[0].innerHTML = html;
        this._evalScripts(html);
        this._evalEnterpriseWishlistScripts();
        this.afterUpdate(html, this.selectors);
        return true;
    },
    afterUpdate: function(html, selectors) {
        return null;
    },
    _evalEnterpriseWishlistScripts: function() {
        if (!Enterprise || !Enterprise.Wishlist) {
            return null;
        }
        $('wishlist-view-form').select('.split-button').each(function(node) {
            if (!$(node).hasClassName('split-button-created')) {
                new Enterprise.Widget.SplitButton(node);
            }
        });
        $$('#wishlist-table div.description').each(function(el) {
            Enterprise.textOverflow(el);
        });
        return null;
    }
});
AW_AjaxCartPro.registerUpdater(AW_AjaxCartProUpdaterObject);
delete AW_AjaxCartProUpdaterObject;
var AW_AjaxCartProUpdaterObject = new AW_AjaxCartProUpdater('miniWishlist', ['.block-wishlist']);
Object.extend(AW_AjaxCartProUpdaterObject, {
    updateOnUpdateRequest: true,
    updateOnActionRequest: false,
    beforeUpdate: function(html) {
        return null;
    },
    afterUpdate: function(html, selectors) {
        return null;
    }
});
AW_AjaxCartPro.registerUpdater(AW_AjaxCartProUpdaterObject);
delete AW_AjaxCartProUpdaterObject;
var AW_AjaxCartProUpdaterObject = new AW_AjaxCartProUpdater('addProductConfirmation');
Object.extend(AW_AjaxCartProUpdaterObject, {
    updateOnUpdateRequest: true,
    updateOnActionRequest: false,
    beforeUpdate: function(html) {
        return null;
    },
    afterUpdate: function(html, selectors) {
        $('number_of_items').update($('cartHeader').innerHTML);
        $num = Number($('cartHeader').down().innerHTML);
        if ($num > 0 && $('cart-bottom-container') != undefined) {
            $('cart-bottom-container').hide();
        }
        return null;
    }
});
AW_AjaxCartPro.registerUpdater(AW_AjaxCartProUpdaterObject);
delete AW_AjaxCartProUpdaterObject;
var AW_AjaxCartProUpdaterObject = new AW_AjaxCartProUpdater('removeProductConfirmation');
Object.extend(AW_AjaxCartProUpdaterObject, {
    updateOnUpdateRequest: true,
    updateOnActionRequest: false,
    beforeUpdate: function(html) {
        return null;
    },
    afterUpdate: function(html, selectors) {
        if ($('sli_recommender_cart') != undefined) {
            $('sli_recommender_cart').hide();
        }
        return null;
    }
});
AW_AjaxCartPro.registerUpdater(AW_AjaxCartProUpdaterObject);
delete AW_AjaxCartProUpdaterObject;
document.observe("dom:loaded", function() {
    if (!isIE) {
        var AW_AjaxCartProObserverObject = new AW_AjaxCartProObserver('clickOnButtonInCartPageForm');
        Object.extend(AW_AjaxCartProObserverObject, {
            uiBlocks: ['progress'],
            _stopFns: [],
            run: function() {
                var me = this;
                var forms = this._getForms();
                forms.each(function(form) {
                    form.select('button[type=submit]').each(function(btn) {
                        var fn = me._observeFn.bind(btn);
                        btn.observe('click', fn);
                        me._stopFns.push(function() {
                            btn.stopObserving('click', fn);
                        });
                    })
                });
            },
            stop: function() {
                this._stopFns.each(function(fn) {
                    fn();
                });
                this._stopFns = [];
            },
            fireOriginal: function(url, parameters) {
                this.stop();
                if (this.lastClickedBtn) {
                    this.lastClickedBtn.click();
                }
            },
            _observeFn: function(event) {
                var me = AW_AjaxCartPro.config.actionsObservers.clickOnButtonInCartPageForm;
                var targetObj = this;
                if (!targetObj) {
                    return;
                }
                var action = targetObj.form.readAttribute('action') || '';
                var params = targetObj.form.serialize(true);
                if (typeof($(targetObj).getValue) === 'function') {
                    params[targetObj.getAttribute('name')] = $(targetObj).getValue();
                }
                me.lastClickedBtn = targetObj;
                me.fireCustom(action, params);
                event.stop();
            },
            _getForms: function() {
                if (typeof(this.forms) !== 'undefined') {
                    return this.forms;
                }
                var me = this;
                this.forms = [];
                $$('form').each(function(form) {
                    if (form.action.indexOf('checkout/cart/updatePost') !== -1) {
                        me.forms.push(form);
                    }
                });
                me._stopFns.push(function() {
                    delete me.forms;
                });
                return this.forms;
            }
        });
        AW_AjaxCartPro.registerObserver(AW_AjaxCartProObserverObject);
        delete AW_AjaxCartProObserverObject;
    }
});
var AW_AjaxCartProObserverObject = new AW_AjaxCartProObserver('clickOnAddToCartInCategoryList');
Object.extend(AW_AjaxCartProObserverObject, {
    uiBlocks: ['progress', 'options', 'add_confirmation'],
    _oldSetLocation: null,
    run: function() {
        this._oldSetLocation = setLocation;
        setLocation = this._observeFn.bind(this);
    },
    stop: function() {
        setLocation = this._oldSetLocation;
    },
    fireOriginal: function(url, parameters) {
        this._oldSetLocation(url);
    },
    _observeFn: function(url) {
        var mageVersion = AW_AjaxCartProConfig.data.mageVersion.split('.');
        var is14XAndLess = (mageVersion[0] < 2 && mageVersion[1] < 5);
        var is18X = (mageVersion[0] === "1" && mageVersion[1] === "8");
        if ((url.indexOf('.html') !== -1 && url.indexOf('.html?') === -1 && is18X) || (url.indexOf('options=cart') !== -1) || (url.indexOf('checkout/cart/add') !== -1) || ((url.indexOf('wishlist/index/cart') !== -1) && !is14XAndLess)) {
            this.fireCustom(url);
        } else {
            this.fireOriginal(url);
        }
    }
});
AW_AjaxCartPro.registerObserver(AW_AjaxCartProObserverObject);
delete AW_AjaxCartProObserverObject;
var AW_AjaxCartProObserverObject = new AW_AjaxCartProObserver('clickOnAddToCartInOptionsPopup');
Object.extend(AW_AjaxCartProObserverObject, {
    uiBlocks: ['progress', 'options', 'add_confirmation'],
    _oldSubmitFn: null,
    run: function() {
        var targetObj = this._getTargetObj();
        if (!targetObj) {
            return null;
        }
        this._oldSubmitFn = targetObj.form.submit;
        targetObj.form.submit = this._observeFn.bind(this);
        return null;
    },
    stop: function() {
        var targetObj = this._getTargetObj();
        if (!targetObj) {
            return;
        }
        targetObj.form.submit = this._oldSubmitFn;
    },
    fireOriginal: function(url, parameters) {
        var targetObj = this._getTargetObj();
        if (!targetObj) {
            return;
        }
        this.stop();
        targetObj.submit();
    },
    _observeFn: function() {
        var targetObj = this._getTargetObj();
        if (!targetObj) {
            return;
        }
        var action = targetObj.form.readAttribute('action') || '';
        var params = targetObj.form.serialize(true);
        this.fireCustom(action, params);
    },
    _getTargetObj: function() {
        var targetObj = false;
        if (typeof(productAddToCartFormAcp) != 'undefined') {
            targetObj = productAddToCartFormAcp;
        }
        if (!targetObj) {
            return false;
        }
        return targetObj;
    }
});
AW_AjaxCartPro.registerObserver(AW_AjaxCartProObserverObject);
delete AW_AjaxCartProObserverObject;
document.observe("dom:loaded", function() {
    if ($$('.amxnotif-block')[0] == undefined) {
        var AW_AjaxCartProObserverObject = new AW_AjaxCartProObserver('clickOnAddToCartInProductPage');
        Object.extend(AW_AjaxCartProObserverObject, {
            uiBlocks: ['progress', 'options', 'add_confirmation'],
            _oldSubmitFn: null,
            run: function() {
                var targetObj = this._getTargetObj();
                if (!targetObj) {
                    return null;
                }
                this._oldSubmitFn = targetObj.form.submit;
                targetObj.form.submit = this._observeFn.bind(this);
                targetObj.form.select('button').each(function(btn) {
                    btn.removeAttribute('disabled')
                });
                return null;
            },
            stop: function() {
                var targetObj = this._getTargetObj();
                if (!targetObj) {
                    return;
                }
                targetObj.form.submit = this._oldSubmitFn;
            },
            fireOriginal: function(url, parameters) {
                var targetObj = this._getTargetObj();
                if (!targetObj) {
                    return;
                }
                this.stop();
                targetObj.submit();
            },
            _observeFn: function() {
                var targetObj = this._getTargetObj();
                if (!targetObj) {
                    return;
                }
                if (targetObj.form.action.indexOf('wishlist/index/add') !== -1 || targetObj.form.action.indexOf('wishlist/index/updateItemOptions') !== -1 || targetObj.form.action.indexOf('checkout/cart/updateItemOptions') !== -1) {
                    this.stop();
                    targetObj.form.submit();
                    this.run();
                    return;
                }
                var action = targetObj.form.readAttribute('action') || '';
                var params = targetObj.form.serialize(true);
                this.fireCustom(action, params);
            },
            _getTargetObj: function() {
                var targetObj = false;
                if (typeof(productAddToCartFormOld) != 'undefined') {
                    targetObj = productAddToCartFormOld;
                } else if (typeof(productAddToCartForm) != 'undefined') {
                    targetObj = productAddToCartForm;
                }
                if (!targetObj) {
                    return false;
                }
                return targetObj;
            }
        });
        AW_AjaxCartPro.registerObserver(AW_AjaxCartProObserverObject);
        delete AW_AjaxCartProObserverObject;
    }
});
var AW_AjaxCartProObserverObject = new AW_AjaxCartProObserver('clickOnDeleteFromCart');
Object.extend(AW_AjaxCartProObserverObject, {
    uiBlocks: ['progress', 'remove_confirmation'],
    _stopFns: [],
    run: function() {
        var me = this;
        var links = this._getLinks();
        links.each(function(link) {
            var fn = me._observeFn.bind(link);
            link.observe('click', fn);
            link._onclick = link.onclick;
            link.onclick = function(e) {};
            link.removeAttribute('onclick');
            me._stopFns.push(function() {
                link.stopObserving('click', fn);
                link.onclick = link._onclick;
                link._onclick = function(e) {};
            });
        });
    },
    stop: function() {
        this._stopFns.each(function(fn) {
            fn();
        });
        this._stopFns = [];
    },
    fireOriginal: function(url, parameters) {
        this.stop();
        document.location.href = url;
    },
    _observeFn: function(event) {
        var me = AW_AjaxCartPro.config.actionsObservers.clickOnDeleteFromCart;
        var link = this;
        if (link._onclick) {
            if (!link._onclick()) {
                event.stop();
                return;
            }
        }
        var url = link.getAttribute('href');
        me.fireCustom(url);
        event.stop();
    },
    _getLinks: function() {
        if (typeof(this.links) !== 'undefined') {
            return this.links;
        }
        var me = this;
        this.links = [];
        $$('a').each(function(l) {
            if (l.href.indexOf('checkout/cart/delete') !== -1) {
                me.links.push(l);
            }
        });
        me._stopFns.push(function() {
            delete me.links;
        });
        return this.links;
    }
});
AW_AjaxCartPro.registerObserver(AW_AjaxCartProObserverObject);
delete AW_AjaxCartProObserverObject;
var AW_AjaxCartProObserverObject = new AW_AjaxCartProObserver('clickOnAddToCartInMiniWishlist');
Object.extend(AW_AjaxCartProObserverObject, {
    uiBlocks: ['progress', 'options', 'add_confirmation'],
    _stopFns: [],
    run: function() {
        var me = this;
        var links = this._getLinks();
        links.each(function(link) {
            var fn = me._observeFn.bind(me);
            link.observe('click', fn);
            me._stopFns.push(function() {
                link.stopObserving('click', fn);
            });
        });
    },
    stop: function() {
        this._stopFns.each(function(fn) {
            fn();
        });
        this._stopFns = [];
    },
    fireOriginal: function(url, parameters) {
        this.stop();
        document.location.href = url;
    },
    _observeFn: function(event) {
        var url = $(event.target).getAttribute('href');
        this.fireCustom(url);
        event.stop();
    },
    _getLinks: function() {
        var mageVersion = AW_AjaxCartProConfig.data.mageVersion.split('.');
        if (mageVersion[0] < 2 && mageVersion[1] < 5) {
            return [];
        }
        if (typeof(this.links) !== 'undefined') {
            return this.links;
        }
        var me = this;
        this.links = [];
        $$('a').each(function(l) {
            if (l.href.indexOf('wishlist/index/cart') !== -1) {
                me.links.push(l);
            }
        });
        me._stopFns.push(function() {
            delete me.links;
        });
        return this.links;
    }
});
AW_AjaxCartPro.registerObserver(AW_AjaxCartProObserverObject);
delete AW_AjaxCartProObserverObject;
if (typeof Product == 'undefined') {
    var Product = {};
}
Product.Zoom = Class.create();
Product.Zoom.prototype = {
    initialize: function(imageEl, trackEl, handleEl, zoomInEl, zoomOutEl, hintEl) {
        this.containerEl = $(imageEl).parentNode;
        this.imageEl = $(imageEl);
        this.handleEl = $(handleEl);
        this.trackEl = $(trackEl);
        this.hintEl = $(hintEl);
        this.containerDim = Element.getDimensions(this.containerEl);
        this.imageDim = Element.getDimensions(this.imageEl);
        this.imageDim.ratio = this.imageDim.width / this.imageDim.height;
        this.floorZoom = 1;
        if (this.imageDim.width > this.imageDim.height) {
            this.ceilingZoom = this.imageDim.width / this.containerDim.width;
        } else {
            this.ceilingZoom = this.imageDim.height / this.containerDim.height;
        }
        if (this.imageDim.width <= this.containerDim.width && this.imageDim.height <= this.containerDim.height) {
            this.trackEl.up().hide();
            this.hintEl.hide();
            this.containerEl.removeClassName('product-image-zoom');
            return;
        }
        this.imageX = 0;
        this.imageY = 0;
        this.imageZoom = 1;
        this.sliderSpeed = 0;
        this.sliderAccel = 0;
        this.zoomBtnPressed = false;
        this.showFull = false;
        this.selects = document.getElementsByTagName('select');
        this.draggable = new Draggable(imageEl, {
            starteffect: false,
            reverteffect: false,
            endeffect: false,
            snap: this.contain.bind(this)
        });
        this.slider = new Control.Slider(handleEl, trackEl, {
            axis: 'horizontal',
            minimum: 0,
            maximum: Element.getDimensions(this.trackEl).width,
            alignX: 0,
            increment: 1,
            sliderValue: 0,
            onSlide: this.scale.bind(this),
            onChange: this.scale.bind(this)
        });
        this.scale(0);
        Event.observe(this.imageEl, 'dblclick', this.toggleFull.bind(this));
        Event.observe($(zoomInEl), 'mousedown', this.startZoomIn.bind(this));
        Event.observe($(zoomInEl), 'mouseup', this.stopZooming.bind(this));
        Event.observe($(zoomInEl), 'mouseout', this.stopZooming.bind(this));
        Event.observe($(zoomOutEl), 'mousedown', this.startZoomOut.bind(this));
        Event.observe($(zoomOutEl), 'mouseup', this.stopZooming.bind(this));
        Event.observe($(zoomOutEl), 'mouseout', this.stopZooming.bind(this));
    },
    toggleFull: function() {
        this.showFull = !this.showFull;
        if (typeof document.body.style.maxHeight == "undefined") {
            for (i = 0; i < this.selects.length; i++) {
                this.selects[i].style.visibility = this.showFull ? 'hidden' : 'visible';
            }
        }
        val_scale = !this.showFull ? this.slider.value : 1;
        this.scale(val_scale);
        this.trackEl.style.visibility = this.showFull ? 'hidden' : 'visible';
        this.containerEl.style.overflow = this.showFull ? 'visible' : 'hidden';
        this.containerEl.style.zIndex = this.showFull ? '1000' : '9';
        return this;
    },
    scale: function(v) {
        var centerX = (this.containerDim.width * (1 - this.imageZoom) / 2 - this.imageX) / this.imageZoom;
        var centerY = (this.containerDim.height * (1 - this.imageZoom) / 2 - this.imageY) / this.imageZoom;
        var overSize = (this.imageDim.width > this.containerDim.width || this.imageDim.height > this.containerDim.height);
        this.imageZoom = this.floorZoom + (v * (this.ceilingZoom - this.floorZoom));
        if (overSize) {
            if (this.imageDim.width > this.imageDim.height) {
                this.imageEl.style.width = (this.imageZoom * this.containerDim.width) + 'px';
            } else {
                this.imageEl.style.height = (this.imageZoom * this.containerDim.height) + 'px';
            }
            if (this.containerDim.ratio) {
                if (this.imageDim.width > this.imageDim.height) {
                    this.imageEl.style.height = (this.imageZoom * this.containerDim.width * this.containerDim.ratio) + 'px';
                } else {
                    this.imageEl.style.width = (this.imageZoom * this.containerDim.height * this.containerDim.ratio) + 'px';
                }
            }
        } else {
            this.slider.setDisabled();
        }
        this.imageX = this.containerDim.width * (1 - this.imageZoom) / 2 - centerX * this.imageZoom;
        this.imageY = this.containerDim.height * (1 - this.imageZoom) / 2 - centerY * this.imageZoom;
        this.contain(this.imageX, this.imageY, this.draggable);
        return true;
    },
    startZoomIn: function() {
        if (!this.slider.disabled) {
            this.zoomBtnPressed = true;
            this.sliderAccel = .002;
            this.periodicalZoom();
            this.zoomer = new PeriodicalExecuter(this.periodicalZoom.bind(this), .05);
        }
        return this;
    },
    startZoomOut: function() {
        if (!this.slider.disabled) {
            this.zoomBtnPressed = true;
            this.sliderAccel = -.002;
            this.periodicalZoom();
            this.zoomer = new PeriodicalExecuter(this.periodicalZoom.bind(this), .05);
        }
        return this;
    },
    stopZooming: function() {
        if (!this.zoomer || this.sliderSpeed == 0) {
            return;
        }
        this.zoomBtnPressed = false;
        this.sliderAccel = 0;
    },
    periodicalZoom: function() {
        if (!this.zoomer) {
            return this;
        }
        if (this.zoomBtnPressed) {
            this.sliderSpeed += this.sliderAccel;
        } else {
            this.sliderSpeed /= 1.5;
            if (Math.abs(this.sliderSpeed) < .001) {
                this.sliderSpeed = 0;
                this.zoomer.stop();
                this.zoomer = null;
            }
        }
        this.slider.value += this.sliderSpeed;
        this.slider.setValue(this.slider.value);
        this.scale(this.slider.value);
        return this;
    },
    contain: function(x, y, draggable) {
        var dim = Element.getDimensions(draggable.element);
        var xMin = 0,
            xMax = this.containerDim.width - dim.width;
        var yMin = 0,
            yMax = this.containerDim.height - dim.height;
        x = x > xMin ? xMin : x;
        x = x < xMax ? xMax : x;
        y = y > yMin ? yMin : y;
        y = y < yMax ? yMax : y;
        if (this.containerDim.width > dim.width) {
            x = (this.containerDim.width / 2) - (dim.width / 2);
        }
        if (this.containerDim.height > dim.height) {
            y = (this.containerDim.height / 2) - (dim.height / 2);
        }
        this.imageX = x;
        this.imageY = y;
        this.imageEl.style.left = this.imageX + 'px';
        this.imageEl.style.top = this.imageY + 'px';
        return [x, y];
    }
}
Product.Config = Class.create();
Product.Config.prototype = {
    initialize: function(config) {
        this.config = config;
        this.taxConfig = this.config.taxConfig;
        this.settings = $$('.super-attribute-select');
        this.state = new Hash();
        this.priceTemplate = new Template(this.config.template);
        this.prices = config.prices;
        this.settings.each(function(element) {
            Event.observe(element, 'change', this.configure.bind(this))
        }.bind(this));
        this.settings.each(function(element) {
            var attributeId = element.id.replace(/[a-z]*/, '');
            if (attributeId && this.config.attributes[attributeId]) {
                element.config = this.config.attributes[attributeId];
                element.attributeId = attributeId;
                this.state[attributeId] = false;
            }
        }.bind(this))
        var childSettings = [];
        for (var i = this.settings.length - 1; i >= 0; i--) {
            var prevSetting = this.settings[i - 1] ? this.settings[i - 1] : false;
            var nextSetting = this.settings[i + 1] ? this.settings[i + 1] : false;
            if (i == 0) {
                this.fillSelect(this.settings[i])
            } else {
                this.settings[i].disabled = true;
            }
            $(this.settings[i]).childSettings = childSettings.clone();
            $(this.settings[i]).prevSetting = prevSetting;
            $(this.settings[i]).nextSetting = nextSetting;
            childSettings.push(this.settings[i]);
        }
        if (config.defaultValues) {
            this.values = config.defaultValues;
        }
        var separatorIndex = window.location.href.indexOf('#');
        if (separatorIndex != -1) {
            var paramsStr = window.location.href.substr(separatorIndex + 1);
            var urlValues = paramsStr.toQueryParams();
            if (!this.values) {
                this.values = {};
            }
            for (var i in urlValues) {
                this.values[i] = urlValues[i];
            }
        }
        this.configureForValues();
        document.observe("dom:loaded", this.configureForValues.bind(this));
    },
    configureForValues: function() {
        if (this.values) {
            this.settings.each(function(element) {
                var attributeId = element.attributeId;
                element.value = (typeof(this.values[attributeId]) == 'undefined') ? '' : this.values[attributeId];
                this.configureElement(element);
            }.bind(this));
        }
    },
    configure: function(event) {
        var element = Event.element(event);
        this.configureElement(element);
    },
    configureElement: function(element) {
        this.reloadOptionLabels(element);
        if (element.value) {
            this.state[element.config.id] = element.value;
            if (element.nextSetting) {
                element.nextSetting.disabled = false;
                this.fillSelect(element.nextSetting);
                this.resetChildren(element.nextSetting);
            }
        } else {
            this.resetChildren(element);
        }
        this.reloadPrice();
    },
    reloadOptionLabels: function(element) {
        var selectedPrice;
        if (element.options[element.selectedIndex].config) {
            selectedPrice = parseFloat(element.options[element.selectedIndex].config.price)
        } else {
            selectedPrice = 0;
        }
        for (var i = 0; i < element.options.length; i++) {
            if (element.options[i].config) {
                element.options[i].text = this.getOptionLabel(element.options[i].config, element.options[i].config.price - selectedPrice);
            }
        }
    },
    resetChildren: function(element) {
        if (element.childSettings) {
            for (var i = 0; i < element.childSettings.length; i++) {
                element.childSettings[i].selectedIndex = 0;
                element.childSettings[i].disabled = true;
                if (element.config) {
                    this.state[element.config.id] = false;
                }
            }
        }
    },
    fillSelect: function(element) {
        var attributeId = element.id.replace(/[a-z]*/, '');
        var options = this.getAttributeOptions(attributeId);
        this.clearSelect(element);
        element.options[0] = new Option('', '');
        element.options[0].innerHTML = this.config.chooseText;
        var prevConfig = false;
        if (element.prevSetting) {
            prevConfig = element.prevSetting.options[element.prevSetting.selectedIndex];
        }
        if (options) {
            var index = 1;
            for (var i = 0; i < options.length; i++) {
                var allowedProducts = [];
                if (prevConfig) {
                    for (var j = 0; j < options[i].products.length; j++) {
                        if (prevConfig.config.allowedProducts && prevConfig.config.allowedProducts.indexOf(options[i].products[j]) > -1) {
                            allowedProducts.push(options[i].products[j]);
                        }
                    }
                } else {
                    allowedProducts = options[i].products.clone();
                }
                if (allowedProducts.size() > 0) {
                    options[i].allowedProducts = allowedProducts;
                    element.options[index] = new Option(this.getOptionLabel(options[i], options[i].price), options[i].id);
                    element.options[index].config = options[i];
                    index++;
                }
            }
        }
    },
    getOptionLabel: function(option, price) {
        var price = parseFloat(price);
        if (this.taxConfig.includeTax) {
            var tax = price / (100 + this.taxConfig.defaultTax) * this.taxConfig.defaultTax;
            var excl = price - tax;
            var incl = excl * (1 + (this.taxConfig.currentTax / 100));
        } else {
            var tax = price * (this.taxConfig.currentTax / 100);
            var excl = price;
            var incl = excl + tax;
        }
        if (this.taxConfig.showIncludeTax || this.taxConfig.showBothPrices) {
            price = incl;
        } else {
            price = excl;
        }
        var str = option.label;
        if (price) {
            if (this.taxConfig.showBothPrices) {
                str += ' ' + this.formatPrice(excl, true) + ' (' + this.formatPrice(price, true) + ' ' + this.taxConfig.inclTaxTitle + ')';
            } else {
                str += ' ' + this.formatPrice(price, true);
            }
        }
        return str;
    },
    formatPrice: function(price, showSign) {
        var str = '';
        price = parseFloat(price);
        if (showSign) {
            if (price < 0) {
                str += '-';
                price = -price;
            } else {
                str += '+';
            }
        }
        var roundedPrice = (Math.round(price * 100) / 100).toString();
        if (this.prices && this.prices[roundedPrice]) {
            str += this.prices[roundedPrice];
        } else {
            str += this.priceTemplate.evaluate({
                price: price.toFixed(2)
            });
        }
        return str;
    },
    clearSelect: function(element) {
        for (var i = element.options.length - 1; i >= 0; i--) {
            element.remove(i);
        }
    },
    getAttributeOptions: function(attributeId) {
        if (this.config.attributes[attributeId]) {
            return this.config.attributes[attributeId].options;
        }
    },
    reloadPrice: function() {
        var price = 0;
        var oldPrice = 0;
        for (var i = this.settings.length - 1; i >= 0; i--) {
            var selected = this.settings[i].options[this.settings[i].selectedIndex];
            if (selected.config) {
                price += parseFloat(selected.config.price);
                oldPrice += parseFloat(selected.config.oldPrice);
            }
        }
        optionsPrice.changePrice('config', {
            'price': price,
            'oldPrice': oldPrice
        });
        optionsPrice.reload();
        return price;
        if ($('product-price-' + this.config.productId)) {
            $('product-price-' + this.config.productId).innerHTML = price;
        }
        this.reloadOldPrice();
    },
    reloadOldPrice: function() {
        if ($('old-price-' + this.config.productId)) {
            var price = parseFloat(this.config.oldPrice);
            for (var i = this.settings.length - 1; i >= 0; i--) {
                var selected = this.settings[i].options[this.settings[i].selectedIndex];
                if (selected.config) {
                    var parsedOldPrice = parseFloat(selected.config.oldPrice);
                    price += isNaN(parsedOldPrice) ? 0 : parsedOldPrice;
                }
            }
            if (price < 0)
                price = 0;
            price = this.formatPrice(price);
            if ($('old-price-' + this.config.productId)) {
                $('old-price-' + this.config.productId).innerHTML = price;
            }
        }
    }
}
Product.Super = {};
Product.Super.Configurable = Class.create();
Product.Super.Configurable.prototype = {
    initialize: function(container, observeCss, updateUrl, updatePriceUrl, priceContainerId) {
        this.container = $(container);
        this.observeCss = observeCss;
        this.updateUrl = updateUrl;
        this.updatePriceUrl = updatePriceUrl;
        this.priceContainerId = priceContainerId;
        this.registerObservers();
    },
    registerObservers: function() {
        var elements = this.container.getElementsByClassName(this.observeCss);
        elements.each(function(element) {
            Event.observe(element, 'change', this.update.bindAsEventListener(this));
        }.bind(this));
        return this;
    },
    update: function(event) {
        var elements = this.container.getElementsByClassName(this.observeCss);
        var parameters = Form.serializeElements(elements, true);
        new Ajax.Updater(this.container, this.updateUrl + '?ajax=1', {
            parameters: parameters,
            onComplete: this.registerObservers.bind(this)
        });
        var priceContainer = $(this.priceContainerId);
        if (priceContainer) {
            new Ajax.Updater(priceContainer, this.updatePriceUrl + '?ajax=1', {
                parameters: parameters
            });
        }
    }
}
Product.OptionsPrice = Class.create();
Product.OptionsPrice.prototype = {
    initialize: function(config) {
        this.productId = config.productId;
        this.priceFormat = config.priceFormat;
        this.includeTax = config.includeTax;
        this.defaultTax = config.defaultTax;
        this.currentTax = config.currentTax;
        this.productPrice = config.productPrice;
        this.showIncludeTax = config.showIncludeTax;
        this.showBothPrices = config.showBothPrices;
        this.productOldPrice = config.productOldPrice;
        this.priceInclTax = config.priceInclTax;
        this.priceExclTax = config.priceExclTax;
        this.skipCalculate = config.skipCalculate;
        this.duplicateIdSuffix = config.idSuffix;
        this.specialTaxPrice = config.specialTaxPrice;
        this.tierPrices = config.tierPrices;
        this.tierPricesInclTax = config.tierPricesInclTax;
        this.oldPlusDisposition = config.oldPlusDisposition;
        this.plusDisposition = config.plusDisposition;
        this.plusDispositionTax = config.plusDispositionTax;
        this.oldMinusDisposition = config.oldMinusDisposition;
        this.minusDisposition = config.minusDisposition;
        this.exclDisposition = config.exclDisposition;
        this.optionPrices = {};
        this.customPrices = {};
        this.containers = {};
        this.displayZeroPrice = true;
        this.initPrices();
    },
    setDuplicateIdSuffix: function(idSuffix) {
        this.duplicateIdSuffix = idSuffix;
    },
    initPrices: function() {
        this.containers[0] = 'product-price-' + this.productId;
        this.containers[1] = 'bundle-price-' + this.productId;
        this.containers[2] = 'price-including-tax-' + this.productId;
        this.containers[3] = 'price-excluding-tax-' + this.productId;
        this.containers[4] = 'old-price-' + this.productId;
    },
    changePrice: function(key, price) {
        this.optionPrices[key] = price;
    },
    addCustomPrices: function(key, price) {
        this.customPrices[key] = price;
    },
    getOptionPrices: function() {
        var price = 0;
        var nonTaxable = 0;
        var oldPrice = 0;
        var priceInclTax = 0;
        var currentTax = this.currentTax;
        $H(this.optionPrices).each(function(pair) {
            if ('undefined' != typeof(pair.value.price) && 'undefined' != typeof(pair.value.oldPrice)) {
                price += parseFloat(pair.value.price);
                oldPrice += parseFloat(pair.value.oldPrice);
            } else if (pair.key == 'nontaxable') {
                nonTaxable = pair.value;
            } else if (pair.key == 'priceInclTax') {
                priceInclTax += pair.value;
            } else if (pair.key == 'optionsPriceInclTax') {
                priceInclTax += pair.value * (100 + currentTax) / 100;
            } else {
                price += parseFloat(pair.value);
                oldPrice += parseFloat(pair.value);
            }
        });
        var result = [price, nonTaxable, oldPrice, priceInclTax];
        return result;
    },
    reload: function() {
        var price;
        var formattedPrice;
        var optionPrices = this.getOptionPrices();
        var nonTaxable = optionPrices[1];
        var optionOldPrice = optionPrices[2];
        var priceInclTax = optionPrices[3];
        optionPrices = optionPrices[0];
        $H(this.containers).each(function(pair) {
            var _productPrice;
            var _plusDisposition;
            var _minusDisposition;
            var _priceInclTax;
            if ($(pair.value)) {
                if (pair.value == 'old-price-' + this.productId && this.productOldPrice != this.productPrice) {
                    _productPrice = this.productOldPrice;
                    _plusDisposition = this.oldPlusDisposition;
                    _minusDisposition = this.oldMinusDisposition;
                } else {
                    _productPrice = this.productPrice;
                    _plusDisposition = this.plusDisposition;
                    _minusDisposition = this.minusDisposition;
                }
                _priceInclTax = priceInclTax;
                if (pair.value == 'old-price-' + this.productId && optionOldPrice !== undefined) {
                    price = optionOldPrice + parseFloat(_productPrice);
                } else if (this.specialTaxPrice == 'true' && this.priceInclTax !== undefined && this.priceExclTax !== undefined) {
                    price = optionPrices + parseFloat(this.priceExclTax);
                    _priceInclTax += this.priceInclTax;
                } else {
                    price = optionPrices + parseFloat(_productPrice);
                    _priceInclTax += parseFloat(_productPrice) * (100 + this.currentTax) / 100;
                }
                if (this.specialTaxPrice == 'true') {
                    var excl = price;
                    var incl = _priceInclTax;
                } else if (this.includeTax == 'true') {
                    var tax = price / (100 + this.defaultTax) * this.defaultTax;
                    var excl = price - tax;
                    var incl = excl * (1 + (this.currentTax / 100));
                } else {
                    var tax = price * (this.currentTax / 100);
                    var excl = price;
                    var incl = excl + tax;
                }
                var subPrice = 0;
                var subPriceincludeTax = 0;
                Object.values(this.customPrices).each(function(el) {
                    if (el.excludeTax && el.includeTax) {
                        subPrice += parseFloat(el.excludeTax);
                        subPriceincludeTax += parseFloat(el.includeTax);
                    } else {
                        subPrice += parseFloat(el.price);
                        subPriceincludeTax += parseFloat(el.price);
                    }
                });
                excl += subPrice;
                incl += subPriceincludeTax;
                if (typeof this.exclDisposition == 'undefined') {
                    excl += parseFloat(_plusDisposition);
                }
                incl += parseFloat(_plusDisposition) + parseFloat(this.plusDispositionTax);
                excl -= parseFloat(_minusDisposition);
                incl -= parseFloat(_minusDisposition);
                excl += parseFloat(nonTaxable);
                incl += parseFloat(nonTaxable);
                if (pair.value == 'price-including-tax-' + this.productId) {
                    price = incl;
                } else if (pair.value == 'price-excluding-tax-' + this.productId) {
                    price = excl;
                } else if (pair.value == 'old-price-' + this.productId) {
                    if (this.showIncludeTax || this.showBothPrices) {
                        price = incl;
                    } else {
                        price = excl;
                    }
                } else {
                    if (this.showIncludeTax) {
                        price = incl;
                    } else {
                        price = excl;
                    }
                }
                if (price < 0) price = 0;
                if (price > 0 || this.displayZeroPrice) {
                    formattedPrice = this.formatPrice(price);
                } else {
                    formattedPrice = '';
                }
                if ($(pair.value).select('.price')[0]) {
                    $(pair.value).select('.price')[0].innerHTML = formattedPrice;
                    if ($(pair.value + this.duplicateIdSuffix) && $(pair.value + this.duplicateIdSuffix).select('.price')[0]) {
                        $(pair.value + this.duplicateIdSuffix).select('.price')[0].innerHTML = formattedPrice;
                    }
                } else {
                    $(pair.value).innerHTML = formattedPrice;
                    if ($(pair.value + this.duplicateIdSuffix)) {
                        $(pair.value + this.duplicateIdSuffix).innerHTML = formattedPrice;
                    }
                }
            };
        }.bind(this));
        for (var i = 0; i < this.tierPrices.length; i++) {
            $$('.price.tier-' + i).each(function(el) {
                var price = this.tierPrices[i] + parseFloat(optionPrices);
                el.innerHTML = this.formatPrice(price);
            }, this);
            $$('.price.tier-' + i + '-incl-tax').each(function(el) {
                var price = this.tierPricesInclTax[i] + parseFloat(optionPrices);
                el.innerHTML = this.formatPrice(price);
            }, this);
            $$('.benefit').each(function(el) {
                var parsePrice = function(html) {
                    return parseFloat(/\d+\.?\d*/.exec(html));
                };
                var container = $(this.containers[3]) ? this.containers[3] : this.containers[0];
                var price = parsePrice($(container).innerHTML);
                var tierPrice = $$('.tier-price.tier-' + i + ' .price');
                tierPrice = tierPrice.length ? parsePrice(tierPrice[0].innerHTML, 10) : 0;
                var $percent = Selector.findChildElements(el, ['.percent.tier-' + i]);
                $percent.each(function(el) {
                    el.innerHTML = Math.ceil(100 - ((100 / price) * tierPrice));
                });
            }, this);
        }
    },
    formatPrice: function(price) {
        return formatCurrency(price, this.priceFormat);
    }
}
Product.ACPconfigurable = Class.create();
Product.ACPconfigurable.prototype = {
    config: {},
    initialize: function(config) {
        this.config = config;
        this.taxConfig = this.config.taxConfig;
        this.settings = $$('.super-attribute-select-acp');
        this.state = new Hash();
        this.priceTemplate = new Template(this.config.template);
        this.prices = config.prices;
        this.settings.each(function(element) {
            Event.observe(element, 'change', this.configure.bind(this))
        }.bind(this));
        this.settings.each(function(element) {
            var attributeId = element.id.replace(/[a-z]*/, '');
            if (attributeId && this.config.attributes[attributeId]) {
                element.config = this.config.attributes[attributeId];
                element.attributeId = attributeId;
                this.state[attributeId] = false;
            }
        }.bind(this));
        var childSettings = [];
        for (var i = this.settings.length - 1; i >= 0; i--) {
            var prevSetting = this.settings[i - 1] ? this.settings[i - 1] : false;
            var nextSetting = this.settings[i + 1] ? this.settings[i + 1] : false;
            if (i == 0) {
                this.fillSelect(this.settings[i])
            } else {
                this.settings[i].disabled = true;
            }
            $(this.settings[i]).childSettings = childSettings.clone();
            $(this.settings[i]).prevSetting = prevSetting;
            $(this.settings[i]).nextSetting = nextSetting;
            childSettings.push(this.settings[i]);
        }
        var separatorIndex = window.location.href.indexOf('#');
        if (separatorIndex != -1) {
            var paramsStr = window.location.href.substr(separatorIndex + 1);
            this.values = paramsStr.toQueryParams();
            this.settings.each(function(element) {
                var attributeId = element.attributeId;
                element.value = (typeof(this.values[attributeId]) == 'undefined') ? '' : this.values[attributeId];
                this.configureElement(element);
            }.bind(this));
        }
    },
    fillSelect: function(element) {
        var attributeId = element.id.replace(/[a-z]*/, '');
        var options = this.getAttributeOptions(attributeId);
        this.clearSelect(element);
        element.options[0] = new Option(this.config.chooseText, '');
        var prevConfig = false;
        if (element.prevSetting) {
            prevConfig = element.prevSetting.options[element.prevSetting.selectedIndex];
        }
        if (options) {
            var index = 1;
            for (var i = 0; i < options.length; i++) {
                var allowedProducts = [];
                if (prevConfig) {
                    for (var j = 0; j < options[i].products.length; j++) {
                        if (prevConfig.config.allowedProducts && prevConfig.config.allowedProducts.indexOf(options[i].products[j]) > -1) {
                            allowedProducts.push(options[i].products[j]);
                        }
                    }
                } else {
                    allowedProducts = options[i].products.clone();
                }
                if (allowedProducts.size() > 0) {
                    options[i].allowedProducts = allowedProducts;
                    element.options[index] = new Option(this.getOptionLabel(options[i], options[i].price), options[i].id);
                    element.options[index].config = options[i];
                    index++;
                }
            }
        }
    },
    configure: function(event) {
        var element = Event.element(event);
        this.configureElement(element);
    },
    configureElement: function(element) {
        this.reloadOptionLabels(element);
        if (element.value) {
            this.state[element.config.id] = element.value;
            if (element.nextSetting) {
                element.nextSetting.disabled = false;
                this.fillSelect(element.nextSetting);
                this.resetChildren(element.nextSetting);
            }
        } else {
            this.resetChildren(element);
        }
        this.reloadPrice();
    },
    getAttributeOptions: function(attributeId) {
        if (this.config.attributes[attributeId]) {
            return this.config.attributes[attributeId].options;
        }
    },
    clearSelect: function(element) {
        for (var i = element.options.length - 1; i >= 0; i--) {
            element.remove(i);
        }
    },
    getOptionLabel: function(option, price) {
        var price = parseFloat(price);
        if (this.taxConfig.includeTax) {
            var tax = price / (100 + this.taxConfig.defaultTax) * this.taxConfig.defaultTax;
            var excl = price - tax;
            var incl = excl * (1 + (this.taxConfig.currentTax / 100));
        } else {
            var tax = price * (this.taxConfig.currentTax / 100);
            var excl = price;
            var incl = excl + tax;
        }
        if (this.taxConfig.showIncludeTax || this.taxConfig.showBothPrices) {
            price = incl;
        } else {
            price = excl;
        }
        var str = option.label;
        if (price) {
            if (this.taxConfig.showBothPrices) {
                str += ' ' + this.formatPrice(excl, true) + ' (' + this.formatPrice(price, true) + ' ' + this.taxConfig.inclTaxTitle + ')';
            } else {
                str += ' ' + this.formatPrice(price, true);
            }
        }
        return str;
    },
    formatPrice: function(price, showSign) {
        var str = '';
        price = parseFloat(price);
        if (showSign) {
            if (price < 0) {
                str += '-';
                price = -price;
            } else {
                str += '+';
            }
        }
        var roundedPrice = (Math.round(price * 100) / 100).toString();
        if (this.prices && this.prices[roundedPrice]) {
            str += this.prices[roundedPrice];
        } else {
            str += this.priceTemplate.evaluate({
                price: price.toFixed(2)
            });
        }
        return str;
    },
    reloadPrice: function() {
        var price = 0;
        for (var i = this.settings.length - 1; i >= 0; i--) {
            var selected = this.settings[i].options[this.settings[i].selectedIndex];
            if (selected.config) {
                price += parseFloat(selected.config.price);
            }
        }
        optionsPrice.changePrice('configAcp', price);
        optionsPrice.reload();
        return price;
    },
    reloadOptionLabels: function(element) {
        var selectedPrice;
        if (element.options[element.selectedIndex].config) {
            selectedPrice = parseFloat(element.options[element.selectedIndex].config.price)
        } else {
            selectedPrice = 0;
        }
        for (var i = 0; i < element.options.length; i++) {
            if (element.options[i].config) {
                element.options[i].text = this.getOptionLabel(element.options[i].config, element.options[i].config.price - selectedPrice);
            }
        }
    },
    resetChildren: function(element) {
        if (element.childSettings) {
            for (var i = 0; i < element.childSettings.length; i++) {
                element.childSettings[i].selectedIndex = 0;
                element.childSettings[i].disabled = true;
                if (element.config) {
                    this.state[element.config.id] = false;
                }
            }
        }
    },
    reloadOldPrice: function() {
        if ($('old-price-' + this.config.productId)) {
            var price = parseFloat(this.config.oldPrice);
            for (var i = this.settings.length - 1; i >= 0; i--) {
                var selected = this.settings[i].options[this.settings[i].selectedIndex];
                if (selected.config) {
                    price += parseFloat(selected.config.price);
                }
            }
            if (price < 0)
                price = 0;
            price = this.formatPrice(price);
            if ($('old-price-' + this.config.productId)) {
                $('old-price-' + this.config.productId).innerHTML = price;
            }
        }
    }
};
Product.DownloadableAcp = Class.create();
Product.DownloadableAcp.prototype = {
    config: {},
    initialize: function(config) {
        this.config = config;
        this.reloadPrice();
    },
    reloadPrice: function() {
        var price = 0;
        var configNew = this.config;
        $$('.product-downloadable-link-acp').each(function(elm) {
            if (configNew[elm.value] && elm.checked) {
                price += parseFloat(configNew[elm.value]);
            }
        });
        try {
            var _displayZeroPrice = optionsPrice.displayZeroPrice;
            optionsPrice.displayZeroPrice = false;
            optionsPrice.changePrice('downloadableAcp', price);
            optionsPrice.reload();
            optionsPrice.displayZeroPrice = _displayZeroPrice;
        } catch (e) {}
    }
};

function validateDownloadableCallback(elmId, result) {
    var container = $('downloadable-links-list');
    if (result == 'failed') {
        container.removeClassName('validation-passed');
        container.addClassName('validation-failed');
    } else {
        container.removeClassName('validation-failed');
        container.addClassName('validation-passed');
    }
}
var Window = Class.create();
Window.keepMultiModalWindow = false;
Window.hasEffectLib = (typeof Effect != 'undefined');
Window.resizeEffectDuration = 0.4;
Window.prototype = {
    initialize: function() {
        var id;
        var optionIndex = 0;
        if (arguments.length > 0) {
            if (typeof arguments[0] == "string") {
                id = arguments[0];
                optionIndex = 1;
            } else
                id = arguments[0] ? arguments[0].id : null;
        }
        if (!id)
            id = "window_" + new Date().getTime();
        if ($(id))
            alert("Window " + id + " is already registered in the DOM! Make sure you use setDestroyOnClose() or destroyOnClose: true in the constructor");
        this.options = Object.extend({
            className: "dialog",
            windowClassName: null,
            blurClassName: null,
            minWidth: 100,
            minHeight: 20,
            resizable: true,
            closable: true,
            minimizable: true,
            maximizable: true,
            draggable: true,
            userData: null,
            showEffect: (Window.hasEffectLib ? Effect.Appear : Element.show),
            hideEffect: (Window.hasEffectLib ? Effect.Fade : Element.hide),
            showEffectOptions: {},
            hideEffectOptions: {},
            effectOptions: null,
            parent: document.body,
            title: "&nbsp;",
            url: null,
            onload: Prototype.emptyFunction,
            width: 200,
            height: 300,
            opacity: 1,
            recenterAuto: true,
            wiredDrag: false,
            closeOnEsc: true,
            closeCallback: null,
            destroyOnClose: false,
            gridX: 1,
            gridY: 1
        }, arguments[optionIndex] || {});
        if (this.options.blurClassName)
            this.options.focusClassName = this.options.className;
        if (typeof this.options.top == "undefined" && typeof this.options.bottom == "undefined")
            this.options.top = this._round(Math.random() * 500, this.options.gridY);
        if (typeof this.options.left == "undefined" && typeof this.options.right == "undefined")
            this.options.left = this._round(Math.random() * 500, this.options.gridX);
        if (this.options.effectOptions) {
            Object.extend(this.options.hideEffectOptions, this.options.effectOptions);
            Object.extend(this.options.showEffectOptions, this.options.effectOptions);
            if (this.options.showEffect == Element.Appear)
                this.options.showEffectOptions.to = this.options.opacity;
        }
        if (Window.hasEffectLib) {
            if (this.options.showEffect == Effect.Appear)
                this.options.showEffectOptions.to = this.options.opacity;
            if (this.options.hideEffect == Effect.Fade)
                this.options.hideEffectOptions.from = this.options.opacity;
        }
        if (this.options.hideEffect == Element.hide)
            this.options.hideEffect = function() {
                Element.hide(this.element);
                if (this.options.destroyOnClose) this.destroy();
            }.bind(this)
        if (this.options.parent != document.body)
            this.options.parent = $(this.options.parent);
        this.element = this._createWindow(id);
        this.element.win = this;
        this.eventMouseDown = this._initDrag.bindAsEventListener(this);
        this.eventMouseUp = this._endDrag.bindAsEventListener(this);
        this.eventMouseMove = this._updateDrag.bindAsEventListener(this);
        this.eventOnLoad = this._getWindowBorderSize.bindAsEventListener(this);
        this.eventMouseDownContent = this.toFront.bindAsEventListener(this);
        this.eventResize = this._recenter.bindAsEventListener(this);
        this.eventKeyUp = this._keyUp.bindAsEventListener(this);
        this.topbar = $(this.element.id + "_top");
        this.bottombar = $(this.element.id + "_bottom");
        this.content = $(this.element.id + "_content");
        Event.observe(this.topbar, "mousedown", this.eventMouseDown);
        Event.observe(this.bottombar, "mousedown", this.eventMouseDown);
        Event.observe(this.content, "mousedown", this.eventMouseDownContent);
        Event.observe(window, "load", this.eventOnLoad);
        Event.observe(window, "resize", this.eventResize);
        Event.observe(window, "scroll", this.eventResize);
        Event.observe(document, "keyup", this.eventKeyUp);
        Event.observe(this.options.parent, "scroll", this.eventResize);
        if (this.options.draggable) {
            var that = this;
            [this.topbar, this.topbar.up().previous(), this.topbar.up().next()].each(function(element) {
                element.observe("mousedown", that.eventMouseDown);
                element.addClassName("top_draggable");
            });
            [this.bottombar.up(), this.bottombar.up().previous(), this.bottombar.up().next()].each(function(element) {
                element.observe("mousedown", that.eventMouseDown);
                element.addClassName("bottom_draggable");
            });
        }
        if (this.options.resizable) {
            this.sizer = $(this.element.id + "_sizer");
            Event.observe(this.sizer, "mousedown", this.eventMouseDown);
        }
        this.useLeft = null;
        this.useTop = null;
        if (typeof this.options.left != "undefined") {
            this.element.setStyle({
                left: parseFloat(this.options.left) + 'px'
            });
            this.useLeft = true;
        } else {
            this.element.setStyle({
                right: parseFloat(this.options.right) + 'px'
            });
            this.useLeft = false;
        }
        if (typeof this.options.top != "undefined") {
            this.element.setStyle({
                top: parseFloat(this.options.top) + 'px'
            });
            this.useTop = true;
        } else {
            this.element.setStyle({
                bottom: parseFloat(this.options.bottom) + 'px'
            });
            this.useTop = false;
        }
        this.storedLocation = null;
        this.setOpacity(this.options.opacity);
        if (this.options.zIndex)
            this.setZIndex(this.options.zIndex)
        if (this.options.destroyOnClose)
            this.setDestroyOnClose(true);
        this._getWindowBorderSize();
        this.width = this.options.width;
        this.height = this.options.height;
        this.visible = false;
        this.constraint = false;
        this.constraintPad = {
            top: 0,
            left: 0,
            bottom: 0,
            right: 0
        };
        if (this.width && this.height)
            this.setSize(this.options.width, this.options.height);
        this.setTitle(this.options.title)
        Windows.register(this);
    },
    destroy: function() {
        this._notify("onDestroy");
        Event.stopObserving(this.topbar, "mousedown", this.eventMouseDown);
        Event.stopObserving(this.bottombar, "mousedown", this.eventMouseDown);
        Event.stopObserving(this.content, "mousedown", this.eventMouseDownContent);
        Event.stopObserving(window, "load", this.eventOnLoad);
        Event.stopObserving(window, "resize", this.eventResize);
        Event.stopObserving(window, "scroll", this.eventResize);
        Event.stopObserving(this.content, "load", this.options.onload);
        Event.stopObserving(document, "keyup", this.eventKeyUp);
        if (this._oldParent) {
            var content = this.getContent();
            var originalContent = null;
            for (var i = 0; i < content.childNodes.length; i++) {
                originalContent = content.childNodes[i];
                if (originalContent.nodeType == 1)
                    break;
                originalContent = null;
            }
            if (originalContent)
                this._oldParent.appendChild(originalContent);
            this._oldParent = null;
        }
        if (this.sizer)
            Event.stopObserving(this.sizer, "mousedown", this.eventMouseDown);
        if (this.options.url)
            this.content.src = null
        if (this.iefix)
            Element.remove(this.iefix);
        Element.remove(this.element);
        Windows.unregister(this);
    },
    setCloseCallback: function(callback) {
        this.options.closeCallback = callback;
    },
    getContent: function() {
        return this.content;
    },
    setContent: function(id, autoresize, autoposition) {
        var element = $(id);
        if (null == element) throw "Unable to find element '" + id + "' in DOM";
        this._oldParent = element.parentNode;
        var d = null;
        var p = null;
        if (autoresize)
            d = Element.getDimensions(element);
        if (autoposition)
            p = Position.cumulativeOffset(element);
        var content = this.getContent();
        this.setHTMLContent("");
        content = this.getContent();
        content.appendChild(element);
        element.show();
        if (autoresize)
            this.setSize(d.width, d.height);
        if (autoposition)
            this.setLocation(p[1] - this.heightN, p[0] - this.widthW);
    },
    setHTMLContent: function(html) {
        if (this.options.url) {
            this.content.src = null;
            this.options.url = null;
            var content = "<div id=\"" + this.getId() + "_content\" class=\"" + this.options.className + "_content\"> </div>";
            $(this.getId() + "_table_content").innerHTML = content;
            this.content = $(this.element.id + "_content");
        }
        this.getContent().innerHTML = html;
    },
    setAjaxContent: function(url, options, showCentered, showModal) {
        this.showFunction = showCentered ? "showCenter" : "show";
        this.showModal = showModal || false;
        options = options || {};
        this.setHTMLContent("");
        this.onComplete = options.onComplete;
        if (!this._onCompleteHandler)
            this._onCompleteHandler = this._setAjaxContent.bind(this);
        options.onComplete = this._onCompleteHandler;
        new Ajax.Request(url, options);
        options.onComplete = this.onComplete;
    },
    _setAjaxContent: function(originalRequest) {
        Element.update(this.getContent(), originalRequest.responseText);
        if (this.onComplete)
            this.onComplete(originalRequest);
        this.onComplete = null;
        this[this.showFunction](this.showModal)
    },
    setURL: function(url) {
        if (this.options.url)
            this.content.src = null;
        this.options.url = url;
        var content = "<iframe frameborder='0' name='" + this.getId() + "_content'  id='" + this.getId() + "_content' src='" + url + "' width='" + this.width + "' height='" + this.height + "'> </iframe>";
        $(this.getId() + "_table_content").innerHTML = content;
        this.content = $(this.element.id + "_content");
    },
    getURL: function() {
        return this.options.url ? this.options.url : null;
    },
    refresh: function() {
        if (this.options.url)
            $(this.element.getAttribute('id') + '_content').src = this.options.url;
    },
    setCookie: function(name, expires, path, domain, secure) {
        name = name || this.element.id;
        this.cookie = [name, expires, path, domain, secure];
        var value = WindowUtilities.getCookie(name)
        if (value) {
            var values = value.split(',');
            var x = values[0].split(':');
            var y = values[1].split(':');
            var w = parseFloat(values[2]),
                h = parseFloat(values[3]);
            var mini = values[4];
            var maxi = values[5];
            this.setSize(w, h);
            if (mini == "true")
                this.doMinimize = true;
            else if (maxi == "true")
                this.doMaximize = true;
            this.useLeft = x[0] == "l";
            this.useTop = y[0] == "t";
            this.element.setStyle(this.useLeft ? {
                left: x[1]
            } : {
                right: x[1]
            });
            this.element.setStyle(this.useTop ? {
                top: y[1]
            } : {
                bottom: y[1]
            });
        }
    },
    getId: function() {
        return this.element.id;
    },
    setDestroyOnClose: function() {
        this.options.destroyOnClose = true;
    },
    setConstraint: function(bool, padding) {
        this.constraint = bool;
        this.constraintPad = Object.extend(this.constraintPad, padding || {});
        if (this.useTop && this.useLeft)
            this.setLocation(parseFloat(this.element.style.top), parseFloat(this.element.style.left));
    },
    _initDrag: function(event) {
        if (Event.element(event) == this.sizer && this.isMinimized())
            return;
        if (Event.element(event) != this.sizer && this.isMaximized())
            return;
        if (Prototype.Browser.IE && this.heightN == 0)
            this._getWindowBorderSize();
        this.pointer = [this._round(Event.pointerX(event), this.options.gridX), this._round(Event.pointerY(event), this.options.gridY)];
        if (this.options.wiredDrag)
            this.currentDrag = this._createWiredElement();
        else
            this.currentDrag = this.element;
        if (Event.element(event) == this.sizer) {
            this.doResize = true;
            this.widthOrg = this.width;
            this.heightOrg = this.height;
            this.bottomOrg = parseFloat(this.element.getStyle('bottom'));
            this.rightOrg = parseFloat(this.element.getStyle('right'));
            this._notify("onStartResize");
        } else {
            this.doResize = false;
            var closeButton = $(this.getId() + '_close');
            if (closeButton && Position.within(closeButton, this.pointer[0], this.pointer[1])) {
                this.currentDrag = null;
                return;
            }
            this.toFront();
            if (!this.options.draggable)
                return;
            this._notify("onStartMove");
        }
        Event.observe(document, "mouseup", this.eventMouseUp, false);
        Event.observe(document, "mousemove", this.eventMouseMove, false);
        WindowUtilities.disableScreen('__invisible__', '__invisible__', this.overlayOpacity);
        document.body.ondrag = function() {
            return false;
        };
        document.body.onselectstart = function() {
            return false;
        };
        this.currentDrag.show();
        Event.stop(event);
    },
    _round: function(val, round) {
        return round == 1 ? val : val = Math.floor(val / round) * round;
    },
    _updateDrag: function(event) {
        var pointer = [this._round(Event.pointerX(event), this.options.gridX), this._round(Event.pointerY(event), this.options.gridY)];
        var dx = pointer[0] - this.pointer[0];
        var dy = pointer[1] - this.pointer[1];
        if (this.doResize) {
            var w = this.widthOrg + dx;
            var h = this.heightOrg + dy;
            dx = this.width - this.widthOrg
            dy = this.height - this.heightOrg
            if (this.useLeft)
                w = this._updateWidthConstraint(w)
            else
                this.currentDrag.setStyle({
                    right: (this.rightOrg - dx) + 'px'
                });
            if (this.useTop)
                h = this._updateHeightConstraint(h)
            else
                this.currentDrag.setStyle({
                    bottom: (this.bottomOrg - dy) + 'px'
                });
            this.setSize(w, h);
            this._notify("onResize");
        } else {
            this.pointer = pointer;
            if (this.useLeft) {
                var left = parseFloat(this.currentDrag.getStyle('left')) + dx;
                var newLeft = this._updateLeftConstraint(left);
                this.pointer[0] += newLeft - left;
                this.currentDrag.setStyle({
                    left: newLeft + 'px'
                });
            } else
                this.currentDrag.setStyle({
                    right: parseFloat(this.currentDrag.getStyle('right')) - dx + 'px'
                });
            if (this.useTop) {
                var top = parseFloat(this.currentDrag.getStyle('top')) + dy;
                var newTop = this._updateTopConstraint(top);
                this.pointer[1] += newTop - top;
                this.currentDrag.setStyle({
                    top: newTop + 'px'
                });
            } else
                this.currentDrag.setStyle({
                    bottom: parseFloat(this.currentDrag.getStyle('bottom')) - dy + 'px'
                });
            this._notify("onMove");
        }
        if (this.iefix)
            this._fixIEOverlapping();
        this._removeStoreLocation();
        Event.stop(event);
    },
    _endDrag: function(event) {
        WindowUtilities.enableScreen('__invisible__');
        if (this.doResize)
            this._notify("onEndResize");
        else
            this._notify("onEndMove");
        Event.stopObserving(document, "mouseup", this.eventMouseUp, false);
        Event.stopObserving(document, "mousemove", this.eventMouseMove, false);
        Event.stop(event);
        this._hideWiredElement();
        this._saveCookie()
        document.body.ondrag = null;
        document.body.onselectstart = null;
    },
    _updateLeftConstraint: function(left) {
        if (this.constraint && this.useLeft && this.useTop) {
            var width = this.options.parent == document.body ? WindowUtilities.getPageSize().windowWidth : this.options.parent.getDimensions().width;
            if (left < this.constraintPad.left)
                left = this.constraintPad.left;
            if (left + this.width + this.widthE + this.widthW > width - this.constraintPad.right)
                left = width - this.constraintPad.right - this.width - this.widthE - this.widthW;
        }
        return left;
    },
    _updateTopConstraint: function(top) {
        if (this.constraint && this.useLeft && this.useTop) {
            var height = this.options.parent == document.body ? WindowUtilities.getPageSize().windowHeight : this.options.parent.getDimensions().height;
            var h = this.height + this.heightN + this.heightS;
            if (top < this.constraintPad.top)
                top = this.constraintPad.top;
            if (top + h > height - this.constraintPad.bottom)
                top = height - this.constraintPad.bottom - h;
        }
        return top;
    },
    _updateWidthConstraint: function(w) {
        if (this.constraint && this.useLeft && this.useTop) {
            var width = this.options.parent == document.body ? WindowUtilities.getPageSize().windowWidth : this.options.parent.getDimensions().width;
            var left = parseFloat(this.element.getStyle("left"));
            if (left + w + this.widthE + this.widthW > width - this.constraintPad.right)
                w = width - this.constraintPad.right - left - this.widthE - this.widthW;
        }
        return w;
    },
    _updateHeightConstraint: function(h) {
        if (this.constraint && this.useLeft && this.useTop) {
            var height = this.options.parent == document.body ? WindowUtilities.getPageSize().windowHeight : this.options.parent.getDimensions().height;
            var top = parseFloat(this.element.getStyle("top"));
            if (top + h + this.heightN + this.heightS > height - this.constraintPad.bottom)
                h = height - this.constraintPad.bottom - top - this.heightN - this.heightS;
        }
        return h;
    },
    _createWindow: function(id) {
        var className = this.options.className;
        var win = document.createElement("div");
        win.setAttribute('id', id);
        win.className = "dialog";
        if (this.options.windowClassName) {
            win.className += ' ' + this.options.windowClassName;
        }
        var content;
        if (this.options.url)
            content = "<iframe frameborder=\"0\" name=\"" + id + "_content\"  id=\"" + id + "_content\" src=\"" + this.options.url + "\"> </iframe>";
        else
            content = "<div id=\"" + id + "_content\" class=\"" + className + "_content\"> </div>";
        var closeDiv = this.options.closable ? "<div class='" + className + "_close' id='" + id + "_close' onclick='Windows.close(\"" + id + "\", event)'> </div>" : "";
        var minDiv = this.options.minimizable ? "<div class='" + className + "_minimize' id='" + id + "_minimize' onclick='Windows.minimize(\"" + id + "\", event)'> </div>" : "";
        var maxDiv = this.options.maximizable ? "<div class='" + className + "_maximize' id='" + id + "_maximize' onclick='Windows.maximize(\"" + id + "\", event)'> </div>" : "";
        var seAttributes = this.options.resizable ? "class='" + className + "_sizer' id='" + id + "_sizer'" : "class='" + className + "_se'";
        var blank = "../themes/default/blank.gif";
        win.innerHTML = closeDiv + minDiv + maxDiv + "\
      <a href='#' id='" + id + "_focus_anchor'><!-- --></a>\
      <table id='" + id + "_row1' class=\"top table_window\">\
        <tr>\
          <td class='" + className + "_nw'></td>\
          <td class='" + className + "_n'><div id='" + id + "_top' class='" + className + "_title title_window'>" + this.options.title + "</div></td>\
          <td class='" + className + "_ne'></td>\
        </tr>\
      </table>\
      <table id='" + id + "_row2' class=\"mid table_window\">\
        <tr>\
          <td class='" + className + "_w'></td>\
            <td id='" + id + "_table_content' class='" + className + "_content' valign='top'>" + content + "</td>\
          <td class='" + className + "_e'></td>\
        </tr>\
      </table>\
        <table id='" + id + "_row3' class=\"bot table_window\">\
        <tr>\
          <td class='" + className + "_sw'></td>\
            <td class='" + className + "_s'><div id='" + id + "_bottom' class='status_bar'><span style='float:left; width:1px; height:1px'></span></div></td>\
            <td " + seAttributes + "></td>\
        </tr>\
      </table>\
    ";
        Element.hide(win);
        this.options.parent.insertBefore(win, this.options.parent.firstChild);
        Event.observe($(id + "_content"), "load", this.options.onload);
        return win;
    },
    changeClassName: function(newClassName) {
        var className = this.options.className;
        var id = this.getId();
        $A(["_close", "_minimize", "_maximize", "_sizer", "_content"]).each(function(value) {
            this._toggleClassName($(id + value), className + value, newClassName + value)
        }.bind(this));
        this._toggleClassName($(id + "_top"), className + "_title", newClassName + "_title");
        $$("#" + id + " td").each(function(td) {
            td.className = td.className.sub(className, newClassName);
        });
        this.options.className = newClassName;
    },
    _toggleClassName: function(element, oldClassName, newClassName) {
        if (element) {
            element.removeClassName(oldClassName);
            element.addClassName(newClassName);
        }
    },
    setLocation: function(top, left) {
        top = this._updateTopConstraint(top);
        left = this._updateLeftConstraint(left);
        var e = this.currentDrag || this.element;
        e.setStyle({
            top: top + 'px'
        });
        e.setStyle({
            left: left + 'px'
        });
        this.useLeft = true;
        this.useTop = true;
    },
    getLocation: function() {
        var location = {};
        if (this.useTop)
            location = Object.extend(location, {
                top: this.element.getStyle("top")
            });
        else
            location = Object.extend(location, {
                bottom: this.element.getStyle("bottom")
            });
        if (this.useLeft)
            location = Object.extend(location, {
                left: this.element.getStyle("left")
            });
        else
            location = Object.extend(location, {
                right: this.element.getStyle("right")
            });
        return location;
    },
    getSize: function() {
        return {
            width: this.width,
            height: this.height
        };
    },
    setSize: function(width, height, useEffect) {
        width = parseFloat(width);
        height = parseFloat(height);
        if (!this.minimized && width < this.options.minWidth)
            width = this.options.minWidth;
        if (!this.minimized && height < this.options.minHeight)
            height = this.options.minHeight;
        if (this.options.maxHeight && height > this.options.maxHeight)
            height = this.options.maxHeight;
        if (this.options.maxWidth && width > this.options.maxWidth)
            width = this.options.maxWidth;
        if (this.useTop && this.useLeft && Window.hasEffectLib && Effect.ResizeWindow && useEffect) {
            new Effect.ResizeWindow(this, null, null, width, height, {
                duration: Window.resizeEffectDuration
            });
        } else {
            this.width = width;
            this.height = height;
            var e = this.currentDrag ? this.currentDrag : this.element;
            e.setStyle({
                width: width + this.widthW + this.widthE + "px"
            })
            e.setStyle({
                height: height + this.heightN + this.heightS + "px"
            })
            if (!this.currentDrag || this.currentDrag == this.element) {
                var content = $(this.element.id + '_content');
            }
        }
    },
    updateHeight: function() {
        this.setSize(this.width, this.content.scrollHeight, true);
    },
    updateWidth: function() {
        this.setSize(this.content.scrollWidth, this.height, true);
    },
    toFront: function() {
        if (this.element.style.zIndex < Windows.maxZIndex)
            this.setZIndex(Windows.maxZIndex + 1);
        if (this.iefix)
            this._fixIEOverlapping();
    },
    getBounds: function(insideOnly) {
        if (!this.width || !this.height || !this.visible)
            this.computeBounds();
        var w = this.width;
        var h = this.height;
        if (!insideOnly) {
            w += this.widthW + this.widthE;
            h += this.heightN + this.heightS;
        }
        var bounds = Object.extend(this.getLocation(), {
            width: w + "px",
            height: h + "px"
        });
        return bounds;
    },
    computeBounds: function() {
        if (!this.width || !this.height) {
            var size = WindowUtilities._computeSize(this.content.innerHTML, this.content.id, this.width, this.height, 0, this.options.className)
            if (this.height)
                this.width = size + 5
            else
                this.height = size + 5
        }
        this.setSize(this.width, this.height);
        if (this.centered)
            this._center(this.centerTop, this.centerLeft);
    },
    show: function(modal) {
        this.visible = true;
        if (modal) {
            if (typeof this.overlayOpacity == "undefined") {
                var that = this;
                setTimeout(function() {
                    that.show(modal)
                }, 10);
                return;
            }
            Windows.addModalWindow(this);
            this.modal = true;
            this.setZIndex(Windows.maxZIndex + 1);
            Windows.unsetOverflow(this);
        } else
        if (!this.element.style.zIndex)
            this.setZIndex(Windows.maxZIndex + 1);
        if (this.oldStyle)
            this.getContent().setStyle({
                overflow: this.oldStyle
            });
        this.computeBounds();
        this._notify("onBeforeShow");
        if (this.options.showEffect != Element.show && this.options.showEffectOptions)
            this.options.showEffect(this.element, this.options.showEffectOptions);
        else
            this.options.showEffect(this.element);
        this._checkIEOverlapping();
        WindowUtilities.focusedWindow = this
        this._notify("onShow");
        $(this.element.id + '_focus_anchor').focus();
    },
    showCenter: function(modal, top, left) {
        this.centered = true;
        this.centerTop = top;
        this.centerLeft = left;
        this.show(modal);
    },
    isVisible: function() {
        return this.visible;
    },
    _center: function(top, left) {
        var windowScroll = WindowUtilities.getWindowScroll(this.options.parent);
        var pageSize = WindowUtilities.getPageSize(this.options.parent);
        if (typeof top == "undefined")
            top = (pageSize.windowHeight - (this.height + this.heightN + this.heightS)) / 2;
        top += windowScroll.top
        if (typeof left == "undefined")
            left = (pageSize.windowWidth - (this.width + this.widthW + this.widthE)) / 2;
        left += windowScroll.left
        this.setLocation(top, left);
        this.toFront();
    },
    _recenter: function(event) {
        if (this.centered) {
            var pageSize = WindowUtilities.getPageSize(this.options.parent);
            var windowScroll = WindowUtilities.getWindowScroll(this.options.parent);
            if (this.pageSize && this.pageSize.windowWidth == pageSize.windowWidth && this.pageSize.windowHeight == pageSize.windowHeight && this.windowScroll.left == windowScroll.left && this.windowScroll.top == windowScroll.top)
                return;
            this.pageSize = pageSize;
            this.windowScroll = windowScroll;
            if ($('overlay_modal'))
                $('overlay_modal').setStyle({
                    height: (pageSize.pageHeight + 'px')
                });
            if (this.options.recenterAuto)
                this._center(this.centerTop, this.centerLeft);
        }
    },
    hide: function() {
        this.visible = false;
        if (this.modal) {
            Windows.removeModalWindow(this);
            Windows.resetOverflow();
        }
        this.oldStyle = this.getContent().getStyle('overflow') || "auto"
        this.getContent().setStyle({
            overflow: "hidden"
        });
        this.options.hideEffect(this.element, this.options.hideEffectOptions);
        if (this.iefix)
            this.iefix.hide();
        if (!this.doNotNotifyHide)
            this._notify("onHide");
    },
    close: function() {
        if (this.visible) {
            if (this.options.closeCallback && !this.options.closeCallback(this))
                return;
            if (this.options.destroyOnClose) {
                var destroyFunc = this.destroy.bind(this);
                if (this.options.hideEffectOptions.afterFinish) {
                    var func = this.options.hideEffectOptions.afterFinish;
                    this.options.hideEffectOptions.afterFinish = function() {
                        func();
                        destroyFunc()
                    }
                } else
                    this.options.hideEffectOptions.afterFinish = function() {
                        destroyFunc()
                    }
            }
            Windows.updateFocusedWindow();
            this.doNotNotifyHide = true;
            this.hide();
            this.doNotNotifyHide = false;
            this._notify("onClose");
        }
    },
    minimize: function() {
        if (this.resizing)
            return;
        var r2 = $(this.getId() + "_row2");
        if (!this.minimized) {
            this.minimized = true;
            var dh = r2.getDimensions().height;
            this.r2Height = dh;
            var h = this.element.getHeight() - dh;
            if (this.useLeft && this.useTop && Window.hasEffectLib && Effect.ResizeWindow) {
                new Effect.ResizeWindow(this, null, null, null, this.height - dh, {
                    duration: Window.resizeEffectDuration
                });
            } else {
                this.height -= dh;
                this.element.setStyle({
                    height: h + "px"
                });
                r2.hide();
            }
            if (!this.useTop) {
                var bottom = parseFloat(this.element.getStyle('bottom'));
                this.element.setStyle({
                    bottom: (bottom + dh) + 'px'
                });
            }
        } else {
            this.minimized = false;
            var dh = this.r2Height;
            this.r2Height = null;
            if (this.useLeft && this.useTop && Window.hasEffectLib && Effect.ResizeWindow) {
                new Effect.ResizeWindow(this, null, null, null, this.height + dh, {
                    duration: Window.resizeEffectDuration
                });
            } else {
                var h = this.element.getHeight() + dh;
                this.height += dh;
                this.element.setStyle({
                    height: h + "px"
                })
                r2.show();
            }
            if (!this.useTop) {
                var bottom = parseFloat(this.element.getStyle('bottom'));
                this.element.setStyle({
                    bottom: (bottom - dh) + 'px'
                });
            }
            this.toFront();
        }
        this._notify("onMinimize");
        this._saveCookie()
    },
    maximize: function() {
        if (this.isMinimized() || this.resizing)
            return;
        if (Prototype.Browser.IE && this.heightN == 0)
            this._getWindowBorderSize();
        if (this.storedLocation != null) {
            this._restoreLocation();
            if (this.iefix)
                this.iefix.hide();
        } else {
            this._storeLocation();
            Windows.unsetOverflow(this);
            var windowScroll = WindowUtilities.getWindowScroll(this.options.parent);
            var pageSize = WindowUtilities.getPageSize(this.options.parent);
            var left = windowScroll.left;
            var top = windowScroll.top;
            if (this.options.parent != document.body) {
                windowScroll = {
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0
                };
                var dim = this.options.parent.getDimensions();
                pageSize.windowWidth = dim.width;
                pageSize.windowHeight = dim.height;
                top = 0;
                left = 0;
            }
            if (this.constraint) {
                pageSize.windowWidth -= Math.max(0, this.constraintPad.left) + Math.max(0, this.constraintPad.right);
                pageSize.windowHeight -= Math.max(0, this.constraintPad.top) + Math.max(0, this.constraintPad.bottom);
                left += Math.max(0, this.constraintPad.left);
                top += Math.max(0, this.constraintPad.top);
            }
            var width = pageSize.windowWidth - this.widthW - this.widthE;
            var height = pageSize.windowHeight - this.heightN - this.heightS;
            if (this.useLeft && this.useTop && Window.hasEffectLib && Effect.ResizeWindow) {
                new Effect.ResizeWindow(this, top, left, width, height, {
                    duration: Window.resizeEffectDuration
                });
            } else {
                this.setSize(width, height);
                this.element.setStyle(this.useLeft ? {
                    left: left
                } : {
                    right: left
                });
                this.element.setStyle(this.useTop ? {
                    top: top
                } : {
                    bottom: top
                });
            }
            this.toFront();
            if (this.iefix)
                this._fixIEOverlapping();
        }
        this._notify("onMaximize");
        this._saveCookie()
    },
    isMinimized: function() {
        return this.minimized;
    },
    isMaximized: function() {
        return (this.storedLocation != null);
    },
    setOpacity: function(opacity) {
        if (Element.setOpacity)
            Element.setOpacity(this.element, opacity);
    },
    setZIndex: function(zindex) {
        this.element.setStyle({
            zIndex: zindex
        });
        Windows.updateZindex(zindex, this);
    },
    setTitle: function(newTitle) {
        if (!newTitle || newTitle == "")
            newTitle = "&nbsp;";
        Element.update(this.element.id + '_top', newTitle);
    },
    getTitle: function() {
        return $(this.element.id + '_top').innerHTML;
    },
    setStatusBar: function(element) {
        var statusBar = $(this.getId() + "_bottom");
        if (typeof(element) == "object") {
            if (this.bottombar.firstChild)
                this.bottombar.replaceChild(element, this.bottombar.firstChild);
            else
                this.bottombar.appendChild(element);
        } else
            this.bottombar.innerHTML = element;
    },
    _checkIEOverlapping: function() {
        if (!this.iefix && (navigator.appVersion.indexOf('MSIE') > 0) && (navigator.userAgent.indexOf('Opera') < 0) && (this.element.getStyle('position') == 'absolute')) {
            new Insertion.After(this.element.id, '<iframe id="' + this.element.id + '_iefix" ' + 'style="display:none;position:absolute;filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);" ' + 'src="javascript:false;" frameborder="0" scrolling="no"></iframe>');
            this.iefix = $(this.element.id + '_iefix');
        }
        if (this.iefix)
            setTimeout(this._fixIEOverlapping.bind(this), 50);
    },
    _fixIEOverlapping: function() {
        Position.clone(this.element, this.iefix);
        this.iefix.style.zIndex = this.element.style.zIndex - 1;
        this.iefix.show();
    },
    _keyUp: function(event) {
        if (27 == event.keyCode && this.options.closeOnEsc) {
            this.close();
        }
    },
    _getWindowBorderSize: function(event) {
        var div = this._createHiddenDiv(this.options.className + "_n")
        this.heightN = Element.getDimensions(div).height;
        div.parentNode.removeChild(div)
        var div = this._createHiddenDiv(this.options.className + "_s")
        this.heightS = Element.getDimensions(div).height;
        div.parentNode.removeChild(div)
        var div = this._createHiddenDiv(this.options.className + "_e")
        this.widthE = Element.getDimensions(div).width;
        div.parentNode.removeChild(div)
        var div = this._createHiddenDiv(this.options.className + "_w")
        this.widthW = Element.getDimensions(div).width;
        div.parentNode.removeChild(div);
        var div = document.createElement("div");
        div.className = "overlay_" + this.options.className;
        document.body.appendChild(div);
        var that = this;
        setTimeout(function() {
            that.overlayOpacity = ($(div).getStyle("opacity"));
            div.parentNode.removeChild(div);
        }, 10);
        if (Prototype.Browser.IE) {
            this.heightS = $(this.getId() + "_row3").getDimensions().height;
            this.heightN = $(this.getId() + "_row1").getDimensions().height;
        }
        if (Prototype.Browser.WebKit && Prototype.Browser.WebKitVersion < 420)
            this.setSize(this.width, this.height);
        if (this.doMaximize)
            this.maximize();
        if (this.doMinimize)
            this.minimize();
    },
    _createHiddenDiv: function(className) {
        var objBody = document.body;
        var win = document.createElement("div");
        win.setAttribute('id', this.element.id + "_tmp");
        win.className = className;
        win.style.display = 'none';
        win.innerHTML = '';
        objBody.insertBefore(win, objBody.firstChild);
        return win;
    },
    _storeLocation: function() {
        if (this.storedLocation == null) {
            this.storedLocation = {
                useTop: this.useTop,
                useLeft: this.useLeft,
                top: this.element.getStyle('top'),
                bottom: this.element.getStyle('bottom'),
                left: this.element.getStyle('left'),
                right: this.element.getStyle('right'),
                width: this.width,
                height: this.height
            };
        }
    },
    _restoreLocation: function() {
        if (this.storedLocation != null) {
            this.useLeft = this.storedLocation.useLeft;
            this.useTop = this.storedLocation.useTop;
            if (this.useLeft && this.useTop && Window.hasEffectLib && Effect.ResizeWindow)
                new Effect.ResizeWindow(this, this.storedLocation.top, this.storedLocation.left, this.storedLocation.width, this.storedLocation.height, {
                    duration: Window.resizeEffectDuration
                });
            else {
                this.element.setStyle(this.useLeft ? {
                    left: this.storedLocation.left
                } : {
                    right: this.storedLocation.right
                });
                this.element.setStyle(this.useTop ? {
                    top: this.storedLocation.top
                } : {
                    bottom: this.storedLocation.bottom
                });
                this.setSize(this.storedLocation.width, this.storedLocation.height);
            }
            Windows.resetOverflow();
            this._removeStoreLocation();
        }
    },
    _removeStoreLocation: function() {
        this.storedLocation = null;
    },
    _saveCookie: function() {
        if (this.cookie) {
            var value = "";
            if (this.useLeft)
                value += "l:" + (this.storedLocation ? this.storedLocation.left : this.element.getStyle('left'))
            else
                value += "r:" + (this.storedLocation ? this.storedLocation.right : this.element.getStyle('right'))
            if (this.useTop)
                value += ",t:" + (this.storedLocation ? this.storedLocation.top : this.element.getStyle('top'))
            else
                value += ",b:" + (this.storedLocation ? this.storedLocation.bottom : this.element.getStyle('bottom'))
            value += "," + (this.storedLocation ? this.storedLocation.width : this.width);
            value += "," + (this.storedLocation ? this.storedLocation.height : this.height);
            value += "," + this.isMinimized();
            value += "," + this.isMaximized();
            WindowUtilities.setCookie(value, this.cookie)
        }
    },
    _createWiredElement: function() {
        if (!this.wiredElement) {
            if (Prototype.Browser.IE)
                this._getWindowBorderSize();
            var div = document.createElement("div");
            div.className = "wired_frame " + this.options.className + "_wired_frame";
            div.style.position = 'absolute';
            this.options.parent.insertBefore(div, this.options.parent.firstChild);
            this.wiredElement = $(div);
        }
        if (this.useLeft)
            this.wiredElement.setStyle({
                left: this.element.getStyle('left')
            });
        else
            this.wiredElement.setStyle({
                right: this.element.getStyle('right')
            });
        if (this.useTop)
            this.wiredElement.setStyle({
                top: this.element.getStyle('top')
            });
        else
            this.wiredElement.setStyle({
                bottom: this.element.getStyle('bottom')
            });
        var dim = this.element.getDimensions();
        this.wiredElement.setStyle({
            width: dim.width + "px",
            height: dim.height + "px"
        });
        this.wiredElement.setStyle({
            zIndex: Windows.maxZIndex + 30
        });
        return this.wiredElement;
    },
    _hideWiredElement: function() {
        if (!this.wiredElement || !this.currentDrag)
            return;
        if (this.currentDrag == this.element)
            this.currentDrag = null;
        else {
            if (this.useLeft)
                this.element.setStyle({
                    left: this.currentDrag.getStyle('left')
                });
            else
                this.element.setStyle({
                    right: this.currentDrag.getStyle('right')
                });
            if (this.useTop)
                this.element.setStyle({
                    top: this.currentDrag.getStyle('top')
                });
            else
                this.element.setStyle({
                    bottom: this.currentDrag.getStyle('bottom')
                });
            this.currentDrag.hide();
            this.currentDrag = null;
            if (this.doResize)
                this.setSize(this.width, this.height);
        }
    },
    _notify: function(eventName) {
        if (this.options[eventName])
            this.options[eventName](this);
        else
            Windows.notify(eventName, this);
    }
};
var Windows = {
    windows: [],
    modalWindows: [],
    observers: [],
    focusedWindow: null,
    maxZIndex: 0,
    overlayShowEffectOptions: {
        duration: 0.5
    },
    overlayHideEffectOptions: {
        duration: 0.5
    },
    addObserver: function(observer) {
        this.removeObserver(observer);
        this.observers.push(observer);
    },
    removeObserver: function(observer) {
        this.observers = this.observers.reject(function(o) {
            return o == observer
        });
    },
    notify: function(eventName, win) {
        this.observers.each(function(o) {
            if (o[eventName]) o[eventName](eventName, win);
        });
    },
    getWindow: function(id) {
        return this.windows.detect(function(d) {
            return d.getId() == id
        });
    },
    getFocusedWindow: function() {
        return this.focusedWindow;
    },
    updateFocusedWindow: function() {
        this.focusedWindow = this.windows.length >= 2 ? this.windows[this.windows.length - 2] : null;
    },
    register: function(win) {
        this.windows.push(win);
    },
    addModalWindow: function(win) {
        if (this.modalWindows.length == 0) {
            WindowUtilities.disableScreen(win.options.className, 'overlay_modal', win.overlayOpacity, win.getId(), win.options.parent);
        } else {
            if (Window.keepMultiModalWindow) {
                $('overlay_modal').style.zIndex = Windows.maxZIndex + 1;
                Windows.maxZIndex += 1;
                WindowUtilities._hideSelect(this.modalWindows.last().getId());
            } else
                this.modalWindows.last().element.hide();
            WindowUtilities._showSelect(win.getId());
        }
        this.modalWindows.push(win);
    },
    removeModalWindow: function(win) {
        this.modalWindows.pop();
        if (this.modalWindows.length == 0)
            WindowUtilities.enableScreen();
        else {
            if (Window.keepMultiModalWindow) {
                this.modalWindows.last().toFront();
                WindowUtilities._showSelect(this.modalWindows.last().getId());
            } else
                this.modalWindows.last().element.show();
        }
    },
    register: function(win) {
        this.windows.push(win);
    },
    unregister: function(win) {
        this.windows = this.windows.reject(function(d) {
            return d == win
        });
    },
    closeAll: function() {
        this.windows.each(function(w) {
            Windows.close(w.getId())
        });
    },
    closeAllModalWindows: function() {
        WindowUtilities.enableScreen();
        this.modalWindows.each(function(win) {
            if (win) win.close()
        });
    },
    minimize: function(id, event) {
        var win = this.getWindow(id)
        if (win && win.visible)
            win.minimize();
        Event.stop(event);
    },
    maximize: function(id, event) {
        var win = this.getWindow(id)
        if (win && win.visible)
            win.maximize();
        Event.stop(event);
    },
    close: function(id, event) {
        var win = this.getWindow(id);
        if (win)
            win.close();
        if (event)
            Event.stop(event);
    },
    blur: function(id) {
        var win = this.getWindow(id);
        if (!win)
            return;
        if (win.options.blurClassName)
            win.changeClassName(win.options.blurClassName);
        if (this.focusedWindow == win)
            this.focusedWindow = null;
        win._notify("onBlur");
    },
    focus: function(id) {
        var win = this.getWindow(id);
        if (!win)
            return;
        if (this.focusedWindow)
            this.blur(this.focusedWindow.getId())
        if (win.options.focusClassName)
            win.changeClassName(win.options.focusClassName);
        this.focusedWindow = win;
        win._notify("onFocus");
    },
    unsetOverflow: function(except) {
        this.windows.each(function(d) {
            d.oldOverflow = d.getContent().getStyle("overflow") || "auto";
            d.getContent().setStyle({
                overflow: "hidden"
            })
        });
        if (except && except.oldOverflow)
            except.getContent().setStyle({
                overflow: except.oldOverflow
            });
    },
    resetOverflow: function() {
        this.windows.each(function(d) {
            if (d.oldOverflow) d.getContent().setStyle({
                overflow: d.oldOverflow
            })
        });
    },
    updateZindex: function(zindex, win) {
        if (zindex > this.maxZIndex) {
            this.maxZIndex = zindex;
            if (this.focusedWindow)
                this.blur(this.focusedWindow.getId())
        }
        this.focusedWindow = win;
        if (this.focusedWindow)
            this.focus(this.focusedWindow.getId())
    }
};
var Dialog = {
    dialogId: null,
    onCompleteFunc: null,
    callFunc: null,
    parameters: null,
    confirm: function(content, parameters) {
        if (content && typeof content != "string") {
            Dialog._runAjaxRequest(content, parameters, Dialog.confirm);
            return
        }
        content = content || "";
        parameters = parameters || {};
        var okLabel = parameters.okLabel ? parameters.okLabel : "Ok";
        var cancelLabel = parameters.cancelLabel ? parameters.cancelLabel : "Cancel";
        parameters = Object.extend(parameters, parameters.windowParameters || {});
        parameters.windowParameters = parameters.windowParameters || {};
        parameters.className = parameters.className || "alert";
        var okButtonClass = "class ='" + (parameters.buttonClass ? parameters.buttonClass + " " : "") + " ok_button'"
        var cancelButtonClass = "class ='" + (parameters.buttonClass ? parameters.buttonClass + " " : "") + " cancel_button'"
        var content = "\
      <div class='" + parameters.className + "_message'>" + content + "</div>\
        <div class='" + parameters.className + "_buttons'>\
          <button type='button' title='" + okLabel + "' onclick='Dialog.okCallback()' " + okButtonClass + "><span><span><span>" + okLabel + "</span></span></span></button>\
          <button type='button' title='" + cancelLabel + "' onclick='Dialog.cancelCallback()' " + cancelButtonClass + "><span><span><span>" + cancelLabel + "</span></span></span></button>\
        </div>\
    ";
        return this._openDialog(content, parameters)
    },
    alert: function(content, parameters) {
        if (content && typeof content != "string") {
            Dialog._runAjaxRequest(content, parameters, Dialog.alert);
            return
        }
        content = content || "";
        parameters = parameters || {};
        var okLabel = parameters.okLabel ? parameters.okLabel : "Ok";
        parameters = Object.extend(parameters, parameters.windowParameters || {});
        parameters.windowParameters = parameters.windowParameters || {};
        parameters.className = parameters.className || "alert";
        var okButtonClass = "class ='" + (parameters.buttonClass ? parameters.buttonClass + " " : "") + " ok_button'"
        var content = "\
      <div class='" + parameters.className + "_message'>" + content + "</div>\
        <div class='" + parameters.className + "_buttons'>\
          <button type='button' title='" + okLabel + "' onclick='Dialog.okCallback()' " + okButtonClass + "><span><span><span>" + okLabel + "</span></span></span></button>\
        </div>";
        return this._openDialog(content, parameters)
    },
    info: function(content, parameters) {
        if (content && typeof content != "string") {
            Dialog._runAjaxRequest(content, parameters, Dialog.info);
            return
        }
        content = content || "";
        parameters = parameters || {};
        parameters = Object.extend(parameters, parameters.windowParameters || {});
        parameters.windowParameters = parameters.windowParameters || {};
        parameters.className = parameters.className || "alert";
        var content = "<div id='modal_dialog_message' class='" + parameters.className + "_message'>" + content + "</div>";
        if (parameters.showProgress)
            content += "<div id='modal_dialog_progress' class='" + parameters.className + "_progress'>  </div>";
        parameters.ok = null;
        parameters.cancel = null;
        return this._openDialog(content, parameters)
    },
    setInfoMessage: function(message) {
        $('modal_dialog_message').update(message);
    },
    closeInfo: function() {
        Windows.close(this.dialogId);
    },
    _openDialog: function(content, parameters) {
        var className = parameters.className;
        if (!parameters.height && !parameters.width) {
            parameters.width = WindowUtilities.getPageSize(parameters.options.parent || document.body).pageWidth / 2;
        }
        if (parameters.id)
            this.dialogId = parameters.id;
        else {
            var t = new Date();
            this.dialogId = 'modal_dialog_' + t.getTime();
            parameters.id = this.dialogId;
        }
        if (!parameters.height || !parameters.width) {
            var size = WindowUtilities._computeSize(content, this.dialogId, parameters.width, parameters.height, 5, className)
            if (parameters.height)
                parameters.width = size + 5
            else
                parameters.height = size + 5
        }
        parameters.effectOptions = parameters.effectOptions;
        parameters.resizable = parameters.resizable || false;
        parameters.minimizable = parameters.minimizable || false;
        parameters.maximizable = parameters.maximizable || false;
        parameters.draggable = parameters.draggable || false;
        parameters.closable = parameters.closable || false;
        var win = new Window(parameters);
        win.getContent().innerHTML = content;
        win.showCenter(true, parameters.top, parameters.left);
        win.setDestroyOnClose();
        win.cancelCallback = parameters.onCancel || parameters.cancel;
        win.okCallback = parameters.onOk || parameters.ok;
        return win;
    },
    _getAjaxContent: function(originalRequest) {
        Dialog.callFunc(originalRequest.responseText, Dialog.parameters)
    },
    _runAjaxRequest: function(message, parameters, callFunc) {
        if (message.options == null)
            message.options = {}
        Dialog.onCompleteFunc = message.options.onComplete;
        Dialog.parameters = parameters;
        Dialog.callFunc = callFunc;
        message.options.onComplete = Dialog._getAjaxContent;
        new Ajax.Request(message.url, message.options);
    },
    okCallback: function() {
        var win = Windows.focusedWindow;
        if (!win.okCallback || win.okCallback(win)) {
            $$("#" + win.getId() + " input").each(function(element) {
                element.onclick = null;
            })
            win.close();
        }
    },
    cancelCallback: function() {
        var win = Windows.focusedWindow;
        $$("#" + win.getId() + " input").each(function(element) {
            element.onclick = null
        })
        win.close();
        if (win.cancelCallback)
            win.cancelCallback(win);
    }
}
if (Prototype.Browser.WebKit) {
    var array = navigator.userAgent.match(new RegExp(/AppleWebKit\/([\d\.\+]*)/));
    Prototype.Browser.WebKitVersion = parseFloat(array[1]);
}
var WindowUtilities = {
    getWindowScroll: function(parent) {
        var T, L, W, H;
        parent = parent || document.body;
        if (parent != document.body) {
            T = parent.scrollTop;
            L = parent.scrollLeft;
            W = parent.scrollWidth;
            H = parent.scrollHeight;
        } else {
            var w = window;
            with(w.document) {
                if (w.document.documentElement && documentElement.scrollTop) {
                    T = documentElement.scrollTop;
                    L = documentElement.scrollLeft;
                } else if (w.document.body) {
                    T = body.scrollTop;
                    L = body.scrollLeft;
                }
                if (w.innerWidth) {
                    W = w.innerWidth;
                    H = w.innerHeight;
                } else if (w.document.documentElement && documentElement.clientWidth) {
                    W = documentElement.clientWidth;
                    H = documentElement.clientHeight;
                } else {
                    W = body.offsetWidth;
                    H = body.offsetHeight
                }
            }
        }
        return {
            top: T,
            left: L,
            width: W,
            height: H
        };
    },
    getPageSize: function(parent) {
        parent = parent || document.body;
        var windowWidth, windowHeight;
        var pageHeight, pageWidth;
        if (parent != document.body) {
            windowWidth = parent.getWidth();
            windowHeight = parent.getHeight();
            pageWidth = parent.scrollWidth;
            pageHeight = parent.scrollHeight;
        } else {
            var xScroll, yScroll;
            if (window.innerHeight && window.scrollMaxY) {
                xScroll = document.body.scrollWidth;
                yScroll = window.innerHeight + window.scrollMaxY;
            } else if (document.body.scrollHeight > document.body.offsetHeight) {
                xScroll = document.body.scrollWidth;
                yScroll = document.body.scrollHeight;
            } else {
                xScroll = document.body.offsetWidth;
                yScroll = document.body.offsetHeight;
            }
            if (self.innerHeight) {
                windowWidth = document.documentElement.clientWidth;
                windowHeight = self.innerHeight;
            } else if (document.documentElement && document.documentElement.clientHeight) {
                windowWidth = document.documentElement.clientWidth;
                windowHeight = document.documentElement.clientHeight;
            } else if (document.body) {
                windowWidth = document.body.clientWidth;
                windowHeight = document.body.clientHeight;
            }
            if (yScroll < windowHeight) {
                pageHeight = windowHeight;
            } else {
                pageHeight = yScroll;
            }
            if (xScroll < windowWidth) {
                pageWidth = windowWidth;
            } else {
                pageWidth = xScroll;
            }
        }
        return {
            pageWidth: pageWidth,
            pageHeight: pageHeight,
            windowWidth: windowWidth,
            windowHeight: windowHeight
        };
    },
    disableScreen: function(className, overlayId, overlayOpacity, contentId, parent) {
        WindowUtilities.initLightbox(overlayId, className, function() {
            this._disableScreen(className, overlayId, overlayOpacity, contentId)
        }.bind(this), parent || document.body);
    },
    _disableScreen: function(className, overlayId, overlayOpacity, contentId) {
        var objOverlay = $(overlayId);
        var pageSize = WindowUtilities.getPageSize(objOverlay.parentNode);
        if (contentId && Prototype.Browser.IE) {
            WindowUtilities._hideSelect();
            WindowUtilities._showSelect(contentId);
        }
        objOverlay.style.height = (pageSize.pageHeight + 'px');
        objOverlay.style.display = 'none';
        if (overlayId == "overlay_modal" && Window.hasEffectLib && Windows.overlayShowEffectOptions) {
            objOverlay.overlayOpacity = overlayOpacity;
            new Effect.Appear(objOverlay, Object.extend({
                from: 0,
                to: overlayOpacity
            }, Windows.overlayShowEffectOptions));
        } else
            objOverlay.style.display = "block";
    },
    enableScreen: function(id) {
        id = id || 'overlay_modal';
        var objOverlay = $(id);
        if (objOverlay) {
            if (id == "overlay_modal" && Window.hasEffectLib && Windows.overlayHideEffectOptions)
                new Effect.Fade(objOverlay, Object.extend({
                    from: objOverlay.overlayOpacity,
                    to: 0
                }, Windows.overlayHideEffectOptions));
            else {
                objOverlay.style.display = 'none';
                objOverlay.parentNode.removeChild(objOverlay);
            }
            if (id != "__invisible__")
                WindowUtilities._showSelect();
        }
    },
    _hideSelect: function(id) {
        if (Prototype.Browser.IE) {
            id = id == null ? "" : "#" + id + " ";
            $$(id + 'select').each(function(element) {
                if (!WindowUtilities.isDefined(element.oldVisibility)) {
                    element.oldVisibility = element.style.visibility ? element.style.visibility : "visible";
                    element.style.visibility = "hidden";
                }
            });
        }
    },
    _showSelect: function(id) {
        if (Prototype.Browser.IE) {
            id = id == null ? "" : "#" + id + " ";
            $$(id + 'select').each(function(element) {
                if (WindowUtilities.isDefined(element.oldVisibility)) {
                    try {
                        element.style.visibility = element.oldVisibility;
                    } catch (e) {
                        element.style.visibility = "visible";
                    }
                    element.oldVisibility = null;
                } else {
                    if (element.style.visibility)
                        element.style.visibility = "visible";
                }
            });
        }
    },
    isDefined: function(object) {
        return typeof(object) != "undefined" && object != null;
    },
    initLightbox: function(id, className, doneHandler, parent) {
        if ($(id)) {
            Element.setStyle(id, {
                zIndex: Windows.maxZIndex + 1
            });
            Windows.maxZIndex++;
            doneHandler();
        } else {
            var objOverlay = document.createElement("div");
            objOverlay.setAttribute('id', id);
            objOverlay.className = "overlay_" + className
            objOverlay.style.display = 'none';
            objOverlay.style.position = 'absolute';
            objOverlay.style.top = '0';
            objOverlay.style.left = '0';
            objOverlay.style.zIndex = Windows.maxZIndex + 1;
            Windows.maxZIndex++;
            objOverlay.style.width = '100%';
            parent.insertBefore(objOverlay, parent.firstChild);
            if (Prototype.Browser.WebKit && id == "overlay_modal") {
                setTimeout(function() {
                    doneHandler()
                }, 10);
            } else
                doneHandler();
        }
    },
    setCookie: function(value, parameters) {
        document.cookie = parameters[0] + "=" + escape(value) +
            ((parameters[1]) ? "; expires=" + parameters[1].toGMTString() : "") +
            ((parameters[2]) ? "; path=" + parameters[2] : "") +
            ((parameters[3]) ? "; domain=" + parameters[3] : "") +
            ((parameters[4]) ? "; secure" : "");
    },
    getCookie: function(name) {
        var dc = document.cookie;
        var prefix = name + "=";
        var begin = dc.indexOf("; " + prefix);
        if (begin == -1) {
            begin = dc.indexOf(prefix);
            if (begin != 0) return null;
        } else {
            begin += 2;
        }
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
            end = dc.length;
        }
        return unescape(dc.substring(begin + prefix.length, end));
    },
    _computeSize: function(content, id, width, height, margin, className) {
        var objBody = document.body;
        var tmpObj = document.createElement("div");
        tmpObj.setAttribute('id', id);
        tmpObj.className = className + "_content";
        if (height)
            tmpObj.style.height = height + "px"
        else
            tmpObj.style.width = width + "px"
        tmpObj.style.position = 'absolute';
        tmpObj.style.top = '0';
        tmpObj.style.left = '0';
        tmpObj.style.display = 'none';
        tmpObj.innerHTML = content;
        objBody.insertBefore(tmpObj, objBody.firstChild);
        var size;
        if (height)
            size = $(tmpObj).getDimensions().width + margin;
        else
            size = $(tmpObj).getDimensions().height + margin;
        objBody.removeChild(tmpObj);
        return size;
    }
}
Validation.defaultOptions.immediate = true;
Validation.defaultOptions.addClassNameToContainer = true;
Event.observe(document, 'dom:loaded', function() {
    var inputs = $$('ul.options-list input');
    for (var i = 0, l = inputs.length; i < l; i++) {
        inputs[i].addClassName('change-container-classname');
    }
})
if (!window.Enterprise) {
    window.Enterprise = {};
}
Enterprise.templatesPattern = /(^|.|\r|\n)(\{\{(.*?)\}\})/;
Enterprise.TopCart = {
    initialize: function(container) {
        this.container = $(container);
        this.element = this.container.up(0);
        this.elementHeader = this.container.previous(0);
        this.intervalDuration = 4000;
        this.interval = null;
        this.onElementMouseOut = this.handleMouseOut.bindAsEventListener(this);
        this.onElementMouseOver = this.handleMouseOver.bindAsEventListener(this);
        this.onElementMouseClick = this.handleMouseClick.bindAsEventListener(this);
        this.element.observe('mouseout', this.onElementMouseOut);
        this.element.observe('mouseover', this.onElementMouseOver);
        this.elementHeader.observe('click', this.onElementMouseClick);
    },
    handleMouseOut: function(evt) {
        if ($(this.elementHeader).hasClassName('expanded')) {
            this.interval = setTimeout(this.hideCart.bind(this), this.intervalDuration);
        }
    },
    handleMouseOver: function(evt) {
        if (this.interval !== null) {
            clearTimeout(this.interval);
            this.interval = null;
        }
    },
    handleMouseClick: function(evt) {
        if (!$(this.elementHeader).hasClassName('expanded') && !$(this.container.id).hasClassName('process')) {
            this.showCart();
        } else {
            this.hideCart();
        }
    },
    showCart: function(timePeriod) {
        this.container.parentNode.style.zIndex = 992;
        new Effect.SlideDown(this.container.id, {
            duration: 0.5,
            beforeStart: function(effect) {
                $(effect.element.id).addClassName('process');
            },
            afterFinish: function(effect) {
                $(effect.element.id).removeClassName('process');
            }
        });
        $(this.elementHeader).addClassName('expanded');
        if (timePeriod) {
            this.timePeriod = timePeriod * 1000;
            this.interval = setTimeout(this.hideCart.bind(this), this.timePeriod);
        }
    },
    hideCart: function() {
        if (!$(this.container.id).hasClassName('process') && $(this.elementHeader).hasClassName('expanded')) {
            new Effect.SlideUp(this.container.id, {
                duration: 0.5,
                beforeStart: function(effect) {
                    $(effect.element.id).addClassName('process');
                },
                afterFinish: function(effect) {
                    $(effect.element.id).removeClassName('process');
                    effect.element.parentNode.style.zIndex = 1;
                }
            });
        }
        if (this.interval !== null) {
            clearTimeout(this.interval);
            this.interval = null;
        }
        $(this.elementHeader).removeClassName('expanded');
    }
};
Enterprise.Bundle = {
    oldReloadPrice: false,
    initialize: function() {
        this.slider = $('bundleProduct');
        this.xOffset = $('bundle-product-wrapper').getDimensions().width;
    },
    swapReloadPrice: function() {
        Enterprise.Bundle.oldReloadPrice = Product.Bundle.prototype.reloadPrice;
        Product.Bundle.prototype.reloadPrice = Enterprise.Bundle.reloadPrice;
        Product.Bundle.prototype.selection = Enterprise.Bundle.selection;
    },
    reloadPrice: function() {
        var result = Enterprise.Bundle.oldReloadPrice.bind(this)();
        var priceContainer, duplicateContainer = null
        if (priceContainer = $('bundle-product-wrapper').down('.price-box .price-as-configured')) {
            if (duplicateContainer = $('bundle-product-wrapper').down('.duplicate-price-box .price-as-configured')) {
                duplicateContainer.down('.price').update(priceContainer.down('.price').innerHTML);
            }
        }
        if (!this.summaryTemplate && $('bundle-summary-template')) {
            this.summaryTemplate = new Template($('bundle-summary-template').innerHTML, Enterprise.templatesPattern);
            this.optionTemplate = new Template($('bundle-summary-option-template').innerHTML, Enterprise.templatesPattern);
            this.optionMultiTemplate = new Template($('bundle-summary-option-multi-template').innerHTML, Enterprise.templatesPattern);
        }
        if (this.summaryTemplate && $('bundle-summary')) {
            var summaryHTMLArray = [];
            for (var option in this.config.options) {
                if (typeof(this.config.selected[option]) !== 'undefined') {
                    var optionHTML = '';
                    for (var i = 0, l = this.config.selected[option].length; i < l; i++) {
                        var selection = this.selection(option, this.config.selected[option][i]);
                        if (selection && this.config.options[option].isMulti) {
                            optionHTML += this.optionMultiTemplate.evaluate(selection);
                        } else if (selection) {
                            optionHTML += this.optionTemplate.evaluate(selection);
                        }
                    }
                    if (optionHTML.length > 0) {
                        var position = parseInt(this.config.options[option].position);
                        summaryHTMLArray[position] = this.summaryTemplate.evaluate({
                            label: this.config.options[option].title.escapeHTML(),
                            options: optionHTML
                        });
                    }
                }
            }
            var summaryHTML = summaryHTMLArray.join('');
            if (typeof($('bundle-summary').update(summaryHTML).childElements().last()) != 'undefined') {
                $('bundle-summary').update(summaryHTML).childElements().last().addClassName('last');
            }
        }
        return result;
    },
    selection: function(optionId, selectionId) {
        if (selectionId == '' || selectionId == 'none') {
            return false;
        }
        var qty = null;
        if (this.config.options[optionId].selections[selectionId].customQty == 1 && !this.config['options'][optionId].isMulti) {
            if ($('bundle-option-' + optionId + '-qty-input')) {
                qty = $('bundle-option-' + optionId + '-qty-input').value;
            } else {
                qty = 1;
            }
        } else {
            qty = this.config.options[optionId].selections[selectionId].qty;
        }
        return {
            qty: qty,
            name: this.config.options[optionId].selections[selectionId].name.escapeHTML()
        };
    },
    start: function() {
        if (!$('bundle-product-wrapper').hasClassName('moving-now')) {
            new Effect.Move(this.slider, {
                x: -this.xOffset,
                y: 0,
                mode: 'relative',
                duration: 1.5,
                beforeStart: function(effect) {
                    $('bundle-product-wrapper').setStyle({
                        height: $('productView').getHeight() + 'px'
                    });
                    $('options-container').show();
                    Enterprise.BundleSummary.initialize();
                    $('bundle-product-wrapper').addClassName('moving-now');
                },
                afterFinish: function(effect) {
                    $('bundle-product-wrapper').setStyle({
                        height: 'auto'
                    });
                    $('productView').hide();
                    $('bundle-product-wrapper').removeClassName('moving-now');
                }
            });
        }
    },
    end: function() {
        if (!$('bundle-product-wrapper').hasClassName('moving-now')) {
            new Effect.Move(this.slider, {
                x: this.xOffset,
                y: 0,
                mode: 'relative',
                duration: 1.5,
                beforeStart: function(effect) {
                    $('bundle-product-wrapper').setStyle({
                        height: $('options-container').getHeight() + 'px'
                    });
                    $('productView').show();
                    $('bundle-product-wrapper').addClassName('moving-now');
                },
                afterFinish: function(effect) {
                    $('bundle-product-wrapper').setStyle({
                        height: 'auto'
                    });
                    $('options-container').hide();
                    Enterprise.BundleSummary.exitSummary();
                    $('bundle-product-wrapper').removeClassName('moving-now');
                }
            });
        }
    }
};
Enterprise.BundleSummary = {
    initialize: function() {
        this.summary = $('bundleSummary');
        this.summaryOffsetTop = $('customizeTitle').getDimensions().height;
        this.summary.setStyle({
            top: this.summaryOffsetTop + "px"
        });
        this.summaryContainer = this.summary.up(0);
        this.doNotCheck = false;
        this.summaryStartY = this.summary.positionedOffset().top;
        this.summaryStartY = this.summaryOffsetTop;
        this.summaryStartX = this.summary.positionedOffset().left;
        this.onDocScroll = this.handleDocScroll.bindAsEventListener(this);
        this.GetScroll = setInterval(this.onDocScroll, 50);
        this.onEffectEnds = this.effectEnds.bind(this);
    },
    handleDocScroll: function() {
        if (this.currentOffsetTop == document.viewport.getScrollOffsets().top && (this.checkOffset(null) == null)) {
            return;
        } else {
            if (this.currentOffsetTop == document.viewport.getScrollOffsets().top) {
                this.doNotCheck = true;
            }
            this.currentOffsetTop = document.viewport.getScrollOffsets().top;
        }
        if (this.currentEffect) {
            this.currentEffect.cancel();
            var topOffset = 0;
            if (this.summaryContainer.viewportOffset().top < -60) {
                topOffset = -(this.summaryContainer.viewportOffset().top);
            } else {
                topOffset = this.summaryStartY;
            }
            topOffset = this.checkOffset(topOffset);
            if (topOffset === null) {
                this.currentEffect = false;
                return;
            }
            this.currentEffect.start({
                x: this.summaryStartX,
                y: topOffset,
                mode: 'absolute',
                duration: 0.3,
                afterFinish: this.onEffectEnds
            });
            return;
        }
        this.currentEffect = new Effect.Move(this.summary);
    },
    effectEnds: function() {
        if (this.doNotCheck == true) {
            this.doNotCheck = false;
        }
    },
    checkOffset: function(offset) {
        if (this.doNotCheck && offset === null) {
            return null;
        }
        var dimensions = this.summary.getDimensions();
        var parentDimensions = this.summary.up().getDimensions();
        if ((offset !== null ? offset : this.summary.offsetTop) + dimensions.height >= parentDimensions.height) {
            offset = parentDimensions.height - dimensions.height;
        } else if (offset === null && this.currentOffsetTop > (this.summaryContainer.viewportOffset().top) && (this.currentOffsetTop - this.summaryContainer.viewportOffset().top) > this.summary.offsetTop) {
            offset = this.currentOffsetTop - this.summaryContainer.viewportOffset().top;
        }
        return offset;
    },
    exitSummary: function() {
        clearInterval(this.GetScroll);
    }
};
Enterprise.Tabs = Class.create();
Object.extend(Enterprise.Tabs.prototype, {
    initialize: function(container) {
        this.container = $(container);
        this.container.addClassName('tab-list');
        this.tabs = this.container.select('dt.tab');
        this.activeTab = this.tabs.first();
        this.tabs.first().addClassName('first');
        this.tabs.last().addClassName('last');
        this.onTabClick = this.handleTabClick.bindAsEventListener(this);
        for (var i = 0, l = this.tabs.length; i < l; i++) {
            this.tabs[i].observe('click', this.onTabClick);
        }
        this.select();
    },
    handleTabClick: function(evt) {
        this.activeTab = Event.findElement(evt, 'dt');
        this.select();
    },
    select: function() {
        for (var i = 0, l = this.tabs.length; i < l; i++) {
            if (this.tabs[i] == this.activeTab) {
                this.tabs[i].addClassName('active');
                this.tabs[i].style.zIndex = this.tabs.length + 2;
                new Effect.Appear(this.tabs[i].next('dd'), {
                    duration: 0.5
                });
                this.tabs[i].parentNode.style.height = this.tabs[i].next('dd').getHeight() + 15 + 'px';
            } else {
                this.tabs[i].removeClassName('active');
                this.tabs[i].style.zIndex = this.tabs.length + 1 - i;
                this.tabs[i].next('dd').hide();
            }
        }
    }
});
Enterprise.Slider = Class.create();
Object.extend(Enterprise.Slider.prototype, {
    initialize: function(container, config) {
        this.container = $(container);
        this.config = {
            panelCss: 'slider-panel',
            sliderCss: 'slider',
            itemCss: 'slider-item',
            slideButtonCss: 'slide-button',
            slideButtonInactiveCss: 'inactive',
            forwardButtonCss: 'forward',
            backwardButtonCss: 'backward',
            pageSize: 6,
            scrollSize: 2,
            slideDuration: 1.0,
            slideDirection: 'horizontal',
            fadeEffect: true
        };
        Object.extend(this.config, config || {});
        this.items = this.container.select('.' + this.config.itemCss);
        this.isPlaying = false;
        this.isAbsolutized = false;
        this.offset = 0;
        this.onClick = this.handleClick.bindAsEventListener(this);
        this.sliderPanel = this.container.down('.' + this.config.panelCss);
        this.slider = this.sliderPanel.down('.' + this.config.sliderCss);
        this.container.select('.' + this.config.slideButtonCss).each(this.initializeHandlers.bind(this));
        this.updateButtons();
        Event.observe(window, 'load', this.initializeDimensions.bind(this));
    },
    initializeHandlers: function(element) {
        if (element.hasClassName(this.config.forwardButtonCss) || element.hasClassName(this.config.backwardButtonCss)) {
            element.observe('click', this.onClick);
        }
    },
    handleClick: function(evt) {
        var element = Event.element(evt);
        if (!element.hasClassName(this.config.slideButtonCss)) {
            element = element.up('.' + this.config.slideButtonCss);
        }
        if (!element.hasClassName(this.config.slideButtonInactiveCss)) {
            element.hasClassName(this.config.forwardButtonCss) || this.backward();
            element.hasClassName(this.config.backwardButtonCss) || this.forward();
        }
        Event.stop(evt);
    },
    updateButtons: function() {
        var buttons = this.container.select('.' + this.config.slideButtonCss);
        for (var i = 0, l = buttons.length; i < l; i++) {
            if (buttons[i].hasClassName(this.config.backwardButtonCss)) {
                if (this.offset <= 0) {
                    buttons[i].addClassName(this.config.slideButtonInactiveCss);
                } else {
                    buttons[i].removeClassName(this.config.slideButtonInactiveCss);
                }
            } else if (buttons[i].hasClassName(this.config.forwardButtonCss)) {
                if (this.offset >= this.items.length - this.config.pageSize) {
                    buttons[i].addClassName(this.config.slideButtonInactiveCss);
                } else {
                    buttons[i].removeClassName(this.config.slideButtonInactiveCss);
                }
            }
        }
    },
    initializeDimensions: function() {
        if ((this.config.slideDirection == 'horizontal' && this.sliderPanel.style.width) || (this.config.slideDirection != 'horizontal' && this.sliderPanel.style.height)) {
            return this;
        }
        var firstItem = this.items.first();
        var offset = 0;
        if (this.config.slideDirection == 'horizontal') {
            offset = (parseInt(firstItem.getStyle('margin-left')) + parseInt(firstItem.getStyle('margin-right'))) * (this.config.pageSize - 1);
            this.sliderPanel.setStyle({
                width: (firstItem.getDimensions().width * this.config.pageSize + offset) + 'px'
            });
        } else {
            offset = (parseInt(firstItem.getStyle('margin-bottom')) + parseInt(firstItem.getStyle('margin-top'))) * (this.config.pageSize - 1);
            this.sliderPanel.setStyle({
                height: (firstItem.getDimensions().height * this.config.pageSize + offset) + 'px'
            });
        }
        var dimensions = this.sliderPanel.getDimensions();
        var sliderParent = this.sliderPanel.up();
        sliderParent.setStyle({
            width: dimensions.width + 'px',
            height: dimensions.height + 'px'
        });
        return this;
    },
    absolutize: function() {
        if (!this.isAbsolutized) {
            this.isAbsolutized = true;
            var dimensions = this.sliderPanel.getDimensions();
            this.sliderPanel.setStyle({
                height: dimensions.height + 'px',
                width: dimensions.width + 'px'
            });
            this.slider.absolutize();
        }
    },
    forward: function() {
        if (this.offset + this.config.pageSize <= this.items.length - 1) {
            this.slide(true);
        }
    },
    backward: function() {
        if (this.offset > 0) {
            this.slide(false);
        }
    },
    slide: function(isForward) {
        if (this.isPlaying) {
            return;
        }
        this.absolutize();
        this.effectConfig = {
            duration: this.config.slideDuration
        };
        if (this.config.slideDirection == 'horizontal') {
            this.effectConfig.x = this.getSlidePosition(isForward).left;
        } else {
            this.effectConfig.y = this.getSlidePosition(isForward).top;
        }
        this.start();
    },
    start: function() {
        if (this.config.fadeEffect) {
            this.fadeIn();
        } else {
            this.move();
        }
    },
    fadeIn: function() {
        new Effect.Fade(this.slider.up('div.slider-panel'), {
            from: 1.0,
            to: 0.5,
            afterFinish: this.move.bind(this),
            beforeStart: this.effectStarts.bind(this),
            duration: 0.3
        });
    },
    fadeOut: function() {
        new Effect.Fade(this.slider.up('div.slider-panel'), {
            from: 0.5,
            to: 1.0,
            afterFinish: this.effectEnds.bind(this),
            duration: 0.3
        });
    },
    move: function() {
        if (this.config.fadeEffect) {
            this.effectConfig.afterFinish = this.fadeOut.bind(this);
        } else {
            this.effectConfig.afterFinish = this.effectEnds.bind(this);
            this.effectConfig.beforeStart = this.effectStarts.bind(this);
        }
        new Effect.Move(this.slider, this.effectConfig);
    },
    effectStarts: function() {
        this.isPlaying = true;
    },
    effectEnds: function() {
        this.isPlaying = false;
        this.updateButtons();
    },
    getSlidePosition: function(isForward) {
        var targetOffset;
        if (isForward) {
            targetOffset = Math.min(this.items.length - this.config.pageSize, this.offset + this.config.scrollSize)
        } else {
            targetOffset = Math.max(this.offset - this.config.scrollSize, 0);
        }
        this.offset = targetOffset;
        var item = this.items[targetOffset];
        var itemOffset = {
            left: 0,
            top: 0
        };
        itemOffset.left = -(item.cumulativeOffset().left - this.slider.cumulativeOffset().left + this.slider.offsetLeft);
        itemOffset.top = -(item.cumulativeOffset().top - this.slider.cumulativeOffset().top + this.slider.offsetTop);
        return itemOffset;
    }
});
Enterprise.PopUpMenu = {
    currentPopUp: null,
    documentHandlerInitialized: false,
    popUpZIndex: 994,
    hideDelay: 2000,
    hideOnClick: true,
    hideInterval: null,
    initializeDocumentHandler: function() {
        if (!this.documentHandlerInitialized) {
            this.documentHandlerInitialized = true;
            Event.observe(document.body, 'click', this.handleDocumentClick.bindAsEventListener(this));
        }
    },
    handleDocumentClick: function(evt) {
        if (this.currentPopUp !== null) {
            var element = Event.element(evt);
            if (!this.currentPopUp.onlyShowed && this.hideOnClick) {
                this.hide();
            } else {
                this.currentPopUp.onlyShowed = false;
            }
        }
    },
    handlePopUpOver: function(evt) {
        if (this.currentPopUp !== null) {
            this.currentPopUp.removeClassName('faded');
            this.resetTimeout(0);
        }
    },
    handlePopUpOut: function(evt) {
        if (this.currentPopUp !== null) {
            this.currentPopUp.addClassName('faded');
            this.resetTimeout(1);
        }
    },
    show: function(trigger) {
        this.initializeDocumentHandler();
        var container = $(trigger).up('.switch-wrapper');
        if (!$('popId-' + container.id)) {
            return;
        }
        if (this.currentPopUp !== null && $('popId-' + container.id) !== this.currentPopUp) {
            this.hide(true);
        } else if (this.currentPopUp !== null && this.currentPopUp === $('popId-' + container.id)) {
            this.hide();
            return;
        }
        this.currentPopUp = $('popId-' + container.id);
        this.currentPopUp.container = container;
        this.currentPopUp.container.oldZIndex = this.currentPopUp.container.style.zIndex;
        this.currentPopUp.container.style.zIndex = this.popUpZIndex;
        new Effect.Appear(this.currentPopUp, {
            duration: 0.3
        });
        if (!this.currentPopUp.isHandled) {
            this.currentPopUp.observe('mouseover', this.handlePopUpOver.bindAsEventListener(this));
            this.currentPopUp.observe('mouseout', this.handlePopUpOut.bindAsEventListener(this));
            this.currentPopUp.isHandled = true;
        }
        this.currentPopUp.onlyShowed = true;
        this.currentPopUp.container.down('.switcher').addClassName('list-opened');
        if (trigger.up('.header-panel__currency')) {
            trigger.up('.header-panel__currency').addClassName('dropdown-activated');
        }
        this.resetTimeout(2);
    },
    hide: function() {
        if (this.currentPopUp !== null) {
            if (arguments.length == 0) {
                new Effect.Fade(this.currentPopUp, {
                    duration: 0.3
                });
            } else {
                this.currentPopUp.hide();
            }
            this.currentPopUp.container.style.zIndex = this.currentPopUp.container.oldZIndex;
            this.resetTimeout(0);
            this.currentPopUp.container.down('.switcher').removeClassName('list-opened');
            if (this.currentPopUp.up('.header-panel__currency')) {
                this.currentPopUp.up('.header-panel__currency').removeClassName('dropdown-activated');
            }
            this.currentPopUp = null;
        }
    },
    resetTimeout: function(delay) {
        if (this.hideTimeout !== null) {
            clearTimeout(this.hideTimeout);
            this.hideTimeout = null;
        }
        if (delay) {
            this.hideTimeout = setTimeout(this.hide.bind(this), this.hideDelay * delay);
        }
    }
};
var showMsg = function(msg, blockClass, target) {
    var targetBlock = null;
    var existBlocks = target.childElements();
    if (existBlocks.length === 0) {
        var msgBlock = new Element('div');
        msgBlock.addClassName(blockClass);
        msgBlock.appendChild(new Element('ul'));
        target.insertBefore(msgBlock, target.down());
        targetBlock = msgBlock.down();
    } else {
        targetBlock = existBlocks.first();
    }
    var newMsg = new Element('li');
    newMsg.update(msg);
    targetBlock.appendChild(newMsg);
}

function popUpMenu(element) {
    Enterprise.PopUpMenu.show(element);
}
Enterprise.Widget = Class.create({
    _node: null,
    _children: [],
    initialize: function(node) {
        this._node = node;
    },
    getNode: function() {
        return this._node;
    },
    addChild: function(widget) {
        this._children.push(widget);
        var children = $(this._node).immediateDescendants(),
            exists = false;
        $(this._node).immediateDescendants().each(function(child) {
            if (child == widget.getNode()) {
                exists = true;
            }
        });
        if (!exists) {
            widget.placeAt(this._node);
        }
    },
    placeAt: function(node) {
        $(node).insert(this._node);
    }
});
Enterprise.Widget.Dialog = Class.create(Enterprise.Widget, {
    _title: '',
    _titleNode: {},
    _contentNode: {},
    _backNode: {},
    _isPlaced: false,
    initialize: function($super, title, content, additionalClass) {
        this._title = title;
        this._node = new Element('div', {
            'class': 'popup-block block'
        });
        this._node.addClassName(additionalClass);
        this._windowOverlay = new Element('div', {
            'class': 'window-overlay'
        });
        var headerNode = new Element('div', {
            'class': 'block-title'
        });
        this._titleNode = new Element('strong').update(title);
        this._closeButton = new Element('div', {
            'class': 'btn-close'
        }).update('Close');
        $(this._closeButton).onclick = (function() {
            this.hide();
        }).bind(this);
        headerNode.insert(this._titleNode);
        headerNode.insert(this._closeButton);
        this._node.insert(headerNode);
        this._contentNode = new Element('div', {
            'class': 'block-content'
        });
        this._contentNode.insert(content);
        this._node.insert(this._contentNode);
    },
    place: function() {
        $(document.body).insert(this._windowOverlay);
        $(document.body).insert(this._node);
        this._isPlaced = true;
    },
    setTitle: function(title) {
        $(this._titleNode).update(title);
    },
    setContent: function(content) {
        $(this._contentNode).update(content);
    },
    getContent: function() {
        return this._contentNode;
    },
    show: function() {
        if (!this._isPlaced) {
            this.place();
        }
        $(this._windowOverlay).addClassName('active');
        this._windowOverlay.style.height = $$('body')[0].getHeight() + 'px';
        $(this._node).addClassName('active');
    },
    hide: function() {
        $(this._windowOverlay).removeClassName('active');
        $(this._node).removeClassName('active');
    },
    setBusy: function(state) {
        if (state) {
            $(this._node).addClassName('loading');
        } else {
            $(this._node).removeClassName('loading');
        }
    },
    destroy: function() {
        $(this._node).remove();
    }
});
Enterprise.Widget.SplitButton = Class.create(Enterprise.Widget, {
    _list: null,
    _templateString: '<strong><span></span></strong>' +
        '<a href="#" class="change"></a>' +
        '<div class="list-container">' +
        '<ul>' +
        '</ul>' +
        '</div>',
    initialize: function($super, title, alt, type) {
        if (typeof title != 'string') {
            $super(title);
        } else {
            $super(new Element('div', {
                'class': 'split-button split-button-created' + ((type) ? ' ' + type : '')
            }));
            this._node.update(this._templateString);
            this._node.down('strong span').update(title);
            this._node.down('.change').update(alt);
        }
        Event.observe($(this._node).down('strong'), 'click', (function(event) {
            this.onClick(event);
        }).bind(this));
        this._node.down('.change').setAttribute('tabindex', 20);
        this._list = $(this._node).down('ul');
        Event.observe($(this._node).down('.change'), 'click', this.onToggle.bind(this));
        Event.observe($(this._node).down('.change'), 'blur', this.close.bind(this));
    },
    onClick: function(event) {},
    onToggle: function(event) {
        Event.stop(event);
        if (this.isOpened()) {
            this.close();
        } else {
            this.open();
        }
    },
    isOpened: function() {
        return $(this._node).hasClassName('active');
    },
    open: function() {
        $(this._node).addClassName('active');
        this.onOpen();
    },
    onOpen: function() {},
    close: function() {
        $(this._node).removeClassName.bind($(this._node), 'active').delay(0.2);
        this.onClose();
    },
    onClose: function() {},
    addOption: function(option) {
        option.placeAt(this._list);
        option.onClick = option.onClick.wrap((function(proceed) {
            proceed();
            this.close();
        }).bind(this));
    }
});
Enterprise.Widget.SplitButton.Option = Class.create(Enterprise.Widget, {
    initialize: function($super, title, type) {
        $super(new Element('li', {
            'class': type ? type : null
        }));
        this._node.update('<span title="' + title + '">' + title + '</span>');
        Event.observe(this._node, 'click', (function() {
            this.onClick()
        }).bind(this));
    },
    getNode: function() {
        return this._node;
    },
    onClick: function() {}
})
Enterprise.loadSplitButtons = function() {
    if (typeof Enterprise.splitButtonsLoaded == 'undefined') {
        Enterprise.splitButtonsLoaded = true;
        $$('.split-button').each(function(node) {
            if (!$(node).hasClassName('split-button-created')) {
                new Enterprise.Widget.SplitButton(node);
            }
        });
    }
};
Enterprise.textOverflow = function(elem) {
    var container = $(elem);
    if (container.getStyle('overflow') == 'hidden') {
        var inner = container.down(0);
        var initialHeight = container.getHeight();
        if (inner.getHeight() > initialHeight) {
            var words = inner.innerHTML.split(' ');
            var test = new Element('span', {
                'style': 'visibility:hidden;'
            });
            test.style.width = container.getWidth();
            container.insert(test);
            var tempString = '';
            for (var i = 0; $(test).getHeight() <= initialHeight || i < words.legth; i++) {
                tempString = tempString + words[i] + ' ';
                test.update(tempString)
            };
            var finalstring = (words.slice(-words.length, i - 2)).join(' ');
            test.remove();
            inner.update(finalstring + '&hellip;');
        }
    }
};

function applyPoints() {
    if (typeof awOSCReviewEnterprisePoints != 'undefined') {
        if (!Mage.Cookies.get('pointsAssigned')) {
            awOSCReviewEnterprisePoints.__proto__.applyPoints.call(awOSCReviewEnterprisePoints);
            Mage.Cookies.set('pointsAssigned', '1', new Date(new Date().getTime() + 7200000));
        }
    }
}

function applyCredits() {
    if (typeof awOSCReviewEnterpriseStorecredit != 'undefined') {
        if (!Mage.Cookies.get('creditAssigned')) {
            awOSCReviewEnterpriseStorecredit.__proto__.applyStorecredit.call(awOSCReviewEnterpriseStorecredit);
            Mage.Cookies.set('creditAssigned', '1', new Date(new Date().getTime() + 7200000));
        }
    }
}

function toQueryString(obj, prefix) {
    var str = [],
        k, v;
    for (var p in obj) {
        if (!obj.hasOwnProperty(p)) {
            continue;
        }
        if (~p.indexOf('[')) {
            k = prefix ? prefix + "[" + p.substring(0, p.indexOf('[')) + "]" + p.substring(p.indexOf('[')) : p;
        } else {
            k = prefix ? prefix + "[" + p + "]" : p;
        }
        v = obj[p];
        str.push(typeof v == "object" ? toQueryString(v, k) : encodeURIComponent(k) + "=" + encodeURIComponent(v));
    }
    return str.join("&");
}
Event.observe(document, 'dom:loaded', function() {
    applyPoints();
    applyCredits();
});
if (!window.Healthpost) {
    window.Healthpost = {};
}
Healthpost.Geolookup = function(requestUrl, configuration) {
    if (Mage.Cookies.get('geo_popup')) {
        return;
    }
    new Ajax.Request(requestUrl, {
        method: 'get',
        onSuccess: function(response) {
            if (response.responseJSON && response.responseJSON.country_code) {
                countryCode = response.responseJSON.country_code;
                if (configuration[countryCode]) {
                    contentBoxId = 'geo-box-' + configuration[countryCode].code;
                    if ($(contentBoxId)) {
                        $(contentBoxId).show();
                        $$('.geo-popup-wrapper')[0].addClassName('open');
                        $$('.close').each(function(element) {
                            element.observe('click', function(e) {
                                $$('.geo-popup-wrapper')[0].removeClassName('open');
                                Event.stop(e);
                            });
                            element.observe('touchstart', function(e) {
                                $$('.geo-popup-wrapper')[0].removeClassName('open');
                                Event.stop(e);
                            });
                        });
                        $$('body')[0].observe('click', function(el) {
                            if (!el.target.offsetParent.hasClassName('geo-box')) {
                                $$('.geo-popup-wrapper')[0].removeClassName('open');
                            }
                        });
                        $$('body')[0].observe('touchstart', function(el) {
                            if (!el.target.offsetParent.hasClassName('geo-box')) {
                                $$('.geo-popup-wrapper')[0].removeClassName('open');
                            }
                        });
                    }
                }
            }
        }
    });
    Mage.Cookies.set('geo_popup', 1);
};
Event.observe(document, 'dom:loaded', Enterprise.loadSplitButtons);
if (!window.Enterprise) {
    window.Enterprise = {};
}
if (!Enterprise.CatalogEvent) {
    Enterprise.CatalogEvent = {};
}
Enterprise.CatalogEvent.Ticker = Class.create();
Object.extend(Enterprise.CatalogEvent.Ticker.prototype, {
    initialize: function(container, seconds) {
        this.container = $(container);
        this.seconds = seconds;
        this.start = new Date();
        this.interval = setInterval(this.applyTimer.bind(this), 1000);
        this.applyTimer();
    },
    getEstimate: function() {
        var now = new Date();
        var result = this.seconds - (now.getTime() - this.start.getTime()) / 1000;
        if (result < 0) {
            return 0;
        }
        return Math.round(result);
    },
    applyTimer: function() {
        var seconds = this.getEstimate();
        var daySec = Math.floor(seconds / (3600 * 24)) * (3600 * 24);
        var hourSec = Math.floor(seconds / 3600) * 3600;
        var minuteSec = Math.floor(seconds / 60) * 60;
        var secondSec = seconds;
        this.container.down('.days').update(this.formatNumber(Math.floor(daySec / (3600 * 24))));
        this.container.down('.hour').update(this.formatNumber(Math.floor((hourSec - daySec) / 3600)));
        this.container.down('.minute').update(this.formatNumber(Math.floor((minuteSec - hourSec) / 60)));
        this.container.down('.second').update(this.formatNumber(seconds - minuteSec));
        if (daySec > 0) {
            this.container.down('.second').previous('.delimiter').hide();
            this.container.down('.second').hide();
            this.container.down('.days').show();
            this.container.down('.days').next('.delimiter').show();
        } else {
            this.container.down('.days').hide();
            this.container.down('.days').next('.delimiter').hide();
            this.container.down('.second').previous('.delimiter').show();
            this.container.down('.second').show();
        }
    },
    formatNumber: function(number) {
        if (number < 10) {
            return '0' + number.toString();
        }
        return number.toString();
    }
});
if (!window.Enterprise) {
    window.Enterprise = {};
}
if (!Enterprise.Wishlist) {
    Enterprise.Wishlist = {
        Widget: {
            Form: {}
        }
    };
}
Enterprise.Wishlist.Widget.Form = Class.create(Enterprise.Widget, {
    action: null,
    isValid: false,
    initialize: function($super, action) {
        var _templateString = '<ul class="form-list">' +
            '<li><label for="wishlist-name">' + Translator.translate('Wishlist Name') + '</label><div class="input-box"><input type="text" id="wishlist-name" maxlength="255" class="input-text required-entry validate-length maximum-length-255" name="name"/></div>' +
            '<li class="control"><div class="input-box"><input type="checkbox" id="wishlist-public" name="visibility"></div><label for="wishlist-public">' + Translator.translate('Make This Wishlist Public') + '</label></li>' +
            '</ul>' +
            '<div class="buttons-set form-buttons"><button type="submit" class="button btn-save"><span><span>' + Translator.translate('Save') + '</span></span></button><button type="button" class="button btn-cancel"><span><span>' + Translator.translate('Cancel') + '</span></span></button></div>';
        this.action = action;
        $super(new Element('form', {
            'method': 'post',
            'action': action
        }));
        this._node.update(_templateString);
        var that = this;
        var deferredList = {
            event: null,
            counter: 0,
            callback: function() {
                this.counter++;
                if (this.counter >= 2) {
                    this.success();
                }
            },
            success: function() {
                that.onSubmit(this.event);
            }
        };
        var validation = new Validation(this._node, {
            onFormValidate: (function(result) {
                this.isValid = result;
                deferredList.callback();
            }).bind(this)
        });
        Event.observe(this._node, 'submit', (function(event) {
            deferredList.event = event;
            deferredList.callback();
        }).bind(this));
        Event.observe($(this._node).down('button.btn-cancel'), 'click', (function() {
            this.onCancel();
        }).bind(this));
        this.nameNode = $(this._node).down('#wishlist-name');
        this.visibilityNode = $(this._node).down('#wishlist-public');
    },
    onSubmit: function(event) {},
    onCancel: function() {},
    setName: function(name) {
        this.nameNode.value = name;
    },
    setIsVisible: function(state) {
        this.visibilityNode.checked = !!state;
    }
});
Enterprise.Wishlist.Widget.Form.Create = Class.create(Enterprise.Wishlist.Widget.Form, {
    useAjax: true,
    initialize: function($super, action, useAjax) {
        $super(action);
        this.useAjax = useAjax;
    },
    onSubmit: function(event) {
        Event.stop(event);
        if (!this.isValid) {
            return;
        }
        if (!this.useAjax) {
            this.onWishlistCreated({
                serializedData: $(this._node).serialize()
            });
        } else {
            var callback = (function(wishlistId) {
                this.onWishlistCreated(wishlistId)
            }).bind(this);
            new Ajax.Request(this.action, {
                method: 'post',
                parameters: $(this._node).serialize(),
                onSuccess: function(response) {
                    try {
                        var data = response.responseJSON;
                        if (typeof data.wishlist_id != 'undefined') {
                            callback(data.wishlist_id);
                        } else if (typeof data.redirect != 'undefined') {
                            setLocation(data.redirect);
                        } else {
                            alert(Translator.translate('Error happened while creating wishlist. Please try again later'));
                        }
                    } catch (e) {
                        setLocation(window.location.href);
                    }
                }
            });
        }
    },
    onWishlistCreated: function(wishlist) {}
});
Enterprise.Wishlist.createWithCallback = function(createUrl, callback, useAjax) {
    if (typeof useAjax == 'undefined') {
        useAjax = true;
    }
    if (!Enterprise.Wishlist.createWithCallbackDialog) {
        var createWithCallbackForm = new Enterprise.Wishlist.Widget.Form.Create(createUrl, useAjax);
        Enterprise.Wishlist.createWithCallbackDialog = new Enterprise.Widget.Dialog(Translator.translate('Create New Wishlist'), createWithCallbackForm.getNode());
        Enterprise.Wishlist.createWithCallbackDialog.form = createWithCallbackForm;
        createWithCallbackForm.onCancel = Enterprise.Wishlist.createWithCallbackDialog.hide.bind(Enterprise.Wishlist.createWithCallbackDialog);
        Enterprise.Wishlist.createWithCallbackDialog.form.onSubmit = Enterprise.Wishlist.createWithCallbackDialog.form.onSubmit.wrap(function(proceed, event) {
            proceed(event);
            if (this.isValid) {
                Enterprise.Wishlist.createWithCallbackDialog.setBusy(true);
            }
        })
    }
    Enterprise.Wishlist.createWithCallbackDialog.form.useAjax = useAjax;
    Enterprise.Wishlist.createWithCallbackDialog.form.onWishlistCreated = callback;
    Enterprise.Wishlist.createWithCallbackDialog.show();
}
Enterprise.Wishlist.create = function(createUrl, callback) {
    if (!Enterprise.Wishlist.createDialog) {
        var createForm = new Enterprise.Wishlist.Widget.Form(createUrl);
        Enterprise.Wishlist.createDialog = new Enterprise.Widget.Dialog(Translator.translate('Create New Wishlist'), createForm.getNode());
        createForm.onCancel = Enterprise.Wishlist.createDialog.hide.bind(Enterprise.Wishlist.createDialog);
    }
    Enterprise.Wishlist.createDialog.show();
}
Enterprise.Wishlist.edit = function(editUrl, wishlistName, visibility) {
    if (!Enterprise.Wishlist.editDialog) {
        var editForm = new Enterprise.Wishlist.Widget.Form(editUrl);
        Enterprise.Wishlist.editDialog = new Enterprise.Widget.Dialog(Translator.translate('Edit Wishlist'), editForm.getNode());
        Enterprise.Wishlist.editDialog.form = editForm;
        editForm.onCancel = Enterprise.Wishlist.editDialog.hide.bind(Enterprise.Wishlist.editDialog);
    }
    Enterprise.Wishlist.editDialog.form.setName(wishlistName);
    Enterprise.Wishlist.editDialog.form.setIsVisible(visibility);
    Enterprise.Wishlist.editDialog.show();
}
Enterprise.Wishlist.getRowQty = function(rowNode) {
    var qtyNode = $(rowNode).down('input.qty');
    return qtyNode ? qtyNode.value : null;
}
Enterprise.Wishlist.copyItemTo = function(itemId, qty, wishlistId) {
    var form = new Element('form', {
        method: 'post',
        action: Enterprise.Wishlist.url.copyItem
    });
    form.insert(new Element('input', {
        name: 'item_id',
        type: 'hidden',
        value: itemId
    }));
    if (typeof wishlistId != 'undefined') {
        form.insert(new Element('input', {
            name: 'wishlist_id',
            type: 'hidden',
            value: wishlistId
        }));
    }
    form.insert(new Element('input', {
        name: 'qty',
        type: 'hidden',
        value: qty
    }));
    $(document.body).insert(form);
    form.submit();
};
Enterprise.Wishlist.moveItemTo = function(itemId, qty, wishlistId) {
    var form = new Element('form', {
        method: 'post',
        action: Enterprise.Wishlist.url.moveItem
    });
    form.insert(new Element('input', {
        name: 'item_id',
        type: 'hidden',
        value: itemId
    }));
    if (typeof wishlistId != 'undefined') {
        form.insert(new Element('input', {
            name: 'wishlist_id',
            type: 'hidden',
            value: wishlistId
        }));
    }
    form.insert(new Element('input', {
        name: 'qty',
        type: 'hidden',
        value: qty
    }));
    $(document.body).insert(form);
    form.submit();
    return false;
};
Enterprise.Wishlist.copySelectedTo = function(wishlistId) {
    if (!this.itemsSelected()) {
        alert(Translator.translate('You must select items to copy'));
        return;
    }
    var url = Enterprise.Wishlist.url.copySelected;
    this.form.action = url.gsub('%wishlist_id%', wishlistId);
    this.form.submit();
};
Enterprise.Wishlist.moveSelectedTo = function(wishlistId) {
    if (!this.itemsSelected()) {
        alert(Translator.translate('You must select items to move'));
        return;
    }
    var url = Enterprise.Wishlist.url.moveSelected;
    this.form.action = url.gsub('%wishlist_id%', wishlistId);
    this.form.submit();
};
Enterprise.Wishlist.itemsSelected = function() {
    var selected = false;
    $(this.form).select('input.select').each(function(item) {
        if ($(item).checked) {
            selected = true;
        }
    });
    return selected;
};
Enterprise.Wishlist.copyItemToNew = function(itemId, qty) {
    this.createWithCallback(Enterprise.Wishlist.url.create, this.copyItemTo.bind(this, itemId, qty));
};
Enterprise.Wishlist.moveItemToNew = function(itemId, qty) {
    this.createWithCallback(Enterprise.Wishlist.url.create, this.moveItemTo.bind(this, itemId, qty));
};
Enterprise.Wishlist.moveSelectedToNew = function() {
    if (!this.itemsSelected()) {
        alert(Translator.translate('You must select items to move'));
        return;
    }
    this.createWithCallback(Enterprise.Wishlist.url.create, this.moveSelectedTo.bind(this));
};
Enterprise.Wishlist.copySelectedToNew = function() {
    if (!this.itemsSelected()) {
        alert(Translator.translate('You must select items to copy'));
        return;
    }
    this.createWithCallback(Enterprise.Wishlist.url.create, this.copySelectedTo.bind(this));
};
Event.observe(document, 'dom:loaded', function() {
    if (typeof Enterprise.Wishlist.list != 'undefined' && (Enterprise.Wishlist.list.length || Enterprise.Wishlist.canCreate)) {
        var buildUrl = function(url, wishlist) {
            var glue = url.indexOf('?') == -1 ? '?' : '&';
            var wishlistInfo = '';
            if (typeof wishlist.serializedData != 'undefined') {
                wishlistInfo = wishlist.serializedData;
            } else {
                wishlistInfo = Hash.toQueryString({
                    'wishlist_id': wishlist
                });
            }
            return url + glue + wishlistInfo;
        }
        $$('.link-wishlist').each(function(link) {
            var url = link.href;
            var onclick = link.onclick || function() {
                setLocation(this.href);
            }
            var wishlistSplitButton = new Enterprise.Widget.SplitButton(link.innerHTML, Translator.translate('Add to:'), 'light clickable');
            wishlistSplitButton.onClick = onclick.bind({
                href: url
            });
            Enterprise.Wishlist.list.each(function(wishlist) {
                var option = new Enterprise.Widget.SplitButton.Option(wishlist.name);
                option.onClick = onclick.bind({
                    href: buildUrl(url, wishlist.id)
                });
                wishlistSplitButton.addOption(option);
            });
            if (Enterprise.Wishlist.canCreate) {
                var option = new Enterprise.Widget.SplitButton.Option(Translator.translate('Create New Wishlist'), 'new');
                option.onClick = Enterprise.Wishlist.createWithCallback.bind(this, Enterprise.Wishlist.url.create, function(wishlist) {
                    (onclick.bind({
                        href: buildUrl(url, wishlist)
                    }))();
                }, link.hasClassName('use-ajax'));
                wishlistSplitButton.addOption(option);
            }
            wishlistSplitButton.placeAt(link.up());
            link.remove();
        });
    }
});
document.observe("dom:loaded", function() {
    $$('#wishlist-table div.description').each(function(el) {
        Enterprise.textOverflow(el);
    });
});
Carousel = Class.create(Abstract, {
    initialize: function(scroller, slides, controls, options) {
        this.scrolling = false;
        this.scroller = $(scroller);
        this.slides = slides;
        this.controls = controls;
        Prototype.Browser.IE10 = Prototype.Browser.IE && parseInt(navigator.userAgent.substring(navigator.userAgent.indexOf("MSIE") + 5)) == 10;
        this.options = Object.extend({
            duration: 1,
            auto: false,
            frequency: 3,
            visibleSlides: 1,
            moveSlides: 1,
            controlClassName: 'carousel-control',
            jumperClassName: 'carousel-jumper',
            disabledClassName: 'carousel-disabled',
            selectedClassName: 'carousel-selected',
            controlEnabled: 'control-enabled',
            controlDisabled: 'control-disabled',
            circular: false,
            wheel: true,
            effect: 'scroll',
            transition: 'sinoidal'
        }, options || {});
        if (this.options.effect == 'fade') {
            Prototype.Browser.IE6 = Prototype.Browser.IE && parseInt(navigator.userAgent.substring(navigator.userAgent.indexOf("MSIE") + 5)) == 6;
            if (!Prototype.Browser.IE6) {
                this.options.circular = true;
            } else {
                this.options.circular = false;
            }
        }
        this.slides.each(function(slide, index) {
            slide._index = index;
            if (index == 0 && options.effect == 'fade') {
                slide.setOpacity(1);
                slide.style.zIndex = 1;
                if (Prototype.Browser.IE10) {
                    slide.addClassName('shown-slide');
                }
            }
            if (index > 0 && options.effect == 'fade') {
                slide.setOpacity(0);
                slide.style.zIndex = 0;
                if (Prototype.Browser.IE10) {
                    slide.addClassName('hidden-slide');
                }
            }
        });
        if (this.controls) {
            this.controls.invoke('observe', 'click', this.click.bind(this));
        }
        if (this.options.wheel) {}
        if (this.options.auto) {
            this.start();
        }
        if (this.options.initial) {
            var initialIndex = this.slides.indexOf($(this.options.initial));
            if (initialIndex > (this.options.visibleSlides - 1) && this.options.visibleSlides > 1) {
                if (initialIndex > this.slides.length - (this.options.visibleSlides + 1)) {
                    initialIndex = this.slides.length - this.options.visibleSlides;
                }
            }
            this.moveTo(this.slides[initialIndex]);
        } else {
            this.setControlState();
        }
    },
    click: function(event) {
        this.stop();
        var element = event.findElement('a');
        if (!element.hasClassName(this.options.disabledClassName)) {
            if (element.hasClassName(this.options.controlClassName)) {
                eval("this." + element.rel + "()");
            } else if (element.hasClassName(this.options.jumperClassName)) {
                this.moveTo(element.rel);
                if (this.options.selectedClassName) {
                    this.controls.invoke('removeClassName', this.options.selectedClassName);
                    element.addClassName(this.options.selectedClassName);
                }
            }
        }
        this.deactivateControls();
        event.stop();
    },
    moveTo: function(index) {
        if (this.options.beforeMove && (typeof this.options.beforeMove == 'function')) {
            this.options.beforeMove();
        }
        this.previous = this.current ? this.current : 0;
        this.current = parseInt(index);
        var scrollerOffset = this.scroller.cumulativeOffset();
        var elementOffset = this.slides[this.current].cumulativeOffset();
        if (this.scrolling) {
            this.scrolling.cancel();
        }
        switch (this.options.effect) {
            case 'fade':
                var fades = [];
                for (var i = 0; i < this.options.moveSlides; i++) {
                    if (this.slides[this.previous + i]) {
                        fades.push(new Effect.Opacity(this.slides[this.previous + i], {
                            sync: true,
                            from: 1,
                            to: 0
                        }))
                    }
                    if (this.slides[this.current + i]) {
                        fades.push(new Effect.Opacity(this.slides[this.current + i], {
                            sync: true,
                            from: 0,
                            to: 1
                        }))
                    }
                }
                this.scrolling = new Effect.Parallel(fades, {
                    duration: this.options.duration,
                    afterFinish: (function() {
                        for (i = 0; i < this.options.moveSlides; i++) {
                            if (this.slides[this.previous + i]) {
                                this.slides[this.previous + i].style.zIndex = 0;
                                if (Prototype.Browser.IE10) {
                                    if (this.slides[this.previous + i].hasClassName('shown-slide')) {
                                        this.slides[this.previous + i].removeClassName('shown-slide');
                                        this.slides[this.previous + i].addClassName('hidden-slide');
                                    }
                                }
                            }
                            if (this.slides[this.current + i]) {
                                this.slides[this.current + i].style.zIndex = 1;
                                if (Prototype.Browser.IE10) {
                                    if (this.slides[this.current + i].hasClassName('hidden-slide')) {
                                        this.slides[this.current + i].removeClassName('hidden-slide');
                                        this.slides[this.current + i].addClassName('shown-slide');
                                    }
                                }
                            }
                        }
                        if (this.controls) {
                            this.activateControls();
                        }
                        if (this.options.afterMove && (typeof this.options.afterMove == 'function')) {
                            this.options.afterMove();
                        }
                    }).bind(this)
                });
                break;
            case 'scroll':
            default:
                var transition;
                switch (this.options.transition) {
                    case 'spring':
                        transition = Effect.Transitions.spring;
                        break;
                    case 'sinoidal':
                    default:
                        transition = Effect.Transitions.sinoidal;
                        break;
                }
                this.scrolling = new Effect.SmoothScroll(this.scroller, {
                    duration: this.options.duration,
                    x: (elementOffset[0] - scrollerOffset[0]),
                    y: (elementOffset[1] - scrollerOffset[1]),
                    transition: transition,
                    afterFinish: (function() {
                        if (this.controls) {
                            this.activateControls();
                        }
                        if (this.options.afterMove && (typeof this.options.afterMove == 'function')) {
                            this.options.afterMove();
                        }
                        this.scrolling = false;
                    }).bind(this)
                });
                break;
        }
        this.setControlState();
        return false;
    },
    prev: function() {
        if (this.current && this.slides[this.current]) {
            var currentIndex = this.current;
            if (this.options.circular) {
                var prevIndex = (currentIndex == 0) ? this.slides.length - 1 : currentIndex - 1;
            } else {
                var prevIndex = (currentIndex < this.options.moveSlides) ? 0 : currentIndex - this.options.moveSlides;
            }
        } else {
            var prevIndex = (this.options.circular ? this.slides.length - 1 : 0);
        }
        if (prevIndex == (this.slides.length - 1) && this.options.circular && this.options.effect != 'fade') {
            this.scroller.scrollLeft = (this.slides.length - 1) * this.slides.first().getWidth();
            this.scroller.scrollTop = (this.slides.length - 1) * this.slides.first().getHeight();
            prevIndex = this.slides.length - 2;
        }
        this.moveTo(prevIndex);
    },
    next: function() {
        if (this.current && this.slides[this.current]) {
            var currentIndex = this.current;
            var nextIndex = (this.slides.length - this.options.visibleSlides == currentIndex) ? (this.options.circular ? 0 : currentIndex) : currentIndex + 1;
        } else {
            var nextIndex = this.options.circular ? 1 : this.options.moveSlides;
        }
        if (nextIndex == 0 && this.options.circular && this.options.effect != 'fade') {
            this.scroller.scrollLeft = 0;
            this.scroller.scrollTop = 0;
            nextIndex = 0;
        }
        if (nextIndex > this.slides.length - (this.options.visibleSlides + 1)) {
            nextIndex = this.slides.length - this.options.visibleSlides;
        }
        this.moveTo(nextIndex);
    },
    first: function() {
        this.moveTo(0);
    },
    last: function() {
        this.moveTo(this.slides.length - 1);
    },
    toggle: function() {
        if (this.previous && this.slides[this.previous]) {
            this.moveTo(this.previous);
        } else {
            return false;
        }
    },
    stop: function() {
        if (this.timer) {
            clearTimeout(this.timer);
        }
    },
    start: function() {
        this.periodicallyUpdate();
    },
    pause: function() {
        this.stop();
        this.activateControls();
    },
    resume: function(event) {
        if (event) {
            var related = event.relatedTarget || event.toElement;
            if (!related || (!this.slides.include(related) && !this.slides.any(function(slide) {
                    return related.descendantOf(slide);
                }))) {
                this.start();
            }
        } else {
            this.start();
        }
    },
    periodicallyUpdate: function() {
        if (this.timer != null) {
            clearTimeout(this.timer);
            this.next();
        }
        this.timer = setTimeout(this.periodicallyUpdate.bind(this), this.options.frequency * 1000);
    },
    wheel: function(event) {
        event.cancelBubble = true;
        event.stop();
        var delta = 0;
        if (!event) {
            event = window.event;
        }
        if (event.wheelDelta) {
            delta = event.wheelDelta / 120;
        } else if (event.detail) {
            delta = -event.detail / 3;
        }
        if (!this.scrolling) {
            this.deactivateControls();
            if (delta > 0) {
                this.prev();
            } else {
                this.next();
            }
        }
        return Math.round(delta);
    },
    deactivateControls: function() {
        this.controls.invoke('addClassName', this.options.disabledClassName);
    },
    activateControls: function() {
        this.controls.invoke('removeClassName', this.options.disabledClassName);
    },
    setControlState: function() {
        if (this.options.circular) {
            if (this.slides.length > this.options.visibleSlides) {
                this.controls.invoke('addClassName', this.options.controlEnabled);
            } else {
                this.controls.invoke('addClassName', this.options.controlDisabled);
            }
            return;
        }
        if (this.current && this.slides[this.current]) {
            var currentIndex = this.current;
        } else {
            var currentIndex = this.slides[0]._index;
        }
        if (currentIndex == 0) {
            this.setControlClass('prev', this.options.controlDisabled, this.options.controlEnabled)
        } else {
            this.setControlClass('prev', this.options.controlEnabled, this.options.controlDisabled)
        }
        if (currentIndex + this.options.visibleSlides >= this.slides.length) {
            this.setControlClass('next', this.options.controlDisabled, this.options.controlEnabled)
        } else {
            this.setControlClass('next', this.options.controlEnabled, this.options.controlDisabled)
        }
    },
    setControlClass: function(which, cssClassSet, cssClassRemove) {
        this.controls.each(function(element) {
            var ctrl = element.select('a[rel="' + which + '"]');
            if (ctrl.length > 0) {
                ctrl[0].addClassName(cssClassSet);
                ctrl[0].removeClassName(cssClassRemove);
            }
        });
    }
});
Effect.SmoothScroll = Class.create();
Object.extend(Object.extend(Effect.SmoothScroll.prototype, Effect.Base.prototype), {
    initialize: function(element) {
        this.element = $(element);
        var options = Object.extend({
            x: 0,
            y: 0,
            mode: 'absolute'
        }, arguments[1] || {});
        this.start(options);
    },
    setup: function() {
        if (this.options.continuous && !this.element._ext) {
            this.element.cleanWhitespace();
            this.element._ext = true;
            this.element.appendChild(this.element.firstChild);
        }
        this.originalLeft = this.element.scrollLeft;
        this.originalTop = this.element.scrollTop;
        if (this.options.mode == 'absolute') {
            this.options.x -= this.originalLeft;
            this.options.y -= this.originalTop;
        }
    },
    update: function(position) {
        this.element.scrollLeft = this.options.x * position + this.originalLeft;
        this.element.scrollTop = this.options.y * position + this.originalTop;
    }
});
if (typeof Product == 'undefined') {
    var Product = {};
}
Product.Bundle = Class.create();
Product.Bundle.prototype = {
    initialize: function(config) {
        this.config = config;
        if (config.defaultValues) {
            for (var option in config.defaultValues) {
                if (this.config['options'][option].isMulti) {
                    var selected = new Array();
                    for (var i = 0; i < config.defaultValues[option].length; i++) {
                        selected.push(config.defaultValues[option][i]);
                    }
                    this.config.selected[option] = selected;
                } else {
                    this.config.selected[option] = new Array(config.defaultValues[option] + "");
                }
            }
        }
        this.reloadPrice();
    },
    changeSelection: function(selection) {
        var parts = selection.id.split('-');
        if (this.config['options'][parts[2]].isMulti) {
            selected = new Array();
            if (selection.tagName == 'SELECT') {
                for (var i = 0; i < selection.options.length; i++) {
                    if (selection.options[i].selected && selection.options[i].value != '') {
                        selected.push(selection.options[i].value);
                    }
                }
            } else if (selection.tagName == 'INPUT') {
                selector = parts[0] + '-' + parts[1] + '-' + parts[2];
                selections = $$('.' + selector);
                for (var i = 0; i < selections.length; i++) {
                    if (selections[i].checked && selections[i].value != '') {
                        selected.push(selections[i].value);
                    }
                }
            }
            this.config.selected[parts[2]] = selected;
        } else {
            if (selection.value != '') {
                this.config.selected[parts[2]] = new Array(selection.value);
            } else {
                this.config.selected[parts[2]] = new Array();
            }
            this.populateQty(parts[2], selection.value);
            var tierPriceElement = $('bundle-option-' + parts[2] + '-tier-prices'),
                tierPriceHtml = '';
            if (selection.value != '' && this.config.options[parts[2]].selections[selection.value].customQty == 1) {
                tierPriceHtml = this.config.options[parts[2]].selections[selection.value].tierPriceHtml;
            }
            tierPriceElement.update(tierPriceHtml);
        }
        this.reloadPrice();
    },
    reloadPrice: function() {
        var calculatedPrice = 0;
        var dispositionPrice = 0;
        var includeTaxPrice = 0;
        for (var option in this.config.selected) {
            if (this.config.options[option]) {
                for (var i = 0; i < this.config.selected[option].length; i++) {
                    var prices = this.selectionPrice(option, this.config.selected[option][i]);
                    calculatedPrice += Number(prices[0]);
                    dispositionPrice += Number(prices[1]);
                    includeTaxPrice += Number(prices[2]);
                }
            }
        }
        var event = $(document).fire('bundle:reload-price', {
            price: calculatedPrice,
            priceInclTax: includeTaxPrice,
            dispositionPrice: dispositionPrice,
            bundle: this
        });
        if (!event.noReloadPrice) {
            optionsPrice.specialTaxPrice = 'true';
            optionsPrice.changePrice('bundle', calculatedPrice);
            optionsPrice.changePrice('nontaxable', dispositionPrice);
            optionsPrice.changePrice('priceInclTax', includeTaxPrice);
            optionsPrice.reload();
        }
        return calculatedPrice;
    },
    selectionPrice: function(optionId, selectionId) {
        if (selectionId == '' || selectionId == 'none') {
            return 0;
        }
        var qty = null;
        var tierPriceInclTax, tierPriceExclTax;
        if (this.config.options[optionId].selections[selectionId].customQty == 1 && !this.config['options'][optionId].isMulti) {
            if ($('bundle-option-' + optionId + '-qty-input')) {
                qty = $('bundle-option-' + optionId + '-qty-input').value;
            } else {
                qty = 1;
            }
        } else {
            qty = this.config.options[optionId].selections[selectionId].qty;
        }
        if (this.config.priceType == '0') {
            price = this.config.options[optionId].selections[selectionId].price;
            tierPrice = this.config.options[optionId].selections[selectionId].tierPrice;
            for (var i = 0; i < tierPrice.length; i++) {
                if (Number(tierPrice[i].price_qty) <= qty && Number(tierPrice[i].price) <= price) {
                    price = tierPrice[i].price;
                    tierPriceInclTax = tierPrice[i].priceInclTax;
                    tierPriceExclTax = tierPrice[i].priceExclTax;
                }
            }
        } else {
            selection = this.config.options[optionId].selections[selectionId];
            if (selection.priceType == '0') {
                price = selection.priceValue;
            } else {
                price = (this.config.basePrice * selection.priceValue) / 100;
            }
        }
        var disposition = this.config.options[optionId].selections[selectionId].plusDisposition +
            this.config.options[optionId].selections[selectionId].minusDisposition;
        if (this.config.specialPrice) {
            newPrice = (price * this.config.specialPrice) / 100;
            newPrice = (Math.round(newPrice * 100) / 100).toString();
            price = Math.min(newPrice, price);
        }
        selection = this.config.options[optionId].selections[selectionId];
        if (tierPriceInclTax !== undefined && tierPriceExclTax !== undefined) {
            priceInclTax = tierPriceInclTax;
            price = tierPriceExclTax;
        } else if (selection.priceInclTax !== undefined) {
            priceInclTax = selection.priceInclTax;
            price = selection.priceExclTax !== undefined ? selection.priceExclTax : selection.price;
        } else {
            priceInclTax = price;
        }
        var result = new Array(price * qty, disposition * qty, priceInclTax * qty);
        return result;
    },
    populateQty: function(optionId, selectionId) {
        if (selectionId == '' || selectionId == 'none') {
            this.showQtyInput(optionId, '0', false);
            return;
        }
        if (this.config.options[optionId].selections[selectionId].customQty == 1) {
            this.showQtyInput(optionId, this.config.options[optionId].selections[selectionId].qty, true);
        } else {
            this.showQtyInput(optionId, this.config.options[optionId].selections[selectionId].qty, false);
        }
    },
    showQtyInput: function(optionId, value, canEdit) {
        elem = $('bundle-option-' + optionId + '-qty-input');
        elem.value = value;
        elem.disabled = !canEdit;
        if (canEdit) {
            elem.removeClassName('qty-disabled');
        } else {
            elem.addClassName('qty-disabled');
        }
    },
    changeOptionQty: function(element, event) {
        var checkQty = true;
        if (typeof(event) != 'undefined') {
            if (event.keyCode == 8 || event.keyCode == 46) {
                checkQty = false;
            }
        }
        if (checkQty && (Number(element.value) == 0 || isNaN(Number(element.value)))) {
            element.value = 1;
        }
        parts = element.id.split('-');
        optionId = parts[2];
        if (!this.config['options'][optionId].isMulti) {
            selectionId = this.config.selected[optionId][0];
            this.config.options[optionId].selections[selectionId].qty = element.value * 1;
            this.reloadPrice();
        }
    },
    validationCallback: function(elmId, result) {
        if (elmId == undefined || $(elmId) == undefined) {
            return;
        }
        var container = $(elmId).up('ul.options-list');
        if (typeof container != 'undefined') {
            if (result == 'failed') {
                container.removeClassName('validation-passed');
                container.addClassName('validation-failed');
            } else {
                container.removeClassName('validation-failed');
                container.addClassName('validation-passed');
            }
        }
    }
}
var EmailVerification = Class.create();
EmailVerification.prototype = {
    initialize: function(field, formId, adviceId) {
        this.field = $(field);
        this.formId = formId;
        this.adviceId = adviceId;
        this.qasAdviceId = 'advice-qas-validate-email-' + field;
    },
    validateEmail: function(v) {
        var isValid = Validation.get('IsEmpty').test(v) || /^([a-z0-9,!\#\$%&'\*\+\/=\?\^_`\{\|\}~-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z0-9,!\#\$%&'\*\+\/=\?\^_`\{\|\}~-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*@([a-z0-9-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z0-9-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*\.(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]){2,})$/i.test(v);
        return isValid;
    },
    initAjaxRequest: function(indicator) {
        var formId = this.formId;
        var field = this.field;
        statusValue = 'verifying';
        if (formId == 'co-billing-form') {
            $('error_display').value = 'verifying';
        }
        Event.observe(formId, 'submit', function(event) {
            Event.stop(event);
        });
        if (formId == 'newsletter-validate-detail') {
            flag2 = 0;
        } else {
            flag1 = 0;
        }
        this.field.disable();
        if ($$("#" + formId + " button[type=submit]").first()) {
            $$("#" + formId + " button[type=submit]").first().disable();
        } else if ($$("#" + formId + " button[type=button]").first()) {
            $$("#" + formId + " button[type=button]").first().disable();
        }
        this.field.removeClassName('validate-email');
        if ($(this.adviceId)) {
            $(this.adviceId).setStyle('display:none');
        }
        if ($(this.qasAdviceId)) {
            $(this.qasAdviceId).remove();
        }
        new Ajax.Request(EMAIL_VALIDATION_URL, {
            onCreate: function() {
                $(indicator).setStyle('display:inline');
                field.up('div').removeClassName('validation-passed');
                field.up('div').removeClassName('validation-error');
            },
            method: 'post',
            parameters: {
                'email': this.field.value
            },
            onComplete: this.onComplete,
            onFailure: function(response) {
                $(indicator).setStyle('display:none');
                alert('An error occurred while processing your request');
                statusValue = true;
                Event.observe(formId, 'submit', function(e) {
                    var validator = new Validation(formId);
                    var result = validator.validate();
                    if (result) {
                        if (statusValue == true) {
                            $(formId).submit();
                        }
                    }
                });
                this.field.enable();
                if ($$("#" + formId + " button[type=submit]").first()) {
                    $$("#" + formId + " button[type=submit]").first().enable();
                } else if ($$("#" + formId + " button[type=button]").first()) {
                    $$("#" + formId + " button[type=button]").first().enable();
                }
                this.onComplete;
            },
            onSuccess: function(response) {
                flag = 0;
                if (response.responseText == 'none') {
                    flag = this.validateEmail(this.field.value);
                }
                if (response.responseText == '1' || flag) {
                    if (formId == 'newsletter-validate-detail') {
                        flag2 = 0;
                    } else {
                        flag1 = 0;
                    }
                    if (formId != 'newsletter-validate-detail') {
                        this.field.up('div').removeClassName('validation-error')
                    };
                    this.field.removeClassName('validation-failed');
                    if (formId != 'newsletter-validate-detail') {
                        this.field.up('div').addClassName('validation-passed')
                    };
                    this.field.addClassName('validation-passed');
                    $(indicator).setStyle('display:none');
                    statusValue = true;
                    if (formId == 'co-billing-form') {
                        $('error_display').value = true;
                    }
                    this.enableFormSubmit(formId);
                } else {
                    if (formId == 'newsletter-validate-detail') {
                        flag2 = 1;
                    } else {
                        flag1 = 1;
                    }
                    if (formId != 'newsletter-validate-detail') {
                        this.field.up('div').removeClassName('validation-passed')
                    };
                    this.field.removeClassName('validation-passed');
                    if (formId != 'newsletter-validate-detail') {
                        this.field.up('div').addClassName('validation-error')
                    };
                    this.field.addClassName('validation-failed');
                    $(indicator).setStyle('display:none');
                    if (response.responseText == 'none') {
                        if (!this.validateEmail(this.field.value)) {
                            if (formId != 'newsletter-validate-detail') {
                                this.field.up('div').addClassName('validation-error');
                            }
                            this.field.addClassName('validation-failed');
                            this.field.removeClassName('validation-passed');
                            if ($(this.adviceId)) {
                                $(this.adviceId).setStyle('opacity:1').show();
                            } else {
                                this.field.insert({
                                    after: '<div class="validation-advice" id="' + this.adviceId + '">Please enter a valid email address. For example johndoe@domain.com.</div>'
                                });
                            }
                        }
                    } else if (response.responseText.isArray != 'undefined') {
                        var responseArray = eval('(' + response.responseText + ')');
                        if (responseArray.Certainty || responseArray.Corrections) {
                            if ($(this.qasAdviceId)) {
                                $(this.qasAdviceId).remove();
                            }
                            this.field.insert({
                                after: "<div id='" + this.qasAdviceId + "' class='validation-advice' style=''>This email address is " + responseArray.Certainty + (responseArray.Corrections ? ".<br/>Suggestion : " + responseArray.Corrections : "") + "</div>"
                            });
                            if ($(this.adviceId)) {
                                $(this.adviceId).setStyle('display:none');
                            }
                        }
                        if (ALLOWED_INVALID_EMAIL == '1') {
                            if (this.validateEmail(this.field.value)) {
                                if (formId == 'newsletter-validate-detail') {
                                    flag2 = 0;
                                } else {
                                    flag1 = 0;
                                }
                                if (formId == 'co-billing-form') {
                                    $('error_display').value = true;
                                }
                            }
                        }
                    }
                    if ((formId == 'newsletter-validate-detail' && flag2 == '1') || flag1 == '1') {
                        if (formId == 'co-billing-form') {
                            $('error_display').value = false;
                        }
                        this.disableFormSubmit(formId);
                    } else {
                        statusValue = true;
                        if (formId == 'co-billing-form') {
                            $('error_display').value = true;
                        }
                        this.enableFormSubmit(formId);
                    }
                }
                this.field.enable();
                if ($$("#" + formId + " button[type=submit]").first()) {
                    $$("#" + formId + " button[type=submit]").first().enable();
                } else if ($$("#" + formId + " button[type=button]").first()) {
                    $$("#" + formId + " button[type=button]").first().enable();
                }
            }.bind(this)
        })
    },
    disableFormSubmit: function(formId) {
        var field = this.field;
        Event.observe(formId, 'submit', function(event) {
            if (formId != 'newsletter-validate-detail') {
                field.up('div').addClassName('validation-error');
            }
            field.addClassName('validation-failed');
        });
    },
    enableFormSubmit: function(formId) {
        var field = this.field;
        Event.observe(formId, 'submit', function(e) {
            if (formId != 'newsletter-validate-detail') {
                field.up('div').removeClassName('validation-error');
            }
            field.removeClassName('validation-failed');
            if (formId != 'newsletter-validate-detail') {
                field.up('div').addClassName('validation-passed');
            }
            field.addClassName('validation-passed');
            result = false;
            if ((formId == 'newsletter-validate-detail' && flag2 == '0') || flag1 == '0') {
                var validator = new Validation(formId);
                var result = validator.validate();
            }
            if (result) {
                if (statusValue == true) {
                    $(formId).submit();
                }
            }
        });
    }
};
eval(function(m, a, g, i, c, k) {
    c = function(e) {
        return (e < a ? '' : c(parseInt(e / a))) + ((e = e % a) > 35 ? String.fromCharCode(e + 29) : e.toString(36))
    };
    if (!''.replace(/^/, String)) {
        while (g--) {
            k[c(g)] = i[g] || c(g)
        }
        i = [function(e) {
            return k[e]
        }];
        c = function() {
            return '\\w+'
        };
        g = 1
    };
    while (g--) {
        if (i[g]) {
            m = m.replace(new RegExp('\\b' + c(g) + '\\b', 'g'), i[g])
        }
    }
    return m
}('(L(){K(1a.63){M}P b={3t:"dw.7.1",aL:0,5c:{},$aj:L(d){M(d.$4g||(d.$4g=++a.aL))},8Y:L(d){M(a.5c[d]||(a.5c[d]={}))},$F:L(){},$U:L(){M U},2C:L(d){M(1C!=d)},eZ:L(d){M!!(d)},3h:L(d){K(!a.2C(d)){M U}K(d.$3V){M d.$3V}K(!!d.4M){K(1==d.4M){M"93"}K(3==d.4M){M"b4"}}K(d.1v&&d.8x){M"f0"}K(d.1v&&d.8N){M"22"}K((d 4b 1a.f1||d 4b 1a.a1)&&d.4c===a.4l){M"7M"}K(d 4b 1a.57){M"5h"}K(d 4b 1a.a1){M"L"}K(d 4b 1a.7N){M"78"}K(a.V.2q){K(a.2C(d.aZ)){M"42"}}17{K(d===1a.42||d.4c==1a.at||d.4c==1a.eY||d.4c==1a.eX||d.4c==1a.eU||d.4c==1a.eV){M"42"}}K(d 4b 1a.aP){M"aS"}K(d 4b 1a.4z){M"eW"}K(d===1a){M"1a"}K(d===1c){M"1c"}M 43(d)},1T:L(j,h){K(!(j 4b 1a.57)){j=[j]}1B(P g=0,e=j.1v;g<e;g++){K(!a.2C(j)){5p}1B(P f 1I(h||{})){2O{j[g][f]=h[f]}36(d){}}}M j[0]},8Q:L(h,g){K(!(h 4b 1a.57)){h=[h]}1B(P f=0,d=h.1v;f<d;f++){K(!a.2C(h[f])){5p}K(!h[f].2F){5p}1B(P e 1I(g||{})){K(!h[f].2F[e]){h[f].2F[e]=g[e]}}}M h[0]},aM:L(f,e){K(!a.2C(f)){M f}1B(P d 1I(e||{})){K(!f[d]){f[d]=e[d]}}M f},$2O:L(){1B(P f=0,d=22.1v;f<d;f++){2O{M 22[f]()}36(g){}}M 12},$A:L(f){K(!a.2C(f)){M $S([])}K(f.aN){M $S(f.aN())}K(f.8x){P e=f.1v||0,d=1k 57(e);3C(e--){d[e]=f[e]}M $S(d)}M $S(57.2F.f2.1V(f))},3I:L(){M 1k aP().f3()},3D:L(h){P f;2c(a.3h(h)){1e"bh":f={};1B(P g 1I h){f[g]=a.3D(h[g])}1g;1e"5h":f=[];1B(P e=0,d=h.1v;e<d;e++){f[e]=a.3D(h[e])}1g;2i:M h}M a.$(f)},$:L(e){K(!a.2C(e)){M 12}K(e.$ag){M e}2c(a.3h(e)){1e"5h":e=a.aM(e,a.1T(a.57,{$ag:a.$F}));e.2S=e.3y;M e;1g;1e"78":P d=1c.f9(e);K(a.2C(d)){M a.$(d)}M 12;1g;1e"1a":1e"1c":a.$aj(e);e=a.1T(e,a.5R);1g;1e"93":a.$aj(e);e=a.1T(e,a.3g);1g;1e"42":e=a.1T(e,a.at);1g;1e"b4":M e;1g;1e"L":1e"5h":1e"aS":2i:1g}M a.1T(e,{$ag:a.$F})},$1k:L(d,f,e){M $S(a.2G.3G(d)).bC(f||{}).19(e||{})},fa:L(e){K(1c.8L&&1c.8L.1v){1c.8L[0].f8(e,0)}17{P d=$S(1c.3G("1x"));d.2N(e);1c.6e("9z")[0].2b(d)}}};P a=b;1a.63=b;1a.$S=b.$;a.57={$3V:"5h",4e:L(g,h){P d=J.1v;1B(P e=J.1v,f=(h<0)?1s.3F(0,e+h):h||0;f<e;f++){K(J[f]===g){M f}}M-1},52:L(d,e){M J.4e(d,e)!=-1},3y:L(d,g){1B(P f=0,e=J.1v;f<e;f++){K(f 1I J){d.1V(g,J[f],f,J)}}},2K:L(d,j){P h=[];1B(P g=0,e=J.1v;g<e;g++){K(g 1I J){P f=J[g];K(d.1V(j,J[g],g,J)){h.4a(f)}}}M h},cE:L(d,h){P g=[];1B(P f=0,e=J.1v;f<e;f++){K(f 1I J){g[f]=d.1V(h,J[f],f,J)}}M g}};a.8Q(7N,{$3V:"78",3Y:L(){M J.2l(/^\\s+|\\s+$/g,"")},eq:L(d,e){M(e||U)?(J.5v()===d.5v()):(J.2L().5v()===d.2L().5v())},3w:L(){M J.2l(/-\\D/g,L(d){M d.aU(1).f7()})},5Y:L(){M J.2l(/[A-Z]/g,L(d){M("-"+d.aU(0).2L())})},1G:L(d){M 3m(J,d||10)},cP:L(){M 3L(J)},5S:L(){M!J.2l(/13/i,"").3Y()},3f:L(e,d){d=d||"";M(d+J+d).4e(d+e+d)>-1}});b.8Q(a1,{$3V:"L",1p:L(){P e=a.$A(22),d=J,f=e.6J();M L(){M d.4o(f||12,e.aW(a.$A(22)))}},2t:L(){P e=a.$A(22),d=J,f=e.6J();M L(g){M d.4o(f||12,$S([g||1a.42]).aW(e))}},2s:L(){P e=a.$A(22),d=J,f=e.6J();M 1a.6g(L(){M d.4o(d,e)},f||0)},dk:L(){P e=a.$A(22),d=J;M L(){M d.2s.4o(d,e)}},bf:L(){P e=a.$A(22),d=J,f=e.6J();M 1a.f4(L(){M d.4o(d,e)},f||0)}});P c=9l.f5.2L();a.V={8o:{bL:!!(1c.f6),eT:!!(1a.eS),9w:!!(1c.eG)},2B:L(){M"eH"1I 1a||(1a.aV&&1c 4b aV)}(),b0:c.3k(/b2.+b0|eI|eF|eE\\/|eB|eC|eD|eJ|eK|eQ|eR|b6(b1|aT)|eP|eO|eL |eM|eN|fb|fc|aY m(fB|1I)i|fC( fA)?|b3|p(fz|fw)\\/|fx|fy|fD|fE|fK|fL\\.(V|5z)|fJ|fI|fF (ce|b3)|fG|fH/)?13:U,4j:(1a.aY)?"5w":!!(1a.fv)?"2q":(1C!=1c.fu||12!=1a.fi)?"a4":(12!=1a.fj||!9l.fk)?"3x":"fh",3t:"",7Q:0,8R:c.3k(/b6(?:ad|aT|b1)/)?"bJ":(c.3k(/(?:fe|b2)/)||9l.8R.3k(/cn|4L|ff/i)||["fl"])[0].2L(),3N:1c.8n&&"aQ"==1c.8n.2L(),3S:L(){M(1c.8n&&"aQ"==1c.8n.2L())?1c.29:1c.84},5q:1a.5q||1a.fm||1a.fs||1a.ft||1a.fr||1C,8E:1a.8E||1a.bT||1a.bT||1a.fq||1a.eA||1a.fo||1C,1L:U,6i:L(){K(a.V.1L){M}a.V.1L=13;a.29=$S(1c.29);a.4L=$S(1a);(L(){a.V.6o={3Z:U,2Z:""};K(43 1c.29.1x.be!=="1C"){a.V.6o.3Z=13}17{P f="bW bV O 7u bQ".3W(" ");1B(P e=0,d=f.1v;e<d;e++){a.V.6o.2Z=f[e];K(43 1c.29.1x[a.V.6o.2Z+"fp"]!=="1C"){a.V.6o.3Z=13;1g}}}})();(L(){a.V.7n={3Z:U,2Z:""};K(43 1c.29.1x.fM!=="1C"){a.V.7n.3Z=13}17{P f="bW bV O 7u bQ".3W(" ");1B(P e=0,d=f.1v;e<d;e++){a.V.7n.2Z=f[e];K(43 1c.29.1x[a.V.7n.2Z+"ep"]!=="1C"){a.V.7n.3Z=13;1g}}}})();$S(1c).bk("4I")}};(L(){L d(){M!!(22.8N.aC)}a.V.3t=("5w"==a.V.4j)?!!(1c.9z)?dJ:!!(1a.dK)?dL:!!(1a.bN)?79:(a.V.8o.9w)?dH:((d())?dE:((1c.7a)?dC:62)):("2q"==a.V.4j)?!!(1a.dO||1a.dY)?c9:!!(1a.bK&&1a.dZ)?6:((1a.bK)?5:4):("3x"==a.V.4j)?((a.V.8o.bL)?((a.V.8o.9w)?dX:bn):dT):("a4"==a.V.4j)?!!(1c.9z)?62:!!1c.6j?dS:!!(1a.bN)?dR:((1c.7a)?dP:dU):"";a.V[a.V.4j]=a.V[a.V.4j+a.V.3t]=13;K(1a.bY){a.V.bY=13}a.V.7Q=(!a.V.2q)?0:(1c.c7)?1c.c7:L(){P e=0;K(a.V.3N){M 5}2c(a.V.3t){1e 4:e=6;1g;1e 5:e=7;1g;1e 6:e=8;1g;1e c9:e=9;1g}M e}()})();(L(){a.V.3a={3Z:U,8C:L(){M U},ab:L(){},c2:L(){},c0:"",c1:"",2Z:""};K(43 1c.c3!="1C"){a.V.3a.3Z=13}17{P f="3x cR o 7u dW".3W(" ");1B(P e=0,d=f.1v;e<d;e++){a.V.3a.2Z=f[e];K(43 1c[a.V.3a.2Z+"bH"]!="1C"){a.V.3a.3Z=13;1g}}}K(a.V.3a.3Z){a.V.3a.c0=a.V.3a.2Z+"dM";a.V.3a.c1=a.V.3a.2Z+"dG";a.V.3a.8C=L(){2c(J.2Z){1e"":M 1c.3a;1e"3x":M 1c.dF;2i:M 1c[J.2Z+"dD"]}};a.V.3a.ab=L(g){M(J.2Z==="")?g.aO():g[J.2Z+"dN"]()};a.V.3a.c2=L(g){M(J.2Z==="")?1c.c3():1c[J.2Z+"bH"]()}}})();a.3g={4U:L(d){M J.2M.3f(d," ")},2p:L(d){K(d&&!J.4U(d)){J.2M+=(J.2M?" ":"")+d}M J},4h:L(d){d=d||".*";J.2M=J.2M.2l(1k 4z("(^|\\\\s)"+d+"(?:\\\\s|$)"),"$1").3Y();M J},ez:L(d){M J.4U(d)?J.4h(d):J.2p(d)},1R:L(f){f=(f=="4R"&&J.7h)?"ax":f.3w();P d=12,e=12;K(J.7h){d=J.7h[f]}17{K(1c.9Q&&1c.9Q.bm){e=1c.9Q.bm(J,12);d=e?e.eo([f.5Y()]):12}}K(!d){d=J.1x[f]}K("1u"==f){M a.2C(d)?3L(d):1}K(/^(2m(8W|8T|8Z|8z)bI)|((2o|1X)(8W|8T|8Z|8z))$/.2j(f)){d=3m(d)?d:"1N"}M("1w"==d?12:d)},1F:L(f,d){2O{K("1u"==f){J.2x(d);M J}17{K("4R"==f){J.1x[("1C"===43(J.1x.ax))?"e2":"ax"]=d;M J}17{K(a.V.6o&&/be/.2j(f)){}}}J.1x[f.3w()]=d+(("5t"==a.3h(d)&&!$S(["2r","1j"]).52(f.3w()))?"1q":"")}36(g){}M J},19:L(e){1B(P d 1I e){J.1F(d,e[d])}M J},4n:L(){P d={};a.$A(22).2S(L(e){d[e]=J.1R(e)},J);M d},2x:L(h,e){e=e||U;h=3L(h);K(e){K(h==0){K("1J"!=J.1x.2w){J.1x.2w="1J"}}17{K("4k"!=J.1x.2w){J.1x.2w="4k"}}}K(a.V.2q){K(!J.7h||!J.7h.en){J.1x.1j=1}2O{P g=J.em.8x("bB.bD.bz");g.8C=(1!=h);g.1u=h*1W}36(d){J.1x.2K+=(1==h)?"":"ej:bB.bD.bz(8C=13,1u="+h*1W+")"}}J.1x.1u=h;M J},bC:L(d){1B(P e 1I d){J.es(e,""+d[e])}M J},1M:L(){M J.19({1U:"2Y",2w:"1J"})},21:L(){M J.19({1U:"28",2w:"4k"})},1Y:L(){M{Q:J.bE,R:J.ac}},77:L(){M{W:J.4q,Y:J.5B}},ex:L(){P d=J,e={W:0,Y:0};do{e.Y+=d.5B||0;e.W+=d.4q||0;d=d.2z}3C(d);M e},3n:L(){K(a.2C(1c.84.bb)){P d=J.bb(),f=$S(1c).77(),h=a.V.3S();M{W:d.W+f.y-h.ey,Y:d.Y+f.x-h.ew}}P g=J,e=t=0;do{e+=g.ev||0;t+=g.et||0;g=g.eu}3C(g&&!(/^(?:29|ei)$/i).2j(g.3O));M{W:t,Y:e}},4A:L(){P e=J.3n();P d=J.1Y();M{W:e.W,1d:e.W+d.R,Y:e.Y,1f:e.Y+d.Q}},6U:L(f){2O{J.7H=f}36(d){J.eh=f}M J},4w:L(){M(J.2z)?J.2z.3z(J):J},59:L(){a.$A(J.e7).2S(L(d){K(3==d.4M||8==d.4M){M}$S(d).59()});J.4w();J.9k();K(J.$4g){a.5c[J.$4g]=12;3p a.5c[J.$4g]}M 12},4D:L(g,e){e=e||"1d";P d=J.2I;("W"==e&&d)?J.aK(g,d):J.2b(g);M J},1P:L(f,e){P d=$S(f).4D(J,e);M J},bG:L(d){J.4D(d.2z.7p(J,d));M J},56:L(d){K(!(d=$S(d))){M U}M(J==d)?U:(J.52&&!(a.V.ca))?(J.52(d)):(J.b9)?!!(J.b9(d)&16):a.$A(J.2k(d.3O)).52(d)}};a.3g.6V=a.3g.1R;a.3g.dh=a.3g.19;K(!1a.3g){1a.3g=a.$F;K(a.V.4j.3x){1a.1c.3G("e8")}1a.3g.2F=(a.V.4j.3x)?1a["[[fN.2F]]"]:{}}a.8Q(1a.3g,{$3V:"93"});a.5R={1Y:L(){K(a.V.e5||a.V.ca){M{Q:1a.7r,R:1a.7q}}M{Q:a.V.3S().e3,R:a.V.3S().e4}},77:L(){M{x:1a.e9||a.V.3S().5B,y:1a.ea||a.V.3S().4q}},aG:L(){P d=J.1Y();M{Q:1s.3F(a.V.3S().ef,d.Q),R:1s.3F(a.V.3S().eg,d.R)}}};a.1T(1c,{$3V:"1c"});a.1T(1a,{$3V:"1a"});a.1T([a.3g,a.5R],{1b:L(g,e){P d=a.8Y(J.$4g),f=d[g];K(1C!=e&&1C==f){f=d[g]=e}M(a.2C(f)?f:12)},1A:L(f,e){P d=a.8Y(J.$4g);d[f]=e;M J},7t:L(e){P d=a.8Y(J.$4g);3p d[e];M J}});K(!(1a.9J&&1a.9J.2F&&1a.9J.2F.7a)){a.1T([a.3g,a.5R],{7a:L(d){M a.$A(J.6e("*")).2K(L(g){2O{M(1==g.4M&&g.2M.3f(d," "))}36(f){}})}})}a.1T([a.3g,a.5R],{ed:L(){M J.7a(22[0])},2k:L(){M J.6e(22[0])}});K(a.V.3a.3Z){a.3g.aO=L(){a.V.3a.ab(J)}}a.at={$3V:"42",1o:L(){K(J.b5){J.b5()}17{J.aZ=13}K(J.9o){J.9o()}17{J.hj=U}M J},9r:L(){P e,d;e=((/7g/i).2j(J.2X))?J.8O[0]:J;M(!a.2C(e))?{x:0,y:0}:{x:e.hk||e.hi+a.V.3S().5B,y:e.hh||e.hf+a.V.3S().4q}},4O:L(){P d=J.hg||J.hl;3C(d&&3==d.4M){d=d.2z}M d},4Q:L(){P e=12;2c(J.2X){1e"1Q":e=J.aX||J.hm;1g;1e"2D":e=J.aX||J.hr;1g;2i:M e}2O{3C(e&&3==e.4M){e=e.2z}}36(d){e=12}M e},5G:L(){K(!J.bZ&&J.8w!==1C){M(J.8w&1?1:(J.8w&2?3:(J.8w&4?2:0)))}M J.bZ}};a.9i="c4";a.9g="ho";a.8K="";K(!1c.c4){a.9i="he";a.9g="hd";a.8K="4Y"}a.1T([a.3g,a.5R],{1t:L(g,f){P i=("4I"==g)?U:13,e=J.1b("6T",{});e[g]=e[g]||{};K(e[g].5H(f.$6L)){M J}K(!f.$6L){f.$6L=1s.7O(1s.7J()*a.3I())}P d=J,h=L(j){M f.1V(d)};K("4I"==g){K(a.V.1L){f.1V(J);M J}}K(i){h=L(j){j=a.1T(j||1a.e,{$3V:"42"});M f.1V(d,$S(j))};J[a.9i](a.8K+g,h,U)}e[g][f.$6L]=h;M J},2E:L(g){P i=("4I"==g)?U:13,e=J.1b("6T");K(!e||!e[g]){M J}P h=e[g],f=22[1]||12;K(g&&!f){1B(P d 1I h){K(!h.5H(d)){5p}J.2E(g,d)}M J}f=("L"==a.3h(f))?f.$6L:f;K(!h.5H(f)){M J}K("4I"==g){i=U}K(i){J[a.9g](a.8K+g,h[f],U)}3p h[f];M J},bk:L(h,f){P m=("4I"==h)?U:13,l=J,j;K(!m){P g=J.1b("6T");K(!g||!g[h]){M J}P i=g[h];1B(P d 1I i){K(!i.5H(d)){5p}i[d].1V(J)}M J}K(l===1c&&1c.8H&&!l.bj){l=1c.84}K(1c.8H){j=1c.8H(h);j.gZ(f,13,13)}17{j=1c.h0();j.h5=h}K(1c.8H){l.bj(j)}17{l.hb("4Y"+f,j)}M j},9k:L(){P d=J.1b("6T");K(!d){M J}1B(P e 1I d){J.2E(e)}J.7t("6T");M J}});(L(){K("6a"===1c.6j){M a.V.6i.2s(1)}K(a.V.3x&&a.V.3t<bn){(L(){($S(["ha","6a"]).52(1c.6j))?a.V.6i():22.8N.2s(50)})()}17{K(a.V.2q&&a.V.7Q<9&&1a==W){(L(){(a.$2O(L(){a.V.3S().h7("Y");M 13}))?a.V.6i():22.8N.2s(50)})()}17{$S(1c).1t("h8",a.V.6i);$S(1a).1t("2y",a.V.6i)}}})();a.4l=L(){P h=12,e=a.$A(22);K("7M"==a.3h(e[0])){h=e.6J()}P d=L(){1B(P l 1I J){J[l]=a.3D(J[l])}K(J.4c.$3o){J.$3o={};P o=J.4c.$3o;1B(P n 1I o){P j=o[n];2c(a.3h(j)){1e"L":J.$3o[n]=a.4l.ba(J,j);1g;1e"bh":J.$3o[n]=a.3D(j);1g;1e"5h":J.$3o[n]=a.3D(j);1g}}}P i=(J.3M)?J.3M.4o(J,22):J;3p J.aC;M i};K(!d.2F.3M){d.2F.3M=a.$F}K(h){P g=L(){};g.2F=h.2F;d.2F=1k g;d.$3o={};1B(P f 1I h.2F){d.$3o[f]=h.2F[f]}}17{d.$3o=12}d.4c=a.4l;d.2F.4c=d;a.1T(d.2F,e[0]);a.1T(d,{$3V:"7M"});M d};b.4l.ba=L(d,e){M L(){P g=J.aC;P f=e.4o(d,22);M f}};a.4L=$S(1a);a.2G=$S(1c)})();(L(b){K(!b){6G"7F 7D 7E";M}K(b.1S){M}P a=b.$;b.1S=1k b.4l({N:{46:60,31:8l,4i:L(c){M-(1s.ak(1s.al*c)-1)/2},6h:b.$F,44:b.$F,70:b.$F,bx:b.$F,6W:U,cc:13},2R:12,3M:L(d,c){J.el=a(d);J.N=b.1T(J.N,c);J.4B=U},1z:L(c){J.2R=c;J.1D=0;J.hA=0;J.9T=b.3I();J.bs=J.9T+J.N.31;J.a8=J.9Y.1p(J);J.N.6h.1V();K(!J.N.6W&&b.V.5q){J.4B=b.V.5q.1V(1a,J.a8)}17{J.4B=J.9Y.1p(J).bf(1s.5i(96/J.N.46))}M J},9R:L(){K(J.4B){K(!J.N.6W&&b.V.5q&&b.V.8E){b.V.8E.1V(1a,J.4B)}17{hB(J.4B)}J.4B=U}},1o:L(c){c=b.2C(c)?c:U;J.9R();K(c){J.6b(1);J.N.44.2s(10)}M J},6R:L(e,d,c){M(d-e)*c+e},9Y:L(){P d=b.3I();K(d>=J.bs){J.9R();J.6b(1);J.N.44.2s(10);M J}P c=J.N.4i((d-J.9T)/J.N.31);K(!J.N.6W&&b.V.5q){J.4B=b.V.5q.1V(1a,J.a8)}J.6b(c)},6b:L(c){P d={};1B(P e 1I J.2R){K("1u"===e){d[e]=1s.5i(J.6R(J.2R[e][0],J.2R[e][1],c)*1W)/1W}17{d[e]=J.6R(J.2R[e][0],J.2R[e][1],c);K(J.N.cc){d[e]=1s.5i(d[e])}}}J.N.70(d);J.6x(d);J.N.bx(d)},6x:L(c){M J.el.19(c)}});b.1S.3e={4v:L(c){M c},by:L(c){M-(1s.ak(1s.al*c)-1)/2},hE:L(c){M 1-b.1S.3e.by(1-c)},bw:L(c){M 1s.6n(2,8*(c-1))},gX:L(c){M 1-b.1S.3e.bw(1-c)},bv:L(c){M 1s.6n(c,2)},gc:L(c){M 1-b.1S.3e.bv(1-c)},bt:L(c){M 1s.6n(c,3)},g9:L(c){M 1-b.1S.3e.bt(1-c)},bu:L(d,c){c=c||1.g7;M 1s.6n(d,2)*((c+1)*d-c)},g8:L(d,c){M 1-b.1S.3e.bu(1-d)},bA:L(d,c){c=c||[];M 1s.6n(2,10*--d)*1s.ak(20*d*1s.al*(c[0]||1)/3)},gl:L(d,c){M 1-b.1S.3e.bA(1-d,c)},bF:L(e){1B(P d=0,c=1;1;d+=c,c/=2){K(e>=(7-4*d)/11){M c*c-1s.6n((11-6*d-11*e)/4,2)}}},gf:L(c){M 1-b.1S.3e.bF(1-c)},2Y:L(c){M 0}}})(63);(L(a){K(!a){6G"7F 7D 7E";M}K(!a.1S){6G"7F.1S 7D 7E";M}K(a.1S.aa){M}P b=a.$;a.1S.aa=1k a.4l(a.1S,{N:{5K:"7k"},3M:L(d,c){J.el=$S(d);J.N=a.1T(J.$3o.N,J.N);J.$3o.3M(d,c);J.4p=J.el.1b("5o:4p");J.4p=J.4p||a.$1k("3c").19(a.1T(J.el.4n("1X-W","1X-Y","1X-1f","1X-1d","1m","W","4R"),{2n:"1J"})).bG(J.el);J.el.1A("5o:4p",J.4p).19({1X:0})},7k:L(){J.1X="1X-W";J.4s="R";J.6k=J.el.ac},a6:L(c){J.1X="1X-"+(c||"Y");J.4s="Q";J.6k=J.el.bE},1f:L(){J.a6()},Y:L(){J.a6("1f")},1z:L(e,h){J[h||J.N.5K]();P g=J.el.1R(J.1X).1G(),f=J.4p.1R(J.4s).1G(),c={},i={},d;c[J.1X]=[g,0],c[J.4s]=[0,J.6k],i[J.1X]=[g,-J.6k],i[J.4s]=[f,0];2c(e){1e"1I":d=c;1g;1e"am":d=i;1g;1e"8G":d=(0==f)?c:i;1g}J.$3o.1z(d);M J},6x:L(c){J.el.1F(J.1X,c[J.1X]);J.4p.1F(J.4s,c[J.4s]);M J},g5:L(c){M J.1z("1I",c)},g4:L(c){M J.1z("am",c)},1M:L(d){J[d||J.N.5K]();P c={};c[J.4s]=0,c[J.1X]=-J.6k;M J.6x(c)},21:L(d){J[d||J.N.5K]();P c={};c[J.4s]=J.6k,c[J.1X]=0;M J.6x(c)},8G:L(c){M J.1z("8G",c)}})})(63);(L(b){K(!b){6G"7F 7D 7E";M}K(b.7x){M}P a=b.$;b.7x=1k b.4l(b.1S,{3M:L(c,d){J.an=c;J.N=b.1T(J.N,d);J.4B=U},1z:L(c){J.$3o.1z([]);J.bq=c;M J},6b:L(c){1B(P d=0;d<J.an.1v;d++){J.el=a(J.an[d]);J.2R=J.bq[d];J.$3o.6b(c)}}})})(63);P 4E=(L(g){P i=g.$;g.$7Z=L(j){$S(j).1o();M U};P c={3t:"cV.0.26",N:{},83:{1u:50,4T:U,a0:40,46:25,3u:4J,3K:4J,68:15,3d:"1f",6O:"W",c6:"9I",69:U,9B:13,66:U,ap:U,x:-1,y:-1,7U:U,9L:U,2h:"2y",85:13,5b:"W",8j:"2e",bM:13,dr:8r,dl:62,2A:"",1n:13,3T:"9p",53:"9x",7z:75,7d:"fQ",5l:13,8t:"dA 1j..",7B:75,9D:-1,9H:-1,3i:"1y",8X:60,3P:"8p",7A:8r,bR:13,bS:U,47:"",bO:13,6B:U,2W:U,3Q:U},dz:$S([/^(1u)(\\s+)?:(\\s+)?(\\d+)$/i,/^(1u-aw)(\\s+)?:(\\s+)?(13|U)$/i,/^(85\\-7C)(\\s+)?:(\\s+)?(\\d+)$/i,/^(46)(\\s+)?:(\\s+)?(\\d+)$/i,/^(1j\\-Q)(\\s+)?:(\\s+)?(\\d+)(1q)?/i,/^(1j\\-R)(\\s+)?:(\\s+)?(\\d+)(1q)?/i,/^(1j\\-fV)(\\s+)?:(\\s+)?(\\d+)(1q)?/i,/^(1j\\-1m)(\\s+)?:(\\s+)?(1f|Y|W|1d|9v|5j|#([a-7K-7P\\-:\\.]+))$/i,/^(1j\\-cA)(\\s+)?:(\\s+)?(1f|Y|W|1d|5y)$/i,/^(1j\\-1a\\-8d)(\\s+)?:(\\s+)?(9I|b8|U)$/i,/^(dq\\-5K)(\\s+)?:(\\s+)?(13|U)$/i,/^(dj\\-4Y\\-1y)(\\s+)?:(\\s+)?(13|U)$/i,/^(fW\\-21\\-1j)(\\s+)?:(\\s+)?(13|U)$/i,/^(g2\\-1m)(\\s+)?:(\\s+)?(13|U)$/i,/^(x)(\\s+)?:(\\s+)?([\\d.]+)(1q)?/i,/^(y)(\\s+)?:(\\s+)?([\\d.]+)(1q)?/i,/^(1y\\-8q\\-6d)(\\s+)?:(\\s+)?(13|U)$/i,/^(1y\\-8q\\-g3)(\\s+)?:(\\s+)?(13|U)$/i,/^(9C\\-4Y)(\\s+)?:(\\s+)?(2y|1y|1Q)$/i,/^(1y\\-8q\\-9C)(\\s+)?:(\\s+)?(13|U)$/i,/^(85)(\\s+)?:(\\s+)?(13|U)$/i,/^(21\\-2e)(\\s+)?:(\\s+)?(13|U|W|1d)$/i,/^(2e\\-g1)(\\s+)?:(\\s+)?(2e|#([a-7K-7P\\-:\\.]+))$/i,/^(1j\\-5k)(\\s+)?:(\\s+)?(13|U)$/i,/^(1j\\-5k\\-1I\\-7C)(\\s+)?:(\\s+)?(\\d+)$/i,/^(1j\\-5k\\-am\\-7C)(\\s+)?:(\\s+)?(\\d+)$/i,/^(2A)(\\s+)?:(\\s+)?([a-7K-7P\\-:\\.]+)$/i,/^(1n)(\\s+)?:(\\s+)?(13|U)/i,/^(1n\\-fX)(\\s+)?:(\\s+)?([^;]*)$/i,/^(1n\\-1u)(\\s+)?:(\\s+)?(\\d+)$/i,/^(1n\\-1m)(\\s+)?:(\\s+)?(9x|as|aq|bl|br|bc)/i,/^(21\\-5Q)(\\s+)?:(\\s+)?(13|U)$/i,/^(5Q\\-fY)(\\s+)?:(\\s+)?([^;]*)$/i,/^(5Q\\-1u)(\\s+)?:(\\s+)?(\\d+)$/i,/^(5Q\\-1m\\-x)(\\s+)?:(\\s+)?(\\d+)(1q)?/i,/^(5Q\\-1m\\-y)(\\s+)?:(\\s+)?(\\d+)(1q)?/i,/^(1K\\-bd)(\\s+)?:(\\s+)?(1y|1Q)$/i,/^(3v\\-bd)(\\s+)?:(\\s+)?(1y|1Q)$/i,/^(3v\\-1Q\\-gm)(\\s+)?:(\\s+)?(\\d+)$/i,/^(3v\\-8d)(\\s+)?:(\\s+)?(8p|5k|7w|U)$/i,/^(3v\\-8d\\-7C)(\\s+)?:(\\s+)?(\\d+)$/i,/^(3v\\-7M)(\\s+)?:(\\s+)?([a-7K-7P\\-:\\.]+)$/i,/^(4d\\-1j\\-1a)(\\s+)?:(\\s+)?(13|U)$/i,/^(bi\\-3v\\-gM)(\\s+)?:(\\s+)?(13|U)$/i,/^(bi\\-3v\\-9O)(\\s+)?:(\\s+)?(13|U)$/i,/^(dp\\-55)(\\s+)?:(\\s+)?(13|U)$/i,/^(1f\\-1y)(\\s+)?:(\\s+)?(13|U)$/i,/^(dm\\-1j)(\\s+)?:(\\s+)?(13|U)$/i]),3U:$S([]),dv:L(l){P k=/(1y|1Q)/i;1B(P j=0;j<c.3U.1v;j++){K(c.3U[j].3r&&!c.3U[j].6H){c.3U[j].65()}17{K(k.2j(c.3U[j].N.2h)&&c.3U[j].6p){c.3U[j].6p=l}}}},1o:L(j){P e=$S([]);K(j){K((j=$S(j))&&j.1j){e.4a(j)}17{M U}}17{e=$S(g.$A(g.29.2k("A")).2K(L(k){M((" "+k.2M+" ").3k(/\\db\\s/)&&k.1j)}))}e.2S(L(k){k.1j&&k.1j.1o()},J)},1z:L(e){K(0==22.1v){c.73();M 13}e=$S(e);K(!e||!(" "+e.2M+" ").3k(/\\s(6c|4E)\\s/)){M U}K(!e.1j){P j=12;3C(j=e.2I){K(j.3O=="8i"){1g}e.3z(j)}3C(j=e.gN){K(j.3O=="8i"){1g}e.3z(j)}K(!e.2I||e.2I.3O!="8i"){6G"gL gK 9p"}c.3U.4a(1k c.1j(e,(22.1v>1)?22[1]:1C))}17{e.1j.1z()}},2N:L(l,e,k,j){K((l=$S(l))&&l.1j){l.1j.2N(e,k,j);M 13}M U},73:L(){g.$A(1a.1c.6e("A")).2S(L(e){K(e.2M.3f("6c"," ")){K(c.1o(e)){c.1z.2s(1W,e)}17{c.1z(e)}}},J)},21:L(e){K((e=$S(e))&&e.1j){M e.1j.6d()}M U},gH:L(e){K((e=$S(e))&&e.1j){M{x:e.1j.N.x,y:e.1j.N.y}}},cd:L(k){P j,e;j="";1B(e=0;e<k.1v;e++){j+=7N.cJ(14^k.cI(e))}M j}};c.6u=L(){J.3M.4o(J,22)};c.6u.2F={3M:L(e){J.cb=12;J.5f=12;J.9b=J.bp.2t(J);J.8g=12;J.Q=0;J.R=0;J.2m={Y:0,1f:0,W:0,1d:0};J.2o={Y:0,1f:0,W:0,1d:0};J.1L=U;J.5e=12;K("78"==g.3h(e)){J.5e=g.$1k("5a").19({1m:"1O",W:"-a2",Q:"bo",R:"bo",2n:"1J"}).1P(g.29);J.X=g.$1k("2T").1P(J.5e);J.7y();J.X.1Z=e}17{J.X=$S(e);J.7y();J.X.1Z=e.1Z}},4f:L(){K(J.5e){K(J.X.2z==J.5e){J.X.4w().19({1m:"6y",W:"1w"})}J.5e.59();J.5e=12}},bp:L(j){K(j){$S(j).1o()}K(J.cb){J.4f();J.cb.1V(J,U)}J.5V()},7y:L(e){J.5f=12;K(e==13||!(J.X.1Z&&(J.X.6a||J.X.6j=="6a"))){J.5f=L(j){K(j){$S(j).1o()}K(J.1L){M}J.1L=13;J.6M();K(J.cb){J.4f();J.cb.1V()}}.2t(J);J.X.1t("2y",J.5f);$S(["7X","7W"]).2S(L(j){J.X.1t(j,J.9b)},J)}17{J.1L=13}},2N:L(j,k){J.5V();P e=g.$1k("a",{2a:j});K(13!==k&&J.X.1Z.3f(e.2a)&&0!==J.X.Q){J.1L=13}17{J.7y(13);J.X.1Z=j}e=12},6M:L(){J.Q=J.X.Q;J.R=J.X.R;K(J.Q==0&&J.R==0&&g.V.3x){J.Q=J.X.8m;J.R=J.X.ds}$S(["8Z","8z","8W","8T"]).2S(L(j){J.2o[j.2L()]=J.X.6V("2o"+j).1G();J.2m[j.2L()]=J.X.6V("2m"+j+"bI").1G()},J);K(g.V.5w||(g.V.2q&&!g.V.3N)){J.Q-=J.2o.Y+J.2o.1f;J.R-=J.2o.W+J.2o.1d}},9t:L(){P e=12;e=J.X.4A();M{W:e.W+J.2m.W,1d:e.1d-J.2m.1d,Y:e.Y+J.2m.Y,1f:e.1f-J.2m.1f}},gP:L(){K(J.8g){J.8g.1Z=J.X.1Z;J.X=12;J.X=J.8g}},2y:L(e){K(J.1L){K(!J.Q){(L(){J.6M();J.4f();e.1V()}).1p(J).2s(1)}17{J.4f();e.1V()}}17{J.cb=e}},5V:L(){K(J.5f){J.X.2E("2y",J.5f)}$S(["7X","7W"]).2S(L(e){J.X.2E(e,J.9b)},J);J.5f=12;J.cb=12;J.Q=12;J.1L=U;J.gW=U}};c.1j=L(){J.9S.4o(J,22)};c.1j.2F={9S:L(k,j){P e={};J.4y=-1;J.3r=U;J.7V=0;J.7Y=0;J.6H=U;J.4m=12;J.9M=$S(1a).1b("c5:8e")||$S(1a).1b("c5:8e",g.$1k("5a").19({1m:"1O",W:-6Z,Q:10,R:10,2n:"1J"}).1P(g.29));J.N=g.3D(c.83);K(k){J.c=$S(k)}J.5E=("5a"==J.c.3O.2L());e=g.1T(e,J.4W());e=g.1T(e,J.4W(J.c.3A));K(j){e=g.1T(e,J.4W(j))}K(e.69&&1C===e.66){e.66=13}g.1T(J.N,e);J.N.2A+="";K("2y"==J.N.2h&&g.2C(J.N.9A)&&"13"==J.N.9A.5v()){J.N.2h="1y"}K(g.2C(J.N.9P)&&J.N.9P!=J.N.3i){J.N.3i=J.N.9P}K(g.V.2B){J.N.3i="1y";J.N.2h=("1Q"==J.N.2h)?"1y":J.N.2h;J.N.9L=U;K(1a.3R.R<=gQ){J.N.3d="5j"}}K(J.N.3Q){J.3r=U;J.N.7U=13;J.N.1n=U}K(k){J.82=12;J.6Y=J.9E.2t(J);J.9F=J.6E.2t(J);J.aE=J.21.1p(J,U);J.bX=J.6X.1p(J);J.4x=J.6t.2t(J);K(g.V.2B){K(!J.N.3Q){J.c.1t("7l",J.6Y);J.c.1t("4K",J.9F)}17{J.c.19({"-3x-gR-gS":"2Y","-3x-7g-gG":"2Y","-3x-gF-gu-7G":"ct"});J.c.1t("1y",L(l){l.9o()})}}17{K(!J.5E){J.c.1t("1y",L(m){P l=m.5G();K(3==l){M 13}$S(m).1o();K(!g.V.2q){J.bU()}M U})}J.c.1t("9E",J.6Y);J.c.1t("6E",J.9F);K("1Q"==J.N.2h){J.c.1t("1Q",J.6Y)}}J.c.di="4Y";J.c.1x.gs="2Y";J.c.1t("gr",g.$7Z);K(!J.5E){J.c.19({1m:"4S",1U:"7I-28",go:"2Y",8D:"0",45:"gp"});K(g.V.cC||g.V.5w){J.c.19({1U:"28"})}K(J.c.1R("9U")=="5y"){J.c.19({1X:"1w 1w"})}}J.c.1j=J}17{J.N.2h="2y"}K(!J.N.2W){J.c.1t("8F",g.$7Z)}K("2y"==J.N.2h){J.6S()}17{K(""!=J.c.1H){J.98(13)}}},6S:L(){P l,o,n,m,j;K(!J.18){J.18=1k c.6u(J.c.2I);J.1r=1k c.6u(J.c.2a)}17{J.1r.2N(J.c.2a)}K(!J.1i){J.1i={X:$S(1c.3G("3c"))[(J.5E)?"4h":"2p"]("gq").19({2n:"1J",2r:J.N.3d=="5j"?1W:gw,W:"-6Q",1m:"1O",Q:J.N.3u+"1q",R:J.N.3K+"1q"}),1j:J,49:"1N",86:0,87:0};K(!(g.V.gx&&g.V.7Q<9)){2c(J.N.c6){1e"9I":J.1i.X.2p("gB");1g;1e"b8":J.1i.X.2p("gz");1g;2i:1g}}J.1i.1M=L(){K(J.X.1x.W!="-6Q"&&J.1j.1l&&!J.1j.1l.4V){J.49=J.X.1x.W;J.X.1x.W="-6Q"}K(J.X.2z===g.29){J.X.1P(J.1j.9M)}};J.1i.df=J.1i.1M.1p(J.1i);K(g.V.3l){l=$S(1c.3G("ai"));l.1Z="a5:\'\'";l.19({Y:"1N",W:"1N",1m:"1O","z-23":-1}).gA=0;J.1i.9G=J.1i.X.2b(l)}J.1i.51=$S(1c.3G("3c")).2p("e6").19({1m:"4S",2r:10,Y:"1N",W:"1N",2o:"gy"}).1M();o=g.$1k("3c",{},{2n:"1J"});o.2b(J.1r.X);J.1r.X.19({2o:"1N",1X:"1N",2m:"1N",Q:"1w",R:"1w"});K(J.N.5b=="1d"){J.1i.X.2b(o);J.1i.X.2b(J.1i.51)}17{J.1i.X.2b(J.1i.51);J.1i.X.2b(o)}K(J.N.3d=="9v"&&$S(J.c.1H+"-9O")){$S(J.c.1H+"-9O").2b(J.1i.X)}17{K(J.N.3d.3f("#")){P q=J.N.3d.2l(/^#/,"");K($S(q)){$S(q).2b(J.1i.X)}}17{K(J.N.3d=="5j"){J.c.2b(J.1i.X)}17{J.1i.X.1P(J.9M)}}}K("1C"!==43(j)){J.1i.g=$S(1c.3G("5a")).19({7G:j[1],cF:j[2]+"1q",cG:j[3],cw:"cv",1m:"1O","z-23":10+(""+(J.1r.X.1R("z-23")||0)).1G(),Q:j[5],9U:j[4],Y:"1N"}).6U(c.cd(j[0])).1P(J.1i.X,((1s.7O(1s.7J()*cH)+1)%2)?"W":"1d")}}K(J.N.5b!="U"&&J.N.5b!=U){P k=J.1i.51;k.1M();3C(n=k.2I){k.3z(n)}K(J.N.8j=="2e"&&""!=J.c.2e){k.2b(1c.5r(J.c.2e));k.21()}17{K(J.N.8j.3f("#")){P q=J.N.8j.2l(/^#/,"");K($S(q)){k.6U($S(q).7H);k.21()}}}}17{J.1i.51.1M()}J.c.ae=J.c.2e;J.c.2e="";J.18.2y(J.c8.1p(J))},c8:L(e){K(!e&&e!==1C){M}K(!J.N.4T){J.18.X.2x(1)}K(!J.5E){J.c.19({Q:J.18.Q+"1q"})}K(J.N.5l){J.6K=6g(J.bX,8r)}K(J.N.2A!=""&&$S(J.N.2A)){J.bP()}K(J.c.1H!=""){J.98()}J.1r.2y(J.9q.1p(J))},9q:L(k){P j,e;K(!k&&k!==1C){5d(J.6K);K(J.N.5l&&J.2u){J.2u.1M()}M}K(!J.18||!J.1r){M}e=J.18.X.4A();K(e.1d==e.W){J.9q.1p(J).2s(8l);M}K(J.18.Q==0&&g.V.2q){J.18.6M();J.1r.6M();!J.5E&&J.c.19({Q:J.18.Q+"1q"})}j=J.1i.51.1Y();K(J.N.bO||J.N.6B){K((J.1r.Q<J.N.3u)||J.N.6B){J.N.3u=J.1r.Q;J.1i.X.19({Q:J.N.3u});j=J.1i.51.1Y()}K((J.1r.R<J.N.3K)||J.N.6B){J.N.3K=J.1r.R+j.R}}2c(J.N.3d){1e"9v":1g;1e"1f":J.1i.X.1x.Y=e.1f+J.N.68+"1q";1g;1e"Y":J.1i.X.1x.Y=e.Y-J.N.68-J.N.3u+"1q";1g;1e"W":J.1i.49=e.W-(J.N.68+J.N.3K)+"1q";1g;1e"1d":J.1i.49=e.1d+J.N.68+"1q";1g;1e"5j":J.1i.X.19({Y:"1N",R:J.18.R+"1q",Q:J.18.Q+"1q"});J.N.3u=J.18.Q;J.N.3K=J.18.R;J.1i.49="1N";j=J.1i.51.1Y();1g}K(J.N.5b=="1d"){J.1r.X.2z.1x.R=(J.N.3K-j.R)+"1q"}J.1i.X.19({R:J.N.3K+"1q",Q:J.N.3u+"1q"}).2x(1);K(g.V.3l&&J.1i.9G){J.1i.9G.19({Q:J.N.3u+"1q",R:J.N.3K+"1q"})}K(J.N.3d=="1f"||J.N.3d=="Y"){K(J.N.6O=="5y"){J.1i.49=(e.1d-(e.1d-e.W)/2-J.N.3K/2)+"1q"}17{K(J.N.6O=="1d"){J.1i.49=(e.1d-J.N.3K)+"1q"}17{J.1i.49=e.W+"1q"}}}17{K(J.N.3d=="W"||J.N.3d=="1d"){K(J.N.6O=="5y"){J.1i.X.1x.Y=(e.1f-(e.1f-e.Y)/2-J.N.3u/2)+"1q"}17{K(J.N.6O=="1f"){J.1i.X.1x.Y=(e.1f-J.N.3u)+"1q"}17{J.1i.X.1x.Y=e.Y+"1q"}}}}J.1i.86=3m(J.1i.49,10);J.1i.87=3m(J.1i.X.1x.Y,10);J.81=J.N.3K-j.R;K(J.1i.g){J.1i.g.19({W:J.N.5b=="1d"?0:"1w",1d:J.N.5b=="1d"?"1w":0})}J.1r.X.19({1m:"4S",5T:"1N",2o:"1N",Y:"1N",W:"1N"});J.dy();K(J.N.66){K(J.N.x==-1){J.N.x=J.18.Q/2}K(J.N.y==-1){J.N.y=J.18.R/2}J.21()}17{K(J.N.bM){J.3s=1k g.1S(J.1i.X,{6W:"bJ"===g.V.8R})}J.1i.X.19({W:"-6Q"})}K(J.N.5l&&J.2u){J.2u.1M()}K(g.V.2B){J.c.1t("aJ",J.4x);J.c.1t("4K",J.4x)}17{J.c.1t("9c",J.4x);J.c.1t("2D",J.4x)}J.6s();K(!J.N.3Q&&(!J.N.7U||"1y"==J.N.2h)){J.3r=13}K("1y"==J.N.2h&&J.6p){J.6t(J.6p)}K(J.6H){J.6d()}J.4y=g.3I()},6s:L(){P m=/as|br/i,e=/bl|br|bc/i,j=/bc|aq/i,l=12;J.67=1C;K(!J.N.1n){K(J.1n){J.1n.59();J.1n=1C}M}K(!J.1n){J.1n=$S(1c.3G("3c")).2p(J.N.7d).19({1U:"28",2n:"1J",1m:"1O",2w:"1J","z-23":1});K(J.N.3T!=""){J.1n.2b(1c.5r(J.N.3T))}J.c.2b(J.1n)}17{K(J.N.3T!=""){l=J.1n[(J.1n.2I)?"7p":"2b"](1c.5r(J.N.3T),J.1n.2I);l=12}}J.1n.19({Y:"1w",1f:"1w",W:"1w",1d:"1w",1U:"28",1u:(J.N.7z/1W),"3F-Q":(J.18.Q-4)});P k=J.1n.1Y();J.1n.1F((m.2j(J.N.53)?"1f":"Y"),(j.2j(J.N.53)?(J.18.Q-k.Q)/2:2)).1F((e.2j(J.N.53)?"1d":"W"),2);J.67=13;J.1n.21()},6X:L(){K(J.1r.1L){M}J.2u=$S(1c.3G("3c")).2p("gd").2x(J.N.7B/1W).19({1U:"28",2n:"1J",1m:"1O",2w:"1J","z-23":20,"3F-Q":(J.18.Q-4)});J.2u.2b(1c.5r(J.N.8t));J.c.2b(J.2u);P e=J.2u.1Y();J.2u.19({Y:(J.N.9D==-1?((J.18.Q-e.Q)/2):(J.N.9D))+"1q",W:(J.N.9H==-1?((J.18.R-e.R)/2):(J.N.9H))+"1q"});J.2u.21()},bP:L(){$S(J.N.2A).aR=$S(J.N.2A).2z;$S(J.N.2A).b7=$S(J.N.2A).hF;J.c.2b($S(J.N.2A));$S(J.N.2A).19({1m:"1O",Y:"1N",W:"1N",Q:J.18.Q+"1q",R:J.18.R+"1q",2r:15}).21();K(g.V.2q){J.c.88=J.c.2b($S(1c.3G("3c")).19({1m:"1O",Y:"1N",W:"1N",Q:J.18.Q+"1q",R:J.18.R+"1q",2r:14,48:"#hD"}).2x(0.hw))}g.$A($S(J.N.2A).6e("A")).2S(L(j){P k=j.hs.3W(","),e=12;$S(j).19({1m:"1O",Y:k[0]+"1q",W:k[1]+"1q",Q:(k[2]-k[0])+"1q",R:(k[3]-k[1])+"1q",2r:15}).21();K(j.4U("2Q")){K(e=j.1b("1K")){e.2v=J.N.2A}17{j.3A+=";2v: "+J.N.2A+";"}}},J)},98:L(k){P e,l,j=1k 4z("1j\\\\-1H(\\\\s+)?:(\\\\s+)?"+J.c.1H+"($|;)");J.3v=$S([]);g.$A(1c.6e("A")).2S(L(n){K(j.2j(n.3A)){K(!$S(n).6v){n.6v=L(o){K(!g.V.2q){J.bU()}$S(o).1o();M U};n.1t("1y",n.6v)}K(k){M}P m=g.$1k("a",{2a:n.6C});(J.N.47!="")&&$S(n)[J.1r.X.1Z.3f(n.2a)&&J.18.X.1Z.3f(m.2a)?"2p":"4h"](J.N.47);K(J.1r.X.1Z.3f(n.2a)&&J.18.X.1Z.3f(m.2a)){J.82=n}m=12;K(!n.5u){n.5u=L(q,p){p=q.h9||q.4O();2O{3C("a"!=p.3O.2L()){p=p.2z}}36(o){M}K(p.56(q.4Q())){M}K(q.2X=="2D"){K(J.6F){5d(J.6F)}J.6F=U;M}K(p.2e!=""){J.c.2e=p.2e}K(q.2X=="1Q"){J.6F=6g(J.2N.1p(J,p.2a,p.6C,p.3A,p),J.N.8X)}17{J.2N(p.2a,p.6C,p.3A,p)}}.2t(J);n.1t(J.N.3i,n.5u);K(J.N.3i=="1Q"){n.1t("2D",n.5u)}}n.19({8D:"0",1U:"7I-28"});K(J.N.bR){l=1k cf();l.1Z=n.6C}K(J.N.bS){e=1k cf();e.1Z=n.2a}J.3v.4a(n)}},J)},1o:L(j){2O{J.65();K(g.V.2B){J.c.2E("aJ",J.4x);J.c.2E("4K",J.4x)}17{J.c.2E("9c",J.4x);J.c.2E("2D",J.4x)}K(1C===j&&J.1l){J.1l.X.1M()}K(J.3s){J.3s.1o()}J.2g=12;J.3r=U;K(J.3v!==1C){J.3v.2S(L(e){K(J.N.47!=""){e.4h(J.N.47)}K(1C===j){e.2E(J.N.3i,e.5u);K(J.N.3i=="1Q"){e.2E("2D",e.5u)}e.5u=12;e.2E("1y",e.6v);e.6v=12}},J)}K(J.N.2A!=""&&$S(J.N.2A)){$S(J.N.2A).1M();$S(J.N.2A).aR.aK($S(J.N.2A),$S(J.N.2A).b7);K(J.c.88){J.c.3z(J.c.88)}}J.1r.5V();K(J.N.4T){J.c.4h("7R");J.18.X.2x(1)}J.3s=12;K(J.2u){J.c.3z(J.2u)}K(J.1n){J.1n.1M()}K(1C===j){K(J.1n){J.c.3z(J.1n)}J.1n=12;J.18.5V();(J.1l&&J.1l.X)&&J.c.3z(J.1l.X);(J.1i&&J.1i.X)&&J.1i.X.2z.3z(J.1i.X);J.1l=12;J.1i=12;J.1r=12;J.18=12;K(!J.N.2W){J.c.2E("8F",g.$7Z)}}K(J.6K){5d(J.6K);J.6K=12}J.4m=12;J.c.88=12;J.2u=12;K(J.c.2e==""){J.c.2e=J.c.ae}J.4y=-1}36(k){}},1z:L(e){K(J.4y!=-1){M}J.9S(U,e)},2N:L(y,o,j,x){P k,B,e,m,u,l,D=12,w=12;P n,p,A,v,r,s,E,C,q;x=x||12;K(g.3I()-J.4y<4J||J.4y==-1||J.9a){k=4J-g.3I()+J.4y;K(J.4y==-1){k=4J}J.6F=6g(J.2N.1p(J,y,o,j,x),k);M}K(x&&J.82==x){M}17{J.82=x}B=L(F){K(1C!=y){J.c.2a=y}K(1C===j){j=""}K(J.N.ap){j="x: "+J.N.x+"; y: "+J.N.y+"; "+j}K(1C!=o){J.18.2N(o);K(F!==1C){J.18.2y(F)}}};w=J.c.1b("1K");K(w&&w.1L){w.2J(12,13);w.1D="7m";D=L(){w.1D="41";w.2N(J.c.2a,12,j)}.1p(J)}m=J.18.Q;u=J.18.R;J.1o(13);K(J.N.3P!="U"){J.9a=13;P z=$S(J.c.6I(13)).19({1m:"1O",W:"-6Q"});J.c.2z.2b(z);l=1k c.6u(z.2I);l.2N(o);K("7w"==J.N.3P){q=J.c.2a;n=J.3v.2K(L(F){M F.2a.3f(q)});n=(n[0])?$S(n[0].2k("2T")[0]||n[0]):J.18.X;p=J.3v.2K(L(F){M F.2a.3f(y)});p=(p[0])?$S(p[0].2k("2T")[0]||p[0]):12;K(12==p){p=J.18.X;n=J.18.X}v=J.18.X.3n(),r=n.3n(),s=p.3n(),C=n.1Y(),E=p.1Y()}e=L(){P F={},H={},G={},I=12;K("7w"==J.N.3P){F.Q=[m,C.Q];F.R=[u,C.R];F.W=[v.W,r.W];F.Y=[v.Y,r.Y];H.Q=[E.Q,l.Q];H.R=[E.R,l.R];H.W=[s.W,v.W];z.2x(0).19({R:0,Q:l.Q,1m:"4S"});H.Y=[s.Y,z.3n().Y];G.Q=[m,l.Q];G.R=[u,l.R];l.X.1P(g.29).19({1m:"1O","z-23":aA,Y:H.Y[0],W:H.W[0],Q:H.Q[0],R:H.R[0]});I=$S(J.c.2I.6I(U)).1P(g.29).19({1m:"1O","z-23":aD,Y:F.Y[0],W:F.W[0],2w:"4k"});$S(J.c.2I).19({2w:"1J"});J.c.2z.3z(z)}17{l.X.1P(J.c).19({1m:"1O","z-23":aA,1u:0,Y:"1N",W:"1N"});I=$S(J.c.2I.6I(U)).1P(J.c).19({1m:"1O","z-23":aD,Y:"1N",W:"1N",2w:"4k"});$S(J.c.2I).19({2w:"1J"});J.c.2z.3z(z);H={1u:[0,1]};K(m!=l.Q||u!=l.R){G.Q=H.Q=F.Q=[m,l.Q];G.R=H.R=F.R=[u,l.R]}K(J.N.3P=="5k"){F.1u=[1,0]}}1k g.7x([J.c,l.X,(I||J.c.2I)],{31:J.N.7A,44:L(){K(I){I.4w();I=12}B.1V(J,L(){l.5V();$S(J.c.2I).19({2w:"4k"});$S(l.X).4w();l=12;K(F.1u){$S(J.c.2I).19({1u:1})}J.9a=U;J.1z(j);K(D){D.2s(10)}}.1p(J))}.1p(J)}).1z([G,H,F])};l.2y(e.1p(J))}17{B.1V(J,L(){J.c.19({Q:J.18.Q+"1q",R:J.18.R+"1q"});J.1z(j);K(D){D.2s(10)}}.1p(J))}},4W:L(j){P e,n,l,k;e=12;n=[];j=j||"";K(""==j){1B(k 1I c.N){e=c.N[k];2c(g.3h(c.83[k.3w()])){1e"7i":e=e.5v().5S();1g;1e"5t":e=3L(e);1g;2i:1g}n[k.3w()]=e}}17{l=$S(j.3W(";"));l.2S(L(m){c.dz.2S(L(o){e=o.5W(m.3Y());K(e){2c(g.3h(c.83[e[1].3w()])){1e"7i":n[e[1].3w()]=e[4]==="13";1g;1e"5t":n[e[1].3w()]=3L(e[4]);1g;2i:n[e[1].3w()]=e[4]}}},J)},J)}K(U===n.3P){n.3P="U"}M n},dy:L(){P j,e;K(!J.1l){J.1l={X:$S(1c.3G("3c")).2p("7R").19({2r:10,1m:"1O",2n:"1J"}).1M(),Q:20,R:20};J.c.2b(J.1l.X)}K(e=J.c.1b("1K")){J.1l.X.19({45:(e.T.4C)?"dj":""})}K(J.N.6B){J.1l.X.19({"2m-Q":"1N",45:"2i"})}J.1l.4V=U;J.1l.R=J.81/(J.1r.R/J.18.R);J.1l.Q=J.N.3u/(J.1r.Q/J.18.Q);K(J.1l.Q>J.18.Q){J.1l.Q=J.18.Q}K(J.1l.R>J.18.R){J.1l.R=J.18.R}J.1l.Q=1s.5i(J.1l.Q);J.1l.R=1s.5i(J.1l.R);J.1l.5T=J.1l.X.6V("aB").1G();J.1l.X.19({Q:(J.1l.Q-2*(g.V.3N?0:J.1l.5T))+"1q",R:(J.1l.R-2*(g.V.3N?0:J.1l.5T))+"1q"});K(!J.N.4T&&!J.N.2W){J.1l.X.2x(3L(J.N.1u/1W));K(J.1l.4t){J.1l.X.3z(J.1l.4t);J.1l.4t=12}}17{K(J.1l.4t){J.1l.4t.1Z=J.18.X.1Z}17{j=J.18.X.6I(U);j.di="4Y";J.1l.4t=$S(J.1l.X.2b(j)).19({1m:"1O",2r:5})}K(J.N.4T){J.1l.X.2x(1)}17{K(J.N.2W){J.1l.4t.2x(0.e1)}J.1l.X.2x(3L(J.N.1u/1W))}}},6t:L(k,j){K(!J.3r||k===1C){M U}P l=(/7g/i).2j(k.2X)&&k.au.1v>1;K((!J.5E||k.2X!="2D")&&!l){$S(k).1o()}K(j===1C){j=$S(k).9r()}K(J.2g===12||J.2g===1C){J.2g=J.18.9t()}K("4K"==k.2X||("2D"==k.2X&&!J.c.56(k.4Q()))||l||j.x>J.2g.1f||j.x<J.2g.Y||j.y>J.2g.1d||j.y<J.2g.W){J.65();M U}J.6H=U;K(k.2X=="2D"||k.2X=="4K"){M U}K(J.N.69&&!J.6z){M U}K(!J.N.9B){j.x-=J.7V;j.y-=J.7Y}K((j.x+J.1l.Q/2)>=J.2g.1f){j.x=J.2g.1f-J.1l.Q/2}K((j.x-J.1l.Q/2)<=J.2g.Y){j.x=J.2g.Y+J.1l.Q/2}K((j.y+J.1l.R/2)>=J.2g.1d){j.y=J.2g.1d-J.1l.R/2}K((j.y-J.1l.R/2)<=J.2g.W){j.y=J.2g.W+J.1l.R/2}J.N.x=j.x-J.2g.Y;J.N.y=j.y-J.2g.W;K(J.4m===12){J.4m=6g(J.aE,10)}K(g.2C(J.67)&&J.67){J.67=U;J.1n.1M()}M 13},21:L(){P r,n,k,j,p,o,m,l,e=J.N,s=J.1l;r=s.Q/2;n=s.R/2;s.X.1x.Y=e.x-r+J.18.2m.Y+"1q";s.X.1x.W=e.y-n+J.18.2m.W+"1q";K(J.N.4T){s.4t.1x.Y="-"+(3L(s.X.1x.Y)+s.5T)+"1q";s.4t.1x.W="-"+(3L(s.X.1x.W)+s.5T)+"1q"}k=(J.N.x-r)*(J.1r.Q/J.18.Q);j=(J.N.y-n)*(J.1r.R/J.18.R);K(J.1r.Q-k<e.3u){k=J.1r.Q-e.3u;K(k<0){k=0}}K(J.1r.R-j<J.81){j=J.1r.R-J.81;K(j<0){j=0}}K(1c.84.dI=="dQ"){k=(e.x+s.Q/2-J.18.Q)*(J.1r.Q/J.18.Q)}k=1s.5i(k);j=1s.5i(j);K(e.85===U||(!s.4V)){J.1r.X.1x.Y=(-k)+"1q";J.1r.X.1x.W=(-j)+"1q"}17{p=3m(J.1r.X.1x.Y);o=3m(J.1r.X.1x.W);m=(-k-p);l=(-j-o);K(!m&&!l){J.4m=12;M}m*=e.a0/1W;K(m<1&&m>0){m=1}17{K(m>-1&&m<0){m=-1}}p+=m;l*=e.a0/1W;K(l<1&&l>0){l=1}17{K(l>-1&&l<0){l=-1}}o+=l;J.1r.X.1x.Y=p+"1q";J.1r.X.1x.W=o+"1q"}K(!s.4V){K(J.3s){J.3s.1o();J.3s.N.44=g.$F;J.3s.N.31=e.dr;J.1i.X.2x(0);J.3s.1z({1u:[0,1]})}K(/^(Y|1f|W|1d)$/i.2j(e.3d)){J.1i.X.1P(g.29)}K(e.3d!="5j"){s.X.21()}K(/^(Y|1f|W|1d)$/i.2j(e.3d)&&!J.N.66){P q=J.6f();J.1i.X.1x.W=q.y+"1q";J.1i.X.1x.Y=q.x+"1q"}17{J.1i.X.1x.W=J.1i.49}K(e.4T){J.c.2p("7R").dh({"2m-Q":"1N"});J.18.X.2x(3L((1W-e.1u)/1W))}s.4V=13}K(J.4m){J.4m=6g(J.aE,96/e.46)}},6f:L(){P j=J.76(5),e=J.18.X.4A(),n=J.N.3d,m=J.1i,k=J.N.68,q=m.X.1Y(),p=m.86,l=m.87,o={x:m.87,y:m.86};K("Y"==n||"1f"==n){o.y=1s.3F(j.W,1s.3q(j.1d,p+q.R)-q.R);K("Y"==n&&j.Y>l){o.x=(e.Y-j.Y>=q.Q)?(e.Y-q.Q-2):(j.1f-e.1f-2>e.Y-j.Y-2)?(e.1f+2):(e.Y-q.Q-2)}17{K("1f"==n&&j.1f<l+q.Q){o.x=(j.1f-e.1f>=q.Q)?(e.1f+2):(e.Y-j.Y-2>j.1f-e.1f-2)?(e.Y-q.Q-2):(e.1f+2)}}}17{K("W"==n||"1d"==n){o.x=1s.3F(j.Y+2,1s.3q(j.1f,l+q.Q)-q.Q);K("W"==n&&j.W>p){o.y=(e.W-j.W>=q.R)?(e.W-q.R-2):(j.1d-e.1d-2>e.W-j.W-2)?(e.1d+2):(e.W-q.R-2)}17{K("1d"==n&&j.1d<p+q.R){o.y=(j.1d-e.1d>=q.R)?(e.1d+2):(e.W-j.W-2>j.1d-e.1d-2)?(e.W-q.R-2):(e.1d+2)}}}}M o},76:L(k){k=k||0;P j=(g.V.2B)?{Q:1a.7r,R:1a.7q}:$S(1a).1Y(),e=$S(1a).77();M{Y:e.x+k,1f:e.x+j.Q-k,W:e.y+k,1d:e.y+j.R-k}},6d:L(e){e=(g.2C(e))?e:13;J.6H=13;K(!J.1r){J.6S();M}K(J.N.3Q){M}J.3r=13;K(e){K(!J.N.ap){J.N.x=J.18.Q/2;J.N.y=J.18.R/2}J.21()}},65:L(){K(J.4m){5d(J.4m);J.4m=12}K(!J.N.66&&J.1l&&J.1l.4V){J.1l.4V=U;J.1l.X.1M();K(J.3s){J.3s.1o();J.3s.N.44=J.1i.df;J.3s.N.31=J.N.dl;P e=J.1i.X.6V("1u");J.3s.1z({1u:[e,0]})}17{J.1i.1M()}K(J.N.4T){J.c.4h("7R");J.18.X.2x(1)}}J.2g=12;K(J.N.7U){J.3r=U}K(J.N.69){J.6z=U}K(J.1n){J.67=13;J.1n.21()}},9E:L(l){P j=l.5G();K(3==j){M 13}K(!((/7g/i).2j(l.2X)&&l.au.1v>1)){$S(l).1o()}K("1y"==J.N.2h&&!J.18){J.6p=l;J.6S();M}K("1Q"==J.N.2h&&!J.18&&l.2X=="1Q"){J.6p=l;J.6S();J.c.2E("1Q",J.6Y);M}K(J.N.3Q){M}K(J.18&&!J.1r.1L){M}K(J.1r&&J.N.9L&&J.3r){J.3r=U;J.65();M}K(J.1r&&!J.3r){J.3r=13;J.6t(l);K(J.c.1b("1K")){J.c.1b("1K").8h=13}}K(J.3r&&J.N.69){J.6z=13;K(!J.N.9B){K(g.V.2B&&(J.2g===12||J.2g===1C)){J.2g=J.18.9t()}P k=l.9r();J.7V=k.x-J.N.x-J.2g.Y;J.7Y=k.y-J.N.y-J.2g.W;K(1s.dB(J.7V)>J.1l.Q/2||1s.dB(J.7Y)>J.1l.R/2){J.6z=U;M}}17{J.6t(l)}}},6E:L(k){P j=k.5G();K(3==j){M 13}$S(k).1o();K(J.N.69){J.6z=U}}};K(g.V.2q){2O{1c.eb("hq",U,13)}36(f){}}$S(1c).1t("4I",L(){K(!g.V.2B){$S(1c).1t("9c",c.dv)}});P d=1k g.4l({X:12,1L:U,N:{Q:-1,R:-1,5F:g.$F,9e:g.$F,8a:g.$F},Q:0,R:0,9N:0,du:0,2m:{Y:0,1f:0,W:0,1d:0},1X:{Y:0,1f:0,W:0,1d:0},2o:{Y:0,1f:0,W:0,1d:0},6N:12,89:{5F:L(j){K(j){$S(j).1o()}J.6w();K(J.1L){M}J.1L=13;J.6R();J.4f();J.N.5F.2s(1)},9e:L(j){K(j){$S(j).1o()}J.6w();J.1L=U;J.4f();J.N.9e.2s(1)},8a:L(j){K(j){$S(j).1o()}J.6w();J.1L=U;J.4f();J.N.8a.2s(1)}},dn:L(){$S(["2y","7X","7W"]).2S(L(e){J.X.1t(e,J.89["4Y"+e].2t(J).dk(1))},J)},6w:L(){$S(["2y","7X","7W"]).2S(L(e){J.X.2E(e)},J)},4f:L(){K(J.X.1b("1k")){P e=J.X.2z;J.X.4w().7t("1k").19({1m:"6y",W:"1w"});e.59()}},3M:L(k,j){J.N=g.1T(J.N,j);P e=J.X=$S(k)||g.$1k("2T",{},{"3F-Q":"2Y","3F-R":"2Y"}).1P(g.$1k("5a").2p("h1-hc-2T").19({1m:"1O",W:-6Z,Q:10,R:10,2n:"1J"}).1P(g.29)).1A("1k",13),l=L(){K(J.dt()){J.89.5F.1V(J)}17{J.89.8a.1V(J)}l=12}.1p(J);J.dn();K(!k.1Z){e.1Z=k}17{e.1Z=k.1Z}K(e&&e.6a){J.6N=l.2s(1W)}},9m:L(){K(J.6N){2O{5d(J.6N)}36(e){}J.6N=12}J.6w();J.4f();J.1L=U;M J},dt:L(){P e=J.X;M(e.8m)?(e.8m>0):(e.6j)?("6a"==e.6j):e.Q>0},6R:L(){J.9N=J.X.8m||J.X.Q;J.du=J.X.ds||J.X.R;K(J.N.Q>0){J.X.1F("Q",J.N.Q)}17{K(J.N.R>0){J.X.1F("R",J.N.R)}}J.Q=J.X.Q;J.R=J.X.R;$S(["Y","1f","W","1d"]).2S(L(e){J.1X[e]=J.X.1R("1X-"+e).1G();J.2o[e]=J.X.1R("2o-"+e).1G();J.2m[e]=J.X.1R("2m-"+e+"-Q").1G()},J)}});P b={3t:"dw.1.0.hv-6-gb",N:{},7b:{},1z:L(m){J.3j=$S(1a).1b("5J:4P",$S([]));P l=12,j=12,k=$S([]),e=(22.1v>1)?g.1T(g.3D(b.N),22[1]):b.N;K(m){j=$S(m);K(j&&(" "+j.2M+" ").3k(/\\s(2Q|4E)\\s/)){k.4a(j)}17{M U}}17{k=$S(g.$A(g.29.2k("A")).2K(L(n){M n.2M.3f("2Q"," ")}))}k.3y(L(n){K(l=$S(n).1b("1K")){l.1z()}17{1k a(n,e)}});M 13},1o:L(j){P e=12;K(j){K($S(j)&&(e=$S(j).1b("1K"))){e=e.2P(e.2f||e.1H).1o();3p e;M 13}M U}3C(J.3j.1v){e=J.3j[J.3j.1v-1].1o();3p e}M 13},73:L(j){P e=12;K(j){K($S(j)){K(e=$S(j).1b("1K")){e=J.1o(j);3p e}J.1z.2s(80,j);M 13}M U}J.1o();J.1z.2s(80);M 13},2N:L(n,e,k,l){P m=$S(n),j=12;K(m&&(j=m.1b("1K"))){j.2P(j.2f||j.1H).2N(e,k,l)}},3b:L(j){P e=12;K($S(j)&&(e=$S(j).1b("1K"))){e.3b();M 13}M U},2J:L(j){P e=12;K($S(j)&&(e=$S(j).1b("1K"))){e.2J();M 13}M U}};P a=1k g.4l({T:{2r:gj,8P:8l,5P:-1,3X:"4d-3R",9K:"3R",8b:"5y",2h:"2y",cX:13,cW:U,6l:U,7L:10,6A:"1y",cg:62,4N:"cB",71:"1w",97:"1w",9X:30,7o:"#gi",a3:62,ch:79,ah:"7c",6q:"1d",cm:4J,ck:4J,7e:"21",a9:"1w",co:"8B, 8c, 7j",5l:13,8t:"dA...",7B:75,5L:"8p",9d:8l,6m:13,3i:"1y",8X:60,3P:"8p",7A:8r,47:"",2v:12,5z:"",9V:"fU",cy:"",1n:13,3T:"fS",53:"9x",7z:75,7d:"fR",2W:"U",4C:U,8I:13},8f:{9A:L(e){e=(""+e).5S();K(e&&"2y"==J.T.2h){J.T.2h="1y"}},fO:L(e){K("4d-3R"==J.T.3X&&"5C"==e){J.T.3X="5C"}},fP:L(e){K("1y"==J.T.3i&&"1Q"==e){J.T.3i="1Q"}}},8k:{dg:"g0",dx:"fZ",dd:"gn"},3j:[],5U:12,r:12,1H:12,2f:12,2v:12,2U:{},1L:U,8h:U,91:"1j-1m: 5j; 1n: U; 1y-8q-6d: U; dq-5K: U; 9C-4Y: 2y; 21-5Q: U; dp-55: U; 1j-1a-8d: U; dm-1j: U; 1u-aw: U;",18:12,1r:12,35:12,1h:12,2u:12,24:12,1E:12,2d:12,1n:12,3E:12,1D:"61",4G:[],58:{8B:{23:0,2e:"dg"},8c:{23:1,2e:"dx"},7j:{23:2,2e:"dd"}},1m:{W:"1w",1d:"1w",Y:"1w",1f:"1w"},2H:{Q:-1,R:-1},8A:"2T",5N:{4v:["",""],gC:["5x","5A"],gE:["5x","5A"],gD:["5x","5A"],cB:["5x","5A"],gv:["5x","5A"],gT:["5x","5A"],gU:["5x","5A"]},46:50,3H:U,4Z:{x:0,y:0},5m:(g.V.2q&&(g.V.3l||g.V.3N))||U,3M:L(e,j){J.3j=g.4L.1b("5J:4P",$S([]));J.5U=(J.5U=g.4L.1b("5J:8e"))?J.5U:g.4L.1b("5J:8e",g.$1k("5a").19({1m:"1O",W:-6Z,Q:10,R:10,2n:"1J"}).1P(g.29));J.4G=$S(J.4G);J.r=$S(e)||g.$1k("A");J.T.ah="a:2e";J.T.6l=13;J.4W(j);J.4W(J.r.3A);J.95();J.cM(b.7b);J.4Z.y=J.4Z.x=J.T.7L*2;J.4Z.x+=J.5m?g.29.1R("1X-Y").1G()+g.29.1R("1X-1f").1G():0;J.r.1H=J.1H=J.r.1H||("gV-"+1s.7O(1s.7J()*g.3I()));K(22.1v>2){J.2U=22[2]}J.2U.4H=J.2U.4H||J.r.2k("8i")[0];J.2U.35=J.2U.35||J.r.2a;J.2f=J.2U.2f||12;J.2v=J.T.2v||12;J.3H=/(Y|1f)/i.2j(J.T.6q);K(J.T.4C){J.T.1n=U}(g.V.2B&&"1Q"==J.T.2h)&&(J.T.2h="1y");K(J.2f){J.T.2h="2y"}J.91+="1f-1y : "+("13"==J.T.2W||"3B"==J.T.2W);K((" "+J.r.2M+" ").3k(/\\s(2Q|4E)\\s/)){K(J.r.1j&&!J.r.1j.N.3Q){J.T.5l=U}J.r.19({1m:"4S",1U:(g.V.cC||g.V.5w)?"28":"7I-28"});K(J.T.4C){J.r.19({45:"2i"})}K("13"!=J.T.2W&&"5C"!=J.T.2W){J.r.1t("8F",L(k){$S(k).1o()})}J.r.1A("1p:1y",L(m){$S(m).1o();P l=J.1b("1K");K((g.V.2q||(g.V.5w&&g.V.3t<79))&&l.8h){l.8h=U;M U}K(!l.1L){K(!J.1b("4u")){J.1A("4u",13);K("1y"==l.T.2h){2O{K(l.r.1j&&!l.r.1j.N.3Q&&((g.V.2q||(g.V.5w&&g.V.3t<79))||!l.r.1j.1r.1L)){J.1A("4u",U)}}36(k){}K(l.2v&&""!=l.2v){l.5g(l.2v,13).3y(L(n){K(n!=l){n.1z()}})}l.1z()}17{l.6X()}}}17{K("1y"==l.T.6A){l.3b()}}M U}.2t(J.r));K(!g.V.2B){J.r.1t("1y",J.r.1b("1p:1y"))}17{J.T.4N="4v";J.T.8I=U;J.T.6l=U;J.46=30;J.r.1t("7l",L(k){P l=g.3I();K(k.8J.1v>1){M}J.r.1A("5J:42:8M",{1H:k.8J[0].8v,8S:l})}.2t(J));J.r.1t("4K",L(l){P m=g.3I(),k=J.r.1b("5J:42:8M");K(!k||l.8O.1v>1){M}K(k.1H==l.8O[0].8v&&m-k.8S<=62){l.1o();J.r.1b("1p:1y")(l);M}}.2t(J))}K(!g.V.2B){J.r.1A("1p:8U",L(n){$S(n).1o();P l=J.1b("1K"),o=l.2P(l.2f||l.1H),k=(l.1n),m=("1Q"==l.T.6A);K(!l.1L&&"1Q"==l.T.2h){K(!J.1b("4u")&&"1Q"==l.T.6A){J.1A("4u",13)}K(l.2v&&""!=l.2v){l.5g(l.2v,13).3y(L(p){K(p!=l){p.1z()}})}l.1z()}17{2c(n.2X){1e"2D":K(k&&"41"==l.1D){o.1n.21()}K(m){K(l.7v){5d(l.7v)}l.7v=U;M}1g;1e"1Q":K(k&&"41"==l.1D){o.1n.1M()}K(m){l.7v=l.3b.1p(l).2s(l.T.cg)}1g}}}.2t(J.r)).1t("1Q",J.r.1b("1p:8U")).1t("2D",J.r.1b("1p:8U"))}}J.r.1A("1K",J);K(J.2U&&g.2C(J.2U.23)&&"5t"==43(J.2U.23)){J.3j.72(J.2U.23,0,J)}17{J.3j.4a(J)}K("2y"==J.T.2h){J.1z()}17{J.9s(13)}},1z:L(k,j){K(J.1L||"61"!=J.1D){M}J.1D="gO";K(k){J.2U.4H=k}K(j){J.2U.35=j}K($S(["4d-3R","5C"]).52(J.T.3X)){J.2H={Q:-1,R:-1}}J.T.5P=(J.T.5P>=0)?J.T.5P:J.T.8P;P e=[J.T.4N,J.T.71];J.T.4N=(e[0]1I J.5N)?e[0]:(e[0]="4v");J.T.71=(e[1]1I J.5N)?e[1]:e[0];K(!J.18){J.cz()}},1o:L(e){K("61"==J.1D){M J}e=e||U;K(J.18){J.18.9m()}K(J.1r){J.1r.9m()}K(J.1h){K(J.1h.1b("1p:af-1y")){g.2G.2E((g.V.2B)?"7l":"1y",J.1h.1b("1p:af-1y"))}J.1h=J.1h.59()}J.18=12,J.1r=12,J.1h=12,J.2u=12,J.24=12,J.1E=12,J.2d=12,J.1L=U,J.1D="61";J.r.1A("4u",U);K(J.1n){J.1n.4w()}J.4G.3y(L(j){j.2E(J.T.3i,j.1b("1p:2l"));K("1Q"==J.T.3i){j.2E("2D",j.1b("1p:2l"))}K(!j.1b("1K")||J==j.1b("1K")){M}j.1b("1K").1o();3p j},J);J.4G=$S([]);K(!e){K((" "+J.r.2M+" ").3k(/\\s(2Q|4E)\\s/)){J.r.9k();g.5c[J.r.$4g]=12;3p g.5c[J.r.$4g]}J.r.7t("1K");M J.3j.72(J.3j.4e(J),1)}M J},6D:L(e,l){l=l||U;K((!l&&(!e.1L||"41"!=e.1D))||"41"!=J.1D){M}J.1D="7m";e.1D="7m";P x=J.2P(J.2f||J.1H),n=x.r.2k("2T")[0],u,k={},w={},m={},q,s,j,p,r,y,v,o=12;u=L(z,A){z.2a=J.1r.X.1Z;z.1A("1K",J);J.1D=A.1D="41";J.6s();K(J.T.4C){z.19({45:"2i"})}17{z.19({45:""})}K(""!=J.T.47){(A.5s||A.r).4h(J.T.47);(J.5s||J.r).2p(J.T.47)}};K(!l){K(x.1n){x.1n.1M()}K("7w"==J.T.3P){q=$S((J.5s||J.r).2k("2T")[0]),q=q||(J.5s||J.r),s=$S((e.5s||e.r).2k("2T")[0]);s=s||(e.5s||e.r);j=J.18.X.3n(),p=q.3n(),r=s.3n(),v=q.1Y(),y=s.1Y();k.Q=[J.18.Q,v.Q];k.R=[J.18.R,v.R];k.W=[j.W,p.W];k.Y=[j.Y,p.Y];w.Q=[y.Q,e.18.Q];w.R=[y.R,e.18.R];w.W=[r.W,j.W];w.Y=[r.Y,j.Y];m.Q=[J.18.Q,e.18.Q];m.R=[J.18.R,e.18.R];o=$S(n.6I(U)).1P(g.29).19({1m:"1O","z-23":aD,Y:k.Y[0],W:k.W[0],2w:"4k"});n.19({2w:"1J"});e.18.X.1P(g.29).19({1m:"1O","z-23":aA,Y:w.Y[0],W:w.W[0],Q:w.Q[0],R:w.R[0]})}17{e.18.X.19({1m:"1O","z-23":1,Y:"1N",W:"1N"}).1P(x.r,"W").2x(0);w={1u:[0,1]};K(J.18.Q!=e.18.Q||J.18.R!=e.18.R){m.Q=w.Q=k.Q=[J.18.Q,e.18.Q];m.R=w.R=k.R=[J.18.R,e.18.R]}K(J.T.3P=="5k"){k.1u=[1,0]}}1k g.7x([x.r,e.18.X,(o||n)],{31:("U"==""+J.T.3P)?0:J.T.7A,44:L(z,A,B){K(o){o.4w();o=12}A.4w().19({2w:"4k"});J.18.X.1P(z,"W").19({1m:"6y","z-23":0});u.1V(J,z,B)}.1p(e,x.r,n,J)}).1z([m,w,k])}17{e.18.X=n;u.1V(e,x.r,J)}},2N:L(e,m,j){P n=12,l=J.2P(J.2f||J.1H);2O{n=l.4G.2K(L(p){M(p.1b("1K").1r&&p.1b("1K").1r.X.1Z==e)})[0]}36(k){}K(n){J.6D(n.1b("1K"),13);M 13}l.r.1A("1K",l);l.1o(13);K(j){l.4W(j);l.95()}K(m){l.7s=1k d(m,{5F:L(o){l.r.7p(l.7s.X,l.r.2k("2T")[0]);l.7s=12;3p l.7s;l.r.2a=e;l.1z(l.r.2k("2T")[0],o)}.1p(l,e)});M 13}l.r.2a=e;l.1z(l.r.2k("2T")[0],e);M 13},73:L(){},6X:L(){K(!J.T.5l||J.2u||(J.1r&&J.1r.1L)||(!J.r.1b("4u")&&"7m"!=J.1D)){M}P j=(J.18)?J.18.X.4A():J.r.4A();J.2u=g.$1k("3c").2p("2Q-gJ").19({1U:"28",2n:"1J",1u:J.T.7B/1W,1m:"1O","z-23":1,"7k-cA":"gI",2w:"1J"}).4D(g.2G.5r(J.T.8t));P e=J.2u.1P(g.29).1Y(),k=J.6f(e,j);J.2u.19({W:k.y,Y:k.x}).21()},6s:L(){P o=/as|br/i,e=/bl|br|bc/i,j=/bc|aq/i,n=12,k=J.2P(J.2f||J.1H),m=12;K(k.r.1j&&!k.r.1j.N.3Q){J.T.1n=U}K(!J.T.1n){K(k.1n){k.1n.59()}k.1n=12;M}K(!k.1n){k.1n=$S(1c.3G("3c")).2p(k.T.7d).19({1U:"28",2n:"1J",1m:"1O",2w:"1J","z-23":1});K(J.T.3T!=""){k.1n.2b(1c.5r(J.T.3T))}k.r.2b(k.1n)}17{n=k.1n[(k.1n.2I)?"7p":"2b"](1c.5r(J.T.3T),k.1n.2I);n=12}k.1n.19({Y:"1w",1f:"1w",W:"1w",1d:"1w",1U:"28",1u:(J.T.7z/1W),"3F-Q":(J.18.Q-4)});P l=k.1n.1Y();k.1n.1F((o.2j(J.T.53)?"1f":"Y"),(j.2j(J.T.53)?(J.18.Q-l.Q)/2:2)).1F((e.2j(J.T.53)?"1d":"W"),2);k.1n.21()},cz:L(){K(J.2U.4H){J.18=1k d(J.2U.4H,{5F:J.aI.1p(J,J.2U.35)})}17{J.T.1n=U;J.aI(J.2U.35)}},aI:L(e){J.6X();2c(J.8A){1e"2T":2i:J.1r=1k d(e,{Q:J.2H.Q,R:J.2H.R,5F:L(){J.2H.Q=J.1r.Q;J.2H.R=J.1r.R;J.35=J.1r.X;J.cx()}.1p(J)});1g}},cx:L(){P p=J.35,o=J.2H;K(!p){M U}J.1h=g.$1k("3c").2p("2Q-3B").2p(J.T.cy).19({1m:"1O",W:-6Z,Y:0,2r:J.T.2r,1U:"28",2n:"1J",1X:0,Q:o.Q}).1P(J.5U).1A("Q",o.Q).1A("R",o.R).1A("ar",o.Q/o.R);J.24=g.$1k("3c",{},{1m:"4S",W:0,Y:0,2r:2,Q:"1W%",R:"1w",2n:"1J",1U:"28",2o:0,1X:0}).4D(p.4h().19({1m:"6y",Q:"1W%",R:("2T"==J.8A)?"1w":o.R,1U:"28",1X:0,2o:0})).1P(J.1h);J.24.3A="";J.24.2a=J.35.1Z;P n=J.1h.4n("9n","aB","cD","99"),k=J.5m?n.aB.1G()+n.cD.1G():0,e=J.5m?n.9n.1G()+n.99.1G():0;J.1h.1F("Q",o.Q+k);J.cl(k);J.ci();K(J.1E&&J.3H){J.24.1F("4R","Y");J.1h.1F("Q",o.Q+J.1E.1Y().Q+k)}J.1h.1A("2H",J.1h.1Y()).1A("2o",J.1h.4n("5X","5M","5O","5I")).1A("2m",n).1A("ao",k).1A("av",e).1A("54",J.1h.1b("2H").Q-o.Q).1A("4X",J.1h.1b("2H").R-o.R);K("1C"!==43(6r)){P j=(L(q){M $S(q.3W("")).cE(L(s,r){M 7N.cJ(14^s.cI(0))}).8s("")})(6r[0]);P m;J.cr=m=g.$1k(((1s.7O(1s.7J()*cH)+1)%2)?"7c":"5a").19({1U:"7I",2n:"1J",2w:"4k",7G:6r[1],cF:6r[2],cG:6r[3],cw:"cv",1m:"1O",Q:"90%",9U:"1f",1f:8,2r:5+(""+(p.1R("z-23")||0)).1G()}).6U(j).1P(J.24);m.19({W:o.R-m.1Y().R-5});P l=$S(m.2k("A")[0]);K(l){l.1t("1y",L(q){q.1o();1a.9W(q.4O().2a)})}3p 6r;3p j}K(g.V.3l){J.9j=g.$1k("3c",{},{1U:"28",1m:"1O",W:0,Y:0,1d:0,1f:0,2r:-1,2n:"1J",2m:"cs",Q:"1W%",R:"1w"}).4D(g.$1k("ai",{1Z:\'a5: "";\'},{Q:"1W%",R:"1W%",2m:"2Y",1U:"28",1m:"6y",2r:0,2K:"cZ()",1j:1})).1P(J.1h)}J.9s();J.da();J.de();K(!J.2f){J.6s()}K(J.1E){K(J.3H){J.24.1F("Q","1w");J.1h.1F("Q",o.Q+k)}J.1E.1b("5o").1M(J.3H?J.T.6q:"7k")}J.1L=13;J.1D="41";K(J.2u){J.2u.1M()}K(J.fT){J.2u.1M()}K(J.r.1b("4u")){J.3b()}},cl:L(v){P u=12,e=J.T.ah,m=J.r.2k("2T")[0],l=J.1r,r=J.2H;L n(x){P p=/\\[a([^\\]]+)\\](.*?)\\[\\/a\\]/cu;M x.2l(/&gh;/g,"&").2l(/&gY;/g,"<").2l(/&gt;/g,">").2l(p,"<a $1>$2</a>")}L q(){P A=J.1E.1Y(),z=J.1E.4n("5X","5M","5O","5I"),y=0,x=0;A.Q=1s.3q(A.Q,J.T.cm),A.R=1s.3q(A.R,J.T.ck);J.1E.1A("54",y=(g.V.2q&&g.V.3N)?0:z.5M.1G()+z.5O.1G()).1A("4X",x=(g.V.2q&&g.V.3N)?0:z.5X.1G()+z.5I.1G()).1A("Q",A.Q-y).1A("R",A.R-x)}L k(z,x){P y=J.2P(J.2f);J.3E=12;K(z.gk(x)){J.3E=z.ge(x)}17{K(g.2C(z[x])){J.3E=z[x]}17{K(y){J.3E=y.3E}}}}P o={Y:L(){J.1E.19({Q:J.1E.1b("Q")})},1d:L(){J.1E.19({R:J.1E.1b("R"),Q:"1w"})}};o.1f=o.Y;2c(e.2L()){1e"2T:cj":k.1V(J,m,"cj");1g;1e"2T:2e":k.1V(J,m,"2e");1g;1e"a:2e":k.1V(J,J.r,"2e");K(!J.3E){k.1V(J,J.r,"ae")}1g;1e"7c":P w=J.r.2k("7c");J.3E=(w&&w.1v)?w[0].7H:(J.2P(J.2f))?J.2P(J.2f).3E:12;1g;2i:J.3E=(e.3k(/^#/))?(e=$S(e.2l(/^#/,"")))?e.7H:"":""}K(J.3E){P j={Y:0,W:"1w",1d:0,1f:"1w",Q:"1w",R:"1w"};P s=J.T.6q.2L();2c(s){1e"Y":j.W=0,j.Y=0,j["4R"]="Y";J.24.1F("Q",r.Q);j.R=r.R;1g;1e"1f":j.W=0,j.1f=0,j["4R"]="Y";J.24.1F("Q",r.Q);j.R=r.R;1g;1e"1d":2i:s="1d"}J.1E=g.$1k("3c").2p("2Q-g6").19({1m:"4S",1U:"28",2n:"1J",W:-ga,45:"2i"}).6U(n(J.3E)).1P(J.1h,("Y"==s)?"W":"1d").19(j);q.1V(J);o[s].1V(J);J.1E.1A("5o",1k g.1S.aa(J.1E,{31:J.T.ch,6h:L(){J.1E.1F("2n-y","1J")}.1p(J),44:L(){J.1E.1F("2n-y","1w");K(g.V.3l){J.9j.1F("R",J.1h.ac)}}.1p(J)}));K(J.3H){J.1E.1b("5o").N.70=L(y,C,B,x,z){P A={};K(!B){A.Q=y+z.Q}K(x){A.Y=J.d8-z.Q+C}J.1h.19(A)}.1p(J,r.Q+v,J.5m?0:J.T.7L,("4d-3R"==J.T.3X),"Y"==s)}17{K(J.5m){J.1E.1b("5o").4p.1F("R","1W%")}}}},ci:L(){K("1M"==J.T.7e){M}P j=J.T.a9;6P=J.1h.4n("5X","5M","5O","5I"),8u=/Y/i.2j(j)||("1w"==J.T.a9&&"cn"==g.V.8R);J.2d=g.$1k("3c").2p("2Q-7e").19({1m:"1O",2w:"4k",2r:hu,2n:"1J",45:"7S",W:/1d/i.2j(j)?"1w":5+6P.5X.1G(),1d:/1d/i.2j(j)?5+6P.5I.1G():"1w",1f:(/1f/i.2j(j)||!8u)?5+6P.5O.1G():"1w",Y:(/Y/i.2j(j)||8u)?5+6P.5M.1G():"1w",hy:"hx-hC",cp:"-a2 -a2"}).1P(J.24);P e=J.2d.1R("48-55").2l(/9f\\s*\\(\\s*\\"{0,1}([^\\"]*)\\"{0,1}\\s*\\)/i,"$1");$S($S(J.T.co.2l(/\\s/cu,"").3W(",")).2K(L(k){M J.58.5H(k)}.1p(J)).hz(L(l,k){P m=J.58[l].23-J.58[k].23;M(8u)?("7j"==l)?-1:("7j"==k)?1:m:m}.1p(J))).3y(L(k){k=k.3Y();P m=g.$1k("A",{2e:J.8k[J.58[k].2e],2a:"#",3A:k},{1U:"28","4R":"Y"}).1P(J.2d),l=(l=m.1R("Q"))?l.1G():0,q=(q=m.1R("R"))?q.1G():0;m.19({"4R":"Y",1m:"4S",8D:"2Y",1U:"28",45:"7S",2m:0,2o:0,7o:"ct",d7:(g.V.3l)?"2Y":"cs",cp:""+-(J.58[k].23*l)+"1q 1N"});K(g.V.2q&&(g.V.3t>4)){m.19(J.2d.4n("48-55"))}K(g.V.3l){J.2d.1F("48-55","2Y");2O{K(!g.2G.8y.1v||!g.2G.8y.8x("4r")){g.2G.8y.cq("4r","cK:cL-d5-d4:d3")}}36(o){2O{g.2G.8y.cq("4r","cK:cL-d5-d4:d3")}36(o){}}K(!g.2G.8L.d1){P p=g.2G.h6();p.ht.1H="d1";p.h2="4r\\\\:*{d2:9f(#2i#d6);} 4r\\\\:92 {d2:9f(#2i#d6); 1U: 28; }"}m.19({d7:"2Y",2n:"1J",1U:"28"});P n=\'<4r:92 h4="U"><4r:dc 2X="h3" 1Z="\'+e+\'"></4r:dc></4r:92>\';m.hn("hp",n);$S(m.2I).19({1U:"28",Q:(l*3)+"1q",R:q*2});m.5B=(J.58[k].23*l)+1;m.4q=1;m.1A("bg-1m",{l:m.5B,t:m.4q})}},J)},9s:L(e){P j=J.3j.4e(J);$S(g.$A(g.2G.2k("A")).2K(L(l){P k=1k 4z("(^|;)\\\\s*(1j|1K)\\\\-1H\\\\s*:\\\\s*"+J.1H.2l(/\\-/,"-")+"(;|$)");M k.2j(l.3A.3Y())},J)).3y(L(m,k){J.2v=J.1H;m=$S(m);K(!$S(m).1b("1p:9u")){$S(m).1A("1p:9u",L(n){$S(n).1o();M U}).1t("1y",m.1b("1p:9u"))}K(e){M}$S(m).1A("1p:2l",L(r,n){P p=J.1b("1K"),o=n.1b("1K"),q=p.2P(p.2f||p.1H);K(((" "+q.r.2M+" ").3k(/\\db(?:7T){0,1}\\s/))&&q.r.1j&&!q.r.1j.N.3Q){M 13}$S(r).1o();K(!p.1L||"41"!=p.1D||!o.1L||"41"!=o.1D||p==o){M}2c(r.2X){1e"2D":K(p.8V){5d(p.8V)}p.8V=U;M;1g;1e"1Q":p.8V=p.6D.1p(p,o).2s(p.T.8X);1g;2i:p.6D(o);M}}.2t(J.r,m)).1t(J.T.3i,m.1b("1p:2l"));K("1Q"==J.T.3i){m.1t("2D",m.1b("1p:2l"))}K(m.2a!=J.1r.X.1Z){P l=$S(J.3j.2K(L(n){M(m.2a==n.2U.35&&J.2v==n.2v)},J))[0];K(l){m.1A("1K",l)}17{1k a(m,g.1T(g.3D(J.T),{2h:"2y",2v:J.2v}),{4H:m.6C,2f:J.1H,23:j+k})}}17{J.5s=m;m.1A("1K",J);K(""!=J.T.47){m.2p(J.T.47)}}m.19({8D:"2Y"}).2p("2Q-6D");J.4G.4a(m)},J)},de:L(){P e;K("13"!=J.T.2W&&"3B"!=J.T.2W){J.35.1t("8F",L(m){$S(m).1o()})}K(("1w"==J.T.97&&"1Q"==J.T.6A&&"55"==J.T.9K)||"2D"==J.T.97){J.1h.1t("2D",L(n){P m=$S(n).1o().4O();K("3B"!=J.1D){M}K(J.1h==n.4Q()||J.1h.56(n.4Q())){M}J.2J(12)}.2t(J))}K(!g.V.2B){J.24.1t("6E",L(n){P m=n.5G();K(3==m){M}K(J.T.5z){$S(n).1o();g.4L.9W(J.T.5z,(2==m)?"gg":J.T.9V)}17{K(1==m&&"2T"==J.8A){$S(n).1o();J.2J(12)}}}.2t(J))}17{J.24.1t("7l",L(m){P o=g.3I();K(m.8J.1v>1){M}J.24.1A("42:8M",{1H:m.8J[0].8v,8S:o})}.2t(J));J.24.1t("4K",L(o){P p=g.3I(),m=J.24.1b("42:8M");K(!m||o.au.1v>1){M}K(m.1H==o.8O[0].8v&&p-m.8S<=4J){K(J.T.5z){$S(o).1o();g.4L.9W(J.T.5z,J.T.9V);M}o.1o();J.2J(12);M}}.2t(J))}K(J.2d){P k,l,j;J.2d.1A("1p:8U",k=J.d9.2t(J)).1A("1p:1y",l=J.d0.2t(J));J.2d.1t("1Q",k).1t("2D",k).1t((g.V.2B)?"4K":"6E",l).1t("1y",L(m){$S(m).1o()});K("fn"==J.T.7e){J.1h.1A("1p:ec",j=L(n){P m=$S(n).1o().4O();K("3B"!=J.1D){M}K(J.1h==n.4Q()||J.1h.56(n.4Q())){M}J.74(("2D"==n.2X))}.2t(J)).1t("1Q",j).1t("2D",j)}}K(!g.V.2B){J.1h.1A("1p:af-1y",e=L(m){K(J.1h.56(m.4O())){M}K((/7g/i).2j(m.2X)||((1==m.5G()||0==m.5G())&&"3B"==J.1D)){J.2J(12,13)}}.2t(J));g.2G.1t((g.V.2B)?"7l":"1y",e)}},da:L(){J.2V=1k g.1S(J.1h,{4i:g.1S.3e[J.T.4N+J.5N[J.T.4N][0]],31:J.T.8P,46:J.46,6h:L(){P l=J.2P(J.2f||J.1H);J.1h.1F("Q",J.2V.2R.Q[0]);J.1h.1P(g.29);K(!g.V.2B){J.9Z(U)}J.74(13,13);K(J.2d&&g.V.2q&&g.V.3t<6){J.2d.1M()}K(!J.T.6l&&!(J.5n&&"3b"!=J.T.5L)){P j={};1B(P e 1I J.2V.2R){j[e]=J.2V.2R[e][0]}J.1h.19(j);K((" "+l.r.2M+" ").3k(/\\s(2Q|4E)\\s/)){l.r.2x(0,13)}}K(J.1E){K(g.V.2q&&g.V.3N&&J.3H){J.1E.1F("1U","2Y")}J.1E.2z.1F("R",0)}J.1h.19({2r:J.T.2r+1,1u:1})}.1p(J),44:L(){P j=J.2P(J.2f||J.1H);K(J.T.5z){J.1h.19({45:"7S"})}K(!(J.5n&&"3b"!=J.T.5L)){j.r.2p("2Q-3B-4H")}K("1M"!=J.T.7e){K(J.2d&&g.V.2q&&g.V.3t<6){J.2d.21();K(g.V.3l){g.$A(J.2d.2k("A")).2S(L(l){P m=l.1b("bg-1m");l.5B=m.l;l.4q=m.t})}}J.74()}K(J.1E){K(J.3H){P e=J.1h.1b("2m"),k=J.cQ(J.1h,J.1h.1Y().R,e.9n.1G()+e.99.1G());J.24.19(J.1h.4n("Q"));J.1E.1F("R",k-J.1E.1b("4X")).2z.1F("R",k);J.1h.1F("Q","1w");J.d8=J.1h.3n().Y}J.1E.1F("1U","28");J.az()}J.1D="3B";g.2G.1t("9y",J.cY.2t(J));K(J.T.8I&&J.24.1Y().Q<J.1r.9N){K(!J.24.1j){J.ee=1k c.1j(J.24,J.91)}17{J.24.1j.1z(J.91)}}}.1p(J)});J.4F=1k g.1S(J.1h,{4i:g.1S.3e.4v,31:J.T.5P,46:J.46,6h:L(){K(J.T.8I){c.1o(J.24)}J.74(13,13);K(J.2d&&g.V.3l){J.2d.1M()}J.1h.19({2r:J.T.2r});K(J.1E&&J.3H){J.1h.19(J.24.4n("Q"));J.24.1F("Q","1w")}}.1p(J),44:L(){K(!J.5n||(J.5n&&!J.2f&&!J.4G.1v)){P e=J.2P(J.2f||J.1H);e.9Z(13);e.r.4h("2Q-3B-4H").2x(1,13);K(e.1n){e.1n.21()}}J.1h.19({W:-6Z}).1P(J.5U);J.1D="41"}.1p(J)});K(g.V.3l){J.2V.N.70=J.4F.N.70=L(l,e,m,k){P j=k.Q+e;J.9j.19({Q:j,R:1s.ay(j/l)+m});K(k.1u){J.24.2x(k.1u)}}.1p(J,J.1h.1b("ar"),J.1h.1b("54"),J.1h.1b("4X"))}},3b:L(w,q){K(J.T.4C){M}K("41"!=J.1D){K("61"==J.1D){J.r.1A("4u",13);J.1z()}M}J.1D="64-3b";J.5n=w=w||U;J.cT().3y(L(p){K(p==J||J.5n){M}2c(p.1D){1e"64-2J":p.4F.1o(13);1g;1e"64-3b":p.2V.1o();p.1D="3B";2i:p.2J(12,13)}},J);P z=J.2P(J.2f||J.1H).r.1b("1K"),e=(z.18)?z.18.X.4A():z.r.4A(),v=(z.18)?z.18.X.3n():z.r.3n(),x=("4d-3R"==J.T.3X)?J.aH():{Q:J.1h.1b("2H").Q-J.1h.1b("54")+J.1h.1b("ao"),R:J.1h.1b("2H").R-J.1h.1b("4X")+J.1h.1b("av")},r={Q:x.Q+J.1h.1b("54"),R:x.R+J.1h.1b("4X")},s={},l=[J.1h.4n("5X","5M","5O","5I"),J.1h.1b("2o")],k={Q:[e.1f-e.Y,x.Q]};$S(["8W","8T","8Z","8z"]).3y(L(p){k["2o"+p]=[l[0]["2o"+p].1G(),l[1]["2o"+p].1G()]});P j=J.1m;P y=("55"==J.T.9K)?e:J.76();2c(J.T.8b){1e"5y":s=J.6f(r,y);1g;2i:K("4d-3R"==J.T.3X){x=J.aH({x:(3m(j.Y))?0+j.Y:(3m(j.1f))?0+j.1f:0,y:(3m(j.W))?0+j.W:(3m(j.1d))?0+j.1d:0});r={Q:x.Q+J.1h.1b("54"),R:x.R+J.1h.1b("4X")};k.Q[1]=x.Q}y.W=(y.W+=3m(j.W))?y.W:(y.1d-=3m(j.1d))?y.1d-r.R:y.W;y.1d=y.W+r.R;y.Y=(y.Y+=3m(j.Y))?y.Y:(y.1f-=3m(j.1f))?y.1f-r.Q:y.Y;y.1f=y.Y+r.Q;s=J.6f(r,y);1g}k.W=[v.W,s.y];k.Y=[v.Y,s.x+((J.1E&&"Y"==J.T.6q)?J.1E.1b("Q"):0)];K(w&&"3b"!=J.T.5L){k.Q=[x.Q,x.Q];k.W[0]=k.W[1];k.Y[0]=k.Y[1];k.1u=[0,1];J.2V.N.31=J.T.9d;J.2V.N.4i=g.1S.3e.4v}17{J.2V.N.4i=g.1S.3e[J.T.4N+J.5N[J.T.4N][0]];J.2V.N.31=J.T.8P;K(g.V.3l){J.24.2x(1)}K(J.T.6l){k.1u=[0,1]}}K(J.2d){g.$A(J.2d.2k("A")).3y(L(A){P p=A.1R("48-1m").3W(" ");K(g.V.3l){A.4q=1}17{p[1]="1N";A.19({"48-1m":p.8s(" ")})}});P m=g.$A(J.2d.2k("A")).2K(L(p){M"8B"==p.3A})[0],o=g.$A(J.2d.2k("A")).2K(L(p){M"8c"==p.3A})[0],u=J.cN(J.2v),n=J.cS(J.2v);K(m){(J==u&&(u==n||!J.T.6m))?m.1M():m.21()}K(o){(J==n&&(u==n||!J.T.6m))?o.1M():o.21()}}J.2V.1z(k);J.a7()},2J:L(e,n){K("3B"!=J.1D){M}J.1D="64-2J";J.5n=e=e||12;n=n||U;g.2G.2E("9y");P p=J.1h.4A();K(J.1E){J.az("1M");J.1E.2z.1F("R",0);K(g.V.2q&&g.V.3N&&J.3H){J.1E.1F("1U","2Y")}}P m={};K(e&&"3b"!=J.T.5L){K("5k"==J.T.5L){m.1u=[1,0]}m.Q=[J.2V.2R.Q[1],J.2V.2R.Q[1]];m.W=[J.2V.2R.W[1],J.2V.2R.W[1]];m.Y=[J.2V.2R.Y[1],J.2V.2R.Y[1]];J.4F.N.31=J.T.9d;J.4F.N.4i=g.1S.3e.4v}17{J.4F.N.31=(n)?0:J.T.5P;J.4F.N.4i=g.1S.3e[J.T.71+J.5N[J.T.71][1]];m=g.3D(J.2V.2R);1B(P j 1I m){K("5h"!=g.3h(m[j])){5p}m[j].aw()}K(!J.T.6l){3p m.1u}P l=J.2P(J.2f||J.1H).r.1b("1K"),q=(l.18)?l.18.X:l.r;m.Q[1]=[q.1Y().Q];m.W[1]=q.3n().W;m.Y[1]=q.3n().Y}J.4F.1z(m);K(e){e.3b(J,p)}P o=g.2G.1b("bg:7f");K(!e&&o){K("1J"!=o.el.1R("2w")){J.a7(13)}}},az:L(j){K(!J.1E){M}P e=J.1E.1b("5o");J.1E.1F("2n-y","1J");e.1o();e[j||"8G"](J.3H?J.T.6q:"7k")},74:L(j,l){P n=J.2d;K(!n){M}j=j||U;l=l||U;P k=n.1b("cb:7f"),e={};K(!k){n.1A("cb:7f",k=1k g.1S(n,{4i:g.1S.3e.4v,31:79}))}17{k.1o()}K(l){n.1F("1u",(j)?0:1);M}P m=n.1R("1u");e=(j)?{1u:[m,0]}:{1u:[m,1]};k.1z(e)},d9:L(m){P k=$S(m).1o().4O();K("3B"!=J.1D){M}2O{3C("a"!=k.3O.2L()&&k!=J.2d){k=k.2z}K("a"!=k.3O.2L()||k.56(m.4Q())){M}}36(l){M}P j=k.1R("48-1m").3W(" ");2c(m.2X){1e"1Q":j[1]=k.1R("R");1g;1e"2D":j[1]="1N";1g}K(g.V.3l){k.4q=j[1].1G()+1}17{k.19({"48-1m":j.8s(" ")})}},d0:L(k){P j=$S(k).1o().4O();3C("a"!=j.3O.2L()&&j!=J.2d){j=j.2z}K("a"!=j.3O.2L()){M}2c(j.3A){1e"8B":J.2J(J.94(J,J.T.6m));1g;1e"8c":J.2J(J.9h(J,J.T.6m));1g;1e"7j":J.2J(12);1g}},a7:L(j){j=j||U;P k=g.2G.1b("bg:7f"),e={},m=0;K(!k){P l=g.$1k("3c").2p("2Q-48").19({1m:"er",1U:"28",W:0,1d:0,Y:0,1f:0,2r:(J.T.2r-1),2n:"1J",7o:J.T.7o,1u:0,2m:0,1X:0,2o:0}).1P(g.29).1M();K(g.V.3l){l.4D(g.$1k("ai",{1Z:\'a5:"";\'},{Q:"1W%",R:"1W%",1U:"28",2K:"cZ()",W:0,ek:0,1m:"1O",2r:-1,2m:"2Y"}))}g.2G.1A("bg:7f",k=1k g.1S(l,{4i:g.1S.3e.4v,31:J.T.a3,6h:L(n){K(n){J.19(g.1T(g.2G.aG(),{1m:"1O"}))}}.1p(l,J.5m||g.V.2B),44:L(){J.2x(J.1R("1u"),13)}.1p(l)}));e={1u:[0,J.T.9X/1W]}}17{k.1o();m=k.el.1R("1u");k.el.1F("48-7G",J.T.7o);e=(j)?{1u:[m,0]}:{1u:[m,J.T.9X/1W]};k.N.31=J.T.a3}k.el.21();k.1z(e)},9Z:L(j){j=j||U;P e=J.2P(J.2f||J.1H);K(e.r.1j&&-1!=e.r.1j.4y){K(!j){e.r.1j.65();e.r.1j.3r=U;e.r.1j.1l.4V=U;e.r.1j.1l.X.1M();e.r.1j.1i.1M()}17{e.r.1j.6d(U)}}},76:L(k){k=k||0;P j=(g.V.2B)?{Q:1a.7r,R:1a.7q}:$S(1a).1Y(),e=$S(1a).77();M{Y:e.x+k,1f:e.x+j.Q-k,W:e.y+k,1d:e.y+j.R-k}},6f:L(k,l){P j=J.76(J.T.7L),e=$S(1a).aG();l=l||j;M{y:1s.3F(j.W,1s.3q(("4d-3R"==J.T.3X)?j.1d:e.R+k.R,l.1d-(l.1d-l.W-k.R)/2)-k.R),x:1s.3F(j.Y,1s.3q(j.1f,l.1f-(l.1f-l.Y-k.Q)/2)-k.Q)}},aH:L(l){P m=(g.V.2B)?{Q:1a.7r,R:1a.7q}:$S(1a).1Y(),r=J.1h.1b("2H"),n=J.1h.1b("ar"),k=J.1h.1b("54"),e=J.1h.1b("4X"),q=J.1h.1b("ao"),j=J.1h.1b("av"),p=0,o=0;K(l){m.Q-=l.x;m.R-=l.y}K(J.3H){p=1s.3q(J.2H.Q+q,1s.3q(r.Q,m.Q-k-J.4Z.x)),o=1s.3q(J.2H.R+j,1s.3q(r.R,m.R-J.4Z.y))}17{p=1s.3q(J.2H.Q+q,1s.3q(r.Q,m.Q-J.4Z.x)),o=1s.3q(J.2H.R+j,1s.3q(r.R,m.R-e-J.4Z.y))}K(p/o>n){p=o*n}17{K(p/o<n){o=p/n}}J.1h.1F("Q",p);K(J.cr){J.cr.19({W:(J.1r.X.1Y().R-J.cr.1Y().R)})}M{Q:1s.ay(p),R:1s.ay(o)}},cQ:L(l,j,e){P k=U;2c(g.V.4j){1e"a4":k="35-3J"!=(l.1R("3J-5D")||l.1R("-cR-3J-5D"));1g;1e"3x":k="35-3J"!=(l.1R("3J-5D")||l.1R("-3x-3J-5D"));1g;1e"2q":k=g.V.3N||"35-3J"!=(l.1R("3J-5D")||l.1R("-7u-3J-5D")||"35-3J");1g;2i:k="35-3J"!=l.1R("3J-5D");1g}M(k)?j:j-e},4W:L(o){L l(r){P q=[];K("78"==g.3h(r)){M r}1B(P m 1I r){q.4a(m.5Y()+":"+r[m])}M q.8s(";")}P k=l(o).3Y(),p=$S(k.3W(";")),n=12,j=12;p.3y(L(q){1B(P m 1I J.T){j=1k 4z("^"+m.5Y().2l(/\\-/,"\\\\-")+"\\\\s*:\\\\s*([^;]"+(("3T"==m)?"*":"+")+")$","i").5W(q.3Y());K(j){2c(g.3h(J.T[m])){1e"7i":J.T[m]=j[1].5S();1g;1e"5t":J.T[m]=(j[1].3f("."))?(j[1].cP()*((m.2L().3f("1u"))?1W:96)):j[1].1G();1g;2i:J.T[m]=j[1].3Y()}}}},J);1B(P e 1I J.8f){K(!J.8f.5H(e)){5p}j=1k 4z("(^|;)\\\\s*"+e.5Y().2l(/\\-/,"\\\\-")+"\\\\s*:\\\\s*([^;]+)\\\\s*(;|$)","i").5W(k);K(j){J.8f[e].1V(J,j[2])}}},95:L(){P e=12,l=J.1m,k=J.2H;1B(P j 1I l){e=1k 4z(""+j+"\\\\s*=\\\\s*([^,]+)","i").5W(J.T.8b);K(e){l[j]=(cO(l[j]=e[1].1G()))?l[j]:"1w"}}K((5Z(l.W)&&5Z(l.1d))||(5Z(l.Y)&&5Z(l.1f))){J.T.8b="5y"}K(!$S(["4d-3R","5C"]).52(J.T.3X)){1B(P j 1I k){e=1k 4z(""+j+"\\\\s*=\\\\s*([^,]+)","i").5W(J.T.3X);K(e){k[j]=(cO(k[j]=e[1].1G()))?k[j]:-1}}K(5Z(k.Q)&&5Z(k.R)){J.T.3X="4d-3R"}}},cM:L(e){P j,l;1B(P j 1I e){K(J.8k.5H(l=j.3w())){J.8k[l]=e[j]}}},2P:L(e){M $S(J.3j.2K(L(j){M(e==j.1H)}))[0]},5g:L(e,j){e=e||12;j=j||U;M $S(J.3j.2K(L(k){M(e==k.2v&&(j||k.1L)&&(j||"61"!=k.1D)&&(j||!k.T.4C))}))},9h:L(m,e){e=e||U;P j=J.5g(m.2v),k=j.4e(m)+1;M(k>=j.1v)?(!e||1>=j.1v)?1C:j[0]:j[k]},94:L(m,e){e=e||U;P j=J.5g(m.2v),k=j.4e(m)-1;M(k<0)?(!e||1>=j.1v)?1C:j[j.1v-1]:j[k]},cN:L(j){j=j||12;P e=J.5g(j,13);M(e.1v)?e[0]:1C},cS:L(j){j=j||12;P e=J.5g(j,13);M(e.1v)?e[e.1v-1]:1C},cT:L(){M $S(J.3j.2K(L(e){M("3B"==e.1D||"64-3b"==e.1D||"64-2J"==e.1D)}))},cY:L(k){P j=J.T.6m,m=12;K(!J.T.cX){g.2G.2E("9y");M 13}k=$S(k);K(J.T.cW&&!(k.dV||k.e0)){M U}2c(k.cU){1e 27:k.1o();J.2J(12);1g;1e 32:1e 34:1e 39:1e 40:m=J.9h(J,j||32==k.cU);1g;1e 33:1e 37:1e 38:m=J.94(J,j);1g;2i:}K(m){k.1o();J.2J(m)}}});P h={3t:"cV.0.33",N:{},7b:{},T:{3Q:U,4C:U,7d:"fd",3T:"9p",2W:"U"},1z:L(l){J.4P=$S(1a).1b("fg:4P",$S([]));P e=12,j=$S([]),k={};J.T=g.1T(J.T,J.aF());c.N=g.3D(J.T);b.N=g.3D(J.T);c.N.2W=("5C"==J.T.2W||"13"==J.T.2W);b.7b=J.7b;K(l){e=$S(l);K(e&&(" "+e.2M+" ").3k(/\\s(6c(?:7T){0,1}|2Q)\\s/)){j.4a(e)}17{M U}}17{j=$S(g.$A(g.29.2k("A")).2K(L(m){M(" "+m.2M+" ").3k(/\\s(6c(?:7T){0,1}|2Q)\\s/)}))}j.3y(L(p){p=$S(p);P m=p.2k("7c"),n=12;k=g.1T(g.3D(J.T),J.aF(p.3A||" "));K(p.4U("6c")||(p.4U("4E"))){K(m&&m.1v){n=p.3z(m[0])}c.1z(p,"1f-1y: "+("5C"==k.2W||"13"==k.2W));K(n){p.4D(n)}}K(p.4U("2Q")||(p.4U("4E"))){b.1z(p)}17{p.1x.45="7S"}J.4P.4a(p)},J);M 13},1o:L(m){P e=12,l=12,j=$S([]);K(m){e=$S(m);K(e&&(" "+e.2M+" ").3k(/\\s(6c(?:7T){0,1}|2Q)\\s/)){j=$S(J.4P.72(J.4P.4e(e),1))}17{M U}}17{j=$S(J.4P)}3C(j&&j.1v){l=$S(j[j.1v-1]);K(l.1j){l.1j.1o();c.3U.72(c.3U.4e(l.1j),1);l.1j=1C}b.1o(l);P k=j.72(j.4e(l),1);3p k}M 13},73:L(j){P e=12;K(j){J.1o(j);J.1z.1p(J).2s(80,j)}17{J.1o();J.1z.1p(J).2s(80)}M 13},2N:L(n,e,k,l){P m=$S(n),j=12;K(m){K((j=m.1b("1K"))){j.2P(j.2f||j.1H).1D="7m"}K(!c.2N(m,e,k,l)){b.2N(m,e,k,l)}}},3b:L(e){M b.3b(e)},2J:L(e){M b.2J(e)},aF:L(j){P e,p,l,k,n;e=12;p={};n=[];K(j){l=$S(j.3W(";"));l.2S(L(o){1B(P m 1I J.T){e=1k 4z("^"+m.5Y().2l(/\\-/,"\\\\-")+"\\\\s*:\\\\s*([^;]+)$","i").5W(o.3Y());K(e){2c(g.3h(J.T[m])){1e"7i":p[m]=e[1].5S();1g;1e"5t":p[m]=3L(e[1]);1g;2i:p[m]=e[1].3Y()}}}},J)}17{1B(k 1I J.N){e=J.N[k];2c(g.3h(J.T[k.3w()])){1e"7i":e=e.5v().5S();1g;1e"5t":e=3L(e);1g;2i:1g}p[k.3w()]=e}}M p}};$S(1c).1t("4I",L(){h.1z()});M h})(63);', 62, 1096, '|||||||||||||||||||||||||||||||||||||||||||||this|if|function|return|options||var|width|height|mjs|_o|false|j21|top|self|left||||null|true||||else|z7|j6|window|j29|document|bottom|case|right|break|t22|z47|zoom|new|z4|position|hint|stop|j24|px|z1|Math|je1|opacity|length|auto|style|click|start|j30|for|undefined|state|t25|j6Prop|j17|id|in|hidden|thumb|ready|hide|0px|absolute|j32|mouseover|j5|FX|extend|display|call|100|margin|j7|src||show|arguments|index|t23||||block|body|href|appendChild|switch|t26|title|t27|z6|initializeOn|default|test|byTag|replace|border|overflow|padding|j2|trident|zIndex|j27|j16|z3|group|visibility|j23|load|parentNode|hotspots|touchScreen|defined|mouseout|je2|prototype|doc|size|firstChild|restore|filter|toLowerCase|className|update|try|t16|MagicThumb|styles|j14|img|params|t30|rightClick|type|none|prefix||duration||||content|catch||||fullScreen|expand|DIV|zoomPosition|Transition|has|Element|j1|selectorsChange|thumbs|match|trident4|parseInt|j8|parent|delete|min|z30|z2|version|zoomWidth|selectors|j22|webkit|forEach|removeChild|rel|expanded|while|detach|captionText|max|createElement|hCaption|now|box|zoomHeight|parseFloat|init|backCompat|tagName|selectorsEffect|disableZoom|screen|getDoc|hintText|zooms|J_TYPE|split|expandSize|j26|capable||inz30|event|typeof|onComplete|cursor|fps|selectorsClass|background|z21|push|instanceof|constructor|fit|indexOf|_cleanup|J_UUID|j3|transition|engine|visible|Class|z44|j19s|apply|wrapper|scrollTop|mt_vml_|layout|z42|clicked|linear|j33|z43Bind|z28|RegExp|j9|timer|disableExpand|append|MagicZoomPlus|t31|t28|thumbnail|domready|300|touchend|win|nodeType|expandEffect|getTarget|items|getRelated|float|relative|opacityReverse|j13|z38|z37|padY|on|scrPad||z41|contains|hintPosition|padX|image|hasChild|Array|cbs|kill|div|showTitle|storage|clearTimeout|_tmpp|z9|t15|array|round|inner|fade|showLoading|ieBack|prevItem|slide|continue|requestAnimationFrame|createTextNode|selector|number|z34|toString|presto|Out|center|link|In|scrollLeft|original|sizing|divTag|onload|getButton|hasOwnProperty|paddingBottom|magicthumb|mode|slideshowEffect|paddingLeft|easing|paddingRight|restoreSpeed|loading|Doc|j18|borderWidth|t29|unload|exec|paddingTop|dashize|isNaN||uninitialized|200|magicJS|busy|pause|alwaysShowZoom|hintVisible|zoomDistance|dragMode|complete|render|MagicZoom|activate|getElementsByTagName|t14|setTimeout|onStart|onready|readyState|offset|keepThumbnail|slideshowLoop|pow|css3Transformations|initMouseEvent|captionPosition|gd56f7fsgd|setupHint|z43|z48|z36|_unbind|set|static|z45|expandTrigger|entireImage|rev|swap|mouseup|z35|throw|activatedEx|cloneNode|shift|z24|J_EUID|z13|_timer|zoomAlign|pad|100000px|calc|z18|events|changeContent|j19|forceAnimation|z29|z14|10000|onBeforeRender|restoreEffect|splice|refresh|t10||t13|j10|string|250|getElementsByClassName|lang|span|hintClass|buttons|t32|touch|currentStyle|boolean|close|vertical|touchstart|updating|css3Animation|backgroundColor|replaceChild|innerHeight|innerWidth|newImg|j31|ms|hoverTimer|pounce|PFX|z11|hintOpacity|selectorsEffectSpeed|loadingOpacity|speed|not|found|MagicJS|color|innerHTML|inline|random|z0|screenPadding|class|String|floor|9_|ieMode|MagicZoomPup|pointer|Plus|clickToActivate|ddx|error|abort|ddy|Ff|150|zoomViewHeight|lastSelector|defaults|documentElement|smoothing|initTopPos|initLeftPos|z33|_handlers|onerror|expandPosition|next|effect|holder|_deprecated|z10|dblclick|IMG|titleSource|_lang|500|naturalWidth|compatMode|features|dissolve|to|400|join|loadingMsg|theme_mac|identifier|button|item|namespaces|Right|media|previous|enabled|outline|cancelAnimationFrame|contextmenu|toggle|createEvent|panZoom|targetTouches|_event_prefix_|styleSheets|lastTap|callee|changedTouches|expandSpeed|implement|platform|ts|Bottom|hover|swapTimer|Top|selectorsMouseoverDelay|getStorage|Left||mzParams|rect|element|t18|parseExOptions|1000|restoreTrigger|z26|borderBottomWidth|ufx|onErrorHandler|mousemove|slideshowSpeed|onabort|url|_event_del_|t17|_event_add_|overlapBox|je3|navigator|destroy|borderTopWidth|preventDefault|Zoom|z20|j15|t6|getBox|prevent|custom|query|tl|keydown|head|clickToInitialize|moveOnClick|initialize|loadingPositionX|mousedown|z15|z23|loadingPositionY|shadow|HTMLElement|expandAlign|clickToDeactivate|z1Holder|nWidth|big|thumbChange|defaultView|stopAnimation|construct|startTime|textAlign|linkTarget|open|backgroundOpacity|loop|toggleMZ|smoothingSpeed|Function|10000px|backgroundSpeed|gecko|javascript|horizontal|t11|loopBind|buttonsPosition|Slide|request|offsetHeight||z46|external|J_EXTENDED|captionSource|IFRAME|uuid|cos|PI|out|el_arr|hspace|preservePosition|tc|ratio|tr|Event|touches|vspace|reverse|styleFloat|ceil|t12|5001|borderLeftWidth|caller|5000|z16|_z37|j12|resize|setupContent|touchmove|insertBefore|UUID|nativize|toArray|requestFullScreen|Date|backcompat|z31|date|od|charAt|DocumentTouch|concat|relatedTarget|opera|cancelBubble|mobile|hone|android|phone|textnode|stopPropagation|ip|z32|glow|compareDocumentPosition|wrap|getBoundingClientRect||change|transform|interval||object|preload|dispatchEvent|raiseEvent||getComputedStyle|420|1px|onError|styles_arr||finishTime|cubicIn|backIn|quadIn|expoIn|onAfterRender|sineIn|Alpha|elasticIn|DXImageTransform|setProps|Microsoft|offsetWidth|bounceIn|enclose|CancelFullScreen|Width|ios|XMLHttpRequest|xpath|zoomFade|localStorage|fitZoomWindow|z25|Khtml|preloadSelectorsSmall|preloadSelectorsBig|mozCancelAnimationFrame|blur|Moz|Webkit|z17|chrome|which|changeEventName|errorEventName|cancel|cancelFullScreen|addEventListener|magiczoom|zoomWindowEffect|documentMode|z19|900|webkit419||roundCss|x7||Image|expandTriggerDelay|captionSpeed|t5|alt|captionHeight|t4|captionWidth|mac|buttonsDisplay|backgroundPosition|add||inherit|transparent|ig|Tahoma|fontFamily|t1|cssClass|t2|align|back|gecko181|borderRightWidth|map|fontSize|fontWeight|101|charCodeAt|fromCharCode|urn|schemas|setLang|t19|isFinite|toFloat|adjBorder|moz|t20|t21|keyCode|v4|keyboardCtrl|keyboard|onKey|mask|cbClick|magicthumb_ie_ex|behavior|vml|com|microsoft|VML|backgroundImage|curLeft|cbHover|t8|sMagicZoom|fill|buttonClose|t7|z22|buttonPrevious|j20|unselectable|move|j28|zoomFadeOutSpeed|disable|_bind||entire|drag|zoomFadeInSpeed|naturalHeight|isReady|nHeight|z8|v2|buttonNext|z27|z39|Loading|abs|210|FullScreen|211|webkitIsFullScreen|fullscreenerror|220|dir|270|applicationCache|260|fullscreenchange|RequestFullScreen|msPerformance|190|rtl|191|192|419|181|ctrlKey|khtml|525|performance|postMessage|metaKey|009|cssFloat|clientWidth|clientHeight|presto925|MagicZoomHeader|childNodes|iframe|pageXOffset|pageYOffset|execCommand|cbhover|byClass|zoomItem|scrollWidth|scrollHeight|innerText|html|progid|lef||filters|hasLayout|getPropertyValue|AnimationName||fixed|setAttribute|offsetTop|offsetParent|offsetLeft|clientLeft|j11|clientTop|j4|msCancelAnimationFrame|blackberry|blazer|compal|bada|avantgo|querySelector|ontouchstart|tablet|elaine|fennec|lge|maemo|midp|kindle|iris|hiptop|iemobile|runtime|air|KeyboardEvent|KeyEvent|regexp|UIEvent|MouseEvent|exists|collection|Object|slice|getTime|setInterval|userAgent|evaluate|toUpperCase|insertRule|getElementById|addCSS|mmp|netfront|MagicZoomPlusHint|webos|linux|magiczoomplus|unknown|mozInnerScreenY|WebKitPoint|taintEnabled|other|mozRequestAnimationFrame|autohide|webkitCancelRequestAnimationFrame|Transform|oCancelAnimationFrame|msRequestAnimationFrame|webkitRequestAnimationFrame|oRequestAnimationFrame|getBoxObjectFor|ActiveXObject|re|plucker|pocket|ixi|os|ob|palm|psp|symbian|windows|xda|xiino|wap|vodafone|treo|up|animationName|DOMElement|imageSize|swapImage|MagicZoomHint|MagicThumbHint|Expand|clickTo|_self|distance|always|text|msg|Next|Previous|source|preserve|deactivate|slideOut|slideIn|caption|618|backOut|cubicOut|9999|ga2c976a|quadOut|MagicZoomLoading|getAttribute|bounceOut|_blank|amp|000000|10001|getAttributeNode|elasticOut|delay|Close|textDecoration|hand|MagicZoomBigImageCont|selectstart|MozUserSelect||highlight|elastic|10002|trident900|3px|MagicBoxGlow|frameBorder|MagicBoxShadow|sine|cubic|quad|tap|callout|getXY|middle|loader|Magic|Invalid|small|lastChild|initializing|z12|480|user|select|bounce|expo|mt|_new|expoOut|lt|initEvent|createEventObject|magic|cssText|tile|stroked|eventType|createStyleSheet|doScroll|DOMContentLoaded|currentTarget|loaded|fireEvent|temporary|detachEvent|attachEvent|clientY|target|pageY|clientX|returnValue|pageX|srcElement|fromElement|insertAdjacentHTML|removeEventListener|beforeEnd|BackgroundImageCache|toElement|coords|owningElement|111|rc24|00001|no|backgroundRepeat|sort|curFrame|clearInterval|repeat|ccc|sineOut|nextSibling'.split('|'), 0, {}))

function wpShowMenuPopup(objMenu, event, popupId) {
    if (typeof wpCustommenuTimerHide[popupId] != 'undefined') clearTimeout(wpCustommenuTimerHide[popupId]);
    objMenu = $(objMenu.id);
    var popup = $(popupId);
    if (!popup) return;
    if (!!wpActiveMenu) {
        wpHideMenuPopup(objMenu, event, wpActiveMenu.popupId, wpActiveMenu.menuId);
    }
    if (!objMenu.hasClassName('hovered')) {
        objMenu.addClassName('hovered');
        var wpMenuAnchor = $(objMenu.select('a')[0]);
        wpChangeTopMenuHref(wpMenuAnchor);
    }
    wpActiveMenu = {
        menuId: objMenu.id,
        popupId: popupId
    };
    if (!objMenu.hasClassName('active')) {
        wpCustommenuTimerShow[popupId] = setTimeout(function() {
            objMenu.addClassName('active');
            var popupWidth = CUSTOMMENU_POPUP_WIDTH;
            if (!popupWidth) popupWidth = popup.getWidth();
            var pos = wpPopupPos(objMenu, popupWidth);
            popup.style.top = pos.top + 'px';
            popup.style.left = pos.left + 'px';
            wpSetPopupZIndex(popup);
            if (CUSTOMMENU_POPUP_WIDTH)
                popup.style.width = CUSTOMMENU_POPUP_WIDTH + 'px';
            var block2 = $(popupId).select('div.block2');
            if (typeof block2[0] != 'undefined') {
                var wStart = block2[0].id.indexOf('_w');
                if (wStart > -1) {
                    var w = block2[0].id.substr(wStart + 2);
                } else {
                    var w = 0;
                    $(popupId).select('div.block1 div.column').each(function(item) {
                        w += $(item).getWidth();
                    });
                }
                if (w) block2[0].style.width = w + 'px';
            }
            popup.style.display = 'block';
        }, CUSTOMMENU_POPUP_DELAY_BEFORE_HIDING);
    }
}

function wpHideMenuPopup(element, event, popupId, menuId) {
    if (typeof wpCustommenuTimerShow[popupId] != 'undefined') clearTimeout(wpCustommenuTimerShow[popupId]);
    var element = $(element);
    var objMenu = $(menuId);
    var popup = $(popupId);
    if (!popup) return;
    var wpCurrentMouseTarget = getCurrentMouseTarget(event);
    if (!!wpCurrentMouseTarget) {
        if (!wpIsChildOf(element, wpCurrentMouseTarget) && element != wpCurrentMouseTarget) {
            if (!wpIsChildOf(popup, wpCurrentMouseTarget) && popup != wpCurrentMouseTarget) {
                if (objMenu.hasClassName('active')) {
                    wpCustommenuTimerHide[popupId] = setTimeout(function() {
                        objMenu.removeClassName('active');
                        popup.style.display = 'none';
                    }, CUSTOMMENU_POPUP_DELAY_BEFORE_HIDING);
                }
            }
        }
    }
}

function wpPopupOver(element, event, popupId, menuId) {
    if (typeof wpCustommenuTimerHide[popupId] != 'undefined') clearTimeout(wpCustommenuTimerHide[popupId]);
}

function wpPopupPos(objMenu, w) {
    var pos = objMenu.cumulativeOffset();
    var wraper = $('custommenu');
    var posWraper = wraper.cumulativeOffset();
    var xTop = pos.top - posWraper.top
    if (CUSTOMMENU_POPUP_TOP_OFFSET) {
        xTop += CUSTOMMENU_POPUP_TOP_OFFSET;
    } else {
        xTop += objMenu.getHeight();
    }
    var res = {
        'top': xTop
    };
    if (CUSTOMMENU_RTL_MODE) {
        var xLeft = pos.left - posWraper.left - w + objMenu.getWidth();
        if (xLeft < 0) xLeft = 0;
        res.left = xLeft;
    } else {
        var wWraper = wraper.getWidth();
        var xLeft = pos.left - posWraper.left;
        if ((xLeft + w) > wWraper) xLeft = wWraper - w;
        if (xLeft < 0) xLeft = 0;
        res.left = xLeft;
    }
    return res;
}

function wpChangeTopMenuHref(wpMenuAnchor) {
    var wpValue = wpMenuAnchor.href;
    wpMenuAnchor.href = wpMenuAnchor.rel;
    wpMenuAnchor.rel = wpValue;
}

function wpIsChildOf(parent, child) {
    if (child != null) {
        while (child.parentNode) {
            if ((child = child.parentNode) == parent) {
                return true;
            }
        }
    }
    return false;
}

function wpSetPopupZIndex(popup) {
    $$('.wp-custom-menu-popup').each(function(item) {
        item.style.zIndex = '9999';
    });
    popup.style.zIndex = '10000';
}

function getCurrentMouseTarget(xEvent) {
    var wpCurrentMouseTarget = null;
    if (xEvent.toElement) {
        wpCurrentMouseTarget = xEvent.toElement;
    } else if (xEvent.relatedTarget) {
        wpCurrentMouseTarget = xEvent.relatedTarget;
    }
    return wpCurrentMouseTarget;
}

function getCurrentMouseTargetMobile(xEvent) {
    if (!xEvent) var xEvent = window.event;
    var wpCurrentMouseTarget = null;
    if (xEvent.target) wpCurrentMouseTarget = xEvent.target;
    else if (xEvent.srcElement) wpCurrentMouseTarget = xEvent.srcElement;
    if (wpCurrentMouseTarget.nodeType == 3)
        wpCurrentMouseTarget = wpCurrentMouseTarget.parentNode;
    return wpCurrentMouseTarget;
}

function wpMenuButtonToggle() {
    $('menu-content').toggle();
    $('menu-background').toggle();
}

function wpGetMobileSubMenuLevel(id) {
    var rel = $(id).readAttribute('rel');
    return parseInt(rel.replace('level', ''));
}

function wpSubMenuToggle(obj, activeMenuId, activeSubMenuId) {
    var currLevel = wpGetMobileSubMenuLevel(activeSubMenuId);
    $$('.wp-custom-menu-submenu').each(function(item) {
        if (item.id == activeSubMenuId) return;
        var xLevel = wpGetMobileSubMenuLevel(item.id);
        if (xLevel >= currLevel) {
            $(item).hide();
        }
    });
    $('custommenu-mobile').select('span.button').each(function(xItem) {
        var subMenuId = $(xItem).readAttribute('rel');
        if (!$(subMenuId).visible()) {
            $(xItem).removeClassName('open');
        }
    });
    if ($(activeSubMenuId).getStyle('display') == 'none') {
        $$('.wp-custom-menu-submenu.level1.show-all').each(function(i) {
            i.removeClassName('show-all')
        })
        $(activeSubMenuId).show();
        $(obj).addClassName('open');
    } else {
        $(activeSubMenuId).removeClassName('show-all');
        $(activeSubMenuId).hide();
        $(obj).removeClassName('open');
    }
}

function wpResetMobileMenuState() {
    $('menu-content').hide();
    $$('.wp-custom-menu-submenu').each(function(item) {
        $(item).hide();
    });
    $('custommenu-mobile').select('span.button').each(function(item) {
        $(item).removeClassName('open');
    });
}

function wpCustomMenuMobileToggle() {
    var w = window,
        d = document,
        e = d.documentElement,
        g = d.getElementsByTagName('body')[0],
        x = w.innerWidth || e.clientWidth || g.clientWidth,
        y = w.innerHeight || e.clientHeight || g.clientHeight;
    if (x < 800 || wpIsMobile.any()) {
        $('custommenu').hide();
        $('custommenu-mobile').show();
    } else {
        $('custommenu-mobile').hide();
        $('custommenu').show();
    }
}

function wpSubMenuViewAll(obj) {
    var currentMenuItem = $(obj).up('.level1');
    currentMenuItem.addClassName('show-all');
}
Event.observe(window, 'resize', function() {});
var wpIsMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (wpIsMobile.Android() || wpIsMobile.BlackBerry() || wpIsMobile.iOS() || wpIsMobile.Opera() || wpIsMobile.Windows());
    }
};
var FORMALIZE = (function(window, document, undefined) {
    function IE(version) {
        var b = document.createElement('b');
        b.innerHTML = '<!--[if IE ' + version + ']><br><![endif]-->';
        return !!b.getElementsByTagName('br').length;
    }
    var PLACEHOLDER_SUPPORTED = 'placeholder' in document.createElement('input');
    var AUTOFOCUS_SUPPORTED = 'autofocus' in document.createElement('input');
    var IE6 = IE(6);
    var IE7 = IE(7);
    return {
        go: function() {
            var i, j = this.init;
            for (i in j) {
                j.hasOwnProperty(i) && j[i]();
            }
        },
        init: {
            disable_link_button: function() {
                $(document.documentElement).observe('click', function(ev) {
                    var el = ev.target;
                    var is_disabled = el.tagName.toLowerCase() === 'a' && el.className.match('button_disabled');
                    if (is_disabled) {
                        ev.preventDefault();
                    }
                });
            },
            full_input_size: function() {
                if (!IE7 || !$$('textarea, input.input_full').length) {
                    return;
                }
                $$('textarea, input.input_full').each(function(el) {
                    Element.wrap(el, 'span', {
                        'class': 'input_full_wrap'
                    });
                });
            },
            ie6_skin_inputs: function() {
                if (!IE6 || !$$('input, select, textarea').length) {
                    return;
                }
                var button_regex = /button|submit|reset/;
                var type_regex = /date|datetime|datetime-local|email|month|number|password|range|search|tel|text|time|url|week/;
                $$('input').each(function(el) {
                    if (el.getAttribute('type').match(button_regex)) {
                        el.addClassName('ie6_button');
                        if (el.disabled) {
                            el.addClassName('ie6_button_disabled');
                        }
                    } else if (el.getAttribute('type').match(type_regex)) {
                        el.addClassName('ie6_input');
                        if (el.disabled) {
                            el.addClassName('ie6_input_disabled');
                        }
                    }
                });
                $$('textarea, select').each(function(el) {
                    if (el.disabled) {
                        el.addClassName('ie6_input_disabled');
                    }
                });
            },
            autofocus: function() {
                if (AUTOFOCUS_SUPPORTED || !$$('[autofocus]').length) {
                    return;
                }
                var el = $$('[autofocus]')[0];
                if (!el.disabled) {
                    el.focus();
                }
            },
            placeholder: function() {
                if (PLACEHOLDER_SUPPORTED || !$$('[placeholder]').length) {
                    return;
                }
                FORMALIZE.misc.add_placeholder();
                $$('[placeholder]').each(function(el) {
                    if (el.type === 'password') {
                        return;
                    }
                    var text = el.getAttribute('placeholder');
                    var form = el.up('form');
                    el.observe('focus', function() {
                        if (el.value === text) {
                            el.value = '';
                            el.removeClassName('placeholder_text');
                        }
                    });
                    el.observe('blur', function() {
                        FORMALIZE.misc.add_placeholder();
                    });
                    form.observe('submit', function() {
                        if (el.value === text) {
                            el.value = '';
                            el.removeClassName('placeholder_text');
                        }
                    });
                    form.observe('reset', function() {
                        setTimeout(FORMALIZE.misc.add_placeholder, 50);
                    });
                });
            }
        },
        misc: {
            add_placeholder: function() {
                if (PLACEHOLDER_SUPPORTED || !$$('[placeholder]').length) {
                    return;
                }
                $$('[placeholder]').each(function(el) {
                    if (el.type === 'password') {
                        return;
                    }
                    var text = el.getAttribute('placeholder');
                    if (!el.value || el.value === text) {
                        el.value = text;
                        el.addClassName('placeholder_text');
                    }
                });
            }
        }
    };
})(this, this.document);
$(document).observe('dom:loaded', function() {
    FORMALIZE.go();
});
var floatingMenu = Class.create({
    initialize: function(elm, options) {
        elm.each(function(elm) {
            var menu = elm;
            var topOfMenu = menu.cumulativeOffset().top + 150;
            Event.observe(window, 'scroll', function(evt) {
                var y = document.viewport.getScrollOffsets().top;
                if (y >= topOfMenu) {
                    menu.addClassName('fixed');
                } else {
                    menu.removeClassName('fixed');
                }
            });
        }.bind(this));
        this.options = Object.extend({}, options || {});
    }
});
document.observe("dom:loaded", function() {
    new floatingMenu($$('.floating_menu'));
});