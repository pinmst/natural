var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

function resizeHeight() {
    var arHeights = new Array();
    if ($('sli_recommender_pdp')) {
        var sliBlock = $('sli_recommender_pdp');
        sliBlock.select('li').each(function(el) {
            var cat_size = el.select('.cat-size');
            cat_size[0].setStyle({
                height: 'auto'
            });
            arHeights.push(cat_size[0].scrollHeight);
        });
        sliBlock.select('li').each(function(el) {
            var cat_size = el.select('.cat-size');
            cat_size[0].setStyle({
                height: arHeights.max() + "px"
            });
        });
    }
}

function hideFilter() {
    if ($$('.block-layered-nav').length != 0) {
        if (document.viewport.getWidth() < 530) {
            if ($$('.block-layered-nav')[0].hasClassName('shown')) {
                $$('.block-layered-nav')[0].removeClassName('shown');
            }
        } else if (document.viewport.getWidth() > 530) {
            if (!$$('.block-layered-nav')[0].hasClassName('shown')) {
                $$('.block-layered-nav')[0].addClassName('shown');
            }
        }
    }
}

function elemFocus(element) {
    Element.scrollTo(element);
}
Event.observe(window, 'resize', function() {
    resizeHeight();
    hideFilter();
});
document.observe('dom:loaded', function() {
    resizeHeight();
    hideFilter();
});
document.observe('sliRecsLoaded', function() {
    resizeHeight();
});

function asignQuantityCounter() {
    $$('.icon-thin-arrow-left').each(function(element) {
        $(element).observe('click', function() {
            var quantityInputBox = this.previous();
            if (parseInt(quantityInputBox.value) == 1) {
                quantityInputBox.value = 0;
            } else if (parseInt(quantityInputBox.value) > 1) {
                quantityInputBox.value = parseInt(quantityInputBox.value) - 1;
            }
        });
    });
    $$('.icon-thin-arrow-right').each(function(element) {
        $(element).observe('click', function() {
            var quantityInputBox = this.next();
            if (parseInt(quantityInputBox.value) < 100) {
                quantityInputBox.value = parseInt(quantityInputBox.value) + 1;
            }
        });
    });
}

function addOverlayToWindow() {
    var B = document.body,
        H = document.documentElement;
    if (typeof document.height !== 'undefined') {
        height = document.height
    } else {
        height = Math.max(B.scrollHeight, B.offsetHeight, H.clientHeight, H.scrollHeight, H.offsetHeight);
    }
    var windowOverlay = '<div id="overlay_modal" class="overlay_alert" style="background: #333333; position: absolute; top: 0px; left: 0px; z-index: 10; width: 100%; height: ' + height + 'px; opacity: 0.6000000238418579;"> </div>';
    $$('body')[0].insert({
        top: windowOverlay
    });
}

function removeOverlayFromWindow() {
    $('overlay_modal').remove();
}

function checkAllChechoutFormFields() {
    var validationMessages = $$('.validation-advice');
    if (validationMessages.length = !0) {
        validationMessages.each(function(validationMessage) {
            if (validationMessage.visible()) {
                if (!$('aw-onestepcheckout-warning-msg').visible()) {
                    Effect.BlindDown('aw-onestepcheckout-warning-msg');
                }
            } else {
                $('aw-onestepcheckout-warning-msg').hide();
            }
        });
    }
}

function charCounter(id, maxlimit, limited) {
    if (!$('counter-' + id)) {
        $(id).insert({
            after: '<div id="counter-' + id + '"></div>'
        });
    }
    if ($F(id).length >= maxlimit) {
        if (limited) {
            $(id).value = $F(id).substring(0, maxlimit);
        }
        $('counter-' + id).addClassName('charcount-limit');
        $('counter-' + id).removeClassName('charcount-safe');
    } else {
        $('counter-' + id).removeClassName('charcount-limit');
        $('counter-' + id).addClassName('charcount-safe');
    }
    $('counter-' + id).update($F(id).length + '/' + maxlimit);
}

function makeItCount(id, maxsize, limited) {
    if (limited == null) limited = true;
    if ($(id)) {
        Event.observe($(id), 'keyup', function() {
            charCounter(id, maxsize, limited);
        }, false);
        Event.observe($(id), 'keydown', function() {
            charCounter(id, maxsize, limited);
        }, false);
        charCounter(id, maxsize, limited);
    }
}

function splitCurrency(price, symbols, container) {
    var arrayLength = symbols.length;
    for (var i = 0; i < arrayLength; i++) {
        if (price.indexOf(symbols[i]) != -1) {
            var priceValue = price.split(symbols[i]);
            var priceValue = '<small>' + priceValue[0] + symbols[i] + '</small>' + priceValue[1];
            $(container).update(priceValue);
        }
    }
}

function moveContent() {
    if (Element.hasClassName($$("body")[0], 'catalog-product-view')) {
        var high = $$('.product-highlights')[0];
        var wrapper = $$('.mobile-highlights');
        if (wrapper[0] != undefined && high != undefined) {
            var initial = wrapper[0].lastElementChild;
            if (document.viewport.getWidth() <= 520) {
                if (high != undefined) {
                    wrapper[0].insertBefore(high, wrapper[0].firstChild);
                }
            } else {
                if (high != undefined) {
                    initial.insertBefore(high, initial.childElements().last());
                }
            }
        }
    }
    if (Element.hasClassName($$('.main')[0], 'col2-left-layout')) {
        var side = $$('.col-left')[0];
        var footer = $$('.footer-icons')[0];
        var initial = $$('.main > .row')[0];
        if (document.viewport.getWidth() <= 520) {
            footer.insertBefore(side, footer.firstChild);
        } else {
            initial.insert(side);
        }
    }
}
document.observe("dom:loaded", function() {
    moveContent();
    if ($$('.readmore')[0] != undefined) {
        $$('.readmore')[0].observe('click', function(el) {
            if (!el.target.hasClassName('less')) {
                el.target.previousElementSibling.setStyle({
                    height: 'auto'
                });
                el.target.addClassName('less');
                el.target.update('Read less');
            } else {
                el.target.removeClassName('less');
                el.target.update('Read more');
                el.target.previousElementSibling.setStyle({
                    height: '85px'
                });
            }
        });
    }
    if ($('instruction-toggle') != undefined) {
        var tog = $('aw-onestepcheckout-container').select('#instruction-toggle');
        tog.each(function(el) {
            el.observe('click', function(event) {
                $$('.main-instruction')[0].toggleClassName('unfold');
                event.stop();
            });
        });
    }
    if ($$('.viewall')[0] != undefined) {
        $$('.viewall').each(function(el) {
            el.observe('click', function(event) {
                var parent = $(this).up('.open');
                parent.up('div').childElements().each(function(a) {
                    if (a.hasClassName('hide')) {
                        a.toggleClassName('show');
                    } else if (a.hasClassName('show')) {
                        a.toggleClassName('hide');
                    }
                });
                if ($(this).down('.text-upper').innerHTML == 'View All') {
                    $(this).down('.text-upper').update('Hide All');
                } else {
                    $(this).down('.text-upper').update('View All');
                }
                event.stop();
            });
        });
    }
    $$('a').each(function(globalLink) {
        var globalLinkUrl = globalLink.href,
            globalLinkUrlHash = globalLinkUrl.split('#')[1];
        if (globalLinkUrlHash != 'next' && globalLinkUrlHash != 'previous') {
            globalLink.observe('click', function(event) {
                if (globalLinkUrl.split('#')[0] == document.URL) {
                    if (globalLinkUrlHash) {
                        Event.stop(event);
                        var elementLink = this.readAttribute('href');
                        Effect.ScrollTo($$('a[name=' + globalLinkUrlHash + ']')[0], {
                            duration: '1',
                            offset: -90
                        });
                        return false;
                    }
                }
            });
        }
    });
    if ($$('.catalog-product-view')[0] != undefined) {
        if ($$('.grouped-items-list')[0] != undefined) {
            $$('.grouped-items-list .price-box .price-numbers').each(function(unformatedPrice) {
                if (unformatedPrice.innerText.indexOf(".") != -1) {
                    if (unformatedPrice.innerHTML.length <= 8) {
                        var splitPrice = unformatedPrice.innerHTML.split('.');
                        $(unformatedPrice).update(splitPrice[0] + '<b>.' + splitPrice[1] + '</b>');
                    }
                }
            });
            $$('.grouped-items-list .price-box .price-numbers .price').each(function(unformatedPrice) {
                if (unformatedPrice.innerText.indexOf('.') != -1) {
                    var splitPrice = unformatedPrice.innerHTML.split('.');
                    $(unformatedPrice).update(splitPrice[0] + '<b>.' + splitPrice[1] + '</b>');
                }
            });
        } else {
            $$('.options-row .price-box .price .price-numbers').each(function(unformatedPrice) {
                if (unformatedPrice.innerText.indexOf(".") != -1) {
                    var splitPrice = unformatedPrice.innerHTML.split('.');
                    $(unformatedPrice).update(splitPrice[0] + '<b>.' + splitPrice[1] + '</b>');
                }
            });
        }
    }
    if ($$('.category-links')[0] != undefined) {
        $$('.category-links a').invoke('observe', 'click', function(event) {
            Event.stop(event);
            var elementLink = this.readAttribute('href');
            var scrollHere = elementLink.substr(1, elementLink.length);
            Effect.ScrollTo(scrollHere, {
                duration: '1',
                offset: -300
            });
            return false;
        });
    }
    if ($$('.checkout-cart-index')[0] != undefined) {
        asignQuantityCounter();
        $$('.cart-price .price').each(function(element) {
            var currentPrice = element.innerHTML;
            var allCurrencies = ['$', '£', '¥', '€', '₩'];
            splitCurrency(currentPrice, allCurrencies, element);
        });
    }
    $$('.product-review-link').each(function(link) {
        $(link).observe('click', function(event) {
            var top = 0;
            if (document.viewport.getWidth() <= 767) {
                top = -180;
            } else {
                top = -310;
            }
            Effect.ScrollTo(link.readAttribute('data-target'), {
                duration: '0.5',
                offset: top
            });
            return false;
        });
    });
    if ($$('.cat-id-12')[0] != undefined) {
        if ($$("body")[0].hasClassName("ie8")) {
            var categoryWrapperOne = '';
            for (var i = 0; i < 14; ++i) {
                var categoryWrap = '<li>' + $$('li.category-name')[i].innerHTML + '</li>';
                var categoryWrapperOne = categoryWrapperOne + categoryWrap;
            }
            var categoryWrap = '';
            var categoryWrapperOne = '<div class="category-inner-wrap">' + categoryWrapperOne + '</div>';
            var categoryWrapperTwo = '';
            for (var i = 15; i < 30; ++i) {
                var categoryWrap = '<li>' + $$('li.category-name')[i].innerHTML + '</li>';
                var categoryWrapperTwo = categoryWrapperTwo + categoryWrap;
            }
            var categoryWrap = '';
            var categoryWrapperTwo = '<div class="category-inner-wrap">' + categoryWrapperTwo + '</div>';
            var categoryWrapperThree = '';
            for (var i = 30; i < 45; ++i) {
                var categoryWrap = '<li>' + $$('li.category-name')[i].innerHTML + '</li>';
                var categoryWrapperThree = categoryWrapperThree + categoryWrap;
            }
            var categoryWrap = '';
            var categoryWrapperThree = '<div class="category-inner-wrap">' + categoryWrapperThree + '</div>';
            $('category-name-wrap').update(categoryWrapperOne + categoryWrapperTwo + categoryWrapperThree);
        }
    }
    if ($$('.catalog-category-view')[0] != undefined || $$('.brand-index-view')[0] != undefined) {
        if (window.location.search != "") {
            Effect.ScrollTo($$('.block-layered-nav')[0], {
                duration: '1',
                offset: -90
            });
        }
        if ($$('.sorter-pager .pages')[0].empty()) {
            $$('.sorter-pager .pages').invoke('addClassName', 'hidden-mobile');
        }
    }
    if ($$('.checkout-cart-index')[0] != undefined) {
        $$('.promo-whats-this a').each(function(whatsThisLink) {
            whatsThisLink.observe('click', function(event) {
                addOverlayToWindow.delay(0.4);
                var giftCodeHelpContent = $$('.promo-whats-this-content')[0].innerHTML;
                var quickViewWindow = new Window({
                    className: 'alphacube',
                    width: 450,
                    height: 170,
                    zIndex: 100,
                    showEffectOptions: {
                        duration: 0.4
                    },
                    hideEffectOptions: {
                        duration: 0.4
                    },
                    minimizable: false,
                    maximizable: false,
                    keepMultiModalWindow: false,
                    onClose: removeOverlayFromWindow
                });
                quickViewWindow.getContent().innerHTML = giftCodeHelpContent;
                quickViewWindow.showCenter();
                return false;
            });
        });
    }
    if ($$('.aw-onestepcheckout-index-index')[0] != undefined) {
        makeItCount('comments', 100);
    }
    Event.observe(window, "resize", function() {
        uHeight();
        moveContent();
    });
});

function showPopup(sUrl) {
    var docWidth = document.viewport.getWidth();
    var docHeight = document.viewport.getHeight();
    var height = 0;
    if (docWidth < 600 && docHeight > docWidth) {
        height = docWidth - 20;
    } else if (docWidth < 600 && docHeight <= docWidth) {
        height = docHeight - 40;
    } else if (docWidth > 600 && docHeight <= 600) {
        height = docHeight - 40;
    } else {
        height = 600;
    }
    var height = 0;
    if (document.viewport.getWidth() > 600) {
        height = 600;
    } else if (document.viewport.getWidth() <= 600 && document.viewport.getWidth() <= document.viewport.getHeight()) {
        height = document.viewport.getWidth() - 20;
    } else if (document.viewport.getWidth() <= 600 && document.viewport.getWidth() > document.viewport.getHeight()) {
        height = document.viewport.getHeight() - 20;
    }
    var _that = this;
    if (!_that.clicked) {
        _that.clicked = true;
        oPopup = new Window({
            id: 'popup_window',
            className: 'healthpost',
            url: sUrl,
            maximizable: false,
            showEffectOptions: {
                duration: 0.2
            },
            onload: function() {
                var d = this.contentWindow.document.getElementsByTagName('body')[0];
                Element.setStyle(d.getElementsByTagName('img')[0], {
                    width: '100%',
                    height: 'auto'
                });
            },
            onShow: function() {
                $('overlay_modal').observe('click', function(e) {
                    Windows.close('popup_window');
                });
            },
            draggable: false,
            resizable: false,
            maximizable: false,
            width: 600,
            height: height,
            hideEffectOptions: {
                duration: 0.2
            },
            destroyOnClose: true,
            closeCallback: function() {
                _that.clicked = false;
                $$("body")[0].setStyle({
                    overflowX: 'auto',
                    position: 'initial'
                });
                return true;
            }
        });
        $$("body")[0].setStyle({
            overflowX: 'hidden',
            position: 'static'
        });
        oPopup.setZIndex(1000);
        oPopup.showCenter(true);
    }
}

function showStaticPopup(id, new_id) {
    var _that = this;
    if (!_that.clicked) {
        var w = 1200,
            h = 800;
        if (new_id == 'popupContentImage') {
            w = 600;
            h = 600;
            
        }
        _that.clicked = true;
        sPopup = new Window({
            id: new_id,
            maximizable: false,
            className: 'staticPop',
            showEffectOptions: {
                duration: 0.2
            },
            onShow: function() {
                $('overlay_modal').observe('click', function(e) {
                    Windows.close(new_id, event);
                });
            },
            draggable: false,
            resizable: false,
            maximizable: false,
            width: w,
            height: h,
            hideEffectOptions: {
                duration: 0.2
            },
            destroyOnClose: true,
            closeCallback: function() {
                _that.clicked = false;
                $$("body")[0].setStyle({
                    overflowX: 'auto',
                    position: 'initial'
                });
                return true;
            }
        });
        $$("body")[0].setStyle({
            overflowX: 'hidden',
            position: 'static'
        });
        sPopup.setContent(id, false, false);
        sPopup.setZIndex(1000);
        sPopup.showCenter(true);
    }
	return false;
}

function closePopup() {
    Windows.close('popup_window');
    $$("body")[0].setStyle({
        overflowX: 'auto',
        position: 'initial'
    });
    document.stopObserving('click');
}

function uHeight() {
    var h;
    h = document.viewport.getHeight() - 80;
    if ($('popupContent') && $('popupContent').getHeight() > h) {
        $('popupContent').setStyle({
            height: 0
        });
        $('popupContent').setStyle({
            height: h
        });
    }
}

function isPrintMode() {
    var printOnlyElements = document.getElementsByClassName("print-only");
    var style = window.getComputedStyle(printOnlyElements[0]);
    if (style.display != "none") {
        return true;
    } else {
        return false;
    }
}
RegionUpdaterContacts = Class.create();
RegionUpdaterContacts.prototype = {
    initialize: function(countryEl, regionTextEl, regionSelectEl, regions, disableAction, zipEl) {
        this.countryEl = $(countryEl);
        this.regionTextEl = $(regionTextEl);
        this.regionSelectEl = $(regionSelectEl);
        this.zipEl = $(zipEl);
        this.config = regions['config'];
        delete regions.config;
        this.regions = regions;
        this.disableAction = (typeof disableAction == 'undefined') ? 'hide' : disableAction;
        this.zipOptions = (typeof zipOptions == 'undefined') ? false : zipOptions;
        if (this.regionSelectEl.options.length <= 1) {
            this.update();
        }
        Event.observe(this.countryEl, 'change', this.update.bind(this));
    },
    _checkRegionRequired: function() {
        var label, wildCard;
        var elements = [this.regionTextEl, this.regionSelectEl];
        var that = this;
        if (typeof this.config == 'undefined') {
            return;
        }
        var regionRequired = this.config.regions_required.indexOf(this.countryEl.value) >= 0;
        elements.each(function(currentElement) {
            Validation.reset(currentElement);
            label = $$('label[for="' + currentElement.id + '"]')[0];
            if (label) {
                wildCard = label.down('em') || label.down('span.required');
                if (regionRequired) {
                    label.up().show();
                } else {
                    label.up().hide();
                }
            }
        });
    },
    update: function() {
        if (this.regions[this.countryEl.value]) {
            var i, option, region, def;
            def = this.regionSelectEl.getAttribute('defaultValue');
            if (this.regionTextEl) {
                if (!def) {
                    def = this.regionTextEl.value.toLowerCase();
                }
                this.regionTextEl.value = '';
            }
            if (this.regionSelectEl && this.regionSelectEl.value && !def) {
                def = this.regionSelectEl.value;
            }
            this.regionSelectEl.options.length = 1;
            for (regionId in this.regions[this.countryEl.value]) {
                region = this.regions[this.countryEl.value][regionId];
                option = document.createElement('OPTION');
                option.value = regionId;
                option.text = region.name.stripTags();
                option.title = region.name;
                if (this.regionSelectEl.options.add) {
                    this.regionSelectEl.options.add(option);
                } else {
                    this.regionSelectEl.appendChild(option);
                }
                if (regionId == def || (region.name && region.name.toLowerCase() == def) || (region.name && region.code.toLowerCase() == def)) {
                    this.regionSelectEl.value = regionId;
                }
            }
            if (this.disableAction == 'hide') {
                if (this.regionTextEl) {
                    this.regionTextEl.style.display = 'none';
                }
                this.regionSelectEl.style.display = '';
            } else if (this.disableAction == 'disable') {
                if (this.regionTextEl) {
                    this.regionTextEl.disabled = true;
                }
                this.regionSelectEl.disabled = false;
            }
            this.setMarkDisplay(this.regionSelectEl, true);
        } else {
            if (this.disableAction == 'hide') {
                if (this.regionTextEl) {
                    this.regionTextEl.style.display = '';
                }
                this.regionSelectEl.style.display = 'none';
                Validation.reset(this.regionSelectEl);
            } else if (this.disableAction == 'disable') {
                if (this.regionTextEl) {
                    this.regionTextEl.disabled = false;
                }
                this.regionSelectEl.disabled = true;
            } else if (this.disableAction == 'nullify') {
                this.regionSelectEl.options.length = 1;
                this.regionSelectEl.value = '';
                this.regionSelectEl.selectedIndex = 0;
                this.lastCountryId = '';
            }
            this.setMarkDisplay(this.regionSelectEl, false);
        }
        this._checkRegionRequired();
    },
    setMarkDisplay: function(elem, display) {
        elem = $(elem);
        var labelElement = elem.up(0).down('label > span.required') || elem.up(1).down('label > span.required') || elem.up(0).down('label.required > em') || elem.up(1).down('label.required > em');
        if (labelElement) {
            inputElement = labelElement.up().next('input');
            if (display) {
                labelElement.show();
            } else {
                labelElement.hide();
            }
        }
    }
};