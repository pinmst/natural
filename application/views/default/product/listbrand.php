<div class="main col2-left-layout container">
  <!-- ESI START DUMMY COMMENT [] -->
  <!-- ESI URL DUMMY COMMENT -->
<div class="row">
<div id="main" class="col-main col-sm-12">
<h1 class="page-title category-title">Thương hiệu <?php echo $brand[0]['title_vn'] ?></h1>
<div class="category-top"></div>

	<div class="grid">
		<div class="clear"></div>
			<div class="wap-brand">
			<?php 
			if(!empty($info)){
				$i=0;
				foreach ($info as $item) {
			?>
			<div class="col-lg-5th-1 "> 
                <div class="home-item home-item--product-grid products-grid__item category-grid">
                    <div class="product-grid__top">
                        <div class="product-images">
                            <a href="<?php echo base_url('thuong-hieu/'.$item['alias']); ?>" title="<?php echo $item['title_vn']; ?>" class="product-image">
                                <img data-src="<?php echo PATH_IMG_MANUFACTURER.$item['images']; ?>" alt="<?php echo $item['title_vn']; ?>" class="lazy" width="100%" src="<?php echo PATH_IMG_MANUFACTURER.$item['images']; ?>" data-loaded="true">
                                <p><a href="<?php echo base_url('thuong-hieu/'.$item['alias']); ?>"><?php echo $item['title_vn']; ?></a></p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
			<?php 
				}
			}else{ ?>
		  		<div class="alert alert-danger">Không có dữ liệu</div>
			<?php } ?>
			</div>
		</div>
	<div class="space_10"></div>
</div>
</div>
</div>