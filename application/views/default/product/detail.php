<div class="main col1-layout container">
  <!-- ESI START DUMMY COMMENT [] -->
  <!-- ESI URL DUMMY COMMENT -->

  <!-- ESI END DUMMY COMMENT [] -->
  <div id="main" class="col-main row">
      <div class="breadcrumbs">
          <ul itemprop="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
              <li class="home" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                  <a itemprop="item" href="" title="Go to Home Page">Home</a>
                  <meta itemprop="name" content="Home" />
                  <meta itemprop="position" content="1" />
                  <span></span>
              </li>
              <li class="product" itemprop="itemListElement" itemscope itemty<?php echo $prod[0]['title_vn'] ?>pe="http://schema.org/ListItem">
                  <strong><?php echo $prod[0]['title_vn'] ?></strong>
                  <meta itemprop="name" content="<?php echo $prod[0]['title_vn'] ?>" />
                  <meta itemprop="position" content="2" />
              </li>
          </ul>
      </div>
      <script type="text/javascript">
          var optionsPrice = new Product.OptionsPrice([]);
      </script>

      <div class="product-view" itemscope itemtype="http://schema.org/Product">
        <form action="<?php echo base_url('addcart_fromcat'); ?>" method="post" class="cart" enctype='multipart/form-data'/>
          <div class="row">
            <div class="product-view-left col-sm-9">
                <!-- <input name="form_key" type="hidden" value="mnXHFyRvKORnlU3c" /> -->
   
                <div class="product-essential">
                  <div class="product-img-box col-sm-5" style="padding:0 20px;height: unset;">
                      <p class="product-image product-image-zoom" style="width: unset;height: unset;">
                        <img itemprop="image" id="image" src="<?php echo PATH_IMG_PRODUCT.$prod[0]['images'] ?>" alt="<?php echo $prod[0]['title_vn'] ?>" title="<?php echo $prod[0]['title_vn'] ?>" /> 
                      </p>
                    <div  id="slider3" style="padding: 20px 0;">
                      <ul>
                        <?php if($prod[0]['images']!="") { ?>
                        <li>
                        <a href="<?php echo PATH_IMG_PRODUCT.$prod[0]['images'] ?>" class="trueimages" > 
                          <img src="<?php echo PATH_IMG_PRODUCT.$prod[0]['images'] ?>" alt="<?php echo $prod[0]['title_vn'] ?>" style="max-width: 80px;">
                        </a>
                        </li>
                        <?php }else { echo ""; } ?>
                        <?php  if($prod[0]['images1']!="") { ?>
                        <li>
                        <a href="<?php echo PATH_IMG_PRODUCT.$prod[0]['images1'] ?>" class="trueimages" > 
                          <img src="<?php echo PATH_IMG_PRODUCT.$prod[0]['images1'] ?>" alt="<?php echo $prod[0]['title_vn'] ?>" style="max-width: 80px;">
                        </a>
                        </li>
                        <?php }else { echo ""; } ?>
                        <?php  if($prod[0]['images2']!="") { ?>
                        <li>
                        <a href="<?php echo PATH_IMG_PRODUCT.$prod[0]['images2'] ?>" class="trueimages" > 
                          <img src="<?php echo PATH_IMG_PRODUCT.$prod[0]['images2'] ?>" alt="<?php echo $prod[0]['title_vn'] ?>" style="max-width: 80px;">
                        </a>
                        </li>
                        <?php }else { echo ""; } ?>

                      </ul>
                    </div>
                  </div>

                  <!-- <div class="product-shop col-xs-12 col-sm-7">
                  <div class="product-main-info">
                      <div class="hidden-lg hidden-md hidden-sm">
                          <div class="product-img-box">
                            <p class="product-image">
                                  <a href="javascript: void(0)" title="<?php echo $prod[0]['title_vn'] ?>" class="abs show-on-big" id="main-image" onclick="showStaticPopup('large_img', 'popupContentImage');">
                                      <img id="image" class="lazy" data-src="<?php echo PATH_IMG_PRODUCT.$prod[0]['images'] ?>" alt="<?php echo $prod[0]['title_vn'] ?>" title="<?php echo $prod[0]['title_vn'] ?>" /> </a>
                              </p>                                                
                          </div>
                          <div class="product-share-icons">
                              <span>Share:</span>&nbsp;
                              <div class="addthis_toolbox addthis_default_style addthis_16x16_style">
                                  <a class="addthis_button_facebook"></a>
                                  <a class="addthis_button_twitter"></a>
                                  <a class="addthis_button_pinterest_share"></a>
                                  <a class="addthis_button_google_plusone" g:plusone:count="false"></a>
                                  <a class="addthis_button_compact"></a>
                                  <a class="addthis_counter addthis_bubble_style"></a>
                              </div>
                          </div>
                      </div>
                    </div>
                  </div> -->

                  <div class="col-sm-7 product-type-data">
                    <h1 itemprop="name" id="product_general" class="product-name">
                        <span class="brand-name"></span>
                        <strong class="display-name"><?php echo $prod[0]['title_vn'] ?></strong>
                    </h1>
                    <p class="product-ids"></p>

                    <div class="ratings" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/aggregateRating">
                      <meta itemprop="reviewCount" content="0">
                      <span class="review-total">0 Điểm</span>
                      <div class="rating-box">
                          <meta itemprop="ratingValue" content="4.45">
                          <!-- DEFINE SVGs -->
                          <svg height="0" width="0">
                              <defs>
                                  <!-- DEFINE ONE STAR -->
                                  <symbol viewBox="0 0 12 12" id="iconstar">
                                      <title>star</title>
                                      <g id="star-Page-1">
                                          <g id="star-Welcome-page-1920-" transform="translate(-548.000000, -484.000000)">
                                              <g id="star-Group-7" transform="translate(480.000000, 326.000000)">
                                                  <g id="star-Group-5">
                                                      <g id="star-Group-2">
                                                          <g id="star-Stars" transform="translate(12.000000, 158.000000)">
                                                              <polygon id="star-Star" points="61.5,9.1 58.1,11 58.8,7 56,4.2 59.8,3.6 61.5,0 63.2,3.6 67,4.2 64.3,7 64.9,11"></polygon>
                                                          </g>
                                                      </g>
                                                  </g>
                                              </g>
                                          </g>
                                      </g>
                                  </symbol>
                                  <!-- DEFINE PATTERN FOR EMPTY STARS -->
                                  <pattern id="stars-empty" patternUnits="userSpaceOnUse" width="15" height="15">
                                      <use x="0" y="0" xlink:href="#iconstar" fill="#818181" height="15" width="15"></use>
                                  </pattern>
                                  <!-- DEFINE PATTERN FOR FULL STARS -->
                                  <pattern id="stars-full" patternUnits="userSpaceOnUse" width="15" height="15">
                                      <use x="0" y="0" xlink:href="#iconstar" fill="#ffcb46" height="15" width="15"></use>
                                  </pattern>
                              </defs>
                          </svg>
                          <!-- INSERT STARS -->
                          <svg height="15" viewBox="0 0 74 15" width="74">
                              <rect fill="url(#stars-empty)" x="0" y="0" height="15" width="74"></rect>
                              <rect fill="url(#stars-full)" x="0" y="0" height="15" width="0%"></rect>
                          </svg>
                      </div>
                      <p class="rating-links">
                          <a href="lifestream-advanced-probiotic-lsap3-g.html#product-reviews-list" data-target="product-reviews-list" class="product-review-link">Read Review(s)</a>
                          <span class="separator"> | </span>
                          <a href="lifestream-advanced-probiotic-lsap3-g.html#review-form" data-target="review-form" class="product-review-link">Write a Review</a>
                      </p>
                    </div>

                    <div class="product-type-data-inner">
                    <div>

                      <ul class="grouped-items-list">
                          <li class="item productspricelist" itemprop="offers" itemscope="itemscope" itemtype="http://schema.org/Offer" style="height: unset !important;">
                              <div class="row">
                                  <div class="data-size col-xs-12 col-md-3">30 capsules</div>
                                  <div class="data-price col-xs-12 col-md-5">
                                      <div class="price-box">
                                          <p class="special-price">
                                              <span class="price" id="product-price-22">
                                                <span class="price-numbers">
                                                  <span class="price">
                                                    <?php echo $this->page->bsVndDot($prod[0]['sale_price']) ?> <sub>(đ)</sub>
                                                  </span>
                                                </span>
                                              </span>
                                              
                                          </p>
                                          <?php if (!empty($prod[0]['price'] && $prod[0]['price'] != $prod[0]['sale_price'])) {
                                            echo '<span class="old-price" style="color: #3a332c;">'.$this->page->bsVndDot($prod[0]['price']).'<sub>(đ)</sub></span>';
                                          } ?>
                                      </div>
                                  </div>
                                  <div class="data-qty col-xs-12 col-md-4">
                                      <link itemprop="availability" href="http://schema.org/InStock">
                                      <div class="qty-counter">
                                          <i title="<?php echo $prod[0]['title_vn']; ?>" class="icon-thin-arrow-right plus fa fa-plus "></i>
                                            <input type="number" pattern="[0-9\.]*" class="input-text qty" title="Qty: " value="1" maxlength="12" id="qty" name="quanty">
                                          <i title="Decrease Quantity" class="icon-thin-arrow-left minus fa fa-minus"></i>
                                      </div>
                                  </div>
                              </div>
                          </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
      </div>
      
      <div class="product-view-right col-sm-3">
        <div class="product-highlights col-lg-12">
            <div class="add-to-box">
                <p style="color: #0c0c54;font-size: 18px;font-weight: bold;margin-top: 30px;"><a href="tel:<?php echo $web['hotline'] ?>" data-track="true" data-category="Footer" data-action="Get in touch" data-label="Call <?php echo $web['hotline'] ?>"><?php echo $web['hotline'] ?></a></p>
                <p style="color: red;font-size: 15px;font-weight: bold;">Gọi đặt hàng nhanh | Ship COD</p>

                <button type="submit" title="Add to Cart" class="button btn-cart" id="ajaxcartview" style="width: unset;height: unset;font-size: 13px;width: unset;height: unset;padding: 10px 20px;background-color: #2fb88a;">Thêm Vào Giỏ Hàng
                </button>
                
                  <script type="text/javascript">
                      $$('.product-essential .icon-thin-arrow-left').each(function(element) {
                          $(element).observe('click', function() {
                              var quantityInputBox = this.previous();
                              if (parseInt(quantityInputBox.value) == 1) {
                                  quantityInputBox.value = 0;
                              } else if (parseInt(quantityInputBox.value) > 1) {
                                  quantityInputBox.value = parseInt(quantityInputBox.value) - 1;
                              }
                          });
                      });
                      $$('.product-essential .icon-thin-arrow-right').each(function(element) {
                          $(element).observe('click', function() {
                              var quantityInputBox = this.next();
                              if (parseInt(quantityInputBox.value) < 100) {
                                  quantityInputBox.value = parseInt(quantityInputBox.value) + 1;
                              }
                          });
                      });
                  </script>
              </div>
        </div>
        <hr style="border-color: #aaa;width: 100%;">
      </div>

      <input type="hidden" value="<?php echo $prod[0]['Id'] ?>" name="idpro" /> 
      <input type="hidden" value="<?php echo $prod[0]['sale_price'] ?>" name="shop_price" id="sale_price" />
      <input type="hidden" value="<?php echo current_url(); ?>" name="link" />
      <!-- <input type="hidden" name="alias" value="<?php echo $prod[0]['alias']; ?>" /> -->
      <input type="hidden" value="<?php echo $prod[0]['title_vn'] ?>" name="name_pro" />
      </form>
    </div>
              
      <!-- <div class="data-qty col-xs-12 col-md-4">
      <link itemprop="availability" href="http://schema.org/InStock" />
        <div class="qty-counter">
          <i title="" class="icon-thin-arrow-right plus fa fa-plus "></i>
          <input type="number" pattern="[0-9\.]*" name="quanty" data-price="<?php echo $prod[0]['sale_price'] ?>"  class="input-text qty" value="1" maxlength="12" id="qty">
          <i title="Decrease Quantity" class="icon-thin-arrow-left minus fa fa-minus"></i>
        </div>
      </div> -->
      

  <div class="row">
    <div class="col-sm-9">
      <div class="product-collateral">
        <div class="product-tab-area">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#nav1" data-toggle="tab"><p>Thông tin sản phẩm</p></a></li>
            <li><a href="#nav2" data-toggle="tab"><p>Thành phần chính</p></a></li>
            <li><a href="#nav3" data-toggle="tab"><p>Hướng dẫn sử dụng</p></a></li>
            <li><a href="#nav4" data-toggle="tab"><p>Thông số sản phẩm</p></a></li>
            <li><a href="#nav5" data-toggle="tab"><p>Đánh giá</p></a></li>
            <li><a href="#nav6" data-toggle="tab"><p>Hỏi đáp</p></a></li>
          </ul>
          <div class="product-tabs-container" style="color: black;border: .1px solid #aaa;">
              <div class="row" style="padding: 10px;">
                <div class="tab-content ">
                  <div class="tab-pane active" id="nav1">
                    <?php echo stripcslashes($prod[0]['content_vn']);?>
                  </div>
                  <div class="tab-pane" id="nav2">
                    <?php echo stripcslashes($prod[0]['description_vn']);?>
                  </div>
                  <div class="tab-pane" id="nav3">
                    <?php echo stripcslashes($prod[0]['tag']);?>
                  </div>
                  <div class="tab-pane" id="nav4">
                    <?php echo stripcslashes($prod[0]['digital']);?>
                  </div>
                  <div class="tab-pane" id="nav5">
                    
                  </div>
                  <div class="tab-pane" id="nav6">
                    
                  </div>
                  </div>
                </div>
              </div>
          </div>

        <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
        <script type="text/javascript">
            //<![CDATA[
            function doTabs() {
                var productTabs = new Varien.Tabs('.nav-tabs');
                //    $('product-review-link').observe('click', function (e) { productTabs.switchTab($('nav-tabs'), $('reviews')); });
            }
            // do not run this when in print mode
            if (!isPrintMode()) {
                doTabs();
            }
            //]]>
        </script>
      </div>
    </div>
    <div class="col-sm-3">
      <div class="box-title dk-blue amcatlist_title col-xs-12" style="padding: unset;border: unset;">
        <h2 style="width: 100%;padding: 10px">Sản phẩm cùng thương hiệu</h2>
        <div style="margin-top: 10px">
          <ul>
          <?php $i=0; ?>
           <?php if(!empty($prod_cl)){ 
              foreach($prod_cl as $item) { ?>
                <?php if($item['alias'] === $prod[0]['alias']){ ?>
                  <?php echo ""; ?>
                <?php }else{ ?>
                <li style="border: .1px solid #aaa;padding: 10px 2px;">
                  <img src="<?php echo PATH_IMG_PRODUCT.$item['images']."?v=".time();?>" style="float: left;max-width:80px;max-height: 80px;padding:0 10px 10px 10px;">
                  <div style="text-align: left;">
                  <?php
                    if ((int)$item['price'] != 0 && (int)$item['price'] != $item['sale_price']) {
                        ?>
                        <span class="price"style="color: #50352f;text-decoration: line-through;"><?php echo bsVndDot($item['price']); ?> <sub>(đ)</sub></span>
                  <?php } ?>
                  <br>
                  <?php if ((int)$item['sale_price'] != 0 ) {?>
                      <span class="price" id="" style="color: #ff5b35">
                          <?php echo bsVndDot($item['sale_price']); ?><sub>(đ)</sub>
                      </span>
                  <?php }else{ ?>
                      <span class="price" id="">
                          <?php echo bsVndDot($item['price']); ?><sub>(đ)</sub>
                      </span>
                  <?php } ?> 
                  <p style="font-size: 12px;color: black;"><?php echo $item['title_vn'] ?></p>
                  </div>
                </li> 
            <?php $i++; ?>
          <?php }}}else{ ?>
            <div class="alert alert-danger">Sản phẩm đang update.</div>
          <?php } ?>
          </ul>
        </div>
      </div>
    </div>
  </div>

          </div>
      </div>
  </div>
</div>


<div class="container" style="padding: 40px 0;">
<h2 class="title_related">Sản phẩm liên quan</h2>
       <?php $i=0; ?>
       <?php if(!empty($prod_cl)){ 
          foreach($prod_cl as $item) { ?>
            <?php if($item['alias'] === $prod[0]['alias']){ ?>
              <?php echo ""; ?>
            <?php }else{ ?>
            <?php if ($i == 4) break; ?>
            <div class="col-sm-3 col-xs-6 " style="padding-top: 5px;"> 
              <div class="home-item home-item--product-grid products-grid__item only-grid only-grid-24">
                  <div class="product-grid__top">
                      <div class="product-images">
                          <a href="<?php echo base_url($item['alias'].'.html'); ?>" title="<?php echo $item['title_vn'] ?>" class="product-image" data-track="tru" data-label="<?php echo $item['title_vn'] ?>">
                              <div class="product-promos">
                                  <?php $price = (int)$item['price'];$sale_price = $item['sale_price'];
                              if ($price != 0 && $price != $sale_price) {
                                  $pt = 100-floor(($sale_price/ $price)*100) ?>
                                  <i style="margin-left: 5px"> -<?php  echo $pt?>%</i>
                              <?php }else{ echo "";} ?>
                              </div>
                              <img data-src="<?php echo PATH_IMG_PRODUCT.$item['images']."?v=".time();?>" width="100%" alt="" class="lazy" src="<?php echo PATH_IMG_PRODUCT.$item['images']."?v=".time();?>" data-loaded="true" style="max-height: 144px">
                          </a>
                      </div>
                      <div class="product-info" style="height: auto;">
                          <div class="product-brand">
                              <a href=""></a>
                          </div>
                          <div class="product-name" style="min-height: 44px">
                              <a href="" title="" class="product-title" data-track="true" data-category="Homepage We Recommend" data-action="<?php echo $item['title_vn'] ?>" data-label="<?php echo $item['title_vn'] ?>"><?php echo $item['title_vn'] ?></a>
                          </div>
                         <div class="color-black"></div>
                      </div>
                  </div>

                  <div class="product-grid__middle" style="max-height: 80px;">
                      <div class="product-grid__middle__left">
                     <div class="price-box">
                          <p class="minimal-price special-price">
                              <?php
                                if ((int)$item['sale_price'] != 0) {?>
                                  <span class="price" id="product-minimal-price-5468-widget-new-grid">
                                      <?php echo bsVndDot($item['sale_price']); ?><sub>(đ)</sub>
                                  </span>
                              <?php }else{ ?>
                                  <span class="price" id="product-minimal-price-5468-widget-new-grid">
                                      <?php echo bsVndDot($item['price']); ?><sub>(đ)</sub>
                                  </span>
                              <?php } ?>                                                        
                          </p>
                          <p class="you-save">
                              <span class="price msrp linemiddle">
                                  <?php
                                    if ((int)$item['price'] != 0 && (int)$item['price'] != $item['sale_price']) {
                                        ?>
                                        <span class="price"><?php echo bsVndDot($item['price']); ?> <sub>(đ)</sub></span>
                                  <?php } ?>
                              </span>
                          </p>
                      </div>
                  </div>

                  <div class="product-grid__middle__right">
                      <div class="ratings" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                              
                      </div>
                  </div>
                  </div>
                  <div class="product-grid__bottom">
                      <div class="width-haft">
                          <button type="button" title="Add to Cart" class="button btn-cart ajaxcartpost" data-id="24">
                              <span style="font-size: 12px;">Mua Hàng</span></button>
                      </div>
                  </div>
              </div>
          </div>
        <?php $i++; ?>
    <?php }}}else{ ?>
        <div class="alert alert-danger">Sản phẩm đang update.</div>
    <?php } ?>
</div>
            <!-- ESI START DUMMY COMMENT [] -->
            <!-- ESI URL DUMMY COMMENT -->
     <script type="text/javascript" >(function($) {
     $('.trueimages').click(function(e){
            e.preventDefault();
            var fullimages = $(this).attr('href');
            $('.product-image-zoom').find('img').attr('src',fullimages);
    })
        
  })(jQuery);
    </script>


<style type="text/css">
  .title_related{
    width: 100%;
    float: left;
    padding: 8px 0;
    margin: 10px 0;
    background: #EEE;
    font-size: 16px;
    text-transform: uppercase;
    text-indent: 10px;
    text-align: left;
  }
  .row{
    margin-left: unset;
    margin-right: unset;
  }
  h2{
    padding-top: unset;
  }
  .product-tab-area .nav-tabs > li{
    margin: unset;
    margin-right: unset;
    margin-bottom: unset;
  }
  .product-tab-area .nav-tabs {
    border-bottom: .01px solid #4FA6BC;
  }
  .product-tab-area .nav-tabs > li p{
    color: #fff;
  }
  .product-tab-area .nav-tabs > li.active p {
    color: #4C4C4C;
  }
  .manu-span span{
    text-align: left;
  }
  .manu-span p{
    text-align: left;
  }
  #slider3 li {
    margin: unset;
  }
</style>