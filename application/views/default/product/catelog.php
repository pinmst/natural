<div class="main col2-left-layout container">
  <!-- ESI START DUMMY COMMENT [] -->
  <!-- ESI URL DUMMY COMMENT -->

  <div class="row">
    <div class="col-sm-2 col-sm-offset-0 col-xs-10 col-xs-offset-1" style="text-align: left;padding: unset;">
      <ol class="rounded-list">
        <?php foreach($menu as $item) { ?>
          <?php if ($item['parentid'] == 0 && $item['home'] == 1){ ?>
            <li><a href="<?php echo base_url($item['alias']); ?>" class="<?php if($item['Id'] == $data['cat'][0]['Id']){echo 'active-menu-dr';} ?>"><?php echo $item['title_vn'] ?></a>
            <?php foreach($menu as $item_chil) { ?>
              <?php if ($item_chil['parentid'] == $item['Id']){ ?>
                  <ol>
                    <li><a href="<?php echo base_url($item_chil['alias']); ?>" class="<?php if($item_chil['Id'] == $data['cat'][0]['Id']){echo 'active-menu-dr';} ?>"><?php echo $item_chil['title_vn']; ?></a></li>
                </ol>
              <?php } ?>
            <?php } ?>  
            </li>   
            <?php } ?>
        <?php } ?>                    
    </ol>

    </div>
    <div class="col-sm-10" style="padding: unset;">
      <div id="main" class="col-main col-sm-12">
      <h1 class="page-title category-title"><?php echo $data['cat'][0]['title_vn']; ?> <span style="font-size: 18px;">(<?php echo $totalItem; ?> sản phẩm)</span></h1>
      <div class="category-top">
      </div>

      <!-- ESI START DUMMY COMMENT [] -->
      <!-- ESI URL DUMMY COMMENT -->
      <!-- ESI END DUMMY COMMENT [] -->
      <div class="clearer"></div>
     
      <div id="block_layeredNav_blockContent" class="block block-layered-nav shown">
          <div class="block-content" style="padding-top: 15px;color: black">
            <dl id="narrow-by-list">
              <div class="item" style="height: auto !important;background: unset;text-shadow:unset;font-weight: bold;">
                <ul class="fillter-sort" style="margin: unset;">
                  <li style="display:inline-block" style="color: black !important;padding-right: 15px;">Sắp xếp</li>
                  <li style="display:inline-block">
                    <a href="<?php echo $linked."&sort=Id DESC" ?>" class="btn btn-default btn-sm <?php if($sort =="Id DESC") echo "active btn-primary"; ?>">Sản phẩm mới nhất</a>
                  </li>
                  <li style="display:inline-block">
                    <a href="<?php echo $linked."&sort=oder DESC" ?>" class="btn btn-default btn-sm <?php if($sort =="oder DESC") echo "active btn-primary"; ?>" >Sản phẩm bán chạy</a>
                  </li>
                  <li style="display:inline-block">
                    <a href="<?php echo $linked."&sort=sale_price DESC" ?>" class="btn btn-default btn-sm <?php if($sort =="sale_price DESC") echo "active btn-primary"; ?>">Giá giảm dần</a>
                  </li>
                  <li style="display:inline-block">
                    <a href="<?php echo $linked."&sort=sale_price ASC" ?>" class="btn btn-default btn-sm <?php if($sort =="sale_price ASC") echo "active btn-primary"; ?>" >Giá tăng dần</a>
                  </li>
                </ul>
                <dd class="filter-brand">
                  <div class="custom-select custom-filter fa-angle-down">
                      <!-- <select onchange="setLocation(this.value)">
                        <option value="<?php echo $linked ?>">Sắp xếp</option>
                        <option value="<?php echo $linked."&sort=Id DESC" ?>" <?php if($sort =="Id DESC") echo "selected"; ?>>Sản phẩm mới nhất</option>
                        <option value="<?php echo $linked."&sort=oder DESC" ?>" <?php if($sort =="oder DESC") echo "selected"; ?>>Sản phẩm bán chạy</option>
                        <option value="<?php echo $linked."&sort=sale_price DESC" ?>" <?php if($sort =="sale_price DESC") echo "selected"; ?>>Giá giảm dần</option>
                        <option value="<?php echo $linked."&sort=sale_price ASC" ?>" <?php if($sort =="sale_price ASC") echo "selected"; ?>>Giá tăng dần</option>
                      </select> -->
                    </div>
                  </dd>
                </div>
            </dl>
          </div>
      </div>

      <div class="category-products">
          <ul class="products-grid">
            <?php if(!empty($info)){ foreach ($info as $item){ ?>

             <div class="col-sm-3 col-xs-6 " style="padding-top: 5px;"> 
                <div class="home-item home-item--product-grid products-grid__item only-grid only-grid-24">
                    <div class="product-grid__top">
                        <div class="product-images">
                            <a href="<?php echo base_url($item['alias'].'.html'); ?>" title="<?php echo $item['title_vn'] ?>" class="product-image" data-track="tru" data-label="<?php echo $item['title_vn'] ?>">
                                <div class="product-promos" style="top: unset;right: 0">
                                    <?php 
                                      $price = (int)$item['price'];$sale_price = $item['sale_price'];
                                      if ($price != 0 && $price != $sale_price) { ?>
                                          <img src="<?php echo PATH_IMG_FLASH.'sale.png' ?>" width="40px">
                                    <?php }else{ echo "";} ?>
                                </div>
                                <img data-src="<?php echo PATH_IMG_PRODUCT.$item['images']."?v=".time();?>" width="100%" alt="" class="lazy" src="<?php echo PATH_IMG_PRODUCT.$item['images']."?v=".time();?>" data-loaded="true" style="max-height: 144px">
                            </a>
                        </div>
                        <div class="product-info" style="height: auto;">
                            <div class="product-brand">
                                <a href=""></a>
                            </div>
                            <div class="product-name" style="min-height: 44px">
                                <a href="" title="" class="product-title" data-track="true" data-category="Homepage We Recommend" data-action="<?php echo $item['title_vn'] ?>" data-label="<?php echo $item['title_vn'] ?>"><?php echo $item['title_vn'] ?></a>
                            </div>
                           <div class="color-black"></div>
                        </div>
                    </div>

                    <div class="product-grid__middle" style="max-height: 80px;">
                        <div class="product-grid__middle__left">
                       <div class="price-box">
                            <p class="minimal-price special-price">
                                <?php
                                  if ((int)$item['sale_price'] != 0) {?>
                                    <span class="price" id="product-minimal-price-5468-widget-new-grid">
                                        <?php echo bsVndDot($item['sale_price']); ?><sub>(đ)</sub>
                                    </span>
                                <?php }else{ ?>
                                    <span class="price" id="product-minimal-price-5468-widget-new-grid">
                                        <?php echo bsVndDot($item['price']); ?><sub>(đ)</sub>
                                    </span>
                                <?php } ?>                                                        
                            </p>
                            <p class="you-save">
                                <span class="price msrp linemiddle">
                                    <?php
                                      if ((int)$item['price'] != 0 && (int)$item['price'] != $item['sale_price']) {
                                          ?>
                                          <span class="price"><?php echo bsVndDot($item['price']); ?> <sub>(đ)</sub></span>
                                    <?php } ?>
                                </span>
                            </p>
                        </div>
                    </div>

                    <div class="product-grid__middle__right">
                        <div class="ratings" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                                
                        </div>
                    </div>
                    </div>

                    <div class="product-grid__bottom">
                        <div class="width-haft">
                            <form action="<?php echo base_url('addcart_fromcat'); ?>" method="post" enctype='multipart/form-data'/>
                                  <input type="hidden" name="quanty" value="1" />
                                  <input type="hidden" value="<?php echo $item['Id'] ?>" name="idpro" /> 
                                  <input type="hidden" value="<?php echo $item['sale_price'] ?>" name="shop_price" id="sale_price" />
                                  <input type="hidden" value="<?php echo current_url(); ?>" name="link" />
                                  <input type="hidden" value="<?php echo $item['title_vn'] ?>" name="name_pro" />
                                  <button type="submit" title="Add to Cart" class="button btn-cart " style="height: unset;float: unset;">
                                      <span style="font-size: 12px;">Mua Hàng</span>
                                  </button>
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
          <?php } }else{ ?>
            <div class="alert alert-danger">Sản phẩm đang update.</div>
          <?php } ?>
          </ul>
          <div class="pagination" style=""><?php echo $this->pagination->create_links();?></div>
          <?php if(!empty($page)) echo '
            <nav class="woocommerce-pagination">
              <div class="pagination" style="">'.$page.' </div>
            </nav>
          '; ?>
      </div>
      
             
          <div class="toolbar-bottom">
          <div class="toolbar"><div class="sorter-pager"><div class="col-lg-12"> <div class="col-md-12 col-md-offset-1 page-count-label"><link rel="stylesheet" href="http://qbnatural.com/libs/paging/tsc_pagination.css" type="text/css" media="screen" charset="utf-8">  <div class="main-pagination"><ul class="tsc_pagination tsc_paginationA tsc_paginationA05"></ul></div></div></div></div></div></div>

      <script type="text/javascript">
          jQuery(function() {
              jQuery('ul.products-grid').find('p.msrp').each(function() {
                  jQuery(this).appendTo(jQuery(this).closest('div.product-grid__middle__left').siblings('div.product-grid__middle__right'));
              });
              var productPriceEle = jQuery('.products-grid__item').find('.product-grid__middle');
              var maxPriceHeight = Math.max.apply(null, productPriceEle.map(function() {
                  return jQuery(this).outerHeight(true);
              }).get());
              productPriceEle.css('height', maxPriceHeight + 'px');
              jQuery(window).resize(function() {
                  var maxPriceHeight = Math.max.apply(null, productPriceEle.map(function() {
                      return jQuery(this).outerHeight(true);
                  }).get());
                  productPriceEle.css('height', maxPriceHeight + 'px');
              });
          });
      </script>

  </div>
  <div class="col-left">
      <img src="skin/frontend/healthpost/default/images/icon-email.svg" alt="">
  </div>
</div>
</div>
</div>

<style type="text/css">
.category-products{
  padding: 10px 0;
}
.pagination{
    margin: auto;
    box-sizing: border-box;
    height: 40px;
    padding: 8px 10px;
    display: block;
    padding-left: 0;
    margin: 20px 0;
    border-radius: 4px;
  }

  .pagination a{
    display: inline-block;
    padding: 6px 10px;
    background-color: #eee;
    text-align: center;
    box-sizing: border-box;
    font-size: 12px;
    color: #686868;
    margin: 0 2px;
    text-decoration: none!important;
  }
  .pagination a:hover{
    color: #fff;
    background-color: #e74847;
  }
  .pagination strong {
    display: inline-block;
    padding: 6px 10px;
    text-align: center;
    box-sizing: border-box;
    font-size: 12px;
    margin: 0 2px;
    text-decoration: none;
    color: #fff;
    background-color: #e74847;
}
ol {
    counter-reset: li; /* Initiate a counter */
    list-style: none; /* Remove default numbering */
    *list-style: decimal; /* Keep using default numbering for IE6/7 */
    font: 15px 'trebuchet MS', 'lucida sans';
    padding: 0;
    margin-bottom: 4em;
    text-shadow: 0 1px 0 rgba(255,255,255,.5);
}
ol ol {
    margin: 0 0 0 2em; /* Add some left margin for inner lists */
}
.rounded-list a{
    position: relative;
    display: block;
    padding: .4em .4em .4em 2em;
    *padding: .4em;
    margin: .5em 0;
    background: #ddd;
    color: #444;
    text-decoration: none;
    border-radius: .3em;
    transition: all .3s ease-out;   
}
.rounded-list a:hover{
    background: #eee;
}
.rounded-list a:hover:before{
    transform: rotate(360deg);  
}
.rounded-list a:before{
    content: counter(li);
    counter-increment: li;
    position: absolute; 
    left: -1.3em;
    top: 50%;
    margin-top: -1.3em;
    background: #87ceeb;
    height: 2em;
    width: 2em;
    line-height: 2em;
    border: .3em solid #fff;
    text-align: center;
    font-weight: bold;
    border-radius: 2em;
    transition: all .3s ease-out;
}
.active-menu-dr{
  background: #bb8484 !important;
    color: aliceblue !important;
}
.btn-default{
  text-shadow: unset;
}
.btn-primary.active {
    background-color: #2fb88a;
    border-color: #2fb88a;
}
</style>