<?php
    $cart = $this->session->userdata('cart');
?>

<div id="wrapper" class="wrapper">
        <div class="page">
            <div class="header-container floating_menu clearfix">
                <div class="header clearfix">
                    <div class="header-left"></div>
                    <div class="row clearfix header-row">
                        <div class="branding" itemscope="" itemtype="http://schema.org/Organization">
                            <a itemprop="url" href="" title="<?php echo $web['title_vn'] ?>" class="logo">
                                <strong>HealthPost NZ</strong>
                                <img style="margin: 20px 5px 0 0;" itemprop="logo" src="<?php echo PATH_IMG_FLASH.$logo['file_vn']."?v=".time();?>" alt="<?php echo $web['title_vn'] ?>" class="logo-image">
                                <img itemprop="logo" src="<?php echo PATH_IMG_FLASH.$logo['file_vn']."?v=".time();?>" alt="HealthPost NZ" class="sticky-logo-image">
                                <img itemprop="logo" src="<?php echo PATH_IMG_FLASH.$logo['file_vn']."?v=".time();?>" alt="HealthPost NZ" class="mobile-logo-image">
                            </a>
                        </div>
                        <div class="form-search-container">
                            <div class="form-search-container">
                                <form name="searchform" action="<?php echo base_url().'search' ?>" method="get" id="SliSearchProductFormasdasd">
                                    <div id="header_search" class="form-search">
                                        <label for="search">Search:</label>
                                        <input type="text" name="s" id="input_searchword" class="input-text" size="" placeholder="Tìm kiếm.... " _flx1_12_1="1" data-provide="rac">
                                        <button type="submit" title="Search" class="button"><i class="fa fa-search" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                   
                                </form>
                            </div>
                        </div>
                        <div class="quick-access">

                            <div class="top-cart">
                                <?php
                                    if(empty($cart)){
                                    ?>
                                    <div class="" >
                                        <div class="top-cart-left">
                                            <div class="top-cart-image">
                                                <img src="themes/skin/frontend/healthpost/default/images/icon-cart.svg" alt="">
                                            </div>
                                            <div class="block-title">
                                                <strong id="cartHeader"><span><?php echo count($cart); ?></span></strong>
                                            </div>
                                        </div>

                                        <div class="top-cart-right">
                                            <div class="block-price">
                                                <span class="price"><?php echo bsVndDot($tongtien) ?> <sub>(đ)</sub></span></div>
                                        </div>
                                    </div>
                                <?php }else{ ?>
                            <div class="">
                                <div class="top-cart-left">
                                    <div class="top-cart-image">
                                        <img src="themes/skin/frontend/healthpost/default/images/icon-cart.svg" alt="">
                                    </div>
                                    <div class="block-title">
                                        <strong id="cartHeader"><span><?php echo count($cart); ?></span></strong>
                                    </div>
                                </div>

                                <div class="top-cart-right">
                                    <div class="block-price">
                                        <span class="price">
                                            <?php
                                                $tongtien = 0;
                                                foreach($cart as $k=>$v){
                                                $idpro = (int)$v['idpro'];
                                                if($v['deal']==1){
                                                    $pro = $this->deal_model->get_Id($idpro);
                                                }else{
                                                    $pro = $this->product_model->get_Id($idpro);
                                                }
                                                $tongtien = $tongtien+ $pro[0]['sale_price']*$v['qty'];
                                            ?>
                                            <?php } ?> <?php echo bsVndDot($tongtien) ?><sub>(đ)</sub>
                                        </span></div>
                                </div>
                            </div>
                        <?php } ?>
            
                            <div id="topCartContent" class="block-content" style="overflow: visible; display: none;">
                                <div class="inner-wrapper" style="">
                                    <p class="block-subtitle">
                                    <span onclick="Enterprise.TopCart.hideCart()" class="close-btn">Đóng</span>Mới Thêm vào</p>
                                    <ol id="mini-cart" class="mini-products-list">
                                        <?php
                                            $tongtien = 0;
                                            foreach($cart as $k=>$v){
                                            $idpro = (int)$v['idpro'];
                                            if($v['deal']==1){
                                                $pro = $this->deal_model->get_Id($idpro);
                                                $link = base_url("deal/".$pro[0]['alias']."-".$pro[0]['Id']."");
                                            }else{
                                                $pro = $this->product_model->get_Id($idpro);
                                                $link = base_url($pro[0]['alias'].".html");
                                            }
                                            $tongtien = $tongtien+ $pro[0]['sale_price']*$v['qty'];
                                        ?>
                                        <li class="item last even parent-21-0 odd" style="height: unset !important;text-align: left;">
                                            <a href="<?php echo $link; ?>" title="<?php echo $pro[0]['title_vn'] ?>" class="product-image">
                                                <img src="<?php echo PATH_IMG_PRODUCT.$pro[0]['images']; ?>" alt="<?php echo $pro[0]['title_vn'] ?>" alt="<?php echo $pro[0]['title_vn'] ?>" width="50" height="50">
                                            </a>
                                            <div class="product-details">
                                                <a href="<?php echo base_url('payment/delcart/'.$k); ?>" data="<?php echo $k ?>" title="Xóa" class="btn-remove">Xóa</a>
                                                <p class="product-name">
                                                    <a href="<?php echo $link; ?>"><?php echo $pro[0]['title_vn'] ?></a>
                                                </p>
                                                <strong><?php echo $v['qty'] ?></strong> x
                                                <span class="price"><?php echo bsVndDot($pro[0]['sale_price']) ?> <sub>(đ)</sub></span>
                                            </div>
                                        </li>
                                        <?php } ?>    
                                    </ol>
                                    <script type="text/javascript">decorateList('mini-cart', 'none-recursive')</script>
                                    <p class="subtotal">
                                    <span class="label">Tạm Tính:</span> <span class="price"><?php echo bsVndDot($tongtien) ?></span></p>
                                    <div class="actions">
                                        <button class="button" type="button" onclick="setLocation('<?php echo base_url('gio-hang#checkout') ?>')">Thanh Toán</button>
                                        <a class="link-view-cart" href="<?php echo base_url('gio-hang') ?>">Giỏ hàng</a>
                                    </div>
                                </div>
                            </div>

                            <script type="text/javascript">
                                    Enterprise.TopCart.initialize('topCartContent');
                                    // Below can be used to show minicart after item added
                                    // Enterprise.TopCart.showCart(7);
                                </script>
                            </div>

                            <div class="customer-point">
                                <div class="customer-point-left">
                                    <div class="customer-point-image">
                                        <img src="themes/skin/frontend/healthpost/default/images/icon-point.svg" alt="">
                                    </div>
                                </div>
                                <div class="customer-point-right">
                                 HOTLINE : <a href="tel:<?php echo $web['hotline'] ?>" title="Call"><?php echo $web['hotline'] ?></a>
                                </div>
                                 <div class="customer-point-right" style="margin-left:20px">
                                    <!-- <a href="http://qbnatural.com/auckland-store/login">| Đăng Nhập</a> -->
                                </div>
                            </div>
                        
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-12 top-menu-wrapper">
                            <a class="sr-only" href="#main"></a>
                            <div class="nav-container container">
                                <div id="custommenu" class="">
                                    
                                    <div id="menu57" class="menu" onmouseover="wpShowMenuPopup(this, event, 'popup57');" onmouseout="wpHideMenuPopup(this, event, 'popup57', 'menu57')">
                                        <div class="parentMenu">
                                            <a data-track="true" data-category="Menu" data-action="Trang Chủ" data-label="Trang Chủ" href="" rel="">
                                                <span>Trang Chủ</span>
                                            </a>
                                        </div>
                                    </div> 

                                    <?php foreach($menu as $item) { ?>
                                        <?php if ($item['parentid'] == 0 && $item['home'] == 1){ ?>

                                        <div id="menu<?php echo $item['Id'] ?>" class="menu" onmouseover="wpShowMenuPopup(this, event, 'popup<?php echo $item['Id'] ?>');" onmouseout="wpHideMenuPopup(this, event, 'popup<?php echo $item['Id'] ?>', 'menu<?php echo $item['Id'] ?>')">
                                            <div class="parentMenu">
                                                <a data-track="true" data-category="Menu" data-action="<?php echo $item['title_vn'] ?>" data-label="<?php echo $item['title_vn'] ?>" href="<?php echo base_url($item['alias']); ?>" rel="<?php echo base_url($item['alias']); ?>">
                                                    <span><?php echo $item['title_vn'] ?></span>
                                                </a>
                                            </div>
                                        </div>
                                        <div id="popup<?php echo $item['Id'] ?>" class="wp-custom-menu-popup" onmouseout="wpHideMenuPopup(this, event, 'popup<?php echo $item['Id'] ?>', 'menu<?php echo $item['Id'] ?>')" onmouseover="wpPopupOver(this, event, 'popup<?php echo $item['Id'] ?>', 'menu<?php echo $item['Id'] ?>')" style="top: 58px; left: 0px; z-index: 10000; display: none;padding: unset;">
                                            <div class="block1">
                                                <div class="column first odd">    
                                                <?php $mnChil=0; ?>
                                                <?php foreach($menu as $item_chil) { ?>
                                                  <?php if ($item_chil['parentid'] == $item['Id']){ ?>
                                                    <?php if ($mnChil == 6 ) break; ?>
                                                        <div class="itemMenu level1">
                                                            <div class="level1Name clearer">
                                                                <a class="itemMenuName left level1" href="<?php echo base_url($item_chil['alias']); ?>">
                                                                    <span><?php echo $item_chil['title_vn'] ?></span>
                                                                </a>
                                                            </div>
                                                            <?php $mnChilSe=0; ?>
                                                            <?php foreach($menu as $item_chil_second) { ?>
                                                              <?php if ($item_chil_second['parentid'] == $item_chil['Id']){ ?>
                                                                <?php if ($mnChilSe == 3 ) break; ?>
                                                                <div class="itemSubMenu clearer level1">
                                                                    <div class="itemMenu level2">
                                                                    <a class="itemMenuName level2" href="<?php echo base_url($item_chil['alias']); ?>"><span><?php echo $item_chil_second['title_vn']?></span></a>            
                                                                    </div>
                                                                </div>    
                                                                <?php $mnChilSe++; ?>
                                                              <?php } ?>
                                                            <?php } ?>   
                                                        </div>    
                                                    <?php $mnChil++; ?>
                                                  <?php } ?>
                                                <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    <?php } ?>

                                    <div class="clearBoth"></div>
                                </div>

                                <div id="custommenu-mobile" class="">
                                    <div id="menu-button" onclick="wpMenuButtonToggle(); $('wrapper').toggleClassName('megamenu'); $('header_help').removeClassName('shown'); return false;">
                                        <a href="javascript:void(0);">
                                            <span class="visible-fa-bars">
                                                <i class="fa fa-bars"></i>
                                            </span>
                                            <span class="text"><i class="fa fa-bars"></i>Shop by Category</span>
                                        </a>
                                    </div>
                                    <div id="menu-content" style="display:none;">
                                        <div class="menu-top">
                                            <div id="toggle_menu_hide_megamenu" class="toggle-menu-hide-megamenu toggle menu-top__left">
                                                <span class="fa-stack fa-lg">
                                                        <i class="fa fa-times fa-stack-1x fa-inverse"></i>
                                                </span>
                                            </div>
                                            <div class="menu-top__right">
                                            </div>
                                        </div>
                                         <div id="menu-mobile-57" class="menu-mobile level0">
                                            <div class="parentMenu">
                                                <a href="supplements-and-natural-health">
                                                    <span>Trang Chủ</span>
                                                </a>
                                                <span class="button" rel="submenu-mobile-57" onclick="wpSubMenuToggle(this, 'menu-mobile-57', 'submenu-mobile-57');">&nbsp;</span>
                                            </div>
                                            
                                        </div> 
                                        <div id="menu-mobile-174" class="menu-mobile level0">
                                            <div class="parentMenu">
                                                <a href="supplements-and-natural-health">
                                                    <span>Beauty</span>
                                                </a>
                                                <span class="button" rel="submenu-mobile-174" onclick="wpSubMenuToggle(this, 'menu-mobile-174', 'submenu-mobile-174');">&nbsp;</span>
                                            </div>
                                            <div id="submenu-mobile-174" rel="level1" class="wp-custom-menu-submenu level1" style="display: none;">
                                                    

                                            
                                        <div id="menu-mobile-179" class="itemMenu level1">
                                            <div class="parentMenu"><a class="itemMenuName level1" href="http://qbnatural.com/auckland-store/hello"><span>Hello</span></a><span class="button" rel="submenu-mobile-179" onclick="wpSubMenuToggle(this, 'menu-mobile-179', 'submenu-mobile-179');">&nbsp;</span></div>
                                            <div id="submenu-mobile-179" rel="level1" class="wp-custom-menu-submenu level1" style="display: none;">
                                            
                                               
                                                 <div id="menu-mobile-100" class="itemMenu level2">
                                                    <div id="menu-mobile-180" class="itemMenu level2">
                                                    <div class="parentMenu"><a class="itemMenuName level2" href="http://qbnatural.com/auckland-store/danh-muc-con"><span>Danh mục con</span></a></div>
                                                </div>
                                                
                                                <div class="clearBoth"></div>



                                            <div class="parentMenu"><a class="itemMenuName level2" href="http://qbnatural.com/auckland-store/danh-muc-con"><span>Danh mục con</span></a></div>
                                                   
                                                </div>
                                                <div class="clearBoth"></div>
                                            </div>
                                        </div>
                                        <div class="clearBoth"></div>
                                            </div>
                                </div> 
                                


                                <div class="menu-account">
                                            <div class="menu-account__top">
                                                <span class="menu-account__top__left">
                                             <span>My Account</span>
                                                </span>
                                                <span class="menu-account__top__right">
                                                <a href="customer/account/login/">Sign In</a>
                                            </span>
                                            </div>
                                            <div class="menu-account__bottom">
                                                <div class="menu-account__bottom__container">
                                                    <div>Don't have an account? <a href="customer/account/create/">Register</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearBoth"></div>
                                    </div>
                                    <div id="menu-background" style="display: none"></div>
                                </div>

                            </div>
                            <script type="text/javascript">
                                //<![CDATA[
                                var CUSTOMMENU_POPUP_WIDTH = 0;
                                var CUSTOMMENU_POPUP_TOP_OFFSET = 0;
                                var CUSTOMMENU_POPUP_DELAY_BEFORE_DISPLAYING = 1000;
                                var CUSTOMMENU_POPUP_DELAY_BEFORE_HIDING = 150;
                                var CUSTOMMENU_RTL_MODE = 0;
                                var wpCustommenuTimerShow = {};
                                var wpCustommenuTimerHide = {};
                                var wpActiveMenu = null;
                                //wpCustomMenuMobileToggle();
                                //]]>
                            </script>
                        </div>
                    </div>
                    <div class="header-right"></div>
                </div>
            </div>

                 <!-- ESI END DUMMY COMMENT [] -->
           
<style type="text/css">
h2{
    margin: 0;     
    color: #666;
    padding-top: 90px;
    font-size: 52px;
    font-family: "trebuchet ms", sans-serif;    
}
.item{
    background: white;    
    text-align: center;
    height: 300px !important;
}
.carousel{
    margin-top: 20px;
}
.bs-example{
    margin: 20px;
}
.carousel.carousel-fade .item {
  opacity:0;
  filter: alpha(opacity=0); /* ie fix */
}
.carousel.carousel-fade .active.item {
    opacity:1;
    filter: alpha(opacity=100); /* ie fix */
}
</style>
