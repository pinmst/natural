<?php
	$cart = $this->session->userdata('cart');
?>

<div class="main col1-layout container" style="text-align: left;">
    <div id="main" class="col-main row">

     <?php
        if(empty($cart)){
        ?>
        <div class="cont-news-detail-page">
            <div class="post_page">
                <div class="woocommerce">
                    <p class="cart-empty">
                    Chưa có sản phẩm nào trong giỏ hàng.</p>
                    <p class="return-to-shop">
                    <a class="button wc-backward button alt" href="<?php echo base_url(); ?>">Quay lại cửa hàng</a>
                    </p>
                </div>
            </div>
        </div>
    <?php }else{ ?>
        

    <div class="cart">
        <ul class="checkout-types right">
            <li></li>
        </ul>
    <br class="clear">    
    <div class="col-md-12">
        <h3 class="" style="color: #2f2323;">Hóa Đơn Thanh Toán</h3>
        <div id="block_layeredNav_blockContent" class="block block-layered-nav shown">
          <div class="block-content" style="padding-top: 5px;color: black;text-align: center;font-style: italic;font-size: 16px;">
            Miễn phí vận chuyển cho đơn hàng trên 500.000 <sub>(đ)</sub>
          </div>
      </div>
        <input name="form_key" value="wuKU6XJ1NuUkbiTY" type="hidden">
        <fieldset>
            <table id="shopping-cart-table" class="data-table cart-table">
                <thead>
                    <tr class="first last">  
                        <th rowspan="1" class="product-image">&nbsp;</th>
                        <th class="product-name" rowspan="1"><span class="nobr">Sản Phẩm</span></th>
                        <th class="a-left" colspan="1"><span class="nobr">Đơn giá</span></th>
                        <th rowspan="1" class="a-center td-qty"><span class="nobr">Số Lượng</span></th>
                        <th class="a-right" colspan="1" style="text-align: left !important;">Thành tiền</th>
                        <th rowspan="1" class="a-center remove">Xóa</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr class="first last">
                        <td colspan="50" class="a-right last">
                            <div class="cart-rewards"></div>
                            <button type="button" title="Continue Shopping" class="button btn-continue" onclick="setLocation('')"><i class="fa fa-chevron-circle-left"></i> Continue Shopping</button>
                            <div class="visible-xs">
                                <button type="submit" name="update_cart_action" value="update_qty" title="Update Cart" class="button btn-update"><span><span>Update Cart</span></span></button>
                            </div>
                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                        $tongtien = 0;
                        foreach($cart as $k=>$v){
                        $idpro = (int)$v['idpro'];
                        if($v['deal']==1){
                            $pro = $this->deal_model->get_Id($idpro);
                            $link = base_url("deal/".$pro[0]['alias']."-".$pro[0]['Id']."");
                        }else{
                            $pro = $this->product_model->get_Id($idpro);
                            $link = base_url($pro[0]['alias'].".html");
                        }
                        $tongtien = $tongtien+ $pro[0]['sale_price']*$v['qty'];
                    ?>
                    <tr class="last even parent-23-2">
                        <td class="product-image">
                                <img src="<?php echo PATH_IMG_PRODUCT.$pro[0]['images']; ?>" alt="<?php echo $pro[0]['title_vn'] ?>" width="75" height="75">
                        </td>
                        <td>
                            <h2 class="product-name" style="padding: unset;">
                                <a href="<?php echo $link; ?>"><?php echo $pro[0]['title_vn'] ?></a>
                            </h2>
                        </td>
                        <td>
                            <span class="cart-price"><?php echo bsVndDot($pro[0]['sale_price']) ?> <sub>(đ)</sub></span>
                        </td>
                        <td class="a-center">
                             <div class="qty-counter qty-counter--cart">
                                <button type="submit" name="update_cart_action" value="update_qty" title="Update" class="button btn-update btn-update-cart hidden-xs" >Update</button>
                                <i title="Increase Quantity" class="icon-thin-arrow-right plus fa fa-plus "></i>
                                <input type="number" step="1" min="1" max="" name="quanty[<?php echo $k; ?>]" value="<?php echo $v['qty'] ?>" title="SL" class="input-text qty text" size="4" pattern="[0-9]*" inputmode="numeric"  id=""/>
                                <i title="Decrease Quantity" class="icon-thin-arrow-left minus fa fa-minus"></i>
                            </div>
                        </td>
                        <td>
                            <span class="cart-price sub-cart-price-23-2">
                             <?php echo bsVndDot($pro[0]['sale_price']*$v['qty']) ?> <sub>(đ)</sub>
                            </span>
                        </td>
                        <td class="a-center remove last">
                            <a href="<?php echo base_url('payment/delcart/'.$k); ?>" title="Xóa" onclick="" data='<?php echo $k; ?>'>
                                <i class="fa fa-trash-o"></i>
                            </a>
                            <!-- <script type="text/javascript">
                                function caldell(id){
                                    // alert('123');
                                    // var id = document.getElementById("id-del").value;
                                    console.log(id);
                                    $.ajax({
                                        url: "<?php echo base_url('payment/delcartajax'); ?>",
                                        type: "post",
                                        dataType : "json",
                                        data: {id: id},
                                        success: function(response) {
                                            var data = JSON.parse(response);
                                            console.log(data);
                                        },
                                        error: function(jqXHR, textStatus, errorThrown) {
                                            console.log(textStatus, errorThrown)
                                        }
                                        
                                    })
                                    
                                    }
                            </script> -->
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
             </table>
        <script type="text/javascript">decorateTable('shopping-cart-table')</script>
        </fieldset>
    </div>
    <br style="clear: both;">
    </div>
    </div>
    <div class="clearfix"></div>
    <div class="cart-tools row">
        <div class="totals col-lg-6 col-md-6 col-lg-push-8 col-md-push-8"></div>
    </div>
</div>

<div class="cart-collaterals">
</div>
</div>
</div>
    <div class="main col1-layout container" id="checkout"  style="text-align: left;padding-bottom: 40px;">
        <div id="main" class="col-main row">
            <div id="aw-onestepcheckout-container">
                <div class="aw-onestepcheckout-row">
                    <div id="aw-onestepcheckout-title">
                        <h3 style="color: black">Thanh Toán</h3>
                        <p id="aw-onestepcheckout-title-description">Điền đầy đủ thông tin để hoàn thành đơn hàng</p>
                    </div>
                </div>
                <div class="aw-onestepcheckout-row auth">
                </div>
                <div class="aw-onestepcheckout-row" id="aw-onestepcheckout-general-container">
                        <div class="address-col aw-onestepchekocut-column aw-onestepchekocut-column-left col-lg-4 col-md-4 col-sm-12 col-xs-12 ajaxautoheight">
                            <!-- <form id="aw-onestepcheckout-general-form" method="post" action="http://qbnatural.com/vn/login" enctype="multipart/form-data"><div id="aw-onestepcheckout-authentification">
                                <p class="aw-onestepcheckout-number ">
                                    <i>1</i>Đăng Nhập Để theo dõi đơn hàng tốt hơn </p>
                                <div id="aw-onestepcheckout-authentification-container">
                                    <div id="aw-onestepcheckout-login-form" action="http://qbnatural.com/vn/login">
                                        <div class="form-list col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">
                                            <div id="aw-onestepcheckout-forgot-password-success">
                                                <p>Nếu bạn muốn theo dõi đơn hàng tốt hơn thì đăng nhập. Hoặc đơn giản đặt hàng nhanh chóng </p>
                                            </div>
                                            <div class="sing-in-title">
                                                <h2>Đăng Nhập</h2>
                                            </div>
                                            <div class="sign-in-form no-padding">
                                                <div class="aw-onestepcheckout-auth-form-block no-padding col-xs-12">
                                                    <label for="login-email" class="required">Email:</label>
                                                    <div class="input-box">
                                                        <input type="text" class="input-text login-email " id="login-email" name="username"  value="" data-validetta="required" />
                                                    </div>
                                                </div>
                                                <div class="aw-onestepcheckout-auth-form-block no-padding col-xs-12">
                                                    <label for="login-password" class="required">Password:</label>
                                                    <div class="input-box">
                                                        <input type="password" class="input-text login-password "  id="login-password" name="password" data-validetta="required" />
                                                    </div>
                                                </div>
                                                <div class="aw-onestepcheckout-auth-form-block no-padding">
                                                    <button class="button" id="aw-onestepcheckout-login-submit-btn">
                                                        <span><span>Đăng nhập</span></span>
                                                    </button>
                                                </div>
                                                <div class="aw-onestepcheckout-auth-form-block no-padding">
                                                    <a id="aw-onestepcheckout-login-forgot-link" href="http://qbnatural.com/vn/forgotpassword">Quên mật khẩu</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form> -->
                        <form id="checkformsubmit" method="post" action="<?php echo base_url('thanh-toan') ?>" enctype="multipart/form-data">
                            <div id="aw-onestepcheckout-address-wrapper">
                                <div id="aw-onestepcheckout-address" class="aw-onestepcheckout-add-loader-into-this-block">
                                    <div id="aw-onestepcheckout-address-billing-wrapper">
                                        <p class="aw-onestepcheckout-number ">Địa chỉ Giao hàng</p>
                                        <div id="aw-onestepcheckout-address-billing">
                                            <div class="form-list">
                                                <div id="aw-onestepcheckout-billing-address-list">
                                                    <div class="customer-name">
                                                        <div class="customer-name">
                                                            <div class="field name-firstname">
                                                                <div class="input-box">
                                                                    <input type="text" id="fullname" name="fullname" value="" title="Họ và Tên" maxlength="255" class="input-text " data-validetta="required" placeholder="Tên *" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="clear:both"></div>
                                                    </div>
                                                      <div class="aw-onestepcheckout-general-form-field aw-onestepcheckout-general-form-field-left">
                                                        <div class="input-box">
                                                            <input type="text" name="phone" title="Số Phone" class="input-text" id="phone" data-validetta="required,number" value="" placeholder="Số Điện Thoại *" />
                                                        </div>
                                                        <div style="clear:both;"></div>
                                                    </div>
                                                    <div class="aw-onestepcheckout-general-form-field aw-onestepcheckout-general-form-field-wide">
                                                    </div>
                                                    <div class="aw-onestepcheckout-general-form-field aw-onestepcheckout-general-form-field-wide">
                                                        <div class="input-box">
                                                            <input type="email" name="email" id="email" title="Email" class="input-text validate-email "  value="" data-validetta="required" placeholder="Email (nếu có)" />
                                                        </div>
                                                        <div style="clear:both;"></div>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="clear:both;"></div>
                                                    <div class="street-wrapper" id="street-wrapper">
                                                        <div class="aw-onestepcheckout-general-form-field aw-onestepcheckout-general-form-field-wide">
                                                            <label for="billing:street1" class="required">Địa chỉ <em>*</em></label>
                                                            <div class="input-box">
                                                                <input type="text" title="Địa chỉ" name="address" id="address" data-validetta="required" class="input-text  " value="" />
                                                            </div>
                                                            <div style="clear:both;"></div>
                                                        </div>
                                                    </div>
                                                     <div class="aw-onestepcheckout-general-form-field aw-onestepcheckout-general-form-field-left">
                                                        <label for="billing:postcode" class="required">Phường <em>*</em></label>
                                                        <div class="input-box">
                                                            <input type="text" title="Phường" name="ward" id="ward" class="input-text validate-zip-international  " data-validetta="required" value="" />
                                                        </div>
                                                        <div style="clear:both;"></div>
                                                    </div>
                                                     <div class="aw-onestepcheckout-general-form-field aw-onestepcheckout-general-form-field-wide">
                                                        <label for="billing:country_id" class="required">Tỉnh/Thành Phố<em>*</em></label>
                                                        <div class="input-box">
                                                            <select name="city" id="city" class="validate-select validation-passed shipping-update" data-validetta="required" title="Thành Phố">
                                                                <option value=""> </option>
                                                                <?php foreach($provinces as $item){ ?>
                                                                    <option value="<?php echo $item['Id'] ?>" data-fee=""><?php echo $item['title_vn']; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                        <div style="clear:both;"></div>
                                                    </div>                                                   
                                                    <div style="clear:both"></div>
                                                    <div class="aw-onestepcheckout-general-form-field aw-onestepcheckout-general-form-field-wide">
                                                    </div>
                                                    <div style="clear:both;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="aw-onestepchekocut-column aw-onestepchekocut-column-middle col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                            <div class="aw-onestepchekocut-column-middle--wrap ajaxautoheight">
                                <p class="aw-onestepcheckout-number aw-onestepcheckout-number-3">Chọn hình thức thanh toán</p>
                                <div id="aw-onestepcheckout-order-review-enterprise-storecredit-wrapper">
                                    <div id="aw-onestepcheckout-review-enterprise-storecredit">
                                    </div>
                                </div>
                                <div id="aw-onestepcheckout-order-review-enterprise-points-wrapper"></div>
                                <div id="aw-onestepcheckout-order-review-points-wrapper"></div>
                                <div id="aw-onestepcheckout-payment-method-wrapper">
                                    <div id="aw-onestepcheckout-payment-method" class="aw-onestepcheckout-add-loader-into-this-block">
                                        <dl class="sp-methods" id="checkout-payment-method-load">
                                           <dt>
                                                <input id="p_method_foomandpsprofusions" value="2" type="radio" name="fpaymentmethod" title="Thanh Toán Qua COD " checked="checked" class="radio" />
                                                <label for="p_method_foomandpsprofusions">Thanh Toán Qua COD </label>
                                            </dt> 
                                        </dl>
                                    </div>
                                </div>
                                 <div id="aw-onestepcheckout-payment-method-wrapper">
                                    <div id="aw-onestepcheckout-payment-method" class="aw-onestepcheckout-add-loader-into-this-block">
                                        <dl class="sp-methods" id="checkout-payment-method-load">
                                            <dt>
                                                <input id="p_method_foomandpsprofusion" value="1" type="radio" name="fpaymentmethod" title="Chuyển Khoản: Banking / ATM "  class="radio" />
                                                <label for="p_method_foomandpsprofusion">Chuyển Khoản: Banking / ATM </label>
                                            </dt>
                                            <p style="margin-left: 26px;font-size: 12px;" >Ưu tiên "Chuyển Khoản" ....</p>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                        </div>
            <div class="aw-onestepchekocut-column aw-onestepchekocut-column-right col-lg-4 col-md-4 col-sm-12 col-xs-12 no-padding ajaxautoheight">
                <div id="aw-onestepcheckout-order-review">
                    <p class="aw-onestepcheckout-number aw-onestepcheckout-number-v"><i class="circle-icon"><span class="fa fa-check"></span></i>Thông Tin Đơn hàng</p>
                    <div id="aw-onestepcheckout-order-review-cart-wrapper">
                        <!-- BEGIN REWARDS INTEGRATION -->
                        <!-- END REWARDS INTEGRATION -->
                        <div id="aw-onestepcheckout-review-cart">
                            <div id="aw-onestepcheckout-review-table-cart-wrapper">
                                <table class="aw-onestepcheckout-cart-table">
                                    <col />
                                    <col width="1" />
                                    <col width="1" />
                                    <thead>
                                        <tr>
                                            <th class="name">
                                                <span class="price totalpriceall" style="float: left;font-size: 14px;">Tổng đơn hàng </span>
                                                <span class="price totalpriceall" style="float: right;font-size: 14px;"><?php echo bsVndDot($tongtien) ?> <sub>(đ)</sub></span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr style="display:none" class="use-voucher" >
                                            <td class="a-right cart-points-total_earned" colspan="2">
                                                <div class="caption healthpost-rewards-checkout-total_earned">Tạm Tính :</div>
                                            </td>
                                            <td style="" class="a-right cart-points-total_earned">
                                                <span class="price points-amount totalprice-all">1.900.000 <sub>(đ)</sub></span>
                                            </td>
                                        </tr>
                                        <tr style="display:none" class="shipping-fee-total" >
                                            <td class="a-right cart-points-total_earned" colspan="2">
                                                <div class="caption healthpost-rewards-checkout-total_earned">Phí Ship :</div>
                                            </td>
                                            <td style="" class="a-right cart-points-total_earned">
                                                <span class="price points-amount shipping-fee-amout">0 <sub>(đ)</sub></span>
                                            </td>
                                        </tr>
                                        <tr style="display:none" class="use-voucher" >
                                            <td class="a-right cart-points-total_earned" colspan="2">
                                                <div class="caption healthpost-rewards-checkout-total_earned">Bạn Dùng Voucher :</div>
                                            </td>
                                            <td style="" class="a-right cart-points-total_earned">
                                                <span class="price points-amount"></span>
                                            </td>
                                        </tr>

                                        <tr style="display:none" class="use-voucher"  >
                                            <td class="a-right cart-points-total_spent" colspan="2">
                                                <div class="caption healthpost-rewards-checkout-total_spent ">Bạn được giảm giá :</div>
                                            </td>
                                            <td class="a-right cart-points-total_spent">
                                                <div class="price points-amount voucherdown">
                                              -     0 <sub>(đ)</sub> </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <!-- -->
                    </div>
                        <div id="aw-onestepcheckout-order-review-coupon-wrapper">
                            <div id="aw-onestepcheckout-review-coupon">
                            <div class="coupon-message-container" style="overflow:hidden;height:0px;"></div>
                            <div class="coupon-code-form">
                                <label for="coupon_code">Voucher</label>
                                <div class="input-box field-row">
                                    <input class="input-text with-buttons" id="coupon_code" name="coupon_code" value="">
                                    <button type="submit" class="button buttonapplyvoucher" id="aw-onestepcheckout-coupon-code-apply-btn " >
                                        <span><span>Apply Code</span></span>
                                    </button>
                                    <button type="submit" class="button" id="aw-onestepcheckout-coupon-code-cancel-btn" onclick="return false;" style="display:none;">
                                        <span><span>Cancel Promo Code</span></span>
                                    </button>
                                </div>
                            </div>
                            <br style="clear: both;">
                            <div>
                                <span class="a-right" style="color: #2948b7;"><strong>Tổng Thanh Toán :</strong></span>
                                <strong style="font-size: 20px;float: right;"><span class="price totalpriceall"><?php echo bsVndDot($tongtien) ?> <sub>(đ)</sub></span></strong>
                            </div>
                            </div>
                        </div>
                            <div id="aw-onestepcheckout-order-review-referafriend-wrapper">
                            </div>
                                <div id="aw-onestepcheckout-order-review-comments-wrapper">
                                    <div id="aw-onestepcheckout-review-comments">
                                        <div id="aw-onestepcheckout-review-comments-simple">
                                            <div class="field-row">
                                                <label for="comments">Ghi Chú : </label>
                                                <div class="input-box">
                                                    <textarea name="note" id="note" rows="2" maxlength="100" placeholder="VD : Ship Trong Giờ hành chính...."  ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   
                                </div>
                                <div id="aw-onestepcheckout-order-review-terms-wrapper">
                                </div>
                                <div id="aw-onestepcheckout-order-review-extra-wrapper">
                                </div>
                                <div id="aw-onestepcheckout-place-order">
                                    <button type="submit" title="Hoàn Tất" id="aw-onestepcheckout-place-order-button">
                                        <span class="aw-onestepcheckout-place-order-grand-total">
                                 
                                        </span>
                                        <span class="aw-onestepcheckout-place-order-title" style="font-size: 22px">Hoàn Tất Thanh Toán</span>
                                    </button>
                                </div>
                                </div>
                            </div>
                        </form>
                    </div>
                <div class="aw-onestepcheckout-row">
            <div id="aw-onestepcheckout-related-wrapper"></div>
        </div>
    </div>
     <?php } ?>
    </div>
</div>


<style type="text/css">
    #aw-onestepcheckout-order-review {
        padding: 15px 0;
    }
</style>