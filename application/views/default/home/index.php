<div class="after-header-container">
   <div class="bs-example">
        <div id="myCarousel" class="carousel slide" data-interval="6500" data-ride="carousel">
            <!-- Carousel indicators -->
            <ol class="carousel-indicators">
                <?php $sl=0; ?>
                <?php foreach ($slide_home as $item){ ?>
                      <?php if ($sl==0) { ?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $sl ?>" class="active"></li>
                      <?php }else{ ?>
                         <li data-target="#myCarousel" data-slide-to="<?php echo $sl ?>"></li>
                      <?php } ?>
                      <?php $sl++; ?>
                <?php } ?>
            </ol>   
           <!-- Carousel items -->
            <div class="carousel-inner">
                <?php $sl=0; ?>
                <?php foreach ($slide_home as $item){ ?>
                    <?php if ($sl==0) { ?>
                      <div class="active item carousel-fade">
                        <a href="<?php echo base_url($item['link']); ?>">
                          <img src="<?php echo PATH_IMG_BANNER.$item['images']."?v=".time();?>" alt="<?php echo $item['images'] ?>" width="100%" style="max-height: 370px">
                        </a>
                      </div>
                    <?php }else{ ?>
                        <div class="item carousel-fade">
                          <a href="<?php echo base_url($item['link']); ?>">
                            <img src="<?php echo PATH_IMG_BANNER.$item['images']."?v=".time();?>" alt="<?php echo $item['images'] ?>" width="100%" style="max-height: 370px">
                          </a>
                        </div>
                    <?php } ?>
                    <?php $sl++; ?>
                <?php } ?>
            </div>
            <!-- Carousel nav -->
            <a class="carousel-control left" href="#myCarousel" data-slide="prev" style="width: 0%;background: none;display: none;">
                <span class="glyphicon glyphicon-chevron-left" style="color: rebeccapurple;"></span>
            </a>
            <a class="carousel-control right" href="#myCarousel" data-slide="next" style="width: 0%;background: none;display: none;">
                <span class="glyphicon glyphicon-chevron-right" style="color: rebeccapurple;"></span>
            </a
        </div>
    </div>
</div>

<form action="" method="post" class="top-form-element nl-desktop" id="newsletter-validate-detail">
    <div class="form-subscribe" style="">
        <div class="form-subscribe-header">
            <label for="femail-header">Subscribe for exclusive deals &amp; <strong class="ex-label">get $5 off your next order!</strong></label>
            <p class="bst-form-toggle-inline"><a class="bst-label-toggle" title="Close" href="javascript:void(0);">Close</a></p>
        </div>
        <div class="mobile-hidden">
            <div class="v-fix">
                <input name="email" id="femail-header" value="" placeholder="Enter your email address" onclick="this.value=='Enter your email address'?this.value='':''" onblur="this.value==''?this.value='Enter your email address':''" class="input-text required-entry validate-email" type="text">
            </div>
            <button type="button" id="btn-addEmail" class="button" title="Subscribe">
                <span><span>Subscribe</span></span>
            </button>
        </div>

    </div>
</form>
</div>
<div class="main col1-layout container">
    <div id="main" class="col-main row">
        <div class="std">
            <div></div>
        </div>
        <div class="home-block home-block--deals">
            <div class="home-title">
                <h1>Our top natural health, supplement & wellness deals</h1>
            </div>
            <!-- <div class="owl-carousel home-content col-sm-12">
                <?php foreach ($info_new as $item){ ?>
                    <div>
                        <img src="<?php echo PATH_IMG_BANNER.$item['images']."?v=".time();?>" alt="">
                    </div>
                 <?php } ?>
            </div> -->

            <div class="home-content owl-carousel owl-theme owl-loaded">

                <?php $sl=0; ?>
                <?php foreach ($info_new as $item){ ?>
                    <?php if ($sl==0) { ?>
                        <div class="owl-item active col-sm-12" style="">
                            <div class="home-item">
                                <a href="<?php echo $item['link']?>" data-track="true" data-category="<?php echo $item['title_vn']?>" data-action="#" data-label="<?php echo $item['title_vn']?>">
                                    <div class="home-item__image">
                                        <img class="lazy" data-src="<?php echo PATH_IMG_BANNER.$item['images']."?v=".time();?>" alt="" src="<?php echo PATH_IMG_BANNER.$item['images']."?v=".time();?>" data-loaded="true"style="width: 100%; height: auto;">
                                    </div>
                                    <div class="home-item__content">
                                        <div class="home-item__content__title">
                                            <h3><?php echo $item['title_vn']?></h3>
                                        </div>
                                        <div class="home-item__content__subtitle">
                                        </div>
                                        <div class="home-item__content__button">
                                        Xem Ngay
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php }else{ ?>
                        <div class="owl-item" style="">
                            <div class="home-item">
                                <a href="<?php echo $item['link']?>" data-track="true" data-category="<?php echo $item['title_vn']?>" data-action="#" data-label="<?php echo $item['title_vn']?>">
                                    <div class="home-item__image">
                                        <img class="lazy" data-src="<?php echo PATH_IMG_BANNER.$item['images']."?v=".time();?>" alt="" src="<?php echo PATH_IMG_BANNER.$item['images']."?v=".time();?>" data-loaded="true">
                                    </div>
                                    <div class="home-item__content">
                                        <div class="home-item__content__title">
                                            <h3><?php echo $item['title_vn']?></h3>
                                        </div>
                                        <div class="home-item__content__subtitle">
                                        </div>
                                        <div class="home-item__content__button">
                                        Xem Ngay
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                    <?php $sl++; ?>
                <?php } ?>
            </div>
        </div>
        
<script type="text/javascript">
jQuery('.home-block--deals .home-content').owlCarousel({
    stagePadding: 50,
    items: 1,
    loop: true,
    margin: 10,
    dots: false,
    nav: true,
    navText: ['<i class="icon icon-prev"></i>', '<i class="icon icon-next"></i>'],
    responsive: {
        768: {
            stagePadding: 0,
            items: 3,
            slideBy: 3
        }
    }
});
</script>
        

        <div class="home-block home-block--popular-categories">
            <div class="home-title">
                <h2>Danh mục</h2>
            </div>
            <div class="container">
                <?php foreach ($menu_chil as $item){ ?>
                 <div class="col-sm-2 col-xs-4" style="padding-top: 5px"> 
                    <div class="home-item home-item--product-grid products-grid__item category-grid">
                        <div class="product-grid__top">
                            <div class="product-images"  style="min-height: 140px">
                                <a href="<?php echo base_url($item['alias']); ?>" title="<?php echo $item['title_vn'] ?>" class="product-image">
                                    <?php if ($item['images'] !== NULL){ ?>
                                        <img data-src="<?php echo 'data/Catelog/'.$item['images']."?v=".time();?>" alt="<?php echo $item['title_vn'] ?>" class="lazy" width="100%" src="<?php echo 'data/Catelog/'.$item['images']."?v=".time();?>" data-loaded="true">
                                    <?php }else{ ?>
                                        <img data-src="<?php echo 'data/Catelog/logo.png'."?v=".time();?>" alt="<?php echo $item['title_vn'] ?>" class="lazy" width="100%" src="<?php echo 'data/Catelog/logo.png'."?v=".time();?>" data-loaded="true">
                                    <?php } ?>
                                </a>
                            </div>
                            <div class="product-info">
                                <div class="product-brand"></div>
                                <div class="product-name" style="font-size: 12px;min-height: 35px">
                                    <a href="<?php echo base_url($item['alias']); ?>" title="<?php echo $item['title_vn'] ?>" class="product-title"><?php echo $item['title_vn'] ?></a>
                                </div>
                            </div>
                            <div class="product-grid__bottom">
                                <div class="widthfull">
                                    <a href="<?php echo base_url($item['alias']); ?>" > <button type="button" title="Xem" class="button btn-cart ">
                                       Xem </button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>  
            </div>
        </div>

         <div class="home-products-row">
      
                <div class="home-block home-block--product-grid home-block--recommend">
                    <div class="home-title">
                        <h2>Sản phẩm mới</h2>
                    </div>
                    <div class="container">
                        
                    <?php $i=0; ?>
                        <?php foreach ($productNew as $item){ ?>
                          <?php if ($i == 4) break; ?>
                          <div class="col-sm-3 col-xs-6 " style="padding-top: 5px;"> 
                            <div class="home-item home-item--product-grid products-grid__item only-grid only-grid-24">
                                <div class="product-grid__top">
                                    <div class="product-images">
                                        <a href="<?php echo base_url($item['alias'].'.html'); ?>" title="<?php echo $item['title_vn'] ?>" class="product-image" data-track="tru" data-label="<?php echo $item['title_vn'] ?>">
                                            <div class="product-promos" style="top: unset;right: 0">
                                                <?php 
                                                  $price = (int)$item['price'];$sale_price = $item['sale_price'];
                                                  if ($price != 0 && $price != $sale_price) { ?>
                                                      <img src="<?php echo PATH_IMG_FLASH.'sale.png' ?>" width="40px">
                                                <?php }else{ echo "";} ?>
                                            </div>
                                            <img data-src="<?php echo PATH_IMG_PRODUCT.$item['images']."?v=".time();?>" width="100%" alt="" class="lazy" src="<?php echo PATH_IMG_PRODUCT.$item['images']."?v=".time();?>" data-loaded="true" style="max-height: 144px">
                                        </a>
                                    </div>
                                    <div class="product-info" style="height: auto;">
                                        <div class="product-brand">
                                            <a href=""></a>
                                        </div>
                                        <div class="product-name" style="min-height: 44px">
                                            <a href="" title="" class="product-title" data-track="true" data-category="Homepage We Recommend" data-action="<?php echo $item['title_vn'] ?>" data-label="<?php echo $item['title_vn'] ?>"><?php echo $item['title_vn'] ?></a>
                                        </div>
                                       <div class="color-black"></div>
                                    </div>
                                </div>

                                <div class="product-grid__middle" style="max-height: 80px;">
                                    <div class="product-grid__middle__left">
                                   <div class="price-box">
                                        <p class="minimal-price special-price">
                                            <?php
                                              if ((int)$item['sale_price'] != 0) {?>
                                                <span class="price" id="product-minimal-price-5468-widget-new-grid">
                                                    <?php echo bsVndDot($item['sale_price']); ?><sub>(đ)</sub>
                                                </span>
                                            <?php }else{ ?>
                                                <span class="price" id="product-minimal-price-5468-widget-new-grid">
                                                    <?php echo bsVndDot($item['price']); ?><sub>(đ)</sub>
                                                </span>
                                            <?php } ?>                                                        
                                        </p>
                                        <p class="you-save">
                                            <span class="price msrp linemiddle">
                                                <?php
                                                  if ((int)$item['price'] != 0 && (int)$item['price'] != $item['sale_price']) {
                                                      ?>
                                                      <span class="price"><?php echo bsVndDot($item['price']); ?> <sub>(đ)</sub></span>
                                                <?php } ?>
                                            </span>
                                        </p>
                                    </div>
                                </div>

                                <div class="product-grid__middle__right">
                                    <div class="ratings" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                                            
                                    </div>
                                </div>
                                </div>
                                <div class="product-grid__bottom">
                                    <div class="width-haft">
                                        <form action="<?php echo base_url('addcart_fromcat'); ?>" method="post" enctype='multipart/form-data'/>
                                              <input type="hidden" name="quanty" value="1" />
                                              <input type="hidden" value="<?php echo $item['Id'] ?>" name="idpro" /> 
                                              <input type="hidden" value="<?php echo $item['sale_price'] ?>" name="shop_price" id="sale_price" />
                                              <input type="hidden" value="<?php echo current_url(); ?>" name="link" />
                                              <input type="hidden" value="<?php echo $item['title_vn'] ?>" name="name_pro" />
                                              <button type="submit" title="Add to Cart" class="button btn-cart " style="height: unset;float: unset;">
                                                  <span style="font-size: 12px;">Mua Hàng</span>
                                              </button>
                                        </form>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                      <?php $i++; ?>
                  <?php } ?>  
                </div>
            </div>
        </div>

    
        <script type="text/javascript">
            $('.cartpost-call').click(function(e) {
                //$("#ajaxcartpro-add-confirm").removeClass("ajaxcartpro-box-hide");
        </script>

        <div class="home-block home-block--popular-categories">
            <div class="home-title">
                <h2>Thương hiệu</h2>
            </div>
            <div class="container">
                <?php $i=0; ?>
                <?php foreach ($manufacturer as $item){ ?>
                    <?php if($i==6) break; ?>
                    <div class="col-lg-5th-1 "> 
                        <div class="home-item home-item--product-grid products-grid__item category-grid">
                            <div class="product-grid__top">
                                <div class="product-images">
                                    <a href="<?php echo base_url('thuong-hieu/'.$item['alias']); ?>" title="<?php echo $item['title_vn']; ?>" class="product-image">
                                        <img data-src="<?php echo PATH_IMG_MANUFACTURER.$item['images']; ?>" alt="<?php echo $item['title_vn']; ?>" class="lazy" width="100%" src="<?php echo PATH_IMG_MANUFACTURER.$item['images']; ?>" data-loaded="true">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $i++; ?>
                <?php } ?>
            </div>
        </div>
     
        <div class="" style="background: #f3f3f3;margin: 0 calc(50% - 50vw);">
            <div class="container">
                 <div class="home-block home-block--blog">
                    <div class="home-title">
                        <a target="_blank" href="<?php echo base_url("tin-tuc-su-kien/bai-viet"); ?>" title="Tin Tức &amp; Sự Kiện" data-track="true" data-category="" data-action="Tin Tức &amp; Sự Kiện" data-label="Tin Tức &amp; Sự Kiện">
                            <h2>Tin Tức &amp; Sự Kiện</h2>
                        </a>
                    </div>

                    <div class="">
                      <ul style="list-style-type: none;">
                        <?php $i=0; ?>
                        <?php foreach($data['baiviet'] as $item) { ?>
                            <?php if($i==6) break; ?>
                                <a href="<?php echo base_url('tin-tuc/'.$item['alias'].'.html'); ?>">
                                    <li style="padding: 10px;overflow: auto;" class="col-sm-4 col-xs-12">
                                        <img src="<?php echo PATH_IMG_NEWS.$item['images']."?v=".time();?>" style="float: left;margin: 0 15px 0 0;max-width: 120px;">
                                        <h3 style="font-size: 13px;margin-top: 0;color: black"><?php echo $item['title_vn'] ?></h3>
                                        <p style="float: right;font-size: 12px;color: #aaa"><?php echo date("d-m-Y",$item['date']) ?></p>
                                    </li>
                                </a>
                            <?php $i++; ?>
                        <?php } ?>
                        <?php $i=0; ?>
                      </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

 <style type="text/css">
    li:hover {
      background: #eee;
      cursor: pointer;
    }
    .home-content .owl-controls{
            display: none;
    }
</style>
<script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2//2.0.0-beta.2.4/owl.carousel.min.js'></script>
<script type="text/javascript">
    jQuery('#btn-addEmail').click(function(e) {
        e.preventDefault();
        var btn = document.getElementById('btn-addEmail');
        var femail = document.getElementById('femail-header').value;
        
        if(femail != '') {
            $.ajax({
                url: 'ajax/addEmail',
                type: 'post',
                data: {femail: femail},
                success: function(res) {
                    console.log(res);
                    document.getElementById('femail-header').value = '';
                    $(this).find("button[id='btn-addEmail']").prop('disabled',true);
                    // btn.attr('disabled','disabled');
                }
            })
        }
        return false;
    });
</script>