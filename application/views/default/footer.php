<?php
$data['hotro'] = $pageSupport = $this->pagehtml_model->get_newsidcat(8,10,0);
$data['thongtin'] = $pageInfo = $this->pagehtml_model->get_newsidcat(5,10,0);
?>

<link rel="stylesheet" href="themes/libs/validetta/validetta.min.css" type="text/css" media="screen" charset="utf-8"> 
        <script type="text/javascript" src="themes/libs/validetta/validetta.min.js"></script>
        <script type="text/javascript" >(function($) {
        

    $("#aw-onestepcheckout-general-form").validetta({
             showErrorMessages : true, // If you dont want to display error messages set this option false
  /** You can display errors as inline or bubble */
  display : 'bubble', // bubble or inline
  /**
   * Error template class
   * This is the class which will be added to the error message window template.
   * If you want special style, you can change class name as you like with this option.
   * Error message window template : <span class="errorTemplateClass">Error messages will be here !</span>
   */
  errorTemplateClass : 'validetta-bubble',
  /** Class that would be added on every failing validation field */
  errorClass : 'validetta-error',
  /** Same for valid validation */
  validClass : 'validetta-valid', // Same for valid validation
  bubblePosition: 'bottom', // Bubble position // right / bottom

        });
        $("#checkformsubmit").validetta({
             showErrorMessages : true, // If you dont want to display error messages set this option false
  /** You can display errors as inline or bubble */
  display : 'bubble', // bubble or inline
  /**
   * Error template class
   * This is the class which will be added to the error message window template.
   * If you want special style, you can change class name as you like with this option.
   * Error message window template : <span class="errorTemplateClass">Error messages will be here !</span>
   */
  errorTemplateClass : 'validetta-bubble',
  /** Class that would be added on every failing validation field */
  errorClass : 'validetta-error',
  /** Same for valid validation */
  validClass : 'validetta-valid', // Same for valid validation
  bubblePosition: 'bottom', // Bubble position // right / bottom
        
        });
    
    })(jQuery);
        </script>
<div class="footer-container">
        <div class="row footer-newsletter-block">
            <form action="" method="post" class="top-form-element footer-newsletter" id="footer-newsletter-validate-detail">
                <div class="form-subscribe">
                    <div class="form-subscribe-header">
                        <label for="femail">Subscribe for exclusive deals and inspirational content. Get 50 Reward Points ($5 saving) when you do!*</label>
                    </div>
                    <div class="mobile-hidden">
                        <div class="v-fix">
                            <input name="email" type="email" id="femail" value="" placeholder="Your email address here" onclick="this.value==''?this.value='':''" onblur="this.value==''?this.value='':''" class="input-text required-entry validate-email" required="" />
                        </div>
                        <button type="button" id="tuan-btn" class="button" title="Subscribe" onclick="submitFooterNewsletterForm()">
                            <span><span>Subscribe Now</span></span>
                        </button>
                        <span class="small-print">*First time subscribers only</span>
                    </div>
                </div>
            </form>
            <script type="text/javascript">
                //<![CDATA[
                jQuery(function() {
                    var stateNl = Mage.Cookies.get("StateNl") || "shown";
                    if (stateNl == "hidden") {
                        jQuery('.row.footer-newsletter-block').hide();
                        jQuery('.top-form-element.footer-newsletter .form-subscribe-header label').html("Don't miss out on exclusive deals and inspirational content.");
                        jQuery('.top-form-element.footer-newsletter .small-print').remove();
                        jQuery('form.footer-newsletter').append('<input type="hidden" name="v2" value="1"/>');
                        jQuery('.footer-links-customer-service ul').append('<li><a href="#footer-newsletter-validate-detail" class="footer-newsletter-v2-link">Subscribe to Newsletter</a></li>');
                        jQuery('a.footer-newsletter-v2-link').click(function(e) {
                            e.preventDefault();
                            jQuery('.row.footer-newsletter-block').slideDown();
                        });
                    }
                });

                var newsletterSubscriberFormDetail = new VarienForm('footer-newsletter-validate-detail');

                function submitFooterNewsletterForm() {
                    var expCookies = new Date(new Date().getTime() + 45000000 * 10000);
                    if (newsletterSubscriberFormDetail.validator.validate()) {
                        var custEmail = document.getElementById('footer-newsletter').value;
                        Mage.Cookies.set('subscribed_as', custEmail, expCookies);
                        Mage.Cookies.set('StateNl', 'hidden', expCookies);
                        if (typeof(ga) == 'function') {
                            var trackerName = ga.getAll()[0].get('name');
                            ga(trackerName + '.send', 'event', 'Email Signup', 'Email Signup Bottom Bar', 'Email Signup Bottom Bar');
                        }
                        newsletterSubscriberFormDetail.form.submit();
                    }
                }

                //]]>
            </script>
        </div>
        
        <div class="row footer-links-block" style="margin: 0 calc(50% - 50vw);">
            <div class="footer container">
                <div class="footer-links group">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="footer-links-customer-service col-xs-6 col-sm-3">
                                <h4>Get in touch</h4>
                                <ul>
                                    <li>
                                        <div class="col-right">
                                            <div class="title">
                                                <?php foreach($info_footer as $item) { ?>
                                                    <?php if ($item->title_vn == "Thông tin footer" ) { ?>
                                                         <?php echo $item->content_vn ?>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="col-left">
                                            <img src="themes/skin/frontend/healthpost/default/images/icon-email.svg" alt="">
                                        </div>
                                        <div class="col-right">
                                            <div class="title">
                                                <a href="contacts" data-track="true" data-category="Footer" data-action="Get in touch" data-label="Contact us">Contact us
                                                </a>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="col-left">
                                            <img src="themes/skin/frontend/healthpost/default/images/icon-phone.svg" alt="">
                                        </div>
                                        <div class="col-right">
                                            <div class="title">
                                                <a href="tel:<?php echo $web['hotline'] ?>" data-track="true" data-category="Footer" data-action="Get in touch" data-label="Call <?php echo $web['hotline'] ?>"><?php echo $web['hotline'] ?></a>

                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="col-left">
                                            <img src="themes/skin/frontend/healthpost/default/images/icon-store.png" alt="">
                                        </div>
                                        <div class="col-right">
                                            <div class="title">
                                                <a href="<?php echo base_url() ?>" data-track="true" data-category="Footer" data-action="Get in touch" data-label="<?php echo $web['title_vn'] ?>"><?php echo $web['title_vn'] ?></a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="social-icons">
                                    <ul>
                                        <li>
                                            <a target="_blank" href="#" data-track="true" data-category="Footer" data-action="Social Media" data-label="Facebook">
                                                <img src="themes/skin/frontend/healthpost/default/images/icon-facebook.svg" alt="healthpost facebook">
                                            </a>
                                        </li>
                                        <li>
                                            <a target="_blank" href="#" data-track="true" data-category="Footer" data-action="Social Media" data-label="Twitter">
                                                <img src="themes/skin/frontend/healthpost/default/images/icon-tweet.svg" alt="healthpost twitter">
                                            </a>
                                        </li>
                                        <li>
                                            <a target="_blank" href="#" data-track="true" data-category="Footer" data-action="Social Media" data-label="YouTube">
                                                <img src="themes/skin/frontend/healthpost/default/images/icon-youtube.svg" alt="healthpost youtube">
                                            </a>
                                        </li>
                                        <li>
                                            <a target="_blank" href="#" data-track="true" data-category="Footer" data-action="Social Media" data-label="Instagram">
                                                <img src="themes/skin/frontend/healthpost/default/images/icon-instagram.svg" alt="healthpost instagram">
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                            <div class="footer-links-company-info col-xs-6 col-sm-3">
                                <h4>About Us</h4>
                                <ul>
                                <?php foreach($pageInfo as $item) { ?>
                                    <li>
                                        <a title="<?php echo $item['title_vn']; ?>" href="<?php echo base_url('page/'.$item['alias'].'.html'); ?>" data-track="true" data-category="Footer" data-action="<?php echo $item['title_vn']; ?>" data-label="<?php echo $item['title_vn']; ?>"><?php echo $item['title_vn']; ?></a>
                                    </li>
                                <?php } ?>

                                </ul>
                            </div>
                            <div class="footer-links-resources col-xs-6 col-sm-3">
                                <h4>Resources</h4>
                                <ul>
                                <?php foreach($pageSupport as $item) { ?>
                                    <li>
                                        <a title="<?php echo $item['title_vn']; ?>" href="<?php echo base_url('page/'.$item['alias'].'.html'); ?>" data-track="true" data-category="Footer" data-action="<?php echo $item['title_vn']; ?>" data-label="<?php echo $item['title_vn']; ?>"><?php echo $item['title_vn']; ?></a>
                                    </li>
                                <?php } ?>
                                </ul>
                            </div>
                            <div class="footer-links-contact col-sm-3">
                                <h4>Facebook fanpage</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row footer-bottom-block">
            <div class="footer container">
                <div class="footer-logo-sets">
                    <span><img src="themes/media/wysiwyg/payment/visa.png" alt="Visa"></span>
                    <span><img src="themes/media/wysiwyg/payment/mastercard.png" alt="Master Card"></span>
                    <span><img src="themes/media/wysiwyg/payment/paypal.png" alt="PayPal"></span>
                    <span><img src="themes/media/wysiwyg/payment/paymentexpress.png" alt="Payment Express"></span>
                    <span><img src="themes/media/wysiwyg/payment/alipay.png" alt="Alipay"></span>
                    <span><img src="themes/media/wysiwyg/payment/wechatpay.png" alt="Wechat Pay"></span>
                    <span><img src="themes/media/wysiwyg/payment/norton.png" alt="Norton"></span> </div>
                <div class="footer-copyright">
                    © <?php echo $web['title_vn'] ?>
                    <script>
                        document.write(new Date().getFullYear())
                    </script> | All rights reserved </div>

            </div>
        </div>
    </div>
     

        <script type="text/javascript">
            $$('.mobile-footer-nav-container li.level0 > a.parent-link').each(function(a) {
                a.observe('click', function(e) {
                    e.preventDefault();
                    var span = e.findElement('a.parent-link');
                    if (span) {
                        var li = e.findElement('li');
                        li.toggleClassName('open');
                        li.toggleClassName('closed');
                        e.stop();
                    }
                });
            });
        </script>

        <div id="acp-overlay" class="ajaxcartpro-box-hide"></div>
        <div id="ajaxcartpro-progress" class="ajaxcartpro-box-hide">
            <img src="themes/skin/frontend/healthpost/default/ajaxcartpro/images/al.gif" alt="">
            <p>Please wait...</p>
        </div>
        <div id="ajaxcartpro-add-confirm" class="ajaxcartpro-box-hide ajaxview" style="left: calc(50% - 325px);top: calc(30% - 28px);">
            <ul class="messages">
                <li class="success-msg">Sản Phẩm đã được thêm vào giỏ hàng</li>
            </ul>

            <div id="ajax_total_items_msg">Bạn có <span id="number_of_items"></span> Sản phẩm trong giỏ hàng</div>

            <!--<div class="ajax-cart-crosssell__heading"><span>You May Also Like</span></div>-->

            <div class="ajax-cart__buttons-set group">
                <a href="http://qbnatural.com/auckland-store/gio-hang" rel="nofollow" title="View Cart" class="button btn-view-cart">Giỏ hàng</a>
                <a href="http://qbnatural.com/auckland-store/gio-hang#checkout" class="aw-acp-checkout"><i class="fa fa-shopping-cart"></i> Thanh Toán</a>
                <a class="aw-acp-continue focus">Đóng</a>
            </div>
        </div>
        <div id="ajaxcartpro-add-confirm" class="ajaxcartpro-box-hide ajaxpost" style="left: calc(50% - 325px);top: calc(20% - 28px);">
            <ul class="messages ">
                 <li class="success-msg products-success-msg"></li>
            </ul>

            <div id="ajax_total_items_msg">
            </div>

            <!--<div class="ajax-cart-crosssell__heading"><span>You May Also Like</span></div>-->

            <div class="ajax-cart__buttons-set group">
            <a class="aw-acp-continue focus">Đóng</a>
                <a href="#" rel="nofollow" title="View Cart" class="button btn-view-cart btn-addcart-post">Mua Hàng</a>
              
                
            </div>
        </div>
        <div id="ajaxcartpro-add-confirm" class="ajaxcartpro-box-hide ajaxremove" style="left: calc(50% - 325px);top: calc(20% - 28px);">
        <ul class="messages">
                <li class="success-msg">Sản Phẩm đã được Xóa khỏi giỏ hàng</li>
            </ul>
            <div class="ajax-cart__buttons-set group">
                <a href="http://qbnatural.com/auckland-store/gio-hang" rel="nofollow" title="View Cart" class="button btn-view-cart">Giỏ hàng</a>
                <a href="http://qbnatural.com/auckland-store/gio-hang#checkout" class="aw-acp-checkout"><i class="fa fa-shopping-cart"></i> Thanh Toán</a>
                <a class="aw-acp-continue focus">Đóng</a>
            </div>
        </div>
        <div id="ajaxcartpro-add-confirm" class="ajaxcartpro-box-hide ajaxupdatecart" style="left: calc(50% - 325px);top: calc(20% - 28px);">
        <ul class="messages">
                <li class="success-msg">Sản Phẩm đã được Cẫp nhật giỏ hàng</li>
            </ul>
           <div class="ajax-cart__buttons-set group">
            
             
                <a class="aw-acp-continue focus">Tiếp Tục</a>
            </div>
        </div>
        <div id="ajaxcartpro-add-confirm" class="ajaxcartpro-box-hide ajaxapplyvoucher" style="left: calc(50% - 325px);top: calc(20% - 28px);">
        <ul class="messages">
                <li class="success-msg"></li>
            </ul>
            <div class="ajax-cart__buttons-set group">
            
              
                <a class="aw-acp-continue focus">Tiếp Tục</a>
            </div>
        </div>
        
        <div id="acp-configurable-block"></div>
 
            <script src="themes/libs/lazyload/lozad.min.js"></script>
            <script src="themes/libs/lazyload/intersection-observer.js"></script>
                    <script type="text/javascript">
                var observer = lozad('.lozad', {
            threshold: 0.1,
            load: function(el) {
                el.src = el.getAttribute("data-src");
                el.onload = function() {}
            }
        });
        var pictureObserver = lozad('.lazy', {
            threshold: 0.1
        });
        pictureObserver.observe();

            jQuery(document).ready(function($) {
            });
                (function($) {
                "use strict";   
                function flyToElement(flyer, flyingTo) {
                var $func = $(this);
                var divider = 3;
                var flyerClone = $(flyer).clone();
                $(flyerClone).css({position: 'absolute', top: $(flyer).offset().top + "px", left: $(flyer).offset().left + "px", opacity: 1, 'z-index': 1000});
                $('body').append($(flyerClone));
                var gotoX = $(flyingTo).offset().left + ($(flyingTo).width() / 2) - ($(flyer).width()/divider)/2;
                var gotoY = $(flyingTo).offset().top + ($(flyingTo).height() / 2) - ($(flyer).height()/divider)/2;
                $(flyerClone).animate({
                    opacity: 0.4,
                    left: gotoX,
                    top: gotoY,
                    width: $(flyer).width()/divider,
                    height: $(flyer).height()/divider
                }, 700,
                function () {
                    $(flyingTo).fadeOut('fast', function () {
                        $(flyingTo).fadeIn('fast', function () {
                            $(flyerClone).fadeOut('fast', function () {
                                $(flyerClone).remove();
                            });
                        });
                    });
                });
            }

        function flyFromElement(flyer, flyingTo, callBack /*callback is optional*/) {
            var $func = $(this);
            var divider = 3;
            var beginAtX = $(flyingTo).offset().left + ($(flyingTo).width() / 2) - ($(flyer).width()/divider)/2;
            var beginAtY = $(flyingTo).offset().top + ($(flyingTo).width() / 2) - ($(flyer).height()/divider)/2;
            var gotoX = $(flyer).offset().left;
            var gotoY = $(flyer).offset().top;
            var flyerClone = $(flyer).clone();
            $(flyerClone).css({
                position: 'absolute',
                top: beginAtY + "px",
                left: beginAtX + "px",
                opacity: 0.4,
                'z-index': 1000,
                width:$(flyer).width()/divider,
                height:$(flyer).height()/divider
            });
            $('body').append($(flyerClone));

            $(flyerClone).animate({
                opacity: 1,
                left: gotoX,
                top: gotoY,
                width: $(flyer).width(),
                height: $(flyer).height()
            }, 700,
                function () {
                    $(flyerClone).remove();
                    $(flyer).fadeOut('fast', function () {
                        $(flyer).fadeIn('fast', function () {
                            if (callBack != null) {
                                callBack.apply($func);
                            }
                        });
                    });
                });
        }           
        $('.aw-acp-continue').click(function(e){
        e.preventDefault();
        $('#acp-overlay').fadeOut();
                $('.ajaxview').fadeOut();
                $('.ajaxpost').fadeOut();
                $('.ajaxremove').fadeOut();
                $('.ajaxupdatecart').fadeOut();
                $('.ajaxapplyvoucher').fadeOut();
                $('.ajaxgetorders').fadeOut();
                return false;
        });
        $('#acp-overlay').click(function(e){
        e.preventDefault();
        $('#acp-overlay').fadeOut();
                $('.ajaxview').fadeOut();
                $('.ajaxpost').fadeOut();
                $('.ajaxremove').fadeOut();
                $('.ajaxupdatecart').fadeOut();
                $('.ajaxapplyvoucher').fadeOut();
                $('.ajaxgetorders').fadeOut();
                return false;
        });
            $('.ajaxcartview').click(function(e) {
            e.preventDefault();
            var qty = [];
            var qtynumkey = [];
            var qtyprice = [];
            var qtyname = [];
            var qtykeyname = [];
            var qtyidkey = [];
            var qtyid = [];
            $('.productspricelist').each(function(){
                qty.push($(this).find('input').val()); 
                qtynumkey.push($(this).find('input').attr('data-key')); 
                qtyprice.push($(this).find('input').attr('data-price')); 
                qtyidkey.push($(this).find('input').attr('data-idkey')); 
                qtyname.push($(this).find('input').attr('data-name')); 
                qtykeyname.push($(this).find('input').attr('data-keyname')); 
                qtyid.push($(this).find('input').attr('data-id')); 
            });
        var values = {
            method: 'addcart',
            qty: JSON.stringify(qty),
            idkey: JSON.stringify(qtyidkey),
            key: JSON.stringify(qtynumkey),
            price: JSON.stringify(qtyprice),
            qtyid: JSON.stringify(qtyid),
            name: JSON.stringify(qtyname),
            keyname: JSON.stringify(qtykeyname)
        };
        $.ajax({
            url: setlang + "/actionmethod",
            type: "post",
            data: values,
            beforeSend: function () {
            var img = $('.product-image-zoom').find('img');
            flyToElement($(img), $('.top-cart-right'));
    },
            success: function(response) {
            
                $('#acp-overlay').fadeIn();
                $('.ajaxview').fadeIn();
                var data = JSON.parse(response);
        $('#topCartContent').html(data.html);
                $('#cartHeader').find('span').html(data.total);
                $('.top-cart-right').find('.block-price').find('span').html(data.totalprice);
                $('#number_of_items').html(data.total);
                
                setTimeout(function(){
                $('#acp-overlay').fadeOut();
                $('.ajaxview').fadeOut();}, 5000); 
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown)
            }
        })
});
$('.btn-update-cart').click(function(e) {
e.preventDefault();
            var qtyidkey =  $(this).attr('data-idkey');
            var qtyprice =  $(this).attr('data-price');
            var qtynumkey =  $(this).attr('data-key');
            var qtyname =  $(this).attr('data-name');
            var qtykeyname =  $(this).attr('data-keyname');
            var qtyid =  $(this).attr('data-id');
            var qty =  $('.qty-'+qtyidkey ).val();
        var values = {
            method: 'upcart',
            qty: qty,
            idkey: qtyidkey,
            key: qtynumkey,
            price: qtyprice,
            qtyid: qtyid,
            name: qtyname,
            keyname: qtykeyname
        };
        $.ajax({
            url: setlang + "/actionmethod",
            type: "post",
            data: values,
            success: function(response) {
            
                $('#acp-overlay').fadeIn();
                $('.ajaxupdatecart').fadeIn();
                var data = JSON.parse(response);
                 $('.sub-cart-price-'+qtyidkey).html(data.subtotalcartpage);
                $('#cartHeader').find('span').html(data.total);
               $('.totalpriceall').html(data.totalprice);
                $('.top-cart-right').find('.block-price').find('span').html(data.totalprice);
                $('.voucherdown').html(data.voucherdown);
                $('.shipping-fee-amout').html(data.shippingfee);
                $('.totalprice-all').html(data.totalpriceall);
                $('.mini-cart-qty-'+qtyidkey).html(qty);
    
                
                setTimeout(function(){
                $('#acp-overlay').fadeOut();
                $('.ajaxupdatecart').fadeOut();}, 5000); 
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown)
            }
        })
        return false;
});
$('.buttonapplyvoucher').click(function(e) {
e.preventDefault();
            var voucher =  $('#coupon_code').val();
        var valuesvoucher = {
            method: 'voucher',
            voucher: voucher,
        };
        $.ajax({
            url: setlang + "/actionmethod",
            type: "post",
            data: valuesvoucher,
            success: function(response) {
            var data = JSON.parse(response);
            $('#acp-overlay').fadeIn();
                $('.ajaxapplyvoucher').fadeIn();
                if(data.type==="success"){
                
               $('.totalpriceall').html(data.totalprice);
                $('.top-cart-right').find('.block-price').find('span').html(data.totalprice);
                $('.voucherdown').html(data.textvoucher);
                $('.textvoucher').html(voucher);
                 $('.use-voucher').fadeIn();
                
                
                }
                
                $('.ajaxapplyvoucher').find('.success-msg').html(data.msg);
                setTimeout(function(){
                $('#acp-overlay').fadeOut();
                $('.ajaxapplyvoucher').fadeOut();}, 5000); 
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown)
            }
        })
        return false;
});
$('.shipping-update').focus(function(e) {
  $(this).attr('oldValue',$(this).val());
 
});
/*$('.shipping-update').change(function(e) {
e.preventDefault();
            var fee =  $(this).find(':selected').attr('data-fee');
            var feeoldsel = $(this).attr('oldValue');
        if(fee===undefined){$('.shipping-update').val(feeoldsel);
        }else{
            
        
           var valuesvoucher = {
            method: 'fee',
            fee: fee,
        };
        $.ajax({
            url: setlang + "/actionmethod",
            type: "post",
            data: valuesvoucher,
            success: function(response) {
                
                $('#acp-overlay').fadeIn();
                $('.ajaxapplyvoucher').fadeIn();
                var data = JSON.parse(response);
               $('.totalpriceall').html(data.totalprice);
               $('.voucherdown').html(data.subpricedown);
                $('.top-cart-right').find('.block-price').find('span').html(data.totalprice);
                $('.shipping-fee-amout').html(data.textshippingfee);
                 $('.shipping-fee-total').fadeIn();
                
                setTimeout(function(){
                $('#acp-overlay').fadeOut();
                $('.ajaxapplyvoucher').fadeOut();}, 5000); 
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown)
            }
        })
        
        }
        return false;
});*/
$('.ajaxcartpost').click(function(e) {
e.preventDefault();
        var id = $(this).attr('data-id');
        var values = {
            method: 'ajaxcartpost',
            id: id,

        };
        $.ajax({
            url: setlang + "/actionmethod",
            type: "post",
            data: values,
            
            success: function(response) {
            var data = JSON.parse(response);
                    $('#acp-overlay').fadeIn();
                $('.ajaxpost').fadeIn();
                 $('.ajaxpost').find('.products-success-msg').html(data.nameproducts);
         $('.ajaxpost').find('#ajax_total_items_msg').html(data.html);
          $('.ajaxpost').find('.btn-num-product-up').on('click', function(e) {
           e.preventDefault();
    var numProduct = Number($(this).prev().val());
    if (numProduct > 1) $(this).prev().val(numProduct - 1)
          
  
});
 $('.ajaxpost').find('.btn-num-product-down').on('click', function(e) {

     e.preventDefault();
    var numProduct = Number($(this).next().val());
    $(this).next().val(numProduct + 1)

});
         $('.ajaxpost').find('.btn-addcart-post').click(function(e){
         e.preventDefault();
         var qty = [];
            var qtynumkey = [];
            var qtyprice = [];
            var qtyname = [];
            var qtykeyname = [];
            var qtyidkey = [];
            var qtyid = [];
            $('.productspricelistpost').each(function(){
                qty.push($(this).find('input').val()); 
                qtynumkey.push($(this).find('input').attr('data-key')); 
                qtyprice.push($(this).find('input').attr('data-price')); 
                qtyidkey.push($(this).find('input').attr('data-idkey')); 
                qtyname.push($(this).find('input').attr('data-name')); 
                qtykeyname.push($(this).find('input').attr('data-keyname')); 
                qtyid.push($(this).find('input').attr('data-id')); 
            });
        var values = {
            method: 'addcart',
            qty: JSON.stringify(qty),
            idkey: JSON.stringify(qtyidkey),
            key: JSON.stringify(qtynumkey),
            price: JSON.stringify(qtyprice),
            qtyid: JSON.stringify(qtyid),
            name: JSON.stringify(qtyname),
            keyname: JSON.stringify(qtykeyname)
        };
        $.ajax({
            url: setlang + "/actionmethod",
            type: "post",
            data: values,
            beforeSend: function () {
            
            
            
            var img = $('.grouped-items-list').find('img').eq(0);
            flyToElement($(img), $('.top-cart-right'));
            },
            success: function(response) {
            
                $('.ajaxpost').fadeOut();
                $('.ajaxview').fadeIn();
                var data = JSON.parse(response);
        $('#topCartContent').html(data.html);
                $('#cartHeader').find('span').html(data.total);
                $('.top-cart-right').find('.block-price').find('span').html(data.totalprice);
                $('#number_of_items').html(data.total);
                
                setTimeout(function(){
                $('#acp-overlay').fadeOut();
                $('.ajaxview').fadeOut();}, 5000); 
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown)
            }
            
        })
            
         });
         return false;
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown)
            }
        })
        return false;
});
 window.onresize = function(event) {
    var viewportwidth = $(window).width();
    if (viewportwidth>991) {
    var maxHeight = 0;
    $(".only-grid .product-info").each(function(){
       if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
    });
    $(".only-grid .product-info").height(maxHeight);
    }else{
        $(".only-grid .product-info").height('auto');
    }
}

     window.onresize();
    })(jQuery); 
    CART = {
         remove: function(ids) {
        var id = ids;
        var valuesremove = {
            method: 'removecart',
            id: id
        };
        jQuery.ajax({
            url: setlang + "/actionmethod",
            type: "post",
            data: valuesremove,
            success: function(response) {
                var resp = jQuery.parseJSON(response);
                jQuery('.parent-' + id).remove();
                setTimeout(function(){
                        
                        jQuery('#acp-overlay').fadeIn();
                        jQuery('.ajaxremove').fadeIn();
       
                        jQuery('.totalprice-all').html(data.totalpriceall);
                        jQuery('#cartHeader').find('span').html(resp.total);
                        jQuery('.top-cart-right').find('.block-price').find('span').html(resp.totalprice);
                        jQuery('.totalpriceall').html(resp.totalprice);
                        }, 1000); 
                         jQuery('.voucherdown').html(data.voucherdown);
                        setTimeout(function(){
                        jQuery('#acp-overlay').fadeOut();
                        jQuery('.ajaxremove').fadeOut();}, 5000); 
                if (resp.type === "error") {
                    
                    window.location.href = 'http://qbnatural.com'
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown)
            }
            
        });
        return false;
    }   
    }
        </script>
        Array
        <div class="geo-popup-wrapper">
            <div class="geo-overlay"></div>

            <div class="geo-box geo-box-style-default" id="geo-box-NZ" style="display:none;">
                <div class="close"><i class="fa fa-times close" aria-hidden="true"></i></div>
                <div class="geo-content-wrapper">
                    <div class="geo-content-store-container" id="geo-content-default">
                        <div class="geo-header"><span><span class="accent">Shopping from <strong>New Zealand?</strong></span></span>
                        </div>
                        <div class="geo-actions">
                            <a href="" class="action">Shop on <strong>HealthPost.co.nz</strong></a>
                            <a href="#" class="close">Or continue shopping on HealthPost.com.au</a>
                        </div>
                        <div class="geo-content">
                            <p>
                                Visit our dedicated New Zealand website for the full range of natural health, supplements and skincare, with New Zealand pricing and delivery options. </p>
                        </div>
                    </div>
                    <div class="geo-footer">
                        <div class="geo-logo">
                            <span><img itemprop="logo" src="themes/skin/frontend/healthpost/default/images/HP_logo.png" alt="Healthpost logo"></span>
                            <span><img src="themes/skin/frontend/healthpost/default/images/HP_tagline.png" alt="Healthpost logo"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="checkout-progressbar" class="ajaxcartpro-box-hide">
            <img src="themes/skin/frontend/healthpost/default/ajaxcartpro/images/al.gif" alt="">
            <p>Please wait, processing your order...</p>
        </div>
    </div>
</div>


<!-- send phone info -->
<script type="text/javascript">
    jQuery('#tuan-btn').click(function(e) {
        e.preventDefault();
        var btn = document.getElementById('tuan-btn');
        var femail = document.getElementById('femail').value;
        // alert(femail);
        if(femail != '') {
            $.ajax({
                url: 'ajax/addEmail',
                type: 'post',
                data: {femail: femail},
                success: function(res) {
                    console.log(res);
                    document.getElementById('femail').value = '';
                    $(this).find("button[id='tuan-btn']").prop('disabled',true);
                    // btn.attr('disabled','disabled');
                }
            })
        }
        return false;
    });
</script>

<style type="text/css">
    .footer-links ul li:hover{
        background: none;
        color: #020000;
    }
</style>