<div class="section pt-7 pb-7">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="blog-list" >
                    <h1 style="color: black;font-weight: 700"><?php echo  $info[0]['title_vn'] ?></h1>
					<?php echo $info[0]['content_vn'];?>
                </div>
            </div>

             <div class="col-md-3">
                <div class="sidebar" style="margin-top: 40px">
                    <div class="widget widget_posts_widget">
                        <h3 class="widget-title" style="color: black">Blog Liên Quan</h3>
                        <ul>
							<?php foreach($baiviet as $item) { ?>
								<?php if ($info[0]['alias'] === $item['alias']){ ?>
									<?php echo ""; ?>
								<?php }else{ ?>
									<li style="padding:5px;text-align: left;">
										<a href="<?php echo base_url('tin-tuc/'.$item['alias'].'.html'); ?>"><?php echo $item['title_vn']; ?></a>
										<span style="float: right;font-size: 10px;color: #666;margin-top: 15px">- <?php echo date("d-m-Y",$item['date']) ?> -</span>
									</li>
								<?php } ?>
							<?php } ?>
						</ul>

                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

 </div>