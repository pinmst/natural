<div class="section pt-7 pb-7">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="blog-list">
                    <ul>
                    <?php
                        if(!empty($info)){
                        foreach($info as $item){
                    ?>
                        <li>
                            <img src="<?php echo PATH_IMG_NEWS.$item['images'] ?>" alt="<?php echo $item['title_vn'] ?>">
                            <h3><?php echo $item['title_vn'] ?></h3>
                            <p><?php echo stripcslashes($item['description_vn']); ?></p>
                            <p style="font-size: 12px;color: #aaa;text-align:center;">-- <?php echo date("d-m-Y",$item['date']) ?> --</p>
                            <p style="float: right;"><a href="<?php echo base_url('tin-tuc/'.$item['alias'].".html"); ?>" style="">Xem Thêm</a></p>
                        </li>

                    <?php }}else{ ?>
                        <div class="alert alert-danger" style="margin-top:20px;">Không có dữ liệu</div>
                    <?php } ?>

                        
                        <div class="pagination" style=""><?php echo $this->pagination->create_links();?></div>
                        
                    
                  </ul>

                </div>


            </div>
        </div>
    </div>
</div>

 </div>

<style type="text/css">
ul {
list-style-type: none;
width: 100%;
}

h3 {
font: bold 20px/1 Helvetica, Verdana, sans-serif;
color: black;
margin-top: 0;
}
li p strong{
    font-weight: normal;
}
li img {
float: left;
margin: 0 15px 0 0;
max-width: 250px
}

li {
padding: 10px;
overflow: auto;
}

li:hover {
background: #eee;
cursor: pointer;
}
.pagination{
    margin: auto;
    box-sizing: border-box;
    height: 40px;
    padding: 8px 10px;
    display: block;
    padding-left: 0;
    margin: 20px 0;
    border-radius: 4px;
  }

  .pagination a{
    display: inline-block;
    padding: 6px 10px;
    background-color: #eee;
    text-align: center;
    box-sizing: border-box;
    font-size: 12px;
    color: #686868;
    margin: 0 2px;
    text-decoration: none!important;
  }
  .pagination a:hover{
    color: #fff;
    background-color: #e74847;
  }
  .pagination strong {
    display: inline-block;
    padding: 6px 10px;
    text-align: center;
    box-sizing: border-box;
    font-size: 12px;
    margin: 0 2px;
    text-decoration: none;
    color: #fff;
    background-color: #e74847;
}
</style>