<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	protected $arrowmap = " | ";
	protected $map_title = '<a href="/">Trang chủ</a>';
	public function __construct()
	{
		parent::__construct();
		$this->load->model('catelog_model');
		$this->load->model('catdeal_model');
		$this->load->model('user_model');
		$this->load->model('manufacturer_model');
		$this->load->model('bmenu_model');
		$this->load->model('menu_model');
		$this->load->model('news_model');
		$this->load->model('product_model');
		$this->load->model('deal_model');
		$this->load->model('pagehtml_model');
		$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
	}
	public function changeviews($id){
		$this->session->set_userdata('gridviews',$id);
	}
	public function index()
	{
		/*$info = $this->product_model->list_data(array('idmanufacturer'=>0),99990,0);
		if(!empty($info)){
			foreach($info as $k){
				$this->product_model->update($k['Id'],array('idmanufacturer'=>'-1'));
			}
		} */
		//-- set cookie lưu id thành viên giời thiệu
		$idab = $this->input->get();
		$this->pagehtml_model->setcookieAbout($idab);

		// Product sale
		// $sql ="SELECT Id FROM  mn_product 
		// 	 	 WHERE ticlock = 0  AND trash = 0 AND status = 0 AND price != sale_price AND price != 0 
		// 	 	ORDER BY Id DESC";
		
		// Product new
		$sql ="SELECT * FROM  mn_product 
			 	 WHERE ticlock = 0  AND trash = 0 AND status = 0
			 	ORDER BY date DESC";
			 	
		$temp['data']['productNew']= $this->product_model->get_query($sql,0,5); 
		
		$temp['linktag'] = array(array("href" => USER_PATH_CSS."owl.carousel.css","type" => "text/css","rel"=>"stylesheet"));
			$temp['scripttag'] = array(USER_PATH_JS.'owl.carousel.js');
			//$temp['data']['cateloghome']  = $this->catelog_model->get_list(array('ticlock'=>0,'home'=>1,"parentid"=>0),'sort ASC, Id DESC',100,0);
			//$temp['data']['menu']  = $this->catdeal_model->get_list(array('ticlock'=>0,'home'=>1,"parentid"=>0),'sort ASC, Id DESC',100,0);
			
			$temp['data']['baiviet'] = $this->pagehtml_model->get_newsidcat(10,10,0);
			$temp['data']['info_new'] = $this->pagehtml_model->get_on_list_weblink(array('style'=>5,'layout'=>'', 'ticlock'=>'0'),9);
			$temp['data']['manufacturer'] =  $this->manufacturer_model->get_Arr(array('ticlock'=>0));
			$temp['template']='default/home/index'; 
			$this->load->view("default/layout",$temp); 
		
	}
	public function page($id){ 
		$this->load->model('flash_model');
		$temp['data']['idcat'] = $id; 
		$temp['data']['about']  = $this->pagehtml_model->get_public($id);
		$temp['data']['breadcrumb'] =  $this->map_title .$this-> arrowmap . '<a href = "'.base_url($temp['data']['about'][0]['alias'].".html").'" >'.$temp['data']['about'][0]['title_vn'].'</a>';
		$temp['template']='default/home/page'; 
			$this->load->view("default/layout",$temp);
	}

	public function detail($alias){
		$temp['data']['idcat'] = 0;
		$temp['data']['info'] = $info = $this->news_model->get_list(array('alias'=>$alias));
		if(!$alias || empty($info)) redirect(base_url('404.html'));
		$temp['template']='default/home/page';
		$this->load->view("default/layout",$temp); 
	}

	public function contact(){
		$this->load->model('contact_model');
		$fullname = $this->input->post('fullname');
		$email = $this->input->post('email');
		$title = $this->input->post('title');
		$content = $this->input->post('content');
		$mess = "Nhập thông tin cần thiết";
		
		if($fullname=="") { $mess = "Nhập họ tên"; $key = "#fullname-contact"; }
		else if($email=="") { $mess = "Nhập email"; $key = "#email-contact";  }
		else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) { $mess = "Email không đúng định dạng"; $key = "#email-contact"; }
		else if($title=="") { $mess = "Nhập tiêu đề";   $key = "#title-contact";} 
		else if($content==""){  $mess = "Nhập nội dung cần gửi"; $key = "#content-contact"; }
		else {
			$arr = array(
				"title" => $title,
				"email" =>$email,
				"fullname" =>$fullname,
				"content_vn"=> $content,
				"date"	=>time(),
			);
			$this->contact_model->add($arr);
			$mess = "Gửi thông tin liên hệ thành công. <br />Chúng tôi sẽ sớm liên hệ với bạn !";
			die(json_encode(array('err'=>"false","mess"=>$mess)));
		}
		die(json_encode(array('err'=>"true","mess"=>$mess,"key"=>$key)));
	}
	function email(){
		$email = $this->input->post('email');
		if($email ==""){
			die(json_encode(array('mess'=>"Địa chỉ email không được bỏ trống!")));
		}
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			die(json_encode(array('mess'=>"Email không đúng định dạng")));
		}else{
			$this->load->model('contact_model');
			$arr = array(
				"email" =>$email,
				'date'=>time(),
				'ticlock' =>0,
				'iduser' => $this->session->userdata('login_user_id'),
			);
			$this->contact_model->add($arr);
			die(json_encode(array('mess'=>"Đăng ký email thành công")));
		}
		
	}
	public function notfound(){
		$temp['data'] = NULL;
		if(USERTYPE=='Mobile'){
			$temp['template']='default/home/m_notfound';
			$this->load->view("default/layoutMobile",$temp); 
		}else{
			$temp['template']='default/home/notfound'; 
			$this->load->view("default/layout",$temp); 
		}
	}
	public function message($id,$deal){
		
		$this->load->model('product_model');
		$this->load->model('deal_model');
		if($deal==1):
			$temp['info'] = $this->deal_model->get_Id($id);
		else:
			$temp['info'] = $this->product_model->get_Id($id);
		endif;
		$temp["deal"] = $deal;
		$this->load->view("default/home/message",$temp); 
	}
	public function feedback($id){
		$this->load->model('payment_model');
		$iduser = $this->session->userdata('login_user_id');
		$temp['cus']= $this->payment_model->get_list(array('code'=>$id,"iduser"=>$iduser));

		if(empty($temp['cus'])) die;

		if($this->input->post('save')){
			$error =false;
			$reasoncancel= $this->input->post('reasoncancel');
			$reason= $this->input->post('reason');
			if($reasoncancel<=0){
				$error =true;
				$message = "<div class=\"alert alert-danger\">Chọn lý do hủy đơn hàng</div>";
			}elseif($reasoncancel==5){
				if(strlen($reason)<=5){
					$error =true;
					$message = "<div class=\"alert alert-danger\">Nhập lý do hủy</div>";
				}
			}
			if($error==false){ 
				$error =false;
				$message = "<div class=\"alert alert-success\">Hủy đơn hàng thành công</div>";
				
				$this->payment_model->update($temp['cus'][0]["Id"],array("status"=>6));
				$this->payment_model->addreasoncancel(array("idcustomer"=>$temp['cus'][0]["Id"],"idreasoncancel"=>$reasoncancel,"content"=>$reason,"date"=>time())); 
			}
			die(json_encode(array("error"=>$error,"message"=>$message)));
		}
		$this->load->view("default/home/feedback",$temp); 
	}
	public function  sentmessage(){
		$this->load->model('contact_model');
		$fullname = $this->input->post('fullname');
		$phone = $this->input->post('phone');
		$idpro = $this->input->post('idpro');
		$deal = $this->input->post('deal');
		//--------check form----------------
		$error  = false;
		$mess=  NULL;
		if(empty($fullname)){
			$error = true;
			$mess['fullname-message'] = "Nhập tên họ tên của bạn";
		}
		if(empty($phone) ){
			$error = true;
			$mess['phone-message'] = "Nhập số điện thoại của bạn";
		}else if(strlen($phone)<9 || strlen($phone)>12 || !is_numeric($phone) ){
			$error = true;
			$mess['phone-message'] = "Số điện thoại không đúng";
		}
		
		if($error==false){
			
			$arr= array(
				"fullname" =>$fullname,
				"email" => "",
				"date" => time(),
				"phone" => $phone,
				"note" => "",
				"deal" => $deal,
				"idpro" => $idpro,
				"ticlock" => 0,
				"view" => 0,
				"iduser" => $this->session->userdata('login_user_id'),
			);
			$this->contact_model->add($arr);
			$mess = '<div class="alert alert-success">Gửi yêu cầu thành công. Mada sẽ gọi lại cho bạn trong thời gian sớm nhất. xin cảm ơn !</div><div class="form-group center " style="margin-top:15px">
	<button type="button" class="btn" onClick="close_box_popup()">Đóng</button></div>';
			die(json_encode(array("err"=>$error,"mess"=>$mess)));
		}
		die(json_encode(array("err"=>$error,"mess"=>$mess)));
	}
	public function landing(){
		$this->load->view("default/home/landing"); 
	}
	public function vquyen($id=0){
		$arr = "";
		$info  = $this->pagehtml_model->get_catelog($id);
		if(!empty($info)){
			foreach($info as $item){
				$subid =$this->page->getSubCatlogId($item['Id']);	
				if($subid !="")
					$arr .=$item["Id"].",".$subid;
				else
				$arr .=$item["Id"].",";
			}
		}
		echo $arr.$id;
	}
	public function excel()
	{
		$sql = "SELECT *,(SELECT title_vn FROM mn_catelog WHERE mn_catelog.Id= mn_product.idcat ) AS danhmuc,(SELECT title_vn FROM mn_manufacturer WHERE mn_manufacturer.Id= mn_product.idmanufacturer) AS manu FROM mn_product WHERE ticlock=0 AND trash=0";
		$data['info'] =  $this->product_model->get_query($sql,9999,0);
		$this->load->view("default/home/excel",$data); 
	}
	
}
