<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Persion extends CI_Controller {
	protected $arrowmap = " > ";
	protected $map_title = '<a href="/">Trang chủ</a>';
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('color_model');
		$this->load->model('size_model');
		$this->load->model('flash_model');
		$this->load->model('manufacturer_model');
		$this->load->library('mgallery');
		$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
		$login_user_ticlock = $this->session->userdata('login_user_ticlock'); 
		$login_user_lock = $this->session->userdata('login_user_lock');
		if($this->session->userdata('login_id')!=1){
			redirect(base_url('dang-nhap.html'));
		}elseif($login_user_ticlock==1){
			redirect(base_url('user/ticlock'));
		}elseif($login_user_lock==1){
			redirect(base_url('user/lock'));
		}
	}
	
	public function  index($p=0)
	{
		$iduser = $this->session->userdata('login_user_id');
		$login_user_level = $this->session->userdata('login_user_level');
		$avatar = $this->session->userdata('login_user_avatar');
		if($login_user_level==1) redirect(base_url('thong-tin-ca-nhan.html'));
		if(!$avatar) $temp['data']['avatar'] = USER_PATH_IMG."avatar.png";
		else $temp['data']['avatar'] = PATH_IMG_USER.$avatar;
		
		$temp['data']['p'] = $p;
		$numrow = 20;
		$div = 10;
		$skip = $p * $numrow;
		$link = base_url("quan-ly-san-pham.html/p");
		$sql = "SELECT * FROM  mn_product WHERE iduser='".$iduser."' AND ticlock='0' AND trash =0  ORDER BY date DESC";
		$sql_total ='SELECT COUNT(Id) as total FROM mn_product WHERE iduser= "'.$iduser.'" AND trash =0 ';
		$temp['data']['info']= $this->product_model->get_query($sql,$numrow,$skip);
		$temp['data']['total']= $this->product_model->count_query($sql_total);
		$temp['data']['page']= $this->page->divPageF($temp['data']['total'],$p,$div,$numrow,$link);
		$temp['data']['user'] = $this->user_model->get_one_user($iduser);
		
		$temp['template']='default/persion/index';

		$this->load->view("default/layout",$temp); 
	}
	
	public function editthread($id){
		$iduser = $this->session->userdata('login_user_id');
		$this->form_validation->set_message('required','Vui lòng nhập %s');
		$this->form_validation->set_message('is_unique','%s đã tồn tại');
		$this->form_validation->set_message('is_natural_no_zero','Vui lòng chọn %s');
		$this->form_validation->set_message('min_length','%s phải lớn hơn 50 ký tự');
		$this->form_validation->set_rules('sale_price', 'Giá bán', 'required');
		$this->form_validation->set_rules('idcat', 'Danh mục đăng tin', 'required');
		$this->form_validation->set_rules('title_vn', 'Tiêu đề', 'required|min_length[10]',array("min_length"=>"Tên sản phẩm quá ngắn"));
		
		$this->form_validation->set_rules('description_vn','Mô tả','max_length[500]',array('max_length'=>'Mô tả sản phẩm không được quá 300 ký tự'));
		$this->form_validation->set_rules('content_vn','Nội dung','required|min_length[10]');
		$this->form_validation->set_error_delimiters('<br /><span class="input-error ">', '</span>');
		
		if($this->input->post('save'))
		{
			if($this->form_validation->run() == TRUE  )
			{
				if(!empty($_FILES['images']['name'])){ 
					$tenhinh = $this->page->strtoseo($this->input->post('title_vn'))."-".time();
					$images  = $this-> mgallery-> Upload($tenhinh,$_FILES['images'],FCPATH."data/Product/",500,"auto");
					if($images!="") $data['images']  = $images;
				}
				if(!empty($_FILES['images1']['name'])){ 
					$tenhinh1 = $this->page->strtoseo($this->input->post('title_vn'))."-1".time();
					$images1 = $this-> mgallery-> Upload($tenhinh1,$_FILES['images1'],FCPATH."data/Product/",500,"auto");
					if($images1!="") $data['images1']  = $images1;
				}
				if(!empty($_FILES['images2']['name'])){ 
					$tenhinh2 = $this->page->strtoseo($this->input->post('title_vn'))."-2".time();
					$images2  = $this-> mgallery-> Upload($tenhinh2,$_FILES['images2'],FCPATH."data/Product/",500,"auto");
					if($images2!="") $data['images2']  = $images2;
				}
				if(!empty($_FILES['images3']['name'])){ 
					$tenhinh3 = $this->page->strtoseo($this->input->post('title_vn'))."-3".time();
					$images3  = $this-> mgallery-> Upload($tenhinh3,$_FILES['images3'],FCPATH."data/Product/",500,"auto");
					if($images3!="") $data['images3']  = $images3;
				}
				if(!empty($_FILES['images4']['name'])){ 
					$tenhinh4 = $this->page->strtoseo($this->input->post('title_vn'))."-4".time();
					$images4  = $this-> mgallery-> Upload($tenhinh4,$_FILES['images4'],FCPATH."data/Product/",500,"auto");
					if($images4!="") $data['images4']  = $images4;
					
				}
				$data['title_vn'] = $this->input->post('title_vn');
				$data['alias'] = $this->page->strtoseo($this->input->post('title_vn'));
				$data['idcat'] = (int)$this->input->post('idcat');
				$data['idmanufacturer'] = (int)$this->input->post('idmanufacturer');
				$data['price'] = str_replace(".","",$this->input->post('price'));
				$data['sale_price'] = str_replace(".","",$this->input->post('sale_price'));
				$data['description_vn'] = nl2br($this->input->post('description_vn'));
				$data['content_vn'] = $this->page->replace_script($this->input->post('content_vn'));
				$this->product_model->update_user($id,$iduser,$data);
				$color = $this->input->post('color');
				$size = $this->input->post('size');
				if(!empty($color)){
					$this->product_model->deleteColor(array("idpro"=>$id));
					foreach($color as $k=>$v){
						$this->product_model->addColor(array("idcolor"=>$v,"idpro"=>$id));
					}
				}
				if(!empty($size)){
					$this->product_model->deleteSize(array("idpro"=>$id));
					foreach($size as $k=>$v){
						$this->product_model->addSize(array("idsize"=>$v,"idpro"=>$id));
					}
				}
				
				redirect(base_url("quan-ly-san-pham.html"));
			}
			
		}
		//-----------
		$temp['data']['catelog'] =  $this->pagehtml_model->get_catelog(0);
		$temp['data']['info']= $this->product_model->get_Id($id);
		$temp['data']['lcolor'] =  $this->color_model->list_where(array('ticlock'=>0));
		$temp['data']['lsize'] =  $this->size_model->list_where(array('ticlock'=>0));
		$arr = array('-1');
		$lpsize = $this->product_model->get_pro_size(array("idpro"=>$id));
		if(!empty($lpsize)){
			foreach($lpsize as $k){
				$arr[] = $k['idsize'];
			}
		}
		$temp['data']['lpsize'] =$arr;
		
		$arr = array('-1');
		$lpcolor = $this->product_model->get_pro_color(array("idpro"=>$id));
		if(!empty($lpcolor)){
			foreach($lpcolor as $k){
				$arr[] = $k['idcolor'];
			}
		}
		$temp['data']['lpcolor'] =$arr;
		
		$temp['data']['manufacturer'] =  $this->manufacturer_model->get_Arr(array('ticlock'=>0));
		
		$temp['template']='default/persion/editthread';
		$this->load->view("default/layout",$temp); 
	}
	public function  addthread()
	{	
		$login_user_level = $this->session->userdata('login_user_level');
		if($login_user_level==1) redirect(base_url('thong-tin-ca-nhan.html'));
		
		$this->form_validation->set_message('required','Vui lòng nhập %s');
		$this->form_validation->set_message('is_unique','%s đã tồn tại');
		$this->form_validation->set_message('is_natural_no_zero','Vui lòng chọn %s');
		$this->form_validation->set_message('min_length','%s phải lớn hơn 50 ký tự');
		$this->form_validation->set_rules('sale_price', 'Giá', 'required');
		$this->form_validation->set_rules('idcat', 'Danh mục đăng tin', 'required');
		$this->form_validation->set_rules('title_vn', 'Tiêu đề', 'required|min_length[10]|callback_total_product_check',array("min_length"=>"Tên sản phẩm quá ngắn"));
		$this->form_validation->set_rules('images','Hình ảnh','callback_images_check');
		$this->form_validation->set_rules('content_vn','Nội dung','required|min_length[10]');
		$this->form_validation->set_rules('description_vn','Mô tả','max_length[500]',array('max_length'=>'Mô tả sản phẩm không được quá 300 ký tự'));
		
		$this->form_validation->set_error_delimiters('<br /><span class="input-error ">', '</span>');
		
		if($this->input->post('save'))
		{
			
			
			if($this->form_validation->run() == TRUE  )
			{
				if(!empty($_FILES['images']['name'])){ 
					$tenhinh = $this->page->strtoseo($this->input->post('title_vn'))."-".time();
					$data['images']  = $this-> mgallery-> Upload($tenhinh,$_FILES['images'],FCPATH."data/Product/",500,"auto");
				}
				if(!empty($_FILES['images1']['name'])){ 
					$tenhinh1 = $this->page->strtoseo($this->input->post('title_vn'))."-1".time();
					$data['images1']  = $this-> mgallery-> Upload($tenhinh1,$_FILES['images1'],FCPATH."data/Product/",500,"auto");
				}
				if(!empty($_FILES['images2']['name'])){ 
					$tenhinh2 = $this->page->strtoseo($this->input->post('title_vn'))."-2".time();
					$data['images2']  = $this-> mgallery-> Upload($tenhinh2,$_FILES['images2'],FCPATH."data/Product/",500,"auto");
				}
				if(!empty($_FILES['images3']['name'])){ 
					$tenhinh3 = $this->page->strtoseo($this->input->post('title_vn'))."-3".time();
					$data['images3']  = $this-> mgallery-> Upload($tenhinh3,$_FILES['images3'],FCPATH."data/Product/",500,"auto");
				}
				if(!empty($_FILES['images4']['name'])){ 
					$tenhinh4 = $this->page->strtoseo($this->input->post('title_vn'))."-4".time();
					$data['images4']  = $this-> mgallery-> Upload($tenhinh4,$_FILES['images4'],FCPATH."data/Product/",500,"auto");
				}
				$data['title_vn'] = $this->input->post('title_vn');
				$data['alias'] = $this->page->strtoseo($this->input->post('title_vn'));
				$data['idcat'] = $this->input->post('idcat');
				$data['price'] = (int)str_replace(".","",$this->input->post('price'));
				$data['sale_price'] = (int)str_replace(".","",$this->input->post('sale_price'));
				$data['description_vn'] = nl2br($this->input->post('description_vn'));
				$data['content_vn'] = $this->page->replace_script($this->input->post('content_vn'));
				$data['iduser'] = $this->session->userdata('login_user_id');
				$data['date'] = time();
				$data['sort'] = 0;
				$data['ticlock'] = '0';
				$data['trash'] = 0;
				$id = $this->product_model->add($data,false);
				$color = $this->input->post('color');
				$size = $this->input->post('size');
				if(!empty($color)){
					foreach($color as $k=>$v){
						$this->product_model->addColor(array("idcolor"=>$v,"idpro"=>$id));
					}
				}
				if(!empty($size)){
					foreach($size as $k=>$v){
						$this->product_model->addSize(array("idsize"=>$v,"idpro"=>$id));
					}
				}
				redirect(base_url("quan-ly-san-pham.html"));
			}
			
		}
		$temp['data']['lcolor'] =  $this->color_model->list_where(array('ticlock'=>0));
		$temp['data']['lsize'] =  $this->size_model->list_where(array('ticlock'=>0));
		$temp['template']='default/persion/addthread';
		$temp['data']['catelog'] =  $this->pagehtml_model->get_catelog(0);
		$temp['data']['manufacturer'] =  $this->manufacturer_model->get_Arr(array('ticlock'=>0));
		$this->load->view("default/layout",$temp); 
	}
	
	public function viewup()
	{
		$iduser = $this->session->userdata('login_user_id');
		$avatar = $this->session->userdata('login_user_avatar');
		
		if(!$avatar) $temp['data']['avatar'] = USER_PATH_IMG."avatar.png";
		else $temp['data']['avatar'] = PATH_IMG_USER.$avatar;
		
		$temp['data']['p'] = $p =  (int)$this->uri->segment(4);
		$numrow = 30;
		$div = 10;
		$skip = $p * $numrow;
		$link = base_url("quan-ly-san-pham.html/p");
		$sql = "SELECT iduser,idpro,date,hour,minute,Id,
		(SELECT title_vn FROM mn_product WHERE mn_product.Id= mn_autoup.idpro) as title_vn,
		(SELECT alias FROM mn_product WHERE mn_product.Id= mn_autoup.idpro) as alias,
		(SELECT images FROM mn_product WHERE mn_product.Id= mn_autoup.idpro) as images
		 FROM mn_autoup WHERE iduser='".$iduser."'";
		$sql_total ='SELECT COUNT(idpro) as total FROM mn_autoup WHERE iduser= "'.$iduser.'"';
		$temp['data']['info']= $this->product_model->get_query($sql,$numrow,$skip);
		$temp['data']['total']= $this->product_model->count_query($sql_total);
		$temp['data']['page']= $this->page->divPageF($temp['data']['total'],$p,$div,$numrow,$link);
		$temp['data']['user'] = $this->user_model->get_one_user($iduser);
		
		$dir = FCPATH.'data/Upuser/'.$iduser;
        $result = @file_get_contents($dir);
		if ($result == FALSE) {
			$temp['data']['up'] = 0;	
		}else{
			$temp['data']['up'] = $result;	
		}
		
		$temp['template']='default/persion/viewup';
		
		$this->load->view("default/layout",$temp); 
	}
	
	public function total_product_check($file){
		$iduser = $this->session->userdata('login_user_id');
		if($iduser<=0) return false;
		$total = $this->product_model->count_user($iduser);
		if($total>200) {
			$this->form_validation->set_message('total_product_check', "Mỗi nhà bán sỉ không được đăng quá 200 sản phẩm");
			return false;
		}else{
			return true;
		} 
	}
	public function images_check($file){
		if(empty($_FILES['images']['name'])) {
			$this->form_validation->set_message('images_check', "Chọn hình ảnh cho sản phẩm");
			return false;
		}else{
			return true;
		} 
	}
	public function delthread($id){
		$iduser = $this->session->userdata('login_user_id');
		$this->product_model->update_user($id,$iduser,array('ticlock'=>'1'));
		redirect(base_url('quan-ly-san-pham.html'));
	}
	
	public function info()
	{	
		$iduser = $this->session->userdata('login_user_id');
		$avatar = $this->session->userdata('login_user_avatar');
		if(!$avatar) $temp['data']['avatar'] = USER_PATH_IMG."avatar.png";
		else $temp['data']['avatar'] = PATH_IMG_USER.$avatar;

		$temp['data']['success'] = 0;

		$this->form_validation->set_message('required','Vui lòng nhập %s');
		$this->form_validation->set_message('is_unique','%s đã tồn tại');
		$this->form_validation->set_message('is_natural_no_zero','Vui lòng chọn %s');
		
		$this->form_validation->set_rules('address', 'Địa chỉ', 'required');
		$this->form_validation->set_rules('phone', 'Điện thoại', 'required');
		$this->form_validation->set_rules('idtinh','Tỉnh thành','required|is_natural_no_zero',array("is_natural_no_zero"=>"Chọn tỉnh thành"));
		$this->form_validation->set_rules('iddistrict','Quận huyện','required|is_natural_no_zero',array("is_natural_no_zero"=>"Chọn quận huyện"));
	
		$this->form_validation->set_error_delimiters('<span class="input-error ">', '</span>');
		
		if($this->input->post('save'))
		{
			if($this->form_validation->run() == TRUE  )
			{
				$this->user_model->update($iduser,NUll,TRUE);
				$temp['data']['success'] = 1;
				//redirect(base_url('thong-tin-ca-nhan.html'));
			}
		}
		$temp['data']['info'] = $this->user_model->get_one_user($iduser);
		$temp['data']['provinces'] =  $this->pagehtml_model->get_provinces();
		if(USERTYPE=='Mobile'){
			$temp['template']='default/persion/m_info';
			$this->load->view("default/layoutMobile",$temp); 
		}else{
			$temp['template']='default/persion/info';
			$this->load->view("default/layout",$temp); 
		}
	}
	public function email_check($email){
		$email_old = $this->input->post('email_old');
		$count = $this->user_model->count_where(array('email'=>$email,"email !=" =>$email_old));
		if($count>=1){
			$this->form_validation->set_message('email_check', "Email này đã có người sử dụng");
			return false;
		}else{
			return true;
		}
	}
	public function changepassword()
	{
		$avatar = $this->session->userdata('login_user_avatar');
		if(!$avatar) $temp['data']['avatar'] = USER_PATH_IMG."avatar.png";
		else $temp['data']['avatar'] = PATH_IMG_USER.$avatar;
		$iduser = $this->session->userdata('login_user_id');
		
		$this->form_validation->set_message('required','Vui lòng nhập %s');
		$this->form_validation->set_message('matches','Mật khẩu không khớp');
		
		$this->form_validation->set_rules('oldpassword', 'Mật khẩu cũ', 'required|callback_passs_check');
		$this->form_validation->set_rules('password', 'Mật khẩu', 'required|matches[repassword]');
		$this->form_validation->set_rules('repassword', 'Nhập lại mật khẩu', 'required');
		$this->form_validation->set_error_delimiters('<span class="input-error ">', '</span>');
		
		if($this->input->post('save'))
		{
			if($this->form_validation->run() == TRUE  )
			{
				$password = $this->input->post('password');
				$arr = array(
					'password' =>md5($password)
				);
				$this->user_model->update($iduser,$arr,FALSE);
				redirect(base_url('user/successfull'));
				
			}
		}
		if(USERTYPE=='Mobile'){
			$temp['template']='default/persion/m_changepassword';
			$this->load->view("default/layoutMobile",$temp); 
		}else{
			$temp['template']='default/persion/changepassword';
			$this->load->view("default/layout",$temp); 
		}
	}
	public function passs_check($pass){
		$iduser = $this->session->userdata('login_user_id');
		$pass =  md5($pass);
		$count = $this->user_model->count_where(array('password'=>$pass,'id'=>$iduser));
		if($count!=1){
			$this->form_validation->set_message('passs_check', "Mật khẩu cũ không chính xác ");
			return false;
		}else{
			return true;
		}
	}
	public function banner(){
		$iduser = $this->session->userdata('login_user_id');
		$temp['success'] = 0;
		if($this->input->post('save'))
		{
			if(!empty($_FILES['images']['name'])){ 
				$tenhinh = $this->page->rand_string(50);
				$images = $this-> mgallery-> Upload($tenhinh,$_FILES['images'],FCPATH."data/User/",300,"auto");
				if($images !="") {
					$data['avatar'] = $images;
					$this->user_model->update($iduser,$data);
					$this->session->set_userdata(array("login_user_images"=>$images));	
					$temp['data']['success'] = 1;
				}
			}
			
			
		}
		$temp['data']['user'] = $this->user_model->get_one_user($iduser);
		
		if(USERTYPE=='Mobile'){
			$temp['template']='default/persion/m_banner';
			$this->load->view("default/layoutMobile",$temp); 
		}else{
			$temp['template']='default/persion/banner';
			$this->load->view("default/layout",$temp); 
		}
	}
	public function avatar(){
		$avatar = $this->session->userdata('login_user_avatar');
		if(!$avatar) $temp['data']['avatar'] = USER_PATH_IMG."avatar.png";
		else $temp['data']['avatar'] = PATH_IMG_USER.$avatar;
		$iduser = $this->session->userdata('login_user_id');
		
		$config['upload_path']          = './data/User/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['max_size']             = 1024;
		$config['max_width']            = 200;
		$config['max_height']           = 200;
		$config['file_name'] = $this->session->userdata('login_username');
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('userfile'))
		{
			$temp['data']['mess']  = $this->upload->display_errors();
		}
		else
		{
			$info = $this->upload->data();
			$this->session->unset_userdata('login_user_avatar');
			$this->session->set_userdata(array('login_user_avatar'=>$info['file_name']));
			$this->user_model->update($iduser,array("avatar"=>$info['file_name']),FALSE);
			$temp['data']['mess'] = "Đổi ảnh đại diện thành công";
		}
		$temp['template']='default/persion/avatar';
		$this->load->view("default/layout",$temp); 
	}
	function follow($p=0){
		$iduser = $this->session->userdata('login_user_id');
		$numrow = 12;
		$div = 5;
		$skip = $p * $numrow;
		$sql ="SELECT mn_product.*, 
		(SELECT SUM(star)  as total FROM mn_comment WHERE ticlock = 0 AND mn_comment.idpro= mn_product.Id) AS star,
		(SELECT COUNT(Id) as total FROM mn_comment WHERE ticlock = 0 AND mn_comment.idpro= mn_product.Id) AS countstar
		FROM  mn_product INNER JOIN mn_follow ON mn_follow.idpro = mn_product.Id
		WHERE  mn_product.ticlock =0 and mn_product.trash = 0 AND mn_follow.iduser = ".$iduser." 
		GROUP BY mn_product.Id 
		ORDER BY date DESC";	
		$sql_total = "SELECT COUNT(mn_product.Id) AS total 
		FROM  mn_product   INNER JOIN mn_follow ON mn_follow.idpro = mn_product.Id
		WHERE  mn_product.ticlock =0 and mn_product.trash = 0 AND mn_follow.iduser = ".$iduser; 
		$total= $this->product_model->count_query($sql_total);
		$temp['data']['info']= $this->product_model->get_query($sql,$numrow,$skip);
		$temp['data']['page']= $this->page->divPageF($total,$p,$div,$numrow,base_url('san-pham-yeu-thich.html')."/");
		$temp['template']='default/persion/follow';
		$this->load->view("default/layout",$temp); 
	}
	public function notification($p = 0){
		$numrow = 50;
		$div = 10;
		$skip = $p * $numrow;
		$link  = BASE_URL."user/notification/";
		$iduser = $this->session->userdata('login_user_id');
		$sql = "SELECT date,ticlock,view,(SELECT fullname FROM mn_user WHERE mn_user.id = mn_follow.iduser) AS fullname,
				(SELECT shop_name FROM mn_user WHERE mn_user.id = mn_follow.iduser) AS shop_name,
				(SELECT level FROM mn_user WHERE mn_user.id = mn_follow.iduser) AS level
				FROM mn_follow WHERE  ticlock ='0' AND idshop = '".$iduser." ORDER BY date DESC'
				";
		$temp['data']['info']= $this->product_model->get_query($sql,$numrow,$skip);
		$count_follow = $this->pagehtml_model->count_follow(array('idshop'=>(int)$iduser,"ticlock"=>0));  
		$temp['data']['page']= $this->page->divPageF($count_follow,$p,$div,$numrow,$link);
		$temp['data']['breadcrumb'] =  $this->map_title .$this-> arrowmap . '<a href = "'.base_url('user/notification').'" >Danh sách thông báo</a>';
		$temp['template']='default/persion/notification';
		$this->load->view("default/layout",$temp); 
	}
	public function history(){
		$iduser = $this->session->userdata('login_user_id');
		$this->load->model('payment_model');
		$temp['data']['info'] = $this->payment_model->get_list(array('iduser'=>$iduser));
		$temp['template']='default/persion/history';
		$this->load->view("default/layout",$temp); 
	}
	public function cart($id){
		$this->load->model('payment_model');
		$this->load->model('district_model');
		$this->load->model('provinces_model');
		$this->load->model('color_model');
		$this->load->model('size_model');
		$temp['data']['cus']= $this->payment_model->get_list(array('code'=>$id));
		if(empty($temp['data']['cus'])) redirect(base_url("404.html"));
		$temp['data']['cart']= $this->payment_model->get_list_cart(array('idcustomer'=>$temp['data']['cus'][0]["Id"]));
		
		$temp['template']='default/persion/cart';
		$this->load->view("default/layout",$temp); 
	}
	public function payment(){
		$this->load->model('payment_model');
		$iduser = $this->session->userdata('login_user_id');
		$login_user_level = $this->session->userdata('login_user_level');
		$avatar = $this->session->userdata('login_user_avatar');
		if($login_user_level==1) redirect(base_url('thong-tin-ca-nhan.html'));
		$temp['data']['info'] = $this->payment_model->get_list_cart(array('idmanu'=>$iduser));
		$temp['template']='default/persion/payment';
		$this->load->view("default/layout",$temp); 
		
	}
}
