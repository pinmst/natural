<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {
	protected $arrowmap = " > ";
	protected $map_title = '<a href="/">Trang chủ</a>';
	public function __construct()
	{
		 parent::__construct();
		 $this->load->model('catelog_model');
		 $this->load->model('flash_model');
		 $this->load->model('user_model');
		 $this->load->model('tags_model');
		 $this->load->model('size_model');
		 $this->load->model('color_model');
		 $this->load->model('comment_model');
		 $this->load->model('provinces_model');
		 $this->load->model('manufacturer_model');
		 $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
	}

	public function cart()
	{
		$temp['data']['provinces'] = $this->provinces_model->getdata(array('ticlock'=>0));
		$temp['template']='default/payment/gio-hang'; 
		$this->load->view("default/layout",$temp); 	
	}

	public function addcart_fromdetail()
	{
		$size = (int)$this->input->post('size');
		$color = (int)$this->input->post('color');
		$idpro = (int)$this->input->post('idpro');
		$quanty = (int)$this->input->post('quanty');
		$deal = (int)$this->input->post('deal');

		$alias = $this->input->post('alias');
		$name_pro = $this->input->post('name_pro');
		
		$cart = $this->session->userdata('cart');
		if(empty($cart)){
			$arr[]= array('idpro'=>$idpro,'qty'=>$quanty,'color'=>$color,'size'=>$size,'deal'=>$deal);
			$this->session->set_userdata('cart',$arr);
		}else{
			$c= 0;
			$j=0;
			foreach($cart as $k=>$v){
				if($v['idpro']==$idpro){
					$arr[$k] = 	array("idpro"=>$idpro,"qty"=>$quanty,"color"=>$color,'size'=>$size,'deal'=>$deal);
					$c = 1;
				}else{
					$arr[$k] = $v;
				}
				$j=$k;
			}
			if($c==0){
				$arr[$j+1] = array("idpro"=>$idpro,"qty"=>$quanty,"color"=>$color,"size"=>$size,'deal'=>$deal);
			}
			$this->session->set_userdata('cart',$arr);
		}
		$this->session->set_flashdata('message_addcart_fromdetail_success', '"'.$name_pro.'" đã được thêm vào giỏ hàng.');
		redirect(base_url($alias.'.html'));
	}

	public function addcart_fromcat()
	{
		$size = (int)$this->input->post('size');
		$color = (int)$this->input->post('color');
		$idpro = (int)$this->input->post('idpro');
		$quanty = (int)$this->input->post('quanty');
		$deal = (int)$this->input->post('deal');

		$link = $this->input->post('link');
		$name_pro = $this->input->post('name_pro');
		
		$cart = $this->session->userdata('cart');
		if(empty($cart)){
			$arr[]= array('idpro'=>$idpro,'qty'=>$quanty,'color'=>$color,'size'=>$size,'deal'=>$deal);
			$this->session->set_userdata('cart',$arr);
		}else{
			$c= 0;
			$j=0;
			foreach($cart as $k=>$v){
				if($v['idpro']==$idpro){
					$arr[$k] = 	array("idpro"=>$idpro,"qty"=>$quanty,"color"=>$color,'size'=>$size,'deal'=>$deal);
					$c = 1;
				}else{
					$arr[$k] = $v;
				}
				$j=$k;
			}
			if($c==0){
				$arr[$j+1] = array("idpro"=>$idpro,"qty"=>$quanty,"color"=>$color,"size"=>$size,'deal'=>$deal);
			}
			$this->session->set_userdata('cart',$arr);
		}
		$this->session->set_flashdata('message_addcart_fromcat_success', '"'.$name_pro.'" đã được thêm vào giỏ hàng.');
		redirect($link);
	}

	public function delcart($id)
	{
		$cart = $this->session->userdata('cart');
		unset($cart[$id]);
		$this->session->set_userdata('cart', $cart);
		redirect(base_url('gio-hang'));
	}

	public function delcartajax()
	{
		$id = (int)$this->input->post('id');
		$cart = $this->session->userdata('cart');
		unset($cart[$id]);
		$this->session->set_userdata('cart', $cart);
		$temp['cart'] = $this->session->userdata('cart');
		$this->load->view("default/payment/gio-hang",$temp); 
	}

	public function addcartajax()
	{
		$size = (int)$this->input->post('size');
		$color = (int)$this->input->post('color');
		$idpro = (int)$this->input->post('idpro');
		$quanty = (int)$this->input->post('quanty');
		$deal = (int)$this->input->post('deal');
		$cart = $this->session->userdata('cart');
		if(empty($cart)){
			$arr[]= array('idpro'=>$idpro,'qty'=>$quanty,'color'=>$color,'size'=>$size,'deal'=>$deal);
			$this->session->set_userdata('cart',$arr);
		}else{
			$c= 0;
			$j=0;
			foreach($cart as $k=>$v){
				if($v['idpro']==$idpro){
					$arr[$k] = 	array("idpro"=>$idpro,"qty"=>$quanty,"color"=>$color,'size'=>$size,'deal'=>$deal);
					$c = 1;
				}else{
					$arr[$k] = $v;
				}
				$j=$k;
			}
			if($c==0){
				$arr[$j+1] = array("idpro"=>$idpro,"qty"=>$quanty,"color"=>$color,"size"=>$size,'deal'=>$deal);
			}
			$this->session->set_userdata('cart',$arr);
		}
		$temp['cart'] = $this->session->userdata('cart');
		
		$this->load->view("default/payment/shortcart",$temp); 
	}

	public function editcart()
	{
		$cart = $this->session->userdata('cart');
		$fillter  = $this->input->post('quanty');
	
		if(!empty($fillter)){
			foreach($fillter as $k=>$v){
				if($v<=0) unset($cart[$k]);
				else
					$cart[$k]['qty'] = $v;
			}
		}
		$this->session->set_userdata('cart',$cart);
		redirect(base_url('gio-hang'));
	}

	public function order()
	{
		$this->load->model('payment_model');
		
		$temp['data']['info_payment']= json_decode(get_cookie("info_payment"));
		$cart = $this->session->userdata('cart');
		$count = count($cart);
		//if($this->input->post('save'))
		if($this->input->post())
		{
			if($count >0 )
			{
				$data['code'] = $code = $this->page->getCode($this->page->rand_number());
				$data['fullname'] = $fullname = $this->input->post('fullname');
				$data['email'] = $email = $this->input->post('email');
				$data['address'] = $address = $this->input->post('address');
				$data['ward'] = $ward = $this->input->post('ward');
				$data['city'] = $city = $this->input->post('city');
				$data['phone'] = $phone = $this->input->post('phone');
				$data['note'] = $note = $this->input->post('note');
				$data['date'] = $date = time();
				$data['methodpay'] = $methodpay = $this->input->post('fpaymentmethod');
				// $data['transfee'] = $transfee = $this->input->post('transfee');

				$id = $this->payment_model->add($data);
				$tongthanhtoan = 0;
				$tong = 0;
				foreach($cart as $k => $v){
					$arr['idpro'] = $idpro=  (int)$v['idpro'];
					if($idpro>0){
						$pro = $this->product_model->get_Id($idpro);
						$this->product_model->countorder($idpro);
						$arr['amount']=  (int)$v['qty'];
						$arr['idmanu']=  (int)$pro[0]['iduser'];
						$arr['idcustomer']=  $id;
						$arr['price']=  $pro[0]['sale_price'];
						$arr['size'] = $v['size'];
						$arr['color'] = $v['color'];
						//$arr['deal'] = (int)$v['deal'];
						$tong = $pro[0]['sale_price']*$v['qty'];
						$tongthanhtoan += $tong; 
						$this->payment_model->addcart($arr);

						$sanpham .= ' <tr><td align="left" valign="top" style="padding:3px 9px">
						                 <span>'.$pro[0]['title_vn'].'</span><br></td>
						                 <td align="left" valign="top" style="padding:3px 9px"><span>'.bsVndDot($pro[0]['sale_price']).'&nbsp;₫</span></td>
						                 <td align="left" valign="top" style="padding:3px 9px">'.$v['qty'].'</td>
						                 <td align="right" valign="top" style="padding:3px 9px"><span>'.bsVndDot($pro[0]['sale_price']*$v['qty']).'&nbsp;₫</span></td>
						               </tr>'; 
					}
				}

				//sent email
				$noidung =  file_get_contents(BASE_URL."/public/email/xacnhandonhang.html");
				$noidung =str_replace("{SANPHAM}", $sanpham ,$noidung);
				$noidung =str_replace("{FULLNAME}", $data['fullname'] ,$noidung);
				$noidung =str_replace("{CODE}", "#".$code ,$noidung);
				$noidung =str_replace("{DATE}", date('d-m-y H:i:s') ,$noidung);
				$noidung =str_replace("{EMAIL}", $email ,$noidung);
				$noidung =str_replace("{PHONE}", $phone ,$noidung);
				$noidung =str_replace("{DIACHI}", $address . ',' . $ward .','. $city  ,$noidung);
				// $noidung =str_replace("{SHIP}", $transfee ,$noidung);
				$noidung =str_replace("{PTTHANHTOAN}", $methodpay,$noidung);
				$noidung =str_replace("{TONGTHANHTOAN}", bsVndDot($tongthanhtoan) ,$noidung);
				//$noidung =str_replace("{HOTLINE}", $web['hotline'] ,$noidung);
				$this->sendmail($noidung, $data['email']);
				$this->session->unset_userdata('code');
				$this->session->unset_userdata('cart');
				redirect(base_url('dat-hang-thanh-cong/'.$code));
			}else{
				redirect(base_url('gio-hang'));
			}
		}
		$temp['data']['breadcrumb'] =  $this->map_title . $this->arrowmap . '<a href = "/gio-hang" >Giỏ hàng</a>';
		$temp['data']['provinces'] = $this->provinces_model->getdata(array('ticlock'=>0));
		$this->session->set_flashdata('payment_success', 'Bạn đã đặt hàng thành công');
		$temp['template']='default/payment/gio-hang'; 
		$this->load->view("default/layout",$temp);
	}

	/*public function testmail(){
		$this->sendmail('test', 'nvtuan.it007@gmail.com');
	}*/

	public function sendmail($body, $email_nguoinhan){
		$this->load->library('send_email');
		$this->send_email->_Send('Xác nhận đặt hàng ', $body, '', $email_nguoinhan);
        $this->session->set_flashdata('message_success', 'Đặt hàng thành công, thông tin đơn hàng được gửi về email của bạn!');
        $response = array('result' => 'success');
        header('Content-Type: application/json');
        echo json_encode( $response );
	}

	public function success($id)
	{
		$this->load->model('payment_model');
		$this->load->model('district_model');
		$this->load->model('provinces_model');
		$this->load->model('color_model');
		$this->load->model('size_model');
		$temp['data']['cus']= $this->payment_model->get_list(array('code'=>$id));
		if(empty($temp['data']['cus'])) redirect(base_url("404.html"));
		$temp['data']['cart']= $this->payment_model->get_list_cart(array('idcustomer'=>$temp['data']['cus'][0]["Id"]));
		$temp['template']='default/payment/over-view';
		$this->load->view("default/layout",$temp); 
		
	}
}
