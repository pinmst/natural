<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {
	protected $arrowmap = " > ";
	protected $map_title = '<a href="/">Trang chủ</a>';
	public function __construct()
	{
		 parent::__construct();
		 $this->load->model('catelog_model');
		 $this->load->model('catnews_model');
		 $this->load->model('news_model');
		 $this->load->model('flash_model');
		 $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
	}
	public function detail($alias){
		$temp['data']['idcat'] = 0;
		$temp['data']['info'] = $info = $this->news_model->get_list(array('alias'=>$alias));
		if(!$alias || empty($info)) redirect(base_url('404.html'));
		$temp['meta']['title'] = strip_tags($info[0]['title_vn']);
		
		$temp['data']['cat'] = $this->catnews_model->get_where($info[0]['idcat']);
		$temp['data']['breadcrumb'] =  $this->map_title .$this-> arrowmap . '<a href = "'.base_url('tin-tuc/'.$info[0]['alias'].".html").'" >'.$info[0]['title_vn'].'</a>';
		
		$temp['data']['baiviet'] = $this->pagehtml_model->get_newsidcat(10,10,0);
		$temp['template']='default/news/detail';
		$this->load->view("default/layout",$temp); 
	}

	public function catelog($alias,$p=0){
		$info_cat = $this->catnews_model->get_list(array('alias'=>$alias));
		if(empty($info_cat)) redirect(base_url('404.html'));
		
		$temp['meta']['title'] = $info_cat[0]['title_vn'];
		$temp['meta']['description'] = $info_cat[0]['meta_description'];
		$temp['meta']['keywork'] = $info_cat[0]['meta_keyword'];
		
		$config['base_url']	=	base_url('tin-tuc-su-kien/'.$info_cat[0]['alias'].".html");
		
		$config['total_rows']	=	$this->news_model->count_where(array("ticlock"=>0,"idcat"=>$info_cat[0]['Id']));
		$config['per_page']	=	10;
		$config['num_links'] = 5;
		
		$this->pagination->initialize($config);
		$temp['data']['info'] = $this->news_model->get_list(array("ticlock"=>0,"idcat"=>$info_cat[0]['Id']),"sort DESC, Id DESC",$config['per_page'],$p);
		
		$temp['data']['breadcrumb'] =  $this->map_title .$this-> arrowmap."<a href='".base_url("tin-tuc-su-kien/".$info_cat['0']['alias'].".html")."'>".$info_cat[0]['title_vn']."</a>";
		
		
		$temp['template']='default/news/catelog'; 
		
		$this->load->view("default/layout",$temp); 
		
	}
}
