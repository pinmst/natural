<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';

$route['index'] = 'home/index';
$route['page/(:any).html'] = 'home/detail/$1';
$route['dang-khuyen-mai'] = 'product/sales/$1'; 
$route['gio-hang'] ="payment/cart";
$route['addcart_fromcat'] ="payment/addcart_fromcat";
$route['addcart_fromdetail'] ="payment/addcart_fromdetail";
$route['search'] ="product/search";
$route['tin-tuc/(:any).html'] ="news/detail/$1";
$route['tin-tuc-su-kien/(:any)'] = 'news/catelog/$1';
$route['tin-tuc-su-kien/(:any)/(:num)'] = 'news/catelog/$1/$2';
$route['thanh-toan'] = 'payment/order';
$route['dat-hang-thanh-cong/(:num)'] = 'payment/success/$1';
$route['thuong-hieu.html'] ="product/listbrand";
$route['thuong-hieu/(:any)'] ="product/brand/$1";
// $route['call-back'] ="home/call_back";

$route['admincp'] ="admincp/login";

$route['404.html'] ="home/notfoud";
$route['(:any)\.html'] ="product/detail/$1";
$route['(:any)'] = 'product/getCatelog/$1';

$route['translate_uri_dashes'] = FALSE;

$route['404_override'] = 'home/notfound';
/*


<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';

$route['user/notification'] = 'persion/notification';
$route['user/notification/(:num)'] = 'persion/notification/$1';
$route['quan-ly-san-pham.html'] = 'persion/index';
$route['quan-ly-san-pham.html/p(:num)'] = 'persion/index/$1';
$route['dang-san-pham.html'] = 'persion/addthread';
$route['doi-mat-khau.html'] = 'persion/changepassword';
$route['gia-soc-moi-ngay.html'] = 'home/landing';
$route['banh-trung-thu-maxims-bakery.html'] = 'landing/index';
$route['doi-banner.html'] = 'persion/banner';
//$route['thanh-toan.html'] = 'payment/order_b1';
$route['thanh-toan.html/buoc-2'] = 'payment/order_b2';
$route['thanh-toan.html'] = 'payment/order_b3';
$route['dat-hang-thanh-cong.html'] = 'payment/success';

$route['dang-nhap-facebook'] = 'user/urlfacebook';
$route['dang-nhap-google'] = 'user/urlgoogle';
$route['dang-nhap-twitter'] = 'user/urltwitter';
$route['san-pham-yeu-thich.html'] = 'persion/follow';
$route['lich-su-giao-dich.html'] = 'persion/history';
$route['order-view/(:num)'] = 'persion/cart/$1';
$route['quan-ly-don-hang.html'] = 'persion/payment';
$route['thanhvien/delpro/(:num).htm'] = 'persion/delthread/$1';
$route['thanhvien/editpro/(:num).htm'] = 'persion/editthread/$1';
$route['thong-tin-ca-nhan.html'] = 'persion/info';
$route['thoat.html'] = 'user/logout';
$route['quen-mat-khau.html'] = 'user/forgot';
$route['hot-deal'] = 'deal/hot';
$route['hot-deal/(:num)'] = 'deal/hot/$1';   
$route['deal/(:any)-(:num).html'] = 'deal/detail/$2'; 
$route['deal/(:any)'] = 'deal/catelog/$1'; 
$route['deal/(:any)/(:num)'] = 'deal/catelog/$1/$2'; 
$route['chu-de/(:any).html'] = 'news/catelog/$1'; 
$route['chu-de/(:any).html/(:num)'] = 'news/catelog/$1/$2'; 
$route['addcart.html'] ="payment/addcart";
$route['gio-hang.html'] ="payment/order";
$route['gio-hang-moblie.html'] ="payment/order"; // giỏ hàng mobile

$route['thuong-hieu.html'] ="product/listbrand";
$route['gio-hang.html/(:num)'] ="payment/cart/$1";
$route['kiem-tra-don-hang.html'] ="payment/search";
$route['xu-huong.html'] ="product/xuhuong";
$route['thuong-hieu/(:any)'] ="product/brand/$1";
$route['khuyen-mai.html'] ="product/sphot";
$route['san-pham-han-che-doi-tra.html'] ="product/xlimit";
$route['tim-kiem.html'] ="product/search";
$route['ban-chay.html'] ="product/order";
$route['bai-viet/(:any).html'] ="news/detail/$1";
$route['dang-nhap.html'] ="user/notaccess";
$route['dang-ky.html'] ="user/register";
$route['dang-ky-thanh-vien.html'] ="user/member";
$route['thong-bao.html'] = "user/thanks";
$route['san-pham/(:any)-(:num).html'] ="product/detail/$2";
$route['admincp'] ="admincp/login";
$route['404.html'] ="home/notfoud";
$route['(:any)'] ="product/catelog/$1";

$route['translate_uri_dashes'] = FALSE;
$route['404_override'] = 'home/notfound';
*/